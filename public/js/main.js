var isMSIE = /*@cc_on!@*/0;
var isSafari = navigator.userAgent.toLowerCase().indexOf('safari') != -1;
var all_content = $('#all-content');
function loadCarousel() {
    $('.carousel').carousel({
        auto: true,
        period: 3000,
        effect: "fade",
        duration: 500,
        // data-width: 100%,
        height: 100,
        markers: {
            type: "default",
            position: "top-left"
        }
    });
}


(function (window, undefined) {

    $(document).ready(function () {
        port_id = $('#port_id').val();
        window.app = {};
        app.BrainSocket = new BrainSocket(
            new WebSocket('ws://' + window.location.hostname + ':' + port_id),
            new BrainSocketPubSub()
        );


        app.BrainSocket.Event.listen('notification.event', function (msg) {
            if (msg !== undefined) {
                $.get("/notifications/sticky").done(function (response) {

                    sticky(response);

                });
            }
        });


        app.BrainSocket.Event.listen('live_score.event', function (msg) {
            if (msg !== undefined) {
                var match_id = msg.client.data[0];
                $("#score_" + match_id).load("/index/live-score/" + match_id, function (response) {
                    loadCarousel();
                });
            }
        });

        app.BrainSocket.Event.listen('live_score.event', function (msg) {
            if (msg !== undefined) {
                var match_id = msg.client.data[0];
                $("#score_" + match_id).load("/scores/player-scores/" + match_id, function (response) {
                    loadCarousel();
                });
            }
        });
        app.BrainSocket.Event.listen('live_score_app_mode.changed', function (msg) {
            if (msg !== undefined) {
                if ($(".live-score-public").length > 0 || $(".live-rfet-score").length > 0 || $(".table-livescore").length > 0) {
                    var match_id = msg.client.data.match_id;
                    var setscore = msg.client.data.setpoints;
                    var tiebreak = msg.client.data.tiebreak;
                    var gamescore = msg.client.data.gamescore;
                    var set_in_progress = msg.client.data.set_in_progress;
                    var tie_break = msg.client.data.tie_break;
                    var completed = msg.client.data.completed;

                    var showscore = ' ';
                    if (completed)
                        showscore = "Completed ";

                    for (i = 0; i < set_in_progress; i++) {
                        //showscore += setscore.team1[i] != 0 || i==0 ? setscore.team1[i] : '';
                        showscore += setscore.team1[i];
                        showscore += tiebreak.team1[i] != 0 && tiebreak.team1[i] != null ? '(' + tiebreak.team1[i] + ')' : '';
                        showscore += '-';
                        //showscore += setscore.team2[i] != 0 || i==0 ? setscore.team2[i] : '';
                        showscore += setscore.team2[i];
                        showscore += tiebreak.team2[i] != 0 && tiebreak.team2[i] != null ? '(' + tiebreak.team2[i] + ')' : '';
                        showscore += ' ';
                    }

                    if (!tie_break) {
                        showscore +=
                            (gamescore.currentScoreTeam11 != 'u' ? gamescore.currentScoreTeam11 : "0")
                            + (gamescore.currentScoreTeam12 != 'n' ? gamescore.currentScoreTeam12 : "0")
                            + ":"
                            + (gamescore.currentScoreTeam21 != 'u' ? gamescore.currentScoreTeam21 : "0")
                            + (gamescore.currentScoreTeam22 != 'n' ? gamescore.currentScoreTeam22 : "0");
                    }

                    $("#score_" + match_id).text(showscore);
                }
            }
        });



    $(document.body).on('click', '.deleteDraw', function(){
            var draw_id = $(this).attr('draw-id');
            var tour_id = $(this).attr('tournament-id');

            if (confirm("Are you sure you want to delete this draw") == true) {
                window.location.href='/tournaments/delete-draw/' + draw_id;
            } else {
                closeDialog();
            }
        });

    });
    var History = window.History;
    if (!History.enabled) {
        return false;
    }

    History.Adapter.bind(window, 'statechange', function () {
        var State = History.getState();
    });

    if (!isSafari) {
        window.addEventListener('popstate', function (event) {
            //console.log(event.target.location.href);
            window.location = event.target.location.href;
        });
    }

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');


    if (!isMSIE) {
        $(document).on("click", "a.call, a.tiles", function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            var title = $(this).data('title');
            loadAjax(link, title);
            // return false;
        });
    }

})(window);

function loadAjax(curl, title) {
    all_content = $('#all-content');
    $("body .metro").prepend('<div id="loading-bar-spinner" class="history-spinner"><div class="spinner-icon"></div></div>');
    $.ajax({
        cache: true,
        type: "GET",
        url: curl,
        data: {'history': true},
        beforeSend: function (xhr) {
            $('#all-content').fadeOut(400);
        },
        success: function (html) {
            $('.history-spinner').remove();
            History.pushState(null, title, curl);
            if (all_content.is(":visible")) {
                all_content.fadeOut(400, function () {
                    all_content.html(html).fadeIn(400);
                });
            }
            else {
                all_content.html(html).fadeIn(400);
                // $.get("/notifications/sticky").done(function(response) {sticky(response);});
            }
            loadCarousel();
        },
        error: function (xhr, textStatus, errorThrown) {
            //TODO: remove after demo
            //if (xhr.status == 500) {
            //    window.location = "/";
            //}
            $('.history-spinner').remove();
            $("body .metro").html('<div id="loading-bar-spinner" class="history-spinner" style="color:#C33A32; font-weight:bold; font-size:20px; font-family: Open Sans;">' +
            "Error Occured" + '</div>');
        }
    });
}

function sticky(response) {
    var counter = 0;
    //response.length je hack ovoga se treba rijesiti na suptilniji nacin
    // if (response != 0 && response.length < 1300) {
    var path = location.pathname.split('/')[1];
    if (response != 0 && path != 'login') {
        $.each(response, function (key, val) {
            setTimeout(function () {
                $.Notify({
                    caption: val.caption,
                    content: val.message,
                    style: {background: '#16499A', color: '#fff'}
                });
            }, counter);
            counter += 1500;
        });
    }


}

function isFullscreen() {

    if (
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement
    ) {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function () {


    $('body').on('change', '#referee_check', function() {
        var id = $(this).data('id');
        if (this.checked)
            var data = {is_super: 1};
        else
            var data = {is_super: 0};

        $.ajax({
            cache: true,
            type: "POST",
            url: '/referees/super-state/' + id,
            data: data,
            success: function (response) {
                $.Notify({
                    caption: "Success",
                    content: response.message,
                    style: {background: '#16499A', color: '#fff'}
                });
            }
        });
    });

    $("#tryCanvas").on('click', function (e) {
        e.preventDefault();
        html2canvas(document.getElementById('gracket'), {
            onrendered: function (canvas) {

                $(document).write(canvas);

                var img = canvas.toDataURL();
                var img_element = "<img id='printImg' src='" + img + "' alt=''>";


                //canvas.strokeStyle = '#111';

                //document.write(img_element);

                //window.print();
            }
        });
    });


    var path = location.pathname.split('/')[1];
    if (path != 'login'
        && path != 'password-reset'
        && path != 'auth') {
        $.get("/notifications/sticky").done(function (response) {
            sticky(response);
        });
    }

    var screen_change_events = "webkitfullscreenchange mozfullscreenchange fullscreenchange";
    $(window).on(screen_change_events, function () {
        var canvas = $('.g_canvas')[0];
        var ctx = canvas.getContext('2d');
        if (isFullscreen())
            ctx.strokeStyle = '#111';
        else
            ctx.strokeStyle = '#fff';

        ctx.stroke();

    });

    $(".showFullScreen").click(function () {
        var fullScreenId = $(this).attr("fullscreen-body");
        if (fullScreenId != "")
            $(fullScreenId).fullscreen();
    });

    $('.view-schedule-full-screen').click(function () {
        $(".grid").fullscreen();
    })

    $('body').on('click', '.printSignupList', function (e) {
        e.preventDefault();

        $("#signup-list-printable").printThis({
            debug: false,               //* show the iframe for debugging
            importCSS: false,            //* import page CSS
            importStyle: false,         //* import style tags
            printContainer: false,       //* grab outer container as well as the contents of the selector
            loadCSS: "/css/bracket-print.css",  //* path to additional css file - us an array [] for multiple
            // pageTitle: "",              //* add title to print page
            removeInline: false,        //* remove all inline styles from print elements
            printDelay: 333,            //* variable print delay
            header: null,               //* prefix to html
            formValues: true            //* preserve input/form values
        });
    });

    $(".printBrackets").on("click", function () {
        window.print();
    });

    Dropzone.options.redirectAfterUpload = {
//        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 5, // MB
        parallelUploads: 1,
        uploadMultiple: false,
        autoRedirect: true,
        accept: function (file, done) {
            done();
        }
    };

    $(document).on("keyup", "#resultsForm input", function (e) {
        var klass = ($(this).hasClass('tie-input')) ? 'tie-input' : 'table-input';

        if (klass == 'table-input') {
            if (e.keyCode != 13 && e.keyCode != 8 && e.keyCode != 9) {
                if (this.tabIndex < 5) {
                    var nextElement = $('input.' + klass + '[tabindex="' + (this.tabIndex + 1) + '"]');
                    if (nextElement.length) { // this is for next element
                        nextElement.focus();
                        nextElement.select();
                    } else {
                        $('input.' + klass + '[tabindex="1"]').focus();  //this is for first element
                        $('input.' + klass + '[tabindex="1"]').select();  //this is for first element
                    }
                }
            }
        }

    });

    $(document).on("click", "#recalculate", function (e) {
        e.preventDefault();

        if (confirm('Recalculate ranking status for player? This can take up to 20 minutes!')) {
            $("#recalculate").text("RECALCULATING");
            counter = setTimeout(function () {
                alert('This action will take longer time. We will email you when the process is done.');
            }, 5000);
            $.ajax({
                cache: true,
                type: "GET",
                url: $(this).attr('href'),
                success: function (response) {
                    if (response.success) {
                        clearTimeout(counter);
                        if (!response.taking_long) {
                            $("#recalculate").text($("#recalculate").attr("msg"));
                            alert('Recalculating points is already done for this week or there is no score to be recalculated');
                        }
                    }
                }

            });
        }
    });

    // $('.carousel').carousel({
    $(window).on('load', function () {
        loadCarousel();
    });

    $(function () {

        draggedSchedule();

        $('.add_new_row').click(function () {
            $(".additional-row .select2-container:first-of-type").remove();
            $(".additional-row").clone().insertBefore(".additional-row").removeClass("additional-row").removeClass("hidden-row");
            var next_row = parseInt($(".hidden-row .court-column").attr('data-row'), 10) + 1;
            $(".additional-row .court-column").attr('data-row', next_row);
            $("select").select2({width: '140px'});
            draggedSchedule();
        })
        $(".button.date_select").click(function () {
            $(".button.date_select").removeClass("selected-date").addClass("not-selected-date");
            $(this).removeClass("not-selected-date").addClass("selected-date");
        })
        //$("body").on("click", ".aHeading", function () {
        $("body").on("click", ".aHeading", function () {
            var ah_id = $(this).attr('ah-id');
            var flag = $('#' + ah_id).hasClass("togAcc");

            $(".toggleAccordion").text('+');
            $(".toggleAccordion").removeClass("togAcc");

            if (!flag) {
                $('#' + ah_id).text('-');
                $('#' + ah_id).addClass("togAcc");
            }
        });

        $(".print_round_right").each(function () {

            if ($(this).attr('print-round') == 1)
                $(this).css('margin-top', 23);
            else if ($(this).attr('print-round') == 2)
                $(this).css('margin-top', 70);
            else if ($(this).attr('print-round') == 3)
                $(this).css('margin-top', 162);
            else if ($(this).attr('print-round') == 4)
                $(this).css('margin-top', 345);
            else if ($(this).attr('print-round') == 5)
                $(this).css('margin-top', 500);
        });

        $(".print_round").each(function () {

            //console.log($(this).attr('print-round'));

            if ($(this).attr('print-round') == 1)
                $(this).css('margin-top', 23);
            else if ($(this).attr('print-round') == 2)
                $(this).css('margin-top', 70);
            else if ($(this).attr('print-round') == 3)
                $(this).css('margin-top', 162);
            else if ($(this).attr('print-round') == 4)
                $(this).css('margin-top', 345);

        });
        $(".match").each(function () {

            //console.log($(this).attr('print-match'));

            if ($(this).attr('print-match') == 1)
                $(this).css('margin-bottom', 52);
            else if ($(this).attr('print-match') == 2)
                $(this).css('margin-bottom', 144);
            else if ($(this).attr('print-match') == 3)
                $(this).css('margin-bottom', 330);
            else if ($(this).attr('print-match') == 4)
                $(this).css('margin-bottom', 500);
            else if ($(this).attr('print-match') == 4)
                $(this).css('margin-bottom', 500);

        });
    });


    $(function () {

        var $sidebar = $(".side-holder"),
            $window = $(window),
            offset = $sidebar.offset(),
            topPadding = -300;

        $window.scroll(function () {
                //$(".viewport").css("height","100%");
                // $(".thirty").css("height", "80%")

                if ($window.scrollTop()) {
                    $sidebar.stop().animate({
                        marginTop: $window.scrollTop() + topPadding
                    });
                } else {
                    $sidebar.stop().animate({
                        marginTop: 0
                    });
                }

        });

    });

    $('body').on('change', '.type_of_time', function (e) {
        var type_of_time = $(this).val();
        var droppedOn = $(this).closest(".droppable");
        var time = $(droppedOn).find(".time_element").val();
        var tournament_id = $("#tournament_id").val();
        var date_selected = $(".date_select.selected").data("id");
        if (date_selected == undefined)
            date_selected = $(".date_select:first-child").data("id");

        if (type_of_time == 3)
            $(droppedOn).find(".time_element").css('visibility', 'hidden');
        else
            $(droppedOn).find(".time_element").css('visibility', 'visible');

        if (droppedOn.has('.draggable').length) {
            droppedOn.find("span.text").html("Checking...");
            $.ajax({
                type: "POST",
                url: "/schedule/schedule-check/true",
                data: {
                    'time_of_play': time,
                    'type_of_time': type_of_time,
                    'tournament_id': tournament_id,
                    'date_selected': date_selected,
                    'row': $(droppedOn).data("row"),
                    'match_id': droppedOn.find(".draggable").data("matchid")
                },
                success: function (response) {
                    droppedOn.find("span.text").html(response.message);
                    droppedOn.removeClass(function (index, css) {
                        return (css.match(/(^|\s)state-\S+/g) || []).join(' ');
                    }).addClass(response.class);
                }
            });
        }
    });

    $('body').on('change', '#courts_number', function (e) {
        var tournament_id = $("#tournament_id").val();
        var date_selected = $(".date_select.selected").data("id");
        if (date_selected == undefined)
            date_selected = $(".date_select:first-child").data("id");

        $.ajax({
            type: "POST",
            url: "/schedule/update-configuration",
            data: {
                'tournament_id': tournament_id,
                'date_selected': date_selected,
                'number_of_courts': $(this).val()
            },
            success: function (response) {
                if (response.success)
                    window.location = response.redirect_to;
                else
                    console.log(response);
            }
        });
    });

    $('body').on('click', '.delete-schedule', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        var tournament_id = $(this).data('tournamentid');
        if (confirm($(this).data('message'))) {
            $.ajax({
                type: "DELETE",
                url: $(this).attr('href'),
                success: function (response) {
                    if (response.success) {
                        $('#draggable_' + id).closest(".court-drop").removeClass('state-error');
                        $('#draggable_' + id).closest(".court-drop").find('.text').html('');

                        $('#draggable_' + id).remove();

                        $('.teams-by-draw').load('/schedule/matches-data/' + tournament_id, function () {
                            draggedSchedule();
                        });

                        scheduleSuccess();

                    }
                    else
                        console.log(response);
                }
            });
        }
    })

    $('body').on('keydown', '.only-number', function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

function scheduleSuccess() {
    $.Notify({
        caption: "Success",
        content: "Form saved",
        style: {background: '#16499A', color: '#fff'}
    });
}

function draggedSchedule() {
    $(".time_element").timepicki({
        increase_direction: 'up',
        max_hour_value: 24,
        show_meridian: false
        //start_time: ["09", "00"]
    });

    $(".time_element").blur(function () {
        var time = $(this).val();
        var droppedOn = $(this).closest(".droppable");
        var type_of_time = $(droppedOn).find(".type_of_time option:selected").val();
        var tournament_id = $("#tournament_id").val();
        var date_selected = $(".date_select.selected").data("id");
        if (date_selected == undefined)
            date_selected = $(".date_select:first-child").data("id");
        if (droppedOn.has('.draggable').length) {
            droppedOn.find("span.text").html("Checking...");
            $.ajax({
                type: "POST",
                url: "/schedule/schedule-check/true",
                data: {
                    'time_of_play': time,
                    'tournament_id': tournament_id,
                    'type_of_time': type_of_time,
                    'date_selected': date_selected,
                    'row': $(droppedOn).data("row"),
                    'match_id': droppedOn.find(".draggable").data("matchid")
                },
                success: function (response) {
                    droppedOn.find("span.text").html(response.message);
                    droppedOn.removeClass(function (index, css) {
                        return (css.match(/(^|\s)state-\S+/g) || []).join(' ');
                    }).addClass(response.class);
                }
            });
        }
    })

    $(".draggable").draggable({
        revert: "invalid",
        revertDuration: 200,
        start: function (event, ui) {
            dragposition = ui.position;
        }
    });

    $(".droppable").droppable({
        activeClass: "state-active",
        hoverClass: "state-hover",
        over: function (event, ui) {
            $('.droppable').droppable('enable');
            if ($(this).has('.draggable').length) {
                $(this).droppable('disable');
            }
        },
        drop: function (event, ui) {
            var date_selected = $(".date_select.selected").data("id");
            if (date_selected == undefined)
                date_selected = $(".date_select:first-child").data("id");

            var tournament_id = $("#tournament_id").val();
            var dropped = ui.draggable;
            var droppedOn = $(this);
            $(dropped).detach().css({top: 0, left: 0}).appendTo(droppedOn);
            var text_holder = $(this)
                .addClass("state-inprogress")

            text_holder.find("span.text").html("Checking...");
            $.each($(".droppable"), function (key, value) {
                if ($(value).has('.draggable').length == 0) {
                    $(value).removeClass("state-inprogress").removeClass("state-error").removeClass("state-accepted");
                }
            });

            $.ajax({
                type: "POST",
                url: "/schedule/schedule-check",
                data: {
                    'match_id': $(dropped).data("matchid"),
                    'draw_id': $(dropped).data("drawid"),
                    'list_type': $(dropped).data("listtype"),
                    'time_of_play': $(droppedOn).find(".time_element").val(),
                    'type_of_time': $(droppedOn).find(".type_of_time option:selected").val(),
                    'court_number': $(droppedOn).data("courtnumber"),
                    'row': $(droppedOn).data("row"),
                    'date_selected': date_selected,
                    'tournament_id': tournament_id
                },
                success: function (data) {
                    text_holder.removeClass(function (index, css) {
                        return (css.match(/(^|\s)state-\S+/g) || []).join(' ');
                    }).addClass(data.class);

                    text_holder.find("span.text").html(data.message);

                    $(dropped).attr('id', 'draggable_' + $(dropped).data('matchid'));

                    $(dropped).prepend('<a class="delete-schedule" data-id="' + $(dropped).data('matchid')
                    + '" data-message="Remove match?" data-tournamentid="' + tournament_id
                    + '" href="/schedule/schedule/' + $(dropped).data('matchid') + '">x</a>');

                    if (data.success)
                        scheduleSuccess();

                    return;
                }
            })
        },
        out: function (event, ui) {
            $(this).find("span.text").html("");
            $(this).removeClass("state-inprogress state-error state-accepted state-warning");
        }
    });
}

function leftPad(number, targetLength) {
    var output = number + '';
    while (output.length < targetLength) {
        output = '0' + output;
    }
    return output;
}


(function ($, undefined) {
    $.expr[":"].containsNoCase = function (el, i, m) {
        var search = m[3];
        if (!search) return false;
        return new RegExp(search, "i").test($(el).text());
    };

    $.fn.searchFilter = function (options) {
        var opt = $.extend({
            // target selector
            targetSelector: "",
            // number of characters before search is applied
            charCount: 1
        }, options);

        return this.each(function () {
            var $el = $(this);
            $el.keyup(function () {
                var search = $(this).val();

                var $target = $(opt.targetSelector);
                $target.show();

                if (search && search.length >= opt.charCount)
                    $target.not(":containsNoCase(" + search + ")").hide();
            });
        });
    };
})(jQuery);
