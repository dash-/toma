var isMSIE = /*@cc_on!@*/0;
var isSafari = navigator.userAgent.toLowerCase().indexOf('safari') != -1;
var all_content = $('#all-content');

(function (window, undefined) {

    window.app = {};
    var port_id = $('#port_id').val();

    app.BrainSocket = new BrainSocket(
        new WebSocket('ws://' + window.location.hostname + ':' + port_id),
        new BrainSocketPubSub()
    );


    app.BrainSocket.Event.listen('live_score.event', function (msg) {
        if (msg !== undefined) {

            var match_id = msg.client.data[0];
            $("#score_" + match_id).load("/tournament/public-live-scores/" + match_id, function (response) {
            });
        }
    });

    app.BrainSocket.Event.listen('messagebox.event', function (msg) {
        if (msg !== undefined) {
            var status = msg.client.data[0];
            var tournament_id = msg.client.data[1];
            if (status) {
                $("#message_box").load('/index/critical-message/' + tournament_id);
                $("#message_box").slideDown();
            }
            else
                $("#message_box").slideUp();
        }
    });

    var History = window.History;
    if (!History.enabled) {
        return false;
    }


    History.Adapter.bind(window, 'statechange', function () {
        var State = History.getState();
    });


    if (!isSafari) {
        window.addEventListener('popstate', function (event) {
            loadAjax(event.target.location.href, History.getState().title);
        });
    }

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    var trident = ua.indexOf('Trident/');

    if (!isMSIE) {
        $(document).on("click", "a.call", function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            var title = $(this).data('title');
            loadAjax(link, title);
            return false;
        });
    }

})(window);

function loadAjax(curl, title) {
    $.ajax({
        cache: true,
        type: "GET",
        url: curl,
        beforeSend: function (xhr) {
            all_content.fadeOut(400);
        },
        success: function (html) {
            History.pushState(null, title, curl);
            if (all_content.is(":visible")) {
                all_content.fadeOut(400, function () {
                    all_content.html(html).fadeIn(400);
                });
            }
            else {
                all_content.html(html).fadeIn(400);
            }
        }
    });
}


$(document).ready(function () {


    var tournamentRegistrationDate = new Date($("#registration_date").val());
    if (tournamentRegistrationDate > new Date().getTime()) {
        var days, hours, minutes, seconds;
        setInterval(function () {
            var current_date = new Date().getTime();
            var seconds_left = (tournamentRegistrationDate - current_date) / 1000;

            // do some time calculations
            days = parseInt(seconds_left / 86400);
            seconds_left = seconds_left % 86400;

            hours = parseInt(seconds_left / 3600);
            seconds_left = seconds_left % 3600;

            minutes = parseInt(seconds_left / 60);
            seconds = parseInt(seconds_left % 60);

            $("#c_days").text(days);
            $("#c_hours").text(hours);
            $("#c_minutes").text(minutes);
            $("#c_seconds").text(seconds);

        }, 1000);
    }
    else {
        $(".countdown").text("Registration closed");
    }


    app.BrainSocket.Event.listen('live_score_app_mode.changed', function (msg) {
        if (msg !== undefined) {
            if ($(".live-score-public").length > 0) {
                var match_id = msg.client.data.match_id;
                var setscore = msg.client.data.setpoints;
                var tiebreak = msg.client.data.tiebreak;
                var gamescore = msg.client.data.gamescore;
                var set_in_progress = msg.client.data.set_in_progress;
                var tie_break = msg.client.data.tie_break;
                var completed = msg.client.data.completed;

                var showscore = ' ';
                if (completed)
                    showscore = "Completed ";

                for (i = 0; i < set_in_progress; i++) {
                    //showscore += setscore.team1[i] != 0 || i==0 ? setscore.team1[i] : '';
                    showscore += setscore.team1[i];
                    showscore += tiebreak.team1[i] != 0 && tiebreak.team1[i] != null ? '(' + tiebreak.team1[i] + ')' : '';
                    showscore += '-';
                    //showscore += setscore.team2[i] != 0 || i==0 ? setscore.team2[i] : '';
                    showscore += setscore.team2[i];
                    showscore += tiebreak.team2[i] != 0 && tiebreak.team2[i] != null ? '(' + tiebreak.team2[i] + ')' : '';
                    showscore += ' ';
                }

                if (!tie_break) {
                    showscore +=
                        (gamescore.currentScoreTeam11 != 'u' ? gamescore.currentScoreTeam11 : "0")
                    + (gamescore.currentScoreTeam12  != 'n' ? gamescore.currentScoreTeam12 : "0")
                    + ":"
                    + (gamescore.currentScoreTeam21  != 'u' ? gamescore.currentScoreTeam21 : "0")
                    + (gamescore.currentScoreTeam22  != 'n' ? gamescore.currentScoreTeam22 : "0");
                }

                $("#score_" + match_id).text(showscore);
            }

            //var match_id = msg.client.data[0];
            //$("#score_" + match_id).load("/scores/player-scores/" + match_id, function (response) {
            //    loadCarousel();
            //});
        }
    });


    Dropzone.options.redirectAfterUpload = {
        paramName: "image", // The name that will be used to transfer the file
        maxFilesize: 5, // MB
        parallelUploads: 1,
        uploadMultiple: true,
        autoRedirect: false,
        accept: function (file, done) {
            // console.log(file, done);
            // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
            done();
            // }
        },
        complete: function (file) {
            if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                window.location.href = $(location).attr('href');
            }
        }
    };

    jQuery('.carousel').carousel({
        interval: 5000,
        pause: 'hover'
    });

    $(".fancybox").fancybox();


    // tinymce.init({
    //     selector: "textarea.tinymce",
    //     theme: "modern",
    //     plugins: [
    //         "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
    //         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    //         "save table contextmenu directionality emoticons textcolor paste textcolor filemanager "
    //     ],
    //     external_plugins: {
    //         //"moxiemanager": "/moxiemanager-php/plugin.js"
    //     },
    //     content_css: "/static//style.css",
    //     add_unload_trigger: false,
    //     autosave_ask_before_unload: true,

    //     // toolbar1: "saveform",
    //     toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontsizeselect",
    //     toolbar3: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime | forecolor backcolor",
    //     toolbar4: "table | hr removeformat | subscript superscript | charmap emoticons | print | ltr rtl | visualchars visualblocks template restoredraft",
    //     menubar: false,
    //     style_formats: [
    //         {title: 'Bold text', inline: 'b'},
    //         {title: 'Red text', inline: 'span', styles: {color: '#ff0000  '}},
    //         {title: 'Red header', block: 'h1', styles: {color: '#ff0000  '}},
    //         {title: 'Example 1', inline: 'span', classes: 'example1'},
    //         {title: 'Example 2', inline: 'span', classes: 'example2'},
    //         {title: 'Table styles'},
    //         {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    //     ]

    // });


});

(function ($, undefined) {
    $.expr[":"].containsNoCase = function (el, i, m) {
        var search = m[3];
        if (!search) return false;
        return new RegExp(search, "i").test($(el).text());
    };

    $.fn.searchFilter = function (options) {
        var opt = $.extend({
            // target selector
            targetSelector: "",
            // number of characters before search is applied
            charCount: 1
        }, options);

        return this.each(function () {
            var $el = $(this);
            $el.keyup(function () {
                var search = $(this).val();

                var $target = $(opt.targetSelector);
                $target.show();

                if (search && search.length >= opt.charCount)
                    $target.not(":containsNoCase(" + search + ")").hide();
            });
        });
    };
})(jQuery);