(function () {

    var usersModule = angular.module('usersApp', ['Application']);
    usersModule.controller('userController', function ($scope, $http, $window) {
        $scope.removeUser = function ($event) {
            var deleteUser = $window.confirm('Delete user?'); 
            if (deleteUser) {
                $http({
                    method: 'DELETE',
                    url: $event.target.href,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (data) {
                        if (!data.success) {
                            console.log(data);
                        } else {
                            $window.location.href = $event.target.baseURI;
                        }
                    });
            }
            $event.preventDefault();
            return false;
        };
    });

})();