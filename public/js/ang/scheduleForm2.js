(function () {

    var scheduleModule = angular.module('scheduleApp', ['Application', 'ui.bootstrap']);

    scheduleModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        };
    });

    scheduleModule.directive('formattime', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'HH:mm');
                });

            }
        };
    });

    scheduleModule.controller('ConfigurationController', function ($scope, $http, $timeout, $window) {
        $scope.formData = {};

        $scope.confSubmit = function ($event, tournament_id) {
            $http({
                method: "POST",
                url: '/schedule/configuration/' + tournament_id,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $.Notify({
                        caption: "Success",
                        content: response.message,
                        style: {background: '#16499A', color: '#fff'}
                    });

                    $timeout(function() {
                        $window.location = response.redirect_to;
                    }, 400);
                }
            });
            $event.preventDefault();
            return false;
        };

    });


    scheduleModule.controller('ScheduleController', function ($scope, $http, $window, $attrs, $modal, dateFilter) {
        $scope.formData = {};
        $scope.formData.listType = '';
        $scope.ismeridian = true;
        $scope.opened = [];
        $scope.url = document.URL;

        var d = new Date();
        d.setHours(09);
        d.setMinutes(00)
        $scope.mytime = dateFilter(new Date(), "HH:mm");


        $scope.open = function (match_id, play_date, size) {
            $scope.match_id = match_id;
            $scope.play_date = play_date;
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {
                    match_id: function () {
                        return $scope.match_id;
                    },
                    current_url: function () {
                        return $scope.url;
                    },
                    winner_next_round: function () {
                        return $scope.winner_next_round;
                    },
                    data: function () {
                        return $scope.data;
                    },
                    play_date: function () {
                        return $scope.play_date;
                    }
                }
            });
        };

        $scope.openModal = function (matchid, play_date) {
            $http({
                method: "GET",
                url: "/draws/match/" + matchid
            }).success(function (response) {
                $scope.data = response;
                $scope.winner_next_round = response.data.winner_next_round;
                if (!$scope.winner_next_round)
                    $scope.open(matchid, play_date);
                else {
                    content = 'Next round winner already exist!';
                    if ($scope.winner_next_round == 'empty')
                        content = 'Score cannot be added for this match.!';

                    $.Notify({
                        content: content,
                        caption: "Warning",
                        style: {background: '#16499A', color: '#fff'}
                    });
                }

            });
            return false;
        }


        $scope.scheduleSubmit = function ($event, requestType, message, action, redirect, listType) {

            var valid = false;
            var invalid_counter = 0;

            angular.forEach($scope.formData, function (val, key) {
                if (val === Object(val)) {
                    if (!angular.isUndefined(val.match_id)) {
                        invalid_counter++;
                        if (angular.isUndefined(val.type_of_time)) {
                            $("#" + key).addClass('order-invalid');
                        } else {
                            $("#" + key).removeClass('order-invalid');
                            invalid_counter--;
                        }
                    }
                }
            });

            valid = (invalid_counter == 0) ? true : false;

            if (valid) {
                redirect = redirect || false;
                action = action || $event.target.action;
                $scope.formData.listType = listType;
                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                    } else {
                        evt = app.BrainSocket.message('generic.event', [00]);
                        if (redirect) {
                            loadAjax(response.redirect_to, '');
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        } else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
            }
            $event.preventDefault();
            return false;
        };


        $scope.objectSize = function (obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };

        $scope.closeAll = function () {

            $scope.opened.date_of_play = false;

        };

    });

    var ModalInstanceCtrl = function ($scope, $modalInstance, $http, match_id, data, winner_next_round, current_url, play_date, $window) {
        $scope.match_id = match_id;
        $scope.play_date = play_date;
        $scope.current_url = current_url;
        $scope.winner_next_round = winner_next_round;
        $scope.data = data;
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.formData = {};
        $scope.errors = [];

        $scope.formSubmit = function ($event) {
            $scope.errors = [];
            $scope.formData['final'] = true;

            if (($scope.formData.team1.score1 < 6 && $scope.formData.team2.score1 < 6) ||
                ($scope.formData.team1.score1 == undefined || $scope.formData.team2.score1 == undefined)) {
                $scope.errors.push('Set 1 score is invalid');
            }
            if (($scope.formData.team1.score2 != undefined && $scope.formData.team2.score2 != undefined) &&
                ($scope.formData.team1.score2 != '' && $scope.formData.team2.score2 != '')) {
                if (($scope.formData.team1.score2 < 6 && $scope.formData.team2.score2 < 6)) {
                    $scope.errors.push('Set 2 score is invalid');
                }
            }

            if ( ($scope.formData.team1.score3 != undefined && $scope.formData.team2.score3 != undefined) &&
                ($scope.formData.team1.score3 != '' && $scope.formData.team2.score3 != '') ) {

                if (($scope.formData.team1.score3 < 6 && $scope.formData.team2.score3 < 6)) {
                    $scope.errors.push('Set 3 score is invalid');
                }
            }
            if ($scope.errors.length == 0) {
                $scope.saveMatch($event.target.action);
            }
            $event.preventDefault();
            return false;
        }

        $scope.saveMatch = function(action) {
            if($scope.errors.length == 0) {
                $http({
                    method: "POST",
                    url: action,
                    data: $.param($scope.formData),
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (response) {
                    if (response.success) {
                        evt = app.BrainSocket.message('generic.event',[response.schedule_id]);
                        evt = app.BrainSocket.message('live_score.event',[response.matchid]);
                        $window.location = $scope.current_url.split('&')[0] + "&scrollTo=" + response.matchid;
                    }
                });
            }
        };

        $scope.partialMatch = function(action) {
            $scope.saveMatch(action);
        };
    };
})();