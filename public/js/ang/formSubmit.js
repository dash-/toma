(function () {

    var formModule = angular.module('formSubmit', ['Application']);
    formModule.controller('submitController', function ($scope, $http, $window) {

        // $scope.formData = {};
        // console.log();
        $scope.sendData = function ($event, action) {
            // console.log($event);
            
            action = action || $event.target.action;

            $http({
                method: 'PUT',
                url: action,
                data: $.param($scope.formData),  // pass in data as strings
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            }).success(function (data) {
                if (!data.success) {
                     angular.forEach(data, function(d, key) {
                        console.log(d + '   ' +key);
                        $scope['error_' + key]  = d[0];
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $window.location.href = data.redirect_to;
                }
            });
            $event.preventDefault();
            return false;
        };

    });

})();