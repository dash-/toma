(function () {
    
    var formUploadModule = angular.module('formUpload', ['Application', 'ui.bootstrap']);

    formUploadModule.directive('formatdate', function(dateFilter) {
      return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });
                
            }
        }
    });

    formUploadModule.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                
                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);


	formUploadModule.controller('FormController', function ($scope, $http, $window) {

        $scope.formData = {};
        $scope.showWeeks = false;
        $scope.errors = [];

        $scope.formSubmit = function($event, requestType, message) {
            var fd = new FormData();
            fd.append('image', $scope.formData.image);
            $http({
                method: requestType,
                url: $event.target.action,
                // data: $.param($scope.formData),
                data: fd,
                // transformRequest: angular.identity,
                // headers: {'Content-Type': 'multipart/form-data'}
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        // console.log(d + '   ' +key);
                        // $scope.errors = d[0];
                        // $scope['errors'] = d[0];
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $.Notify({
						caption: "Success",
						content: message,
						style: {background: '#16499A', color: '#fff'}
					});
                }
            });
            $event.preventDefault();
            return false;

        }
        $scope.opened = [];
        $scope.openDatePicker = function($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[id] = true;
        };

    });
})();