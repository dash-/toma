(function () {

    var bracketModule = angular.module('bracket', ['Application', 'ui.bootstrap']);

    bracketModule.config(['$locationProvider', function ($locationProvider) {
        $locationProvider.html5Mode(true);
    }]);

    bracketModule.controller('bracketController', function ($scope, $http, $window, $modal) {
        $scope.url = document.URL;

        $scope.open = function (match_id, size) {
            $scope.match_id = match_id;

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {
                    match_id: function () {
                        return $scope.match_id;
                    },
                    schedule_id: function () {
                        return $scope.schedule_id;
                    },
                    current_url: function () {
                        return $scope.url;
                    },
                    winner_next_round: function () {
                        return $scope.winner_next_round;
                    },
                    data: function () {
                        return $scope.data;
                    }
                }
            });
        };

        $scope.openSignSheet = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'refereeSubmit.html',
                controller: RefereeModalCtrl,
                size: size,
                resolve: {
                    current_url: function () {
                        return $scope.url;
                    }
                }
            });
        }

        $scope.openChangeRequest = function (matchid, size) {
            var modalInstance = $modal.open({
                templateUrl: 'changeRequest.html',
                controller: ChangeRequestModalCtrl,
                size: size,
                resolve: {
                    current_url: function () {
                        return $scope.url;
                    },
                    matchid: function () {
                        return matchid;
                    },
                    data: function () {
                        return $scope.data;
                    }
                }
            });
        }

        $scope.openModal = function (matchid) {
            $http({
                method: "GET",
                url: "/draws/match/" + matchid,
            }).success(function (response) {
                if (response.qualifications_finished) {
                    $.Notify({
                        content: response.message,
                        caption: "Warning",
                        style: {background: '#16499A', color: '#fff'}
                    });
                //} else if (response.data.winner_next_round) {
                //    $scope.data = response;
                //    // console.log(response);
                //    $scope.openChangeRequest(matchid);
                } else {
                    $scope.data = response;
                    $scope.winner_next_round = response.data.winner_next_round;
                    $scope.schedule_id = response.data.schedule_id;
                    $scope.open(matchid);
                }


                // removed if else because client requested it, but this we need to check this
                // if (!$scope.winner_next_round)
                // $scope.open(matchid);
                // else {
                //     content = 'Next round winner already exist!';
                //     if($scope.winner_next_round=='empty')
                //     content = 'Score cannot be added for this match.!';

                //     $.Notify({
                //         content: content,
                //         caption: "Warning",
                //         style: {background: '#16499A', color: '#fff'}
                //     });
                // }

            });
            return false;
        }

    });

    var ChangeRequestModalCtrl = function ($scope, $modalInstance, $http, $window, $location, current_url, matchid, data) {

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.matchid = matchid;
        $scope.data = data.data;

        $scope.formData = {};

        $scope.submitForm = function ($event, submitAction) {
            $scope.errors = {
                msg: ''
            };
            $scope.formData['list_type'] = $location.search().list_type;

            $http({
                method: "POST",
                url: submitAction + '/' + $scope.matchid,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success) {
                    $modalInstance.dismiss('cancel');
                    $.Notify({
                        content: response.message,
                        caption: "Success",
                        style: {background: '#16499A', color: '#fff'}
                    });
                } else {
                    $scope.errors.msg = response.msg;
                }
            });
            $event.preventDefault();
            return false;
        };
    };

    var RefereeModalCtrl = function ($scope, $modalInstance, $http, $window, current_url) {
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.formData = {};

        $scope.signForm = function ($event, submitAction) {
            $scope.errors = {
                msg: ''
            };
            $http({
                method: "POST",
                url: $event.target.action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success) {
                    $window.location = submitAction;
                } else {
                    $scope.errors.msg = response.msg;
                }
            });
            $event.preventDefault();
            return false;
        };
    };

    var ModalInstanceCtrl = function ($scope, $modalInstance, $http, match_id, data, winner_next_round, schedule_id, current_url, $window) {
        $scope.match_id = match_id;
        $scope.schedule_id = schedule_id;
        $scope.current_url = current_url;
        $scope.winner_next_round = winner_next_round;
        $scope.data = data;
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.formData = {};
        $scope.errors = [];


        $scope.formSubmit = function ($event) {
            $scope.errors = angular.copy([]);
            $scope.formData['final'] = true;

            if (($scope.formData.team1.score1 < 6 && $scope.formData.team2.score1 < 6) ||
                ($scope.formData.team1.score1 == undefined || $scope.formData.team2.score1 == undefined)) {
                $scope.errors.push('Set 1 score is invalid');
            }
            if (($scope.formData.team1.score2 != undefined && $scope.formData.team2.score2 != undefined) &&
                ($scope.formData.team1.score2 != '' && $scope.formData.team2.score2 != '')) {
                if (($scope.formData.team1.score2 < 6 && $scope.formData.team2.score2 < 6)) {
                    $scope.errors.push('Set 2 score is invalid');
                }
            }

            if (($scope.formData.team1.score3 != undefined && $scope.formData.team2.score3 != undefined) &&
                ($scope.formData.team1.score3 != '' && $scope.formData.team2.score3 != '')) {

                if (($scope.formData.team1.score3 < 6 && $scope.formData.team2.score3 < 6)) {
                    $scope.errors.push('Set 3 score is invalid');
                }
            }

            if ($scope.errors.length == 0) {
                $scope.saveMatch($event.target.action);
            }
            $event.preventDefault();
            return false;
        };

        $scope.saveMatch = function (action) {
            if ($scope.errors.length == 0) {
                $http({
                    method: "POST",
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response.success) {
                        evt = app.BrainSocket.message('generic.event', [response.schedule_id]);
                        evt = app.BrainSocket.message('live_score.event', [response.matchid]);
                        var sc = '&scrollTo=';
                        if(!($scope.current_url.split('&')[0].indexOf("?") > -1))
                            sc = '?scrollTo=';
                        $window.location = $scope.current_url.split('&')[0] + sc + response.matchid;
                    }
                    else
                    {
                        $scope.error_message = response.message;
                    }
                });
            }
        };

        $scope.partialMatch = function (action) {
            $scope.saveMatch(action);
        };

        $scope.addMatchStatus = function ($event, action, whichTeam, status) {

            $scope.sendData = {
                'team': whichTeam,
                'status': status
            };

            $http({
                method: "POST",
                url: action,
                data: $.param($scope.sendData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                // console.log(response);
                if (response.success) {
                    $scope.partialMatch('/draws/add-results');
                    //var sc = '&scrollTo=';
                    //if(!($scope.current_url.split('&')[0].indexOf("?") > -1))
                    //    sc = '?scrollTo=';
                    //$window.location = $scope.current_url.split('&')[0] + sc + response.matchid;
                }
            });

        };
    };


})();