(function () {

    var loginModule = angular.module('login', ['ngAnimate', 'Application']);
    loginModule.controller('loginController', function ($scope, $http, $window) {

        $scope.user = {};
        $scope.showError = false;

        $scope.login = function (form) {
            $scope.showError = false;
            $http({
                method: 'POST',
                url: '/login',
                data: $.param($scope.user),  // pass in data as strings
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            })
                .success(function (data) {
                    if (!data.success) {
                        // if not successful, bind errors to error variables
                        $scope.errorName = 'You entered wrong email or password.';
                        $scope.showError = true;
                    } else {
                        // if successful, bind success message to message
                        $window.location.href = data.redirect_to;
                    }
                });

        };

    });

})();