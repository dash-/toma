(function () {


    var multiStepFormModule = angular.module('multiStepForm', ['Application', 'ui.bootstrap', 'ngAnimate', 'FormErrors'])
        .config(function (datepickerConfig) {
            datepickerConfig.showWeeks = false;
            datepickerConfig.startingDay = 1;
        });

    multiStepFormModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        };
    });


    multiStepFormModule.controller('FormController', function ($scope, $http, $window, $attrs) {
        $scope.formData = {};
        $scope.formData.draws = {};
        $scope.formData.has_qualifying = 1;
        $scope.showWeeks = false;
        $scope.errors = [];
        $scope.showMe = false;
        $scope.drawsData = [];
        $scope.opened = [];
        $scope.minDate2 = "";
        $scope.clicked = false;
        $scope.drawclicked = false;
        $scope.no_draw = false;
        $scope.hide_drawadd = true;

        $scope.getOrganizers = function (val) {
            return $http.get('/tournaments/organizers-data', {
                params: {
                    query: val
                }
            }).then(function (response) {
                var gamers = [];
                angular.forEach(response.data, function (item) {
                    gamers.push({
                        'full_name': item.full_name,
                        'name': item.name,
                        'email': item.email,
                        'phone': item.phone,
                        'fax': item.fax,
                        'website': item.website
                    });
                });
                return gamers;
            });
        };

        $scope.populateInfo = function (info) {
            $scope.formData.organizer = {
                'name': info.name,
                'email': info.email,
                'phone': info.phone,
                'fax': info.fax,
                'website': info.website
            };
        }

        $scope.init = function () {
            $http({
                url: '/tournaments/draw-categories',
                method: "GET"
            }).success(function (response) {
                $scope.drawCategories = response.categories;
            });

            return false;
        }; // init

        $scope.toggleMin = function () {
            $scope.minDate =  null;//$scope.minDate ? null : new Date();

        };
        $scope.toggleMin();


        $scope.addDraw = function () {


            if ($scope.drawsData.length == 0) {
                $scope.drawclicked = true;
                //$(".custom-errors").show();
            } else {
                $scope.drawclicked = false;
                //$(".custom-errors").hide();
            }

            var isValidCategory = $scope.createForm.draw_category_id.$invalid;
            var isValidDrawType = $scope.createForm.draw_type.$invalid;
            var isValidDrawGender = $scope.createForm.draw_gender.$invalid;
            var isValidTotal = $scope.createForm.total.$invalid;
            var isValidDrawSize = $scope.createForm.draw_size.$invalid;
            var isValidPrizePool = $scope.createForm.prize_pool.$invalid;

            if ($scope.formData.sizes != undefined) {
                var Sum1 = Number($scope.formData.sizes.accepted_qualifiers)
                    + Number($scope.formData.sizes.direct_acceptances)
                    + Number($scope.formData.sizes.wild_cards)
                    + Number($scope.formData.sizes.special_exempts);

                var Total = Number($scope.formData.sizes.total);
            }

            //if ($scope.drawsData.length != 0)
            if(!isNaN(Sum1) && !isNaN(Total))
                $scope.Sum1Error = Sum1 !== Total;

            function isScopeTrue(element, index, array) {
                return (element == false);
            }

            var ArrayScope = [isValidCategory, isValidDrawType, isValidDrawGender,
                isValidTotal, isValidDrawSize, isValidPrizePool, $scope.Sum1Error].every(isScopeTrue);

            if (ArrayScope == true) {
                $scope.drawclicked = false;
                $scope.drawAdded = true;
                var new_obj = {

                    'draws': $scope.formData.draws,
                    'qualifications': $scope.formData.qualifications,
                    'sizes': $scope.formData.sizes,
                    'is_prequalifying': $scope.formData.is_prequalifying
                };




                $scope.drawsData.push(angular.copy(new_obj));

                $scope.formData.draws = angular.copy({});
                $scope.formData.qualifications = angular.copy({});
                $scope.formData.sizes = angular.copy({});

                if(!$scope.has_qualifying)
                    $scope.formData.qualifications.draw_size = $scope.formData.sizes.accepted_qualifiers = 0;

                $('select').select2('val', '', false);

                if ($scope.drawsData.length == 0) {
                    $scope.drawAdded = false
                }

            }

        }

        $scope.$watch("formData.is_prequalifying", function () {
            if($scope.formData.is_prequalifying){
                if($scope.formData.sizes == undefined)
                    $scope.formData.sizes = {};
                $scope.formData.sizes.accepted_qualifiers = 0;
                if($scope.formData.qualifications == undefined)
                    $scope.formData.qualifications = {};
                $scope.formData.qualifications.draw_size = 0;
            }
        });
        $scope.$watch("formData.sizes.total", function () {
            current_data = $scope.formData.sizes;
            if (current_data !== undefined) {

                switch (Number($scope.formData.sizes.total)) {
                    case 8:
                    $scope.formData.sizes.direct_acceptances = 4;
                    $scope.formData.sizes.accepted_qualifiers = 4;
                    $scope.formData.sizes.wild_cards = 0;
                    $scope.formData.sizes.special_exempts = 0;
                    $scope.formData.draws.number_of_seeds = 4;
                    break;

                    case 16:
                        $scope.formData.sizes.direct_acceptances = 12;
                        $scope.formData.sizes.accepted_qualifiers = 4;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 24:
                        $scope.formData.sizes.direct_acceptances = 18;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 32:
                        $scope.formData.sizes.direct_acceptances = 26;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 48:
                        $scope.formData.sizes.direct_acceptances = 42;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 64:
                        $scope.formData.sizes.direct_acceptances = 58;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 16;
                        break;
                    case 96:
                        $scope.formData.sizes.direct_acceptances = 84;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                    case 128:
                        $scope.formData.sizes.direct_acceptances = 116;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                }


                if ($scope.formData.tournament.has_qualifying) {
                    $scope.formData.sizes.direct_acceptances = $scope.formData.sizes.direct_acceptances + $scope.formData.sizes.accepted_qualifiers;
                    $scope.formData.sizes.accepted_qualifiers = 0;
                }
                if($scope.formData.is_prequalifying == 1)
                {
                    $scope.formData.qualifications.draw_size = 0;
                    $scope.formData.sizes.accepted_qualifiers = 0;

                }

            }
            ;

        });

        $scope.removeDraw = function (array, index) {
            array.splice(index, 1);
            if ($scope.drawsData.length == 0) {
                $scope.drawAdded = false
            }
            ;
        }

        $scope.makeCopy = function (draw, index) {
            $scope.formData.draws = angular.copy(draw.draws);
            $scope.formData.qualifications = angular.copy(draw.qualifications);
            $scope.formData.sizes = angular.copy(draw.sizes);

            $('#draw_category_id').select2('val', draw.draws.draw_category_id, false);
            $('#draw_type').select2('val', draw.draws.draw_type, false);
            $('#draw_gender').select2('val', draw.draws.draw_gender, false);
            $('#total_acceptance').select2('val', draw.sizes.total, false);
            $('#onsite_direct').select2('val', draw.sizes.onsite_direct, false);
            $('#match_rule').select2('val', draw.draws.match_rule, false);
            $('#set_rule').select2('val', draw.draws.set_rule, false);
            $('#game_rule').select2('val', draw.draws.game_rule, false);
        }

        $scope.withoutDraw = function () {
            $scope.no_draw = true;
        }

        $scope.formSubmit = function ($event, requestType, message, action, redirect) {

            redirect = redirect || false;

            action = action || $event.target.action;

            if ($scope.no_draw != true) {
                if ($scope.formData.draws != undefined)
                    $scope.addDraw();
            }

            $scope.drawAdded = $scope.drawsData.length == 0 ? false : true;
            if ($scope.no_draw)
                $scope.drawAdded = true;


            var referee_id_real = $('#referee_id_real').val();
            if (referee_id_real == '')
                referee_id_real = 0;

            $scope.formData.tournament.referee_id_real = referee_id_real;

            $scope.dataToSend = {
                'tournament': $scope.formData.tournament,
                'date': $scope.formData.date,
                'organizer': $scope.formData.organizer,
                'drawsData': $scope.drawsData,
                'sponsor': $scope.formData.sponsor,
                'is_prequalifying': $scope.formData.is_prequalifying
            };

            if ($scope.drawAdded == true) {
                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.dataToSend),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                    } else {

                        evt = app.BrainSocket.message('notification.event', [1]);

                        if (redirect) {
                            loadAjax(response.redirect_to, '');
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }
                        else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
                $event.preventDefault();
                return false;
            }
        };

        $scope.closeAll = function () {
            $scope.opened.mainFrom = false;
            $scope.opened.mainTo = false;
            $scope.opened.qualifyingFrom = false;
            $scope.opened.qualifyingTo = false;
            $scope.opened.entryDeadline = false;
            $scope.opened.withdrawalDeadline = false;
        };

        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
            // console.log($scope.formData.date.qualifying_date_to);
            current_data = $scope.formData.date;
            if (current_data !== undefined) {
                var moreDay = new Date();
                var act = new Date(current_data.qualifying_date_from);
                if (id == 'qualifyingTo') {
                    if (!current_data.hasOwnProperty("qualifying_date_to"))
                        current_data.qualifying_date_to = act.format("yyyy-mm-dd");
                } else if (id == 'mainFrom') {
                    if (!current_data.hasOwnProperty("main_draw_from")) {
                        act = new Date(current_data.qualifying_date_to);
                        current_data.main_draw_from = act.format("yyyy-mm-dd");
                    }
                } else if (id == 'mainTo') {
                    if (!current_data.hasOwnProperty("main_draw_to")) {
                        act = new Date(current_data.main_draw_from);
                        current_data.main_draw_to = act.format("yyyy-mm-dd");
                    }
                } else if (id == 'entryDeadline') {
                    if (!current_data.hasOwnProperty("entry_deadline")) {
                        act = new Date(current_data.main_draw_from);
                        current_data.entry_deadline = act.format("yyyy-mm-dd");
                    }
                } else if (id == 'withdrawalDeadline') {
                    if (!current_data.hasOwnProperty("withdrawal_deadline")) {
                        act = new Date(current_data.main_draw_from);
                        current_data.withdrawal_deadline = act.format("yyyy-mm-dd");
                    }
                }
            }
        };

        $scope.replaceText = function (str, mapObj) {
            if (!str)
                return false;

            var re = new RegExp(Object.keys(mapObj).join("|"), "gi");

            return str.replace(re, function (matched) {
                return mapObj[matched.toLowerCase()];
            });
        }

        $scope.showDrawAdd = function () {
            $scope.hide_drawadd = false;
        }

        $scope.loadStep = function () {

            //$(".custom-errors").show();

            $scope.clicked = true;
            $scope.DateDraw = false;
            $scope.DateQualifying = false;
            $scope.DateEntry = false;
            //TODO: Create validtion for main and qualy dates
            $scope.DateFirstError = false;

            var isValidTitle = $scope.createForm.title.$invalid;
            var isValidSurface = $scope.createForm.surface.$invalid;
            var isValidClub = $scope.createForm.club.$invalid;
            //var isValidMainDrawFrom = $scope.createForm.main_draw_from.$invalid;
            //var isValidMainDrawTo = $scope.createForm.main_draw_to.$invalid;
            //var isValidQualifyingDateFrom = $scope.createForm.qualifying_date_from.$invalid;
            //var isValidQualifyingDateTo = $scope.createForm.qualifying_date_to.$invalid;
            //var isValidEntryDeadline = $scope.createForm.entry_deadline.$invalid;
            //var isValidWithdrawalDeadline = $scope.createForm.withdraw24193rve;fal_deadline.$invalid;
            var isValidName = $scope.createForm.name.$invalid;
            var isValidEmail = $scope.createForm.email.$invalid;


            current_data = $scope.formData.date;
            //if (current_data !== undefined) {
            //
            //    if ($scope.formData.date.qualifying_date_from > $scope.formData.date.qualifying_date_to) {
            //        $scope.DateQualifying = true;
            //    }
            //    ;
            //    if ($scope.formData.date.main_draw_from > $scope.formData.date.main_draw_to) {
            //        $scope.DateDraw = true;
            //    }
            //    ;
            //    //if ($scope.formData.date.entry_deadline > $scope.formData.date.withdrawal_deadline) {
            //    //    $scope.DateEntry = true;
            //    //};
            //    if ($scope.formData.date.qualifying_date_to > $scope.formData.date.main_draw_from) {
            //        $scope.DateFirstError = true;
            //    }
            //    ;
            //}
            //;


            function isScopeTrue(element, index, array) {
                return (element == false);
            }

            //TODO: In case we need date validation
            //var ArrayScope = [isValidTitle, isValidSurface, isValidClub, isValidMainDrawFrom, isValidMainDrawTo, isValidQualifyingDateFrom, isValidQualifyingDateTo, isValidWithdrawalDeadline, isValidName, isValidEmail,
            var ArrayScope = [isValidTitle, isValidSurface, isValidClub, isValidName, isValidEmail].every(isScopeTrue);


            if ($scope.showMe == false && ArrayScope == true) {
                $scope.showMe = true;
                if ($scope.formData.tournament.has_qualifying == 0) {
                    $scope.formData.sizes = {};
                    $scope.formData.qualifications = {};
                    $scope.formData.sizes.accepted_qualifiers = 0;
                    $scope.formData.qualifications.draw_size = 0;
                }
            } else {
                $scope.showMe = false;

            }
            ;
        }

        $scope.$watch("formData.date.qualifying_date_to", function () {
            //current_data = $scope.formData.date;
            console.log(current_data);
            if (current_data !== undefined) {
                var moreDay = new Date();
                console.log(moreDay);
                var act = new Date(current_data.qualifying_date_to);
                console.log(act);
                moreDay.setDate(act.getDate());
                console.log(moreDay);
                $scope.minDate2 = act.format("yyyy-mm-dd");
                console.log($scope.minDate2);
            }
            else{
                $scope.minDate2 = null;
            }
        });


        $scope.removeCustomErrors = function () {

        };


    });
})();