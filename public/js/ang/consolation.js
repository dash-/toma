(function () {

    var consolationModule = angular.module('consolationApp', ['Application', 'ui.bootstrap', 'checklist-model']);

    consolationModule.controller('ConsolationController', ['$scope', '$http', '$window', function ($scope, $http) {
        $scope.formData = {};
        $scope.errors = [];

        $scope.getCheckedSignups = function(draw_id, consolation_type) {
            $http({
                method: 'GET',
                url: '/draws/checked-signups/' + draw_id + '/' + consolation_type
            }).success(function (response) {
                $scope.players = response.players;
                $scope.formData.signups = response.signups;
            });
        }

        $scope.createConsolation = function ($event, message, action, consolation_type) {
            message = message || false;
            action = action || false;
            $scope.formData.consolation_type = consolation_type;

            if ($scope.formData.signups != undefined) {
                $http({
                    method: 'POST',
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response.success) {
                        loadAjax(response.redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: response.message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    } else {
                        console.log(response);
                    }
                });
            } else {
                $scope.errors['msg'] = "Choose at least one player";
            }
        };

        $scope.checkAll = function() {
            angular.forEach($scope.players, function(val, key) {
                $scope.formData.signups.push(val.id);
            });
        };
    }]);


})();