(function () {

    var calendarModule = angular.module('calendarApp', ['ui.calendar', 'Application', 'ui.bootstrap']);

    calendarModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });
            }
        }
    });

    calendarModule.controller('CalendarController',['$scope', '$http', '$window', function ($scope, $http, $window) {

        $scope.changeView = function (view, calendar) {
            calendar.fullCalendar('changeView', view);
        };

        $scope.next = function () {
            $('#calendar').fullCalendar('next');
        };

        $scope.prev = function () {
            $('#calendar').fullCalendar('prev');
        };

        $scope.renderCalender = function (calendar) {
            $('#calendar').fullCalendar('render');
        };

        /* config object */
        $scope.uiConfig = {
            calendar: {
                height: "550",
                editable: false,
                firstDay: 1,
                header: {
                    // left: 'month basicWeek basicDay agendaWeek agendaDay',
                    center: '',
                    right: 'prev, today ,next'
                },
                eventClick: function (event, jsEvent, view) {
                    $scope.$apply();
                    $window.location = event.url_to;
                }
            }
        };

        $scope.events = {
            url: "/calendar/data"
        };

        $scope.eventSources = [$scope.events];

        $scope.search = function($event) {
            var values = angular.extend({},
                {
                    'club_id': $scope.tournament.club_id,
                    'club_city': $scope.tournament.club_city,
                    'category': $scope.tournament.category
                }
            );
            $http({
                method: 'GET',
                url: '/calendar/search',
                params: values
            }).success(function (response) {
                $scope.scSearch = 1;
                $scope.eventSources = response;

                console.log($scope.eventSources);

                $('#calendar').fullCalendar( 'removeEvents');
                $('#calendar').fullCalendar( 'addEventSource', $scope.eventSources);
                $('#calendar').fullCalendar('render');
            }).error(function (error) {
                console.log(error);
            });
        }

    }]);

    calendarModule.controller('ListController', ['$scope', '$http', '$window', function ($scope, $http, $window) {

        $http({
            method: 'GET',
            url: '/calendar/data?list_view=1'
        }).success(function (response) {
            $scope.tournaments = response;
        }).error(function (error) {
            console.log(error);
        });

        $scope.search = function($event) {
            var values = angular.extend({},
                {
                    'club_id': $scope.tournament.club_id,
                    'club_city': $scope.tournament.club_city,
                    'category': $scope.tournament.category,
                    'list_view': true
                }
            );
            $http({
                method: 'GET',
                url: '/calendar/search',
                params: values
            }).success(function (response) {
                $scope.scSearch = 1;
                $scope.tournaments = response;
            }).error(function (error) {
                console.log(error);
            });
        }

    }]);


})();