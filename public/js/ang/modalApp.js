angular.module('tModalApp', ['Application', 'ui.bootstrap']);

var tModalController = function ($scope, $modal, $log, $http) {


    $scope.openModal = function (action, tournament_id) {
        $http({
          method: "GET",
          url: action + '/' + tournament_id
        }).success(function (response) {
          $scope.open(response);
        });
        return false;
    }

    $scope.open = function (tournament, size) {
        $scope.tournament = tournament;

        var modalInstance = $modal.open({
            templateUrl: 'qrContent.html',
            controller: ModalInstanceCtrl,
            size: size,
            resolve: {
                tournament: function () {
                    return $scope.tournament;
                }
            }
        });
    };
};

var ModalInstanceCtrl = function ($scope, $modalInstance, tournament) {
    $scope.tournament = tournament.data;
};