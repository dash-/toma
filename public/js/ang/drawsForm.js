(function () {

    var drawsFormModule = angular.module('drawsFormApp', ['Application', 'ui.bootstrap', 'FormErrors']);


    drawsFormModule.filter("asDate", function () {
        return function (input) {
            date = new Date(input);
            if (isNaN(date))
                date = new Date(input.replace(/-/g, "/"));

            return date;
        }
    });
    drawsFormModule.filter("asApprovalStatus", function () {
        return function (input) {
            var list = {
                '0': 'Draw not completed',
                '1': 'Waiting for validation',
                '2': 'Validated by regional admin',
                '3': 'Validated by superadmin'
            };
            return list[input];
        }
    });

    drawsFormModule.controller('FormController', function ($scope, $http, $window, $attrs) {
        $scope.formData = {};
        $scope.formData.qualifications = {};
        $scope.showWeeks = false;
        $scope.errors = [];
        $scope.isCollapsed = true;
        $scope.fetchUrl = $attrs.scurl;
        $scope.has_qualifying = $attrs.hasqualifying;

        $scope.$watch("formData.is_prequalifying", function () {
            console.log($scope.formData.is_prequalifying);
            if($scope.formData.is_prequalifying){
                //if($scope.formData.sizes == undefined)
                //    $scope.formData.sizes = {};
                $scope.formData.sizes.accepted_qualifiers = 0;
                //if($scope.formData.qualifications == undefined)
                //    $scope.formData.qualifications = {};
                $scope.formData.qualifications.draw_size = 0;
            }
        });

        $scope.$watch("formData.sizes.total", function () {
            current_data = $scope.formData.sizes;
            if (current_data !== undefined) {

                switch (Number($scope.formData.sizes.total)) {
                    case 8:
                        $scope.formData.sizes.direct_acceptances = 4;
                        $scope.formData.sizes.accepted_qualifiers = 4;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;

                    case 16:
                        $scope.formData.sizes.direct_acceptances = 12;
                        $scope.formData.sizes.accepted_qualifiers = 4;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 24:
                        $scope.formData.sizes.direct_acceptances = 18;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 32:
                        $scope.formData.sizes.direct_acceptances = 26;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 48:
                        $scope.formData.sizes.direct_acceptances = 42;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 64:
                        $scope.formData.sizes.direct_acceptances = 58;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 16;
                        break;
                    case 96:
                        $scope.formData.sizes.direct_acceptances = 84;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                    case 128:
                        $scope.formData.sizes.direct_acceptances = 116;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                }
                if ($scope.has_qualifying == 0) {
                    $scope.formData.sizes.direct_acceptances = $scope.formData.sizes.direct_acceptances + $scope.formData.sizes.accepted_qualifiers;
                    $scope.formData.sizes.accepted_qualifiers = 0;
                    $scope.formData.qualifications.draw_size = 0;
                }
                if($scope.formData.is_prequalifying == 1)
                {
                    $scope.formData.qualifications.draw_size = 0;
                    $scope.formData.sizes.accepted_qualifiers = 0;

                }
            }
            ;

        });


        isRequest = true;
        $scope.init = function () {

            if (isRequest) {
                isRequest = false;

                $http({
                    method: 'JSONP',
                    url: $scope.fetchUrl + '?callback=JSON_CALLBACK'
                }).success(function (response) {
                    $scope.draws = response;
                    isRequest = true;
                }).error(function (error) {
                    console.log(error);
                });

                return false;
            }
        }; // init


        $scope.formSubmit = function ($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            if ($scope.formData.sizes != undefined) {
                $scope.Sum1 = Number($scope.formData.sizes.accepted_qualifiers)
                + Number($scope.formData.sizes.direct_acceptances)
                + Number($scope.formData.sizes.wild_cards)
                + Number($scope.formData.sizes.special_exempts);
                $scope.Total = Number($scope.formData.sizes.total);
            }

            $scope.Sum1Error = false;

            if ($scope.Sum1 !== $scope.Total) {
                $scope.Sum1Error = true;
            }
            if ($scope.createForm.$valid && $scope.Sum1Error == false) {
                $scope.showErrors = false;
                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                    } else {
                        if (redirect) {
                            $window.location.href = response.redirect_to;
                        }
                        else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                            $scope.draws.push(response.data);
                            $scope.isCollapsed = false;
                        }

                    }
                });
                $event.preventDefault();
                return false;
            } else {
                $scope.showErrors = true;
                $('.form-errors').show();
            }

        }
        $scope.opened = [];
        //$scope.openDatePicker = function ($event, id) {
        //    $event.preventDefault();
        //    $event.stopPropagation();
        //    $scope.opened[id] = true;
        //};

        $scope.replaceText = function (str, mapObj) {
            var re = new RegExp(Object.keys(mapObj).join("|"), "gi");

            return str.replace(re, function (matched) {
                return mapObj[matched.toLowerCase()];
            });
        }

    });


    drawsFormModule.controller('EditFormController', function ($scope, $http, $window, $attrs) {
        $scope.formData = {};
        $scope.showWeeks = false;
        $scope.errors = [];
        $scope.isCollapsed = true;
        $scope.has_qualifying = $attrs.hasqualifying;
        $scope.first_load = true;
        $scope.$watch("formData.is_prequalifying", function () {
            console.log($scope.formData.is_prequalifying);
            if($scope.formData.is_prequalifying){
                if($scope.formData.sizes == undefined)
                    $scope.formData.sizes = {};
                $scope.formData.sizes.accepted_qualifiers = 0;
                if($scope.formData.qualifications == undefined)
                    $scope.formData.qualifications = {};
                $scope.formData.qualifications.draw_size = 0;
            }
        });
        $scope.$watch("formData.sizes.total", function () {
            current_data = $scope.formData.sizes;
            if (!$scope.first_load && current_data !== undefined) {

                switch (Number($scope.formData.sizes.total)) {
                    case 8:
                        $scope.formData.sizes.direct_acceptances = 4;
                        $scope.formData.sizes.accepted_qualifiers = 4;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;

                    case 16:
                        $scope.formData.sizes.direct_acceptances = 12;
                        $scope.formData.sizes.accepted_qualifiers = 4;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 24:
                        $scope.formData.sizes.direct_acceptances = 18;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 4;
                        break;
                    case 32:
                        $scope.formData.sizes.direct_acceptances = 26;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 48:
                        $scope.formData.sizes.direct_acceptances = 42;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 8;
                        break;
                    case 64:
                        $scope.formData.sizes.direct_acceptances = 58;
                        $scope.formData.sizes.accepted_qualifiers = 6;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 16;
                        break;
                    case 96:
                        $scope.formData.sizes.direct_acceptances = 84;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                    case 128:
                        $scope.formData.sizes.direct_acceptances = 116;
                        $scope.formData.sizes.accepted_qualifiers = 12;
                        $scope.formData.sizes.wild_cards = 0;
                        $scope.formData.sizes.special_exempts = 0;
                        $scope.formData.draws.number_of_seeds = 0;
                        break;
                }
                if ($scope.has_qualifying == 0) {
                    $scope.formData.sizes.direct_acceptances = $scope.formData.sizes.direct_acceptances + $scope.formData.sizes.accepted_qualifiers;
                    $scope.formData.sizes.accepted_qualifiers = 0;
                    $scope.formData.qualifications.draw_size = 0;
                }
                if($scope.formData.is_prequalifying == 1)
                {
                    $scope.formData.qualifications.draw_size = 0;
                    $scope.formData.sizes.accepted_qualifiers = 0;

                }
            }
            $scope.first_load = false;
        });

        $scope.formSubmit = function ($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            if ($scope.formData.sizes != undefined) {
                $scope.Sum1 = Number($scope.formData.sizes.accepted_qualifiers)
                + Number($scope.formData.sizes.direct_acceptances)
                + Number($scope.formData.sizes.wild_cards)
                + Number($scope.formData.sizes.special_exempts);
                $scope.Total = Number($scope.formData.sizes.total);
            }

            $scope.Sum1Error = false;

            if ($scope.Sum1 !== $scope.Total) {
                $scope.Sum1Error = true;
            }

            if ($scope.createForm.$valid && $scope.Sum1Error == false) {
                $scope.showErrors = false;

                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                    } else {
                        if (redirect) {
                            $window.location.href = response.redirect_to;
                        }
                        else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
                $event.preventDefault();
                return false;

            } else {
                $scope.showErrors = true;
                $('.form-errors').show();
            }
        }
        $scope.opened = [];
        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[id] = true;
        };

    });
})();