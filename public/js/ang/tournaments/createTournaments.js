(function () {



    var test = angular.module('testApp', ['Application','ui.bootstrap','ui.select2']);


    test.controller("TournamentsController", function ($scope, $http, $window) {

        var tagsData = [
            {id:1,tag:'Apple'},
            {id:2,tag:'Banana'},
            {id:3,tag:'Cherry'},
            {id:4,tag:'Cantelope'},
            {id:5,tag:'Grapefruit'},
            {id:6,tag:'Grapes',selected:true},
            {id:7,tag:'Lemon'},
            {id:8,tag:'Lime'},
            {id:9,tag:'Melon',selected:true},
            {id:10,tag:'Orange'},
            {id:11,tag:'Strawberry'},
            {id:11,tag:'Watermelon'}
        ];

        $scope.items = tagsData;

        $scope.tournament = {};

        $scope.create = function ($event) {
            $http({
                method: 'POST',
                url: '/tournaments/create',
                //url: $event.target.action,
                data: $.param($scope.tournament),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
                .success(function (data) {

                    if (!data.success) {
                        angular.forEach(data, function (d, key) {
                            console.log(d + '   ' + key);
                            $scope['error_' + key] = d[0];
                        });

                        // add red border
                        $scope.errorState = 'error-state';
                    } else {

                   

                        // if successful, bind success message to message
                        $window.location.href = data.redirect_to;
                    }
                });
        };
    })

})();