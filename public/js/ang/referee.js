(function () {

    var refereeModule = angular.module('refereeApp', ['Application', 'ui.bootstrap'])
        .config(function (datepickerConfig) {
            datepickerConfig.showWeeks = false;
        });

    refereeModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });
            }
        }
    });

    refereeModule.directive('validSubmit', ['$parse', function ($parse) {
        return {
            require: '^form',
            link: function (scope, element, iAttrs, controller) {
                var form = element.controller('form');
                form.$submitted = false;
                var fn = $parse(iAttrs.validSubmit);

                element.on('submit', function (event) {
                    scope.$apply(function () {
                        element.addClass('ng-submitted');
                        form.$submitted = true;
                        $(".has-error-container").css("right", $("fieldset").position().left);
                        if (form.$valid) {
                            fn(scope, {$event: event});
                            form.$submitted = false;
                        }
                    });
                });
            }
        };
    }
    ]);

    refereeModule.controller('IndexController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];


    }]);

    refereeModule.controller('CreateController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];
        $scope.showWeeks = false;


        $scope.formSubmit = function ($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            if ($scope.createForm.$valid) {
                $scope.showErrors = false;

                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                    } else {
                        if (redirect) {
                            loadAjax(response.redirect_to, '');
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        } else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
            } else {
                $scope.showErrors = true;
                $('.form-errors').show();
            }
            $event.preventDefault();
            return false;
        }


        $scope.opened = [];

        $scope.closeAll = function () {
            $scope.opened.date_of_birth = false;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };
    }]);


    refereeModule.controller('EditController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];
        $scope.showWeeks = false;


        $scope.formSubmit = function ($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    if (redirect) {
                        loadAjax(response.redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    } else {
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }

                }
            });
            $event.preventDefault();
            return false;
        }


        $scope.opened = [];

        $scope.closeAll = function () {
            $scope.opened.date_of_birth = false;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };
    }]);


    refereeModule.controller('ExternalTournamentController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];
        $scope.showWeeks = false;


        $scope.formSubmit = function ($event, action) {
            $http({
                method: 'POST',
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $.Notify({
                        caption: "Success",
                        content: response.message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    loadAjax(response.redirect_to);
                }
            });
            $event.preventDefault();
            return false;
        };


        $scope.opened = [];

        $scope.closeAll = function () {
            $scope.opened.dateFrom = false;
            $scope.opened.dateTo = false;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };
    }]);

    refereeModule.controller('RefereeDetailsController', ['$scope', '$http', '$window', function ($scope, $http, $window) {

        $scope.removeExternal = function ($event, id) {
            var deleteConfirm = $window.confirm('Delete tournament?');
            if (deleteConfirm) {
                $http({
                    method: 'DELETE',
                    url: $event.target.href,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (data) {
                    if (!data.success) {
                        console.log(data);
                    } else {
                        $('#external_' + id).remove();
                    }
                });
            }
            $event.preventDefault();
            return false;

        };

    }]);


})();