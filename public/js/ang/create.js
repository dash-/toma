(function () {

    var loginModule = angular.module('create', ['Application', 'FormErrors']);

    loginModule.controller('createController', function ($scope, $http, $window) {

        $scope.formData = {};

        $scope.createUser = function () {
            if ($scope.createForm.$valid) {
                $scope.showErrors = false;
                $http({
                    method: 'POST',
                    url: '/superadmin/users/create',
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data) {

                        if (!data.success) {
                            angular.forEach(data, function (d, key) {
                                $scope['error_' + key] = d[0];
                            });

                            // add red border
                            $scope.errorState = 'error-state';
                        } else {
                            // if successful, bind success message to message
                            $window.location.href = data.redirect_to;
                        }
                    });
            } else {
                $scope.showErrors = true;
                $('.form-errors').show();
            }
        };

    });
})();