(function () {

    var resetModule = angular.module('resetPassApp', ['ngAnimate', 'Application']);
    resetModule.controller('formController', function ($scope, $http, $window) {

        $scope.user = {};
        $scope.showError = false;
        $scope.msg = '';
        $scope.formSubmit = function ($event) {
            $scope.showError = false;
            $http({
                method: 'POST',
                url: $event.target.action,
                data: $.param($scope.user), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
                .success(function (data) {
                    // console.log(data);

                    if (!data.success) {
                        // if not successful, bind errors to error variables
                        $scope.errorName = 'User not found in database';
                        $scope.showError = true;
                    } else {
                        $scope.msg = data.message;

                        setTimeout(function() {
                            $window.location = '/login'; 
                        }, 5000);
                    }
                });

        };

    });

})();