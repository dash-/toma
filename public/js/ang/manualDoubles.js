(function () {

    var manualDoubles = angular.module('manualDoubles', ['Application', 'ui.bootstrap']);

    manualDoubles.filter("br2comma", function () {
        return function (input) {
            return input.replace(/<br\s*\/?>/mg, ", ");
            // return input.replace(/(\r\n|\n|\r)/gm, "<br>");
        }
    });

    manualDoubles.controller('generatorController', function ($scope, $http, $window, $attrs) {
        $scope.formData= {};
        $scope.fetchUrl = $attrs.scurl;

        $scope.init = function() {
            $http({
                method: 'GET',
                url: $scope.fetchUrl
            }).success(function (response) {

                $scope.list1 = response.single_players;
                $scope.list2 = response.pairs;
                $scope.positions = response.positions;

                $scope.generateNewData();

            }).error(function (error) {
                console.log(error);
            });

            return false;
        };

        var choosen_positions = [];

        $scope.onChanged = function(index, position) {

            if (choosen_positions.indexOf(position) == -1) {
                choosen_positions.push(position);
                $scope.list1[index].position = position;
                $scope.list2.push($scope.list1[index]);
            } else {
                angular.forEach($scope.list2, function(val, key) {
                   if (val.position == position) {
                       $scope.positions.splice($scope.positions.indexOf(position), 1);
                       $scope.list2[key].teams.push($scope.list1[index].teams[0]);
                   }

                });
            }

            if($scope.list1!=undefined)
                $scope.list1.splice(index, 1);

            $scope.generateNewData();

        }

        $scope.generateNewData = function() {
            $scope.formData.player = [];

            angular.forEach($scope.list2, function(val, key) {
                if (val.teams.length == 2) {
                    $scope.formData.player.push({
                        'signup_id': val.id,
                        'team1_id': val.teams[0].team_id,
                        'team2_id': val.teams[1].team_id,
                    });
                }
            });
            return $scope.formData.player;
        }



        $scope.manualSubmit = function($event, message, action) {
            action = action || $event.target.action;

            $http({
                method: 'POST',
                url: action,
                data: $.param($scope.formData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $.Notify({
                        caption: "Error",
                        content: response.message,
                        style: {background: '#FF0000', color: '#fff'}
                    });

                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });

                    setTimeout(function () {
                        $window.location = response.redirect_to;

                    }, 1000);
                }
            });
            $event.preventDefault();
            return false;
        };

    });
})();