(function () {

    var refereeTournament = angular.module('refereeTournaments', ['Application', 'ui.bootstrap']);

    refereeTournament.controller('TournamentsController', ['$scope', '$http', '$window',  function ($scope, $http, $window) {
    	$scope.formData = {};
        $scope.errors = [];

        $scope.apply = function($event, requestType, message, action, confirm_msg, tournament_id)
        {
            action = action || $event.target.action;
            if ($window.confirm(confirm_msg)) {
	        	$http({
	                method: requestType,
	                url: action,
	                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	            }).success(function (response) {
	                if (!response.success) {
	                    $window.alert('There was an error, please contact system administrator');
	                } else {

	                	var redirect_to = '/referee/tournaments?open=' + tournament_id;
	                    
	                    loadAjax(redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
	                }
	            });
			}	
            $event.preventDefault();
            return false;
        }

    }]);
})();