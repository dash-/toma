(function () {
    
    var formAppModule = angular.module('replyFormApp', ['Application', 'ui.bootstrap']);

	formAppModule.controller('FormController', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];


        $scope.formSubmit = function($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    evt = app.BrainSocket.message('notification.event',[1]);
                    if (redirect)
                    {
                        loadAjax(response.redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }
                    else
                    {
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }
                    
                }
            });
            $event.preventDefault();
            return false;

        }
        $scope.opened = [];
        $scope.openDatePicker = function($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[id] = true;
        };

    });
})();