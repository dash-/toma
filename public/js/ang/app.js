var publicModule = angular.module('rfetApp', [
  'ngRoute',
  'ngResource',
  'Application',
  'ui.bootstrap',
  'ui.select2',
  'ngTagsInput',
  'rfetApp.filters',
  'rfetApp.services',
  'rfetApp.directives',
  'rfetApp.controllers'
])