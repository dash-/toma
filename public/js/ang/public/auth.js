(function () {

    var auth = angular.module('auth', ['Application', 'ui.bootstrap']);


    auth.controller('LoginController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
       
        $scope.showError = false;

        $scope.loginSubmit = function (tournament_slug) {
            $scope.showError = false;
            $http({
                method: 'POST',
                url: '/' + tournament_slug + '/user/login',
                data: $.param($scope.formData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  
            }).success(function (response) {
                if (!response.success) {
                    $scope.errorMsg = response.msg;
                    $scope.showError = true;
                } else {
                    // if successful, bind success message to message
                    $window.location.href = response.redirect_url;
                }
            });

        };
       

    }]);

    auth.controller('SignupController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
       
        $scope.signupSubmit = function (tournament_slug) {
            $scope.errors = [];
            $http({
                method: 'POST',
                url: '/' + tournament_slug + '/user/signup',
                data: $.param($scope.formData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  
            }).success(function (response) {
                if (!response.success) {
                    angular.forEach(response, function(d, key) {
                        $scope.errors['error_' + key]  = d[0];
                    });  
                } else {
                    // if successful, redirect to index page
                    $window.location.href = response.redirect_url;
                }
            });

        };
       

    }]);

})();