(function () {

    var messageBoxApp = angular.module('messageBox', ['Application', 'ui.bootstrap', 'ui.tinymce']);


    messageBoxApp.controller('MessageBoxController', ['$scope', '$http', '$window', function ($scope, $http, $window) {

        $scope.formData = {};
        $scope.message = {};
        $scope.message.state = [];
        $scope.message.status = [];

        $scope.formSubmit = function ($event, requestType, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    if (response.action == 'create')
                        evt = app.BrainSocket.message('messagebox.event', [true, response.tournament_id]);

                    $window.location.href = response.redirect_to;
                }
            });
            $event.preventDefault();
            return false;
        }

        $scope.publishAction = function (action) {
            $http({
                method: 'POST',
                url: action,
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    evt = app.BrainSocket.message('messagebox.event', [response.status_bool, response.tournament_id]);
                    angular.forEach(response.messages, function(value, key) {
                        $scope.message.state[value.message_id] = value.state;
                        $scope.message.status[value.message_id] = value.status;
                    });
                }
            });
        }

        $scope.remove = function ($event, action) {
            action = action || $event.target.href;
            var deleteQuestion = $window.confirm('Delete item?');
            if (deleteQuestion) {
                $http({
                    method: 'DELETE',
                    url: action
                }).success(function (data) {
                    if (!data.success) {
                        console.log(data);
                    } else {
                        $('#message_' + data.message_id).remove();
                    }
                });
            }
            $event.preventDefault();
            return false;
        };

    }]);
})();