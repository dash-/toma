(function () {

    var newsApp = angular.module('news', ['Application', 'ui.bootstrap', 'ui.tinymce']);


    newsApp.controller('NewsAdminController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
       
       $scope.formData = {};
       $scope.article = {};
       $scope.article.state = [];
       $scope.article.status = [];

       $scope.tinymceOptions = {
            // selector: "textarea.tinymce",
            theme: "modern",
            plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons textcolor paste textcolor filemanager "
            ],
            // add_unload_trigger: false,
            // autosave_ask_before_unload: true,
            // toolbar1: "saveform",
            toolbar2: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontsizeselect",
            toolbar3: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime | forecolor backcolor",
            toolbar4: "table | hr removeformat | subscript superscript | charmap emoticons | print | ltr rtl | visualchars visualblocks template restoredraft",
            menubar: false,
            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000  '}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000  '}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
        };

       $scope.formSubmit = function($event, requestType, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;
            // tinyMCE.triggerSave();
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $window.location.href = response.redirect_to;
                }
            });
            $event.preventDefault();
            return false;
        }

        $scope.publishAction = function(action) {
            $http({
                method: 'POST',
                url: action,
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $scope.article.state[response.article_id] = response.state;
                    $scope.article.status[response.article_id] = response.status;
                }
            });
        }

        $scope.remove = function ($event, action) {
            action = action || $event.target.href;

            var deleteQuestion = $window.confirm('Delete item?'); 
            
            if (deleteQuestion) {
                $http({
                    method: 'DELETE',
                    url: action
                }).success(function (data) {
                    if (!data.success) {
                        console.log(data);
                    } else {
                        $('#article_' + data.article_id).remove();
                    }
                });
            }
            $event.preventDefault();
            return false;
        };

    }]);
    

    newsApp.controller('NewsController', ['$scope', '$http', '$window', function ($scope, $http, $window) {


    }]);

    newsApp.controller('CommentsController', ['$scope', '$http', '$window', function ($scope, $http, $window) {

        $scope.init = function(url) {
            $http({
                method: 'GET',
                url: url,
            }).success(function (response) {
                $scope.comments = response.comments;
                $scope.tournament_slug = response.tournament_slug;
            });
        }

        $scope.commentSubmit = function(tournament_slug) {
            $scope.errors = [];
            $http({
                method: 'POST',
                url: '/' + tournament_slug + '/news/comment',
                data: $.param($scope.formData),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  
            }).success(function (response) {
                if (!response.success) {
                    angular.forEach(response, function(d, key) {
                        $scope.errors['error_' + key]  = d[0];
                    });
                } else {
                    $scope.formData.text = angular.copy("");
                    $scope.comments.unshift(response.comment[0]);
                }
            });
        }

        $scope.remove = function($event, index, msg) {
            $event.preventDefault();

            if (confirm(msg)) {
                $http({
                    method: 'DELETE',
                    url: $event.target.href,
                }).success(function (response) {
                    if (response.success) {
                        $scope.comments.splice(index, 1);
                    }
                });
            }
            
        }

    }]);


})();