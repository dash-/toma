(function () {

    var tournamentApp = angular.module('tournament', ['Application', 'ui.bootstrap']);


    tournamentApp.controller('ContactController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
       
       $scope.formData = {};
       $scope.alertShow = false;
       $scope.formSubmit = function($event, requestType, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $scope.formData = {};
                    $scope.alertShow = true;
                }
            });
            $event.preventDefault();
            return false;
        }

    }]);
})();