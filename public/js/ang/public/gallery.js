(function () {

    var galleryApp = angular.module('gallery', ['Application', 'ui.bootstrap']);


    galleryApp.controller('GalleryAdminController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
       
        $scope.formData = {};
        $scope.article = {};
        $scope.article.state = [];
        $scope.article.status = [];

        $scope.remove = function ($event, action) {
            action = action || $event.target.href;

            var deleteQuestion = $window.confirm('Delete item?'); 
            
            if (deleteQuestion) {
                $http({
                    method: 'DELETE',
                    url: action
                }).success(function (data) {
                    if (!data.success) {
                        console.log(data);
                    } else {
                        $('#asset_' + data.asset_id).remove();
                    }
                });
            }
            $event.preventDefault();
            return false;
        };

    }]);

})();