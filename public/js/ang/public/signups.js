(function () {

    var signupsApp = angular.module('signups', ['Application', 'ui.bootstrap'])
    .config(function (datepickerConfig) {
        datepickerConfig.showWeeks = false;
    });

    signupsApp.directive('formatdate', function(dateFilter) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });
            }
        }
    });

    signupsApp.controller('SignupsController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
        $scope.hideForm = false;
        
        $scope.getPlayerData = function(tournament_slug, token) {
            $http({
                method: 'GET',
                url: '/' + tournament_slug + '/signups/player-data',
            }).success(function (response) {
               $scope.formData = response.data;
               $scope.formData["_token"] = token;
            });
        }

        $scope.formSubmit = function($event) {
            $event.preventDefault();
            var token = $scope.formData._token;
            $http({
                method: 'POST',
                url: $event.target.action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    if (response.msg)
                        $scope.msg = response.msg;
                } else {
                    $scope.message = response.message;
                    $scope.formData = angular.copy({_token: token});
                    $scope.hideForm = true;
                }
            });
        }
        
        $scope.opened = [];
        $scope.closeAll = function() {
            $scope.opened.date_of_birth = false;
        };

        $scope.openDatePicker = function($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };

    }]);

})();