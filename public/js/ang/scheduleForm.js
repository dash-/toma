(function () {

    var scheduleModule = angular.module('scheduleApp', ['Application', 'ui.bootstrap']);

    scheduleModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        };
    });

    scheduleModule.directive('formattime', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'HH:mm');
                });

            }
        };
    });


    scheduleModule.controller('ScheduleController', function ($scope, $http, $window, $attrs) {
        $scope.formData= {};
        $scope.formData.listType= '';
        $scope.ismeridian = true;
        $scope.opened = [];

        $scope.formSubmit = function($event, requestType, message, action, redirect, listType) {
            redirect = redirect || false;
            action = action || $event.target.action;
            $scope.formData.listType = listType;

            // console.log($scope.formData.listType);
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    if (redirect) {
                        loadAjax(response.redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    } else {
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }

                }
            });
            $event.preventDefault();
            return false;
        };


        $scope.objectSize = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };

        $scope.closeAll = function () {
            
            $scope.opened.date_of_play = false;
            
        };

    });
})();