(function () {

    var signupFormModule = angular.module('signupForm', ['Application', 'ui.bootstrap', 'angularFileUpload']);

    signupFormModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        }
    });

    signupFormModule.controller('checkController', function ($scope, $http, $window) {
        // Default checkers wheter to show or hide divs
        $scope.showInfo = false;
        $scope.noInfo = false;
        $scope.showForm = false;

        $scope.formData = {
            'player': {},
            'player2': {}
        };
        $scope.master = {
            'player': {},
            'player2': {}
        };
        $scope.checker = "";

        $scope.getPlayers = function (val, second, gender, category_id, tournament_id) {
            second = second || false;
            category_id = category_id || false;
            tournament_id = tournament_id || false;
            return $http.get('/signups/players', {
                params: {
                    query: val,
                    gender: gender,
                    category_id: category_id,
                    tournament_id: tournament_id
                }
            }).then(function (response) {
                if (second) {
                    gamers2 = [];
                    angular.forEach(response.data, function (item) {
                        gamers2.push({'licence_number': item.licence_number, 'full_name': item.full_name});
                    });
                    return gamers2;
                }
                var gamers = [];
                angular.forEach(response.data, function (item) {
                    gamers.push({'licence_number': item.licence_number, 'full_name': item.full_name});
                });
                return gamers;
            });
        };

        $scope.checkLicence = function (category_id, draw_id, isDoubles, whichPlayer) {

            isDoubles = isDoubles || false;
            whichPlayer = whichPlayer || false;
            licence_num = '';
            if ($scope.formData.player || $scope.formData.player2) {
                if (isDoubles) {
                    licence_num = (whichPlayer == 1) ? $scope.formData.player.licence_number : $scope.formData.player2.licence_number;
                } else {
                    licence_num = $scope.formData.player.licence_number;
                }

                var check = $scope.formData.player.licence_number != $scope.formData.player2.licence_number
                if (check) {
                    $http({
                        method: 'GET',
                        url: '/signups/licence' + '?licence_number=' + licence_num + '&category_id=' + category_id + '&draw_id=' + draw_id + '&doubles=' + isDoubles,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (response) {
                        if (isDoubles)
                            $scope.doublesResponse(response, whichPlayer);
                        else
                            $scope.singlesResponse(response);
                    });
                }
            }
            // $event.preventDefault();
            return false;
        }

        $scope.singlesResponse = function (response) {
            if (!response.success) {
                $scope.noInfo = true;
                $scope.showInfo = false;
                $scope.showForm = false;
                $scope.message = response.message;
            } else {
                $scope.player = response.player;

                $scope.noInfo = false;
                $scope.showForm = false;
                $scope.showInfo = true;
            }
        }

        $scope.doublesResponse = function (response, whichPlayer) {
            if (!response.success) {
                $scope.noInfo = true;
                $scope.showInfo = false;
                $scope.showForm = false;
                if (whichPlayer == 2)
                    $scope.message2 = response.message;
                else
                    $scope.message = response.message;
            } else {
                if (whichPlayer == 2)
                    $scope.player2 = response.player;
                else
                    $scope.player = response.player;

                $scope.noInfo = false;
                $scope.showForm = false;
                $scope.showInfo = true;
            }
        }

        $scope.showSignupForm = function ($event, whichPlayer) {
            whichPlayer = whichPlayer || false;

            if (!whichPlayer) {
                $scope.showForm = ($scope.showForm == false) ? true : false;
                $scope.noInfo = false;
                $scope.showInfo = false;
                $scope.formData = angular.copy($scope.master);
            }
            else {
                $scope.showForm = ($scope.showForm == false) ? true : false;
                $scope.noInfo = false;
                $scope.showInfo = false;
                $scope.formData = angular.copy($scope.master);
            }
            $event.preventDefault();
            return false;
        }

        $scope.doSignup = function (data) {
            $http({
                method: 'POST',
                url: '/signups/register',
                data: $.param(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success) {
                    evt = app.BrainSocket.message('notification.event', [1]);
                    $window.location = response.redirect;
                } else {
                    console.log('USAO');
                    $scope.noInfo = true;
                    $scope.showInfo = false;
                    $scope.showForm = false;
                    $scope.message = response.message;
                }
            });
        }

        $scope.doDoublesSignup = function (data) {
            if (data.player2 && data.player) {
                $http({
                    method: 'POST',
                    url: '/signups/register-doubles',
                    data: $.param(data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response.success) {
                        evt = app.BrainSocket.message('notification.event', [1]);
                        $window.location = response.redirect;
                    }
                });
            } else {
                alert("please populate both players");
            }

        }

        $scope.signupForm = function ($event, draw_id, manual, isDoubles) {
            manual = manual || false;
            isDoubles = isDoubles || false;
            if (isDoubles) {
                if ($scope.player && $scope.player2) {
                    var data = {
                        player: $scope.player,
                        player2: $scope.player2,
                        draw_id: draw_id
                    };
                    if (manual)
                        $scope.checkManualSignup(data, isDoubles);
                    else
                        $scope.doDoublesSignup(data);
                }
            } else {
                if ($scope.player) {
                    var data = {
                        player: $scope.player,
                        draw_id: draw_id
                    };
                    if (manual)
                        $scope.checkManualSignup(data);
                    else
                        $scope.doSignup(data);
                }
            }

            $event.preventDefault();
            return false;
        }
        $scope.checkManualSignup = function (data, isDoubles) {
            isDoubles = isDoubles || false;
            var href = (isDoubles) ? '/signups/check-doubles' : '/signups/check-singuper'
            $http({
                method: 'POST',
                url: href,
                data: $.param(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success) {
                    if (isDoubles)
                        $scope.doDoublesSignup(data);
                    else
                        $scope.doSignup(data);
                } else {
                    $scope.noInfo = true;
                    $scope.showInfo = false;
                    $scope.showForm = false;
                    $scope.message = response.message;
                }
            });
            return $scope.checker;
        }


        $scope.input = [];
        $scope.showInput = function ($event, type) {
            // console.log($scope.input[type]);
            $scope.input[type] = ($scope.input[type] == false || $scope.input[type] == undefined ) ? true : false;
            $event.preventDefault();
            return false;
        }


        $scope.opened = [];

        $scope.closeAll = function () {
            $scope.opened.date_of_birth = false;
            $scope.opened.date_of_birth2 = false;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };

    });


    signupFormModule.controller('bulkController', function ($scope, $http, $window, $upload) {
        $scope.formData = {};

        function handleResponse(response) {
            if (response.success) {
                $scope.msg = response.msg;
                $scope.fails = response.licence_fails;
                $scope.formData = angular.copy({});
            } else {
                alert('Error! Please contact system administrator');
            }
        }

        $scope.textBulk = function (draw_id) {
            $http({
                method: 'POST',
                url: '/signups/bulk-import/' + draw_id,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                handleResponse(response);
            });
        };

        $scope.onFileSelect = function ($files, draw_id) {
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];
                $scope.upload = $upload.upload({
                    url: '/signups/bulk-import/' + draw_id,
                    method: 'POST',
                    data: {file: file},
                    file: file
                }).success(function (data, status, headers, config) {
                    handleResponse(data);
                });

            }
        };
    });

    signupFormModule.controller('fileController', function ($scope, $http, $window) {
        $scope.formData = {};

        function handleResponse(response) {
            if (response.success) {
                $window.location = response.redirect_to;
            } else {
                console.log('Error! Please contact system administrator');
            }
        }

        $scope.pairsImport = function (draw_id) {
            if ($scope.formData.list_type) {
                $scope.errorList = {};
                $http({
                    method: 'POST',
                    url: '/signups/file-pairs/' + draw_id,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    handleResponse(response);
                });
            } else {
                $scope.errorList = {
                    list_type: 'Please choose list type'
                };
            }

        };
    });

    signupFormModule.controller('externalController', function($scope, $http, $window) {

        $scope.getPlayersExternal = function (val) {
            $http({
                method:'GET',
                url: 'external/pinfo',
            }).success(function (data) {
                console.log(data);
            });
        };
});

})();