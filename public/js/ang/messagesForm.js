(function () {
    
    var formAppModule = angular.module('messagesFormApp', ['Application', 'ngTagsInput', 'ui.bootstrap']);

    formAppModule.directive('formatdate', function(dateFilter) {
      return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });
                
            }
        }
    });


	formAppModule.controller('FormController', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.master = {};
        $scope.showWeeks = false;
        $scope.errors = [];

        $scope.loadUsers = function(query) {
            return $http.get('/messages/users?query=' + query);
        }; 

        $scope.formSubmit = function($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;
            if($scope.formData.messages.user_id.length > 0) {
                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData), 
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (response) {
                    if (!response.success) {
                        $scope.errors = [];
                        angular.forEach(response, function(d, key) {
                            ket = key.split('.');
                            $scope.errors[ket[1]] = d[0];
                        });
                        $scope.errorState = 'error-state';
                    } else {

                        evt = app.BrainSocket.message('notification.event',[1]);
                        if (redirect)
                        {
                            loadAjax(response.redirect_to, '');
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }
                        else
                        {
                            $scope.formData = angular.copy($scope.master);
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }
                        
                    }
                });
            }
            $event.preventDefault();
            return false;

        }
        $scope.opened = [];
        $scope.openDatePicker = function($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.opened[id] = true;
        };

    });
})();