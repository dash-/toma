angular.module('rfetApp.controllers', [])
  
    .controller('loginController', function ($scope, $http, $window) {

        $scope.user = {};
        $scope.showError = false;

        $scope.login = function (form) {
            $scope.showError = false;
            $http({
                method: 'POST',
                url: '/login',
                data: $.param($scope.user),  // pass in data as strings
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            })
                .success(function (data) {
                    if (!data.success) {
                        // if not successful, bind errors to error variables
                        $scope.errorName = 'You entered wrong email or password.';
                        $scope.showError = true;
                    } else {
                        // if successful, bind success message to message
                        $window.location.href = "/";
                    }
                });

        };

    })
    
    .controller('userCreateController', function ($scope, $http, $window) {

        $scope.formData = {};
        console.log('iusao');
        $scope.createUser = function() {
            $http({
                method: 'POST',
                url: '/superadmin/users/create',
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' } 
            })
                .success(function (data) {

                    if (!data.success) {
                        angular.forEach(data, function(d, key) {
                            console.log(d + '   ' +key);
                            $scope['error_' + key]  = d[0];
                        });

                        // add red border
                        $scope.errorState = 'error-state';
                    } else {
                        $window.location.href = data.redirect_to;
                    }
                });

        };

        $scope.hasError = function(field, validation){
            if(validation){
                return ($scope.form[field].$dirty && $scope.form[field].$error[validation]) || ($scope.submitted && $scope.form[field].$error[validation]);
            }
            return ($scope.form[field].$dirty && $scope.form[field].$invalid) || ($scope.submitted && $scope.form[field].$invalid);
        };

    })
  
    .controller('userController', function ($scope, $http, $window) {
        $scope.removeUser = function ($event) {
            var deleteUser = $window.confirm('Delete user?'); 
            if (deleteUser) {
                $http({
                    method: 'DELETE',
                    url: $event.target.href,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (data) {
                        if (!data.success) {
                            console.log(data);
                        } else {
                            $window.location.href = $event.target.baseURI;
                        }
                    });
            }
            $event.preventDefault();
            return false;
        };
    });

