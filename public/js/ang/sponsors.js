(function () {

    var sponsorsAppModule = angular.module('sponsorsApp', ['Application', 'ui.bootstrap']);

    sponsorsAppModule.controller('SponsorsController', function ($scope, $http, $window, $modal) {

        $scope.openModal = function (action, sponsor_id) {
            $http({
                method: "GET",
                url: action + '/' + sponsor_id
            }).success(function (response) {
                $scope.open(response.image);
            });
            return false;
        }

        $scope.open = function (image_name, size) {
            $scope.image_name = image_name;

            var modalInstance = $modal.open({
              template: '<img src="' + image_name + '">',
              controller: ModalInstanceCtrl,
              size: size,
              resolve: {
                image_name: function () {
                    return $scope.image_name;
                }
              }
            });
        };

        
    });

    var ModalInstanceCtrl = function ($scope, $modalInstance, $http, image_name, $window) {
        $scope.image_name = image_name;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

})();