(function () {

    var approvalActionModule = angular.module('approvalActions', ['Application', 'ui.bootstrap']);


    approvalActionModule.controller('actionsController', function ($scope, $http, $window, $modal) {
        $scope.actionsPermited = true;
        $scope.postRequest = function ($event, message, sts) {
            /*var confirm = $window.confirm(confirmation); 
             if (confirm)
             {*/
            $http({
                method: 'POST',
                url: $event.target.href + '?status=' + sts,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {


                    evt = app.BrainSocket.message('notification.event', [1]);


                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    $scope.actionsPermited = false;
                }
            });
            /*}*/
            $event.preventDefault();
            return false;
        }
        $scope.validated = false;
        $scope.validateTournament = function ($event, message) {
            $http({
                method: 'POST',
                url: $event.target.href,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success) {
                    $scope.validated = true;
                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                }
            });
            $event.preventDefault();
            return false;
        }

        $scope.open = function (size) {

            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });

        };

    });

    var ModalInstanceCtrl = function ($scope, $modalInstance, $http) {
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
        $scope.formData = {};
        $scope.errors = [];
        $scope.errorMsg = "";
        $scope.hasError = false;

        $scope.formSubmit = function (requestType, message, action, $event) {
            action = action || $event.target.action;
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {

                    evt = app.BrainSocket.message('notification.event', [1]);

                    $modalInstance.close();
                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                }
            });
            // $event.preventDefault();
            return false;
        };

        $scope.approvePlayer = function (invoice_check, player_id) {
            $http({
                method: 'GET',
                url: '/players/approve/' + player_id + '?invoice=' + invoice_check,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errorMsg = "Something went wrong";
                    $scope.hasError = true;
                } else {
                    evt = app.BrainSocket.message('notification.event', [1]);
                    $scope.hasError = false;
                    $modalInstance.close();
                    $.Notify({
                        caption: "Success",
                        content: response.message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    loadAjax(response.redirect_to, '');
                }
            });
            return false;
        };

        $scope.licenceSubmit = function (requestType, message, action, $event) {
            action = action || $event.target.action;
            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response.success == "false") {
                    $scope.errorMsg = "Licence number is invalid";
                    $scope.hasError = true;
                } else {

                    evt = app.BrainSocket.message('notification.event', [1]);

                    $scope.hasError = false;
                    $modalInstance.close();
                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    loadAjax(response.redirect_to, '');
                }
            });
            // $event.preventDefault();
            return false;
        };
    };

    approvalActionModule.controller('playerActionsController', function ($scope, $http, $window, $modal) {

        $scope.postRequest = function ($event, message, sts) {
            $http({
                method: 'POST',
                url: $event.target.href + '?status=' + sts,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    $.Notify({
                        caption: "Success",
                        content: message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    location.reload();
                }
            });

            $event.preventDefault();
            return false;
        }

        $scope.open = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'myModalContent.html',
                controller: ModalInstanceCtrl,
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
        };

        $scope.openLicNum = function (size) {

            var modalInstance = $modal.open({
                templateUrl: 'licenceNumberContent.html',
                controller: ModalInstanceCtrl,
                size: size
            });

        };

    });

})();