(function () {

    var searchModule = angular.module('playerSearch', ['Application', 'ui.bootstrap']);

    searchModule.filter("asDate", function () {
        return function (input) {
            date = new Date(input);
            if (isNaN(date))
                date = new Date(input.replace(/-/g, "/"));

            return date;
        }
    });

    searchModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        };
    });


    searchModule.controller('PlayerSearchController', ['$scope', '$http', '$window', function ($scope, $http) {
        $scope.formData = {};
        $scope.errors = [];

        $scope.hideForm = false;
        $scope.formData.sex = 'M';

        $scope.processForm = function (data) {
            if (data.sex) {
                $http({
                    method: 'POST',
                    url: '/players/search',
                    data: $.param(data),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    $scope.hideForm = true;

                    if (response.success) {
                        $scope.players = response.players;
                    } else {
                        $scope.players = [];
                    }
                }).error(function () {
                    $scope.players = [];
                });
            } else {
                $scope.errors['sex'] = true;
            }

        };

        $scope.searchByName = function ($event) {
            $scope.errors = [];
            var data = {
                'sex': $scope.formData.sex,
                'surname': $scope.formData.surname
            };

            if (data.surname != undefined && data.surname != "") {
                $scope.errors['surname'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['surname'] = true;
            }

            $event.preventDefault();
            return false;
        };

        $scope.searchByLicence = function ($event) {
            $scope.errors = [];
            var chosen_sex = ($scope.formData.sex) ? $scope.formData.sex : -1;

            var data = {
                'sex': chosen_sex,
                'licence_number': $scope.formData.licence_number
            };

            console.log(data);

            if (data.licence_number != undefined && data.licence_number != "") {
                $scope.errors['licence_number'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['licence_number'] = true;
            }


            $event.preventDefault();
            return false;
        };

        $scope.searchByRanking = function ($event) {
            $scope.errors = [];
            var data = {
                'sex': $scope.formData.sex,
                'ranking_from': $scope.formData.ranking_from,
                'ranking_to': $scope.formData.ranking_to
            };

            if (data.ranking_from != undefined && data.ranking_from != "") {
                $scope.errors['ranking_from'] = false;
            } else {
                $scope.errors['ranking_from'] = true;
            }

            if (data.ranking_to != undefined && data.ranking_to != "") {
                $scope.errors['ranking_to'] = false;
            } else {
                $scope.errors['ranking_to'] = true;
            }

            if ((data.ranking_from != undefined && data.ranking_from != "")
                && (data.ranking_to != undefined && data.ranking_to != "")) {
                $scope.processForm(data);
            }


            $event.preventDefault();
            return false;
        };

        $scope.searchByPoints = function ($event) {
            $scope.errors = [];
            var data = {
                'sex': $scope.formData.sex,
                'points_from': $scope.formData.points_from,
                'points_to': $scope.formData.points_to
            };

            if (data.points_from != undefined && data.points_from != "") {
                $scope.errors['points_from'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['points_from'] = true;
            }

            if (data.points_to != undefined && data.points_to != "") {
                $scope.errors['points_to'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['points_to'] = true;
            }


            $event.preventDefault();
            return false;
        };

        $scope.searchByDate = function ($event) {
            $scope.errors = [];
            var data = {
                'sex': $scope.formData.sex,
                'birth_from': $scope.formData.birth_from,
                'birth_to': $scope.formData.birth_to
            };

            if (data.birth_from != undefined && data.birth_from != "") {
                $scope.errors['birth_from'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['birth_from'] = true;
            }

            if (data.birth_to != undefined && data.birth_to != "") {
                $scope.errors['birth_to'] = false;
                $scope.processForm(data);
            } else {
                $scope.errors['birth_to'] = true;
            }


            $event.preventDefault();
            return false;
        };

        $scope.closeAll = function() {
            $scope.opened.birthFrom = false;
            $scope.opened.birthTo = false;
        };

        $scope.opened = [];
        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
            if (id == 'birthTo') {
                if ($scope.formData.birth_from != undefined) {
                    var act = new Date($scope.formData.birth_from);
                    $scope.minDate = act;
                    $scope.formData.birth_to = act;
                }
            }
        };

    }]);


})();