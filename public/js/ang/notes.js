(function () {
    
    var notesModule = angular.module('notesApp', ['Application', 'ui.bootstrap']);

    notesModule.directive('validSubmit', [ '$parse', function($parse) {
        return {
            require : '^form',
            link: function(scope, element, iAttrs, controller) {
                var form = element.controller('form');    
                form.$submitted = false;
                var fn = $parse(iAttrs.validSubmit);
                
                element.on('submit', function(event) {
                    scope.$apply(function() {
                        element.addClass('ng-submitted');
                        form.$submitted = true;
                        $(".has-error-container").css("right", $("fieldset").position().left);
                        if (form.$valid) {
                            fn(scope, { $event : event });
                            form.$submitted = false;
                        }});
                });
            }
        };
    }
    ]);

    notesModule.controller('IndexController', ['$scope', '$http', '$window',  function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.errors = [];
        $scope.init = function (url) {
            $http({
                method: 'GET',
                url: url,
                //headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                $scope.notes = response.notes;
                if (response.player)
                    $scope.player = response.player;
            }).error(function (error) {
                console.log(error);
            });

            return false;
        }; // init

        $scope.formSubmit = function($event, action) {
            $http({
                method: 'POST',
                url: action,
                data: $.param($scope.formData), 
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (response) {
                if (response.success) {
                    $scope.notes.unshift(response.data);
                    $scope.formData.text = "";
                    $scope.errors = [];
                } else {
                    angular.forEach(response, function(d, key) {
                        $scope.errors.push({key: d[0]});
                    });
                }
            });
            $event.preventDefault();
            return false;
        }

        $scope.delete = function($event, index) {
            if ($window.confirm('Delete note?')) {
                $http({
                    method: 'DELETE',
                    url: $event.currentTarget.href,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                }).success(function (response) {
                    if (response.success)
                        $scope.notes.splice(index, 1);
                });
            }
            $event.preventDefault();
            return false;
        }
    }]);

    
})();