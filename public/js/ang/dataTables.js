(function () {

    var datatablesApp = angular.module('datatablesApp', ['ngResource', 'ui.bootstrap', 'Application']);

    datatablesApp.filter("asDate", function () {
        return function (input) {
            date = new Date(input);
            if(isNaN(date))
                date = new Date(input.replace(/-/g, "/"));

            return date;
        }
    });
    datatablesApp.filter("rankingMove", function () {
        return function (input) {
            if(input>0)
                input = '+'+input;
            return input;
        }
    });

    datatablesApp.filter("showStatus", function () {
        return function (input) {
            var active = {
                '-3': "RIP",
                '-2': 'out',
                '-1': 'Only applied',
                '0': 'No',
                '1': 'Yes'
            };

            return active[input];
        }
    });
    datatablesApp.filter("externalTourType", function () {
        return function (input) {
            var types = {
                '1' : 'Tennis Europe',
                '2' : 'ITF Junior',
                '3' : 'ATP',
                '4' : 'WTA'
            };

            return types[input];
        }
    });

    datatablesApp.controller('tableController', function ($scope, $http, $attrs,  $window) {
        $scope.ascender = true;
        $scope.order = $attrs.tablesortcolumn != undefined ? $attrs.tablesortcolumn : 'name';
        $scope.sort = 'asc';
        $scope.scSearch = 0;
        $scope.callback = 'JSON_CALLBACK';
        $scope.showDeleted = false;
        $scope.sex = "M";
        $scope.federation_club_id = null;

        if(window.location.href.indexOf("rankings") > -1)
           $scope.counter = 50;
        else
            $scope.counter = 10;

        $scope.masterForm = {
            "search": 1
        };

        $scope.fetchUrl = $attrs.scurl;

        isRequest = true;


        $scope.init = function (num, queryString) {
            if (isRequest) {
                isRequest = false;
                values = angular.extend({},
                    {'callback': $scope.callback},
                    {'items_counter': $scope.counter},
                    {'sex': $scope.sex},
                    {'order': $scope.order},
                    {'sort': $scope.sort},
                    {'show_deleted': $scope.showDeleted},
                    {'federation_club_id': $scope.federation_club_id},
                    {'fined': $scope.fined}
                );
                num = num || 1;
                initValues = angular.extend(values, {'page': num});

                searchQuery = queryString || initValues;

                $http({
                    method: 'JSONP',
                    url: $scope.fetchUrl,
                    params: searchQuery
                }).success(function (response) {
                    $scope.rows = response.data;
                    $scope.user_region = response.user_region_id;
                    $scope.user_role = response.user_role;
                    $scope.totalItems = response.total;
                    $scope.noOfPages = response.last_page;
                    $scope.bigCurrentPage = num;
                    isRequest = true;
                    // console.log($scope.fetchUrl);
                }).error(function (error) {
                    console.log(error);
                });

                return false;
            }
        }; // init

        $scope.resetSearch = function () {
            $scope.showDeleted = false;
            $scope.init(1);
            $scope.searchText = angular.copy($scope.masterForm);
            $scope.scSearch = 0;
            $scope.bigCurrentPage = 1;

            return false;
        }

        $scope.maxSize = 10;
        $scope.$watch('bigCurrentPage', function (newPage, oldValue) {
            if (typeof oldValue === 'undefined') return;

            if ($scope.scSearch == 1)
                $scope.search($scope.scEvent, newPage);
            else
                $scope.init(newPage);
        });

        $scope.search = function ($event, currentPage) {
            currentPage = currentPage || 1;
            searchQuery = angular.extend({},
                {'callback': $scope.callback},
                $scope.searchText,
                {'items_counter': $scope.counter},
                {'order': $scope.order},
                {'sort': $scope.sort},
                {'page': currentPage},
                {'federation_club_id': $scope.federation_club_id},
                {'fined': $scope.fined}
            );

            $scope.scSearch = 1;
            $scope.scEvent = $event;
            $event.preventDefault();
            $scope.init(currentPage, searchQuery);
        }

        $scope.contactsFilter = function(val)
        {
            $scope.scSearch = 1;
            searchQuery = angular.extend({}, {'callback': $scope.callback}, $scope.searchText, {'items_counter': $scope.counter}, {'order': $scope.order}, {'sort': $scope.sort}, {'page': 1});
            $scope.init(1, searchQuery);
        }

        $scope.sortBy = function (column) {
            if ($scope.order == column && $scope.sort == 'asc')
                return 'icon-arrow-down-5';
            else if ($scope.order == column && $scope.sort == 'desc')
                return 'icon-arrow-up-5';

            return '';
        }
        $scope.doOrder = function (type) {
            $scope.ascender = ($scope.ascender === false) ? true : false;
            $scope.order = type;
            if ($scope.ascender)
                $scope.sort = 'asc';
            else
                $scope.sort = 'desc';

            if ($scope.scSearch == 1)
                $scope.search($scope.scEvent, 1);
            else
                $scope.init(1);

            $scope.bigCurrentPage = 1;
            return false;
        }

        $scope.itemsPerPage = function () {
            if ($scope.scSearch == 1)
                $scope.search($scope.scEvent, 1);
            else
                $scope.init(1);

            $scope.bigCurrentPage = 1;
        }

        $scope.filterBy = function() {
            if ($scope.scSearch == 1)
                $scope.search($scope.scEvent, 1);
            else
                $scope.init(1);
        }

        $scope.sexChange = function () {
            if ($scope.scSearch == 1)
                $scope.search($scope.scEvent, 1);
            else
                $scope.init(1);
        }

        $scope.remove = function ($event, index) {
            $http({
                method: 'DELETE',
                url: $event.target.href,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                if (!data.success) {
                    alert('There was a problem please contact system admin');
                } else {
                    $.Notify({
                        caption: "Success",
                        content: "Notification succesfully deleted",
                        style: {background: '#16499A', color: '#fff'}
                    });
                    $scope.init(1);
                }
            });
            $event.preventDefault();
            return false;
        };

        $scope.removeAll = function ($event) {
            $http({
                method: 'DELETE',
                url: $event.target.href,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).success(function (data) {
                if (!data.success) {
                    alert('There was a problem please contact system admin');
                } else {
                    $.Notify({
                        caption: "Success",
                        content: "All notifications succesfully deleted",
                        style: {background: '#16499A', color: '#fff'}
                    });
                    $scope.init(1);
                }
            });
            $event.preventDefault();
            return false;
        };

        $scope.showAll = function ($event) {

            $scope.showDeleted = true;
            $scope.init(1,'');
            $event.preventDefault();
            return false;
        };

    }); // Ctrl

})();