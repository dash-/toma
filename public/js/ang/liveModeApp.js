(function () {

    var liveModeModule = angular.module('liveModeApp', ['Application', 'ui.bootstrap']);
    liveModeModule.filter("asDateTime", function () {
        return function (input) {
            date = new Date(input);
            if (isNaN(date))
                date = new Date(input.replace(/-/g, "/"));

            date = date.format("dd/mm/yyyy HH:MM");
            return date;
        }
    });
    liveModeModule.filter("tiebreakscore", function () {
        return function (input) {
            if (input)
                return '(' + input + ')';
        }
    });

    liveModeModule.controller("liveModeController", function ($scope, $http, $interval, $timeout) {
        $scope.button_hold = false;
        $scope.show_popup = 0;
        $scope.match_id = 0;
        $scope.matchStatus = -1;
        $scope.finished_at = '';
        $scope.serving = 1;
        $scope.counter = 0;
        $scope.removing = 0;
        $scope.game_number = 1;
        $scope.draw_rules = {};
        $scope.tieBreakEnabled = false;
        $scope.game_score_id = 0;
        $scope.showWarningMsg = false;
        $scope.match_rule = 0;
        $scope.gamescore = {
            "currentScoreTeam1": 0,
            "currentScoreTeam2": 0,
            "currentScoreTeam11": 0,
            "currentScoreTeam12": 0,
            "currentScoreTeam21": 0,
            "currentScoreTeam22": 0
        };

        $scope.scoreValues = {
            0: "00",
            1: "15",
            2: "30",
            3: "40",
            4: "  ",
            5: " A",
            6: "  "
        };

        $scope.setInProgress = 1;
        $scope.setPoints = {};
        $scope.setPoints.team1 = [0, 0, 0, 0, 0];
        $scope.setPoints.team2 = [0, 0, 0, 0, 0];
        $scope.tiePoints = {};
        $scope.tiePoints.team1 = [0, 0, 0, 0, 0];
        $scope.tiePoints.team2 = [0, 0, 0, 0, 0];
        //$scope.holder = {
        //    'gamescore': $scope.gamescore,
        //    'setpoints': $scope.setPoints,
        //    'tiebreak': $scope.tiePoints,
        //    'match_id': $scope.match_id
        //};

        $scope.init = function (match_id) {
            $scope.match_id = match_id;
            $http({
                method: 'GET',
                url: "/match/match-data/" + $scope.match_id
            }).success(function (data) {
                if (data.success) {
                    $scope.match = data.match;
                    $scope.draw_rules = data.draw_rules;
                    //$scope.team1_id = data.
                    if (data.match) {
                        $scope.matchStatus = data.match.match_status;
                        $scope.finished_at = data.match.match_finished_at;
                    }
                    if (data.set_in_progress)
                        $scope.setInProgress = data.set_in_progress;

                    if (data.team1_score && data.team2_score) {
                        $scope.copySetScore(data.team1_score, data.team2_score);


                        if (data.team1_score[$scope.setInProgress - 1] == 6 && data.team2_score[$scope.setInProgress - 1] == 6) {
                            $scope.tieBreakEnabled = true;
                        }
                    }
                    if (data.game_score) {
                        $scope.serving = data.game_score.served_by == 0 ? 1 : data.game_score.served_by;
                        $scope.game_number = data.game_score.game_number;
                        //$scope.setInProgress = data.game_score.set_number;
                        $scope.game_score_id = data.game_score.id;
                        if ($scope.matchStatus == 3)
                            $scope.playing_counter();
                        if ($scope.matchStatus == 4)
                            $scope.calculatePlayingTimeWhenFinished();
                    }
                    if (data.game_score_details) {
                        $scope.gamescore.currentScoreTeam1 = data.game_score_details.team1_score;
                        $scope.gamescore.currentScoreTeam2 = data.game_score_details.team2_score;
                    }

                    if (data.team1_tiescore && data.team2_tiescore) {
                        $scope.copyTieSetScore(data.team1_tiescore, data.team2_tiescore);
                    }

                    if ($scope.tieBreakEnabled) {
                        $scope.tiePoints.team1[$scope.setInProgress] = data.tiebreak_score.team1_tiescore;
                        $scope.tiePoints.team2[$scope.setInProgress] = data.tiebreak_score.team2_tiescore;
                        $scope.showTiebreakScore();
                    }
                    else
                        $scope.showScore();
                } else {
                    $scope.matchStatus = 0;
                    $scope.showWarningMsg = true;
                }
            });

        };

        $scope.startTheMatch = function (match_id) {
            $http({
                method: 'POST',
                url: "/match/start-match",
                data: {"match_id": match_id}
            }).success(function (data) {
                $scope.init(match_id);
            });
        };

        $scope.copySetScore = function (team1_score, team2_score) {
            if (team1_score) {
                team1_score.forEach(function (entry, key) {
                    $scope.setPoints.team1[key] = entry;
                });
            }
            if (team2_score) {
                team2_score.forEach(function (entry, key) {
                    $scope.setPoints.team2[key] = entry;
                });
            }
        };
        $scope.copyTieSetScore = function (team1_tie, team2_tie) {
            if (team1_tie) {
                team1_tie.forEach(function (entry, key) {
                    $scope.tiePoints.team1[key] = entry;
                });
            }
            if (team2_tie) {
                team2_tie.forEach(function (entry, key) {
                    $scope.tiePoints.team2[key] = entry;
                });
            }
        };

        $scope.addScore = function (team) {
            if ($scope.button_hold)
                return;
            $scope.button_hold = true;
            $timeout(function () {
                $scope.button_hold = false
            }, 1000);


            if (!$scope.tieBreakEnabled) {
                if (team == 1)
                    $scope.gamescore.currentScoreTeam1 = $scope.gamescore.currentScoreTeam1 + 1;

                if (team == 2)
                    $scope.gamescore.currentScoreTeam2 = $scope.gamescore.currentScoreTeam2 + 1;

                $scope.removing = team;


                $scope.processPoints();
                $scope.showScore();
            }
            else {

                var progressSet = $scope.setInProgress - 1;
                if (team == 1)
                    $scope.tiePoints.team1[progressSet] = $scope.tiePoints.team1[progressSet] + 1;

                if (team == 2)
                    $scope.tiePoints.team2[progressSet] = $scope.tiePoints.team2[progressSet] + 1;

                $scope.processTiebreakPoint();
                $scope.showTiebreakScore();
            }

            //console.log($scope.gamescore);
            var msg = {
                'gamescore': $scope.gamescore,
                'setpoints': $scope.setPoints,
                'tiebreak': $scope.tiePoints,
                'match_id': $scope.match_id,
                'set_in_progress' : $scope.setInProgress,
                'tie_break' : $scope.tieBreakEnabled,
                'completed' : $scope.matchStatus == 4
            };
            app.BrainSocket.message('live_score_app_mode.changed', msg);

        };

        $scope.removeScore = function (team) {
            if ($scope.button_hold)
                return;
            $scope.button_hold = true;
            $timeout(function () {
                $scope.button_hold = false
            }, 1000);


            if (team == 1)
                $scope.gamescore.currentScoreTeam1 = $scope.gamescore.currentScoreTeam1 - 1;
            if (team == 2)
                $scope.gamescore.currentScoreTeam2 = $scope.gamescore.currentScoreTeam2 - 1;

            $scope.removing = 0;

            if (
                ($scope.gamescore.currentScoreTeam1 == 3 && $scope.gamescore.currentScoreTeam2 == 4) ||
                ($scope.gamescore.currentScoreTeam1 == 4 && $scope.gamescore.currentScoreTeam2 == 3)
            ) {
                $scope.gamescore.currentScoreTeam1 = 3;
                $scope.gamescore.currentScoreTeam2 = 3;
            }

            $scope.showScore();
        };

        $scope.processTiebreakPoint = function () {
            var progressSet = $scope.setInProgress - 1;

            var difference = Math.abs($scope.tiePoints.team1[progressSet] - $scope.tiePoints.team2[progressSet]);

            if ($scope.tiePoints.team1[progressSet] < 7 && $scope.tiePoints.team2[progressSet] < 7) {
                $http({
                    method: 'POST',
                    url: "/match/save-tiebreak-point/" + $scope.game_score_id,
                    data: {
                        'set_in_progress': $scope.setInProgress,
                        'match_id': $scope.match_id,
                        'tiepoints_team1': $scope.tiePoints.team1[$scope.setInProgress - 1],
                        'tiepoints_team2': $scope.tiePoints.team2[$scope.setInProgress - 1],
                        'game_number_id': $scope.game_number,
                        'game_score_id': $scope.game_score_id
                    }
                })
            }
            else if (difference >= 2) {
                if ($scope.tiePoints.team1[progressSet] > $scope.tiePoints.team2[progressSet])
                    $scope.gameWon(1, $scope.tiePoints.team1[progressSet], $scope.tiePoints.team2[progressSet]);
                else
                    $scope.gameWon(2, $scope.tiePoints.team1[progressSet], $scope.tiePoints.team2[progressSet]);

                return;
            }
            //if ($scope.gamescore.currentScoreTeam1 == 4 && $scope.gamescore.currentScoreTeam2 < 3) {
            //    $scope.gameWon(1, $scope.gamescore.currentScoreTeam1, $scope.gamescore.currentScoreTeam2);
            //    return;
            //}

        }
        $scope.processPoints = function () {

            if ($scope.gamescore.currentScoreTeam1 > 5 && $scope.gamescore.currentScoreTeam2 > 5)
                $scope.tieBreakEnabled = true;

            //normal scoring
            if ($scope.gamescore.currentScoreTeam1 == 4 && $scope.gamescore.currentScoreTeam2 < 3) {
                $scope.gameWon(1, $scope.gamescore.currentScoreTeam1, $scope.gamescore.currentScoreTeam2);
                return;
            }
            else if ($scope.gamescore.currentScoreTeam2 == 4 && $scope.gamescore.currentScoreTeam1 < 3) {
                $scope.gameWon(2, $scope.gamescore.currentScoreTeam1, $scope.gamescore.currentScoreTeam2);
                return;
            }

            //give won after A
            if ($scope.gamescore.currentScoreTeam1 > 5 && $scope.gamescore.currentScoreTeam2 == 3) {
                $scope.gameWon(1, $scope.gamescore.currentScoreTeam1, $scope.gamescore.currentScoreTeam2);
                return;
            }
            else if ($scope.gamescore.currentScoreTeam2 > 5 && $scope.gamescore.currentScoreTeam1 == 3) {
                $scope.gameWon(2, $scope.gamescore.currentScoreTeam1, $scope.gamescore.currentScoreTeam2);
                return;
            }

            //giving A for a player
            if ($scope.gamescore.currentScoreTeam1 > 3 && $scope.gamescore.currentScoreTeam2 == 3)
                $scope.gamescore.currentScoreTeam1 = 5;
            else if ($scope.gamescore.currentScoreTeam2 > 3 && $scope.gamescore.currentScoreTeam1 == 3)
                $scope.gamescore.currentScoreTeam2 = 5;

            //if player has A and other scored, give both 40:40
            if (($scope.gamescore.currentScoreTeam1 == 5 && $scope.gamescore.currentScoreTeam2 == 4) ||
                ($scope.gamescore.currentScoreTeam1 == 4 && $scope.gamescore.currentScoreTeam2 == 5)) {
                $scope.gamescore.currentScoreTeam1 = 3;
                $scope.gamescore.currentScoreTeam2 = 3;
            }
            $http({
                method: 'POST',
                url: "/match/save-game-point/" + $scope.game_score_id,
                data: {
                    'gamescore': $scope.gamescore,
                    'game_score_id': $scope.game_score_id,
                }
            })
        };

        $scope.processSetPoint = function (team) {
            var progressSet = $scope.setInProgress - 1;
            //console.log($scope.setPoints.team1[progressSet]);
            //console.log($scope.setPoints.team2[progressSet]);

            if (team == 1)
                $scope.setPoints.team1[progressSet] = $scope.setPoints.team1[progressSet] + 1;
            if (team == 2)
                $scope.setPoints.team2[progressSet] = $scope.setPoints.team2[progressSet] + 1;


            if ($scope.setPoints.team1[progressSet] == 6 && $scope.setPoints.team2[progressSet] == 6) {
                $scope.tieBreakEnabled = true;
                return false;
            }

            if ($scope.tieBreakEnabled) {
                $scope.setWon(team);
                return true;
            }


            //gives teams set win
            if ($scope.setPoints.team1[progressSet] == 6 && $scope.setPoints.team2[progressSet] < 5) {
                $scope.setWon(1);
                return true;
            }
            if ($scope.setPoints.team2[progressSet] == 6 && $scope.setPoints.team1[progressSet] < 5) {
                $scope.setWon(2);
                return true;
            }

            //if 2 points are difference
            if ($scope.setPoints.team1[progressSet] > 6 && ($scope.setPoints.team1[progressSet] - $scope.setPoints.team2[progressSet]) > 1) {
                $scope.setWon(1);
                return true;
            }
            if ($scope.setPoints.team2[progressSet] > 6 && ($scope.setPoints.team2[progressSet] - $scope.setPoints.team1[progressSet]) > 1) {
                $scope.setWon(2);
                return true;
            }

            //if ($scope.setPoints.team1[progressSet] == 6 && $scope.setPoints.team2[progressSet] == 6)
            //    $scope.tieBreakEnabled = true;


            //if($scope.setPoints.team2[progressSet] == 5 && $scope.setPoints.team1[progressSet] == 5)
            //    $scope.tieBreakEnabled = true;


            return false;

        };

        $scope.setWon = function (team) {
            //call service
            $http({
                method: 'POST',
                url: "/match/save-set-win",
                data: {
                    'team': team,
                    'team1_score': $scope.setPoints.team1[$scope.setInProgress - 1],
                    'team2_score': $scope.setPoints.team2[$scope.setInProgress - 1],
                    'match_id': $scope.match_id,
                    'set_in_progress': $scope.setInProgress,
                    //'tie_breaks'
                }
            });

            var finished = $scope.checkIfMatchIsFinished();

            if (!finished)
                $scope.setInProgress = $scope.setInProgress + 1;
        };

        $scope.checkIfMatchIsFinished = function () {
            //* 0 - Standard (3 set)
            //* 1 - Grand Slam (5 set)

            if (($scope.match_rule == 0 && $scope.setInProgress == 3) ||
                ($scope.match_rule == 1 && $scope.setInProgress == 5)) {
                $scope.show_popup = 1;
                return true;
            }
            return false;

        };
        $scope.gameWon = function (team, team1_score, team2_score) {
            //TODO: SET TIEBREAK WIN
            var setWon = $scope.processSetPoint(team);
            $scope.removing = 0;
            $scope.serving = $scope.serving == 1 ? 2 : 1;

            if (!setWon) {
                $scope.game_number = $scope.game_number + 1;
                $http({
                    method: 'POST',
                    url: "/match/game-win",
                    data: {
                        'gamescore': $scope.gamescore,
                        'match_id': $scope.match_id,
                        'team_ids': $scope.team_ids,
                        'team1_score': team1_score,
                        'team2_score': team2_score,
                        'game_won': team,
                        'game_score_id': $scope.game_score_id,
                        'game_score': $scope.gamescore,
                        'game_number': $scope.game_number,
                        'set_number': $scope.setInProgress,
                        'served_by': $scope.serving,
                        'setpoints_team1': $scope.setPoints.team1[$scope.setInProgress - 1],
                        'setpoints_team2': $scope.setPoints.team2[$scope.setInProgress - 1]
                    }
                }).success(function (data) {
                    if (data && data.success) {
                        $scope.game_score_id = data.game_score_id;
                    }
                })


            }
            if (setWon) {
                if ($scope.tieBreakEnabled) {
                    $scope.tiePoints.team1 = 0;
                    $scope.tiePoints.team2 = 0;
                    $scope.tieBreakEnabled = false;
                }
            }


            //if ($scope.team1_score == 6 && $scope.team2_score == 6) {
            //    $scope.tieBreakEnabled = true;
            //}


            $scope.gamescore.currentScoreTeam1 = 0;
            $scope.gamescore.currentScoreTeam2 = 0;
            $scope.showTiebreakScore();
            $scope.showScore();
        };

        $scope.showScore = function () {
            if ($scope.gamescore.currentScoreTeam1 == undefined)
                $scope.gamescore.currentScoreTeam1 = 0;
            if ($scope.gamescore.currentScoreTeam2 == undefined)
                $scope.gamescore.currentScoreTeam2 = 0;

            var score11 = $scope.scoreValues[$scope.gamescore.currentScoreTeam1].charAt(0);
            var score12 = $scope.scoreValues[$scope.gamescore.currentScoreTeam1].charAt(1);
            var score21 = $scope.scoreValues[$scope.gamescore.currentScoreTeam2].charAt(0);
            var score22 = $scope.scoreValues[$scope.gamescore.currentScoreTeam2].charAt(1);
            $scope.gamescore.currentScoreTeam11 = score11 != 'u' ? score11 : '0';
            $scope.gamescore.currentScoreTeam12 = score12 != 'u' ? score12 : '0';

            $scope.gamescore.currentScoreTeam21 = score21 != 'u' ? score21 : '0';
            $scope.gamescore.currentScoreTeam22 = score22 != 'u' ? score22 : '0';
        };

        $scope.showTiebreakScore = function () {

            var progressSet = $scope.setInProgress - 1;

            if ($scope.tiePoints.team1[progressSet] == undefined)
                $scope.tiePoints.team1[progressSet] = 0;
            if ($scope.tiePoints.team2[progressSet] == undefined)
                $scope.tiePoints.team2[progressSet] = 0;

            $scope.gamescore.currentScoreTeam11 = leftPad($scope.tiePoints.team1[progressSet], 2).charAt(0);
            $scope.gamescore.currentScoreTeam12 = leftPad($scope.tiePoints.team1[progressSet], 2).charAt(1);

            $scope.gamescore.currentScoreTeam21 = leftPad($scope.tiePoints.team2[progressSet], 2).charAt(0);
            $scope.gamescore.currentScoreTeam22 = leftPad($scope.tiePoints.team2[progressSet], 2).charAt(1);
        };

        $scope.playing_counter = function () {
            from = new Date($scope.match.match_started_at.replace(/-/g, "/"))
            $interval(function () {
                playing_time = new Date() - from;
                var hours = Math.floor(playing_time / 1000 / 60 / 60);
                var minutes = Math.floor(((playing_time / 1000) - hours * 60 * 60) / 60);
                var seconds = Math.floor((playing_time / 1000) - hours * 60 * 60 - minutes * 60);
                var duration = leftPad(hours - 1, 2) + ':' + leftPad(minutes, 2) + ':' + leftPad(seconds, 2);
                $scope.counter = duration;
            }, 1000);

        };

        $scope.finishSet = function () {
            $scope.setInProgress = $scope.setInProgress + 1;
            $scope.game_number = 1;
            $scope.gamescore.currentScoreTeam1 = 0;
            $scope.gamescore.currentScoreTeam2 = 0;
            $scope.showScore();

            $http({
                method: 'POST',
                url: "/match/manual-finish-set",
                data: {
                    'match_id': $scope.match_id,
                    'new_set': $scope.setInProgress,
                    'game_number': $scope.game_number,
                }
            });
        };

        $scope.finishMatch = function () {

            var msg = {
                'gamescore': $scope.gamescore,
                'setpoints': $scope.setPoints,
                'tiebreak': $scope.tiePoints,
                'match_id': $scope.match_id,
                'set_in_progress' : $scope.setInProgress,
                'tie_break' : $scope.tieBreakEnabled,
                'completed' : true
            };
            app.BrainSocket.message('live_score_app_mode.changed', msg);

            $scope.finished_at = new Date();
            $http({
                method: 'POST',
                url: "/match/finish-match",
                data: {
                    'match_id': $scope.match_id
                }
            }).success(function (data) {
                $scope.finished_at = data.finished_at;
            });
            $scope.show_popup = 0;
            $scope.matchStatus = 4;
            $scope.calculatePlayingTimeWhenFinished();
            //console.log($scope.gamescore);
        };

        $scope.calculatePlayingTimeWhenFinished = function () {
            var from = new Date($scope.finished_at.replace(/-/g, "/"))
            var to = new Date($scope.match.match_started_at.replace(/-/g, "/"))
            var playing_time = from - to;
            var hours = Math.floor(playing_time / 1000 / 60 / 60);
            var minutes = Math.floor(((playing_time / 1000) - hours * 60 * 60) / 60);
            var seconds = Math.floor((playing_time / 1000) - hours * 60 * 60 - minutes * 60);
            var duration = leftPad(hours, 2) + ':' + leftPad(minutes, 2) + ':' + leftPad(seconds, 2);
            $scope.counter = duration;
        };
    });
})();