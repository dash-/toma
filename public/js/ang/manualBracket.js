(function () {

    var manualBracketModule = angular.module('manualBracketApp', ['Application', 'ui.bootstrap', 'ngDragDrop']);

    manualBracketModule.filter("br2comma", function () {
        return function (input) {
            return input.replace(/<br\s*\/?>/mg, ", ");
            // return input.replace(/(\r\n|\n|\r)/gm, "<br>");
        }
    });

    manualBracketModule.controller('DragController', function ($scope, $http, $window, $attrs) {
        $scope.formData = {};
        $scope.formData.player = [];
        $scope.query_params_help = '';
        $scope.formData.listType = [];
        $scope.listType = 2;
        $scope.disabledSubmit = false;
        $scope.fetchUrl = $attrs.scurl;
        $scope.byeList = {
            'id': 0,
            'name': 'BYE',
            'drag': true,
            'seed_num': "",
            'hidden': true
        };

        $scope.quali = {
            'id': -1,
            'name': 'Q',
            'drag': true,
            'seed_num': "",
            'hidden': true
        };
        $scope.quali_ll = {
            'id': -2,
            'name': 'Q/LL',
            'drag': true,
            'seed_num': "",
            'hidden': true
        };

        $scope.ll = {
            'id': -3,
            'name': 'LL',
            'drag': true,
            'seed_num': "",
            'hidden': true
        };

        $scope.list1 = {};
        $scope.list2 = [];

        $scope.init = function () {
            $http({
                method: 'GET',
                url: $scope.fetchUrl
            }).success(function (response) {
                if (!response.match_exist)
                    $scope.list1 = response.data;
                else {
                    $scope.list2 = response.data;
                    $scope.formData.player = $scope.generateNewData();
                }

                $scope.list_type = response.list_type;
                $scope.positions = response.positions;
                $scope.automatic_draw_disabled = response.automatic_draw_disabled;

                for (var i = 0; i < response.total; i++) {
                    if (!response.match_exist)
                        $scope.list2.push({});
                    else {
                        $scope.list1 = [];
                        $scope.list1.push({});
                    }
                }
                if ($scope.list1 != undefined) {
                    $scope.list1.splice(0, 0, $scope.byeList);
                }

                $scope.show_quali_options = response.show_quali_options;

                if ($scope.list_type == 1 && response.show_quali_options) {
                    $scope.list1.splice(1, 0, $scope.quali);
                    $scope.list1.splice(2, 0, $scope.quali_ll);
                    $scope.list1.splice(3, 0, $scope.ll);
                }

                if (response.new_players != undefined && response.new_players.length) {
                    angular.forEach(response.new_players, function (val, key) {
                        $scope.list1.push(val);
                    });
                }

                setTimeout(function () {
                    $(".thirty").tinyscrollbar();
                }, 500);

            }).error(function (error) {
                console.log(error);
            });

            return false;
        };

        $scope.automaticDraw = function (href) {
            var queryParams = [];

            angular.forEach($scope.list1, function (val, key) {
                if (val.seed_num && val.seed_num != 0)
                    queryParams[val.id] = val.seed_num;
            });

            if($scope.list1.length)
                $scope.query_params_help = queryParams;
            else if($scope.query_params_help)
                queryParams = $scope.query_params_help;

            $http({
                method: 'GET',
                url: href,
                params: queryParams
            }).success(function (response) {
                $scope.list1 = [];
                $scope.list2 = response.data;
                $scope.list_type = response.list_type;
                $scope.generateNewData();
            }).error(function (error) {
                console.log(error);
            });
            return false;
        };


        $scope.onChanged = function (index, position) {
            $scope.list2[position.id] = $scope.list1[index];
            $scope.list1.splice(index, 1);
            $scope.positions.splice($scope.positions.indexOf(position), 1);

            $scope.generateNewData();

        };

        $scope.dropCallback = function (event, ui, index, item) {
            var errorHappened = false;
            if ($scope.formData.player[index] != null) {
                if (index % 2 == 0 && item['id'] == 0 && $scope.formData.player[index + 1]['id'] == 0) {
                    $scope.list2.splice(index, 1);
                    errorHappened = true;
                }


                if (index % 2 != 0 && item['id'] == 0 && $scope.formData.player[index - 1]['id'] == 0) {
                    $scope.list2.splice(index, 1);
                    errorHappened = true;
                }

            }
            if (!errorHappened) {
                var result = $.grep($scope.positions, function (e) {
                    return e.id == index;
                });

                $scope.positions.splice($scope.positions.indexOf(result[0]), 1);
            }
            $scope.generateNewData();
            $scope.clearExcessByes();
        };

        $scope.clearExcessByes = function () {
            angular.forEach($scope.list1, function (val, key) {
                if (key > 0 && val['id'] == 0)
                    $scope.list1.splice(key, 1);
            });
        }

        $scope.generateNewData = function () {
            $scope.formData.player = [];

            angular.forEach($scope.list2, function (val, key) {

                $scope.formData.player.push({
                    'id': val.id,
                    'seed_num': val.seed_num,
                    'move_from_qualifiers_type': val.move_from_qualifiers_type
                });
            });

            return $scope.formData.player;
        };

        $scope.toObject = function (arr) {
            var rv = {};
            for (var i = 0; i < arr.length; ++i)
                if (arr[i] !== undefined) rv[i] = arr[i];
            return rv;
        };

        $scope.shuffle = function (object) {

            // if last element of list2 array is only jquiposition remove it and push empty object
            if (Object.keys($scope.list2[$scope.list2.length - 1]).length < 2) {
                $scope.list2.pop();
                $scope.list2.push({});
            }

            var quali_options_checker = ($scope.list_type == 1 && $scope.show_quali_options);
            // remove bye from list1 array
            var bye_element = (quali_options_checker) ? object.splice(0, 4) : object.shift();

            // filter list1 array from empty fields
            object = object.filter(function (n) {
                return Object.keys(n).length > 1
            });

            // shuffle object(list1) array
            for (var j, x, i = object.length; i; j = Math.floor(Math.random() * i), x = object[--i], object[i] = object[j], object[j] = x);

            // iterate list1 array and populate empty fields
            angular.forEach($scope.list1, function (value, key) {
                if (Object.keys($scope.list2[key]).length == 0) {
                    $scope.list2[key] = object[key];
                } else {
                    for (var i = 0; i < $scope.list2.length; i++) {
                        if (Object.keys($scope.list2[i]).length == 0) {
                            $scope.list2[i] = object[key];
                            break;
                        }
                    }
                    ;
                }
            });

            $scope.list1 = angular.copy([]);

            if (quali_options_checker) {
                angular.forEach(bye_element, function (val, key) {
                    $scope.list1.push(val);
                });
            } else {
                $scope.list1.push(bye_element);
            }
            $scope.generateNewData();
            return object;
        };

        $scope.manualSubmit = function ($event, requestType, message, action, redirect, listType) {
            redirect = redirect || false;
            action = action || $event.target.action;
            $scope.formData.listType.push(listType);

            if ($scope.disabledSubmit == false) {
                $scope.disabledSubmit = true;
                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        $.Notify({
                            caption: "Error",
                            content: response.message,
                            style: {background: '#FF0000', color: '#fff'}
                        });

                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                        $scope.disabledSubmit = false;
                    } else {
                        if (redirect) {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });

                            setTimeout(function () {
                                $window.location = response.redirect_to;
                            }, 1000);

                        } else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
            }
            $event.preventDefault();
            return false;
        };

    });
})();