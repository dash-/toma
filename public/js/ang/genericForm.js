(function () {

    var formAppModule = angular.module('genericFormApp', ['Application', 'ui.bootstrap', 'ui.select2', 'FormErrors'])
        .config(function (datepickerConfig) {
            datepickerConfig.showWeeks = false;
        });

    formAppModule.filter('to_trusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);

    formAppModule.directive('formatdate', function (dateFilter) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (viewValue) {
                    return dateFilter(viewValue, 'yyyy-MM-dd');
                });

            }
        }
    });

    formAppModule.directive('validSubmit', ['$parse', function ($parse) {
        return {
            require: '^form',
            link: function (scope, element, iAttrs, controller) {
                var form = element.controller('form');
                form.$submitted = false;
                var fn = $parse(iAttrs.validSubmit);

                element.on('submit', function (event) {
                    scope.$apply(function () {
                        element.addClass('ng-submitted');
                        form.$submitted = true;
                        $(".has-error-container").css("right", $("fieldset").position().left);
                        if (form.$valid) {
                            fn(scope, {$event: event});
                            form.$submitted = false;
                        }
                    });
                });
            }
        };
    }
    ]);

    formAppModule.controller('FormController', function ($scope, $http, $window) {
        $scope.formData = {};
        $scope.showWeeks = false;
        $scope.errors = [];
        $scope.minDate2 = "";
        // $scope.playerCreation = false;


        $scope.loadData = function (team) {
            if (team) {
                $scope.formData.contact = {};
                $http({
                    method: 'GET',
                    url: '/players/pdata/' + team
                }).success(function (response) {
                    var data = response.data;
                    $scope.formData.contact.name = data.name;
                    $scope.formData.contact.surname = data.surname;
                    $scope.formData.contact.date_of_birth = data.date_of_birth;
                    $scope.formData.contact.email = data.email;
                    $scope.formData.contact.phone = data.phone;

                }).error(function (error) {
                    console.log(error);
                });
            }
        }

        $scope.$watch("formData.data.club_id", function () {

            if ($scope.formData.data && $scope.formData.data.club_id) {

                $http({
                    method: 'GET',
                    url: '/clubs/region-licences/' + $scope.formData.data.club_id
                }).success(function (response) {
                    var message = response.message;
                    $scope.licence_range = message;


                }).error(function (error) {
                    console.log(error);
                });
            }
        });
        $scope.$watch("formData.date.qualifying_date_to", function () {
            current_data = $scope.formData.date;
            if (current_data !== undefined) {
                var moreDay = new Date();
                var act = new Date(current_data.qualifying_date_to);
                moreDay.setDate(act.getDate() + 1);
                $scope.minDate2 = moreDay.format("yyyy-mm-dd");
            }
        });

        $scope.toggleMin = function () {
            $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();


        $scope.formSubmit = function ($event, requestType, message, action, redirect, plRedirect) {

            redirect = redirect || false;
            action = action || $event.target.action;
            plRedirect = plRedirect || false;

            if ($scope.editForm.$valid) {
                $scope.showErrors = false;
            } else {
                $scope.showErrors = true;
                $('.form-errors').show();
            }

            if ($scope.editForm.$valid) {
                var referee_id_real = $('#referee_id_real').val();
                if (referee_id_real == '' || referee_id_real == undefined)
                    referee_id_real = 0;

                if ($('#referee_id_real').val() !== undefined) {
                    $scope.formData.tournament.referee_id_real = referee_id_real;
                }

                $http({
                    method: requestType,
                    url: action,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        //console.log(response.success);
                        $scope.errors = [];
                        angular.forEach(response, function (d, key) {
                            $scope.errors.push(d[0]);
                        });
                        $scope.errorState = 'error-state';
                        if (response.show_message == true) {
                            $.Notify({
                                caption: "Warning",
                                content: response.message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    } else {
                        if (redirect) {
                            $window.location.href = response.redirect_to;
                        } else if (plRedirect) {
                            loadAjax(response.redirect_to, '');
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        } else {
                            $.Notify({
                                caption: "Success",
                                content: message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }

                    }
                });
            }
            $event.preventDefault();
            return false;

        }
        $scope.opened = [];

        $scope.closeAll = function () {
            $scope.opened.mainFrom = false;
            $scope.opened.mainTo = false;
            $scope.opened.qualifyingFrom = false;
            $scope.opened.qualifyingTo = false;
            $scope.opened.entryDeadline = false;
            $scope.opened.withdrawalDeadline = false;
        };


        $scope.openDatePicker = function ($event, id) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.closeAll();
            $scope.opened[id] = true;
        };

        // $scope.manualData = [];
        $scope.listType = 2;


        $scope.toObject = function (arr) {
            var rv = {};
            for (var i = 0; i < arr.length; ++i)
                if (arr[i] !== undefined) rv[i] = arr[i];
            return rv;
        };

        $scope.manualSubmit = function ($event, requestType, message, action, redirect) {
            redirect = redirect || false;
            action = action || $event.target.action;

            // console.log($scope.formData);

            // dataToSend = [];
            // // dataToSend = {player_ids: $scope.manualData, list_type: $scope.listType};

            // angular.forEach($scope.manualData, function(value, key) {
            //    dataToSend.push({player_id: key, round_num: value});
            // });

            // dataToSend.push({list_type:$scope.listType});

            // test = $scope.toObject(dataToSend);


            $http({
                method: requestType,
                url: action,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    $scope.errors = [];
                    angular.forEach(response, function (d, key) {
                        $scope.errors.push(d[0]);
                    });
                    $scope.errorState = 'error-state';
                } else {
                    if (redirect) {
                        loadAjax(response.redirect_to, '');
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    } else {
                        $.Notify({
                            caption: "Success",
                            content: message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }

                }
            });
            $event.preventDefault();
            return false;
        };

    });

    formAppModule.controller("InvoiceController", function ($scope, $http) {
        $scope.invoice = {};
        $scope.formData = {
            invoice: ''
        };

        $scope.init = function (invoice_id) {
            $http({
                method: 'GET',
                url: '/invoices/data/' + invoice_id
            }).success(function (response) {
                $scope.invoice = response.data;
            }).error(function (error) {
                console.log(error);
            });

            return false;
        }

        $scope.formSubmit = function ($event, invoice_id) {
            if ($scope.formData.invoice != '') {
                $http({
                    method: 'PUT',
                    url: '/invoices/update/' + invoice_id,
                    data: $.param($scope.formData),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (!response.success) {
                        angular.forEach(response, function (d, key) {
                            console.log(d + '   ' + key);
                            $scope['error_' + key] = d[0];
                        });
                        $scope.errorState = 'error-state';
                        if (response.show_message == true) {
                            $.Notify({
                                caption: "Warning",
                                content: response.message,
                                style: {background: '#16499A', color: '#fff'}
                            });
                        }
                    } else {
                        $scope.invoice = response.data;
                        $scope.formData.invoice = angular.copy('');
                        $.Notify({
                            caption: "Success",
                            content: response.message,
                            style: {background: '#16499A', color: '#fff'}
                        });
                    }
                });
            }
        };
    });

    formAppModule.controller("TypeEditController", function ($scope, $http) {
        $scope.formData = {};

        $scope.typeSubmit = function ($event, href) {
            $http({
                method: 'PUT',
                url: href,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    console.log(response);
                } else {
                    $.Notify({
                        caption: "Success",
                        content: response.message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    loadAjax(response.redirect_to, '');
                }
            });
        };
    });

    formAppModule.controller("SettingsController", function ($scope, $http) {
        $scope.formData = {};

        $scope.formSubmit = function ($event, href) {
            $http({
                method: 'PUT',
                url: href,
                data: $.param($scope.formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (!response.success) {
                    console.log(response);
                } else {
                    $.Notify({
                        caption: "Success",
                        content: response.message,
                        style: {background: '#16499A', color: '#fff'}
                    });
                    loadAjax(response.redirect_to, '');
                }
            });
        };
    });
})();