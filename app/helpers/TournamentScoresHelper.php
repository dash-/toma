<?php

class TournamentScoresHelper
{

    public static function getScores($tournament_slug)
    {
        $scores = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.team1_data_array', 'match.team2_data_array', 'match.scores', 'tournament')
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })->whereHas('tournament', function ($query) use ($tournament_slug) {
                $query->whereSlug($tournament_slug);
            })->get();
        $result = [];
        foreach ($scores as $key => $score) {

            $item['match_id'] = $score->match_id;
            $score_string = '';

            foreach ($score->match->scores as $fresult) {
                $score_string .= $fresult->set_team1_score . '' . \DrawMatchHelper::matchTie($fresult->set_team1_tie) . '-' . $fresult->set_team2_score . \DrawMatchHelper::matchTie($fresult->set_team2_tie) . '; ';
            }

            $item['score'] = $score_string;

            $item['team1_name'] = ($score->match->team1_data) ?
                substr($score->match->team1_data->name, 0, 1) . '. ' . \DrawMatchHelper::shorten($score->match->team1_data->surname)
                : '';

            $item['team2_name'] = ($score->match->team2_data) ? substr($score->match->team2_data->name, 0, 1) . '. ' . \DrawMatchHelper::shorten($score->match->team2_data->surname) : '';

            $item['date_of_play'] = DateTimeHelper::GetShortDateFullYear($score->date_of_play);
            $item['date_time'] = DrawMatchHelper::getStartingDateTime($score);
            $item['order_of_play'] = $score->order_of_play;
            $item['court_number'] = $score->court_number;


            $result[$score->match_status][] = $item;
        }

        // check results count
        if (count($result)) {

            krsort($result);

            $first = (isset($result[3])) ? $result[3] : null;
            unset($result[3]);

            $result = [3 => $first] + $result;

        }

        return new Illuminate\Support\Collection($result);
    }

    public static function getStatusByKey($key)
    {
        $statuses = [
            0 => LangHelper::get('not_started', 'Not started'),
            1 => LangHelper::get('to_be_played', 'To be played'),
            2 => LangHelper::get('delayed', 'Delayed'),
            3 => LangHelper::get('in_progress', 'In progress'),
            4 => LangHelper::get('completed', 'Completed'),
        ];

        return $statuses[$key];
    }

    public static function getRefereeLiveScoreAssignedTournaments($tournament_id = '')
    {
        $tournaments = Tournament::select('id')->whereRefereeId(Auth::user()->id)->get();
        $in_draw_ids = [];
        $f_draw_ids = [];
        $not_draw_ids = [];
        $tournament_ids = (count($tournaments)) ? array_pluck($tournaments, 'id') : array(0);

        $in_progress = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereIn('tournament_id', $tournament_ids)
            ->where('match_status', 3)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();

        $in_progress = $in_progress->filter(function ($item) {
            if ($item->match->team1_id > 0 OR $item->match->team2_id > 0)
                return true;
        });

        foreach ($in_progress as $in) {
            $in_draw_ids[] = $in->match->id;
        }

        $in_progress_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
        if (count($in_draw_ids))
            $in_progress_matches = $in_progress_matches->whereIn('draw_id', $in_draw_ids);

        $in_progress_matches = $in_progress_matches->get();

        $finished = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereIn('tournament_id', $tournament_ids)
            ->where('match_status', 4)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();

        $finished = $finished->filter(function ($item) {
            if ($item->match->team1_id > 0 OR $item->match->team2_id > 0)
                return true;
        });

        foreach ($finished as $f) {
            $f_draw_ids[] = $f->match->id;
        }

        $finished_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
        if (count($f_draw_ids))
            $finished_matches = $finished_matches->whereIn('draw_id', $f_draw_ids);

        $finished_matches = $finished_matches->get();

        $not_started = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereIn('tournament_id', $tournament_ids)
            ->where('match_status', 0)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();

        $not_started = $not_started->filter(function ($item) {
            if ($item->match->team1_id > 0 OR $item->match->team2_id > 0)
                return true;
        });

        foreach ($not_started as $not) {
            $not_draw_ids[] = $not->match->id;
        }

        $not_started_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
        if (count($not_draw_ids))
            $not_started_matches = $not_started_matches->whereIn('draw_id', $not_draw_ids);

        $not_started_matches = $not_started_matches->get();


        return array($in_progress, $finished, $not_started, $in_progress_matches, $finished_matches, $not_started_matches);
    }

    /**
     * generate score data in string for live scores carousel on index page
     * @param $score
     * @return string
     */
    public static function getLiveScoreStatus($score)
    {
        $score_string = [];
        if (count($score->match->scores)) {

            foreach ($score->match->scores as $result) {
                $score_string[] = $result->set_team1_score . DrawMatchHelper::matchTie($result->set_team1_tie)
                    . '-' . $result->set_team2_score . DrawMatchHelper::matchTie($result->set_team2_tie);
            }
        } else {
            $score_string[] = DrawMatchHelper::getStartingDateTime($score);
        }


        return implode('; ', $score_string);

    }

    public static function getBracketScoresTeam1($match_id)
    {
        $scores = MatchScore::where('match_id', $match_id)
            ->get();
        $match = MatchSchedule::with('match')->where('match_id', $match_id)->first();

        $violation = '';
        if ($match) {
            $violation = MatchViolation::where('match_id', $match_id)
                ->where('draw_id', $match->draw_id)
                ->where('team_id', $match->match->team_1->id)
                ->first();
        }
        $type = '';
        if ($violation)
            $type = $violation->type_of_violation;
        $result = '';
        $result2 = '';

        if ($type == null) {
            foreach ($scores as $score) {
                $result .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . ' ';
            }

        } else {
            $result = $match->match->match_status();
        }


        if ($type == 3) {
            foreach ($scores as $score) {
                $result2 .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . ' ';
            }
            $result = $result2 . ' - ' . $match->match->match_status();
        }


        return $result;
    }

    public static function getBracketScoresTeam2($match_id)
    {
        $scores = MatchScore::where('match_id', $match_id)
            ->get();
        $match = MatchSchedule::where('match_id', $match_id)->first();


        $violation = '';
        if ($match) {
            $violation = MatchViolation::where('match_id', $match_id)
                ->where('draw_id', $match->draw_id)
                ->where('team_id', $match->match->team2->id)
                ->first();
        }

        $type = '';
        if ($violation)
            $type = $violation->type_of_violation;

        $result = '';
        $result2 = '';

        if ($type == null) {
            foreach ($scores as $score) {
                $result .= $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . ' ';
            }
        } else
            $result = $match->match->match_status();


        if ($type == 3) {
            foreach ($scores as $score) {
                $result2 .= $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . ' ';
            }
            $result = $result2 . ' - ' . $match->match->match_status();
        }


        return $result;
    }

    public static function getOrderOfPlayScores($match_id)
    {

        $scores = MatchScore::where('match_id', $match_id)
            ->get();
        $match = MatchSchedule::where('match_id', $match_id)->first();

        $violation = MatchViolation::where('match_id', $match_id)
            ->where('draw_id', $match->draw_id)
            ->first();

        $type = '';
        $result_team1 = '';
        $result_team2 = '';
        $show_score = '';

        if ($violation) {
            $type = $violation->type_of_violation;
            $violating_team = $violation->team_id == $match->match->team1->id ? $match->match->team1->id : $match->match->team2->id;

        }

        if ($type == null) {
            foreach ($scores as $score) {
                $result_team1 .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . ' ';
                $result_team2 .= $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . ' ';
            }
        }

        if ($type == 3 && $violating_team == $match->match->team1->id) {

            $result_team1_retired = '';
            $result_team2_retired = '';

            foreach ($scores as $score) {
                $result_team1_retired .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . ' ';
                $result_team2_retired .= $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . ' ';
            }
            $result_team1 = $result_team1_retired . ' - ' . $match->match->match_status();
            $result_team2 = $result_team2_retired;
        } else if ($type == 3 && $violating_team == $match->match->team2->id) {

            $result_team1_retired = '';
            $result_team2_retired = '';

            foreach ($scores as $score) {
                $result_team1_retired .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . ' ';
                $result_team2_retired .= $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . ' ';
            }

            $result_team1 = $result_team1_retired;
            $result_team2 = $result_team2_retired . ' - ' . $match->match->match_status();

        } else if ($type != null && $type != 3 && $violating_team == $match->match->team1->id) {
            $result_team1 = $match->match->match_status();
            $result_team2 = ' - ';
        } else if ($type != null && $type != 3 && $violating_team == $match->match->team2->id) {
            $result_team1 = ' - ';
            $result_team2 = $match->match->match_status();
        }


        $show_score = '<div class="yellow-color order-of-play-preview">' . $result_team1 . '</div>
                        <span class="small">vs</span>
                        <div class="yellow-color order-of-play-preview">' . $result_team2 . '</div>';

        return $show_score;

    }


}