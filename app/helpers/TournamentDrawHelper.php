<?php

class TournamentDrawHelper
{

    public static function saveDraw($pairs, $tournament_id)
    {
        foreach ($pairs as $key => $pair) {
            $match_id = ++$key;
            foreach ($pair as $p) {
                $draw = new TournamentDraw;
                $draw->match_id = $match_id;
                $draw->level_id = 1;
                $draw->tournament_id = $tournament_id;
                $draw->player_id = $p['id'];
                $draw->save();
            }
        }
    }

    public static function jsonData($tournament_id)
    {
        $draws = TournamentDraw::with('size', 'qualification', 'category', 'signups', 'tournament.date')
            ->where('tournament_id', '=', $tournament_id)
            ->get()
            ->toArray();
        return $draws;
    }

    public static function jsonLastRow($tournament_id)
    {
        $draws = TournamentDraw::with('size', 'qualification', 'category')
            ->where('tournament_id', '=', $tournament_id)
            ->orderBy('tournament_draws.id', 'desc')
            ->first()
            ->toArray();
        return $draws;
    }


    /**
     * @param $draw_id int
     *            id of user draw
     *
     * @return returns (array) of strings or empty array
     */
    public static function getBracketStatusReport($draw_id)
    {
        $submitted_report = DrawHistory::with("user")
            ->where('draw_id', $draw_id)
            ->get();

        $reports = $submitted_report->map(function ($report) {
            $return = "<p class='yellow-color'>";
            if ($report->list_type == 2)
                $return .= LangHelper::get('qualifying_draw_submitted_on', 'Qualifying draw submitted on');
            else
                $return .= LangHelper::get('main_draw_submitted_on', 'Main draw submitted on');

            return $return .= ': ' . DateTimeHelper::GetFullDateTime($report->created_at) . ' ' . LangHelper::get('by', 'by') . ' ' . $report->user->email . "</p>";
        })->reverse()->toArray();

        return implode("", $reports);
    }

    public static function sortSignupsByTeams($signups)
    {

        return $signups->sortBy(function ($signup) {
            $team = $signup->teams->first();
            if (!is_null($team)) {
                if (Request::query("sort") && Request::query("or"))
                {
                    if(Request::query("sort")=="name")
                        return $team->name;
                    if(Request::query("sort")=="surname")
                        return $team->surname;
                    if(Request::query("sort")=="licence_number")
                        return $team->licence_number;
                    if(Request::query("sort")=="date_of_birth")
                        return $team->date_of_birth;
                    if(Request::query("sort")=="ranking")
                        return $team->ranking;
//                    if (Request::query("sort") && Request::query("or"))
//                        $signups = $signups->sortBy(Request::query("sort"), null, Request::query("or")=="desc");
                }

                return $team->name;
            }
        }, null, Request::query("or")=="desc");
    }

    public static function sortSignupsByRankingPoints($signups, $draw_type = 'singles')
    {
        return $signups->sortByDesc(function ($signup) use ($draw_type) {
            if ($draw_type == 'singles') {
                $team = $signup->teams->first();
                if (!is_null($team))
                    return $team->ranking_points;
            } else {
                $team = $signup->teams;
                $team1 = $team[0];
                $team2 = $team[1];
                return ($team1->ranking_points > $team2->ranking_points)
                    ? $team1->ranking_points : $team2->ranking_points;
            }
        });
    }

    public static function countDrawsInSignups($tournaments)
    {
        $count = 0;
        foreach ($tournaments->get() as $tournament) {
            $count += $tournament->draws->count();
        }

        return $count;
    }

    public static function drawStatus(TournamentDraw $draw, $draws_with_matches_list, $entry_deadline, $draw_history)
    {
        if (!$entry_deadline) {
            $entry_deadline = $draw->tournament->date->qualifying_date_from;
        }

        if (TournamentHelper::mainDrawFinished($draw->id, $draw_history)) {
            if ($draw->status != 5) {
                $draw->status = 5;
                $draw->save();
            }
        } else if (TournamentHelper::drawHasMatches($draw->id, 1, $draws_with_matches_list)) {
            if ($draw->status != 4) {
                $draw->status = 4;
                $draw->save();
            }
        } else if (TournamentHelper::drawHasMatches($draw->id, 2, $draws_with_matches_list)) {
            if ($draw->status != 3) {
                $draw->status = 3;
                $draw->save();
            }
        } else if (strtotime($entry_deadline) < time()) {
            if ($draw->status != 2) {
                $draw->status = 2;
                $draw->save();
            }
        } else {
            if ($draw->status != 1) {
                $draw->status = 1;
                $draw->save();
            }
        }


        return $draw->status();
    }

    public static function getDrawsWithMatches()
    {
        //SELECT DISTINCT draw_matches.draw_id, draw_matches.list_type FROM tournament_draws JOIN draw_matches ON draw_matches.draw_id = tournament_draws.id
        return TournamentDraw::join('draw_matches', 'draw_matches.draw_id', '=', 'tournament_draws.id')
            ->distinct()
            ->get(array('draw_matches.draw_id', 'draw_matches.list_type'));
    }

    public static function getMatchRule($fordropdown = false)
    {
        $first_element = $fordropdown ? null : false;
        $setup = [
            $first_element => LangHelper::get("standard_3_set", "Standard (3 set)"),
            1 => LangHelper::get("grand_slam_5_set", "Grand Slam (5 set)"),
        ];
        return $setup;
    }

    public static function getSetRule($fordropdown = false)
    {
        $first_element = $fordropdown ? null : false;
        $setup = [
            $first_element => LangHelper::get("standard_6_games", "Standard (6 games, tie-break at 6:6)"),
            1 => LangHelper::get("short_4_games", "Short Set (4 games, tie-break at 4:4)"),
        ];
        return $setup;
    }

    public static function setGameRule($fordropdown = false)
    {
        $first_element = $fordropdown ? null : false;
        $setup = [
            $first_element => LangHelper::get("standard", "Standard"),
            1 => LangHelper::get("no_Ad", "no_Ad"),
        ];
        return $setup;
    }

    public static function tournamentDrawPrice($draw_category_id, $draw_prize, $number_of_players)
    {
        if (!$number_of_players)
            return ['price' => 0, 'coefficient' => 0];


        if ($draw_prize == 0)
            $coefficient = 'premia';
        else if ($draw_prize > 0 && $draw_prize <= 3000)
            $coefficient = 'premia_0_3000';

        else if ($draw_prize >= 3001 && $draw_prize < 6000)
            $coefficient = 'premia_3000_6000';

        else if ($draw_prize >= 6001)
            $coefficient = 'premia_6001';


        $query = TournamentPerPlayerPrice::select($coefficient)->where('draw_category_id', $draw_category_id)->first();
        $price = number_format((float)($number_of_players * $query[$coefficient]), 2, '.', '');;

        return array('price' => $price, 'coefficient' => $query[$coefficient]);
    }
}