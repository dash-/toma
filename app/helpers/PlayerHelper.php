<?php

/**
 * Created by PhpStorm.
 * User: damirseremet
 * Date: 28/10/14
 * Time: 19:09
 */
class PlayerHelper
{

    public static function getTournamentsPerWeekForDisplay($when, $what, $no_ending = false)
    {
        $tournament_ids = self::getTournamentsPerWeek($when, $what, $no_ending);

        $t_ids = array_flatten($tournament_ids->toArray());

        if ($t_ids) {
            $tournaments = Tournament::leftJoin('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->whereIn("tournaments.id", $t_ids)->get();

            return $tournaments;
        }

    }

    public static function getTournamentsPerWeek($when, $what, $no_ending = false)
    {
        $from_date = new DateTime('monday ' . $when);
        $to_date = new DateTime('sunday ' . $when);

        // fallback because DateTime have problems with time zones, first day of week is sunday
        if (in_array($when, ['+1 week', '+2 weeks']))
            $to_date->add(new DateInterval('P1W'));


        return self::filteredTournament($from_date, $to_date, $what, $no_ending);
    }

    private static function filteredTournament($from_date, $to_date, $what, $no_ending = false)
    {
        $player_age = Auth::getUser()->getPlayerAge();
        $player_age = DateTimeHelper::GetDifferenceInTime($player_age);

        $tournaments = TournamentDraw::
        select("tournament_draws.tournament_id")->
        join("draw_categories", "draw_categories.id", "=", "tournament_draws.draw_category_id")->
        join("tournaments", "tournaments.id", "=", "tournament_draws.tournament_id")->
        join("tournament_dates", "tournament_dates.tournament_id", "=", "tournaments.id")->
        where("tournaments.status", 2);

        if ($no_ending)
            $tournaments->where("tournament_dates.main_draw_from", "<", $from_date->format('Y-m-d H:i:s'));
        else
            $tournaments->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')));


        $tournaments->where("draw_categories.range_from", "<", $player_age)->
        where("draw_categories.range_to", ">", $player_age)->
        groupBy("tournament_draws.tournament_id");


        return ($what == 'count') ? $tournaments->count() : $tournaments->get();
    }

    public static function getRefereeAssignedPlayers()
    {
        $draw_ids = TournamentDraw::whereHas('tournament', function ($query) {
            $query->whereRefereeId(Auth::user()->id);
        })->lists('id');

        $licence_numbers = TournamentTeam::whereHas('signup', function ($query) use ($draw_ids) {
            $query->whereIn('draw_id', $draw_ids);
        })->lists('licence_number', 'licence_number');

        return $licence_numbers;
    }

    public static function checkIfAssignedPoints($matchpoints, $tournament_id)
    {
        $matchpoints = $matchpoints->filter(function ($item) use ($tournament_id) {
            return $item->tournament_id == $tournament_id;
        });

        if ($matchpoints->first()->assigned)
            return LangHelper::get('assigned', 'Assigned');

        return LangHelper::get('unassigned', 'Unassigned');
    }

    /**
     * calculate total points for tournament
     * @param $matchpoints \Illuminate\Database\Eloquent\Collection
     * @param $tournament_id int
     * @return int total of points
     */
    public static function perTournamentTotal($matchpoints, $tournament_id)
    {
        $matchpoints = $matchpoints->filter(function ($item) use ($tournament_id) {
            return $item->tournament_id == $tournament_id;
        });

        $round_points = $matchpoints->max('round_points');
        $otorga_points = 0;
        $total_otorga = $matchpoints->map(function ($item) use (&$otorga_points) {
            return $otorga_points += $item->otroga_points;
        });

        return $round_points + $otorga_points;
    }

    /**
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @return int
     */
    public static function tournamentTotalPoints($matches)
    {
        $round_points = self::maxRoundPoints($matches);
        $points_otorga = self::otorgaPointsTotal($matches);

        return $round_points + $points_otorga;
    }

    /**
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @return int
     */
    public static function maxRoundPoints($matches)
    {
        $last_match = $matches->last();

        $round_points = 0;

        if (DrawMatchHelper::matchStatusByTeamId($last_match, $last_match->matchpoints->signup_id) == "W") {
            $round_points = RankingMatchpoint::where('player_id', $last_match->matchpoints->player_id)
                ->where('draw_id', $last_match->matchpoints->draw_id)
                ->max('round_points');
        } else {
            $round_points = array_reduce($matches->toArray(), function ($a, $b) {
                return $a > $b['matchpoints']['round_points'] ? $a : $b['matchpoints']['round_points'];
            });
        }

        return $round_points;
    }

    /**
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @return int
     */
    public static function otorgaPointsTotal($matches)
    {
        $points_otorga = 0;
        foreach ($matches as $match) {
            $points_otorga += $match->matchpoints->otroga_points;
        }

        return $points_otorga;
    }

    public static function calculateBestTournamentMatchPoints($player_matchpoints, $licence_number, $weekly_recalculation = true)
    {
        $points_result = [];

        $tournament_match_points = $player_matchpoints->groupBy('tournament_id');

        foreach ($tournament_match_points as $tournament_id => $matchpoint) {
            $points_result[$tournament_id] = self::perTournamentTotal($player_matchpoints, $tournament_id);
        }

        arsort($points_result);
        $best_fourteen = array_slice($points_result, 0, 14, true);

        foreach (array_keys($best_fourteen) as $tournament_id) {
            if ($weekly_recalculation) {
                RankingMatchpoint::where('tournament_id', $tournament_id)
                    ->where('licence_number', $licence_number)
                    ->update([
                        'assigned' => true,
                    ]);
            } else {
                RankingMatchpoint::where('tournament_id', $tournament_id)
                    ->where('licence_number', $licence_number)
                    ->update([
                        'assigned_for_quarter_ranking' => true,
                    ]);
            }
        }

        $total_points_sum = array_sum($best_fourteen);
        return round($total_points_sum);
    }
} 