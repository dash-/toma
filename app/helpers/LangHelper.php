<?php

class LangHelper
{
    public static function get($key, $value)
    {
        if (Lang::has('lang.'.$key) AND Lang::get('lang.'.$key) !== '')
            return Lang::get('lang.'.$key);
         else {
         	$file = public_path().'/csv/missing_langs.txt';
         	file_put_contents($file, $key.'|'.$value."\n", FILE_APPEND);
         }
        return $value;
    }

}