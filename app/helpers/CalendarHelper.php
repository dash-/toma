<?php

class CalendarHelper
{
    /**
     * method for filtering tournaments from calendar search
     * @param $query array
     * @param $list_view boolean
     * @return array
     */
    public static function filterData($query, $list_view = false)
    {
        $tournaments = Tournament::with('draws', 'club', 'date')
            ->leftJoin('tournament_draws', 'tournaments.id', '=', 'tournament_draws.tournament_id')
            ->leftJoin('draw_categories', 'tournament_draws.draw_category_id', '=', 'draw_categories.id')
            ->where('tournaments.status', 2);

        if (isset($query['club_id']) AND $query['club_id'])
            $tournaments->whereClubId($query['club_id']);

        if (isset($query['club_city']) AND $query['club_city'])
            $tournaments->whereClubId($query['club_city']);

        if (isset($query['category']) AND $query['category']) {
            $tournaments->where('draw_categories.id', $query['category']);
        }


        $tournaments = $tournaments->select("*", "tournaments.id as id")
            ->get()
            ->unique();

        return self::calendarData($tournaments, $list_view);
    }

    /**
     * parse tournaments collection to array for calendar processing
     * @param $tournaments Illuminate\Database\Eloquent\Collection
     * @param $list_view boolean checks if list view is used. If its TRUE use normal date instead of postgres date
     * @return array
     */
    public static function calendarData($tournaments, $list_view = false)
    {
        $final = [];
        $item = [];
        $counter = 1;
        foreach ($tournaments as $tournament)
        {
            $item['__id'] = $counter++;
            $item['id'] = $tournament->id;
            $item['title'] = $tournament->title.' - '. $tournament->status();
            $item['start'] = ($list_view)
                ? DateTimeHelper::GetShortDateFullYear($tournament->date->main_draw_from)
                : DateTimeHelper::GetPostgresDate($tournament->date->main_draw_from);
            $item['end'] = ($list_view)
                ? DateTimeHelper::GetShortDateFullYear($tournament->date->main_draw_to)
                : DateTimeHelper::GetPostgresDate($tournament->date->main_draw_to);
            $item['url_to']   = '/tournaments/details/'.$tournament->id;
            $item['public_page_url']   = URL::route('default_public', $tournament->slug);

            $final[] = $item;
        }
        return $final;
    }

    /**
     * @param $tournaments Illuminate\Database\Eloquent\Collection
     * @param $type string
     * @return array
     */
    public static function getClubs($tournaments, $type = 'club_name')
    {
        $clubs_array = [];
        foreach ($tournaments as $tournament) {
            $clubs_array[$tournament->club->id] = $tournament->club->$type;
        }

        $default = ($type == 'club_name')
            ? [null => LangHelper::get('choose_club', 'Choose club')]
            : [null => LangHelper::get('choose_city', 'Choose city')];

        return $default +  $clubs_array;
    }

    /**
     * @param $tournaments Illuminate\Database\Eloquent\Collection
     * @return array drop down formatted array
     */
    public static function getCategories($tournaments)
    {
        $category_array = [];

        foreach ($tournaments as $tournament) {
            foreach ($tournament->draws as $draw) {
                $category_array[$draw->draw_category_id] = $draw->category->name;
            }
        }

        return [null => LangHelper::get('choose_category', 'Choose category')] + $category_array;
    }
}