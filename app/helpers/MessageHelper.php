<?php

class MessageHelper {

    public static function setSeen($conversation_id)
    {
        $msgs = Auth::getUser()->messages()
            ->where('conversation_id', '=', $conversation_id)
            ->where('messages_users.seen', '=', FALSE)
            ->get();

        // dd(count($msgs));

        foreach ($msgs as $msg)
        {
            $msg->pivot->seen = true;
            $msg->pivot->time_seen = DateTimeHelper::getDateNow();

            $msg->pivot->save();
        }
    }
}