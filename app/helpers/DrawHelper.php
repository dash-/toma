<?php

class DrawHelper
{
    private static $main_rounds = [
        'Winner',
        'Finals',
        'Semifinals',
        'Quarterfinals',
        'R16',
        'R32',
        'R64',
        'R128',
    ];

    /**
     * @param $rounds array of rounds
     * @return array
     */
    public static function mainRoundNames($rounds)
    {
        return array_reverse(
            array_slice(self::$main_rounds, 0, count($rounds))
        );
    }

    /**
     * @param $rounds array of rounds
     * @param $accepted_qualifiers integer how many players goes from qualifications to main rounds
     * @return array round names
     */
    public static function qualiRoundNames($rounds, $accepted_qualifiers)
    {
        // if we have 6 accepted qualifiers we need to calculate to which round are qualifying rounds
        // so we need qualifiers to be semifinals (4 players) the rest should be lucky losers chosen from previous round
        $acceptances = Draw::roundUpToNextPowerOfTwo($accepted_qualifiers);
        
        $accepted_qualifiers = ($accepted_qualifiers == $acceptances) ? $accepted_qualifiers : $acceptances / 2;
        $round_names = [];

        foreach ($rounds as $key => $round) {
            if (count($round) < $accepted_qualifiers) {
                $round_names[] = LangHelper::get('qualifiers', 'Qualifiers');
                break;
            } else
                $round_names[] = LangHelper::get('round', 'Round').' '.$key;
        }
        
        
        return $round_names;
    }

    public static function dataForAutomaticBracket($players)
    {
        $result = [];
        foreach ($players as $key => $signup) {
            $item['id'] = is_int($signup['id']) ? $signup['id'] : 0;
            $item['drag'] = true;
            $item['name'] = is_int($signup['id']) ? DrawMatchHelper::getDragDropName($signup) : 'BYE';
            $item['seed_num'] = isset($signup['seeds'][0]['seed_num']) ? $signup['seeds'][0]['seed_num'] : '';
            if ($item['id'])
                $item['move_from_qualifiers_type'] = $signup['move_from_qualifiers_type'];
            $result[] = $item;
        }

        return $result;
    }

    public static function prepareDataForRoundsParsing($inputs, $draw_id, $list_type)
    {
        $counter = 1;
        $data = [];
        foreach ($inputs as $key => $input) {
            if ($key % 2 != 0) {
                $round_num = $counter++;
            } else {
                $round_num = $counter;
            }

            $item['id'] = $input['id'];
            $item['round_num'] = $round_num;

            $seed_num = (isset($input['seed_num']) && !empty($input['seed_num'])) ? $input['seed_num'] : 0;

            DrawSeed::createSingleSeed($draw_id, $item['id'], $list_type, $seed_num);

            if (isset($input['move_from_qualifiers_type']) AND $input['move_from_qualifiers_type'] != ''
                AND $input['move_from_qualifiers_type'] != 0) {
                $signup = TournamentSignup::find($item['id']);
                $signup->move_from_qualifiers_type = $input['move_from_qualifiers_type'];
                $signup->save();
            }

            $data[$item['round_num']][] = $item;
        }
        return $data;
    }

    /**
     * @param $signups Illuminate\Database\Eloquent\Collection
     * @param $num_of_seeds integer
     * @return array
     */
    public static function prepareDataForManualDraws($signups, $num_of_seeds)
    {
        $temp_points = 0;
        $before_order = 0;
        $result = [];
        foreach ($signups as $key => $signup) {
            $item['id'] = $signup->id;
            $item['drag'] = true;
            $item['name'] = DrawMatchHelper::getDragDropName($signup);
            $item['ranking_points'] = $signup->ranking_points;
            $item['seed_num'] = DrawMatchHelper::sameSeedPoints($num_of_seeds, $key + 1, $before_order, $temp_points, $signup->ranking_points, true);
            $item['move_from_qualifiers_type'] = $signup->move_from_qualifiers_type;
            $item['order_num'] = $key + 1;
            $item['ranking_points'] = $signup->ranking_points; // used for checking first user status
            $item['same_points_class'] = DrawMatchHelper::sameSeedPoints($num_of_seeds, $key + 1, $before_order, $temp_points, $signup->ranking_points);
            $result[] = $item;

            $before_order = ($temp_points == $signup->ranking_points) ? $before_order : $key + 1;
            $temp_points = $signup->ranking_points;
        }
        return $result;
    }
}