<?php

class ValidationHelper
{
    public static function checkIfPlayerExist($contact_data)
    {
        if (!count($contact_data))
            return FALSE;


        $dob = DateTimeHelper::GetContactsDate($contact_data['date_of_birth']);

        $player = Player::leftJoin('contacts', 'contacts.id', '=', 'players.contact_id')
            ->where('contacts.name', 'ILIKE', "%".$contact_data['name']."%")
            ->where('contacts.surname', 'ILIKE', "%".$contact_data['surname']."%")
            ->where('contacts.date_of_birth', '=', $dob)
            ->count();

        return ($player > 0) ? TRUE : FALSE;
    }

    /**
    * helper to validate signup data 
    * @param array $input
    * @param collection $draw
    * @return mixed boolean true || string message 
    **/
    
    public static function validateSignupData($input, $draw)
    {

        $signups = TournamentSignup::leftJoin('tournament_teams', 'tournament_signups.id', '=', 'tournament_teams.team_id')
            ->whereDrawId($draw->id)
            ->where('name', 'ILIKE', "%" . Input::get('name') . "%")
            ->where('surname', 'ILIKE', "%" . Input::get('surname') . "%")
            ->where('date_of_birth', '=', DateTimeHelper::GetPostgresDateTime(Input::get('date_of_birth')))
            ->first();

        $years = DateTimeHelper::GetDifferenceInTime(Input::get('date_of_birth'));
        $years_check = $years >= $draw->category->range_from && $years <= $draw->category->range_to;

        if ($signups)
            return LangHelper::get('already_signed_up_for_draw', 'You have already signed up for this draw');
        elseif (!$years_check)
            return LangHelper::get('you_dont_belong_to_category', 'Sorry, You don\'t belong to this category');


        return TRUE;
    }

    /**
     * @param $licence_number integer
     * @return mixed string|boolean
     */
    public static function checkIfLicenceExist($licence_number)
    {
        if (Auth::user()->hasRole('superadmin'))
            return false;

        // check if licence number is integer
        if (!filter_var($licence_number, FILTER_VALIDATE_INT))
            return LangHelper::get('licence_number_must_be_integer', 'Licence number must be integer');

        $licence_range = LicenceRange::getLicenceRanges(Auth::user()->getUserRegion());
        if ($licence_range->range_from > $licence_number OR $licence_range->range_to < $licence_number)
            return LangHelper::get('licence_not_in_range', 'Licence number is not in range');

        $player = Player::whereLicenceNumber($licence_number)->first();
        if (!is_null($player))
            return LangHelper::get('licence_already_exist', 'Licence number is already in database');

        return false;
    }
} 