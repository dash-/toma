<?php

class EmailHelper
{

    public static function sendEmail($view, $emailData, $viewData = array(), $attachment = '' )
    {
        Mail::send($view, $viewData, function ($message) use ($emailData, $attachment) {

            $message->to($emailData->to, $emailData->name ? $emailData->name : $emailData->to)->subject($emailData->subject);

            if($attachment)
                $message->attach($attachment);

            if(isset($emailData->replyTo) && $emailData->replyTo != '')
                $message->replyTo($emailData->replyTo, isset($emailData->replyToName) ? $emailData->replyToName :  $emailData->replyTo);
        });

    }
    public static function queueEmail($view, $emailData, $viewData = array() )
    {
        Mail::queue($view, $viewData, function ($message) use ($emailData) {

            $message->to($emailData->to, $emailData->name ? $emailData->name : $emailData->to)->subject($emailData->subject);
            
            if(isset($emailData->replyTo) && $emailData->replyTo != '')
                $message->replyTo($emailData->replyTo, isset($emailData->replyToName) ? $emailData->replyToName :  $emailData->replyTo);
        });

    }

}