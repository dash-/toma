<?php

class TournamentHelper
{

    /**
     * Calculate number of tournaments in next weeks
     *
     * @param   string   how far in future - we have next week, +1 week, +2 weeks
     * @param   string   receives 'count' or 'get'
     * @return  int   tournaments count
     */
    public static function calculateByWeeks($when, $what, $model = 'Tournament')
    {
        $from_date = new DateTime('monday ' . $when);
        $to_date = new DateTime('sunday ' . $when);

        // fallback because DateTime have problems with time zones, first day of week is sunday
        if (in_array($when, ['+1 week', '+2 weeks']))
            $to_date->add(new DateInterval('P1W'));

        if ($model == 'Referee') {
            return self::filteredOrganizers('', '', 'count');
        }

        return ($model == 'Tournament')
            ? self::filteredTournament($from_date, $to_date, $what)
            : self::filteredOrganizers($from_date, $to_date, $what);
    }

    public static function countRefereesByWeeks($when)
    {
        $from_date = new DateTime('monday ' . $when);
        $to_date = new DateTime('sunday ' . $when);

        if (in_array($when, ['+1 week', '+2 weeks']))
            $to_date->add(new DateInterval('P1W'));

        if (Auth::getUser()->hasRole("superadmin") || Auth::getUser()->hasRole("referee")) {
            $tournaments = Tournament::leftJoin('tournament_dates', function ($join) {
                $join->on('tournaments.id', '=', 'tournament_dates.tournament_id');
            })
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->whereStatus(2)
                ->whereNotNull("referee_id_real");
        } else {
            $tournaments = Tournament::join("clubs", function ($join) {
                $join->on("tournaments.club_id", "=", "clubs.id");
            })->join("regions", function ($join) {
                $join->on("regions.id", "=", "clubs.region_id");
            })->leftJoin('tournament_dates', function ($join) {
                $join->on('tournaments.id', '=', 'tournament_dates.tournament_id');
            })
                ->where("regions.id", Auth::getUser()->getUserRegion())
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->whereStatus(2)
                ->whereNotNull("referee_id_real");
        }

        return $tournaments->remember(10, 'referees_by_weeks_count')->count();
    }

    public static function assignedTournaments()
    {
        $tournament = Tournament::join("clubs", function ($join) {
            $join->on("tournaments.club_id", "=", "clubs.id");
        })->join("regions", function ($join) {
            $join->on("regions.id", "=", "clubs.region_id");
        })->leftJoin('tournament_dates', function ($join) {
            $join->on('tournaments.id', '=', 'tournament_dates.tournament_id');
        })
            ->where("tournaments.referee_id", Auth::getUser()->id)
            ->whereStatus(2)->get();

        return $tournament;
    }

    public static function sumCountByWeeks()
    {
        $from_date = new DateTime('monday next week');
        $to_date = new DateTime('sunday +2 weeks');
        $to_date->add(new DateInterval('P1W'));

        return self::filteredTournament($from_date, $to_date, 'count');
    }

    private static function filteredTournament($from_date, $to_date, $what)
    {
        if (Auth::getUser()->hasRole("superadmin") || Auth::getUser()->hasRole("referee")) {
            $tournaments = Tournament::leftJoin('tournament_dates', function ($join) {
                $join->on('tournaments.id', '=', 'tournament_dates.tournament_id');
            })
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->whereStatus(2);
        } else {
            $tournaments = Tournament::join("clubs", function ($join) {
                $join->on("tournaments.club_id", "=", "clubs.id");
            })->join("regions", function ($join) {
                $join->on("regions.id", "=", "clubs.region_id");
            })->leftJoin('tournament_dates', function ($join) {
                $join->on('tournaments.id', '=', 'tournament_dates.tournament_id');
            })
                ->where("regions.id", Auth::getUser()->getUserRegion())
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->whereStatus(2);
        }

        return ($what == 'count') ? $tournaments->remember(10, 'filtered_tournaments_count')->count() : $tournaments->get();
    }

    private static function filteredOrganizers($from_date, $to_date, $what)
    {
        if (Auth::getUser()->hasRole("superadmin")) {
            $organizers = TournamentOrganizer::join('tournaments', 'tournaments.organizer_id', '=', 'tournament_organizers.id')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->where('tournaments.status', 2);
        } elseif (Auth::getUser()->hasRole("referee")) {
            //only tournaments assigned for referee
            $organizers = TournamentOrganizer::join('tournaments', 'tournaments.organizer_id', '=', 'tournament_organizers.id')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->join('clubs', "tournaments.club_id", "=", "clubs.id")
                ->where("tournaments.referee_id", Auth::getUser()->id)
                ->where('tournaments.status', 2);
        } else {
            $organizers = TournamentOrganizer::join('tournaments', 'tournaments.organizer_id', '=', 'tournament_organizers.id')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->join('clubs', "tournaments.club_id", "=", "clubs.id")
                ->join('regions', "regions.id", "=", "clubs.region_id")
                ->where("regions.id", Auth::getUser()->getUserRegion())
                ->whereBetween('tournament_dates.main_draw_from', array($from_date->format('Y-m-d H:i:s'), $to_date->format('Y-m-d H:i:s')))
                ->where('tournaments.status', 2);
        }


        return ($what == 'count') ? $organizers->count() : $organizers->get();
    }


    public static function genericCounter()
    {
        if (Auth::getUser()->hasRole('superadmin'))
            $tournament_counts = Tournament::remember(10, 'tournaments_count')->count();
        elseif (Auth::getUser()->hasRole("regional_admin"))
            $tournament_counts = Tournament::join("clubs", function ($join) {
                $join->on("tournaments.club_id", "=", "clubs.id");
            })->join("regions", function ($join) {
                $join->on("regions.id", "=", "clubs.region_id");
            })->where("regions.id", Auth::getUser()->getUserRegion())->remember(10, 'regional_tournaments_count')->count();
        else
            $tournament_counts = Tournament::where('created_by', Auth::getUser()->id)
                ->remember(10, 'user_tournaments_count')
                ->count();


        return $tournament_counts;
    }


    public static function countCalendarTournaments($status)
    {
        if (Auth::getUser()->hasRole('superadmin') || Auth::getUser()->hasRole('referee'))
            return Tournament::whereStatus($status)->remember(10, 'role_calendar_count')->count();


        return Tournament::join("clubs", function ($join) {
            $join->on("tournaments.club_id", "=", "clubs.id");
        })->join("regions", function ($join) {
            $join->on("regions.id", "=", "clubs.region_id");
        })->where("regions.id", Auth::getUser()->getUserRegion())
            ->whereStatus($status)
            ->remember(10, 'regional_calendar_count')
            ->count();

    }

    public static function countContacts()
    {
        if (Auth::getUser()->hasRole('superadmin')) {
            return Tournament::where('tournaments.status', 2)
                ->remember(10, 'contacts_count')
                ->count();
        }

        return Tournament::join("clubs", "tournaments.club_id", "=", "clubs.id")
            ->join("regions", "regions.id", "=", "clubs.region_id")
            ->where("regions.id", Auth::getUser()->getUserRegion())
            ->whereStatus(2)
            ->remember(10, 'regional_contacts_count')
            ->count();
    }


    public static function countTotalSignUps($status = false, $diff = false)
    {
        if (Auth::getUser()->hasRole('superadmin')) {
            $count = TournamentSignup::join("tournaments", function ($join) {
                $join->on("tournament_signups.tournament_id", "=", "tournaments.id");
            });

            if ($status && $diff)
                $count->where("tournaments.status", "!=", $status);

            $count->remember(10, 'signups_count');
            return $count->count();
        }
        $count = TournamentSignup::join("tournaments", function ($join) {
            $join->on("tournament_signups.tournament_id", "=", "tournaments.id");
        })->join("clubs", function ($join) {
            $join->on("tournaments.club_id", "=", "clubs.id");
        })->join("regions", function ($join) {
            $join->on("regions.id", "=", "clubs.region_id");
        })->where("regions.id", Auth::getUser()->getUserRegion())
            ->remember(10, 'regional_signups_count');

        if ($status && $diff)
            $count->where("tournaments.status", "!=", $status);

        $count->count();
    }

    public static function countTotalDraws($status = false, $diff = false)
    {
        if (Auth::getUser()->hasRole('superadmin') OR Auth::user()->hasRole('referee')) {
            if ($status) {

                $count = TournamentDraw::join("tournaments", "tournament_draws.tournament_id", "=", "tournaments.id");

                if (!$diff)
                    $count->where('tournaments.status', $status);
                else
                    $count->where('tournaments.status', "!=", $status);

//                    $count->remember(10, 'draws_count');

                return $count->count();
            } else
                return TournamentDraw::remember(10, 'all_draws_count')->count();

        }

        $counter = TournamentDraw::join("tournaments", function ($join) {
            $join->on("tournament_draws.tournament_id", "=", "tournaments.id");
        })->join("clubs", function ($join) {
            $join->on("tournaments.club_id", "=", "clubs.id");
        })->join("regions", function ($join) {
            $join->on("regions.id", "=", "clubs.region_id");
        })->where("regions.id", Auth::getUser()->getUserRegion());

        if ($status)
            $counter->where('tournaments.status', $status);

        return $counter->remember(10, 'regional_draws_count')->count();
    }

    public static function countDraws($tournament_id)
    {
        return TournamentDraw::whereTournamentId($tournament_id)->remember(10, 'count_draws_in_tournament')->count();
    }

    /**
     * @param $draw Illuminate\Database\Eloquent\Collection
     */
    public static function prepareSignupLists($draw)
    {
//        echo Debug::vars($draw->status);die;
        // if draw has matches do not prepare signup lists
        if (in_array($draw->status, [3, 4, 5])) {
            return;
        }

        // get all signups
        $all_signups = TournamentSignup::whereDrawId($draw->id)
            ->select(DB::raw("COALESCE(ranking_points, 0) as ranking_points_null"), "*")
            ->orderBy('ranking_points_null', 'desc')
            ->get()
            ->toArray();

//echo Debug::vars($all_signups);die;
        // remove withdrawals because we don't need them
        $all_signups = array_filter($all_signups, function ($item) {
            return ($item['list_type'] != 4);
        });

        // get count of all statuses of signups
        $statuses = array_count_values(array_filter(array_column($all_signups, 'status')));

        // get counts of wild cards and manually moved players
        $main_wild_cards = (isset($statuses[1])) ? $statuses[1] : 0;
        $quali_wild_cards = (isset($statuses[2])) ? $statuses[2] : 0;
        $manual_moved_players_to_main = (isset($statuses[5])) ? $statuses[5] : 0;
        $manual_moved_players_to_quali = (isset($statuses[6])) ? $statuses[6] : 0;

        // remove players with status because we don't process them
        $signups = array_filter($all_signups, function ($item) {
            return !in_array($item['status'], [1, 2, 5, 6, 7]);
        });

        // get wild cards from array and give them list type
        $wild_cards = array_filter($all_signups, function ($item) {
            return in_array($item['status'], [1, 2]);
        });

        if (count($wild_cards)) {
            foreach ($wild_cards as $wild_card) {
                TournamentSignup::whereId($wild_card['id'])
                    ->update([
                        'list_type' => $wild_card['status']
                    ]);
            }
        }

        $main_count = $draw->size->direct_acceptances - $main_wild_cards - $manual_moved_players_to_main;
        $quali_count = $draw->qualification->draw_size - $quali_wild_cards - $manual_moved_players_to_quali;

        $final = array(
            1 => array_splice($signups, 0, $main_count),
            2 => array_splice($signups, 0, $quali_count),
            3 => array_splice($signups, 0),
        );

        foreach ($final as $key => $data) {
            foreach ($data as $num => $player) {
                TournamentSignup::whereId($player['id'])
                    ->update([
                        'list_type' => $key
                    ]);
            }
        }
    }

    public static function populateSignupLists($draw_id, $direct_count, $qualification_count)
    {
        if (self::checkListState($draw_id) OR self::checkQualifyingListState($qualification_count, $draw_id)) {
            $withdrawals = TournamentSignup::whereDrawId($draw_id)
                ->whereListType(4)
                ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
                ->get()
                ->toArray();

            if (self::checkQualifyingListState($qualification_count, $draw_id)) {
                $players = TournamentSignup::where(function ($query) {
                    $query->whereNotIn('list_type', array(1, 4))
                        ->orWhereNull('list_type');
                })->whereDrawId($draw_id)
                    ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
                    ->get()
                    ->toArray();

                $players_fin = $players;

                $final = array(
                    2 => array_splice($players_fin, 0, $qualification_count),
                    3 => array_splice($players_fin, 0),
                );
                $final[4] = $withdrawals;
            } else {
                $players = TournamentSignup::whereNull('list_type')
                    ->orWhere('list_type', '!=', 4)
                    ->whereDrawId($draw_id)
                    ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
                    ->get()
                    ->toArray();

                $wild_cards = TournamentSignup::whereDrawId($draw_id)
                    ->whereStatus(1)
                    ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
                    ->get()
                    ->toArray();

                $players_fin = $wild_cards + $players;

                $final = array(
                    1 => array_splice($players_fin, 0, $direct_count),
                    2 => array_splice($players_fin, 0, $qualification_count),
                    3 => array_splice($players_fin, 0),
                );
                $final[4] = $withdrawals;
            }

            foreach ($final as $key => $data) {
                foreach ($data as $num => $player) {
                    $ts = TournamentSignup::find($player['id']);
                    $ts->list_type = $key;
                    $ts->update();
                }
            }
        }
    }

    public static function checkListState($draw_id)
    {
        $count = TournamentSignup::whereDrawId($draw_id)
            ->whereNull('list_type')
            ->count();
//echo Debug::vars($count);die;
        if ($count)
            return true;

        return false;
    }

    public static function checkQualifyingListState($qualification_count, $draw_id)
    {
        $qualifications_draw_size = TournamentSignup::whereDrawId($draw_id)
            ->whereListType(2)
            ->count();

        if ($qualification_count > $qualifications_draw_size AND $qualifications_draw_size > 0)
            return true;

        return false;
    }

    public static function separateSignupsByCategories($players)
    {
        $final = array();
        foreach ($players as $player) {
            if ($player['move_from_qualifiers_type'] > 0) {
                $final[1][] = $player;
                $final[2][] = $player;
            } else
                $final[$player['list_type']][] = $player;
        }
        ksort($final);

        return $final;
    }

    /**
     * @param $array (array)
     *            array of pairs
     * @param $type (string)
     *            key to return from array
     * @return returns $value (string)
     *            imploded values with <br>
     */
    public static function getPlayerValue($array, $type)
    {
        $result = [];

        foreach ($array['teams'] as $pair) {
            $result[] = ($type == 'date_of_birth')
                ? DateTimeHelper::GetShortDateFullYear($pair[$type])
                : $pair[$type];
        }


        return $value = implode("<br>", $result);

    }

    public static function getNumberOfSeeds($draw_size)
    {

        switch ($draw_size) {
            case 8:
                return 2;
                break;
            case 16:
                return 4;
                break;
            case 24:
            case 32:
                return 8;
                break;
            case 48:
            case 64:
            case 96:
            case 128:
                return 16;
                break;
            default:
                return 4;
                break;
        }
    }

    public static function getTotalAcceptance()
    {
        return array(null => 'Choose total acceptance') +
        [
            '8' => '8',
            '16' => '16',
            '24' => '24',
            '32' => '32',
            '48' => '48',
            '64' => '64',
            // '96'    =>  '96',
            '128' => '128',
        ];


    }

    public static function getPreQualyTotalAcceptance()
    {
        return
            [
                '0' => '0',
                '8' => '8',
                '16' => '16',
                '24' => '24',
                '32' => '32',
                '48' => '48',
                '64' => '64',
                // '96'    =>  '96',
                '128' => '128',
            ];


    }

    public static function calculateTournamentLevel($players, TournamentDraw $draw)
    {
        $prize_stars = PrizesCalculation::whereRaw(DB::raw($draw->prize_pool . ' BETWEEN prize_from AND prize_to'))
            ->first()
            ->stars;

        $player_sum = 0;
        foreach ($players as $key => $player) {
            $player_sum += $player['teams'][0]['ranking_points'];
        }

        if ($player_sum > 3000)
            $final_stars = 19;
        else {
            $points_stars = PointsCalculation::whereRaw(DB::raw($player_sum . 'BETWEEN points_from AND points_to'))->first();
            $final_stars = ($points_stars) ? $points_stars->stars : 0;
        }


        $draw->level = $prize_stars + $final_stars;

        $draw->save();
    }

    public static function checkIfMatchesExist($draw_id, $list_type)
    {
        $count_matches = DrawMatch::whereDrawId($draw_id)
            ->whereListType($list_type)
            ->count();

        return ($count_matches) ? true : false;
    }

    public static function checkIfTournamentHasMatches($tournament_id)
    {
        $tournament_draws = TournamentDraw::with('matches')->whereTournamentId($tournament_id)->get();
        $counter = 0;
        foreach ($tournament_draws as $draw) {
            $counter += $draw->matches->count();
        }

        return ($counter > 0) ?: false;

    }

    public static function drawHasMatches($draw_id, $list_type, $draws_with_matches_list)
    {
        $checker = $draws_with_matches_list->filter(function ($draw) use ($draw_id, $list_type) {
            if ($draw->draw_id == $draw_id AND $draw->list_type == $list_type)
                return true;
        });

        return ($checker->isEmpty()) ? false : true;
    }

    public static function countTournamentMatches($tournament_id, $list_type)
    {
        $draws = TournamentDraw::whereTournamentId($tournament_id)->get()->lists('id', 'id');

        $count_matches = DrawMatch::whereIn('draw_id', $draws)
            ->whereListType($list_type)
            ->count();

        return ($count_matches) ? true : false;
    }

    public static function checkIfQualifierWinnerExist($draw_id)
    {
        $pair = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
            ->where('draw_id', $draw_id)
            ->where('list_type', 2)
            ->orderBy('id', 'desc')
            ->first();

        if (count($pair))
            return ($pair->team1_score OR $pair->team2_score) ? true : false;
        else
            return false;

    }

    public static function checkIfMainWinnerExist($draw_id)
    {
        $pair = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
            ->where('draw_id', $draw_id)
            ->where('list_type', 1)
            ->orderBy('id', 'desc')
            ->first();

        if (count($pair))
            return ($pair->team1_score OR $pair->team2_score) ? true : false;
        else
            return false;

    }

    public static function checkIfQualificationsAreFinished($draw_id)
    {
        $count = TournamentSignup::where('draw_id', $draw_id)
            ->where('list_type', 2)
            ->where('move_from_qualifiers_type', "!=", 0)
            ->count();

        return $count > 0;

    }

    public static function checkIfMainDrawIsFinished($draw_id)
    {

        $count = DrawHistory::where('draw_id', $draw_id)->where('list_type', 1)->count();

        return $count > 0;
    }

    public static function mainDrawFinished($draw_id, $draw_history)
    {

        $checker = $draw_history->filter(function ($history) use ($draw_id) {
            if ($history->draw_id == $draw_id AND $history->list_type == 1)
                return true;
        });

        return ($checker->isEmpty()) ? false : true;
    }

    public static function getDrawDates($date_model, $list_type)
    {
        if ($list_type == 2) {
            $begin = new DateTime($date_model->qualifying_date_from);
            $end = new DateTime($date_model->qualifying_date_to);
        } elseif ($list_type == 1) {
            $begin = new DateTime($date_model->main_draw_from);
            $end = new DateTime($date_model->main_draw_to);
        } elseif ($list_type == 0) {
            //Qualifying date can be null (no qualifications)
            $begin = is_null($date_model->qualifying_date_from) ? new DateTime($date_model->main_draw_from) : new DateTime($date_model->qualifying_date_from);
            $end = new DateTime($date_model->main_draw_to);
        } else {
            return null;
        }

        $end = $end->modify('+1 day');

        $result = [];

        $interval = new DateInterval('P1D');

        $daterange = new DatePeriod($begin, $interval, $end);


        foreach ($daterange as $date) {
            $result[$date->format('Y-m-d H:i:s')] = DateTimeHelper::getOnlyDate($date);
        }

        return $result;
    }

    public static function checkIfThereIsntQualificationDraw($draw)
    {
        return $draw->qualification->draw_size == 0;
    }

    public static function getRefereesDropdown($ref_id = null)
    {
        $referees = User::with('contact')
            ->whereHas('roles', function ($query) {
                $query->where('role_id', 6);
            })->orderBy('id', 'DESC')
            ->get();

        $item = [];
        foreach ($referees as $ref) {
            $item[$ref->id] = $ref->info('name') . ' ' . $ref->info('surname');
        }


        if ($ref_id) {
            $current_ref = User::with('contact')
                ->find($ref_id);


            return (!is_null($current_ref))
                ? [$ref_id => $current_ref->info('name') . ' ' . $current_ref->info('surname')] + $item
                : $item;

        } else
            return [null => LangHelper::get('choose_referee', 'Choose referee')] + $item;
    }

    public static function getStatuses($status = null)
    {
        $statuses = [
            1 => LangHelper::get('pending_approval', 'Pending approval'),
            2 => LangHelper::get('approved', 'Approved'),
            3 => LangHelper::get('rejected', 'Rejected'),
            // 4 => LangHelper::get('submitted_for_approval', 'Submited for approval'), // 
            5 => LangHelper::get('finished', 'Finished'),
        ];

        return (!$status)
            ? [null => 'Choose status'] + $statuses
            : [$status => $statuses[$status]] + $statuses;
    }

    public static function checkIfOrganizerExists($email)
    {
        $organizer = TournamentOrganizer::whereEmail($email)
            ->first();

        return (is_null($organizer)) ? false : $organizer;
    }

    /**
     * check draws in tournament, if all draws have status 5 - finished close tournament
     * @param integer $tournament_id
     * @return void
     **/

    public static function checkTournamentStatus($tournament_id)
    {
        $tournament = Tournament::with('draws')->find($tournament_id);

        // get statuses of all draws in tournament
        $draw_statuses = array_pluck($tournament->draws->toArray(), 'status');
        // count statuses by status index return ex: [5] => 4;
        // explanation we have 4 draws with status 5
        $count_draw_statuses = array_count_values($draw_statuses);

        // check if we have draws with status 5 - finished
        if (isset($count_draw_statuses[5])) {
            // check if count of draws in tournament is equal to count of finished draws
            if ($count_draw_statuses[5] == $tournament->draws->count()) {
                $tournament->status = 5;
                $tournament->save();
            }
        }
    }

    public static function countDrawPlayers($signups)
    {

        if (!$signups->count())
            return 0;

        $sum = 0;
        foreach ($signups as $signup) {
            $sum += ($signup->teams) ? $signup->teams->count() : 0;
        }

        return $sum;
    }

    public static function invoiceTotal($draws, $price)
    {
        $total = 0;
        foreach ($draws as $draw) {
            $total += self::countDrawPlayers($draw->id) * $price;
        }

        return $total;
    }

    public static function getOnSiteDropdown($chosen_value = false)
    {
        $values = [
            0 => LangHelper::get('no', 'No'),
            1 => LangHelper::get('yes', 'Yes'),
        ];

        return ($chosen_value)
            ? [$chosen_value => $values[$chosen_value]] + $values
            : $values;
    }

    public static function getQualiFinalRoundDropdown($chosen_value = false)
    {
        $values = [
            false => LangHelper::get('no', 'No'),
            true => LangHelper::get('yes', 'Yes'),
        ];

        return ($chosen_value)
            ? [$chosen_value => $values[$chosen_value]] + $values
            : $values;
    }
//
//    public static function external_points_types()
//    {
//        $types = [
//            'Tennis Europe' => 1,
//            'ITF Junior' => 5,
//            'ATP' => 150,
//            'WTA' => 250,
//        ];
//
//        return $types;
//    }
    public static function external_points_type_keys($key)
    {
        $types = [
            1 => 1,
            2 => 5,
            3 => 150,
            4 => 250,
        ];

        return $types[$key];
    }

    public static function external_points_type_names($key = '')
    {
        $types = [
            1 => 'Tennis Europe',
            2 => 'ITF Junior',
            3 => 'ATP',
            4 => 'WTA',
        ];

        if ($key)
            return $types[$key];

        return $types;
    }

    public static function getHasQualiQuestions($key = '')
    {
        $q = [
            0 => 'No',
            1 => 'Yes'
        ];

        if ($key)
            return $q[$key];

        return $q;
    }

    public static function getHasPreQualiQuestions($key = '')
    {
        $q = [
            0 => 'No',
            1 => 'Yes'
        ];

        if ($key)
            return $q[$key];

        return $q;
    }

    public static function getRefereeInfo($tournament)
    {
        $referee_info = Referee::with('contact')->where('id', $tournament->referee_id_real)->first();
        if ($referee_info) {
            $info = $referee_info->contact->phone;
            return $info;
        } else
            return 'N/A';
    }

    public static function RequestAction($tournament_id, $status)
    {
        $tournament = Tournament::find($tournament_id);

        switch ($status) {
            case 1:
                $tournament->status = 4;
                $tournament->save();

                if (!Auth::getUser()->hasRole('superadmin')) {
                    $message = LangHelper::get('request_for_tournament_approval_received', 'Request for tournament approval received');
                    $send_to = Role::find(1)->users()->get()->lists('id', 'id');
                    Notification::TournamentNotif($send_to, $tournament->id, 2, $message);
                }
                break;
            // approved
            case 2:
                $tournament->status = 2;
                $tournament->date_of_approval = DateTimeHelper::GetDateNow();
                $tournament->approved_by = Auth::getUser()->id;

                if (Auth::user()->hasRole('superadmin')) {
                    $tournament->validated = true;
                    $tournament->validated_by = Auth::user()->id;
                }

                $tournament->save();
                $tournament_draws = TournamentDraw::with("tournament")->whereTournamentId($tournament_id)->get();
                $main_draw_short_date = DateTimeHelper::convertToFormat($tournament->date->main_draw_from, "Y.m.d");
                foreach ($tournament_draws as $key => $draw) {
                    $unique_draw_id =
                        $main_draw_short_date . "." .
                        str_pad($tournament->club->province->region->id, 6, '0', STR_PAD_LEFT) . "." .
                        $tournament->club->id . "." .
                        str_pad($tournament->id, 4, '0', STR_PAD_LEFT) . "." .
                        str_pad($key + 1, 2, '0', STR_PAD_LEFT);

                    $draw->unique_draw_id = $unique_draw_id;
                    $draw->save();
                }


                $message = 'Tournament ' . $tournament->title . ' is approved';
                Notification::TournamentNotif($tournament->created_by, $tournament->id, 3, $message);
                break;
            // rejected
            case 3:
                $tournament->status = 3;
                $tournament->date_of_approval = DateTimeHelper::GetDateNow();
                $tournament->save();

                $message = 'Tournament ' . $tournament->title . ' is rejected';
                Notification::TournamentNotif($tournament->created_by, $tournament->id, 4, $message);
                break;
        }

    }
}