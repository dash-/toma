<?php

class ConsolationHelper
{
    /**
     * method to return players separated in main consolation draw and qualifying consolation draw
     * @param $signups array
     * @return array
     */
    public static function consolationLists($signups)
    {
        $players = [];
        foreach ($signups as $signup) {
            if ($signup['consolation'] > 0) {
                $players[$signup['consolation']][] = $signup;
            }
        }
        ksort($players);

        return $players;
    }

    /**
     * method user for getting players that lost matches in first round
     * passed consolation type because consolation type for qualifying matches should be same as listType
     * @param $draw_id integer
     * @param $consolation_type integer
     * @return mixed
     */
    public static function playersForDraw($draw_id, $consolation_type)
    {
        $pairs = DrawMatch::whereDrawId($draw_id)
            ->whereListType($consolation_type)
            ->whereRoundNumber(1)
            ->get();

        if (!$pairs->count())
            return false;

        $loser_keys = [];
        foreach ($pairs as $pair) {
            $loser_keys[] = ($pair->team1_score > $pair->team2_score) ? $pair->team2_id : $pair->team1_id;
        }

        $signups = TournamentSignup::with('teams')
            ->whereIn('id', $loser_keys)
            ->orderBy('ranking_points', 'desc')
            ->get();

        $result = [];
        foreach ($signups as $signup) {
            $item['id'] = $signup->id;
            $item['name'] = DrawMatchHelper::getDragDropName($signup);

            $result[] = $item;
        }

        return $result;
    }

    public static function checkIfMatchesExist($draw_id, $consolation_type)
    {
        $count_matches = DrawMatch::whereDrawId($draw_id)
            ->whereConsolationType($consolation_type)
            ->count();

        return ($count_matches) ? true : false;
    }


    /**
     * method for getting players data for drag and drop consolation creation
     * @param $draw Illuminate\Database\Eloquent\Collection
     * @param $consolation_type integer
     * @return array
     */
    public static function manualCreationData($draw, $consolation_type)
    {
        $result = [];
        // check if matches in this draw exists
        if (!self::checkIfMatchesExist($draw->id, $consolation_type)) {

            $signups = TournamentSignup::with('teams')
                ->whereDrawId($draw->id)
                ->whereConsolation($consolation_type)
                ->get();

            if (!count($signups))
                return [];

            foreach ($signups as $key => $signup) {
                $item['id'] = $signup->id;
                $item['drag'] = true;
                $item['name'] = DrawMatchHelper::getDragDropName($signup);
                $item['order_num'] = $key + 1;
                $item['seed_num'] = '';
                $item['ranking_points'] = $signup->ranking_points; // used for checking first user status
                $result[] = $item;
            }
        } else {
            $pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
                ->where('draw_id', $draw->id)
                ->where('consolation_type', $consolation_type)
                ->where('round_number', 1)
                ->get();

            $seed_numbers = DrawSeed::getSeeds($draw->id, $consolation_type, true);

            foreach ($pairs as $pair) {
                $item1['id'] = $pair->team1_id;
                $item1['drag'] = true;
                $item1['name'] = $pair->team1_data
                    ? DrawMatchHelper::getTeamNames($pair->team1_data)
                    : DrawMatchHelper::getPlaceholderNames($pair->team1_id);
                $item1['seed_num'] = (array_key_exists($pair->team1_id, $seed_numbers)) ? $seed_numbers[$pair->team1_id] : NULL;
                $result[] = $item1;

                $item2['id'] = $pair->team2_id;
                $item2['drag'] = true;
                $item2['name'] = $pair->team2_data
                    ? DrawMatchHelper::getTeamNames($pair->team2_data)
                    : DrawMatchHelper::getPlaceholderNames($pair->team2_id);
                $item2['seed_num'] = (array_key_exists($pair->team2_id, $seed_numbers)) ? $seed_numbers[$pair->team2_id] : NULL;
                $result[] = $item2;
            }
        }

        return $result;
    }

    /**
     * @param $draw Illuminate\Database\Eloquent\Collection
     * @param $consolation_type integer
     * @return integer
     */
    public static function getTotalPlayers($draw, $consolation_type)
    {
        $signups_count = TournamentSignup::whereDrawId($draw->id)
            ->whereConsolation($consolation_type)
            ->count();

        return Draw::roundUpToNextPowerOfTwo($signups_count);
    }

    /**
     * method to return array of teams per round
     * @param $inputs array
     * @return array
     */
    public static function preparePlayersForRounds($inputs, $draw_id, $consolation_type)
    {
        $data = [];
        $counter = 1;

        foreach ($inputs as $key => $input) {
            if ($key % 2 != 0) {
                $round_num = $counter++;
            } else {
                $round_num = $counter;
            }

            $item['id'] = $input['id'];
            $item['round_num'] = $round_num;

            $seed_num = (isset($input['seed_num']) && !empty($input['seed_num'])) ? $input['seed_num'] : 0;

            if ($seed_num > 0) {
                DrawSeed::create([
                    'draw_id'          => $draw_id,
                    'signup_id'        => $item['id'],
                    'consolation_type' => $consolation_type,
                    'seed_num'         => $seed_num,
                ]);
            }


            $data[$item['round_num']][] = $item;
        }
        return $data;
    }

    /**
     * method to save pairs for consolation matches
     * @param $pairs_per_round array
     * @param $draw_id integer
     * @param $consolation_type integer
     */
    public static function saveMatchesToDB($pairs_per_round, $draw_id, $consolation_type)
    {
        foreach ($pairs_per_round as $key => $rounds) {
            $counter = 1;
            foreach ($rounds as $round) {
                DrawMatch::create([
                    'team1_id'         => (!isset($round[0]['id'])) ? 0 : $round[0]['id'],
                    'team2_id'         => (!isset($round[1]['id'])) ? 0 : $round[1]['id'],
                    'draw_id'          => $draw_id,
                    'consolation_type' => $consolation_type,
                    'list_type'        => 0,
                    'round_position'   => $counter++,
                    'round_number'     => $key,
                ]);
            }
        }

        //  load matches and setup initial scores for bye's
        $matches = DrawMatch::where(function ($query) {
            $query->where('team1_id', 0)
                ->orWhere('team2_id', 0);
            })
            ->where('draw_id', $draw_id)
            ->whereRoundNumber(1)
            ->whereConsolationType($consolation_type)
            ->orderBy('id')
            ->get();

        if (count($matches))
            self::setUpInitialScores($matches, $draw_id, $consolation_type);
    }

    /**
     * method to set up direct passes to next round - bye calculation
     * @param $pairs array
     * @param $draw_id integer
     * @param $consolation_type integer
     */
    private static function setUpInitialScores($pairs, $draw_id, $consolation_type)
    {
        foreach ($pairs as $key => $pair) {
            // calculate next round position for player
            $next_round_pos = ($pair->round_position % 2 != 0) ? ($pair->round_position + 1) / 2 : $pair->round_position / 2;

            $obj = DrawMatch::where('round_position', $next_round_pos)
                ->where('round_number', $pair->round_number + 1)
                ->where('consolation_type', $consolation_type)
                ->where('draw_id', $draw_id)
                ->first();

            $passing_team = max([$pair->team1_id, $pair->team2_id]);
            // position in pair calcuation
            if ($pair->round_position % 2 == 1) {
                $obj->team1_id = $passing_team;
                $data[] = $passing_team;
            } else {
                $obj->team2_id = $passing_team;
                $data[] = $passing_team;
            }

            $obj->update();

        }
    }

    public static function checkIfDrawHasScores($draw_id, $consolation_type)
    {
        $match_with_score_counter = DrawMatch::whereDrawId($draw_id)
            ->whereConsolationType($consolation_type)
            ->where(function ($query) {
                $query->where('team1_score', '>', 0);
                $query->orWhere('team2_score', '>', 0);
            })
            ->count();

        return ($match_with_score_counter > 0) ? true : false;
    }
}