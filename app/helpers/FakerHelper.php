<?php

class FakerHelper
{
    public static function insert($model_name, $table_id)
    {
        Faker::create([
            'model_name' => $model_name,
            'table_id'   => $table_id,
        ]);
    }

    public static function remove($model_name, $table_id)
    {
        $model = $model_name::where('id', $table_id)->delete();

        if ($model_name == 'TournamentDraw')
        {
            MatchSchedule::where('draw_id', $table_id)->delete();
            DrawMatch::where('draw_id', $table_id)->delete();
            DrawSeed::where('draw_id', $table_id)->delete();
            TournamentSignup::where('draw_id', $table_id)->delete();
        }

    }
} 