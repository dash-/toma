<?php

class HtmlHelper {

	public static function entity_decode($string)
	{
		if (is_array($string))
		{
			$decoded = array();
			
			foreach ($string as $key => $value)
			{
				$decoded[HtmlHelper::entity_decode($key)] = HtmlHelper::entity_decode($value);
			}
			
			return $decoded;
		}
		
		$string = html_entity_decode($string, ENT_QUOTES, "UTF-8");
		
		return $string;
	}

	/**
	 * Strips all NUL bytes, HTML and PHP tags from a string
	 * Optional second parameter for allowed tags
	 *
	 * @param	mixed	string or array of strings to strip tags from
	 * @param	mixed	list of allowed tags ( string or array )
	 * @param	bool	if TRUE, entities will be converted too
	 * @return	string	tag-stripped string
	 */
	public static function strip($string, $allowed_tags = NULL, $entity_decode = TRUE)
	{ 
		if (is_array($string) or is_object($string))
		{
			$stripped = array();
			
			foreach ($string as $k => $v) 
			{
				$stripped[HtmlHelper::strip($k, $allowed_tags, $entity_decode)] = HtmlHelper::strip($v, $allowed_tags, $entity_decode);
			}
			
			return $stripped;
		}
		
		// If array of allowed tags is passed
		if (is_array($allowed_tags))
		{
			$as_string = '';
			
			foreach ($allowed_tags as $tag)
			{
				if (is_string($tag))
				{
					$as_string .= '<'.$tag.'>';
				}
			}
			
			$allowed_tags = $as_string;
		}
		
		if ($entity_decode === TRUE)
		{
			$string = str_replace('&nbsp;', ' ', $string);
			$string = HtmlHelper::entity_decode($string);
		}
		
		return strip_tags($string, $allowed_tags);
	}

	public static function numberFormat($number)
	{
		$number = str_replace(',', '.', $number);
		return number_format($number, 2);
	}

}
