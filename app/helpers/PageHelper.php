<?php

/**
 * Created by PhpStorm.
 * User: damirseremet
 * Date: 21/10/14
 * Time: 16:25
 */
class PageHelper
{
    public function playerPage($referencedPage)
    {
        $alert_count = Auth::getUser()->notifications()->wherePivot('seen', '=', false)->count();

        $favorited_players = 0;

        if (Auth::user()->player)
            $favorited_players = FavoritesPlayer::where("player_id", Auth::getUser()->player->id)->count();

        $players_count = Player::where("mutua", "!=", 0)->whereNotNull("mutua")->count();

        $this_week_tournaments = PlayerHelper::getTournamentsPerWeek('this week', 'count');
        $next_week_tournaments = PlayerHelper::getTournamentsPerWeek('+1 week', 'count');
        $next_2week_tournaments = PlayerHelper::getTournamentsPerWeek('+2 weeks', 'count');


        $referencedPage->layout->title = "RFET";
        $referencedPage->layout->content = View::make('index_player', array(
            'alert_count' => $alert_count,
            'favorited_players' => $favorited_players,
            'players_count' => $players_count,
            'this_week_tournaments' => $this_week_tournaments,
            'next_week_tournaments' => $next_week_tournaments,
            'next_2week_tournaments' => $next_2week_tournaments,

        ));
    }

    public function refereePage($referencedPage)
    {
        $alert_count = Auth::getUser()->notifications()->wherePivot('seen', '=', false)->count();
        $players_count = TournamentSignup::join('tournaments', 'tournaments.id', '=', 'tournament_signups.tournament_id')
            ->where('tournaments.referee_id', Auth::user()->id)
            ->count();
        $ranking_move_count = Player::whereNotNull("ranking")->orWhere("ranking", "!=", 0)->count();
        $clubs_count = ClubHelper::countClubs();
        $live_score = DrawMatchHelper::getLiveScores();
        $calendar_count = TournamentHelper::countCalendarTournaments(2);
        $assigned_tournaments = TournamentHelper::assignedTournaments();
        $contacts_count = TournamentHelper::calculateByWeeks('', 'count', 'Referee');
        $referees_count = Referee::remember(10, 'ref_referees_count')->count();


        $referencedPage->layout->title = "RFET";
        $referencedPage->layout->content = View::make('index_referee', array(
            'players_count' => $players_count,
            'ranking_move_count' => $ranking_move_count,
            'clubs_count' => $clubs_count,
            'live_score' => $live_score,
            'calendar_count' => $calendar_count,
            'alert_count' => $alert_count,
            'assigned_tournaments' => $assigned_tournaments,
            'contacts_count' => $contacts_count,
            'referees_count' => $referees_count,
        ));
    }

    public function adminPage($referencedPage)
    {
        $this_week = TournamentHelper::calculateByWeeks('this week', 'get');
        $isSuperAdmin = Auth::getUser()->hasRole("superadmin");

        $alert_count = Auth::getUser()->notifications()->wherePivot('seen', '=', false)
            ->count();
        $regions_count = $isSuperAdmin ? Region::remember(10, 'regions_count')->count() : 0;
        $players_count = Player::remember(10, 'players_count_mutua')->where("mutua", "!=", 0)->whereNotNull("mutua")->count();

//        $players_count = Player::remember(10, 'players_count')->count();
        $ranking_move_count = Player::whereNotNull("ranking")
            ->orWhere("ranking", "<>", 0)->remember(10, 'ranking_count')->count();
        $invoices_count = Invoice::getCountUnpaidInvoicesByRole();

        $tournament_count = TournamentHelper::genericCounter();
        $calendar_count = TournamentHelper::countCalendarTournaments(2);
        $clubs_count = ClubHelper::countClubs();
        $signups_count = TournamentHelper::countTotalSignUps(5, true);

        $contacts_count = TournamentHelper::calculateByWeeks('this week', 'count', 'Organizer');

        $live_score = DrawMatchHelper::getLiveScores();

        $tournament_draws_count = TournamentHelper::countTotalDraws(5, true);

        $referees_count = Referee::remember(10, 'referees_count')->count();

        $regional_draws = TournamentDraw::join("tournaments", "tournament_draws.tournament_id", "=", "tournaments.id")->join ("clubs","tournaments.club_id", "=", "clubs.id")->join("regions", "regions.id", "=", "clubs.region_id")
            ->select('*', 'tournament_draws.id as id')
            ->where("regions.id", Auth::getUser()->getUserRegion())
            ->orderBy('tournaments.title')
            ->get();

        $referencedPage->layout->title = "RFET";
        $referencedPage->layout->content = View::make('index', array(
            'this_week' => $this_week,
            'alert_count' => $alert_count,
            'regions_count' => $regions_count,
            'tournament_count' => $tournament_count,
            'players_count' => $players_count,
            'regional_draws' => $regional_draws,
            'calendar_count' => $calendar_count,
            'clubs_count' => $clubs_count,
            'contacts_count' => $contacts_count,
            'signups_count' => $signups_count,
            'tournament_draws_count' => $tournament_draws_count,
            'isSuperAdmin' => $isSuperAdmin,
            'ranking_move_count' => $ranking_move_count,
            'live_score' => $live_score,
            'referees_count' => $referees_count,
            'invoices_count' => $invoices_count != -1 ? $invoices_count : 0,
        ));
    }

    public static function getSortDirection($key)
    {
        return Request::query("sort") == $key && Request::query("or") == "asc" ? "desc" : "asc";
    }

    public static function createSignupListLink($draw_id, $key)
    {
        $link = "/signups/list/" . $draw_id . "?";
        if (Request::query("list_type"))
            $link .= "list_type=" . Request::query("list_type") . "&";

        $link .= "sort=" . $key . "&or=" . self::getSortDirection($key) . "&his_ignore=true";

        return $link;
    }

    public static function createSignupListLinkPublic($key, $tournament_slug, $open_draw = '')
    {
        $link = '/' . $tournament_slug . "/signups?";
        $link .= "sort=" . $key . "&or=" . self::getSortDirection($key);

        if ($open_draw)
            $link .= "&draw=".$open_draw;

        return $link;
    }

    public static function returnClassAndDirection($key)
    {
        if ($key == Request::query("sort") && Request::query("or") == "desc")
            return "<i class='icon-arrow-up-5'></i>";
        elseif ($key == Request::query("sort"))
            return "<i class='icon-arrow-down-5'></i>";

        return "";

    }

    public static function CheckBrowser()
    {
        $browser = Agent::browser();
        if ($browser == 'IE') {
            $version = Agent::version($browser);
            if (intval($version) < 10)
                return View::make("partials.browser_version");
        }

//        echo Debug::vars();die;
    }
}
