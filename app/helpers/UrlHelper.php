<?php

class UrlHelper {

    public static function getDrawsUrl($link, $replaceWith)
    {
        return str_replace("{0}",$replaceWith, $link);
    }

    public static function createBackUrl()
    {
        if(URL::previous())
            return URL::previous();

        return "/";
    }

} 