<?php

class BackButtonHelper
{
    //if ajax call from history.js or direct http request, than save as a path in session
    private static function proceed_loading()
    {
        if (Request::wantsJson())
            return false;

        //ignore history saving
        if(Request::get("his_ignore"))
            return false;

        if ((Request::get('history') || !Request::ajax()) && !Request::isJson() && !Request::get('callback'))
            return true;

        return false;
    }

    public static function new_step()
    {
        if (!self::proceed_loading())
            return;

        self::save_trace_log();

        $url_full = Request::url();
        unset($_GET['history']); // delete history parameter;
        $qs = http_build_query($_GET);
        $qs = $qs ? "?" . $qs : "";
        $url_full = $url_full . $qs;

        if ($url_full == Config::get("app.url")) {
            Session::forget('url_through');
            Session::push('url_through', $url_full);
        } else {

            $previous_pages = Session::get('url_through') ? Session::get('url_through') : "";
            $previous_pages_unique = $previous_pages ? array_unique($previous_pages) : "";


            if ($previous_pages_unique && $url_full != end($previous_pages_unique)) {


                $searched = array_search($url_full, $previous_pages);
                if ($searched) {
                    $reversed = array_reverse($previous_pages, true);

                    foreach ($reversed as $key => $page) {
                        if ($key == $searched)
                            break;
                        unset($previous_pages[$key]);
                    }

                }

                array_push($previous_pages, $url_full);

                Session::put('url_through', array_unique($previous_pages));
            }
        }
    }

    public static function go_back()
    {
        $back = Session::get('url_through');
        if ($back) {
            $back = array_unique($back);
            end($back);
            $latest = prev($back);
            return $latest && is_string($latest) ? $latest : Config::get("app.url");
        }
        return "/";
    }

    private static function save_trace_log()
    {
        if (Config::get("app.enable_trace_log") && Auth::check()) {
            $trace_log = new TraceLog();
            $trace_log->user_id = Auth::getUser()->id;
            $trace_log->ip = Request::getClientIp(true);
            $trace_log->url = URL::full();
            $trace_log->save();
        }
    }

} 