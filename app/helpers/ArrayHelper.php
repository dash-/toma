<?php


class ArrayHelper
{

    public static function swapAssoc($key1, $key2, $array)
    {
        if (!isset($newArray[$key1]))
            return $array;

        if (!isset($newArray[$key2]))
            return $array;

        $newArray = array();
        foreach ($array as $key => $value) {
            if ($key == $key1) {
                $newArray[$key2] = $array[$key2];
            } elseif ($key == $key2) {
                $newArray[$key1] = $array[$key1];
            } else {
                $newArray[$key] = $value;
            }
        }
        return $newArray;
    }

    /**
     * @param &$array array array holder where swapping is done
     * @param $index_a int search index to swap from
     * @param $index_b int search index to swap to
     * @return array swapped array
     */
    public static function swapArrayElements(&$array, $index_a, $index_b)
    {
        $temp = $array[$index_a];
        $array[$index_a] = $array[$index_b];
        $array[$index_b] = $temp;
        return $array;
    }

    /**
     * @param $array pass array to swap in
     * @param $index1 first index
     * @param $index2 second index for swap
     * @return array
     */
    public static function swap(&$array, $index1, $index2)
    {
        $temp = $array[$index1];
        if (isset($array[$index2])) {
            $array[$index1] = $array[$index2];
            $array[$index2] = $temp;
        }
        return $array;
    }

    /**
     * @param $array array array to search from
     * @param $search int search by index from array
     * @param string $akey string default value to search in multidimensional associative array
     * @return bool|int $key mixed int | boolean $key index that is desired to find in array | FALSE in case index is not found
     */
    public static function getKeyByValue($array, $search, $akey = 'id')
    {
        foreach ($array as $key => $product) {
            if ($array[$key][$akey] === $search)
                return $key;
        }
        return false;
    }
} 

