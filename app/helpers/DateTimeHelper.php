<?php
use Illuminate\Support\Facades\Config;

class DateTimeHelper
{

    public static function GetDateNow()
    {
        $dt = new DateTime();
        $o = new ReflectionObject($dt);
        $p = $o->getProperty('date');
        $date = $p->getValue($dt);
        $format = Config::get('localSettings.postgresDateTime');

        return date($format, strtotime($date));

    }

    public static function GetShortDate($datetime)
    {
        $format = Config::get("localSettings.shortDate");
        $date_unix = strtotime($datetime);
        $datetime = date($format, $date_unix);
        return $datetime;
    }

    public static function convertToFormat($datetime, $format)
    {
        $date_unix = strtotime($datetime);
        $datetime = date($format, $date_unix);
        return $datetime;
    }

    public static function GetShortDateFullYear($datetime)
    {
        if (is_null($datetime))
            return '';

        $format = Config::get("localSettings.shortDateFullYear");
        // dd($datetime);

        $date_unix = strtotime($datetime);
        $datetime = date($format, $date_unix);
        return $datetime;
    }

    public static function GetSpanishShortDateFullYear($datetime)
    {
        if (is_null($datetime))
            return '';

        $format = Config::get("localSettings.shortDateFullYear");
        // dd($datetime);

        $date_unix = strtotime($datetime);
        $datetime = date($format, $date_unix);
        return $datetime;
    }

    public static function GetFullDateTime($datetime)
    {
        $format = Config::get("localSettings.fullDateTime");
        $date_unix = strtotime($datetime);
        $date = date($format, $date_unix);

        return $date;
    }

    public static function GetContactsDate($datetime)
    {
        $format = 'n/j/Y';
        $date_unix = strtotime($datetime);
        $date = date($format, $date_unix);

        return $date;
    }

    public static function GetPostgresDate($datetime = false)
    {
        $date_unix = ($datetime) ? strtotime($datetime) : time();
        $date = date('Y-m-d', $date_unix);

        return $date;
    }
    public static function GetOnlyYearDate($datetime = false)
    {
        $date_unix = ($datetime) ? strtotime($datetime) : time();
        $date = date('Y-01-01', $date_unix);

        return $date;
    }
    public static function GetTournamentPeriodDate($datetime = false)
    {
        $date_unix = ($datetime) ? strtotime($datetime) : time();
        $date = date('Y-m-01', $date_unix);

        return $date;
    }

    public static function ConvertSpanishFormatToPos($datetime)
    {
        if ($datetime) {
            if (!str_contains($datetime, "/"))
                return self::GetPostgresDate($datetime);
            $d = explode("/", $datetime);

            $date = $d[2] . '-' . $d[1] . '-' . $d[0] . ' 00:00:00';
            return $date;
        }

        return null;
    }

    public static function GetPostgresDateTime($datetime = false)
    {
        if (!$datetime)
            $date_unix = strtotime(date('Y-m-d 00:00:00', time()));
        else
            $date_unix = strtotime($datetime);

        $date = date('Y-m-d H:i:s', $date_unix);

        return $date;
    }

//    public static function parseContactDateToPostgres($contact_date)
//    {
//
//    }


    public static function GetFullShortDateTime($datetime)
    {
        $format = Config::get("localSettings.fullShortDateTime");
        $date_unix = strtotime($datetime);
        $date = date($format, $date_unix);

        return $date;
    }

    public static function GetOnlyTime($datetime)
    {
        $format = Config::get("localSettings.onlyTime");
        $date_unix = strtotime($datetime);
        $date = date($format, $date_unix);

        return $date;
    }

    public static function GetDifferenceInTime($datetime)
    {
        $date = self::GetPostgresDate($datetime);
        $from = new DateTime($date);
        $to = new DateTime('today');

        return $from->diff($to)->y;
    }

    public static function CompareDifferenceInTime($datetime, $datetime2)
    {
        $date = self::GetOnlyYearDate($datetime);
        $date2 = self::GetOnlyYearDate($datetime2);
        $from = new DateTime($date);
        $to = new DateTime($date2);

        return $from->diff($to)->y;
    }

    public static function GetPostgresDateMinusYear()
    {
        $year_before = strtotime("-1 year", time());
        
        return date('Y-m-d H:i:s', $year_before);
    }

    public static function GetTournamentPeriodMinusYear()
    {
        $year_before = strtotime("-1 year", time());

        return date('Y-m-01', $year_before);
    }

    public static function GetDateFromYear($year, $contact_date = false, $with_timestamp = true)
    {
        $today = new DateTime('today');
        $to = $today->sub(new DateInterval('P' . $year . 'Y'));


        if ($contact_date)
            return self::GetContactsDate($to->format('Y-m-d H:i:s'));
        elseif ($with_timestamp)
            return $to->format('Y-m-d 00:00:00');

        return $to->format('Y-m-d');
    }
    public static function GetYearsForDrawCategories($date, $draw_date, $last_in_year = false)
    {
        $draw_date = new DateTime($draw_date);
        $draw_date->sub(new DateInterval('P'.$date.'Y'));

        if($last_in_year)
            return $draw_date->format('Y-12-31 23:59:59');
        else
            return $draw_date->format('Y-01-01 00:00:00');

    }

    public static function GetFuzzyTime($datetime, $invalid_date = 'a long time ago')
    {

        $date_from = strtotime($datetime);
        $_time_formats = array(
            array(60, 'just now'),
            array(90, '1 minute'),
            array(3600, 'minutes', 60),
            array(5400, '1 hour'),
            array(86400, 'hours', 3600),
            array(129600, '1 day'),
            array(604800, 'days', 86400),
            array(907200, '1 week'),
            array(2628000, 'weeks', 604800),
            array(3942000, '1 month'),
            array(31536000, 'months', 2628000),
            array(47304000, '1 year'),
            array(3153600000, 'years', 31536000),
        );

        $now = time(); // current unix timestamp

        // if a number is passed assume it is a unix time stamp
        // if string is passed try and parse it to unix time stamp
        if (is_numeric($date_from)) {
            $dateFrom = $date_from;
        } elseif (is_string($date_from)) {
            $dateFrom = strtotime($date_from);
        }

        $difference = $now - $dateFrom; // difference between now and the passed time.
        $val = ''; // value to return

        if ($dateFrom <= 0) {
            $val = $invalid_date;
        } else {
            //loop through each format measurement in array
            foreach ($_time_formats as $format) {
                // if the difference from now and passed time is less than first option in format measurment
                if ($difference < $format[0]) {
                    //if the format array item has no calculation value
                    if (count($format) == 2) {
                        $val = $format[1] . ($format[0] === 60 ? '' : ' ago');
                        break;
                    } else {
                        // divide difference by format item value to get number of units
                        $val = ceil($difference / $format[2]) . ' ' . $format[1] . ' ago';
                        break;
                    }
                }
            }
        }
        return $val;
    }

    public static function cleanDate($input)
    {
        $parts = explode('/', $input);
        if (count($parts) != 3) return false;

        $month = (int)$parts[0];
        $day = (int)$parts[1];
        $year = (int)$parts[2];

        if ($year < 100) {
            if ($year < 14) {
                $year += 2000;
            } else {
                $year += 1900;
            }
        }

        // if(!checkdate($month, $day, $year)) return false;

        return sprintf('%01d/%01d/%d', $day, $month, $year);
        // OR
        // $time = mktime(0, 0, 0, $month, $day, $year);
        // return date('j/n/Y', $time);
    }

    public static function getMonths($month = NULL)
    {
        $months = [
            1 => LangHelper::get('january', 'January'),
            2 => LangHelper::get('february', 'February'),
            3 => LangHelper::get('march', 'March'),
            4 => LangHelper::get('april', 'April'),
            5 => LangHelper::get('may', 'May'),
            6 => LangHelper::get('june', 'June'),
            7 => LangHelper::get('july', 'July'),
            8 => LangHelper::get('august', 'August'),
            9 => LangHelper::get('september', 'September'),
            10 => LangHelper::get('october', 'October'),
            11 => LangHelper::get('november', 'November'),
            12 => LangHelper::get('december', 'December'),
        ];

        return (!$month)
            ? [NULL => 'Choose month'] + $months
            : [$month => $months[$month]] + $months;;

    }

    public static function getYears($year = NULL)
    {
        $years = array_combine(range(2014, date('Y')), range(2014, date('Y')));

        return (!$year)
            ? [NULL => 'Choose year'] + $years
            : [$year => $years[$year]] + $years;
    }

    public static function GetJSOnlyDate($datetime)
    {
        $date_unix = strtotime($datetime);
        $datetime = date("Y-m-d", $date_unix);
        return $datetime;
    }

    public static function getOnlyDate($date)
    {

        $date = $date->format(Config::get("localSettings.onlyDate"));
        return $date;
    }

    public static function getOnlyDateFromString($date_string)
    {

        $format = Config::get("localSettings.onlyDate");
        $date_unix = strtotime($date_string);
        $date = date($format, $date_unix);
        return $date;
    }

    public static function castPostgresDate($postgres_datetime)
    {
        $date = new DateTime($postgres_datetime);
        $datetime = $date->format(Config::get("localSettings.onlyDate"));
        return $datetime;
    }

} 