<?php

class DrawMatchHelper
{
    /**
     * Setup initial scores for bracket
     * @param $pairs array
     * @param $draw_id
     * @param $list_type
     * @return void
     */
    public static function setUpInitialScores($pairs, $draw_id, $list_type)
    {
        foreach ($pairs as $key => $pair) {
            // calculate next round position for player
            $next_round_pos = ($pair->round_position % 2 != 0) ? ($pair->round_position + 1) / 2 : $pair->round_position / 2;

            $obj = DrawMatch::where('round_position', $next_round_pos)
                ->where('round_number', $pair->round_number + 1)
                ->where('list_type', $list_type)
                ->where('draw_id', $draw_id)
                ->first();

            $passing_team = max([$pair->team1_id, $pair->team2_id]);
            // position in pair calcuation
            if ($pair->round_position % 2 == 1) {
                $obj->team1_id = $passing_team;
                $data[] = $passing_team;
            } else {
                $obj->team2_id = $passing_team;
                $data[] = $passing_team;
            }

            $obj->update();

        }
    }

    public static function addViolationForTeam($match, $violation_team)
    {

        MatchViolation::where("draw_id", $match->draw_id)
            ->where("round_number", $match->round_number)
            ->where("match_id", $match->id)->delete();

        $violation = array(
            'team_id' => $violation_team,
            'draw_id' => $match->draw_id,
            'round_number' => $match->round_number,
            'tournament_id' => TournamentDraw::find($match->draw_id)->first()->tournament_id,
            'match_id' => $match->id,
            'type_of_violation' => $match->match_status,
        );
        if ($match->match_status)
            MatchViolation::create($violation);
    }

    public static function sendTeamToNextRound($pair)
    {

        // calculate next round position for playershortSinglePlayerName
        $next_round_pos = ($pair->round_position % 2 != 0) ? ($pair->round_position + 1) / 2 : $pair->round_position / 2;

        $obj = DrawMatch::where('round_position', $next_round_pos)
            ->where('round_number', $pair->round_number + 1)
            ->where('list_type', $pair->list_type)
            ->where('draw_id', $pair->draw_id)
            ->first();

        $passing_team = ($pair->team1_score >= $pair->team2_score) ? $pair->team1_id : $pair->team2_id;

        if ($obj != null) {
            // position in pair calcuation
            if ($pair->round_position % 2 == 1) {
                $obj->team1_id = $passing_team;
            } else {
                $obj->team2_id = $passing_team;
            }

            $obj->save();
        }

    }

    public static function getDoublesPair($doubles, $isJS = false, $second_round = false)
    {
        $result = [];
        // echo Debug::vars($doubles->toArray());die;
        foreach ($doubles as $d) {
            if ($second_round)
                $result[] = $d->surname . '. ' . $d->name[0];
            else
                $result[] = $d->surname . ' ' . $d->name;
        }

        if (!$isJS)
            return $value = implode("<br>", $result);
        else
            return $value = implode("\n", $result);
    }

    public static function createScoreHtml($scores)
    {
        if ($scores) {
            $score1 = "<table><tr>";
            $score2 = "<table><tr>";

            foreach ($scores as $score) {
                $score1 .= "<td>" . $score['set_team1_score'];
                $score1 .= $score['set_team1_tie'] != '' || $score['set_team1_tie'] == 0 ? "(" . $score['set_team1_tie'] . ")" : "";
                $score1 .= "</td>";

                $score2 .= "<td>" . $score['set_team2_score'];
                $score2 .= $score['set_team2_tie'] != '' || $score['set_team2_tie'] == 0 ? "(" . $score['set_team2_tie'] . ")" : "";
                $score2 .= "</td>";
            }
            $score1 .= "</tr></table>";
            $score2 .= "</tr></table>";

            return array('score1' => $score1, 'score2' => $score2);
        }
    }

    public static function createScoreHtmlForBracket($scores, DrawMatch $match = null, $match_status = 0)
    {
        $score_str = '';
        if ($scores) {
            $score_str = "<table><tr>";
            foreach ($scores as $score) {
                $first_place = $score['set_team1_score'];
                $second_place = $score['set_team2_score'];
                $tie_break_score = -1;
                if ($match && $match->team2_score > $match->team1_score) {
                    $first_place = $score['set_team2_score'];
                    $second_place = $score['set_team1_score'];
                }
                if ($score['set_team1_tie'] || $score['set_team2_tie']) {
                    if ($score['set_team1_tie'] < $score['set_team2_tie'])
                        $tie_break_score = $score['set_team1_tie'];
                    else
                        $tie_break_score = $score['set_team2_tie'];
                }

                $score_str .= "<td>" . $first_place . $second_place;
                $score_str .= $tie_break_score != -1 ? "(" . $tie_break_score . ")" : "";
                $score_str .= "</td>";
            }
            if ($match_status == 3)
                $score_str .= "<td>" . $match->match_status() . "</td>";

            $score_str .= "</tr></table>";
        } elseif ($match_status) {
            return $match->match_status();
        }

        return $score_str;
    }

    public static function createMatchStatusHtml($match)
    {
        if ($match->match_status > 0) {
            $score_str = "<table><tr>";
            $score_str .= "<td>" . $match->match_status() . "</td>";
            $score_str .= "</tr></table>";

            return $score_str;

        }

    }

    public static function scoreAsString($match)
    {
        if ($match->match_status > 0) {
            if ($match->match_status == 1) {
                $team1_status = ($match->team1_score > $match->team2_score) ? 'W.O.' : '';
                $team2_status = ($match->team1_score > $match->team2_score) ? '' : 'W.O.';
            } else {
                $team1_status = ($match->team1_score > $match->team2_score) ? '' : 'INJ';
                $team2_status = ($match->team1_score > $match->team2_score) ? 'INJ' : '';
            }

            return $team1_status . ' ' . $team2_status;
        } else {
            $score1 = "";
            foreach ($match->scores as $score) {
                $score1 .= $score['set_team1_score'];
                $score1 .= $score['set_team1_tie'] != '' ? "(" . $score['set_team1_tie'] . ")" : "";
                $score1 .= '-';
                $score1 .= $score['set_team2_score'];
                $score1 .= $score['set_team2_tie'] != '' ? "(" . $score['set_team2_tie'] . ")" : "";
                $score1 .= "; ";
            }

            return $score1;
        }
    }

    public static function checkWinner($draw, $list_type, $consolation = false)
    {
        // get final round
        $pair = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
            ->where('draw_id', $draw->id);


        $pair = ($consolation)
            ? $pair->where('consolation_type', $list_type)
            : $pair->where('list_type', $list_type);


        $pair = $pair->orderBy('id', 'desc')
            ->first();
        if ($pair->team1_score OR $pair->team2_score) {
            if ($draw->draw_type == 'singles') {
                if ($pair->team1_score >= $pair->team2_score) {
                    $name = ($pair->team1_data) ? $pair->team1_data->full_name() : "";
                    $id = $pair->team1_id;

                } else {
                    $name = ($pair->team2_data) ? $pair->team2_data->full_name() : "";
                    $id = $pair->team2_id;
                }
            } else {
                if ($pair->team1_score >= $pair->team2_score) {
                    if ($pair->team1_data)
                        $name = DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $pair->team1_data->team_id)->get());
                    else
                        $name = "";

                    $id = $pair->team1_id;
                } else {
                    if ($pair->team2_data)
                        $name = DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $pair->team2_data->team_id)->get());
                    else
                        $name = "";

                    $id = $pair->team2_id;
                }

            }

            return [
                array('name' => $name, 'id' => $id, 'matchid' => $pair->id)
            ];
        } else
            return [['name' => '', 'id' => '']];
    }

    public static function retrieveTeamId($draw_id, $player_id)
    {
        return TournamentHistory::whereDrawId($draw_id)->wherePlayerId($player_id)->first()->team_id;
    }

    public static function matchStatusByTeamId($match, $team_id)
    {
        if ($team_id == $match->team1_id AND $match->team1_score > $match->team2_score)
            return 'W';
        elseif ($team_id == $match->team2_id AND $match->team1_score < $match->team2_score)
            return 'W';
        elseif ($team_id == $match->team1_id AND !$match->team2_id)
            return "BYE";
        else
            return 'L';
    }

    public static function getVersusName($match, $team_id)
    {
        $get_id = ($match->team1_id != $team_id) ? $match->team1_id : $match->team2_id;

        if ($get_id != 0)
            return self::getDoublesPair(TournamentTeam::whereTeamId($get_id)->get());
    }

    public static function listDoublesCoplayer($team_id, $licence_number)
    {
        $coplayer = TournamentTeam::whereTeamId($team_id)->where('licence_number', '!=', $licence_number)->first();

        return ($coplayer) ? $coplayer->full_name() : false;
    }

    public static function getScoresInString($scores)
    {
        $result = [];
        foreach ($scores as $score) {
            $result[] = $score->set_team1_score . '-' . $score->set_team2_score;
        }

        return implode('; ', $result);
    }


    public static function getBracketPairs($pairs, TournamentDraw $draw, $list_type, $seed_ids, $consolation_draw = false)
    {
        $counter = 1;
        $temp_pairs_scores = array();
        foreach ($pairs as $pair) {
            $seed_class1 = (in_array($pair->team1_id, array_keys($seed_ids))) ? 'seeded-player' : '';
            $seed_class2 = (in_array($pair->team2_id, array_keys($seed_ids))) ? 'seeded-player' : '';

            // If player is seed add seed number
            $seed_num1 = in_array($pair->team1_id, array_keys($seed_ids)) ? '[' . ($seed_ids[$pair->team1_id] + 1) . ']' : "";
            $seed_num2 = in_array($pair->team2_id, array_keys($seed_ids)) ? '[' . ($seed_ids[$pair->team2_id] + 1) . ']' : "";
            $qualy_val1 = "";
            $qualy_val2 = "";
            $qualy_val1_class = $qualy_val2_class = "";

            if ($pair->round_number == 1) {
                if ($pair->team1_id > 0) {
                    if ($pair->team1->move_from_qualifiers_type > 0 OR $pair->team1->status > 0) {
                        $qualy_val1_class = 'has_qualy';
                        if (isset($pair->team1) AND $pair->team1->move_from_qualifiers_type == 2)
                            $qualy_val1 = '(Q)';
                        elseif (isset($pair->team1) AND $pair->team1->move_from_qualifiers_type == 3)
                            $qualy_val1 = '(LL)';
                        elseif (isset($pair->team1) AND in_array($pair->team1->status, [1, 2]))
                            $qualy_val1 = '(WC)';
                        else
                            $qualy_val1 = $qualy_val1_class = "";

                    }
                }


                if ($pair->team2_id > 0) {
                    if ($pair->team2->move_from_qualifiers_type > 0 OR $pair->team2->status > 0) {
                        $qualy_val2_class = 'has_qualy';
                        if ($pair->team2->move_from_qualifiers_type == 2)
                            $qualy_val2 = '(Q)';
                        elseif ($pair->team2->move_from_qualifiers_type == 3)
                            $qualy_val2 = '(LL)';
                        elseif (isset($pair->team1) AND in_array($pair->team1->status, [1, 2]))
                            $qualy_val2 = '(WC)';
                        else
                            $qualy_val2 = $qualy_val2_class = "";
                    }
                }


                if ($draw->draw_type == 'singles') {
                    $qualy_val1_class = $qualy_val2_class = "qualy_first_round";
                    if ($pair->team1_data)
                        $name1 = $pair->team1_data->full_name();
                    else {
                        if ($pair->team1_id == 0)
                            $name1 = 'Bye';
                        elseif ($pair->team1_id == -1)
                            $name1 = 'Q';
                        elseif ($pair->team1_id == -2)
                            $name1 = 'Q/LL';
                        elseif ($pair->team1_id == -3)
                            $name1 = 'LL';
                    }
                    //team 2
                    if ($pair->team2_data)
                        $name2 = $pair->team2_data->full_name();
                    else {
                        if ($pair->team2_id == 0)
                            $name2 = 'Bye';
                        elseif ($pair->team2_id == -1)
                            $name2 = 'Q';
                        elseif ($pair->team2_id == -2)
                            $name2 = 'Q/LL';
                        elseif ($pair->team2_id == -3)
                            $name2 = 'LL';
                    }

                } else {
                    $qualy_val1_class = $qualy_val2_class = "qualy_first_round";

                    if ($pair->team1_data)
                        $name1 = self::getDoublesPair(TournamentTeam::where('team_id', $pair->team1_data->team_id)->get());
                    else {
                        if ($pair->team1_id == 0)
                            $name1 = "Bye<br/>&nbsp;";
                        elseif ($pair->team1_id == -1)
                            $name1 = "Q<br/>&nbsp;";
                        elseif ($pair->team1_id == -2)
                            $name1 = "Q/LL<br/>&nbsp;";
                        elseif ($pair->team1_id == -3)
                            $name1 = "LL<br/>&nbsp;";
                    }

                    if ($pair->team2_data)
                        $name2 = self::getDoublesPair(TournamentTeam::where('team_id', $pair->team2_data->team_id)->get());
                    else {
                        if ($pair->team2_id == 0)
                            $name2 = "Bye<br/>&nbsp;";
                        elseif ($pair->team2_id == -1)
                            $name2 = "Q<br/>&nbsp;";
                        elseif ($pair->team2_id == -2)
                            $name2 = "Q/LL<br/>&nbsp;";
                        elseif ($pair->team2_id == -3)
                            $name2 = "LL<br/>&nbsp;";
                    }

                }
                $seed1 = $counter++;
                $seed2 = $counter++;
                $id1 = ($pair->team1_id == 0) ? Str::random(13) : $pair->team1_id;
                $id2 = ($pair->team2_id == 0) ? Str::random(13) : $pair->team2_id;
            } else {
                $seed1 = '';
                $seed2 = '';
                if ($draw->draw_type == 'singles') {
                    $name1 = ($pair->team1_data) ? $pair->team1_data->short_name() : "";
                    $name2 = ($pair->team2_data) ? $pair->team2_data->short_name() : "";
                } else {
                    if ($pair->team1_data)
                        $name1 = self::getDoublesPair(TournamentTeam::where('team_id', $pair->team1_data->team_id)->get(), false, true);
                    else
                        $name1 = "";

                    if ($pair->team2_data)
                        $name2 = self::getDoublesPair(TournamentTeam::where('team_id', $pair->team2_data->team_id)->get(), false, true);
                    else
                        $name2 = "";
                }

                $id1 = $pair->team1_id;
                $id2 = $pair->team2_id;
            }

            if ($pair->match_status == 0 || $pair->match_status == 3) {
                $scores = self::createScoreHtmlForBracket($pair->scores->toArray(), $pair, $pair->match_status);
            } else {
                $scores = self::createMatchStatusHtml($pair);
            }
            $team1_previous_scores = '';
            $team2_previous_scores = '';
            if ($pair->round_number > 1) {
                if ($pair->team1_id)
                    $team1_previous_scores = $temp_pairs_scores[$pair->team1_id];
                if ($pair->team2_id)
                    $team2_previous_scores = $temp_pairs_scores[$pair->team2_id];

            }

            $temp_pairs_scores[$pair->team1_id] = $scores;
            $temp_pairs_scores[$pair->team2_id] = $scores;

            $jsArray[$pair->round_number][] = [
                array(
                    'name' => $name1,
                    'id' => $id1,
                    'matchid' => $pair->id,
                    'score' => $team1_previous_scores,
                    'seed' => $seed1,
                    'seed_num' => $seed_num1,
                    'seed_class' => $seed_class1,
                    "round_number" => $pair->round_number,
                    'qualy_val' => $qualy_val1,
                    'qualy_class' => $qualy_val1_class,
                ),
                array(
                    'name' => $name2,
                    'id' => $id2,
                    'matchid' => $pair->id,
                    'score' => $team2_previous_scores,
                    'seed' => $seed2,
                    'seed_num' => $seed_num2,
                    'seed_class' => $seed_class2,
                    "round_number" => $pair->round_number,
                    'qualy_val' => $qualy_val2,
                    'qualy_class' => $qualy_val2_class,
                )
            ];
        }

        //must add one more place for winner for bracket to work properly

        $winner = ($consolation_draw)
            ? self::checkWinner($draw, $list_type, true)
            : self::checkWinner($draw, $list_type);
        $winner[0]['winner_score'] = isset($winner[0]['matchid']) ? $temp_pairs_scores[$winner[0]['id']] : "";
        $jsArray[][] = $winner;

        return $jsArray;
    }

    public static function moveTeamsFromQualifyingToMainDraw($draw)
    {
        $players_to_take = $draw->size->accepted_qualifiers;

        $how_many_to_take = $players_to_take;

        if ($players_to_take % 2 == 0)
            $players_to_take = $players_to_take / 2;
        else
            $players_to_take = ($players_to_take + 1) / 2;

        $matches = DrawMatch::with('team1', 'team2')
            ->where(function ($subQuery) {
                $subQuery->whereHas('team1', function ($query) {
                    $query->where('move_from_qualifiers_type', '=', 0);
                })->orWhereHas('team2', function ($query) {
                    $query->where('move_from_qualifiers_type', '=', 0);
                });
            })->whereDrawId($draw->id)
            ->whereListType(2)
            ->where("team1_id", '>', 0)
            ->where("team2_id", '>', 0)
            ->orderBy('round_number', 'desc')
            ->lists('team1_id', 'team2_id');

        $lucky_loser_keys = [];

        foreach ($matches as $key => $match) {
            $lucky_loser_keys[] = $key;
            $lucky_loser_keys[] = $match;
        }

        $lucky_loser_keys = array_slice(array_unique($lucky_loser_keys), 0, $how_many_to_take);
        if (count($lucky_loser_keys)) {
            $teams = TournamentSignup::whereIn('id', $lucky_loser_keys)
                ->where('move_from_qualifiers_type', 0)
                ->orderBy('rank')
                ->take($how_many_to_take)
                ->get();

            foreach ($teams as $team) {
                $team->move_from_qualifiers_type = 2;
                $team->save();
            }

            return true;
        }

        return false;
    }

    public static function checkIfWinnerInNextRoundExist($match)
    {
        $team1_id = $match->team1_id;
        $team2_id = $match->team2_id;


        $next_round = DrawMatch::where('draw_id', $match->draw_id)
            ->where('round_number', $match->round_number + 1)
            ->where('list_type', $match->list_type)
            ->where(function ($query) use ($team1_id, $team2_id) {
                $query->where('team1_id', $team1_id)
                    ->orWhere('team2_id', $team2_id)
                    ->orWhere('team1_id', $team2_id)
                    ->orWhere('team2_id', $team1_id);
            })
            ->first();

        if ($team1_id == 0 || $team2_id == 0)
            return 'empty';

        if ($next_round) {
            if (($next_round->team1_score > $next_round->team2_score) OR ($next_round->team1_score < $next_round->team2_score))
                return true;
        }

        return false;
    }

    public static function getDragDropName($team_model)
    {
        $result = [];
        foreach ($team_model['teams'] as $team) {
            $qualifier = ($team_model['move_from_qualifiers_type'] == 2) ? ' (Q)' : null;
            $wc_status = (is_object($team_model)) ? SignupHelper::wildCardStatus($team_model->toArray()) : '';
            $result[] = $qualifier . '' . $wc_status . ' ' . $team['surname'] . '. ' . substr($team['name'], 0, 1)
                . ' (' . $team_model['ranking_points'] . ' pts)';
        }

        return $value = implode("\n", $result);
    }

    public static function fillSeedNum($num_of_seeds, $player_order)
    {
        if ($player_order <= $num_of_seeds)
            return $player_order;

        return '';
    }

    public static function sameSeedPoints($num_of_seeds, $player_order, $before_order, $temp_points, $current_points, $return_seed_num = false)
    {
        if ($player_order <= $num_of_seeds) {
            if (!$return_seed_num)
                return ($temp_points == $current_points) ? 'team-holder-danger' : '';
            else {
                if ($current_points == 0)
                    return "";

                return ($temp_points == $current_points) ? $before_order : $player_order;
            }
        }

        return '';
    }

    public static function getSchedulePairs($pairs_array)
    {
        $item = [];
        $result = [];
        foreach ($pairs_array as $key => $value) {
            // echo Debug::vars($value->toArray());die;
            $item['team1'] = self::getTeamNames($value->team1_data);
            $item['team2'] = self::getTeamNames($value->team2_data);
            $item['match_id'] = $value->id;

            $result[] = $item;
        }

        return $result;
    }

    public static function getSchedulePairsDropdown($pairs_array, $loaded_data)
    {
        $result = [];
        $already_loaded = $loaded_data->lists('match_id', 'match_id');

        $key2 = 0;
        foreach ($pairs_array as $key => $value) {
            $key = $key2;
            $key = $key + 1;

            $key2 = $key + 1;

            if ($value->team1_id == 0 || $value->team2_id == 0)
                continue;

            $result[$value->id] =
                '(' . $key . ')' . self::getTeamNames($value->team1_data, true)
                . ' vs ' . '(' . $key2 . ')' . self::getTeamNames($value->team2_data, true);

        }

        foreach ($result as $key => $r) {
            if (in_array($key, $already_loaded)) {
                unset($result[$key]);
            }
        }

        return $result;
    }

    public static function getTeamNames($team, $short = false)
    {
        $result = [];

        if (is_null($team))
            return 'BYE';

        if (isset($team[0])) {

            foreach ($team as $d) {
                $result[] = ($short) ? substr($d->name, 0, 1) . '. ' . $d->surname : $d->name . ' ' . $d->surname;
            }

        } else {
            $result[] = ($short) ? substr($team->name, 0, 1) . '. ' . $team->surname : $team->name . ' ' . $team->surname;
        }

        return implode("<br>", $result);
    }

    public static function getPlaceholderNames($team_id)
    {
        if ($team_id == 0)
            return 'Bye';
        elseif ($team_id == -1)
            return 'Q';
        elseif ($team_id == -2)
            return 'Q/LL';
        elseif ($team_id == -3)
            return 'LL';
    }

    /**
     * @param $data array
     * @return string
     */
    public static function getScheduleTime($data)
    {

        if (isset($data['type_of_time'])) {

            $type_of_time = $data['type_of_time'];
            $time_of_play = isset($data['time_of_play']) ? $data['time_of_play'] : "10:00";


            switch ($type_of_time) {
                case 1:
                    return 'Starting at ' . $time_of_play;
                    break;

                case 2:
                    return 'Not before ' . $time_of_play;
                    break;

                default:
                    return 'Followed by';
                    break;
            }

        }

    }

    public static function getScheduleData($court_num, $order, $tournament_id, $date, $list_type = 2, $return_type = "boolean")
    {

        $date = str_replace('_', ' ', $date);

        $scheduled_match_query = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.team1_data_array', 'match.team2_data_array', 'match.scores')
            ->where('court_number', $court_num + 1)
            ->where('order_of_play', $order + 1)
            ->where('tournament_id', $tournament_id)
            ->whereRaw("date_of_play::date = '" . DateTimeHelper::GetPostgresDate($date) . "'");
//            ->where('date_of_play', $date);


        if ($list_type != 0)
            $scheduled_match_query->where('list_type', $list_type);

        $scheduled_match = $scheduled_match_query->first();

        if ($return_type == 'boolean')
            return is_null($scheduled_match) ? false : true;


        return $scheduled_match;

    }

    public static function checkScheduleDataByDates($date, $tournament_id)
    {
        return MatchSchedule::where('tournament_id', $tournament_id)
//            ->whereRaw('date_of_play', $date)
            ->whereRaw("date_of_play::date = '" . DateTimeHelper::GetPostgresDate($date) . "'")
            ->count();
    }

    public static function checkIfScheduleExist($tournament_id, $list_type)
    {
        return MatchSchedule::where('tournament_id', $tournament_id)
            ->where('list_type', $list_type)
            ->count();

    }

    public static function scheduleExist($schedules, $list_type)
    {
        $checker = $schedules->filter(function ($schedule) use ($list_type) {
            if ($schedule->list_type == $list_type)
                return true;
        });

        return !$checker->isEmpty();
    }


    public static function getMatchStatus($match_data)
    {
        if (!is_null($match_data)) {
            return ($match_data->match_status) ? 'background-color: ' . $match_data->color() : null;
        }

        return false;

    }

    public static function getNotStarted($match_data)
    {
        if ($match_data->match_status == 3) {
            return 'schedule-status';
        } else {
            return 'not-started';
        }
    }


    public static function getPlayerImage($licence_number)
    {
        $player = Player::with('contact')
            ->where('licence_number', $licence_number)
            ->first();

        if (!is_null($player))
            return $player->contact->image_link;

        return false;


    }

    public static function getLiveScores($tournament_id = '', $draw_type = '')
    {
        if ($tournament_id && !$draw_type) {
            return MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
//                ->where('match_status', '!=', 4)
                ->where('tournament_id', $tournament_id)
                ->orderBy('date_of_play')
                ->get();
        } elseif ($tournament_id && $draw_type) {
            return MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
//                ->where('match_status', '!=', 4)
                ->where('list_type', $draw_type)
                ->where('tournament_id', $tournament_id)
                ->orderBy('date_of_play')
                ->get();
        } else {
            if (Auth::getUser()->hasRole('superadmin')) {

                $livescore = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
                    ->where('match_status', 3)
                    ->orderBy('date_of_play')
                    ->take(10)
                    ->get();

                return $livescore;

            } elseif (Auth::getUser()->hasRole('referee')) {

                $tournaments = Tournament::select("id")->whereRefereeId(Auth::getUser()->id)->get();
                $tournament_ids = (count($tournaments)) ? array_pluck($tournaments, 'id') : array(0);

                return MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
                    ->whereIn('tournament_id', $tournament_ids)
                    ->orderBy('date_of_play')
                    ->take(10)
                    ->get();
            }
        }

        // } else {

        // return $tournament = Tournament::leftJoin("clubs", "tournaments.club_id", "=", "clubs.id")
        //     ->leftJoin("regions", "regions.id", "=", "clubs.region_id")
        //     ->where("regions.id", Auth::getUser()
        //     ->getUserRegion())
        //     ->whereStatus(2)
        //     ->select("*", "tournaments.id as id")
        //     ->get();
        // }

    }


    public static function shorten($string)
    {

        $explode = explode(' ', $string);

        return $explode[0];

    }

    public static function matchTie($tie)
    {
        if ($tie != null || is_int($tie)) {
            return '(' . $tie . ')';
        }
    }

    public static function saveToDrawHistory($draw_id, $list_type)
    {
        $draw_history = new DrawHistory;
        $draw_history->draw_id = $draw_id;
        $draw_history->list_type = $list_type;
        $draw_history->submitted_by = Auth::user()->id;

        $draw_history->save();
    }

    public static function getLuckyLosers($draw_id, $list_type)
    {
        if ($list_type == 2 || $list_type == 1) {
            return TournamentSignup::whereDrawId($draw_id)
                ->whereListType(2)
                ->where('move_from_qualifiers_type', '>', 0)
                ->get();
        } else
            return null;
    }

    public static function checkIfDrawIsFinished($draw_id, $list_type)
    {
        $lucky_losers_count = count(self::getLuckyLosers($draw_id, $list_type));

        if (!Auth::user()->hasRole('superadmin')) {
            if ($list_type == 2 && $lucky_losers_count > 0) {
                return Response::json([
                    "qualifications_finished" => true,
                    "message" => LangHelper::get('draw_finished', 'Draw finished'),
                ]);
            }
        }

        return false;
    }

    public static function getStartingDateTime(MatchSchedule $schedule)
    {
        return DateTimeHelper::GetShortDateFullYear($schedule->date_of_play) . ' - ' . $schedule->time_of_play;
    }

    public static function getManualDrawPositionNumbers($signups_count)
    {
        $counter = Draw::roundUpToNextPowerOfTwo($signups_count);
        for ($i = 0; $i < $counter; $i++) {
            $item['id'] = $i;
            $item['vrijednost'] = $i + 1;

            $positions[] = $item;
        }

        return $positions;
    }

    public static function getManualDrawQualiPositions($pairs)
    {
        $quali_placeholders = [];
        $positions = [];
        for ($i = 0; $i < count($pairs); $i++) {
            if ($pairs[$i]['team1_id'] < 0) {
                $quali_placeholders[] = $i * 2 + 1;
            } elseif ($pairs[$i]['team2_id'] < 0) {
                $quali_placeholders[] = $i * 2 + 2;
            }
        }

        for ($i = 0; $i < count($quali_placeholders); $i++) {
            if (isset($quali_placeholders[$i])) {
                $item['id'] = $quali_placeholders[$i] - 1;
                $item['vrijednost'] = $quali_placeholders[$i];

                $positions[] = $item;
            }
        }

        return $positions;
    }

    public static function checkIfDrawHasScores($draw_id, $list_type)
    {
        $match_with_score_counter = DrawMatch::whereDrawId($draw_id)
            ->whereListType($list_type)
            ->where(function ($query) {
                $query->where('team1_score', '>', 0);
                $query->orWhere('team2_score', '>', 0);
            })
            ->count();

        return ($match_with_score_counter > 0) ? true : false;
    }

    /**
     * @param $matches Illuminate\Database\Eloquent\Collection
     * @param $list_type integer
     * @param $consolation bool
     */
    public static function undoMatches($matches, $list_type, $consolation = false)
    {
        foreach ($matches as $match) {
            $seeds = DrawSeed::whereIn('signup_id', [$match->team1_id, $match->team2_id]);

            $seeds = ($consolation)
                ? $seeds->whereConsolationType($list_type)
                : $seeds->whereListType($list_type);

            $seeds = $seeds->get();

            foreach ($seeds as $seed) {
                $seed->delete();
            }
            $match->delete();
        }
    }

    public static function extractTeamids($pairs)
    {
        $result = [];
        if (count($pairs)) {
            foreach ($pairs as $pair) {
                $result[] = $pair->team1_id;
                $result[] = $pair->team2_id;
            }
        }

        return $result;
    }

    public static function manualBracketNewPlayers($pairs, $draw_id)
    {
        $team_ids = self::extractTeamids($pairs);
        if (count($team_ids)) {
            $signups = TournamentSignup::with('teams')
                ->whereDrawId($draw_id)
                ->whereListType(1)
                ->whereNotIn('id', $team_ids)
                ->get()
                ->toArray();
            $new_players = [];
            foreach ($signups as $player) {
                $item['id'] = $player['id'];
                $item['drag'] = false;
                $item['name'] = DrawMatchHelper::getDragDropName($player);
                $item['seed_num'] = '';
                $item['move_from_qualifiers_type'] = $player['move_from_qualifiers_type'];
                $new_players[] = $item;
            }

            return $new_players;
        }

        return [];
    }

    public static function getContestants($match)
    {
//     try {
        if ($match->team1_data)
            $str = substr($match->team1_data->name, 0, 1) . '. ' . self::shorten($match->team1_data->surname);
        $str .= '<div class="vs live">vs</div>';
        if ($match->team2_data)
            $str .= substr($match->team2_data->name, 0, 1) . '. ' . self::shorten($match->team2_data->surname);

        return $str;
//     }
//     catch(Exception $ex){
//         return '';
//     }
    }

    /**
     * @param $schedules_collection all schedules for this day
     * @param $row
     * @param $court_number
     * @param bool $waiting_list
     * @return mixed bool|collection
     */
    public static function getScheduledMatch($schedules_collection, $row, $court_number, $waiting_list = false)
    {

        foreach ($schedules_collection as $schedule) {
            if ($waiting_list) {
                if ($schedule->waiting_list_row == $row AND $schedule->waiting_list_court_number == $court_number)
                    return $schedule;
            } else {
                if ($schedule->row == $row AND $schedule->court_number == $court_number)
                    return $schedule;
            }
        }

        return false;
    }


    public static function shortSinglePlayerName($team_name)
    {
        if (!is_null($team_name))
            return substr($team_name->name, 0, 1) . '. ' . self::shorten($team_name->surname);

        return 'BYE';
    }

    public static function fullSinglePlayerName($team_name)
    {
        if (!is_null($team_name))
            return $team_name->name . ' ' . $team_name->surname;

        return 'BYE';
    }

    public static function scheduleName($match, $which_team = 'team1_data', $all_matches, $second_match = false)
    {
        if ($match && is_null($match->$which_team)) {
            $previous_round = $match->round_number - 1;
            $first_winner = ($match->round_position * 2) - 1;
            $second_winner = $match->round_position * 2;
            $winners = [];

            foreach ($all_matches as $m) {
                if ($m->round_position == $first_winner AND $m->round_number == $previous_round AND $second_match == false) {
                    if ($m->team1_data && $m->team2_data)
                        return '<i> W: ' . $m->team1_data->surname . ' / <br/> ' . $m->team2_data->surname . '</i>';
                    else
                        return 'W: ' . LangHelper::get('round', 'Round') . ' ' . $m->round_number . ';' . LangHelper::get('match', 'Match') . ' ' . $m->round_position;
                } else if ($m->round_position == $second_winner AND $m->round_number == $previous_round AND $second_match == true) {
                    if ($m->team1_data && $m->team2_data)
                        return '<i>W: ' . $m->team1_data->surname . ' / <br/>' . $m->team2_data->surname . '</i>';
                    else
                        return 'W: ' . LangHelper::get('round', 'Round') . ' ' . $m->round_number . ';' . LangHelper::get('match', 'Match') . ' ' . $m->round_position;
                }
            }
        }

        if ($match)
            return self::fullSinglePlayerName($match->$which_team);
        return "";
    }

    public static function checkIfPlayerHasMoreMatches($schedules, $schedule, $match = false)
    {
        $item = [];

        if ($schedule && $schedule->match) {
            $team1_licence_number = ($schedule->match->team1_data) ? $schedule->match->team1_data->licence_number : false;
            $team2_licence_number = ($schedule->match->team2_data) ? $schedule->match->team2_data->licence_number : false;
        } else {
            $team1_licence_number = ($match && $match->team1_data) ? $match->team1_data->licence_number : false;
            $team2_licence_number = ($match && $match->team2_data) ? $match->team2_data->licence_number : false;

            if ($match && !in_array($match->id, array_pluck($schedules, 'match_id')))
                $item = [$team1_licence_number, $team2_licence_number];
        }

        $licence_numbers = [$team1_licence_number, $team2_licence_number];
        if ($match) {
            foreach ($schedules as $sc) {
                $item[] = ($sc->match->team1_data) ? $sc->match->team1_data->licence_number : null;
                $item[] = ($sc->match->team2_data) ? $sc->match->team2_data->licence_number : null;
            }
        }
        $item = array_count_values(array_filter($item));

        if ((isset($item[$team1_licence_number]) AND $item[$team1_licence_number] > 1) OR
            (isset($item[$team2_licence_number]) AND $item[$team2_licence_number] > 1)
        )
            return true;

        return false;
    }

    public static function checkIfMatchCantBePlayed($schedules, $match, $time_of_play, $type_of_time, $row)
    {
        $team1_licence_number = ($match->team1_data) ? $match->team1_data->licence_number : false;
        $team2_licence_number = ($match->team2_data) ? $match->team2_data->licence_number : false;

        if (!$team2_licence_number AND !$team1_licence_number)
            return false;

//        $time_of_play_minus_one = date("H:i", strtotime($time_of_play) - 3600);

        $schedules = $schedules->filter(function ($schedule) use ($match) {
            return ($schedule->match_id == $match->id) ? false : true;
        });
        foreach ($schedules as $schedule) {
            $licence_checker = self::licenceNumberTroughSchedule($schedule, 'team1_data') == $team1_licence_number OR
            self::licenceNumberTroughSchedule($schedule, 'team1_data') == $team2_licence_number OR
            self::licenceNumberTroughSchedule($schedule, 'team2_data') == $team1_licence_number OR
            self::licenceNumberTroughSchedule($schedule, 'team2_data') == $team2_licence_number;

            if ($type_of_time != 3) {
                if ($schedule->getTime() >= $time_of_play OR $time_of_play - $schedule->getTime() <= 1) {
                    if ($licence_checker)
                        return true;
                }
            } elseif ($type_of_time == 3) {
                if ($schedule->row == $row - 1 OR $schedule->row == $row) {
                    if ($licence_checker)
                        return true;
                }
            }
        }

        return false;
    }

    private static function licenceNumberTroughSchedule($schedule, $team_data = 'team1_data')
    {
        return ($schedule->match->$team_data) ? $schedule->match->$team_data->licence_number : null;
    }

    public static function ShowIfRetired($match)
    {
        if ($match->match_status == 3)
            return $match->match_status();
    }
}

