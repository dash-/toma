<?php

class PlayerModuleHelper
{
    public static function getPlayerStatistics($player_id, $date_from = null, $date_to = null)
    {
        $date_range_full = PlayerRankingHistory::select("created_at")->where("player_id", $player_id)->orderBy("created_at")->get();

        $statistics_query = PlayerRankingHistory::where("player_id", $player_id);
        if($date_from)
            $statistics_query->where("created_at", ">=", $date_from);
        if($date_to)
            $statistics_query->where("created_at", "<=", $date_to);

        $statistics = $statistics_query->orderBy("created_at")->get();

        $ranking_points = array_pluck($statistics->toArray(), "ranking_points");
        $ranking = array_pluck($statistics->toArray(), "ranking");
        $date_range = array_pluck($statistics->toArray(), "created_at");
        $date_range_full_formatted = [];
        foreach ($date_range_full as $date) {
            $date_range_full_formatted[$date->created_at->format("Y-m-d")] = DateTimeHelper::getOnlyDateFromString($date->created_at);
        }
        $date_range_formatted = [];
        foreach ($date_range as $date) {
            $date_range_formatted[$date->format("Y-m-d")] = DateTimeHelper::getOnlyDateFromString($date);
        }


        return array(
            'statistics' => $statistics,
            'ranking' => $ranking,
            'ranking_points' => $ranking_points,
            'date_range' => $date_range_formatted,
            'date_range_full' => $date_range_full_formatted,
            'date_from' => $date_from,
            'date_to' => $date_to,
        );
    }
}