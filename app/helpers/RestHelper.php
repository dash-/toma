<?php

class RestHelper
{
    /**
    * parse comments and return array for json response
    * @param Collection $comments
    * @return array $data
    **/
    
    public static function getComments($comments, $single_comment = FALSE)
    {
        $data = [];

        // if we receive single comment parse it without foreach
        if ($single_comment) {
            $data[] = self::generateCommentArray($comments);
        } else {
            foreach ($comments as $comment) {
                $data[] = self::generateCommentArray($comment);
            }
        }
        return $data;
    }

    public static function generateCommentArray($comment)
    {
        $item['id'] = $comment->id;
        $item['created_at'] = DateTimeHelper::GetShortDateFullYear($comment->created_at);
        $item['text'] = $comment->text;
        $item['user_id'] = $comment->user_id;
        $item['username'] = ($comment->user->info('name')) ? $comment->user->info('name').' '.$comment->user->info('surname') : $comment->user->username;
        $item['remove_allow'] = ($comment->user_id == Auth::user()->id OR Auth::user()->hasRole('superadmin')) ? true : false;

        return $item;
    }
  
} 