<?php

class PermissionHelper
{

    public static function noPermissionPage($thisPage, $title = 'No Permission')
    {
        $thisPage->layout->title = $title;
        $thisPage->layout->content = View::make("errors/no_permission");
    }

    public static function checkPagePermission($tournament)
    {
        if (Auth::getUser()->hasRole("superadmin"))
            return false;
        elseif(Auth::getUser()->hasRole("referee") && $tournament->referee_id == Auth::getUser()->id)
            return false;
        elseif($tournament->club->region_id == Auth::getUser()->getUserRegion())
            return false;

        return true;
    }

    public static function checkAdminPermission($tournament)
    {
        if (Auth::getUser()->hasRole("superadmin"))
            return true;
        elseif($tournament->club->region_id == Auth::getUser()->getUserRegion())
            return true;

        return false;
    }

} 