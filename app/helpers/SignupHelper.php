<?php

class SignupHelper
{
    /**
     * helper to get player licence number and current ranking
     * if player is not found return empty string
     * @param array $input
     * @return array
     **/

    public static function getPlayerData($input, $draw)
    {
        $current_ranking = 0;
        $ranking_points = 0;
        $licence_number = isset($input['licence_number']) ? $input['licence_number'] : '';
        if ($licence_number != "") {
            $player = Player::whereLicenceNumber($licence_number)->first();
            $current_ranking = $player->ranking;
            $ranking_points = $player->ranking_points;

            // if tournament type is different from RFET check if player belongs to federation/province
            $check_message = false;
            if ($draw->tournament->tournament_type > 1) // check tournament type if != RFET
                $check_message = Import::checkTournamentType($player, $draw->tournament); // return check message if there is no message return false

            if ($check_message) {
                return Response::json([
                    'success' => false,
                    'msg' => $check_message,
                ]);
            }

        } else {
            $date_of_birth = isset($input['date_of_birth']) ? $input['date_of_birth'] : '';
            $name = isset($input['name']) ? $input['name'] : '';
            $surname = isset($input['surname']) ? $input['surname'] : '';

            $dob = DateTimeHelper::GetContactsDate($date_of_birth);

            $player = Player::leftJoin('contacts', 'contacts.id', '=', 'players.contact_id')
                ->where('contacts.name', 'ILIKE', "%" . $name . "%")
                ->where('contacts.surname', 'ILIKE', "%" . $surname . "%")
                ->where('contacts.date_of_birth', '=', $dob)
                ->first();



            if (!is_null($player)) {
                $licence_number = $player->licence_number;
                $current_ranking = $player->ranking;
                $ranking_points = $player->ranking_points;

                // if tournament type is different from RFET check if player belongs to federation/province
                $check_message = false;
                if ($draw->tournament->tournament_type > 1) // check tournament type if != RFET
                    $check_message = Import::checkTournamentType($player, $draw->tournament); // return check message if there is no message return false

                if ($check_message) {
                    return Response::json([
                        'success' => false,
                        'msg' => $check_message,
                    ]);
                }

            }
        }

        return [
            'licence_number'  => $licence_number,
            'current_ranking' => $current_ranking ? $current_ranking : 0,
            'ranking_points' => $ranking_points ? $ranking_points : 0,
        ];

    }

    /**
     * separate doubles signups on left and right side for drag and drop functionality
     * left side are players without pair and right side are players with pair
     * @param $signups Collection
     * @param $without_pair boolean
     * @return array
     */
    public static function separatePairs($signups, $without_pair = false)
    {
        $item = [];
        foreach ($signups as $player) {
            if (!$without_pair) {
                if (count($player->teams) == 1)
                    $item[] = $player->toArray();
            } else {
                if (count($player->teams) > 1)
                    $item[] = $player->toArray();
            }

        }

        return $item;
    }

    public static function playerRankingPoints($player, $draw_type = 'singles')
    {
        if ($draw_type == 'doubles') {
            return ($player['teams'][0]['ranking_points'] > $player['teams'][1]['ranking_points'])
                ? $player['teams'][0]['ranking_points'] : $player['teams'][1]['ranking_points'];
        } else {
            return $player['teams'][0]['ranking_points'];
        }
    }

    public static function wildCardStatus($player)
    {
        return (in_array($player['status'], [1, 2])) ? '[WC]' : false;
    }

    public static function checkPlayersWithoutListType($players)
    {
        if (!count($players))
            return false;

        foreach ($players as $player) {
            if (!$player->list_type) {
                $player->list_type = 3;
                $player->save();
            }
        }

    }
} 