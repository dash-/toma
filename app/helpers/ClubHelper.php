<?php

/**
 * Created by PhpStorm.
 * User: damirseremet
 * Date: 23/07/14
 * Time: 17:02
 */
class ClubHelper
{
    public static function getListOfClubs($include_choose_one=false)
    {
        $listOne = [];
        if ($include_choose_one)
            $listOne = array(NULL => 'Choose club');

        if (Auth::getUser()->hasRole('superadmin') OR Auth::getUser()->hasRole('referee'))
            $list = Club::lists('club_name', 'id');
        else
            $list = Club::whereRegionId(Auth::getUser()->getUserRegion())->lists('club_name', 'id');

        return $listOne+$list;

    }

    public static function countClubs()
    {
        if (Auth::getUser()->hasRole('superadmin') || Auth::getUser()->hasRole('referee'))
            return Club::remember(10, 'clubs_count')->count();

        return Club::whereRegionId(Auth::getUser()->getUserRegion())->remember(10, 'regiona_clubs_count')->count();
    }
} 