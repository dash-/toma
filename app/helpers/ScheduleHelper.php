<?php

class ScheduleHelper
{
    public static function updateTimer($timer, $minimum_break)
    {
        return date('H:i', strtotime($timer . ' + ' . $minimum_break . ' minutes'));
    }

    public static function generateWaitingList($conf)
    {
        $schedules = MatchSchedule::whereTournamentId($conf->tournament_id)
            ->whereDateOfPlay($conf->date)
            ->orderBy('row', 'asc')
            ->get();

        $available_courts = $conf->number_of_courts;
        $court_number = 1;
        $row = 1;

        foreach ($schedules as $schedule) {
            $schedule->waiting_list_court_number = $court_number;
            $schedule->waiting_list_row = $row;
            $schedule->waiting_list_type_of_time = ($row == 1) ? $schedule->type_of_time : 3;
            $schedule->waiting_list_time = ($row == 1) ? $schedule->schedule_time : null;
            $schedule->save();
            if (($court_number == $available_courts)) {
                $court_number = 1;
                $row++;
            } else
                $court_number++;
        }

    }

    /**
     * @param string $what court number or row
     * @param $schedule - pass the schedule with latest position
     * @return integer
     */
    public static function positionCalculator($what = 'court_number', $schedule)
    {
        if ($what == 'court_number') {
            if ($schedule->court_number < $schedule->tournament->number_of_courts)
                return $schedule->court_number + 1;
            else
                return 1;
        } else {
            if ($schedule->court_number < $schedule->tournament->number_of_courts)
                return $schedule->row;
            else
                return $schedule->row + 1;
        }
    }


    /**
     * generate multidimensional array to keep track of matches per player
     * @param $schedules \Illuminate\Database\Eloquent\Collection
     * @param $configurations \Illuminate\Database\Eloquent\Collection
     * @return array $items
     */
    public static function sortByPlayer($schedules, $configurations)
    {
        $items = [];

        foreach ($schedules as $schedule) {
            $config = $configurations->filter(function($item) use ($schedule) {
                return $item->date == $schedule->date_of_play;
            })->first();
            

            $team1_licence = isset($schedule->match->team1_data) ? $schedule->match->team1_data->licence_number : '';
            $team2_licence = isset($schedule->match->team2_data) ? $schedule->match->team2_data->licence_number : '';

            $item['match_id'] = $schedule->match_id;
            $item['team1'] = isset($schedule->match->team1_data) ? DrawMatchHelper::getTeamNames($schedule->match->team1_data) : '';
            $item['team2'] = isset($schedule->match->team2_data) ? DrawMatchHelper::getTeamNames($schedule->match->team2_data) : '';

            $item['teams'] = $item['team1'] . ' vs ' . $item['team2'];
            $item['date'] = DateTimeHelper::GetShortDateFullYear($schedule->date_of_play);

            if ($config->waiting_list) {
                $time = $schedule->type_of_time(true);
                $time .= ($schedule->waiting_list_type_of_time != 3) ? ": " . $schedule->getTime() : null;
            } else {
                $time = $schedule->type_of_time();
                $time .= ($schedule->type_of_time != 3) ? ": " . $schedule->getTime() : null;
            }

            $item['time'] = $time;
            $item['court_number'] = ($config->waiting_list) ? $schedule->waiting_list_court_number : $schedule->court_number;
            $item['order_number'] = ($config->waiting_list) ? $schedule->waiting_list_row : $schedule->row;

            $team1_key = $item['team1'] . ' - ' . $team1_licence;
            $team2_key = $item['team2'] . ' - ' . $team2_licence;

            $items[$team1_key][] = $item;
            $items[$team2_key][] = $item;

        }

        ksort($items);
        
        return $items;

    }
}