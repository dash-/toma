<?php

class DrawTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testRoundUp()
	{
		$players = Player::orderBy('ranking')
			->take(19)
			->get()
			->toArray();

		$num_of_seeds = 4;
		$counter = Draw::roundUpToNextPowerOfTwo(count($players));
		
		$this->assertEquals(32, $counter);
	}

	public function testInitialShuffle()
	{
		$players = Player::orderBy('ranking')
			->take(32)
			->get()
			->toArray();
		$temp = $players;
		$num_of_seeds = 4;
		
		$sliced_players = array(
			'seeds' => array_splice($players, 0, $num_of_seeds),
			'rest'  => array_splice($players, 0),
	    );

		$shuffled = Draw::shufflePlayers($temp, $num_of_seeds);

		$shuffled_splice = array(
			'seeds' => array_splice($shuffled, 0, $num_of_seeds),
			'rest'  => array_splice($shuffled, 0),
	    );

	    $this->assertEquals(json_encode($sliced_players['seeds']), json_encode($shuffled_splice['seeds']));
	    $this->assertNotEquals(json_encode($sliced_players['rest']), json_encode($shuffled_splice['rest']));
	}

	public function testPlayerPasses()
	{
		$players = Player::orderBy(DB::raw("ranking"))
			->take(15)
			->get()
			->toArray();
		$num_of_seeds = 8;

		$pairs = Draw::makePairs($players, $num_of_seeds);

		$checker = Draw::roundUpToNextPowerOfTwo(count($players)) / 2;


		for ($i=0; $i < 5; $i++) { 
			$item = [];
			$result = [];
			foreach ($pairs as $pair) 
			{
				// dd(count($pair));
				foreach ($pair as $key => $p) {
					if ($key == "player1")
						$item['player1'] = $p['id'];
					else
						$item['player2'] = $p['id'];

				}
				if (strpos($item['player1'],'bye') === false OR strpos($item['player2'],'bye') === false) {
					$result[] = $item;
				}
			}

			$this->assertEquals($checker, count($result));
			
		}



	}

}
