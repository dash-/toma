<?php namespace App\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class ImportFacade extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return new \App\Services\Import;
    }
 
}