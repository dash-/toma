<?php namespace App\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class DrawFacade extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return new \App\Services\Draw;
    }
 
}