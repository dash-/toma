<?php namespace App\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class PointsCalculatorFacade extends Facade {
 
    protected static function getFacadeAccessor()
    {
        return new \App\Services\PointsCalculator;
    }
 
}