<?php

namespace Player;

use BaseController, View, Input, Response, Auth, PlayerHelper, LangHelper, PlayerModuleHelper, DateTimeHelper;

class IndexController extends BaseController
{
    public function __construct()
    {
        $this->beforeFilter(function(){
            if(!(Auth::check() && Auth::getUser()->hasRole("player")))
                return \Redirect::to("/");
            }
        );
    }

    public function getIndex()
    {
    }

    public function getMyRanking()
    {
        $statistics = PlayerModuleHelper::getPlayerStatistics(Auth::getUser()->player->id);
        $statistics['show_reset'] = false;
        $this->layout->title = 'My Ranking';
        $this->layout->content = View::make('player/ranking', $statistics);
    }

    public function getMyStatistics()
    {

        $player_history = \PlayerHistoryStatistic::where("player_id", Auth::getUser()->player->id)
            ->orderBy("date")
            ->get()->toArray();
        $height = array_pluck($player_history, "height");
        $weight = array_pluck($player_history, "weight");
        $date_range = array_pluck($player_history, "date");
        $date_range_formatted = [];
        foreach ($date_range as $date) {
            $date_range_formatted[] = DateTimeHelper::getOnlyDateFromString($date);
        }
        $this->layout->title = 'My Statistics';
        $this->layout->content = View::make('player/statistics', array(
            'player_history' => $player_history,
            "height" => $height,
            "weight" => $weight,
            "date_range" => $date_range_formatted
        ));

    }

    public function postMyRanking()
    {
        $date_from = Input::get('from_date');
        $date_to = Input::get('to_date');
        $statistics = PlayerModuleHelper::getPlayerStatistics(Auth::getUser()->player->id, $date_from, $date_to);
        $statistics['show_reset'] = true;

        $this->layout->title = 'My Ranking';
        $this->layout->content = View::make('player/ranking', $statistics);
    }

    public function getEditPlayerStatisticsData()
    {
        $player_history = \PlayerHistoryStatistic::where("player_id", Auth::getUser()->player->id)
            ->orderBy("date")
            ->get();

        $data = [];
        foreach ($player_history as $history) {
            $item['id']     = $history->id;
            $item['height'] = $history->height;
            $item['weight'] = $history->weight;
            $item['handed'] = $history->handed;
            $item['date']   = \DateTimeHelper::GetShortDate($history->date)   ;//->format(\Config::get("localSettings.onlyDate"));

            $data[] = $item;
        }

        return Response::json([
            'player_statistics' => $data,
        ]);
    }
    public function postEditPlayerStatistics(){
        $rules = array(
            'height' => array('required', 'integer'),
            'weight' => array('required', 'integer'),
            'date' => array('required', 'date'),
        );
        $validator = \Validator::make(Input::all(), $rules);


        if ($validator->passes()) {
            $stat = \PlayerHistoryStatistic::create([
                'height' => Input::get('height'),
                'weight' => Input::get('weight'),
                'handed' => Input::get('handed'),
                'date' => \DateTimeHelper::GetPostgresDateTime(Input::get('date')),
                'player_id' => Auth::getUser()->player->id,
            ]);

            $responseData = [
                'id'     => $stat->id,
                'height' => Input::get('height'),
                'weight' => Input::get('weight'),
                'handed' => Input::get('handed'),
                'date'   => \DateTimeHelper::GetShortDate(Input::get('date')),
            ];

            return Response::json(array('success' => 'true', 'data' => $responseData));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function deletePlayerStat($stat_id)
    {
        \PlayerHistoryStatistic::whereId($stat_id)->delete();

        return Response::json([
            'success' => true,
        ]);
    }

    public function getEditPlayerStatistics()
    {
        $this->layout->title = 'Edit Statistics';
        $this->layout->content = View::make('player/edit_player_statistics');
    }

    public function getUpcoming()
    {
        $weeks = [
            'Next week' => PlayerHelper::getTournamentsPerWeekForDisplay('next week', 'get'),
            'Next 2 weeks' => PlayerHelper::getTournamentsPerWeekForDisplay('+1 week', 'get'),
            'Next 3 weeks' => PlayerHelper::getTournamentsPerWeekForDisplay('+2 weeks', 'get'),
            'Future' => PlayerHelper::getTournamentsPerWeekForDisplay('+3 weeks', 'get', true),
        ];


        $this->layout->title = LangHelper::get('upcoming_tournaments', 'Upcoming tournaments');
        $this->layout->content = View::make('tournaments/upcoming', array(
            'weeks' => $weeks,
        ));
    }
}