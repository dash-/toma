<?php
class __MessagesController extends BaseController {

	
	public function getIndex()
	{
		$messages = Message::where('receiver_id', '=', Auth::getUser()->id)
			->where('deleted_at', '=', NULL)
			->paginate(10);
		
		$view_file = View::make('messages/inbox', array(
			'messages'  => $messages,
		));

		if (Request::query('type') == 'outbox')
		{
			$messages = Message::where('sender_id', '=', Auth::getUser()->id)->paginate(10);
			$view_file = View::make('messages/outbox', array(
				'messages'  => $messages,
			));
		}
		
		$this->layout->title = "Messages";
		$this->layout->content = View::make('messages/index', array(
			'messages'  => $messages,
			'view_file' => $view_file,
		));
	}

	public function getShow($id)
	{
		$messages = Message::where('conversation_id', '=', $id)
			->whereDeletedAt(NULL)
			->get();
		
		//set seen boolean and time
		Message::where('conversation_id', '=', $id)->setSeen();

		$this->layout->title = "Messages conversation";
		$this->layout->content = View::make('messages/show', array(
			'messages' => $messages,
		));
	}


	public function getCreate()
	{
		$users_array = User::where('id', '<>', Auth::user()->id)->lists('username', 'id');

		$this->layout->title = "New message";
		$this->layout->content = View::make('messages/create', array(
			'users_array' => $users_array,
		));
	}

	public function postCreate()
	{
		$rules = array(
			'messages.receiver_id' => array('required'),
			'messages.text'        => array('required'),
        );

        $last_conversation_id = Message::max('conversation_id');

        $validator = Validator::make(array_dot(Input::all()), $rules);

        $message_data = Input::get('messages');

        if ($validator->passes())
        {
            $message = Message::create($message_data + array(
				'sender_id'       => Auth::getUser()->id,
				'time_sent'       => DateTimeHelper::getDateNow(),
				'conversation_id' => $last_conversation_id + 1,
            ));
            
            Notification::MessageNotif($message_data['receiver_id'], $message->conversation_id);

            return Response::json(array('success' => 'true'));
        }
        else
        {
            return $validator->messages()->toJson();
        }
	}

	public function getReply($id)
	{
		$message = Message::whereConversationId($id)
			->where('sender_id', '!=', Auth::getUser()->id)
			->orderBy('id', 'DESC')
			->first();

		$this->layout->title = "Reply message ".$message->getUser('sender_id')->username;
		$this->layout->content = View::make('messages/reply', array(
			'message' => $message,
		));
	}

	public function postReply($id)
	{
		$message = Message::whereConversationId($id)
			->where('sender_id', '!=', Auth::getUser()->id)
			->orderBy('id', 'DESC')
			->first();

		$rules = array(
			'messages.receiver_id' => array('required'),
			'messages.text'        => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);

        $message_data = Input::get('messages');

        if ($validator->passes())
        {
            $msg = Message::create($message_data + array(
				'sender_id'       => Auth::getUser()->id,
				'time_sent'       => DateTimeHelper::getDateNow(),
				'conversation_id' => $message->conversation_id,
            ));

            Notification::MessageNotif($message_data['receiver_id'], $msg->conversation_id);
            
            return Response::json(array('success' => 'true'));
        }
        else
        {
            return $validator->messages()->toJson();
        }
	}

	public function deleteM($id) {
		$message = Message::find($id);

		$message->deleted_at = DateTimeHelper::getDateNow();

		$message->save();

		return \Response::json(array('success' => true));
	}

	public function getUnread($id)
	{
		$message = Message::find($id);

		$message->seen = FALSE;
		$message->save();

		return  Redirect::to('/messages');
	}

}
