<?php

class NotificationsController extends BaseController
{

    public function getIndex()
    {
        foreach (Auth::getUser()->notifications()->wherePivot('seen', '=', false)->get() as $notif) {
            $notif->pivot->seen = true;
            $notif->pivot->save();
        }

        $this->layout->title = LangHelper::get('notifications', 'Notifications');
        $this->layout->content = View::make('notifications/index', array(// 'notifications' => $notifications,
        ));
    }

    public function getData()
    {
        $show_deleted = false;
        if (Request::query("show_deleted"))
            $show_deleted = Request::query("show_deleted");

        $notifications = Notification::with('type')
            ->whereHas('users', function ($query) use ($show_deleted) {
                $query->where('user_id', '=', Auth::getUser()->id);
                $query->where('deleted', '=', $show_deleted);
            })
            ->orderBy('id', 'desc')
            ->paginate(10);

        return Response::json($notifications->toArray())->setCallback(\Request::get('callback'));
    }


    public function getSticky()
    {
        $notifications = Auth::user()->notifications()
            ->wherePivot('sticky', '=', true)
            ->get();

        $data = array();
        $count = count($notifications);

        if ($count) {
            foreach ($notifications as $notif) {
                $data['caption'] = $notif->type->name;
                $data['message'] = $notif->text;
                $items[] = $data;

                $notif->pivot->sticky = false;
                $notif->pivot->save();
            }

            return $items;
        }

        return $count;
    }

    public function deleteNotif($id)
    {
        $notif = Auth::getUser()->notifications()
            ->wherePivot('notification_id', '=', $id)
            ->first();

        $notif->pivot->deleted = true;
        $notif->pivot->save();

        return Response::json(array('success' => 'true'));
    }

    public function deleteAll()
    {
        $notifications = Auth::getUser()->notifications()
            ->get();

        foreach ($notifications as $notif) {
            $notif->pivot->deleted = true;
            $notif->pivot->save();
        }

        return Response::json(array('success' => 'true'));
    }
}
