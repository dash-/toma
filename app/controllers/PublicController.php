<?php
class PublicController extends Controller {

    public $layout = 'layouts.public';
    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        $this->tournament_slug = Request::segment(1);
        $exploded_slug = explode('-', $this->tournament_slug);

        $this->message = TournamentMessage::where('active', true)
            ->whereTournamentId(end($exploded_slug))
            ->orderBy('id', 'desc')
            ->first();

        $this->logged_user = \Auth::user();
        View::share('logged_user', $this->logged_user);
        View::share('tournament_slug', $this->tournament_slug);
        View::share('message', $this->message);

        if ( ! is_null($this->layout))
        {
            $layout = Request::ajax() ? 'layouts/ajax' : $this->layout;
            $this->layout = View::make($layout);

            $this->layout->tournament_slug = $this->tournament_slug;
            $this->layout->controller = RouteClass::getParams();
            $this->layout->styles = array(
                'css/animate.css'                              => 'screen, projection',
                'css/loading-bar.css'                          => 'screen, projection',
                'js/thirdparty/eventCalendar/fullcalendar.css' => 'screen, projection',
                'js/bootstrap/css/metro-bootstrap.css'         => 'screen, projection',
                'js/thirdparty/select2/select2.css'            => 'screen, projection',
                'css/public.css'                               => 'screen, projection',
                'css/helpers.css'                              => 'screen, projection',
                'css/dropzone.css'                             => 'screen, projection',
                'css/icons.css'                                => 'screen, projection',
                'js/thirdparty/fancybox/jquery.fancybox.css'   => 'screen, projection',
                'css/public-print.css'                         => 'print',

            );

            $this->layout->external_scripts = array(

                'http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
                'js/thirdparty/tinymce/tinymce.min.js',
                'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.16/angular-animate.js',
                '//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.16/angular-resource.min.js',
                '//ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular-route.js',
                '//cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js',
                'https://maps.googleapis.com/maps/api/js?key=AIzaSyCMHx_ag-fR-MlV6EPsiea8GywL3KuZqcY',

            );

            $this->layout->scripts = array(
                'js/ang/bootstrap/ui-bootstrap-tpls-0.11.0.js',
                'js/bootstrap/js/bootstrap.js',
                'js/thirdparty/history.js',
                'min/jquery-widget.min.js',
                'js/ang/loading-bar.js',
                'js/thirdparty/eventCalendar/moment.js',
                'js/thirdparty/eventCalendar/calendar.js',
                'js/thirdparty/eventCalendar/fullcalendar.min.js',
                'js/ang/calendarApp.js',
                'js/thirdparty/dropzone.js',
                'js/jquery.tinyscrollbar.js',
                'js/thirdparty/fancybox/jquery.fancybox.pack.js',
                'js/brain-socket.min.js',
                'js/ang/public/tiny.js',
                'js/thirdparty/select2/select2.js',
                'js/ang/select2.js',
                'js/ang/public/dataTablesPublic.js',

                'js/ang/public/draws.js',
                'js/ang/public/signups.js',
                'js/ang/public/scores.js',
                'js/ang/public/news.js',
                'js/ang/public/gallery.js',
                'js/ang/public/tournament.js',
                'js/ang/public/auth.js',
                'js/ang/public/messagebox.js',
                'js/ang/search.js',
                'js/public.js',
            );
        }
    }

}
