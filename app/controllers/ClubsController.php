<?php

class ClubsController extends BaseController
{

    public function getIndex()
    {
        $items_per_page = array(
            10 => 10,
            20 => 20,
            30 => 30,
            50 => 50,
            100 => 100,
        );

        $this->layout->title = LangHelper::get('clubs', 'Clubs');
        $this->layout->content = View::make('clubs/index', array(
            'items_per_page' => $items_per_page
        ));
    }

    public function getData()
    {
        $items = Request::query('items_counter');
        $order = Request::query('order') ? Request::query('order') : "id";
        $sort = Request::query('sort');

        if ($order == 'name')
            $order = 'club_' . $order;

        // changed order to preserve club.id 
        $query = Region::leftJoin("clubs", 'clubs.region_id', '=', 'regions.id');

        if (!Auth::getUser()->hasRole('superadmin') && !Auth::getUser()->hasRole('referee'))
            $query->where("clubs.region_id", Auth::getUser()->getUserRegion());

        if (Request::query('search') == 1) {
            $posts = Request::query();

            foreach ($posts as $key => $post) {
                $key = str_replace("INT_", "", $key, $int);

                if ($key == "provinces") {
                    $post = get_object_vars(json_decode($post));
                    $keyVa = array_keys($post);

                    $query->where("provinces." . $keyVa[0], 'ILIKE', "%" . $post[$keyVa[0]] . "%");
                } else if ($post AND !in_array($key, array('items_counter', 'order', 'sort', 'search', 'callback', 'page'))) {
                    if ($int)
                        $query->where($key, '=', $post);
                    else
                        $query->where($key, 'ILIKE', "%" . $post . "%");
                }
            }


            if ($order == 'region_name')
                $clubs = $query->orderBy('regions.region_name', $sort)->paginate($items);
            else
                $clubs = $query->orderBy('clubs.' . $order, $sort)->paginate($items);


            return Response::json($clubs->toArray())->setCallback(Request::get('callback'));
        }


        if ($order == 'region_name')
            $clubs = $query->orderBy('regions.region_name', $sort)->paginate($items);
        else
            $clubs = $query->orderBy('clubs.' . $order, $sort)->paginate($items);
        return Response::json($clubs->toArray())->setCallback(\Request::get('callback'));
    }

    public function getEdit($id)
    {

        if (Auth::getUser()->hasRole("referee"))
            return PermissionHelper::noPermissionPage($this);

        $club = Club::find($id);

        $province = array($club->province_id => $club->province->province_name) + Province::all()->lists('province_name', 'id');
        $region = array($club->region_id => $club->province->region->region_name) + Region::all()->lists('region_name', 'id');

        $this->layout->title = LangHelper::get('edit_club', 'Edit club') . " - " . $club->club_name;
        $this->layout->content = View::make('clubs/edit', array(
            'club' => $club,
            'province' => $province,
            'region' => $region,
        ));
    }

    public function putEdit($id)
    {
        $club = Club::find($id);

        $rules = array(
            'club.club_name' => array('required'),
            'club.club_city' => array('required'),
            'club.province_id' => array('required'),
            'club.region_id' => array('required'),
            'club.club_address' => array('required'),
            'club.short_name' => array('required'),
            'club.founded_date' => array('required'),
            'club.number_of_tracks' => array('required'),
            'club.club_email' => array('required'),
            'club.club_phone' => array('required'),
            'club.nif' => array('required'),
        );

        $validator = Validator::make(Input::all(), $rules);

        $club_data = Input::get('club');

        if ($validator->passes()) {

            $club = $club->update($club_data);

            return Response::json(array('success' => 'true'));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getInfo($id)
    {
        $club = Club::find($id);

        $this->layout->title = LangHelper::get('info_club', 'Info Club') . " - " . $club->club_name;
        $this->layout->content = View::make('clubs/info', array(
            'club' => $club,
        ));
    }

    public function getUpload($club_id)
    {
        $club = Club::find($club_id);
        $this->layout->title = 'Upload image';
        $this->layout->content = View::make('clubs/upload', array(
            'club' => $club,
        ));
    }


    public function postUpload($club_id)
    {
        $club = Club::find($club_id);

        $rules = array(
            'image' => 'required',
        );

        $image = Input::file('image');

        if ($image == '') {
            $image = Input::file("file");
            $rules = array('file' => 'required');
        }

        if ($image)
            $filename = Str::random() . '_' . $image->getClientOriginalName();

        $save_path = public_path() . '/uploads/images/clubs';

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            if ($image) {
                Image::make($image)->save($save_path . '/full/' . $filename);
                Image::make($image)->resize(210, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(280, 210)->save($save_path . '/thumbnails/' . $filename);
                Image::make($image)->resize(75, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(100, 75)->save($save_path . '/small_thumbnails/' . $filename);
            }

            $club->image_link = $filename;

            $club->save();

            return Redirect::to('clubs/upload/' . $club->id);
        } else {
            return Redirect::to('clubs/upload/' . $club->id);
        }
    }

    public function getPosition($club_id)
    {
        $club = Club::find($club_id);
//        echo Debug::vars($club);die;
        $this->layout->title = LangHelper::get('info_club', 'Info Club') . " - " . $club->club_name;
        $this->layout->content = View::make('clubs/position', array(
            'club' => $club,
        ));
    }

    public function postPosition($club_id)
    {

        $club = Club::find($club_id);
        $latitude = Input::get("latitude");
        $longitude = Input::get("longitude");

        if ($latitude && $longitude) {
            $club->position_lat = $latitude;
            $club->position_long = $longitude;
            $club->save();
        }

        return Redirect::to('clubs/position/' . $club->id);
    }

    public function getRegionLicences($club_id)
    {
        $club = Club::find($club_id);
        if ($club) {
            $player_licence = DB::table('players')->select("licence_number")->where("federation_club_id", $club->region_id)->max('licence_number');
            $licence_range = LicenceRange::where("region_id", $club->region_id)->first()->rangeToString();
            $licence_range .= " | Last licence used: " . $player_licence;

            return Response::json(["message" => $licence_range]);
        }
        return Response::json(["message" => '']);


    }
}