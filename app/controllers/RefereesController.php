<?php

class RefereesController extends BaseController {

	public function getIndex()
	{
		$this->layout->title = LangHelper::get('referees', "Referees");
		$this->layout->content = View::make('referees/index');
	}

	public function getData()
	{
		$order = Request::query('order') ? Request::query('order') : "contacts.id";
		$sort = Request::query('sort');
		if (Request::query('search') == 1) {
			$referees = Referee::filteredData(Request::query());
		} else {
			$referees = Referee::getData($order, $sort);
		}

		return Response::json(
			$referees->toArray() + [
				'user_role' => Auth::getUser()->getRole('id'),
				'user_region_id' => Auth::getUser()->getUserRegion()
			])->setCallback(\Request::get('callback'));
	}


	public function getDetails($referee_id)
	{
		$ref = Referee::with('contact', 'player', 'user', 'external_tournaments', 'external_tournaments.user')
			->find($referee_id);

        $tournaments = Tournament::with('organizer', 'surface')
            ->where('referee_id_real', $ref->id)
            ->get();

        $this->layout->title = LangHelper::get('referee details', "Referee details");
        $this->layout->content = View::make('referees/details', array(
            'ref'         => $ref,
            'tournaments' => $tournaments,
        ));
	}

    public function postSuperState($user_id)
    {
        $user = User::with('roles')->find($user_id);
        $input = Input::get('is_super');

        if ($input == 1) {
            $user->addRefereeAdminRole();

            return Response::json([
                'success' => true,
                'message' => LangHelper::get('referee_admin_role_added', 'Referee admin role added'),
            ]);
        } else {
            $user->addRole(6);
            return Response::json([
                'success' => true,
                'message' => LangHelper::get('referee_admin_role_removed', 'Referee admin role removed'),
            ]);
        }

    }

	public function getCreate()
	{
		$genders = [NULL => LangHelper::get('choose_gender', 'Choose gender')]
        	+ ['M' => 'Male', 'F' => 'Female',];

		$this->layout->title = LangHelper::get('referee add', "Referee add");
		$this->layout->content = View::make('referees/create', array(
			'genders' => $genders,
		));
	}

	public function postCreate()
	{
		$rules = array(
			'name'    => array('required'),
			'surname' => array('required'),
			'email'   => array('required', 'email', 'unique:users'),
			'sex'     => array('required'),
		);

		$validator = \Validator::make(Input::get('contact'), $rules);

		$contact_data = Input::get('contact');

		if ($validator->passes())
		{
            $user = new User;
			//explode username from email
			$username = explode("@", $contact_data['email']);
			// get first part if: johndoe@dash.ba get johndoe
			$user->username = $username[0];
			$user->email = $contact_data['email'];

			// send password with email REMINDER
            $password = Str::random(12);
			$user->password = Hash::make($password);
			$user->save();

			$userrole = new UserRole;
			$userrole->role_id = 6; // add referee role
			$userrole->user_id = $user->id;
			$userrole->save();

			$contact = Contact::create($contact_data);
			$contact->date_of_birth = DateTimeHelper::GetContactsDate(Input::get('contact.date_of_birth'));
			$contact->save();

			$admin_table = new Admin;
			$admin_table->contact_id = $contact->id;
			$admin_table->user_id = $user->id;
			$admin_table->save();

            $emaildata = new EmailClass();
            $emaildata->to = $contact_data['email'];
            $emaildata->name = $contact_data['name'].' '.$contact_data['surname'];

            $userData = array(
                'username' => $contact_data['email'],
                'password' => $password
            );
            EmailHelper::queueEmail('emails.referee.create', $emaildata, $userData);

			return Response::json(array('success' => 'true', 'redirect_to' => URL::to('referees')));
		}
		else
		{
			return $validator->messages()->toJson();
		}
	}

	public function getEdit($referee_id)
	{
		$ref = Referee::with('contact', 'player')->find($referee_id);
		$status_array = $ref->statusDropdown($ref->active);
        $genders = [$ref->contactInfo('sex') => $ref->contact->gender($ref->contactInfo('sex'))]
        	+ ['M' => 'Male', 'F' => 'Female',];

		$this->layout->title = LangHelper::get('referee edit', "Referee edit");
		$this->layout->content = View::make('referees/edit', array(
			'ref'     => $ref,
			'genders' => $genders,
			'status_array' => $status_array,
		));
	}

	public function putEdit($referee_id)
	{
		$ref = Referee::find($referee_id);
		$contact = $ref->contact;
		$rules = array(
            'contact.name' => array('required'),
            'contact.surname' => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);
		$contact_data = Input::get('contact');
		$referee_data = Input::get('referee');

        if ($validator->passes()) {
            if ($contact) {
				$contact->update($contact_data);
				$contact->date_of_birth = DateTimeHelper::GetContactsDate(Input::get('contact.date_of_birth'));
				$contact->save();
            }
			else {
				$contact = Contact::create($contact_data);
				$contact->date_of_birth = DateTimeHelper::GetContactsDate(Input::get('contact.date_of_birth'));
				$contact->save();
			}

			$ref->update($referee_data);

            return Response::json(array('success' => 'true'));
        } else {
            return $validator->messages()->toJson();
        }
	}

	public function getCreateAccount($referee_id)
	{
		$referee = Referee::with('contact')->find($referee_id);

		if ($referee && $referee->user_id)
			return Redirect::to('/referees/details/'.$referee->id);

		if (is_null($referee))
			return Redirect::to('/referees');

		$this->layout->title = LangHelper::get('create_referee_account', 'Create referee account');
		$this->layout->content = View::make('referees/create_account', array(
			'referee' => $referee,
		));
	}

    public function getSearchReferees()
    {
        $search_term = Request::query('q');

        $referees = Referee::dataForDropdown($search_term);
        return Response::json([
            'items' => $referees,
        ]);
    }

    public function getAddExternal($referee_id)
    {
        $referee = Referee::with('contact')->find($referee_id);


        $this->layout->title = LangHelper::get('add_external_tournament', 'Add external tournament');
        $this->layout->content = View::make('referees/add_external_tournament', array(
            'referee' => $referee,
        ));
    }

    public function postAddExternal($referee_id)
    {
        $rules = array(
            'referee.tournament_title'   => array('required'),
            'referee.city'               => array('required'),
            'referee.country'            => array('required'),
            'referee.refereed_date_from' => array('required'),
            'referee.refereed_date_to'   => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);
        $data = Input::get('referee');

        if ($validator->passes()) {
            $external = new RefereeExternalTournament;
            $external->tournament_title = $data['tournament_title'];
            $external->city = $data['city'];
            $external->country = $data['country'];
            $external->refereed_date_from = $data['refereed_date_from'];
            $external->refereed_date_to = $data['refereed_date_to'];
            $external->referee_id = $referee_id;
            $external->created_by = Auth::user()->id;
            $external->save();

            return Response::json([
                'success' => true,
                'message' => LangHelper::get('external_tournament_added', 'External tournament added'),
                'redirect_to' => '/referees/details/'.$referee_id,
            ]);
        }
    }

    public function getEditExternal($external_id)
    {
        $tournament = RefereeExternalTournament::with('referee', 'referee.contact')->find($external_id);


        $this->layout->title = LangHelper::get('edit_external_tournament', 'Edit external tournament');
        $this->layout->content = View::make('referees/edit_external_tournament', array(
            'tournament' => $tournament,
        ));
    }

    public function postEditExternal($external_id)
    {
        $rules = array(
            'referee.tournament_title'   => array('required'),
            'referee.city'               => array('required'),
            'referee.country'            => array('required'),
            'referee.refereed_date_from' => array('required'),
            'referee.refereed_date_to'   => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);
        $data = Input::get('referee');

        if ($validator->passes()) {
            $external = RefereeExternalTournament::with('referee')->find($external_id);
            $external->tournament_title = $data['tournament_title'];
            $external->city = $data['city'];
            $external->country = $data['country'];
            $external->refereed_date_from = $data['refereed_date_from'];
            $external->refereed_date_to = $data['refereed_date_to'];
            $external->save();

            return Response::json([
                'success' => true,
                'message' => LangHelper::get('external_tournament_updated', 'External tournament updated'),
                'redirect_to' => '/referees/details/'.$external->referee_id,
            ]);
        }
    }

    public function deleteExternal($id)
    {
        $external = RefereeExternalTournament::find($id);

        if ($external->delete()) {
            return Response::json([
                'success' => true,
            ]);
        }

        return Response::json([
            'success' => false,
        ]);
    }
}
