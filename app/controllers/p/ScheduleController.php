<?php

namespace P;

use PublicController, View, Input, Request, Response, Debug;


class ScheduleController extends PublicController
{

    public function getIndex($tournament_slug)
    {
        $tournament = \Tournament::with("draws")
            ->whereSlug($tournament_slug)
            ->first();

        $dates_array = \TournamentHelper::getDrawDates($tournament->date, 0);
        $current_date = (Request::query('date')) ?: array_keys($dates_array)[0];
        $configuration = \ScheduleConfiguration::loadConfiguration($tournament, $current_date);

        $all_schedules = \MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $schedules = $all_schedules->filter(function ($schedule) use ($current_date) {
            return ($schedule->date_of_play == $current_date) ? true : false;
        });

        $draws_ids = array_pluck($tournament->draws, "id");
        $all_matches = \DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category')
            ->whereIn("draw_id", $draws_ids)
            ->get();

        if ($configuration->waiting_list)
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'waiting_list_row')) : 3;
        else
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'row')) : 3;

        $this->layout->title = \LangHelper::get('order_of_play', 'Order of play');
        $this->layout->content = View::make('public/schedule/index', [
            'tournament'     => $tournament,
            'dates_array'    => $dates_array,
            'schedules'      => $schedules,
            'current_date'   => $current_date,
            'all_schedules'  => $all_schedules,
            'configuration'  => $configuration,
            'number_of_rows' => $number_of_rows,
            'all_matches'    => $all_matches,
        ]);
    }

    public function getPrintList($tournament_slug)
    {
        $tournament = \Tournament::with("draws")
            ->whereSlug($tournament_slug)
            ->first();

        $configurations = \ScheduleConfiguration::loadAllConfigurations($tournament);

        $schedules = \MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $matches = \ScheduleHelper::sortByPlayer($schedules, $configurations);

        $this->layout->title = \LangHelper::get('view_per_player', 'View per player');
        $this->layout->content = View::make('public/schedule/player_list', [
            'tournament'   => $tournament,
            'matches'      => $matches,
        ]);
    }

}

