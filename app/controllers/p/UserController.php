<?php

namespace P;
use PublicController, View, Input, LangHelper;

class UserController extends PublicController {

    public function __construct()
    {
        $this->beforeFilter('csrf', ['on' => 'post']);
    }
    
    public function getIndex($tournament_slug)
    {
        if(!\Auth::check())
            return \Redirect::to(\URL::to($tournament_slug . '/users/login'));

        $this->layout->title = LangHelper::get('profile', "Profile");
        $this->layout->content = View::make('public/user/index', array(

        ));
    }

    public function getLogin()
    {
        if (!\Auth::guest())
            return \Redirect::to(\URL::route('default_public', $this->tournament_slug));

        $this->layout->title   = LangHelper::get('login', "Login");
        $this->layout->content = View::make('public/user/login');
    }

    public function postLogin()
    {
        $data = array(
            'email'    => Input::get('email'),
            'password' => Input::get('password'),
        );

        $data2 = array(
            'username' => Input::get('email'),
            'password' => Input::get('password'),
        );

        $remember_post = Input::get('remember_me');
        $remember_me = ($remember_post != NULL AND $remember_post != "false") ? TRUE : FALSE;

        if (\Auth::attempt($data, $remember_me) OR \Auth::attempt($data2, $remember_me)) {
            $language_code = (\Auth::getUser()->language) ? \Auth::getUser()->language->code : NULL;
            \Session::put('my.locale', $language_code);


            return \Response::json([
                'success'      => true,
                'redirect_url' => \URL::route('default_public', $this->tournament_slug),
            ]);
        }
        else
            return \Response::json([
                'success' => false,
                'msg'     => LangHelper::get('wrong_email_or_password', 'You entered wrong email or password.'),
            ]);
    }

    public function postSignup()
    {
        $rules =  array(
            'email'    => array('required', 'email', 'unique:users'),
            'password' => array('required'),
            'name'     => array('required'),
            'surname'  => array('required'),
        );

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return $validator->messages()->toJson();
        }
        else
        {
            $user = new \User;
            $user->email = Input::get('email');
            //explode username from email
            $username = explode("@", Input::get('email'));
            // get first part if: johndoe@dash.ba get johndoe
            $user->username = $username[0];
            $user->password = \Hash::make(Input::get('password'));
            $user->save();

            $role = new \UserRole;
            $role->role_id = 4; // add standard user role
            $role->user_id = $user->id;
            $role->save();

            $contact = new \Contact;
            $contact->name = Input::get('name');
            $contact->surname = Input::get('surname');
            $contact->save();

            $admin_table = new \Admin;
            $admin_table->contact_id = $contact->id;
            $admin_table->user_id = $user->id;
            $admin_table->save();



            // login user automatically
            $data = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            );
            \Auth::attempt($data, FALSE);

            return \Response::json([
                'success'      => true,
                'redirect_url' => \URL::route('default_public', $this->tournament_slug),
            ]);
        }

    }

    public function getLogout()
    {
        \Auth::logout();
        return \Redirect::to(\URL::route('default_public', $this->tournament_slug));
    }
}
