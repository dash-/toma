<?php

namespace P;
use PublicController, View, Input;

class GalleryAdminController extends PublicController {

	public function __construct()
    {
        $this->beforeFilter('auth.all_admins');
    }

	public function getIndex($tournament_slug)
	{
		$tournament = \Tournament::with('galleries')
			->whereSlug($tournament_slug)
			->first();

		$this->layout->title = \LangHelper::get('gallery_administration', "Gallery administration");
		$this->layout->content = View::make('public/gallery/admin/index', array(
			'tournament' => $tournament,
		));
	}
    
    public function postIndex($tournament_slug)
    {
        $tournament = \Tournament::whereSlug($tournament_slug)
            ->first();

        $rules = array(
            'image' => 'required',
        );

        $image = Input::file('image');

        if($image==''){
            $image = Input::file("file");
        }

        $fname = (is_object($image)) ? ($image ? $image->getClientOriginalName() : "") : (isset($image[0]) ? $image[0]->getClientOriginalName() : "");

        if ($image)
            $filename = \Str::random().'_'.$fname;

        $save_path = public_path().'/uploads/images/gallery';


        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->passes())
        {
            if ($image) {

                $image = (is_object($image)) ? $image : $image[0];

                \Image::make($image)->save($save_path.'/full/'.$filename);

                \Image::make($image)->resize(540, 315, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(540, 315)->save($save_path.'/thumbnails/'.$filename);
                
                \Image::make($image)->resize(100, 75, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(100, 75)->save($save_path.'/small_thumbnails/'.$filename);
            }

            \TournamentGallery::create([
                'image_path'    => $filename,
                'tournament_id' => $tournament->id,
            ]);

            return \Redirect::to(\URL::to($tournament_slug.'/gallery-admin'));
        }
        else
        {
            return \Redirect::to(\URL::to($tournament_slug.'/gallery-admin'));
        }
    }

    public function deleteImage($tournament_slug, $image_id)
    {
        $image = \TournamentGallery::find($image_id);

        \File::delete(public_path().'/uploads/images/gallery/full/'.$image->image_path);
        \File::delete(public_path().'/uploads/images/gallery/thumbnails/'.$image->image_path);
        \File::delete(public_path().'/uploads/images/gallery/small_thumbnails/'.$image->image_path);
        $image->delete();

        return \Response::json([
            'success' => true,
            'asset_id' => $image_id,
        ]);
    }
	

}
