<?php

namespace P;
use PublicController, View, Input, Request, Response;


class CalendarController extends PublicController {

        public function getIndex($tournament_slug)
        {
            $tournaments =  \Tournament::with('club', 'draws', 'draws.category')
                ->whereStatus(2)
                ->get();

            $clubs_array = \CalendarHelper::getClubs($tournaments);
            $cities_array = \CalendarHelper::getClubs($tournaments, 'club_city');
            $categories = \CalendarHelper::getCategories($tournaments);
            $view_string = Request::query('list_view') ? 'public/calendar/list' : 'public/calendar/index';

            $this->layout->title = \LangHelper::get('calendar', "Calendar");
            $this->layout->content = View::make($view_string, array(
                'clubs_array'  => $clubs_array,
                'cities_array' => $cities_array,
                'categories'   => $categories,
            ));
        }

        public function getData()
        {
            $tournaments = \Tournament::getCalendarData();
            $list_view = Request::query('list_view') ? true : false;
            return \CalendarHelper::calendarData($tournaments, $list_view);
        }

        public function getSearch()
        {
            $list_view = Request::query('list_view') ? true : false;
            return \CalendarHelper::filterData(Request::query(), $list_view);
        }

}
