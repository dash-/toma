<?php

namespace P;
use PublicController, View, Input;

class GalleryController extends PublicController {


	public function getIndex($tournament_slug)
	{
		$tournament = \Tournament::with('galleries')
			->whereSlug($tournament_slug)
			->first();

		$this->layout->title = \LangHelper::get('gallery', "Gallery");
		$this->layout->content = View::make('public/gallery/index', array(
			'tournament' => $tournament,
		));
	}
}
