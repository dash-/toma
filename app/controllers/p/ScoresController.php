<?php

namespace P;
use PublicController, View, Input, Request, Response;


class ScoresController extends PublicController {

    public function getIndex($tournament_slug)
	{
		$tournament = \Tournament::with('draws', 'draws.signups', 'draws.signups.teams')
			->whereSlug($tournament_slug)
			->first();

		$scores = \TournamentScoresHelper::getScores($tournament->slug);

		// echo \Debug::vars($scores->toArray());die;

		$this->layout->title = \LangHelper::get('scores', "Scores");
	    $this->layout->content = View::make('public/scores/index', array(
			'tournament' => $tournament,
			'scores'     => $scores,
	    ));
	}

}

