<?php

namespace P;
use PublicController, View, Input;

class MessageBoxController extends PublicController {

	public function __construct()
    {
        $this->beforeFilter('auth.all_admins');
    }

	public function getIndex($tournament_slug)
	{
        $tournament = \Tournament::with('messages')
            ->where('slug', $tournament_slug)
            ->first();

		$this->layout->title = \LangHelper::get('message_box_administration', "Message box administration");
		$this->layout->content = View::make('public/message_box/index', array(
				'tournament' => $tournament,
			));
	}

	public function getCreate($tournament_slug)
	{
		$this->layout->title = \LangHelper::get('add_new_message', "Add new message");
        $this->layout->content = View::make('public/message_box/create');
	}
	public function postCreate($tournament_slug)
	{
		$tournament_id = \Tournament::whereSlug($tournament_slug)
			->pluck('id');

        $rules = array(
            'message.message' => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);
        $message_data = Input::get('message');

        if ($validator->passes()) {
			$message = \TournamentMessage::create($message_data + [
				'created_by'    => \Auth::user()->id,
				'tournament_id' => $tournament_id
            ]);

			\TournamentMessage::updateStatus($tournament_id, $message->id);

            return \Response::json([
				'success'       => 'true',
				'redirect_to'   => \URL::to($tournament_slug . '/message-box'),
				'tournament_id' => $tournament_id,
				'action'        => 'create',
            ]);
        } else {
            return $validator->messages()->toJson();
        }
	}

	public function getEdit($tournament_slug, $message_id)
	{
		$message = \TournamentMessage::find($message_id);
		$this->layout->title = \LangHelper::get('edit_message', "Edit message");
        $this->layout->content = View::make('public/message_box/edit', array(
       		'message' => $message,
        ));
	}

	public function postEdit($tournament_slug, $message_id)
	{
		$message = \TournamentMessage::find($message_id);
        $rules = array(
            'message.message' => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);
        $message_data = Input::get('message');

        if ($validator->passes()) {
            $message->update($message_data);

            return \Response::json([
            	'success' => 'true', 
            	'redirect_to' => \URL::to($tournament_slug.'/message-box'),
            ]);
        } else {
            return $validator->messages()->toJson();
        }
	}

    public function postPublish($tournament_slug, $message_id)
	{
		$tournament_id = \Tournament::where('slug', $tournament_slug)->pluck('id');
		\TournamentMessage::updateStatus($tournament_id, $message_id);

		$message = \TournamentMessage::find($message_id);
		$message->active = ($message->active == 1) ? 0 : 1;
		$message->save();

		$message_statuses = \TournamentMessage::getStates($tournament_id);

		return \Response::json([
			'success'       => 'true',
			'message_id'    => $message->id,
			'status_bool'   => $message->active,
			'tournament_id' => $message->tournament_id,
			'messages'      => $message_statuses,
        ]);
	}

	public function deleteMessage($tournament_slug, $message_id)
	{
		$message = \TournamentMessage::find($message_id);
        $message->delete();

		return \Response::json([
            'success'    => true,
            'message_id' => $message_id,
		]);
	}
}
