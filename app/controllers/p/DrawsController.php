<?php

namespace P;
use PublicController, View, Input, Request, Response;


class DrawsController extends PublicController {

	public function getIndex($tournament_slug)
	{
		$tournament = \Tournament::with('draws', 'draws.size', 'draws.category')
			->whereSlug($tournament_slug)
			->first();

        $draws_with_matches_list = \TournamentDrawHelper::getDrawsWithMatches();
        $draw_history = \DrawHistory::all();

		$this->layout->title = \LangHelper::get('draws', "Draws");
	    $this->layout->content = View::make('public/draws/index', array(
            'tournament'              => $tournament,
            'draws_with_matches_list' => $draws_with_matches_list,
            'draw_history'            => $draw_history,
	    ));
	}

	public function getBracket($tournament_slug, $draw_id, $draw_type)
	{
		$list_type = ($draw_type == 'q') ? 2 : 1;

		$draw = \TournamentDraw::with('tournament', 'tournament.sponsors', 'tournament.club')->find($draw_id);
        $tournament = $draw->tournament;
		if (!$draw->manual_draw)
		{
			$seeds = \TournamentSignup::with('teams')->whereDrawId($draw_id)
				->whereListType($list_type)
	        	->orderBy('rank')
	        	->take($draw->number_of_seeds)
	        	->get();
		}
		else
		{
			$seeds = \DrawSeed::with('teams')
				->whereDrawId($draw_id)
				->whereListType($list_type)
				->orderBy('seed_num', 'asc')
	        	->get();
		}

		$pairs = \DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data', 'scores')
			->where('draw_id', $draw->id)
			->where('list_type', $list_type)
			->orderBy('id')
			->get();

		// prepare seed keys - user for getting seed number in front of team name
        $seed_ids = !$draw->manual_draw ? array_flip($seeds->lists('id')) : array_flip($seeds->lists('signup_id')); 

		$jsArray = \DrawMatchHelper::getBracketPairs($pairs, $draw, $list_type, $seed_ids);

        $referee_name = ($tournament->referee() && $tournament->referee()->contact[0])
            ? $tournament->referee()->contact[0]->full_name()
            : "";

		$title = ($list_type == 2) ? \LangHelper::get('qualifying_draw_bracket', "Qualifying draw bracket") : \LangHelper::get('main_draw_bracket', "Main draw bracket");
		$this->layout->title = $title;
		$this->layout->content = View::make('public/draws/bracket', array(
			'draw'       => $draw,
            'title'      => $title,
            'jsArray'    => json_encode(array_values($jsArray)),
            'isDoubles'  => $draw->draw_type == "doubles",
            'tournament' => $tournament,
            'referee_name' => $referee_name,
		));
	}


}

