<?php

namespace P;
use PublicController, View, Input, Request, Response, Debug;


class SignupsController extends PublicController {

    public function __construct()
    {
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

	public function getIndex($tournament_slug)
	{

		$tournament = \Tournament::with('draws', 'draws.signups', 'draws.signups.teams')
			->whereSlug($tournament_slug)
			->first();


		$this->layout->title = \LangHelper::get('signups', "Signups");
	    $this->layout->content = View::make('public/signups/index', array(
			'tournament' => $tournament,
            'open_accordion' => Input::get("draw") ?: ""
	    ));
	}

    public function getRegister($tournament_slug, $draw_id)
    {
        $draw = \TournamentDraw::with('tournament', 'category', 'size')
            ->whereHas('tournament', function($query) use ($tournament_slug){
                $query->where('slug', $tournament_slug);
            })->whereId($draw_id)
            ->first();

        $this->layout->title = \LangHelper::get('register', "Register");
        $this->layout->content = View::make('public/signups/register', array(
            'draw'        => $draw,
        ));
    }

    public function getPlayerData() {
        $player_data = [];
        if ($this->logged_user->hasRole('player')) {
            $player_data = [
                'name'           => $this->logged_user->playerInfo('name'),
                'surname'        => $this->logged_user->playerInfo('surname'),
                'date_of_birth'  => $this->logged_user->playerInfo('date_of_birth'),
                'licence_number' => $this->logged_user->player->licence_number,
                'email'          => $this->logged_user->playerInfo('email'),
                'phone'          => $this->logged_user->playerInfo('phone'),
            ];
        }

        return Response::json([
            'data' => $player_data,
        ]);
    }

    public function postRegister($tournament_slug, $draw_id)
    {
        $draw = \TournamentDraw::with('category')->find($draw_id);
        $rules = [
            'name'          => ['required'],
            'surname'       => ['required'],
            'date_of_birth' => ['required'],
        ];
        // first step of validation - simple form validation
        $validation = \Validator::make(Input::all(), $rules);
        
        if ($validation->fails())
            return Response::json([
                'success'  => false,
                'messages' => $validation->messages()->toArray(),
            ]);

        // second step of validation - check if player fits in age category and check if player already signed up for draw
        $validate = \ValidationHelper::validateSignupData(Input::all(), $draw);

        if(is_string($validate)) {
            return Response::json([
                'success' => false,
                'msg' => $validate,
            ]);
        }
        // get current ranking and licence number if player exist in database
        $player_data = \SignupHelper::getPlayerData(Input::all(), $draw);

        //if player data is response object return it for validation messages
        if (!is_array($player_data))
            return $player_data;

        // generate signup data array
        $signup_data = [
            'submited_by'    => \Auth::getUser()->id,
            'tournament_id'  => $draw->tournament_id,
            'draw_id'        => $draw->id,
            'created_at'     => \DateTimeHelper::GetDateNow(),
            'rank'           => $player_data['current_ranking'],
            'ranking_points' => $player_data['ranking_points'],
        ];
        $signup = \TournamentSignup::create($signup_data);

        $team_data = [
            'licence_number'  => $player_data['licence_number'],
            'name'            => Input::get('name'),
            'surname'         => Input::get('surname'),
            'date_of_birth'   => \DateTimeHelper::GetPostgresDateTime(Input::get('date_of_birth')),
            'email'           => Input::get('email'),
            'phone'           => Input::get('phone'),
            'current_ranking' => $player_data['current_ranking'],
            'ranking_points'  => $player_data['ranking_points'],
            'team_id'         => $signup->id,
        ];
        $team = \TournamentTeam::create($team_data);

        // send notification to superadmin if licence number is empty
        if (!$team->licence_number)
            \Notification::TournamentSignupNotif($signup->id, $draw->tournament->title);


        return Response::json([
            'success' => true,
            'message' => \LangHelper::get('successful_signup', 'You have successfully signed up for tournament draw'),
        ]);

    }
}

