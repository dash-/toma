<?php

namespace P;
use PublicController, View, Input, Request, Response;


class NewsController extends PublicController {
    
    public function __construct()
    {
        $this->beforeFilter('csrf', ['on' => 'post']);
    }

	public function getIndex($tournament_slug)
	{
		$tournament_id = \Tournament::whereSlug($tournament_slug)->pluck('id');


		$news = \TournamentArticle::with('images')
			->whereTournamentId($tournament_id)
			->whereActive(1)
			->orderBy('id', 'DESC')
			->get();


		$this->layout->title = \LangHelper::get('news', "News");
	    $this->layout->content = View::make('public/news/index', array(
			'news' => $news,
	    ));
	}

	public function getView($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::with('images')
			->find($article_id);

		$this->layout->title = \LangHelper::get('news', "News").': '.$article->title;
	    $this->layout->content = View::make('public/news/view', array(
			'article' => $article,
	    ));
	}

    public function postComment()
    {
        $rules =  array(
            'text' => array('required'),
        );

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            return $validator->messages()->toJson();
        }
        else
        {
            $comment = \PublicComment::create([
                'user_id'    => \Auth::user()->id,
                'table_id'   => Input::get('table_id'),
                'table_name' => Input::get('table_name'),
                'text'       => Input::get('text'),
            ]);

            return Response::json([
                'success' => true,
                'comment' => \RestHelper::getComments($comment, TRUE),
            ]);
        }
    }

    public function getComments($tournament_slug, $article_id)
    {
        $comments = \PublicComment::with('user', 'user.contact')->whereTableId($article_id)
            ->whereTableName('tournament_articles')
            ->orderBy('id', 'DESC')
            ->get();

        $data = \RestHelper::getComments($comments);

        return Response::json([
            'comments'        => $data,
            'tournament_slug' => $this->tournament_slug,
        ]);
    }

    public function deleteComment($tournament_slug, $comment_id)
    {
        $comment = \PublicComment::find($comment_id);
        $comment->delete();
        return Response::json([
            'success' => true,
        ]);
    }
}

