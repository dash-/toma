<?php

namespace P;

use PublicController, View, Input;

class TournamentController extends PublicController
{

    public function getIndex($tournament_slug)
    {
        $tournament = \Tournament::with('galleries', 'date', 'club', 'sponsors')
            ->whereSlug($tournament_slug)->first();

        if(!$tournament)
            return \Redirect::to("/");

        $score = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->where('match_status', 3)
            ->where('tournament_id', $tournament->id)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->take(5)
            ->get();


//        $today = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
////            ->whereIn('match_status')//, [0, 1])
//            ->where('tournament_id', $tournament->id)
//            ->whereHas('match', function ($query) {
//                $query->whereNotNull('id');
//            })
//            ->get();


        $news = \TournamentArticle::with('images')
            ->whereTournamentId($tournament->id)
            ->whereActive(1)
            ->orderBy('id', 'DESC')
            ->take(3)
            ->get();

        $this->layout->title = 'Tournament public page';
        $this->layout->content = View::make('public/index', array(
            'tournament' => $tournament,
            'score'      => $score,
//            'today'      => $today,
            'news'       => $news,
        ));
    }


    public function getPublicLiveScores($match_id)
    {

        $this->layout = null;

        $scores = \MatchScore::where('match_id', $match_id)
            ->get();

        $result = '';

        foreach ($scores as $score) {
            $result .= $score->set_team1_score . \DrawMatchHelper::matchTie($score->set_team1_tie) . '-' . $score->set_team2_score . \DrawMatchHelper::matchTie($score->set_team2_tie) . '; ';
        }

        return $result;

    }


    public function getDetails($tournament_slug)
    {
        $tournament = \Tournament::with('draws', 'organizer', 'surface', 'date', 'draws.category', 'draws.size')
            ->whereSlug($tournament_slug)
            ->first();

        $this->layout->title = 'Tournament details';
        $this->layout->content = View::make('public/tournament/details', array(
            'tournament' => $tournament,
        ));
    }

    public function getContactUs($tournament_slug)
    {
        $tournament = \Tournament::whereSlug($tournament_slug)
            ->first();

        $this->layout->title = 'Contact us';
        $this->layout->content = View::make('public/tournament/contact_us', array(
            'tournament' => $tournament,
        ));
    }


    public function postContactUs($tournament_slug)
    {
        $rules = array(
            'name'  => array('required'),
            'email' => array('required', 'email'),
            'msg'   => array('required'),
        );

        $validator = \Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $data = Input::all();

            \EmailLog::create([
                'email_from' => Input::get('email'),
                'email_to'   => 'sasa@dash.ba',
                'created_at' => \DateTimeHelper::GetDateNow(),
                'full_body'  => View::make('emails/public_contact', $data)->render(),
            ]);


            \Mail::send('emails.public_contact', $data, function ($message) {
                $message->from('info@dash.ba', 'Dash');
                $message->to('sasa.milasinovic@gmail.com');

            });

            \Log::info(View::make('emails/public_contact', $data)->render());

            return \Response::json([
                'success' => 'true',
            ]);

        } else
            return $validator->messages()->toJson();
    }
}

