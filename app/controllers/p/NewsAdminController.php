<?php

namespace P;
use PublicController, View, Input;

class NewsAdminController extends PublicController {

	public function __construct()
    {
        $this->beforeFilter('auth.all_admins');
    }

	public function getIndex($tournament_slug)
	{

		$tournament = \Tournament::with('articles', 'articles.user')
			->whereSlug($tournament_slug)
			->first();

		$this->layout->title = \LangHelper::get('news_administration', "News administration");
		$this->layout->content = View::make('public/news/admin/index', array(
				'tournament' => $tournament,
			));
	}

	public function getCreate($tournament_slug)
	{
		$this->layout->title = \LangHelper::get('add_new_article', "Add new article");
        $this->layout->content = View::make('public/news/admin/create', array(
            
        ));
	}


	public function postCreate($tournament_slug)
	{
		$tournament_id = \Tournament::whereSlug($tournament_slug)
			->pluck('id');

        $rules = array(
            'article.title' => array('required'),
            'article.content' => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);
        $article_data = Input::get('article');

        if ($validator->passes()) {
            $article = \TournamentArticle::create($article_data + [
				'created_by'    => \Auth::user()->id,
				'tournament_id' => $tournament_id
            ]);

            return \Response::json([
            	'success' => 'true', 
            	'redirect_to' => \URL::to($tournament_slug.'/news-admin'),
            ]);
        } else {
            return $validator->messages()->toJson();
        }
	}

	public function getEdit($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::find($article_id);


		$this->layout->title = \LangHelper::get('edit_article', "Edit article");
        $this->layout->content = View::make('public/news/admin/edit', array(
       		'article' => $article, 
        ));
	}

	public function postEdit($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::find($article_id);
        $rules = array(
            'article.title' => array('required'),
            'article.content' => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);
        $article_data = Input::get('article');

        if ($validator->passes()) {
            $article->update($article_data);

            return \Response::json([
            	'success' => 'true', 
            	'redirect_to' => \URL::to($tournament_slug.'/news-admin'),
            ]);
        } else {
            return $validator->messages()->toJson();
        }
	}

	public function getImages($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::with('images')->find($article_id);

		$this->layout->title = \LangHelper::get('article_images', "Article images");
        $this->layout->content = View::make('public/news/admin/images', array(
       		'article' => $article, 
        ));
	}

	public function postImages($tournament_slug, $article_id)
    {
        $rules = array(
            'image' => 'required',
        );

        $image = Input::file('image');

        if($image==''){
            $image = Input::file("file");
        }

        if (is_null($image))
            return \Redirect::to(\URL::to($tournament_slug.'/news-admin/'));

        $fname = (is_object($image)) ? $image->getClientOriginalName() : $image[0]->getClientOriginalName();

        if ($image)
            $filename = \Str::random().'_'.$fname;

        $save_path = public_path().'/uploads/images';

        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->passes())
        {
            if ($image) {

            	$image = (is_object($image)) ? $image : $image[0];

                \Image::make($image)->save($save_path.'/articles/full/'.$filename);

                \Image::make($image)->resize(280, 210, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(280, 210)->save($save_path.'/articles/thumbnails/'.$filename);
                
                \Image::make($image)->resize(100, 85, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(100, 85)->save($save_path.'/articles/small_thumbnails/'.$filename);
            }

            \ArticleImage::create([
            	'image' => $filename,
            	'article_id' => $article_id,
            ]);

            return \Redirect::to(\URL::to($tournament_slug.'/news-admin/images/'.$article_id));
            // return \Response::json(['success' => true]);
        }
        else
        {
            return \Redirect::to(\URL::to($tournament_slug.'/news-admin/images/'.$article_id));
        }
    }


	public function postPublish($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::find($article_id);

		$article->active = ($article->active == 1) ? 0 : 1;
		$article->save();

		return \Response::json([
            'success'    => 'true', 
            'state'      => ($article->active == 1) ? 'Unpublish' : 'Publish',
            'status'     => $article->status(),
            'article_id' => $article->id,
        ]);
	}

	public function deleteArticle($tournament_slug, $article_id)
	{
		$article = \TournamentArticle::with('images')->find($article_id);

		foreach ($article->images as $image) {
			\File::delete(public_path().'/uploads/images/articles/full/'.$image->image);
			\File::delete(public_path().'/uploads/images/articles/thumbnails/'.$image->image);
			\File::delete(public_path().'/uploads/images/articles/small_thumbnails/'.$image->image);
			$image->delete();
		}

		$article->delete();

		return \Response::json([
			'success' => true,
			'article_id' => $article_id,
		]);
	}

	public function deleteImage($tournament_slug, $image_id)
	{
		$image = \ArticleImage::find($image_id);

		\File::delete(public_path().'/uploads/images/articles/full/'.$image->image);
		\File::delete(public_path().'/uploads/images/articles/thumbnails/'.$image->image);
		\File::delete(public_path().'/uploads/images/articles/small_thumbnails/'.$image->image);
		$image->delete();

		return \Response::json([
			'success' => true,
			'article_id' => $image_id,
		]);
	}

}
