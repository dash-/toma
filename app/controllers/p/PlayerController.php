<?php

namespace P;

use PublicController, View, Input;

class PlayerController extends PublicController
{
    public function getInfo($tournament_slug, $id)
    {
        $team = \TournamentTeam::find($id);

        $teamid = $team->team_id;

        $lic_num = $team->licence_number;

        $player = \Player::with('contact')->whereLicenceNumber($lic_num)->first();

        $results = \DrawMatch::with('scores')
            // ->where('match_status', 4)
            ->where('team1_id', $teamid)
            ->orWhere('team2_id', $teamid)
            // ->take(5)
            ->get();

        // echo \Debug::vars($results->toArray());die;
        $this->layout->title = \LangHelper::get('players', 'Player') . " - " . $player->contact->name;
        $this->layout->content = View::make('public/player/info', array(
            'player'  => $player,
            'results' => $results,
        ));

    }

    public function getSearch()
    {
        $this->layout->title = \LangHelper::get('player_search', 'Player search');
        $this->layout->content = View::make('public/player/search', array());
    }

    public function getInformations($tournament_slug, $player_id)
    {
        $player = \Player::with('contact')->find($player_id);

        if (is_null($player))
            return \Redirect::to(\URL::to($tournament_slug.'/tournament'));

        $this->layout->title = \LangHelper::get('players', 'Player') . " - " . $player->contact->name;
        $this->layout->content = View::make('public/player/search_info', array(
            'player' => $player,
        ));
    }


}
