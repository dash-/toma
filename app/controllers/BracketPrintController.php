<?php

class BracketPrintController extends BaseController {
	
	public function getPrint($draw_id) {

		$draw = TournamentDraw::with('tournament', 'size', 'qualification')->find($draw_id);

		$list_type = (Request::query('list_type')) ? Request::query('list_type') : 1;

		$total = ($list_type == 1) ? $draw->size->total : $draw->qualification->draw_size;

		if ($total <= 64)
			return Redirect::to('bracketprint/print2/'.$draw->id.'?list_type='.$list_type);

		// if (!$draw->manual_draw)
		// {
		// 	$seeds = TournamentSignup::with('teams')
		// 		->whereDrawId($draw_id)
		// 		->whereListType($list_type)
	 //        	->orderBy('rank')
	 //        	->take($draw->number_of_seeds)
	 //        	->get();
		// }
		// else
		// {
			$seeds = DrawSeed::with('teams')
				->whereDrawId($draw_id)
				->whereListType($list_type)
				->orderBy('seed_num', 'asc')
	        	->get();
		// }

		$pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data', 'scores')
				->where('draw_id', $draw->id)
				->where('list_type', $list_type)
				->orderBy('id')
				->get();


		$brackets = array();	
		$final_brackets = array();	

		// vrti parove da bi se dobio array sa keyevima rundi
		foreach ($pairs as $pair) {
			$brackets[$pair["round_number"]][] = $pair->toArray();

		}

		// vrti runde i kreiraj lijevu i desnu stranu
		foreach ($brackets as $round_number => $bracket) {


			// splitaj array na dva jednaka dijela			
			$len = count($bracket);

			$firsthalf = array_slice($bracket, 0, $len / 2);
			$secondhalf = array_slice($bracket, $len / 2);

			$first_len = count($firsthalf);
			$second_len = count($secondhalf);


			$quarter11 = array_slice($firsthalf, 0 , $first_len/2);
			$quarter12 = array_slice($firsthalf, $first_len/2);

			$quarter21 = array_slice($secondhalf,0,  $second_len/2);
			$quarter22 = array_slice($secondhalf, $second_len/2);

			// echo Debug::vars($quarter22);die;

			$final_brackets['L1'][$round_number] = $quarter11;
			$final_brackets['R1'][$round_number] = $quarter12;
			$final_brackets['L2'][$round_number] = $quarter21;
			$final_brackets['R2'][$round_number] = $quarter22;

			}

		$finalists = array_pop($final_brackets['R2']);
		// echo Debug::vars($finalists);die;


		$winner = ($finalists[0]['team1_score'] == $finalists[0]['team2_score'] ? NULL : ($finalists[0]['team1_score'] > $finalists[0]['team2_score'] ? $finalists[0]['team1_data'] : $finalists[0]['team2_data']));

		if (!count($pairs))
			return Redirect::to('signups/final-list/'.$draw_id);


		$this->layout->title = 'Print brackets';
		$this->layout->content = View::make('draws/print_draws', [
			'draw'       => $draw,
			'pairs'      => $pairs,
			'finalists'  => $finalists,
			'winner'	 => $winner,
			'left_side1'  => $final_brackets["L1"],
			'left_side2'  => $final_brackets["L2"],
			'right_side1' => $final_brackets["R1"],
			'right_side2' => $final_brackets["R2"],
			'round_number' => $round_number,
		]);

	}

	public function getPrint2($draw_id)
	{
		$draw = TournamentDraw::with('tournament', 'size', 'qualification')->find($draw_id);
		
		$list_type = (Request::query('list_type')) ? Request::query('list_type') : 1;

		$total = ($list_type == 1) ? $draw->size->total : $draw->qualification->draw_size;
  
		if ($total > 64)
			return Redirect::to('bracketprint/print/'.$draw->id.'?list_type='.$list_type);

		$pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
			->where('draw_id', $draw_id)
			->where('list_type', $list_type)
			->orderBy('id')
			->get();


		$seeds = DrawSeed::with('teams')
			->whereDrawId($draw_id)
			->whereListType($list_type)
			->orderBy('seed_num', 'asc')
        	->get();

        	// echo Debug::vars($seeds ->toArray());die;

		$brackets = array();	
		$final_brackets = array();	

		// vrti parove da bi se dobio array sa keyevima rundi
		foreach ($pairs as $pair) {
			$brackets[$pair["round_number"]][] = $pair->toArray();
		}

		// vrti runde i kreiraj lijevu i desnu stranu
		foreach ($brackets as $round_number => $bracket) {
			
			// splitaj array na dva jednaka dijela

			$len = count($bracket);
			$firsthalf = array_slice($bracket, 0, $len / 2);
			$secondhalf = array_slice($bracket, $len / 2);

			$all['L'][$round_number]= $bracket;

			$final_brackets['L'][$round_number] = $firsthalf;
			$final_brackets['R'][$round_number] = $secondhalf;

		}

		$finalists = array_pop($final_brackets['R']);

		$winner = ($finalists[0]['team1_score'] == $finalists[0]['team2_score'] ? NULL : ($finalists[0]['team1_score'] > $finalists[0]['team2_score'] ? $finalists[0]['team1_data'] : $finalists[0]['team2_data']));

		if (!count($pairs))
			return Redirect::to('signups/final-list/'.$draw_id); 


		if($total <= 32) 
		{
			$this->layout->title = 'Print brackets';
			$this->layout->content = View::make('draws/print_draws3', array(
				'draw'       => $draw,
				'pairs'      => $pairs,
				'all'		=> $all['L'],
				'round_number' => $round_number,
				));
		}
		else 
		{

		$this->layout->title = 'Print brackets';
		$this->layout->content = View::make('draws/print_draws2', array(
			'draw'       => $draw,
			'pairs'      => $pairs,
			'left_side'  => $final_brackets["L"],
			'right_side' => $final_brackets["R"],
			'finalists'  => $finalists,
			'winner'	 => $winner,
			'round_number' => $round_number,
		));

		}
	}

}