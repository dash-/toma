<?php

class VideosController extends BaseController{

    public function getIndex()
    {
        $this->layout->title = 'Video tutorials';
        $this->layout->content = View::make('videos/index');
    }
}