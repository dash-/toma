<?php

class RankingsController extends BaseController
{

    public function getIndex()
    {
        $items_per_page = array(
            50    => 50,
            100   => 100,
            200   => 200,
            500   => 500,
            1000  => 1000,
            10000 => 10000
        );

        $types_array = [
            '0' => 'All',
            'M' => 'Male',
            'F' => 'Female',
        ];

        $regions = [null => LangHelper::get('all_federations', 'All federations')]
            + Region::orderBy('region_name')->get()->lists('region_name', 'id');

        $movers_male = Player::join('contacts', 'contacts.id', '=', 'players.contact_id')
            ->whereNotNull("ranking_move")
            ->where("ranking_move", ">", 0)
            ->where("contacts.sex", "M")
            ->orderBy("ranking_move", "DESC")
            ->limit(5)
            ->get();

        $movers_female = Player::join('contacts', 'contacts.id', '=', 'players.contact_id')
            ->whereNotNull("ranking_move")
            ->where("ranking_move", ">", 0)
            ->where("contacts.sex", "F")
            ->orderBy("ranking_move", "DESC")
            ->limit(5)
            ->get();

        $last_weekly_recalculation = RankingCalculationHistory::where("ranking_type", 0)->orderBy('date', 'DESC')->first();
        $last_quarter_recalculation = RankingCalculationHistory::where("ranking_type", 1)->orderBy('date', 'DESC')->first();

        $this->layout->title = LangHelper::get('rankings', "Rankings");
        $this->layout->content = View::make('rankings/index', array(
            'items_per_page' => $items_per_page,
            'types_array'    => $types_array,
            'movers_male'    => $movers_male,
            'movers_female'  => $movers_female,
            'regions'        => $regions,
            'last_weekly_recalculation' => $last_weekly_recalculation,
            'last_quarter_recalculation' => $last_quarter_recalculation,
        ));
    }


    public function getData()
    {
        $items = Request::query('items_counter');
        $order = Request::query('order') ? Request::query('order') : "player.id";
        $sort = Request::query('sort');

        $query = Request::query();

        if (Request::query('search') == 1) {
            $posts = Request::query();

            $query = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id')
                ->where('players.ranking', '>', 0);

            foreach ($posts as $key => $post) {
                $key = str_replace("INT_", "", $key, $int);

                if ($post AND !in_array($key, array('items_counter', 'order', 'sort', 'search', 'callback', 'page'))) {
                    if ($int)
                        $query->where($key, '=', $post);
                    else
                        $query->where($key, 'ILIKE', "%" . $post . "%");
                }
            }

            if (Request::query('sex')) {
                $query = $query->where('sex', Request::query('sex'));
            } else
                $query = $query->where('sex', 'M');

            if (Request::query('federation_club_id')) {
                $query = $query->where('federation_club_id', Request::query('federation_club_id'));
            }

            $players = $query->orderBy($order, $sort)->paginate($items);

            return Response::json(
                $players->toArray() + [
                    'user_role'      => Auth::getUser()->getRole('id'),
                    'user_region_id' => Auth::getUser()->getUserRegion()
                ])->setCallback(Request::get('callback'));
        }

        $players = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id')
            ->where('players.ranking', '>', 0);
        if (Request::query('sex')) {
            $players = $players->where('sex', Request::query('sex'));
        } else
            $players = $players->where('sex', 'M');

        if (Request::query('federation_club_id')) {
            $players = $players->where('federation_club_id', Request::query('federation_club_id'));
        }

        $players = $players->orderBy($order, $sort)->paginate($items);

        return Response::json($players->toArray() + [
                'user_role' => Auth::getUser()->getRole('id'),
                'user_region_id' => Auth::getUser()->getUserRegion()
            ])->setCallback(\Request::get('callback'));
    }

    public function getDownloadExport($type)
    {
        $last_export = ExportRanking::where('doc_type', $type)
            ->orderBy('id', 'DESC')
            ->first();

        $ext = ($type == 3 || $type == 4 ? '.pdf' : '.xlsx');
        $export_name = ($last_export) ? $last_export->document_name : "";
        $app_path = app_path();
        // $filePath = public_path().'/exports/'.$key;
        $filePath = $app_path . '/storage/exports/' . $export_name . $ext;

        if (!File::exists($filePath)) {
            return "File does not exist.";
        }

        return Response::download($filePath);
    }

} 