<?php
class MessagesController extends BaseController {

	
	public function getIndex()
	{
		$messages = Auth::getUser()
			->messages()
			->selectRaw("DISTINCT ON (messages.conversation_id) *")
			->orderBy('messages.conversation_id', 'desc')
            ->whereNull('type_id')
			->paginate(35);

		$view_file = View::make('messages/inbox', array(
			'messages'  => $messages,
		));

		if (Request::query('type') == 'outbox') {
			$messages = Message::selectRaw("DISTINCT ON (messages.conversation_id) *")
				->where('sender_id', '=', Auth::getUser()->id)
				->orderBy('conversation_id', 'desc')
				->paginate(35);

			$view_file = View::make('messages/outbox', array(
				'messages'  => $messages,
			));
		} else if (Request::query('type') == 'requests') {
            $messages = Auth::getUser()
                ->messages()
                ->selectRaw("DISTINCT ON (messages.conversation_id) *")
                ->orderBy('messages.conversation_id', 'desc')
                ->whereTypeId(1)
                ->paginate(35);

            $view_file = View::make('messages/inbox', array(
                'messages'  => $messages,
            ));
        }
        
                // echo Debug::vars($messages->toArray());die;
		$this->layout->title = LangHelper::get('messages', "Messages");
		$this->layout->content = View::make('messages/index', array(
			'messages'  => $messages,
			'view_file' => $view_file,
		));
	}

	public function getShow($id)
	{
		$first_message = Message::where('conversation_id', '=', $id)->first();
		$messages = Message::where('conversation_id', '=', $id)->get();

        // echo Debug::vars($first_message->getTo());die;

		MessageHelper::setSeen($id);
		
		$this->layout->title = LangHelper::get('messages_conversation', "Messages conversation");
		$this->layout->content = View::make('messages/show', array(
			'messages'      => $messages,
			'first_message' => $first_message,
		));
	}

	public function getUsers()
	{
		$query = Request::query('query');

		$users = User::where('username', 'ILIKE', "%".$query."%")
            ->where('id', '!=', Auth::user()->id)
            ->take(10)->get();
		$users_array = [];

		foreach ($users as $user) {
			$item['id'] = $user->id;
			$item['text'] = $user->username;

			$users_array[] = $item;
		}

		return Response::json($users_array);
	}

	public function getCreate()
	{
		$this->layout->title = LangHelper::get('new_message', "New message");
		$this->layout->content = View::make('messages/create', array(
		));
	}

	public function postCreate()
	{
		$rules = array(
            // 'messages.user_id' => array('required'),
            'messages.text'    => array('required'),
            'messages.subject' => array('required'),
        );
        $messages = array(
            'required' => 'This field is required.',
        );
        $last_conversation_id = Message::max('conversation_id');

        $validator = Validator::make(array_dot(Input::all()), $rules, $messages);

        $message_data = Input::get('messages');

        if ($validator->passes())
        {
            $message = Message::create(array(
				'subject'         => $message_data['subject'],
				'text'            => $message_data['text'],
				'sender_id'       => Auth::getUser()->id,
				'time_sent'       => DateTimeHelper::getDateNow(),
				'conversation_id' => $last_conversation_id + 1,
            ));

            
            if (is_array($message_data['user_id'])) 
            {
	            foreach($message_data['user_id'] as $usr)
	            {
	            	$message->users()->attach($usr['id']);
	            	Notification::MessageNotif($usr['id'], $message->conversation_id);
	            }
            }
            else
            {
            	$message->users()->attach($message_data['user_id']);
            	Notification::MessageNotif($message_data['user_id'], $message->conversation_id);
            }
            

            return Response::json(array('success' => 'true', 'redirect_to' => '/messages'));

	        }
	        
	        else
	        {
	            return $validator->messages()->toJson();
	        }
	}

	public function postReply($id)
	{
		$rules = array(
            'messages.text' => array('required'),
        );

        $messages = array(
            'required' => 'This field is required.',
        );

        $validator = Validator::make(array_dot(Input::all()), $rules, $messages);

        $message_data = Input::get('messages');
        // dd($message_data);
        if ($validator->passes())
        {
            $message = Message::create(array(
            	'text'            => $message_data['text'],
            	'subject'         => $message_data['subject'],
				'sender_id'       => Auth::getUser()->id,
				'time_sent'       => DateTimeHelper::getDateNow(),
				'conversation_id' => $id,
            ));

            foreach($message_data['user_id'] as $usr)
            {
            	$message->users()->attach($usr['id']);
            	Notification::MessageNotif($usr['id'], $message->conversation_id);
            }
                        
            return Response::json(array('success' => 'true', 'redirect_to' => '/messages'));
        }
        else
        {
            return $validator->messages()->toJson();
        }
	}

	public function deleteM($id) {
		$message = Message::find($id);

		$message->deleted_at = DateTimeHelper::getDateNow();

		$message->save();

		return \Response::json(array('success' => true));
	}

	public function getUnread($id)
	{
		$messages = Auth::user()->messages()
			->where('conversation_id', '=', $id)->get();


		foreach($messages as $message)
		{
			$message->pivot->seen = FALSE;
			$message->pivot->save();
		}

		return  Redirect::to('/messages');
	}

}
