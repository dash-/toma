<?php

class MatchController extends BaseController
{

    public function getIndex($match_id)
    {
//        $match_score = MatchScore::where("match_id", $match_id)->where("set_number", 1)->first();
//        dd($match_score == null);
//        die;
    }

    public function getDummy($match_id)
    {
        $match = DrawMatch::find($match_id);

        $game_score = GameScore::create([
            'match_id' => $match_id,
            'team1_id' => $match->team1_id,
            'team2_id' => $match->team2_id,
            'game_number' => 1,
            'set_number' => 1,
            'served_by' => 1,
            'game_winner' => 1,
            'break_point' => 0,
        ]);

        $game_details = GameScoreDetail::create([
            'game_score_id' => $game_score->id,
            'team1_score' => 15,
            'team2_score' => 0,
        ]);

        return Redirect::to('/draws/db-bracket/' . $match->draw_id . '?list_type=' . $match->list_type);
    }

    public function getMatchLiveMode($match_id)
    {
//        var_dump(Agent::isMobile());die;

        $match = DrawMatch::with("draw", "team1_data", "team2_data")->where("id", $match_id)->get()->first();
        $tournament = Tournament::where("id", $match->draw->tournament_id)->get()->first();
        $this->layout->title = "Match live mode";
        $this->layout->content = View::make('match/live_mode', array(
            'match' => $match,
            "tournament" => $tournament,
            "is_mobile" => Agent::isMobile()
        ));
    }

    public function getMatchData($match_id)
    {
        $match = MatchSchedule::where("match_id", $match_id)->get()->first();

        if (is_null($match))
            return ['success' => false];

        if ($match->match_status < 3)
            return Response::json(array("match" => $match->toArray(), "success" => true));

        $draw_rules = TournamentDraw::where("id", $match->draw_id)->first();

        $game_score = GameScore::select("id", "game_number", "set_number", "served_by", "game_winner")
            ->where("match_id", $match_id)
            ->orderBy("id", "DESC")
            ->get()
            ->first();
        if (!$game_score) {
            $game_score = new GameScore();
            $game_score->match_id = $match_id;
            $game_score->game_number = 1;
            $game_score->set_number = 1;
            $game_score->served_by = 0;
            $game_score->game_winner = 0;
            $game_score->break_point = 0;
            $game_score->save();
        }
        $tiebreak_score = '';

        if ($game_score) {
            $game_score_details = GameScoreDetail::select("team1_score", "team2_score")
                ->where("game_score_id", $game_score->id)
                ->orderBy("id", "DESC")
                ->get()
                ->first();

            $tiebreak_score = GameScoreTiebreak::where("game_number_id", $game_score->game_number)
                ->where("match_id", $match_id)
                ->orderBy("id", "DESC")->first();
//                ->get();
        }

//        GameScoreTiebreak::create([
//            'match_id' => $match_id,
//            'game_score_id' => 15,
//            'game_number_id' => $game_score->game_number,
//            'team1_tiescore' => 0,
//            'team2_tiescore' => 0
//        ]);

//        $tiebreak_score = GameScoreTiebreak::where("game_number_id",  $game_score->game_number)->first();

        if (!$game_score_details) {
            $game_score_details = new GameScoreDetail();
            $game_score_details->game_score_id = $game_score->id;
            $game_score_details->team1_score = 0;
            $game_score_details->team2_score = 0;
            $game_score_details->save();
        }

        //TODO: povuci tie score i povezati
        $match_scores = MatchScore::where("match_id", $match_id)->orderBy("id")->get();
        $set_in_progress = $match_scores->max("set_number");
        $team1_score = array_pluck($match_scores, "set_team1_score");
        $team2_score = array_pluck($match_scores, "set_team2_score");
        $team1_tiescore = array_pluck($match_scores, "set_team1_tie");
        $team2_tiescore = array_pluck($match_scores, "set_team2_tie");


        return Response::json(array(
            "match"              => $match->toArray(),
            "team1_score"        => $team1_score,
            "team2_score"        => $team2_score,
            "team1_tiescore"        => $team1_tiescore,
            "team2_tiescore"        => $team2_tiescore,
            "tiebreak_score" => $tiebreak_score ? $tiebreak_score->toArray() : "",
            "draw_rules" => $draw_rules,
            "success" => true,
            "set_in_progress"    => $set_in_progress,
            "game_score"         => $game_score ? $game_score->toArray() : "",
            "game_score_details" => $game_score_details ? $game_score_details->toArray() : "",
        ));

    }

    public function postStartMatch()
    {
        $match_id = Input::get('match_id');
        if ($match_id && is_numeric($match_id)) {
            $match_schedule = MatchSchedule::where("match_id", $match_id)->first();
            $match_schedule->match_status = 3;
            $match_schedule->match_started_at = DateTimeHelper::GetDateNow();
            $match_schedule->save();

            return Response::json(array("success" => true));
        }
        return Response::json(array("success" => false));
    }

    public function postSaveGamePoint($action = 'add')
    {
        try {


            $post = Input::all();
            if ($post) {
                $game_score_details = new GameScoreDetail();

                $team1_score = Input::get('gamescore.currentScoreTeam1');
                $team2_score = Input::get('gamescore.currentScoreTeam2');
                $game_score_id = Input::get('game_score_id');

                if (!is_numeric($game_score_id) || !is_numeric($team1_score) || !is_numeric($team2_score) || $team1_score < 0 || $team1_score > 6 || $team2_score < 0 || $team2_score > 6)
                    return;

                $game_score_details->game_score_id = $game_score_id;
                $game_score_details->team1_score = $team1_score;
                $game_score_details->team2_score = $team2_score;
                $game_score_details->save();
                return Response::json(array("success" => true));
            }

        } catch (Exception $ex) {
            return Response::json(array("success" => false), 500);

        }
        return Response::json(array("success" => false), 500);
    }

    public function postSaveTiebreakPoint($action = 'add')
    {
//        try {
            $post = Input::all();
            if ($post) {
                $game_score_tiebreak = new GameScoreTiebreak();

                $team1_tiepoint = Input::get('tiepoints_team1');
                $team2_tiepoint = Input::get('tiepoints_team2');
                $game_score_id = Input::get('game_score_id');
                $game_number_id = Input::get('game_number_id');
                $set_in_progress = Input::get('set_in_progress');
                $match_id = Input::get('match_id');

                if (!is_numeric($game_score_id) || !is_numeric($set_in_progress) || !is_numeric($match_id) || !is_numeric($game_number_id) || !is_numeric($team1_tiepoint) || !is_numeric($team2_tiepoint))// || $team1_tiepoint < 0 || $team1_tiepoint > 6 || $team2_tiepoint < 0 || $team2_tiepoint > 6)
                    return;

                $game_score_tiebreak->match_id = $match_id;
                $game_score_tiebreak->game_number_id = $game_number_id;
                $game_score_tiebreak->team1_tiescore = $team1_tiepoint;
                $game_score_tiebreak->team2_tiescore = $team2_tiepoint;
                $game_score_tiebreak->game_score_id = $game_score_id;
                $game_score_tiebreak->save();

                MatchScore::where("match_id", $match_id)
                    ->where("set_number", $set_in_progress)->update(['set_team1_tie' => $team1_tiepoint,
                        'set_team2_tie' => $team2_tiepoint]);

                return Response::json(array("success" => true));
            }

//        } catch (Exception $ex) {
//            return Response::json(array("success" => false), 500);
//
//        }
        return Response::json(array("success" => false), 500);
    }

    public function postGameWin()
    {
        try {
            $post = Input::all();
            if ($post) {
                $match_id = Input::get('match_id');
                $game_won = Input::get('game_won');
                $game_number = Input::get('game_number');
                $game_score_id = Input::get('game_score_id');
                $set_number = Input::get('set_number');
                $served_by = Input::get('served_by');
                $team1_score = Input::get('team1_score');
                $team2_score = Input::get('team2_score');
                $setpoints_team1 = Input::get('setpoints_team1');
                $setpoints_team2 = Input::get('setpoints_team2');

                $game_score_details_final = new GameScoreDetail();
                $game_score_details_final->game_score_id = $game_score_id;
                $game_score_details_final->team1_score = $team1_score;
                $game_score_details_final->team2_score = $team2_score;
                $game_score_details_final->save();

                $game_score_update = GameScore::find($game_score_id);
                if ($game_score_update->served_by == 0) {
                    $game_score_update->served_by = $served_by == 1 ? 2 : 1;
                }
                $game_score_update->game_winner = $game_won;
                $game_score_update->save();

                $game_score = new GameScore();
                $game_score->match_id = $match_id;
                $game_score->game_number = $game_number;
                $game_score->break_point = 0;
                $game_score->set_number = $set_number;
                $game_score->game_winner = 0;
                $game_score->served_by = $served_by;
                $game_score->save();

                $game_score_details = new GameScoreDetail();
                $game_score_details->game_score_id = $game_score->id;
                $game_score_details->team1_score = 0;
                $game_score_details->team2_score = 0;
                $game_score_details->save();

                $match_score = MatchScore::where("match_id", $match_id)->where("set_number", $set_number)->first();
                if ($match_score == null) {
                    $match_score = new MatchScore();
                    $match_score->match_id = $match_id;
                    $match_score->set_number = $set_number;
                }
                $match_score->set_team1_score = $setpoints_team1;
                $match_score->set_team2_score = $setpoints_team2;
                $match_score->save();

                return Response::json(array("success" => true, 'game_score_id' => $game_score->id));
            }
            return Response::json(array("success" => false), 500);

        } catch (Exception $ex) {

            return Response::json(array("success" => false), 500);

        }
        return Response::json(array("success" => false), 500);
    }

    public function postSaveSetWin()
    {
        $teamWon = Input::get("team");
        $match_id = Input::get("match_id");
        $team1_score = Input::get("team1_score");
        $team2_score = Input::get("team2_score");
        $set_number = Input::get("set_in_progress");

        if (!$match_id || !is_numeric($match_id) ||
            !is_numeric($team1_score) ||
            !is_numeric($team2_score) ||
            !is_numeric($set_number)
        )
            return Response::json(array("success" => false), 500);

        $match_score_update = MatchScore::where("match_id", $match_id)->where("set_number", $set_number)->first();
        $match_score_update->set_team1_score = $team1_score;
        $match_score_update->set_team2_score = $team2_score;
        $match_score_update->save();

        $match_scores = new MatchScore();
        $match_scores->set_number = $set_number + 1;
        $match_scores->set_team1_score = 0;
        $match_scores->set_team2_score = 0;
        $match_scores->match_id = $match_id;
        $match_scores->save();


        return Response::json(array("success" => true));

    }

    public function postManualFinishSet()
    {
        //TODO: reset game id
        $match_id = Input::get("match_id");
        $new_set = Input::get("new_set");
        $game_number = Input::get("game_number");

        if ($match_id && $new_set) {
            $match_score = new MatchScore();
            $match_score->match_id = $match_id;
            $match_score->set_number = $new_set;
            $match_score->set_team1_score = 0;
            $match_score->set_team2_score = 0;
            $match_score->save();

            $game_score = new GameScore();
            $game_score->game_number = $game_number;

            return Response::json(array("success" => true));
        }
        return Response::json(array("success" => false), 500);
    }

    public function postFinishMatch()
    {
        $match_id = Input::get("match_id");
        if ($match_id) {
            $match_schedule = MatchSchedule::where("match_id", $match_id)->get()->first();
            $match_schedule->match_status = 4;
            $finished_at = DateTimeHelper::GetDateNow();
            $match_schedule->match_finished_at = $finished_at;
            $match_schedule->save();

//            if new set is created, score for that set is 0:0 - remove this score if match is finished
            $match_last_score = MatchScore::where("match_id", $match_id)->orderBy("id", "DESC")->get()->first();
            if ($match_last_score && $match_last_score->set_team1_score == 0 && $match_last_score->set_team2_score == 0)
                $match_last_score->delete();

            $match_scores = MatchScore::where("match_id", $match_id)->get();
            if ($match_scores) {
                $team1_final_score = 0;
                $team2_final_score = 0;
                foreach ($match_scores as $match_score) {
                    if ($match_score->set_team1_score > $match_score->set_team2_score)
                        $team1_final_score += 1;
                    elseif ($match_score->set_team2_score > $match_score->set_team1_score)
                        $team2_final_score += 1;
                }



                $draw_match = DrawMatch::find($match_id);
                $draw_match->completed = true;
                $draw_match->team1_score = $team1_final_score;
                $draw_match->team2_score = $team2_final_score;
                $draw_match->save();

                DrawMatchHelper::sendTeamToNextRound($draw_match);
            }

            return Response::json(array("success" => true, "finished_at" => $finished_at));
        }
        return Response::json(array("success" => false), 500);
    }
} 