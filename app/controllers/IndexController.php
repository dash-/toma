<?php

class IndexController extends BaseController {

	public function getIndex()
	{
        if (Auth::check() && Auth::getUser()->hasRole("standard_user")) {
            Auth::logout();
            return Redirect::to("/");

//                Redirect::to("/logout");
//                die;
        }

        $pageHelper = new PageHelper();
        if(Auth::getUser()->hasRole("referee"))
            $pageHelper->refereePage($this);
        elseif(Auth::getUser()->hasRole("player"))
            $pageHelper->playerPage($this);
        else
            $pageHelper->adminPage($this);
	}

	public function getLocale($lang)
	{
		Session::put('my.locale', $lang);
		return Redirect::to(URL::previous());
	}

	public function getParse()
	{
		$file = public_path().'/csv/translations.csv';

		$parsed = ParserHelper::csvToArray($file);

		echo Debug::vars($parsed ? 'Done!' : "Failed!");die;

	}

    public function getError(){
        $this->layout->title = LangHelper::get("page_not_found", "Page not found");
        $this->layout->content = View::make('errors.missing');
    }
	public function getLiveScore($match_id)
	{
		$this->layout = NULL;

		$scores = MatchScore::where('match_id', $match_id)
			->get();

		$result = '';

		foreach ($scores as $score) {
			$result .= $score->set_team1_score.DrawMatchHelper::matchTie($score->set_team1_tie).'-'.$score->set_team2_score.DrawMatchHelper::matchTie($score->set_team2_tie).'; ';
		}

		return $result;


		// $live_score = DrawMatchHelper::getLiveScores();

		// return View::make('partials/live_score', array( 
		// 	'live_score' => $live_score,
		// ));

	}

	public function getDbBracket($draw_id)
	{
		$list_type = (Request::query('list_type')) ? Request::query('list_type') : 2;

		$draw = TournamentDraw::find($draw_id);

		$tournament = $draw->tournament;

		$matches = DrawMatch::where(function ($query) {
		    	$query->where('team1_id',  0)
	        		->orWhere('team2_id', 0);
			})
			->where('draw_id', $draw_id)
			->whereRoundNumber(1)
			->whereListType($list_type)
			->orderBy('id')
			->get();

		if (count($matches))
			DrawMatchHelper::setUpInitialScores($matches, $draw_id, $list_type);
		
		if (!$draw->manual_draw)
		{
			$seeds = TournamentSignup::with('teams')->whereDrawId($draw_id)
				->whereListType($list_type)
	        	->orderBy('rank')
	        	->take($draw->number_of_seeds)
	        	->get();
		}
		else
		{
			$seeds = DrawSeed::with('teams')
				->whereDrawId($draw_id)
				->whereListType($list_type)
				->orderBy('seed_num', 'asc')
	        	->get();
		}


		$pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
			->where('draw_id', $draw->id)
			->where('list_type', $list_type)
			->orderBy('id')
			->get();

		if (!count($pairs))
			return Redirect::to('signups/final-list/'.$draw_id);

		
		// prepare seed keys - user for getting seed number in front of team name
        $seed_ids = !$draw->manual_draw ? array_flip($seeds->lists('id')) : array_flip($seeds->lists('signup_id')); 

		$jsArray = DrawMatchHelper::getBracketPairs($pairs, $draw, $list_type, $seed_ids);

		//checker to see if draw has a winner
		$winner_exist = (end($jsArray)[0][0]['id']) ? TRUE : FALSE;
        if ($winner_exist AND $list_type == 1)
			PointsCalculator::saveTournamentHistory($jsArray, $draw->level, $tournament->id, $draw->id);
        
		$results_form_view = View::make('draws/add_results'	);

		if ($list_type == 2 || $list_type == 1)
		{
			$lucky_losers = TournamentSignup::whereDrawId($draw_id)
				->whereListType(2)
				->where('move_from_qualifiers_type', '>', 0)
				->get();
		}
        else
			$lucky_losers = NULL;


		$title = ($list_type == 2) ? LangHelper::get('qualifying_draw_bracket', "Qualifying draw bracket") : LangHelper::get('main_draw_bracket', "Main draw bracket");
		$this->layout->title = $title;
		$this->layout->content = View::make('partials/tournament_gracket', array(
			'draw'              => $draw,
			'tournament'        => $tournament,
			'title'             => $title,
			'jsArray'           => json_encode(array_values($jsArray)),
			'results_form_view' => $results_form_view,
			'isDoubles'         => $draw->draw_type == "doubles",
			'seeds'             => $seeds,
			'winner_exist'      => $winner_exist,
			'lucky_losers'      => $lucky_losers,
		));
	}

    public function getTest()
    {
        $players = Player::with('old_ranking')
            ->whereHas('contact', function ($query) {
                $query->where('sex', 'F');
            })
            ->whereRaw('ranking_points NOTNULL')
            ->where('club_id', '>', 0)
            ->orderBy('club_id')
            ->orderBy('ranking_points', 'desc')
            ->take(30)
            ->get(['club_id', 'ranking_points', 'club_ranking', 'ranking'])
            ->toArray();


        echo Debug::vars($players);die;
    }

	public function getCriticalMessage($tournament_id)
	{
		$message = TournamentMessage::whereTournamentId($tournament_id)
			->whereActive(true)
			->first();

		return $message->message;
	}
    
    public function getTest2()
    {
        $ranking_matchpoints = \RankingMatchpoint::where('assigned', false)
        ->selectRaw("DISTINCT ON (ranking_matchpoints.player_id) *")
        ->orderBy('player_id')
        ->get();
//echo Debug::vars($ranking_matchpoints->toArray());die;
        // ovo treba unaprijediti + prebaciti u points calculator
        foreach ($ranking_matchpoints as $ranking_matchpoint) {
            
//            $player = \Player::with('contact')->find($ranking_matchpoint->player_id);
            $player = \Player::with('contact')->where("licence_number", $ranking_matchpoint->licence_number)->first();
echo Debug::vars($player->toArray());
//echo Debug::vars($player2->toArray());die;
            $draw = \TournamentDraw::find($ranking_matchpoint->draw_id);
            $category = \DrawCategory::find($draw->draw_category_id);
            $years = DateTimeHelper::GetDifferenceInTime($player->contact->date_of_birth);
            $years_check = $years >= $category->range_for_points_from && $years <= $category->range_for_points_to;

            
            if ($years_check) {

                $player_matchpoints = \RankingMatchpoint::where('player_id', $ranking_matchpoint->player_id)
                    ->where('assigned', false)
                    ->where('created_at', '>', DateTimeHelper::GetPostgresDateMinusYear())
                    ->get();
                echo Debug::vars($player_matchpoints);die;
                
                $best_tournament_matchpoints = \PlayerHelper::calculateBestTournamentMatchPoints($player_matchpoints, $player->id);
                $player->ranking_points = ($player->ranking_points)
                    ? $player->ranking_points + $best_tournament_matchpoints
                    : 0 + $best_tournament_matchpoints;

                $player->save();
            }
        }
        
        die;
    }

    public function getStatus($ok)
    {
        $key = Input::get("key");
        if($ok && $key!='ksdghkj23509238-632a')
        {
            Artisan::call("checkstatus:do");
        }
    }

}
