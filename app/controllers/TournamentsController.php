<?php

class TournamentsController extends BaseController
{

    public function getIndex()
    {
        if (Auth::getUser()->hasRole('superadmin')) {

            $tournaments = Tournament::with('surface', 'organizer', 'draws', 'date')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->select("*", "tournaments.id as id");

        } elseif (Auth::getUser()->hasRole("regional_admin")) {

            $tournaments = Tournament::join("clubs", function ($join) {
                $join->on("tournaments.club_id", "=", "clubs.id");
            })->join("regions", function ($join) {
                $join->on("regions.id", "=", "clubs.region_id");
            })
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->where("regions.id", Auth::getUser()->getUserRegion())
                ->select("*", "tournaments.id as id");

        } else {
            $tournaments = Tournament::where('created_by', Auth::getUser()->id)
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->select("*", "tournaments.id as id");;
        }

        $year = (Request::query('year')) ? Request::query('year') : 'Y';
        $month = (Request::query('month')) ? Request::query('month') : '';
        $status = (Request::query('status')) ? Request::query('status') : '';
        $title = (Request::query('title')) ? Request::query('title') : '';

        if (Request::query('search')) {

            if (Request::query('month')) {
                // if year is submitted in query set that year if not set current year

                $from_date = date($year . '-' . $month . '-01');
                $to_date = date($year . '-' . $month . '-t');

                $formatted_from = new DateTime($from_date);
                $formatted_to = new DateTime($to_date);

                // $tournaments = $tournaments->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                $tournaments = $tournaments->whereBetween('tournament_dates.main_draw_from', array($formatted_from->format('Y-m-d H:i:s'), $formatted_to->format('Y-m-d H:i:s')));

                if (Auth::user()->hasRole('superadmin'))
                    $tournaments = $tournaments->select("*", "tournaments.id as id");

            }
            if ($status)
                $tournaments = $tournaments->where('tournaments.status', Request::query('status'));

            if ($title)
                $tournaments = $tournaments->where('tournaments.title', 'ILIKE', "%" . Request::query('title') . "%");
        }

        $tournaments = $tournaments->paginate(30);

        $print_info_view = View::make('tournaments/print_info');

        $this->layout->title = LangHelper::get('tournaments', 'Tournaments');
        $this->layout->content = View::make('tournaments/index', array(
            'tournaments'     => $tournaments,
            'print_info_view' => $print_info_view,
            'year'            => $year,
            'month'           => $month,
            'status'          => $status,
        ));
    }


    public function getWeek($param)
    {
        $week_params = [
            1 => 'this week',
            2 => 'next week',
            3 => '+1 week',
            4 => '+2 weeks',
        ];
        $tournaments = TournamentHelper::calculateByWeeks($week_params[$param], 'get');

        $this->layout->title = LangHelper::get('tournaments_in_this_weekr', 'Tournaments in this week');
        $this->layout->content = View::make('tournaments/week', array(
            'tournaments' => $tournaments,
            'param'       => $param,
        ));
    }

    public function getUpcoming()
    {
        $weeks = [
            'Next week'    => TournamentHelper::calculateByWeeks('next week', 'get'),
            'Next 2 weeks' => TournamentHelper::calculateByWeeks('+1 week', 'get'),
            'Next 3 weeks' => TournamentHelper::calculateByWeeks('+2 weeks', 'get'),
        ];

        $this->layout->title = LangHelper::get('upcoming_tournaments', 'Upcoming tournaments');
        $this->layout->content = View::make('tournaments/upcoming', array(
            'weeks' => $weeks,
        ));
    }

    public function getCreate()
    {
        $surfaces_array = array(null => LangHelper::get('choose_surface', 'Choose Surface')) + \TournamentSurface::dropdown();
        $tournament_types = TournamentType::getDropdown(null, true);

        $types = [null => LangHelper::get('choose_type', 'Choose type')] +
            ['singles' => LangHelper::get('singles', 'Singles'),
             'doubles' => LangHelper::get('doubles', 'Doubles'),
            ];
        $genders = [null => LangHelper::get('choose_gender', 'Choose gender')]
            + [
                'M' => LangHelper::get('male', 'Male'),
                'F' => LangHelper::get('female', 'Female'),
            ];

        $clubs_array = ClubHelper::getListOfClubs(true);
        $categories = array(null => LangHelper::get('choose_category', 'Choose category')) + DrawCategory::lists('name', 'id');
        $sponsors_array = [null => LangHelper::get('choose_sponsor', 'Choose Sponsor')] + Sponsor::lists('title', 'id');
        $total_acceptance = TournamentHelper::getTotalAcceptance();
        $referee_array = TournamentHelper::getRefereesDropdown();
        $qualiy_q = TournamentHelper::getHasQualiQuestions();
        $prequali_q = TournamentHelper::getHasPreQualiQuestions();

        $match_rules = TournamentDrawHelper::getMatchRule(true);
        $set_rules = TournamentDrawHelper::getSetRule(true);
        $game_rules = TournamentDrawHelper::setGameRule(true);

        $onsite_direct = array(null => "No", 1 => "Yes");
        $quali_has_final_round = TournamentHelper::getQualiFinalRoundDropdown();


        $step2 = View::make('tournaments/step2', array(
            'categories'            => $categories,
            'types'                 => $types,
            'genders'               => $genders,
            'total_acceptance'      => $total_acceptance,
            "onsite_direct"         => $onsite_direct,
            "match_rules"           => $match_rules,
            "set_rules"             => $set_rules,
            "game_rules"            => $game_rules,
            "quali_has_final_round" => $quali_has_final_round,
            'prequali_q'            => $prequali_q,
        ));

        $this->layout->title = LangHelper::get('add_tournament', 'Add tournament');
        $this->layout->content = View::make('tournaments/create', array(
            'surfaces_array'   => $surfaces_array,
            'qualiy_q' => $qualiy_q,
            'clubs_array'      => $clubs_array,
            'step2'            => $step2,
            'sponsors_array'   => $sponsors_array,
            'referee_array'    => $referee_array,
            "tournament_types" => $tournament_types,
        ));
    }

    public function postCreate()
    {
        $rules = array(
            'tournament.title'          => array('required'),
            'date.main_draw_from'       => array('required'),
            'date.main_draw_to'         => array('required'),
            'organizer.name'            => array('required'),
        );

        if(Input::get("tournament.has_qualifying"))
        {
            $rules += array(
                'date.qualifying_date_from' => array('required'),
                'date.qualifying_date_to'   => array('required')
            );
        }

//        echo Debug::vars(Input::all());die;
        $validator = Validator::make(array_dot(Input::all()), $rules);
        $tournament_data = Input::get('tournament');
        $date_data = Input::get('date');
        $organizer_data = Input::get('organizer');
        $draws_data = Input::get('drawsData');
        if ($validator->passes()) {

            // check if organizer exists in database by email
            if ($organizer = TournamentHelper::checkIfOrganizerExists(Input::get('organizer.email'))) {
                $organizer->update($organizer_data);
            } else
                $organizer = TournamentOrganizer::create($organizer_data);

            //TODO: Remove temp hack for having approved tournament if regional admin
            $status = Auth::getUser()->hasRole('referee')  ? 1 : 2;
            $validated = Auth::getUser()->hasRole('referee')  ? false : true;
//            $status = 2;

            $tournament = Tournament::create($tournament_data + array(
                    'organizer_id' => $organizer->id,
                    'created_by'   => Auth::getUser()->id,
                    'status'       => $status,
                    'validated'     => $validated,
                    'validated_by' => Auth::user()->id,
                    'approved_by' => Auth::user()->id,
                ));

            $referee = Referee::where("id", $tournament_data['referee_id_real'])->first();
            if($referee && $referee->user_id)
                $tournament->referee_id = $referee->user_id;
            else
                $tournament->referee_id = null;



            $tournament->slug = Str::slug($tournament->title) . '-' . $tournament->id;
            $tournament->save();

            $dates = TournamentDate::create($date_data + [
                    'tournament_id' => $tournament->id,
                ]);

            $tournament->addSponsor(Input::get('sponsor.sponsor_id'));

            if($draws_data)
{            foreach ($draws_data as $draw) {
                $draws_data = $draw['draws'];
                $sizes_data = $draw['sizes'];
                $is_prequalifying = $draw['is_prequalifying'];
                $qualifications_data = $draw['qualifications'];

                if (empty($draws_data['number_of_seeds'])) {
                    $number_of_seeds = TournamentHelper::getNumberOfSeeds($sizes_data['total']);
                } else {
                    $number_of_seeds = $draws_data['number_of_seeds'];
                }

                unset($draws_data['number_of_seeds']);

                // $number_of_seeds = TournamentHelper::getNumberOfSeeds($sizes_data['total']);

                $tournament_draw = TournamentDraw::create($draws_data + array(
                        'tournament_id'   => $tournament->id,
                        'number_of_seeds' => $number_of_seeds,
                        'is_prequalifying' => $is_prequalifying,
                    ));

                $draw_size = DrawSize::create($sizes_data + array(
                        'draw_id' => $tournament_draw->id,
                    ));

                $draw_qualification = DrawQualification::create($qualifications_data + array(
                        'draw_id' => $tournament_draw->id,
                    ));
            }}

            if (Auth::getUser()->hasRole('referee')) {

                $region = Tournament::join("clubs", "tournaments.club_id", "=", "clubs.id")->join("regions", "regions.id", "=", "clubs.region_id")
                    ->where("tournaments.id", $tournament->id)
                    ->get();

                $region_id = $region[0]['region_id'];
                $admins = User::join('users_regions', 'users.id', '=', 'users_regions.user_id')->where('users_regions.region_id', $region_id)->get();

                if ($admins) {
                    foreach ($admins as $admin) {
                        $message = LangHelper::get('draw_submitted_for_approval', 'Draw submitted for approval');
                        Notification::TournamentNotif($admin->user_id, $tournament->id, 2, $message);
                    }
                }
//                $message = LangHelper::get('request_for_tournament_approval_received', 'Request for tournament approval received') . ' - ' . $tournament->title;
//                Notification::TournamentNotif(Role::find(1)->users()->get()->lists('id', 'id'), $tournament->id, 2, $message);
            }

            //TODO: REMOVE LATER
//            TournamentHelper::RequestAction($tournament->id, 2);

            return Response::json(array('success' => 'true', 'redirect_to' => URL::to('tournaments/details', $tournament->id)));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getDetails($id)
    {
        $tournament = Tournament::with('organizer', 'date')->find($id);
        $sponsor = $tournament->sponsors()->first();

        $notes_count = TournamentNote::where("tournament_id", $id)->count();
        $referee = User::with('contact')
            ->whereHas('roles', function ($query) {
                $query->where('role_id', 6);
            })->where('id', $tournament->referee_id)
            ->first();

        if (!Auth::getUser()->hasRole("superadmin") && $tournament->club->region_id != Auth::getUser()->getUserRegion() && !Auth::getUser()->hasRole("referee"))
            return PermissionHelper::noPermissionPage($this);

        $draws = $tournament->draws()->orderBy('tournament_draws.id')->get();
        $ask_question_view = View::make('tournaments/ask_question', array(
            'tournament' => $tournament,
        ));

        $this->layout->title = LangHelper::get('tournament_details', 'Tournament details');
        $this->layout->content = View::make('tournaments/details', array(
            'tournament'        => $tournament,
            'draws'             => $draws,
            'ask_question_view' => $ask_question_view,
            'sponsor'           => $sponsor,
            'referee'           => $referee,
            'notes_count'           => $notes_count,
        ));
    }

    public function getEdit($id)
    {
        $tournament = Tournament::find($id);
        if (PermissionHelper::checkPagePermission($tournament))
            return PermissionHelper::noPermissionPage($this);

        $selected_surface = ($tournament->surface)
            ? [$tournament->surface->id => $tournament->surface->name()]
            : [null => LangHelper::get('choose_surface', 'Choose surface')];
        $surfaces_array = $selected_surface + TournamentSurface::dropdown();
        $clubs_array = array($tournament->club->id => $tournament->club->club_name) + ClubHelper::getListOfClubs(true);
        $status_array = $tournament->statusDropdown();
        $qualiy_q = TournamentHelper::getHasQualiQuestions();


        if ($tournament->getSponsor('id'))
            $sponsors_array = [$tournament->getSponsor('id') => $tournament->getSponsor('title')] + Sponsor::lists('title', 'id');
        else
            $sponsors_array = [null => LangHelper::get('choose_sponsor', 'Choose Sponsor')] + Sponsor::lists('title', 'id');

        $referee_name = Referee::refereeName($tournament->referee_id_real);

        $tournament_types = TournamentType::getDropdown($tournament->tournament_type, true);


        $this->layout->title = LangHelper::get('edit_tournament', 'Edit tournament');
        $this->layout->content = View::make('tournaments/edit', array(
            'surfaces_array'   => $surfaces_array,
            'tournament'       => $tournament,
            'clubs_array'      => $clubs_array,
            'sponsors_array'   => $sponsors_array,
            'referee_name'     => $referee_name,
            'tournament_types' => $tournament_types,
            'status_array'     => $status_array,
            'qualiy_q'         => $qualiy_q,
        ));

    }

    public function putEdit($id)
    {
        $tournament = Tournament::find($id);
        $organizer = $tournament->organizer;
        $dates = $tournament->date;

        $rules = array(
            'tournament.title'          => array('required'),
            'date.main_draw_from'       => array('required'),
            'date.main_draw_to'         => array('required'),
            'organizer.name'            => array('required'),
        );

        if(Input::get("tournament.has_qualifying"))
        {
            $rules += array(
                'date.qualifying_date_from' => array('required'),
                'date.qualifying_date_to'   => array('required')
            );
        }

        $validator = Validator::make(Input::all(), $rules);

        $tournament_data = Input::get('tournament');


        $date_data = Input::get('date');

        array_walk_recursive($date_data, function (&$element) {
            $element = DateTimeHelper::ConvertSpanishFormatToPos($element);
        });
//echo Debug::vars($date_data);die;
        $organizer_data = Input::get('organizer');
        if ($validator->passes()) {

            $tournament_data['surface_id'] = (isset($tournament_data['surface_id']) AND !empty($tournament_data['surface_id']))
                ? $tournament_data['surface_id']
                : 0;

            $referee = Referee::where("id", $tournament_data['referee_id_real'])->first();
            if($referee && $referee->user_id)
                $tournament_data['referee_id'] = $referee->user_id;
            else
                $tournament_data['referee_id']= null;


            $tournament_data['has_qualifying'] = $tournament_data['has_qualifying'] == 1;
            if(!$tournament_data['has_qualifying'])
                $date_data['date.qualifying_date_to'] = $date_data['date.qualifying_date_from'] = null;

            $organizer->update($organizer_data);
            $tournament->update($tournament_data);
            $dates->update($date_data);

            $tournament->slug = Str::slug($tournament->title) . '-' . $tournament->id;
            $tournament->save();

            $tournament->addSponsor(Input::get('sponsor.sponsor_id'));

            return Response::json(array(
                'success' => true,
                'redirect_to' => '/tournaments/details/'.$tournament->id,
            ));
        } else {
            return $validator->messages()->toJson();
        }
    }


    public function getDraws($tournament_id)
    {
        $tournament = Tournament::find($tournament_id);

        $draws = $tournament->draws()->orderBy('tournament_draws.id')->get();

        $types = array(null => LangHelper::get('choose_type', 'Choose type')) + [
                'singles' => LangHelper::get('signles', 'Singles'),
                'doubles' => LangHelper::get('doubles', 'Doubles'),
            ];
        $genders = array(null => LangHelper::get('choose_gender', 'Choose gender')) + [
                'M' => LangHelper::get('male', 'Male'),
                'F' => LangHelper::get('female', 'Female'),
            ];

        $match_rules = TournamentDrawHelper::getMatchRule(true);
        $set_rules = TournamentDrawHelper::getSetRule(true);
        $game_rules = TournamentDrawHelper::setGameRule(true);
        $onsite_direct = TournamentHelper::getOnSiteDropdown();
        $quali_has_final_round = TournamentHelper::getQualiFinalRoundDropdown();

        $total_acceptance = TournamentHelper::getTotalAcceptance();
        $total_acceptance_prequali = TournamentHelper::getPreQualyTotalAcceptance();
        $prequali_q = TournamentHelper::getHasPreQualiQuestions();

        $categories = array(null => LangHelper::get('choose_category', 'Choose category')) + DrawCategory::lists('name', 'id');

        $this->layout->title = LangHelper::get('tournament_draws', 'Tournament draws');
        $this->layout->content = View::make('tournaments/draws/index', array(
            'tournament'            => $tournament,
            'draws'                 => $draws,
            'categories'            => $categories,
            'genders'               => $genders,
            'types'                 => $types,
            'total_acceptance'      => $total_acceptance,
            'total_acceptance_prequali'         => $total_acceptance_prequali,
            'match_rules'           => $match_rules,
            'set_rules'             => $set_rules,
            'game_rules'            => $game_rules,
            'onsite_direct'         => $onsite_direct,
            'quali_has_final_round' => $quali_has_final_round,
            'prequali_q' => $prequali_q,
        ));
    }

    public function postCreateDraw($tournament_id)
    {
        $rules = array(
            'draws.draw_category_id' => array('required'),
            'draws.draw_type'        => array('required'),
            'draws.draw_gender'      => array('required'),
            'sizes.total'            => array('required'),
        );

        $tournament = Tournament::find($tournament_id);

        $validator = Validator::make(array_dot(Input::all()), $rules);

        $draws_data = Input::get('draws');
        $sizes_data = Input::get('sizes');
        $is_prequalifying = Input::get('is_prequalifying') ? Input::get('is_prequalifying') : false;
        $qualifications_data = Input::get('qualifications');


        if (empty($draws_data['number_of_seeds'])) {
            $number_of_seeds = TournamentHelper::getNumberOfSeeds($sizes_data['total']);
        } else {
            $number_of_seeds = $draws_data['number_of_seeds'];
        }

//        $sizes_data['prequalifying_size'] = isset($sizes_data['prequalifying_size']) && $sizes_data['prequalifying_size'] ? $sizes_data['prequalifying_size'] : 0;
        unset($draws_data['number_of_seeds']);

        // $number_of_seeds = TournamentHelper::getNumberOfSeeds($sizes_data['total']);

        if ($validator->passes()) {
            $key = $tournament->draws->count() + 1;
            $unique_draw_id = date("Y.m.d") . "." . str_pad($tournament->club->province->region->id, 6, '0', STR_PAD_LEFT) . "." . $tournament->club->id . "." . str_pad($tournament->id, 4, '0', STR_PAD_LEFT) . "." . str_pad($key + 1, 2, '0', STR_PAD_LEFT);


            $draw = TournamentDraw::create($draws_data + array(
                    'tournament_id'   => $tournament_id,
                    'number_of_seeds' => $number_of_seeds,
                    'unique_draw_id'  => $unique_draw_id,
                    'is_prequalifying' => $is_prequalifying
                ));

            $draw_size = DrawSize::create($sizes_data + array(
                    'draw_id' => $draw->id,
                ));

            $draw_qualification = DrawQualification::create($qualifications_data + array(
                    'draw_id' => $draw->id,
                ));

            return Response::json(['success' => 'true', 'redirect_to'=> URL::to("/tournaments/draws/".$tournament_id), 'data' => TournamentDrawHelper::jsonLastRow($tournament_id)]);
        } else {
            return $validator->messages()->toJson();
        }

    }

    public function getEditDraw($draw_id)
    {
        $draw = TournamentDraw::with('category', 'qualification', 'size')
            ->find($draw_id);
        $categories = array($draw->category_id => $draw->category->name) + DrawCategory::lists('name', 'id');
        $types = [
            'singles' => 'Singles',
            'doubles' => 'Doubles',
        ];
        $genders = [
            'M' => 'Male',
            'F' => 'Female',
        ];

        $tournament = Tournament::find($draw->tournament_id);
        $match_rules = TournamentDrawHelper::getMatchRule(true);

        $set_rules = TournamentDrawHelper::getSetRule(true);
        $game_rules = TournamentDrawHelper::setGameRule(true);
        $onsite_direct =($draw->size) ?  TournamentHelper::getOnSiteDropdown($draw->size->onsite_direct) : 0;

        $total_acceptance = TournamentHelper::getTotalAcceptance();
        $total_acceptance_prequali = TournamentHelper::getPreQualyTotalAcceptance();
        $quali_has_final_round = $draw->qualification ? TournamentHelper::getQualiFinalRoundDropdown($draw->qualification->has_final_round): false;
        $prequali_q = TournamentHelper::getHasPreQualiQuestions();

        $this->layout->title = LangHelper::get('tournament_draws_edit_draw', 'Tournament draws - edit draw');
        $this->layout->content = View::make('tournaments/draws/edit', array(
            'draw'                  => $draw,
            'categories'            => $categories,
            'genders'               => $genders,
            'types'                 => $types,
            'total_acceptance'      => $total_acceptance,
            'total_acceptance_prequali' => $total_acceptance_prequali,
            "match_rules"           => $match_rules,
            "set_rules"             => $set_rules,
            "game_rules"            => $game_rules,
            "onsite_direct"         => $onsite_direct,
            "quali_has_final_round" => $quali_has_final_round,
            'tournament'            => $tournament,
            'prequali_q'            => $prequali_q,
        ));
    }

    public function putEditDraw($draw_id)
    {
        $draw = TournamentDraw::with('size')->find($draw_id);
        $draw_size = $draw->size;
        $draw_qualification = $draw->qualification;


        $rules = array(
            'draws.draw_category_id' => array('required'),
            'draws.draw_type'        => array('required'),
            'draws.draw_gender'      => array('required'),
            'sizes.total'            => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);

        $draws_data = Input::get('draws');
        $sizes_data = Input::get('sizes');
        $qualifications_data = Input::get('qualifications');

        if (empty($draws_data['number_of_seeds'])) {
            $number_of_seeds = TournamentHelper::getNumberOfSeeds($sizes_data['total']);
        } else {
            $number_of_seeds = $draws_data['number_of_seeds'];
        }
//echo Debug::vars(Input::all());die;
        unset($draws_data['number_of_seeds']);
        $is_prequalifying = Input::get('is_prequalifying') ? Input::get('is_prequalifying') : false;

//        $sizes_data['prequalifying_size'] = $sizes_data['prequalifying_size'] ?: 0;

        if ($validator->passes()) {
            $draw->update($draws_data + [
                    'number_of_seeds' => $number_of_seeds,
                    'is_prequalifying' => $is_prequalifying,
                ]);
            $draw_size->update($sizes_data);
            $draw_qualification->update($qualifications_data);

            return Response::json(array('success' => 'true'));
        } else {
            return $validator->messages()->toJson();
        }

    }

    public function getDrawDetails($draw_id)
    {
        $draw = TournamentDraw::with('size', 'qualification')->find($draw_id);

        $this->layout->title = LangHelper::get('tournament_draws_draw_details', 'Tournament draws - draw details');
        $this->layout->content = View::make('tournaments/draws/details', array(
            'draw' => $draw,
        ));
    }

    public function getSurfaces()
    {
        $surfaces = TournamentSurface::all();

        return $surfaces;
    }

    public function getDrawsData($tournament_id)
    {
        $draws = TournamentDrawHelper::jsonData($tournament_id);
        return Response::json($draws)->setCallback(\Request::get('callback'));
    }

    public function getDrawCategories()
    {
        $cats = DrawCategory::lists('name', 'id');

        return Response::json([
            'categories' => $cats,
        ]);
    }

    public function postRequestAction($tournament_id)
    {
        $status = Request::query('status');

        TournamentHelper::RequestAction($tournament_id, $status);
        return Response::json(['success' => 'true']);
    }

    public function postValidate($tournament_id)
    {
        $tournament = Tournament::find($tournament_id);

        $tournament->validated = true;
        $tournament->validated_by = Auth::user()->id;
        $tournament->update();

        return Response::json(['success' => 'true']);

    }

    public function getPrintInfo($tournament_id)
    {
        $tournament = Tournament::find($tournament_id);

        $data = [
            'title' => $tournament->title,
            'url'   => URL::route('default_public', $tournament->slug),
            'image' => $tournament->generateQr(),
        ];

        $this->layout = null;

        return Response::json(['data' => $data]);

    }

    public function getOrganizersData()
    {
        $query = Request::query('query');

        $organizers = TournamentOrganizer::where('name', 'ILIKE', "%" . $query . "%")
            ->orderBy('name', 'asc')
            ->take(10)
            ->get();


        $organizers_array = [];

        foreach ($organizers as $organizer) {
            $item['full_name'] = $organizer->name . ' - ' . $organizer->email;
            $item['name'] = $organizer->name;
            $item['email'] = $organizer->email;
            $item['phone'] = $organizer->phone;
            $item['fax'] = $organizer->fax;
            $item['website'] = $organizer->website;

            $organizers_array[] = $item;
        }

        return Response::json($organizers_array);
    }

    public function getNotes($tournament_id)
    {
        $tournament = Tournament::with('club')->find($tournament_id);

        $this->layout->title = LangHelper::get('tournament_notes', 'Tournament notes');
        $this->layout->content = View::make('tournaments/notes/index', array(
            'tournament' => $tournament,
        ));
    }

    public function postNotes($tournament_id)
    {
        $rules = array(
            'text' => array('required'),
        );

        $validator = Validator::make(Input::all(), $rules);


        if ($validator->passes()) {
            $note = TournamentNote::create([
                'text'          => Input::get('text'),
                'user_id'       => Auth::user()->id,
                'tournament_id' => $tournament_id,
            ]);

            $responseData = [
                'current_user' => Auth::user()->id,
                'user_name'    => $note->user->username,
                'note_user'    => $note->user_id,
                'id'           => $note->id,
                'text'         => $note->text,
                'created'      => $note->created_at,
            ];

            Notification::sendNoteNotification($tournament_id);

            return Response::json(array('success' => 'true', 'data' => $responseData));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getNotesData($tournament_id)
    {
        $notes = TournamentNote::with('user')
            ->whereTournamentId($tournament_id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = [];
        foreach ($notes as $note) {
            $item['current_user'] = Auth::user()->id;
            $item['user_name'] = $note->user->username;
            $item['note_user'] = $note->user_id;
            $item['id'] = $note->id;
            $item['text'] = $note->text;
            $item['created'] = $note->created_at;

            $data[] = $item;
        }

        return Response::json([
            'notes' => $data,
        ]);
    }

    public function deleteNote($note_id)
    {
        $note = TournamentNote::find($note_id);

        $note->delete();

        return Response::json(['success' => true]);
    }


    public function getTournamentInvoice($tournament_id)
    {
        $tournament = Tournament::with('club', 'draws', 'draws.signups', 'draws.signups.teams')->find($tournament_id);

        if ($tournament->status != 5)
            return Redirect::to('/tournaments/details/' . $tournament_id);

        $invoice_id = Invoice::createTournamentInvoice($tournament);


        return Redirect::to('/invoices/pdf/'.$invoice_id.'?his_ignore=true');

    }

    public function getPerClubInvoice($tournament_id)
    {
        $tournament = Tournament::with('club', 'draws', 'draws.signups', 'draws.signups.teams', 'draws.signups.teams.player')->find($tournament_id);

        if ($tournament->status != 5)
            return Redirect::to('/tournaments/details/' . $tournament_id);

        $invoice_id = Invoice::createPerClubInvoice($tournament);


        return Redirect::to('/invoices');

    }

    public function getTournamentTypes()
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $types = TournamentType::orderBy('id')
            ->get();

        $this->layout->title = LangHelper::get('tournament_types', 'Tournament types');
        $this->layout->content = View::make('tournaments/types/index', array(
            'types' => $types,
        ));
    }

    public function getEditType($type_id)
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $type = TournamentType::find($type_id);

        $this->layout->title = LangHelper::get('tournament_types_edit', 'Tournament types edit');
        $this->layout->content = View::make('tournaments/types/edit', array(
            'type' => $type,
        ));
    }

    public function putEditType($type_id)
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $rules = array(
            'type.type'                => array('required'),
            'type.category_tournament' => array('required'),
            'type.coeficient'          => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);

        if ($validator->passes()) {
            $type = TournamentType::find($type_id);
            $type->type = Input::get('type.type');
            $type->category_tournament = Input::get('type.category_tournament');
            $type->coeficient = Input::get('type.coeficient');
            $type->save();

            return Response::json([
                'success'     => true,
                'redirect_to' => '/tournaments/tournament-types',
                'message'     => LangHelper::get('tournament_type_updated', 'Tournament type updated'),
            ]);
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getDeleteDraw($draw_id) {

        $tournament_id = TournamentDraw::where('id', $draw_id)->first();

        DB::table('tournament_signups')->where('draw_id', $draw_id)->delete();
        DB::table('match_schedules')->where('draw_id', $draw_id)->delete();
        DB::table('match_violations')->where('draw_id', $draw_id)->delete();
        DB::table('draw_qualifications')->where('draw_id', $draw_id)->delete();
        DB::table('draw_sizes')->where('draw_id', $draw_id)->delete();
        DB::table('draw_matches')->where('draw_id', $draw_id)->delete();
        DB::table('draw_seeds')->where('draw_id', $draw_id)->delete();
        DB::table('draws_referees')->where('draw_id', $draw_id)->delete();
        DB::table('draws_referees_history')->where('draw_id', $draw_id)->delete();
        DB::table('draw_history')->where('draw_id', $draw_id)->delete();
        DB::table('tournament_histories')->where('draw_id', $draw_id)->delete();
        DB::table('tournament_draws')->where('id', $draw_id)->delete();

        return Redirect::to('/tournaments/draws/'.$tournament_id->tournament_id);
    }

    public static function getValidateAndApprove ($tournament_id) {

        $tournament = Tournament::find($tournament_id);
        $tournament->status = 2;
        $tournament->validated = true;
        $tournament->save();

        $superadmins = User::join('users_roles', 'users.id', '=', 'users_roles.user_id')->where('users_roles.role_id', 1)->get();

        if ($superadmins) {
            foreach ($superadmins as $sadmin) {
                $message = LangHelper::get('draw_submitted_for_approval', 'Draw validated and approved by regional admin');
                Notification::TournamentNotif($sadmin->user_id, $tournament->id, 2, $message);
            }
        }

        return Redirect::to('/tournaments/details/'.$tournament_id);
    }
}
