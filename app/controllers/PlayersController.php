<?PHP

class PlayersController extends BaseController
{

    public function getIndex($tournament_id = '')
    {
        $items_per_page = array(
            10 => 10,
            20 => 20,
            30 => 30,
            50 => 50,
            100 => 100,
        );

        $regions = [null => LangHelper::get('all_federations', 'All federations')]
            + Region::orderBy('region_name')->get()->lists('region_name', 'id');

        $fined_array = [
            null => LangHelper::get('all_players', 'All players'),
            1 => LangHelper::get('fined_players', 'Fined players'),
        ];

        $this->layout->title = LangHelper::get('players', "Players");
        $this->layout->content = View::make('players/index', array(
            'items_per_page' => $items_per_page,
            'tournament_id' => $tournament_id,
            'regions' => $regions,
            'fined_array' => $fined_array,
        ));
    }

    //get players only when referee and for the single tournaments
    public function getReferee($tournament_id)
    {

    }

    public function getNotApproved()
    {
        if (Auth::getUser()->hasRole('superadmin')) {
            $players = Player::where('status', '!=', 2)
                ->where('created_by', '!=', 0)
                ->get();
        } else {
            $players = Player::where('status', '!=', 2)
                ->where('created_by', Auth::getUser()->id)
                ->get();
        }

        $this->layout->title = LangHelper::get('not_approved_players', "Not Approved players");
        $this->layout->content = View::make('players/not_approved', array(
            'players' => $players,
        ));
    }

    public function getInfo($id)
    {
        $player = Player::with('contact', 'notes')->find($id);

        $sign = ['', ''];
        if ($player->ranking_move > 0)
            $sign = ['+', 'positive'];
        elseif ($player->ranking_move < 0)
            $sign = ['-', 'negative'];

        $history = RankingMatchpoint::where('licence_number', $player->licence_number)
            ->lists('tournament_id');

        $tournaments = null;
        if ($history) {
            $tournaments = Tournament::with('club', 'surface')
                ->whereIn('id', $history)
                ->get();
        }

        $old_tournaments = abs(count($history) - count($tournaments));
        $matchpoints = RankingMatchpoint::where('licence_number', $player->licence_number)->get();

        $this->layout->title = LangHelper::get('players', 'Player') . " - " . $player->contact->full_name();
        $this->layout->content = View::make('players/info', array(
            'player' => $player,
            'tournaments' => $tournaments,
            'sign' => $sign,
            'matchpoints' => $matchpoints,
            'old_tournaments' => $old_tournaments
        ));

    }

    public function getCreate()
    {
        $clubs = ClubHelper::getListOfClubs(true);
//        $licence_range = Auth::user()->hasRole('regional_admin')
//            ? LicenceRange::getLicenceRanges(Auth::user()->getUserRegion())->rangeToString()
//            : LangHelper::get('licence_number', 'Licence number');

        $team_id = Request::input('team');
        $genders = array(null => LangHelper::get('choose_gender', 'Choose gender')) + ['M' => 'Male', 'F' => 'Female'];

        $this->layout->title = LangHelper::get('submit_new_player', "Submit new player");
        $this->layout->content = View::make('players/create', array(
            'clubs' => $clubs,
            'genders' => $genders,
            'team_id' => $team_id,
//            'licence_range' => $licence_range,
        ));
    }

    public function getPdata($team_id)
    {
        $team = TournamentTeam::find($team_id);
        $data = [];
        if ($team) {
            $data = [
                'name' => $team->name,
                'surname' => $team->surname,
                'date_of_birth' => DateTimeHelper::GetShortDateFullYear($team->date_of_birth),
                'email' => $team->email,
                'phone' => $team->phone,
            ];
        }

        return Response::json(['data' => $data]);
    }

    public function postCreate()
    {
        $player = new Player;
        $rules = array(
            'contact.name' => array('required'),
            'contact.surname' => array('required'),
            'contact.date_of_birth' => array('required'),
            'data.licence_number' => array('required'),
            'contact.sex' => array('required'),
            'data.club_id' => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);
        if ($validation = ValidationHelper::checkIfLicenceExist(Input::get('data.licence_number'))) {
            return Response::json([
                'success' => false,
                'message' => $validation,
                'show_message' => true,
            ]);
        } else if ($validator->passes()) {

            if (ValidationHelper::checkIfPlayerExist(Input::get('contact'))) {
                return Response::json([
                    'show_message' => true,
                    'message' => LangHelper::get('player_is_already_in_database', 'Player is already in database'),
                ]);
            }
            $ex_player = Player::where("licence_number", Input::get('data.licence_number'))->first();
            if ($ex_player)
                return array('success' => false, 'show_message' => true, 'message' => LangHelper::get("licence_number_already_exists", "Licence number already exists"));

            $club_id = Input::get('data.club_id');
            $date_of_birth = DateTimeHelper::GetContactsDate(Input::get('contact.date_of_birth'));
            $contact = Contact::create(Input::get('contact'));
            $contact->date_of_birth = $date_of_birth;
            $contact->dob = DateTimeHelper::GetPostgresDateTime($date_of_birth);
            $contact->save();
            $club = Club::find($club_id);

            $player = Player::create([
                'created_by' => Auth::getUser()->id,
                'status' => 4, // stratus 4 == in progress
                'contact_id' => $contact->id,
                'licence_number' => Input::get('data.licence_number'),
                'ape_id' => Input::get('data.ape_id'),
                'club_id' => $club_id,
                'federation_club_id' => $club->region->id,
                'province_club_id' => $club->province_id,
            ]);

            if (!Auth::getUser()->hasRole('superadmin'))
                Notification::PlayerNotif(Role::find(1)->users()->get()->lists('id', 'id'), $player->id, 5, $message = 'Request for player approval received');

            return Response::json(array('success' => 'true', 'redirect_to' => URL::to('players')));
        } else {
            return $validator->messages()->toJson();
        }

    }

    public function getEdit($id)
    {

        if (Auth::getUser()->hasRole("referee"))
            return PermissionHelper::noPermissionPage($this);

        $player = Player::with('Contact', 'Club')->find($id);
        $clubs = [];
        if (Auth::getUser()->hasRole('superadmin')) {
            if (count($player->clubs))
                $predefined_clubs = array($player->club_id => $player->club->club_name);
            else
                $predefined_clubs = [null => LangHelper::get('choose_club', 'Choose club')];

            $clubs = $predefined_clubs + ClubHelper::getListOfClubs();

        } else {
            if (count($player->clubs))
                $clubs = array($player->club_id => $player->club->club_name);

        }

        $clubs = array_filter($clubs);

        $genders = array($player->contact->sex => $player->contact->gender($player->contact->sex)) + ['M' => 'Male', 'F' => 'Female',];


        $this->layout->title = LangHelper::get('edit_player', 'Edit player') . " - " . $player->contact->full_name();
        $this->layout->content = View::make('players/edit', array(
            'player' => $player,
            'clubs' => $clubs,
            'genders' => $genders,
            'id' => $id
        ));
    }

    public function getDelete($id)
    {
        if (Auth::getUser()->hasRole('superadmin')) {
            $player = Player::find($id);
//            echo Debug::vars($player->toArray());die;
            if($player->status ==1 || $player->status == 4)
                $player->delete();
            return Redirect::to(URL::to("/players"));
        }
    }

    public function putEdit($id)
    {
        $player = Player::find($id);

        $rules = array(
            'contact.name' => array('required'),
            'contact.surname' => array('required'),
            'data.licence_number' => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);

        if ($validator->passes()) {
            $data = Input::get('data');
            $data['ranking'] = $data['ranking'] ?: null;
            $player->update($data);
            $contact = Input::get('contact');
            $player->contact->update($contact);
            $player->contact->date_of_birth = DateTimeHelper::GetContactsDate(Input::get('contact.date_of_birth'));
            $player->contact->save();

            return Response::json(array('success' => 'true', 'redirect_to' => URL::to('players/details/' . $player->id)));
        } else {
            return $validator->messages()->toJson();
        }
    }

    //Get players for the tournaments when referee access
    public function getData($tournament_id = '')
    {
        $licence_numbers = []; // used for referee assigned players

        //$tournament_id is used for players that are signed for that tournament
        if ($tournament_id) {
            $player_ids = TournamentSignup::with('teams')->whereTournamentId($tournament_id)
                ->orderBy('rank')->get();
            $licence_numbers = [];
            foreach ($player_ids as $p) {
                $licence_numbers[] = $p->teams[0]['licence_number'];
            }
        } else if (Auth::user()->hasRole('referee')) {
            $licence_numbers = PlayerHelper::getRefereeAssignedPlayers();
        }

        $items = Request::query('items_counter');
        $order = Request::query('order') ? Request::query('order') : "contacts.id";
        $sort = Request::query('sort');

        $query = Request::query();

        if (Request::query('search') == 1) {
            $posts = Request::query();

            $query = Contact::join('players', 'players.contact_id', '=', 'contacts.id')
                ->leftJoin('penalty_notes', 'penalty_notes.player_id', '=', 'players.id')
                ->select("*", "players.id as id");

            foreach ($posts as $key => $post) {
                $key = str_replace("INT_", "", $key, $int);

                if ($post AND !in_array($key, array('items_counter', 'order', 'sort', 'search', 'callback', 'page'))) {
                    if ($int)
                        $query->where($key, '=', $post);
                    elseif ($key == 'licence_number')
                        $query->whereRaw(" CAST(players.licence_number AS TEXT) LIKE '" . $post . "%' ");
                    else
                        $query->where($key, 'ILIKE', $post . "%");
                }
            }

            if (count($licence_numbers))
                $query->whereIn('licence_number', $licence_numbers);

            $query->orderBy($order, $sort);

            if ($tournament_id)
                $query = $query->whereIn("players.licence_number", $licence_numbers);

            if (Request::query('federation_club_id'))
                $query = $query->where('federation_club_id', Request::query('federation_club_id'));

            if (Request::query('fined'))
                $query = $query->where('penalized', true);

            $players = $query->paginate($items);

            return Response::json(
                $players->toArray() + [
                    'user_role' => Auth::getUser()->getRole('id'),
                    'user_region_id' => Auth::getUser()->getUserRegion()
                ])->setCallback(Request::get('callback'));
        }

        $players = Contact::join('players', 'players.contact_id', '=', 'contacts.id')
            ->leftJoin('penalty_notes', 'penalty_notes.player_id', '=', 'players.id')
            ->select("*", "players.id as id");

        if ($tournament_id)
            $players->whereIn("players.licence_number", $licence_numbers);

        if (Request::query('federation_club_id'))
            $players = $players->where('federation_club_id', Request::query('federation_club_id'));

        if (Request::query('fined'))
            $players = $players->where('penalized', true);

        if (count($licence_numbers))
            $players = $players->whereIn('licence_number', $licence_numbers);

        $players = $players->orderBy($order, $sort)
            ->paginate($items);

        return Response::json(
            $players->toArray() + [
                'user_role' => Auth::getUser()->getRole('id'),
                'user_region_id' => Auth::getUser()->getUserRegion()
            ])->setCallback(\Request::get('callback'));
    }

    public function getDetails($id)
    {
        $player = Player::with('Contact')->find($id);

        if (is_null($player))
            return Redirect::to('/players');

        if ($player->status == 2)
            return Redirect::to('/players/info/' . $player->id);

        if (!Auth::getUser()->hasRole('superadmin'))
            return Redirect::to('/players/info/' . $player->id);

        $ask_question_view = View::make('players/ask_question', array(
            'player' => $player,
        ));

        $mutua_number = Player::newMutuaNumber();
        $licence_number_view = View::make('players/licence_number_add', array(
            'player' => $player,
            'mutua_number' => $mutua_number,
        ));

        $this->layout->title = LangHelper::get('', 'Player') . " - " . $player->contact->full_name();
        $this->layout->content = View::make('players/details', array(
            'player' => $player,
            'ask_question_view' => $ask_question_view,
            'licence_number_view' => $licence_number_view,
        ));
    }


    public function getApprove($player_id)
    {
        $player = Player::with('contact')->find($player_id);

        if ($player->updateStatus()) {

            if (Request::query('invoice'))
                Invoice::createMutuaInvoice($player);

            $message = 'Player ' . $player->contact->full_name() . ' is approved';
            Notification::PlayerNotif($player->created_by, $player->id, 6, $message);

            return Response::json([
                'success' => true,
                'redirect_to' => '/players/info/' . $player->id,
                'message' => LangHelper::get('player_is_approved', 'Player is approved'),
            ]);
        }

        return Response::json([
            'success' => false,
            'redirect_to' => '/players/info/' . $player->id,
        ]);
    }

    public function postApprove($player_id)
    {
        $player = Player::find($player_id);
        $licence_number = Input::get('licence_number');

        if ($licence_number) {
            $player->licence_number = $licence_number;
            $player->status = 2;
            $player->approved_by = Auth::getUser()->id;
            $player->date_of_approval = DateTimeHelper::GetDateNow();
            $player->save();
            $message = 'Player ' . $player->contact->full_name() . ' is approved';
            Notification::PlayerNotif($player->created_by, $player->id, 6, $message);

            return Response::json(['success' => 'true']);
        }

        return Response::json(['success' => 'false', 'redirect_to' => '/players/info/' . $player->id]);

    }


    public function postRequestAction($player_id)
    {
        $player = Player::find($player_id);
        $status = Request::query('status');

        switch ($status) {
            case 1:
                $player->status = 1;
                $player->save();

                if (!Auth::getUser()->hasRole('superadmin')) {
                    $message = 'Request for player approval received';
                    $send_to = Role::find(1)->users()->get()->lists('id', 'id');
                    Notification::PlayerNotif($send_to, $player->id, 5, $message);

                }
                break;

            case 2:
                $player->status = 2;
                $player->date_of_approval = DateTimeHelper::GetDateNow();
                $player->save();
                $message = 'Player ' . $player->contact->full_name() . ' is approved';
                Notification::PlayerNotif($player->created_by, $player->id, 6, $message);
                break;

            case 3:
                $player->status = 3;
                $player->date_of_approval = DateTimeHelper::GetDateNow();

                $message = 'Player ' . $player->contact->full_name() . ' is rejected';
                Notification::PlayerNotif($player->created_by, $player->id, 7, $message);
                $player->delete();

                break;
        }

        return Response::json(['success' => 'true']);
    }


    public function getUpload($player_id = '')
    {
        if ($player_id == '')
            return Redirect::to('players');

        $user = Auth::user();
        $player = Player::with('contact')->find($player_id);

        $this->layout->title = 'Upload image';
        $this->layout->content = View::make('players/upload', array(
            'user' => $user,
            'player' => $player,
        ));
    }


    public function postUpload($id)
    {
        $player = Player::with('contact')->find($id);

        $rules = array(
            'image' => 'required',
        );

        $image = Input::file('image');
        if ($image == '') {
            $image = Input::file("file");
            $rules = array('file' => 'required');
        }

        if ($image)
            $filename = Str::random() . '_' . $image->getClientOriginalName();

        $save_path = public_path() . '/uploads/images';

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            if ($image) {
                Image::make($image)->save($save_path . '/user/full/' . $filename);
                Image::make($image)->resize(210, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(210, 280)->save($save_path . '/user/thumbnails/' . $filename);
                Image::make($image)->resize(75, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(75, 100)->save($save_path . '/user/small_thumbnails/' . $filename);
            }

            $player->contact->image_link = $filename;

            $player->contact->save();

            return Redirect::to('players/upload/' . $player->id);
        } else {
            return Redirect::to('players/upload/' . $player->id);
            // return $validator->messages()->toJson();
        }
    }

    public function getNotes($player_id)
    {
        $player = Player::with('contact')->find($player_id);


        $this->layout->title = LangHelper::get('player_notes', 'Player notes');
        $this->layout->content = View::make('players/notes/index', array(
            'player' => $player,
        ));
    }

    public function postNotes($player_id)
    {
        $rules = array(
            'text' => array('required'),
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $note = PlayerNote::create([
                'text' => Input::get('text'),
                'user_id' => Auth::user()->id,
                'player_id' => $player_id,
            ]);

            $responseData = [
                'current_user' => Auth::user()->id,
                'user_name' => $note->user->username,
                'note_user' => $note->user_id,
                'id' => $note->id,
                'text' => $note->text,
                'created' => $note->created_at,
            ];

            return Response::json(array('success' => 'true', 'data' => $responseData));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getNotesData($player_id)
    {
        $player = Player::find($player_id);

        $notes = PlayerNote::with('user')
            ->wherePlayerId($player_id)
            ->orderBy('created_at', 'desc')
            ->get();

        $data = [];
        foreach ($notes as $note) {
            $item['current_user'] = Auth::user()->id;
            $item['user_name'] = $note->user->username;
            $item['note_user'] = $note->user_id;
            $item['id'] = $note->id;
            $item['text'] = $note->text;
            $item['created'] = $note->created_at;

            $data[] = $item;
        }

        $player_data = [
            'id' => $player->id,
        ];

        return Response::json([
            'notes' => $data,
            'player' => $player_data,
        ]);
    }

    public function deleteNote($note_id)
    {
        $note = PlayerNote::find($note_id);

        $note->delete();

        return Response::json(['success' => true]);
    }

    public function getPenaltyNotes($player_id)
    {
        $player = Player::with('contact')->find($player_id);
        $tournaments = TournamentTeam::with("signup")->where("licence_number", $player->licence_number)->get();
        $tournament_ids_arr = [];
        foreach ($tournaments as $tournament) {
            $tournament_ids_arr[] = $tournament->signup->tournament_id;
        }

        $tournaments = Tournament::tournamentListDropdownFilter(array_flatten($tournament_ids_arr));

        $this->layout->title = LangHelper::get('code_of_conduct', 'Code of conduct');
        $this->layout->content = View::make('players/code_of_conduct/index', array(
            'player' => $player,
            'tournaments' => $tournaments,
        ));
    }

    public function postPenaltyNotes($player_id)
    {
        $rules = array(
            'subject' => array('required'),
            'text' => array('required'),
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $note = PenaltyNote::create([
                'tournament_id' => Input::get('tournament_id'),
                'subject' => Input::get('subject'),
                'text' => Input::get('text'),
                'penalized' => (Input::get('penalized')) ? true : false,
                'user_id' => Auth::user()->id,
                'player_id' => $player_id,
            ]);

            $responseData = [
                'current_user' => Auth::user()->id,
                'user_name' => $note->user->username,
                'note_user' => $note->user_id,
                'id' => $note->id,
                'text' => $note->text,
                'subject' => $note->subject,
                'tournament_id' => $note->tournament_id,
                'penalized' => $note->penalized,
                'created' => $note->created_at,
            ];

            return Response::json(array('success' => 'true', 'data' => $responseData));
        } else {
            return $validator->messages()->toJson();
        }
    }


    public function getPenaltyNotesData($player_id)
    {
        $player = Player::find($player_id);
        $notes = PenaltyNote::getNotes($player_id);

        $data = [];
        foreach ($notes as $note) {
            $item['current_user'] = Auth::user()->id;
            $item['user_name'] = $note->user->username;
            $item['note_user'] = $note->user_id;
            $item['id'] = $note->id;
            $item['subject'] = $note->subject;
            $item['tournament'] = (!is_null($note->tournament)) ? $note->tournament->title : false;
            $item['text'] = $note->text;
            $item['penalized'] = $note->penalized;
            $item['created'] = $note->created_at;

            $data[] = $item;
        }

        $player_data = [
            'id' => $player->id
        ];

        return Response::json([
            'notes' => $data,
            'player' => $player_data,
        ]);
    }

    public function deletePenaltyNote($note_id)
    {
        $note = PenaltyNote::find($note_id);
        $note->delete();

        return Response::json(['success' => true]);
    }


    public function getSearch()
    {
        $this->layout->title = LangHelper::get('player_search', 'Player search');
        $this->layout->content = View::make('players/search', array());
    }

    public function postSearch()
    {
        $players = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id');

        if (Input::get('sex') != -1)
            $players = $players->where('sex', '=', Input::get('sex'));

        if (Input::get('surname')) {
            $players = $players->where('surname', 'ILIKE', "%" . Input::get('surname'));
        } elseif (Input::get('licence_number')) {
            $players = $players->where('licence_number', '=', Input::get('licence_number'));
        } elseif (Input::get('ranking_from')) {
            if (is_numeric(Input::get('ranking_from')) AND is_numeric(Input::get('ranking_to')))
                $players = $players->whereBetween('ranking', [Input::get('ranking_from'), Input::get('ranking_to')]);
        } elseif (Input::get('points_from')) {
            if (is_numeric(Input::get('points_from')) AND is_numeric(Input::get('points_to')))
                $players = $players->whereBetween('ranking_points', [Input::get('points_from'), Input::get('points_to')]);
        } elseif (Input::get('birth_from')) {
            $players = $players->whereBetween('dob', [Input::get('birth_from'), Input::get('birth_to')]);
//                $players = $players->whereRaw(
//                    "substr(date_of_birth, length(date_of_birth)-3, 4) BETWEEN '" . Input::get('birth_from') . "' AND '" . Input::get('birth_to') . "'"
//                );
        }

        $players = $players->take(60)->get()->toArray();

//        $players = $players->get()->toArray();

        return Response::json([
            'success' => 'true',
            'players' => $players,
        ]);
    }


    public function getCreateAccount($player_id)
    {
        $player = Player::with('contact')->find($player_id);

        if ($player && $player->user_id)
            return Redirect::to('/players/info/' . $player->id);

        if (is_null($player))
            return Redirect::to('/players');

        $this->layout->title = LangHelper::get('create_player_account', 'Create player account');
        $this->layout->content = View::make('players/create_account', array(
            'player' => $player,
        ));
    }

    public function getMatchScore($player_id, $match_id)
    {
        $player = Player::with('contact')->find($player_id);

        $history = TournamentHistory::wherePlayerId($player_id)
            ->first();

        if (!$history)
            return Redirect::to('/player/info/' . $player_id);

        $last_match = DrawMatch::with('game_scores', 'game_scores.details', 'draw', 'draw.tournament', 'team1', 'team2')
            ->whereId($match_id)
            ->where('team1_id', $history->team_id)
            ->orWhere('team2_id', $history->team_id)
            ->orderBy('id', 'desc')
            ->first();

        $chart_data = Chart::scoresLine($last_match, 1, $history->team_id);


        $this->layout->title = LangHelper::get('last_match_score', 'Match score');
        $this->layout->content = View::make('players/match_scores', array(
            'last_match' => $last_match,
            'player' => $player,
            'chart_data' => json_encode($chart_data),
        ));
    }

    public function getTournamentInfo($player_id, $tournament_id)
    {
        $player = Player::with('contact')->find($player_id);

        $tournament = Tournament::with('surface', 'club', 'draws')->find($tournament_id);
        $match_ids = RankingMatchpoint::where("licence_number", $player->licence_number)
            ->whereTournamentId($tournament_id)
            ->lists('match_id');

        $matches = DrawMatch::with('scores', 'game_scores', 'matchpoints')
            ->whereIn('id', $match_ids)
            ->orderBy('round_number')
            ->get();

        $this->layout->title = LangHelper::get('tournament_info', 'Tournament info');
        $this->layout->content = View::make('players/tournament_info', array(
            'player' => $player,
            'tournament' => $tournament,
            'matches' => $matches,
        ));
    }
}
