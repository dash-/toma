<?php

class AuthController extends BaseController {

	public function getSignup()
	{
		$this->layout->title = LangHelper::get('signup', 'Signup');
		$this->layout->content = View::make('auth/signup');
	}

	public function postSignup()
	{
		$rules =  array(
			'email'    => array('required', 'email', 'unique:users'),
			'password' => array('required')
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
        	dd($validator->messages()->toJson());
        	return $validator->messages()->toJson();
        }
        else
        {
        	$user = new User;
	        $user->email = strtolower(Input::get('email'));
	        $user->password = Hash::make(Input::get('password'));
		    $user->save();

		    return Request::json(array('success' => 'true'));
        }

	}

	public function getLogin()
	{
		$this->layout->title   = LangHelper::get('login', "Login");
		$this->layout->content =  View::make('auth/login');
	}

	public function postLogin()
	{
		$data = array(
			'email'    => Input::get('email'),
			'password' => Input::get('password'),
		);

		$data2 = array(
			'username' => strtolower(Input::get('email')),
			'password' => Input::get('password'),
		);


		$remember_post = Input::get('remember_me');
		$remember_me = ($remember_post != NULL AND $remember_post != "false") ? TRUE : FALSE;

		if (Auth::attempt($data, $remember_me) OR Auth::attempt($data2, $remember_me)) {
			$language_code = (Auth::getUser()->language) ? Auth::getUser()->language->code : "bs";
			Session::put('my.locale', $language_code);

			return Response::json(array(
                'success' => 'ok',
                'redirect_to' => '/',
            ));
		}
		else
			return Response::json(array('success' => false));

	}

	public function getPasswordReset()
	{
		$this->layout->title   = LangHelper::get('reset_password', "Reset password");
		$this->layout->content =  View::make('auth/password_reset');
	}

	public function postPasswordReset()
	{
		$user = User::whereEmail(Input::get('email'))->first();

		$email = new EmailClass();
			$email->to = $user->email;
			$email->name =$user->name;
			$email->subject = LangHelper::get('request_for_password_reset', 'Request for password reset');

		$data = ['name' => $user->name, 'url' => '/auth/change-password?token='.$user->secret];

		if ($user)
		{
			EmailHelper::sendEmail('emails.auth.reminder',$email, $data);
			return Response::json(array(
				'success' => 'ok',
				'message' => LangHelper::get('password_reset_link_was_sent_to_your_email', 'Password reset link was sent to your email.'),
			));
		}
		else

			return Response::json(array('success' => false));

	}

	public function getChangePassword()
	{
		$user = User::whereSecret(Request::query('token'))->first();

		if ($user)
		{
			$this->layout->title   = LangHelper::get('change_password', "Change password");
			$this->layout->content =  View::make('auth/change_password');
		}
		else
		{
			return Redirect::to('/');
		}
	}

	public function postChangePassword()
	{
		$rules = array(
	        'password' => 'required|confirmed'
	    );

	    $user = User::whereSecret(Input::get('queryToken'))->first();

	    $validator = Validator::make(Input::all(), $rules);

	    if ($user)
	    {
		    if ($validator->passes())
		    {
	            $user->password = Hash::make(Input::get('password'));
	            $user->secret = Str::random($length = 16);
	            $user->save();
	            return Response::json(array('success' => 'true', 'redirect_to' => URL::to('/')));
		    }
		    else
		    {
		    	return $validator->messages()->toJson();
		    }
	    }
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('login');
	}

}
