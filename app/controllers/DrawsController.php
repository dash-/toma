<?php

class DrawsController extends BaseController
{
    public function getDbBracket($draw_id)
    {
        $list_type = (Request::query('list_type')) ? Request::query('list_type') : 2;

        $draw = TournamentDraw::with('tournament', 'tournament.club', 'tournament.sponsors')->find($draw_id);
        $tournament = $draw->tournament;
        $referee_name = ($tournament->referee() && $tournament->referee()->contact[0])
            ? $tournament->referee()->contact[0]->full_name()
            : "";

        $matches = DrawMatch::where(function ($query) {
            $query->where('team1_id', 0)
                ->orWhere('team2_id', 0);
        })->where('draw_id', $draw_id)
            ->whereRoundNumber(1)
            ->whereListType($list_type)
            ->orderBy('id')
            ->get();

        if (count($matches))
            DrawMatchHelper::setUpInitialScores($matches, $draw_id, $list_type);

        $seeds = DrawSeed::with('teams')
            ->whereDrawId($draw_id)
            ->whereListType($list_type)
            ->orderBy('seed_num', 'asc')
            ->get();


        $pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
            ->where('draw_id', $draw->id)
            ->where('list_type', $list_type)
            ->orderBy('id')
            ->get();

        if (!count($pairs))
            return Redirect::to('signups/final-list/' . $draw_id);

        // prepare seed keys - user for getting seed number in front of team name
        $seed_ids = !$draw->manual_draw ? array_flip($seeds->lists('id')) : array_flip($seeds->lists('signup_id'));
        $jsArray = DrawMatchHelper::getBracketPairs($pairs, $draw, $list_type, $seed_ids);


        //checker to see if draw has a winner
        $winner_exist = (end($jsArray)[0][0]['id']) ? true : false;

        if ($winner_exist AND $list_type == 1)
            PointsCalculator::saveTournamentHistory($jsArray, $draw->level, $tournament->id, $draw->id);

        $results_form_view = View::make('draws/add_results');
        $referee_form_view = false;

        // ako je user referee generisi view za modal        
        if (Auth::user()->hasRole('referee'))
            $referee_form_view = View::make('draws/referee_sign', ['draw' => $draw]);

        $change_request_form_view = View::make('draws/change_request');

        $round_names = ($list_type == 1 OR $draw->qualification->has_final_round)
            ? DrawHelper::mainRoundNames($jsArray)
            : DrawHelper::qualiRoundNames($jsArray, $draw->size->accepted_qualifiers);

        // get lucky losers collection
        $lucky_losers = DrawMatchHelper::getLuckyLosers($draw_id, $list_type);

        $winner_jsArray = $jsArray;
        $winner = array_pop($winner_jsArray);

        $title = ($list_type == 2) ? LangHelper::get('qualifying_draw_bracket', "Qualifying draw bracket") : LangHelper::get('main_draw_bracket', "Main draw bracket");

        if (Agent::isMobile() && !Agent::isTablet()) {
            $this->layout->title = $title;
            $this->layout->content = View::make('draws/tournament_gracket_mobile', array(
                'draw' => $draw,
                'tournament' => $tournament,
                'title' => $title,
                'jsArray' => $jsArray,
                'winner' => $winner,
                'results_form_view' => $results_form_view,
                'isDoubles' => $draw->draw_type == "doubles",
                'seeds' => $seeds,
                'winner_exist' => $winner_exist,
                'lucky_losers' => $lucky_losers,
                'referee_form_view' => $referee_form_view,
                'change_request_form_view' => $change_request_form_view,
                'referee_name' => $referee_name,
                'round_names' => json_encode($round_names),
            ));
        } else {
            $this->layout->title = $title;
            $this->layout->content = View::make('draws/tournament_gracket', array(
                'draw' => $draw,
                'tournament' => $tournament,
                'title' => $title,
                'jsArray' => json_encode(array_values($jsArray)),
                'results_form_view' => $results_form_view,
                'isDoubles' => $draw->draw_type == "doubles",
                'seeds' => $seeds,
                'winner_exist' => $winner_exist,
                'lucky_losers' => $lucky_losers,
                'referee_form_view' => $referee_form_view,
                'change_request_form_view' => $change_request_form_view,
                'referee_name' => $referee_name,
                'round_names' => json_encode($round_names),
            ));
        }

    }

    public function getMatch($match_id)
    {
        $match = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data', 'scores')
            ->where('id', $match_id)
            ->first();

        // check if draw is finished
        if ($finish_check = DrawMatchHelper::checkIfDrawIsFinished($match->draw_id, $match->list_type))
            return $finish_check;

        $check_winner_next_round = false;
        if (!Auth::user()->hasRole('superadmin')) {
            $check_winner_next_round = DrawMatchHelper::checkIfWinnerInNextRoundExist($match);
        }

        $team1 = [];
        $team2 = [];
        $tie1 = [];
        $tie2 = [];
        if ($match->team1_data) {
            $name1 = DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $match->team1_data->team_id)->get(), true);

            if (count($match->scores)) {
                foreach ($match->scores as $key => $score) {
                    $team1[] = $score->set_team1_score;
                    $tie1[] = $score->set_team1_tie;
                }
            }
        } else {
            $name1 = "";
        }

        if ($match->team2_data) {
            $name2 = DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $match->team2_data->team_id)->get(), true);

            if (count($match->scores)) {
                foreach ($match->scores as $key => $score) {
                    $team2[] = $score->set_team2_score;
                    $tie2[] = $score->set_team2_tie;
                }
            }
        } else {
            $name2 = "";
        }

        $match_score = DrawMatchHelper::scoreAsString($match);
        $schedule = $match->schedule;
        $schedule_id = $schedule ? $schedule->id : null;

        $data = [
            'team1_name' => $name1,
            'team2_name' => $name2,
            'team1' => $team1,
            'team2' => $team2,
            'tie1' => $tie1,
            'tie2' => $tie2,
            'winner_next_round' => $check_winner_next_round,
            'match_score' => $match_score,
            'schedule_id' => $schedule_id,
        ];


        return Response::json(["data" => $data]);
    }

    public function postAddResults()
    {
        $match_id = Input::get('match_id');
        $team1_scores = Input::get('team1');
        $team2_scores = Input::get('team2');
        $scores = MatchScore::where("match_id", $match_id)->get();

        if(!Input::get('team1') || !Input::get('team2')){
            return Response::json(['success' => false, 'message' => LangHelper::get('cannot_add_score', 'You cannot add score for this match because teams are invalid.')]);
        }

        $match = DrawMatch::find($match_id);
        $draw = TournamentDraw::find($match->draw_id);

        if (Auth::getUser()->hasRole('referee') && $draw->approval_status == 1) {
            return Response::json(['success' => false, 'message' => LangHelper::get('cannot_edit_draw_referee', 'Draw cannot be edited because it was submitted to regional admin for approval')]);
        } else if (Auth::getUser()->hasRole('regional_admin') && $draw->approval_status == 2) {
            return Response::json(['success' => false, 'message' => LangHelper::get('cannot_edit_draw_regional', 'Draw cannot be edited because it was submitted to superadmin for approval')]);
        }

        if (count($scores)) {
            foreach ($scores as $score) {
                $score->delete();
            }
        }


        $set_counter = max([count($team1_scores), count($team2_scores)]);

        $final1_score = 0;
        $final2_score = 0;
        $inserted_sets = 0;
        $wrong_sets = $missing_tie_score = $has_error = $invalid_set_score = $invalid_set_for_tie_score = $invalid_tie_score = $invalid_2p_score = $match_rule_error = false;
        $max_set_score_number = 7;
        $as_final = Input::get('final');
        for ($i = 1; $i <= $set_counter; $i++) {
            // scores
            if (isset($team1_scores['score' . $i]) AND (!empty($team1_scores['score' . $i]) || $team1_scores['score' . $i] == 0))
                $team1_score = $team1_scores['score' . $i];
            else
                $team1_score = null;

            if (isset($team2_scores['score' . $i]) AND (!empty($team2_scores['score' . $i]) || $team2_scores['score' . $i] == 0))
                $team2_score = $team2_scores['score' . $i];
            else
                $team2_score = null;

            // tie breaks
            if ((isset($team1_scores['tie' . $i]) AND !empty($team1_scores['tie' . $i])) || (isset($team1_scores['tie' . $i]) && $team1_scores['tie' . $i] == 0))
                $team1_tie = $team1_scores['tie' . $i];
            else
                $team1_tie = null;

//            echo Debug::vars($team1_tie);die;

            if ((isset($team2_scores['tie' . $i]) AND !empty($team2_scores['tie' . $i])) || (isset($team2_scores['tie' . $i]) && $team2_scores['tie' . $i] == 0))
                $team2_tie = $team2_scores['tie' . $i];
            else
                $team2_tie = null;


            $final1_score += ($team1_score > $team2_score) ? 1 : 0;
            $final2_score += ($team1_score < $team2_score) ? 1 : 0;
            $team1_sc_arr[] = ($team1_score > $team2_score) ? 1 : 0;
            $team2_sc_arr[] = ($team1_score < $team2_score) ? 1 : 0;
            if ($team1_score != null AND $team2_score != null) {
                $inserted_sets++;
            }


            //validate if there is 2 points difference when there isn't tie break score
            if (!$invalid_2p_score && $team1_score != null && $team2_score != null && abs($team1_score - $team2_score) < 2 && $team1_tie == null && $team2_tie == null)
                $has_error = $invalid_2p_score = true;

            //validate if tie break is inserted on set_rule 0 - 6:6 or set_rule 1 - 4:4
            if ($team1_score != null && $team2_score != null) {
                $set_rules_number = $draw->set_rule == 0 ? 5 : 3;
                if ($team1_score > $set_rules_number && $team2_score > $set_rules_number && ($team1_tie == null || $team2_tie == null))
                    $has_error = $invalid_tie_score = true;
            }
            $max_set_score_number = $draw->set_rule == 0 ? 7 : 5;
            if ($team1_score > $max_set_score_number || $team2_score > $max_set_score_number)
                $has_error = $invalid_set_score = true;

//            if (
//                ($team1_score == $max_set_score_number && ($team2_score != $max_set_score_number - 1)) ||
//                ($team2_score == $max_set_score_number && ($team1_score != $max_set_score_number - 1))
//            )
//                $has_error = $invalid_set_for_tie_score = true;

            //check if sets winner and tie break winner are the same
            if ($team1_tie != null && $team2_tie != null) {
                if ($team1_score > $team2_score) {
                    if ($team2_tie > $team1_tie)
                        $has_error = $invalid_tie_score = true;
                } elseif ($team2_score > $team1_score) {
                    if ($team1_tie > $team2_tie)
                        $has_error = $invalid_tie_score = true;
                }

                if ($team1_tie < 7 && $team2_tie < 7)
                    $has_error = $invalid_tie_score = true;

                $diff = abs($team1_tie - $team2_tie);
                if (
                    (($team1_tie > 7 || $team2_tie > 7) && $diff != 2) ||
                    ($team1_tie == 7 && $team2_tie == 6) || ($team1_tie == 6 && $team2_tie == 7)
                ) {
                    $has_error = $invalid_tie_score = true;
                }


                if (
                    ($team1_score == 7 && $team2_score < 6) ||
                    ($team2_score == 7 && $team1_score < 6)
                )
                    $has_error = $invalid_set_for_tie_score = true;
            }

            //if score if 7:4 then we need tie score
            if (
                (($team1_score >= $max_set_score_number && $team2_score == $max_set_score_number - 1) ||
                    ($team2_score >= $max_set_score_number && $team1_score == $max_set_score_number - 1))
                && ($team1_tie == null || $team2_tie == null)
            ) {
                $has_error = $missing_tie_score = true;
            }


            if (!$has_error || ($has_error && !$as_final)) {
                if ($team1_score != null AND $team2_score != null) {
                    $match_score = new MatchScore;
                    $match_score->set_team1_score = $team1_score;
                    $match_score->set_team2_score = $team2_score;
                    $match_score->set_number = $i;
                    $match_score->match_id = $match_id;

                    if ($team1_tie != null AND $team2_tie != null) {
                        $match_score->set_team1_tie = $team1_tie;
                        $match_score->set_team2_tie = $team2_tie;
                    }

                    $match_score->save();
                }
            }
        }

        if ($as_final) {
            $message = '';


            if (
                ($draw->match_rule == 0 && ($inserted_sets < 2 || $inserted_sets > 3)) ||
                ($draw->match_rule == 1 && ($inserted_sets < 3 || $inserted_sets > 5))
            ) {
                $match_rule_error = true;
            }
            //check if player won round to  finish before last rounds
            if ($draw->match_rule == 0) {
                if (($final1_score >= 2 && $final2_score == 0) || ($final2_score >= 2 && $final1_score == 0)) {
                    if ($inserted_sets != 2) {
                        $wrong_sets = true;
                        $match_rule_error = false;
                    }
                }elseif (($final1_score >= 2 && $final2_score == 1) || ($final2_score >= 2 && $final1_score == 1)) {
                    if ($inserted_sets != 2) {
                        if (isset($team1_sc_arr[0]) && isset($team1_sc_arr[1]) && $team1_sc_arr[0] == $team1_sc_arr[1])
                            $wrong_sets = true;
                        if (isset($team2_sc_arr[0]) && isset($team2_sc_arr[1]) && $team2_sc_arr[0] == $team2_sc_arr[1])
                            $wrong_sets = true;
                    }
//                    if ($inserted_sets != 2) {
//                        $wrong_sets = true;
//                        $match_rule_error = false;
//                    }
                }
                else {
                    if ($inserted_sets < 3) {
                        $has_error =  $wrong_sets = true;
                    }
                }
            } //TODO: Provjeriti  5 set rules - treba dodati rule da se moze dodat rezultat od 4 seta
            elseif ($draw->match_rule == 1) {
                if (($final1_score >= 3 && $final2_score == 0) || ($final2_score >= 3 && $final1_score == 0)) {
                    if ($inserted_sets != 3) {
                        $wrong_sets = true;
                        $match_rule_error = false;
                    }
                } elseif (($final1_score == 4 && $final2_score == 1) || ($final2_score == 4 && $final1_score == 1)) {
                    if ($inserted_sets != 4) {
                        $wrong_sets = true;
                        $match_rule_error = false;
                    }
                } else if ($final1_score == 3 && $final2_score == 1 || $final2_score == 3 && $final1_score == 1) {
                    if ($inserted_sets != 4) {
                        $wrong_sets = true;
                        $match_rule_error = false;
                    }
                } else {
                    if ($inserted_sets < 5) {
                        $wrong_sets = true;
                    }
                }
            }


            //VALIDATION MESSAGES
            //tiebreak at 6:6
            if ($invalid_set_score)
                $message .= LangHelper::get("invalid_set_score_set_score_can_not_be_greater_than", "Invalid set score. Set score can not be greater than") . ' ' . $max_set_score_number . '. ';

            //tiebreak at 6:6
            if ($missing_tie_score)
                $message .= LangHelper::get("missing_tie_break_score", "Missing tie break score") . '. ';

            if ($invalid_set_for_tie_score)
                $message .= LangHelper::get("invalid_set_score_for_tie_score", "Invalid set score for tie score") . '. ';

            if ($invalid_tie_score)
                $message .= LangHelper::get("invalid_tie_break_score", "Invalid tie break score") . '. ';

            if ($wrong_sets)
                $message .= LangHelper::get("wrong_number_of_sets", "Wrong number of sets") . '. ';

            if ($invalid_2p_score)
                $message .= LangHelper::get("all_sets_must_have_at_least_2_points_difference", "All sets must have at least 2 points difference") . '. ';

            if ($match_rule_error)
                $message .= LangHelper::get("sets_to_be_played_is", "Sets to be played is ") . ($draw->match_rule == 0 ? " 3" : " 5");

            if ($message)
                return Response::json(["success" => false, 'message' => $message]);
        }


        $match->team1_score = $final1_score;
        $match->team2_score = $final2_score;
        $match->completed = false;
        if (Input::get('final'))
            $match->completed = true;

        $match->save();

        $schedule = MatchSchedule::where("match_id", $match_id)->first();
        if ($schedule && $schedule->match_status < 2) {
            $schedule->match_status = 3;
            $schedule->save();
        } elseif ($schedule && Input::get('final')) {
            $schedule->match_status = 4;
            $schedule->save();
        }

        if (Input::get('final'))
            DrawMatchHelper::sendTeamToNextRound($match);

        // if match has schedule send schedule id to brain socket
        $schedule = $match->schedules->first();
        $schedule_id = $schedule ? $schedule->id : null;

        MatchViolation::where("draw_id", $match->draw_id)
            ->where("round_number", $match->round_number)
            ->where("match_id", $match->id)->delete();

        return Response::json(["success" => true, 'matchid' => $match->id, 'schedule_id' => $schedule_id]);

    }

    public function postAddMatchStatus($match_id)
    {
        $match = DrawMatch::find($match_id);

        $input = Input::all();
        $winning_team = $input['team'];
        $match_status = $input['status'];

        if ($winning_team == 1) {
            $match->team1_score = 3;
            $match->team2_score = 0;
            $violation_team = $match->team2_id;
        } else {
            $match->team1_score = 0;
            $match->team2_score = 3;
            $violation_team = $match->team1_id;

        }

        $match_schedule = MatchSchedule::where("match_id", $match_id)->first();
        if ($match_schedule) {
            $match_schedule->match_status = 4;
            $match_schedule->save();
        }

        $match->completed = true;
        $match->match_status = $match_status;
        $match->save();

        DrawMatchHelper::addViolationForTeam($match, $violation_team);
        DrawMatchHelper::sendTeamToNextRound($match);

        return Response::json(["success" => true, 'matchid' => $match->id]);

    }

    public function getFinishQulifications($draw_id)
    {
        ini_set("memory_limit", -1);
        $matches = DrawMatch::whereDrawId($draw_id)
            ->where("list_type", 2)
            ->orderBy('id')
            ->get();

        $match_ids = array_pluck($matches->toArray(), 'id');

        DB::table('match_scores')->whereIn('match_id', $match_ids)->delete();

        foreach ($matches as $match) {

            $final1_score = 0;
            $final2_score = 0;

            for ($i = 1; $i <= 5; $i++) {
                $team1_score = rand(0, 6);
                $team2_score = rand(0, 6);

                $final1_score += ($team1_score >= $team2_score) ? 1 : 0;
                $final2_score += ($team1_score < $team2_score) ? 1 : 0;

                MatchScore::create([
                    'set_team1_score' => $team1_score,
                    'set_team2_score' => $team2_score,
                    'set_number' => $i,
                    'match_id' => $match->id,
                ]);
            }

            $teamscore1 = ($final1_score == $final2_score) ? 4 : $final1_score;
            $teamscore2 = $final2_score;

            if ($match->id != end($match_ids))
                $test[] = $match->sendTeamToNextRound($teamscore1, $teamscore2);

            $match->team1_score = $teamscore1;
            $match->team2_score = $teamscore2;
            $match->save();
        }

        return Redirect::to('/draws/db-bracket/' . $draw_id . '?list_type=2');
    }

    public function getSubmitQualifyingDraw($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);

        DrawMatchHelper::moveTeamsFromQualifyingToMainDraw($draw);

        DrawMatchHelper::saveToDrawHistory($draw_id, 2);

        return Redirect::to('/signups/final-list/' . $draw_id);
    }

    public function getSubmitMainDraw($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        $user = Auth::getUser();
        // save to draw history state of draw and who submitted draw
        DrawMatchHelper::saveToDrawHistory($draw_id, 1);

        $draw_history = DrawHistory::all();
        $draw_approval = new DrawApproval();
        if (TournamentHelper::mainDrawFinished($draw_id, $draw_history)) {
            if ($draw->status != 5) {
                $draw->status = 5;

                if ($user->getRole() == 'referee') {
                    $draw_approval->draw_id = $draw_id;
                    $draw_approval->approval_status = 1;
                    $draw_approval->approved_by = $user->id;
                    $draw_approval->save();

                    $draw->approval_status = 1;

                    $draw_region = TournamentDraw::join("tournaments", "tournament_draws.tournament_id", "=", "tournaments.id")->join("clubs", "tournaments.club_id", "=", "clubs.id")->join("regions", "regions.id", "=", "clubs.region_id")
                        ->where("tournament_draws.id", $draw_id)
                        ->get();

                    $region_id = $draw_region[0]['region_id'];
                    $admins = User::join('users_regions', 'users.id', '=', 'users_regions.user_id')->where('users_regions.region_id', $region_id)->get();

                    if ($admins) {
                        foreach ($admins as $admin) {
                            $message = LangHelper::get('draw_submitted_for_approval', 'Draw submitted for approval');
                            Notification::TournamentNotif($admin->user_id, $draw[0]['tournament_id'], 2, $message);
                        }
                    }
                } else if ($user->getRole() == 'regional_admin') {
                    $draw_approval->draw_id = $draw_id;
                    $draw_approval->approval_status = 2;
                    $draw_approval->approved_by = $user->id;
                    $draw_approval->save();

                    $draw->approval_status = 2;

                    $superadmins = User::join('users_roles', 'users.id', '=', 'users_roles.user_id')->where('users_roles.role_id', 1)->get();

                    if ($superadmins) {
                        foreach ($superadmins as $sadmin) {
                            $message = LangHelper::get('draw_submitted_for_approval', 'Draw submitted for approval');
                            Notification::TournamentNotif($sadmin->user_id, $draw[0]['tournament_id'], 2, $message);
                        }
                    }
                } else if ($user->getRole() == 'superadmin') {
                    $draw_approval->draw_id = $draw_id;
                    $draw_approval->approval_status = 3;
                    $draw_approval->approved_by = $user->id;
                    $draw_approval->save();

                    $draw->approval_status = 3;
                }

                $draw->save();
            }
        }
        // check if all draws in tournament are finished if they are set tournament status 5 - finished
        TournamentHelper::checkTournamentStatus($draw->tournament_id);

        return Redirect::to('/signups/final-list/' . $draw_id);
    }

    public function postCheckRefereeSign()
    {
        $name_check = strtolower(Auth::user()->info('name')) == strtolower(Input::get('name'));
        $surname_check = strtolower(Auth::user()->info('surname')) == strtolower(Input::get('surname'));

        if ($name_check AND $surname_check)
            return Response::json([
                'success' => true,
            ]);
        else
            return Response::json([
                'success' => false,
                'msg' => LangHelper::get('name_mismatch', 'You are not found in database with this credentials'),
            ]);


    }


    public function getManualBracket($draw_id)
    {
        $list_type = Request::query('list_type');
        $draw = TournamentDraw::find($draw_id);
        $tournament = $draw->tournament;

        if (PermissionHelper::checkPagePermission($tournament))
            return PermissionHelper::noPermissionPage($this);

        $qualifier_types = [
            0 => '',
            2 => '(Q)',
            3 => '(LL)',
        ];


        $this->layout->title = LangHelper::get('create_manual_bracket', "Create manual bracket");
        $this->layout->content = View::make('draws/manual_bracket', array(
            'list_type' => $list_type,
            'draw' => $draw,
            'tournament' => $tournament,
            'qualifier_types' => $qualifier_types,
        ));
    }

    public function postManualBracket($draw_id)
    {
        $this->layout = null;
        $inputs = Input::all();
        $list_type = array_pop($inputs)[0];
        $draw = TournamentDraw::find($draw_id);
        $draw->manual_draw = 1;
        $draw->save();

        $inputs = $inputs['player'];

        $intput_collection = new Illuminate\Database\Eloquent\Collection($inputs);
        $count_empty_values = $intput_collection->fetch('id')->filter(function ($item) {
            return (!isset($item) || trim($item) === '');
        })->count();
        // if there are empty values notifiy administrator
        if ($count_empty_values)
            return Response::json(['success' => false, 'message' => 'Please check your form, there are empty values']);

        //clear draw seed table with this draw id
        DrawSeed::whereDrawId($draw->id)->whereListType($list_type)->delete();
        usleep(250000);
        //clear draw match table with this draw id and this list type we are recreating everything
        DrawMatch::where('draw_id', $draw->id)->whereListType($list_type)->delete();
        usleep(250000);

        $data = DrawHelper::prepareDataForRoundsParsing($inputs, $draw_id, $list_type);

        $rounds_preparation = Draw::prepareTournament($data, $draw_id);

        DrawMatch::createMatchesFromManualDraw($rounds_preparation, $draw_id, $list_type);
        $matches = DrawMatch::firstRoundMatches($draw_id, $list_type);

        if (count($matches))
            DrawMatchHelper::setUpInitialScores($matches, $draw_id, $list_type);

        return Response::json(['success' => true, 'redirect_to' => '/draws/db-bracket/' . $draw_id . '?list_type=' . $list_type]);
    }

    public function getAutomaticDraw($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        $draw->manual_draw = 1;
        $draw->save();
        $list_type = (Request::query('list_type')) ? Request::query('list_type') : 1;
        $seed_numbers = Request::query();
        unset($seed_numbers['list_type']);

        $num_of_seeds = ($list_type == 1 OR $draw->qualification->has_final_round)
            ? $draw->number_of_seeds : ($draw->size->accepted_qualifiers * 2);

        //clear draw_seeds table for this draw
        DrawSeed::whereDrawId($draw->id)->whereListType($list_type)->delete();
        usleep(250000);
//echo Debug::vars($seed_numbers);
        if (count($seed_numbers)) {
            $signups = TournamentSignup::manualDrawDataWithDefinedSeeds($list_type, $draw_id, $seed_numbers, $num_of_seeds);
        } else {
            $signups = TournamentSignup::manualDrawData($list_type, $draw_id);
            DrawSeed::createSeeds($signups, $draw, $list_type);
        }


        $players = ($list_type == 1)
            ? Draw::makePairs($signups->toArray(), $num_of_seeds)
            : Draw::makeQualiPairs($signups->toArray(), $num_of_seeds);
        $result = DrawHelper::dataForAutomaticBracket($players);

        $total = $list_type == 2 ? $draw->qualification->draw_size : $draw->size->total;

        return Response::json([
            'data' => $result,
            'total' => $total,
            'list_type' => $list_type,
        ]);
    }

    public function getSignupsData($draw_id)
    {
        $list_type = Request::query('list_type');
        $draw = TournamentDraw::with('category', 'qualification', 'tournament')->find($draw_id);
        $tournament = $draw->tournament;
        $match_exist = false;
        $positions = [];

        $new_players = [];
        $show_quali_options = false;

        $num_of_seeds = ($list_type == 1 OR $draw->qualification->has_final_round)
            ? $draw->number_of_seeds : ($draw->size->accepted_qualifiers * 2);

        // check if matches in this draw exists
        if (!TournamentHelper::checkIfMatchesExist($draw->id, $list_type)) {
            if ($list_type == 2) {
                $signups = TournamentSignup::with('teams')
                    ->where('draw_id', $draw_id)
                    ->where('list_type', $list_type)
                    ->orderByRaw('(CASE ranking_points WHEN 0 THEN NULL ELSE ranking_points END) DESC NULLS LAST')
                    ->get();
            } else {
                $signups = TournamentSignup::where(function ($query) use ($list_type) {
                    $query->where('move_from_qualifiers_type', '>', 0)
                        ->orWhere('list_type', '=', $list_type);
                })->where('draw_id', $draw->id)
                    ->orderByRaw('(CASE ranking_points WHEN 0 THEN NULL ELSE ranking_points END) DESC NULLS LAST')
                    ->orderBy('move_from_qualifiers_type')
                    ->get();
            }

            if (!count($signups))
                return [];

            $positions = DrawMatchHelper::getManualDrawPositionNumbers(count($signups));

            $result = DrawHelper::prepareDataForManualDraws($signups, $num_of_seeds);

            $show_quali_options = (count($signups) < $draw->size->total);

            if ($result[0]['ranking_points'] == $result[1]['ranking_points'])
                $result[0]['same_points_class'] = 'team-holder-danger';

        } else {

            $pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
                ->where('draw_id', $draw->id)
                ->where('list_type', $list_type)
                ->where('round_number', 1)
                ->get();

            $seed_numbers = DrawSeed::getSeeds($draw_id, $list_type);

            foreach ($pairs as $pair) {
                $item1['id'] = $pair->team1_id;
                $item1['drag'] = true;
                $item1['name'] = $pair->team1_data
                    ? DrawMatchHelper::getTeamNames($pair->team1_data)
                    : DrawMatchHelper::getPlaceholderNames($pair->team1_id);
                $item['ranking_points'] = $pair->team1->ranking_points;
                $item1['seed_num'] = (array_key_exists($pair->team1_id, $seed_numbers)) ? $seed_numbers[$pair->team1_id] : null;
                $item1['move_from_qualifiers_type'] = ($pair->team1) ? $pair->team1->move_from_qualifiers_type : null;

                $result[] = $item1;

                $item2['id'] = $pair->team2_id;
                $item2['drag'] = true;
                $item2['name'] = $pair->team2_data
                    ? DrawMatchHelper::getTeamNames($pair->team2_data)
                    : DrawMatchHelper::getPlaceholderNames($pair->team2_id);
                $item['ranking_points'] = $pair->team2->ranking_points;
                $item2['seed_num'] = (array_key_exists($pair->team2_id, $seed_numbers)) ? $seed_numbers[$pair->team2_id] : null;
                $item2['move_from_qualifiers_type'] = ($pair->team2) ? $pair->team2->move_from_qualifiers_type : null;

                $result[] = $item2;
            }
            $match_exist = true;
            if ($list_type == 1) {
                $new_players = DrawMatchHelper::manualBracketNewPlayers($pairs, $draw->id);
                $positions = DrawMatchHelper::getManualDrawQualiPositions($pairs->toArray());
            }

            $show_quali_options = (count($new_players)) ? true : false;
        }

        $automatic_draw_disabled = false;
//        $automatic_draw_disabled = Draw::isAutomaticDrawDisabled($num_of_seeds, count($result));

        $total = ($list_type == 2)
            ? Draw::roundUpToNextPowerOfTwo($draw->qualification->draw_size)
            : Draw::roundUpToNextPowerOfTwo($draw->size->total);

        return Response::json([
            'data' => $result,
            'total' => $total,
            'list_type' => $list_type,
            'match_exist' => $match_exist,
            'positions' => $positions,
            'new_players' => $new_players,
            'show_quali_options' => $show_quali_options,
            'automatic_draw_disabled' => $automatic_draw_disabled,
        ]);
    }

    public function getSchedule($draw_id)
    {
        //redirect to new scheduler
        $draw = TournamentDraw::with('tournament')->find($draw_id);
        return Redirect::to("/schedule/show/" . $draw->tournament->id);


        $list_type = Request::query('list_type');
        $date_of_play = Request::query('date_of_play');
        $draw = TournamentDraw::with('tournament', 'tournament.date')->find($draw_id);
        $dates_array = TournamentHelper::getDrawDates($draw->tournament->date, $list_type);
        $draw_id_for_redirect = $draw_id;
        if ($list_type == 1)
            $first_date = $draw->tournament->date->main_draw_from;
        else
            $first_date = $draw->tournament->date->qualifying_date_from;

        $date = $first_date;

        $draw = TournamentDraw::with('tournament')->find($draw_id);

        $data = DrawMatch::with('team1_data', 'team2_data')
            ->whereDrawId($draw_id)
            ->whereListType($list_type)
            ->orderBy('id')
            ->get();


        $loaded_data = MatchSchedule::with('match')
            ->where('tournament_id', $draw->tournament_id)
            ->where('date_of_play', $date)
            ->get();


//		$default_counter = $dataCount / 2;
        $default_counter = 4;
        $plucked_loaded_date = array_pluck($loaded_data->toArray(), "court_number");
        if ($loaded_data && $plucked_loaded_date) {
            $count_values = max(array_count_values($plucked_loaded_date));
            $default_counter = $count_values + 1;
        }

        $first_one = [];

        if (!$loaded_data->isEmpty())
            $first_one = $loaded_data->first()->date_of_play;

        $pairs = DrawMatchHelper::getSchedulePairsDropdown($data, $loaded_data);


        $time_types = [null => 'Choose type of time'] + [
                '1' => 'Starting at',
                '2' => 'Not before',
                '3' => 'Followed by',
            ];
        $match_statuses = [
            null => 'Choose match status',
            '0' => 'Not started',
            '1' => 'To be played',
            '2' => 'Delayed',
            '3' => 'In progress',
            '4' => 'Completed',
        ];
        $results_form_view = View::make('draws/add_results');

        $view_partial = View::make('draws/schedule_content', array(
            'list_type' => $list_type,
            'draw' => $draw,
            'pairs' => [null => '---'] + $pairs,
            'json_pairs' => json_encode($pairs),
            'time_types' => $time_types,
            'match_statuses' => $match_statuses,
            'dates_array' => $dates_array,
            'date' => $date,
            'default_counter' => $default_counter,
            'results_form_view' => $results_form_view,
            'draw_id_for_redirect' => $draw_id_for_redirect,
            'first_one' => $first_one
        ));

        $this->layout->title = "Create schedule";
        $this->layout->content = View::make('draws/schedule', array(
            'list_type' => $list_type,
            'draw' => $draw,
            'date_of_play' => $date_of_play,
            'draw_id_for_redirect' => $draw_id,
            'dates_array' => $dates_array,
            'first_date' => $first_date,
            'view_partial' => $view_partial,
        ));
    }

    public function postSchedule($draw_id)
    {
        $inputs = Input::all();
        $list_type = isset($inputs['listType']) ? $inputs['listType'] : 0;
        $date = isset($inputs['date_of_play']) ? date('Y-m-d 00:00:00', strtotime($inputs['date_of_play'])) : DateTimeHelper::GetPostgresDateTime();

        foreach ($inputs as $key => $input) {
            if (!isset($input['match_id'])) {
                unset($inputs[$key]);
            }
        }

        $draw = TournamentDraw::with('tournament')->find($draw_id);

        foreach ($inputs as $key => $data) {

            MatchSchedule::create([
                'match_id' => isset($data['match_id']) ? $data['match_id'] : 0,
                'date_of_play' => $date,
                'court_number' => isset($data['court_num']) ? $data['court_num'] : 0,
                'order_of_play' => isset($data['order']) ? $data['order'] : 0,
                'time_of_play' => DrawMatchHelper::getScheduleTime($data),
                'list_type' => $list_type,
                'draw_id' => $draw_id,
                'tournament_id' => $draw->tournament->id,
            ]);
        }

        return Response::json([
            'success' => true,
            'redirect_to' => '/schedule/show/' . $draw->tournament->id . '?list_type=' . $list_type . '&date_of_play=' . $date
        ]);

    }

    public function getScheduleData($draw_id)
    {
        $this->layout = null;

        $list_type = Request::query('list_type');
        $draw_id_for_redirect = Request::query('redirection_draw');

        $data = Input::all();
        $date = str_replace('_', ' ', array_keys($data)[2]);
        $draw = TournamentDraw::with('tournament')->find($draw_id);

        $data = DrawMatch::with('team1_data', 'team2_data')
            ->whereDrawId($draw_id)
            ->whereListType($list_type)
            ->orderBy('id')
            ->get();


        $loaded_data = MatchSchedule::with('match')
            ->where('tournament_id', $draw->tournament->id)
            ->whereRaw("date_of_play::date = '" . DateTimeHelper::GetPostgresDate($date) . "'")
            ->get();


        $pairs = DrawMatchHelper::getSchedulePairsDropdown($data, $loaded_data);

//		$default_counter = $dataCount / 2;
        $default_counter = 4;
        $plucked_loaded_date = array_pluck($loaded_data->toArray(), "court_number");
        if ($loaded_data && $plucked_loaded_date) {
            $count_values = max(array_count_values($plucked_loaded_date));
            $default_counter = $count_values + 1;
        }

        $dates_array = [null => 'Choose date of play'] + TournamentHelper::getDrawDates($draw->tournament->date, $list_type);

        $time_types = [null => 'Choose type of time'] + [
                '1' => 'Starting at',
                '2' => 'Not before',
                '3' => 'Followed by',
            ];
        $match_statuses = [
            null => 'Choose match status',
            '0' => 'Not started',
            '1' => 'To be played',
            '2' => 'Delayed',
            '3' => 'In progress',
            '4' => 'Completed',
        ];
        $results_form_view = View::make('draws/add_results');

        return View::make('draws/schedule_content', array(
            'list_type' => $list_type,
            'draw' => $draw,
            'pairs' => [null => '---'] + $pairs,
            'json_pairs' => json_encode($pairs),
            'time_types' => $time_types,
            'match_statuses' => $match_statuses,
            'dates_array' => $dates_array,
            'date' => $date,
            'default_counter' => $default_counter,
            'results_form_view' => $results_form_view,
            'draw_id_for_redirect' => $draw_id_for_redirect,
        ));

    }

    public function getShowSchedule($tournament_id)
    {
        $tournament = Tournament::with('draws')->find($tournament_id);

        $list_type = (Request::query('list_type')) ? Request::query('list_type') : 0;

        $counter_builder = DrawMatch::whereIn('draw_id', $tournament->draws->lists('id', 'id'))->whereRoundNumber(1);

        if ($list_type != 0)
            $counter_builder->whereListType($list_type);

        $default_counter = $counter_builder->count();

        $dates = TournamentHelper::getDrawDates($tournament->date, $list_type);

        if (Request::query('ajax')) {
            $this->layout = null;

            return View::make('draws/show_schedule_content', array(
                'list_type' => $list_type,
                'tournament' => $tournament,
                'default_counter' => $default_counter,
                'dates' => $dates,
            ));
        }

        $this->layout->title = LangHelper::get('view_schedule', "View schedule");
        $this->layout->content = View::make('draws/show_schedule', array(
            'list_type' => $list_type,
            'tournament' => $tournament,
            'default_counter' => $default_counter,
            'dates' => $dates,
        ));

    }

    public function getSingleSchedule($schedule_id)
    {
        $this->layout = null;

        $match_data = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores')
            ->find($schedule_id);

        $checker = ($match_data)
            ? DrawMatchHelper::getScheduleData($match_data->court_number - 1, $match_data->order_of_play - 1,
                $match_data->tournament_id, DateTimeHelper::GetPostgresDateTime($match_data->date_of_play), $match_data->list_type)
            : false;

        return View::make('draws/single_schedule', array(
            'match_data' => $match_data,
            'checker' => $checker,
        ));

    }

    public function postChangeMatchStatus()
    {
        $match_id = Input::get('match_id');
        $match_status = Input::get('status');
        if ($match_id != "" && $match_status != "") {
            $match = MatchSchedule::whereMatchId($match_id)->first();
            if ($match_status == 3) {
                if (!$match->match_started_at)
                    $match->match_started_at = DateTimeHelper::GetDateNow();
            }
            if ($match_status == 4) {
                if (!$match->match_started_at)
                    $match->match_started_at = DateTimeHelper::GetDateNow();
                if (!$match->match_finished_at)
                    $match->match_finished_at = DateTimeHelper::GetDateNow();
            }

            $match->match_status = $match_status;
            $match->save();
        }

        return Response::json(["data" => 'true']);
    }

    public function getRemoveScheduledMatch($id, $draw_id, $list_type, $date_of_play)
    {
        if ($id) {
            $match = MatchSchedule::find($id);
            $match->delete();
        }
        $draw = TournamentDraw::with('tournament')->find($draw_id);

        return Redirect::to('/schedule/show/' . $draw->tournament->id . '?list_type=' . $list_type . '&date_of_play=' . $date_of_play);
    }

    public function getUndoBracket($draw_id)
    {
        $list_type = Request::query('list_type');

        $matches = DrawMatch::whereDrawId($draw_id);

        $matches = ($list_type)
            ? $matches->whereListType($list_type)
            : $matches->whereConsolationType(Request::query('consolation_type'));

        $matches = $matches->get();

        if ($list_type) {
            if (DrawMatchHelper::checkIfDrawHasScores($draw_id, $list_type)) {
                return Redirect::to('/draws/db-bracket/' . $draw_id . '?list_type=' . $list_type);
            } else {
                $draw = TournamentDraw::find($draw_id);
                $draw->status = 0;
                $draw->save();
                DrawMatchHelper::undoMatches($matches, $list_type, false);

                return Redirect::to('/draws/manual-bracket/' . $draw_id . '?list_type=' . $list_type);
            }
        } elseif (Request::query('consolation_type')) {
            $consolation_type = Request::query('consolation_type');
            if (ConsolationHelper::checkIfDrawHasScores($draw_id, $consolation_type)) {
                return Redirect::to('/draws/consolation-bracket/' . $draw_id . '?consolation_type=' . $consolation_type);
            } else {
                DrawMatchHelper::undoMatches($matches, $consolation_type, true);

                return Redirect::to('/draws/consolation-manual-bracket/' . $draw_id . '?consolation_type=' . $consolation_type);
            }
        }
    }

    public function postRequestChange($match_id)
    {
        $match = DrawMatch::with('team1_data', 'team2_data', 'draw')->find($match_id);

        $rules = [
            'text' => array('required'),
        ];


        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
            return $validator->messages()->toJson();

        $last_conversation_id = Message::max('conversation_id');

        $msg_template = View::make('messages/change_request_template', [
            'match' => $match,
            'list_type' => Input::get('list_type'),
            'msg_text' => Input::get('text'),
        ]);

        $message = Message::create(array(
            'subject' => LangHelper::get('request_for_result_change', 'Request for result change'),
            'text' => $msg_template,
            'sender_id' => Auth::user()->id,
            'time_sent' => DateTimeHelper::getDateNow(),
            'conversation_id' => $last_conversation_id + 1,
            'type_id' => 1,
        ));

        $send_to = User::whereHas('roles', function ($query) {
            $query->where('role_id', 1);
        })
            ->lists('id');

        $message->users()->attach($send_to);
        Notification::ChangeRequestNotif($send_to, $message->conversation_id);

        return Response::json([
            'success' => true,
            'message' => LangHelper::get('request_for_result_change_successfully_sent', 'Request for result change successfully sent!')
        ]);
    }

    public function getFinishDraw($draw_id)
    {
        $matches = DrawMatch::with('scores')
            ->whereDrawId($draw_id)
            ->whereListType(1)
            ->get();

        foreach ($matches as $match) {
            foreach ($match->scores as $score) {
                $score->delete();
            }

            for ($i = 1; $i <= 3; $i++) {
                $match_score = new MatchScore;
                $match_score->set_team1_score = 6;
                $match_score->set_team2_score = 4;
                $match_score->set_number = $i;
                $match_score->match_id = $match->id;

                $match_score->save();
            }

            $match->team1_score = 3;
            $match->team2_score = 0;
            DrawMatchHelper::sendTeamToNextRound($match);

            $match->save();
        }

        return Redirect::to('/draws/db-bracket/' . $draw_id . '?list_type=1');
    }

    public function getConsolationDraws($draw_id)
    {
        $draw = TournamentDraw::with('tournament', 'category')->find($draw_id);
        // fallback
        if (is_null($draw))
            return Redirect::to('/signups');

        $signups = TournamentSignup::with('teams')->whereDrawId($draw_id)
            ->whereIn('consolation', [1, 2])// search for players that are in consolation draws
            ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
            ->get()
            ->toArray();

        $players = ConsolationHelper::consolationLists($signups);

        $this->layout->title = LangHelper::get('consolation_draws', 'Consolation draws');
        $this->layout->content = View::make('draws/consolation/index', [
            'draw' => $draw,
            'players' => $players,
        ]);
    }

    public function getCreateConsolation($draw_id)
    {
        $draw = TournamentDraw::with('tournament')->find($draw_id);

        // fallback
        if (is_null($draw))
            return Redirect::to('/signups');

        $consolation_type = Request::query('consolation_type', 2);

        $this->layout->title = LangHelper::get('consolation_draw_create', 'Consolation draw create');
        $this->layout->content = View::make('draws/consolation/create', [
            'consolation_type' => $consolation_type,
            'draw' => $draw,
        ]);
    }

    public function postCreateConsolation($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        $signup_ids = Input::get('signups');

        // clear all consolation flags in signups table
        TournamentSignup::whereDrawId($draw_id)
            ->whereConsolation(Input::get('consolation_type'))
            ->update([
                'consolation' => 0,
            ]);

        if (count($signup_ids)) {
            // insert new consolation data
            TournamentSignup::whereIn('id', $signup_ids)
                ->update([
                    'consolation' => Input::get('consolation_type'),
                ]);
        }

        return Response::json([
            'success' => true,
            'message' => LangHelper::get('consolation_list_saveed', 'Consolation list saved'),
            'redirect_to' => URL::to('draws/consolation-draws', $draw->id),
        ]);
    }

    public function getCheckedSignups($draw_id, $consolation_type)
    {
        $signups = TournamentSignup::whereDrawId($draw_id)
            ->whereConsolation($consolation_type)
            ->lists('id');

        $players = ConsolationHelper::playersForDraw($draw_id, $consolation_type);

        return Response::json([
            'signups' => $signups,
            'players' => $players,
        ]);
    }

    public function getConsolationManualBracket($draw_id)
    {
        $draw = TournamentDraw::with('tournament')
            ->find($draw_id);

        $consolation_type = Request::query('consolation_type', 1);

        // fallback
        if (is_null($draw))
            return Redirect::to('/signups');

        $this->layout->title = LangHelper::get('create_manual_bracket', "Create manual bracket");
        $this->layout->content = View::make('draws/consolation/manual_bracket', array(
            'draw' => $draw,
            'consolation_type' => $consolation_type,
        ));
    }

    public function getConsolationManualData($draw_id)
    {
        $consolation_type = Request::query('consolation_type', 2); // default is 2 if not defined
        $draw = TournamentDraw::with('tournament')->find($draw_id);

        $match_exist = ConsolationHelper::checkIfMatchesExist($draw->id, $consolation_type);

        $data = ConsolationHelper::manualCreationData($draw, $consolation_type);
        $positions = DrawMatchHelper::getManualDrawPositionNumbers(count($data));
        $total = ConsolationHelper::getTotalPlayers($draw, $consolation_type);

        return Response::json([
            'data' => $data,
            'total' => $total,
            'consolation_type' => $consolation_type,
            'match_exist' => $match_exist,
            'positions' => $positions,
        ]);
    }

    public function postConsolationManualBracket($draw_id)
    {
        $this->layout = null;
        $inputs = Input::all();
        $consolation_type = array_pop($inputs)[0];
        $draw = TournamentDraw::find($draw_id);

        $inputs = $inputs['player'];

        //clear draw seed table with this draw id
        DrawSeed::whereDrawId($draw->id)->whereConsolationType($consolation_type)->delete();

        //clear draw match table with this draw id and this list type we are recreating everything
        DrawMatch::where('draw_id', $draw->id)->whereConsolationType($consolation_type)->delete();

        $data = ConsolationHelper::preparePlayersForRounds($inputs, $draw_id, $consolation_type);

        $rounds_preparation = Draw::prepareTournament($data, $draw_id);

        ConsolationHelper::saveMatchesToDB($rounds_preparation, $draw_id, $consolation_type);

        return Response::json(['success' => true, 'redirect_to' => '/draws/consolation-bracket/' . $draw_id . '?consolation_type=' . $consolation_type]);
    }

    public function getConsolationBracket($draw_id)
    {
        $consolation_type = Request::query('consolation_type', 1);

        $draw = TournamentDraw::with('tournament', 'tournament.club', 'tournament.sponsors')->find($draw_id);
        $tournament = $draw->tournament;
        $referee_name = ($tournament->referee() && $tournament->referee()->contact[0])
            ? $tournament->referee()->contact[0]->full_name()
            : "";

        $seeds = DrawSeed::with('teams')
            ->whereDrawId($draw_id)
            ->whereConsolationType($consolation_type)
            ->orderBy('seed_num', 'asc')
            ->lists('signup_id');


        $pairs = DrawMatch::with('team1', 'team2', 'team1_data', 'team2_data')
            ->where('draw_id', $draw->id)
            ->where('consolation_type', $consolation_type)
            ->orderBy('id')
            ->get();

        if (!count($pairs))
            return Redirect::to('draws/consolation-draws/' . $draw_id);

        // prepare seed keys - user for getting seed number in front of team name
        $seed_ids = array_flip($seeds);

        $jsArray = DrawMatchHelper::getBracketPairs($pairs, $draw, $consolation_type, $seed_ids, true);

        //checker to see if draw has a winner
        $winner_exist = (end($jsArray)[0][0]['id']) ? true : false;

        $results_form_view = View::make('draws/add_results');
        $referee_form_view = false;

        // ako je user referee generisi view za modal
        if (Auth::user()->hasRole('referee'))
            $referee_form_view = View::make('draws/referee_sign', ['draw' => $draw]);

        $change_request_form_view = View::make('draws/change_request');

        $round_names = ($consolation_type == 1)
            ? DrawHelper::mainRoundNames($jsArray)
            : DrawHelper::qualiRoundNames($jsArray, 0);

        $this->layout->title = LangHelper::get('consolation_bracket', "Consolation bracket");
        $this->layout->content = View::make('draws/consolation/bracket', array(
            'draw' => $draw,
            'tournament' => $tournament,
            'title' => $this->layout->title,
            'jsArray' => json_encode(array_values($jsArray)),
            'results_form_view' => $results_form_view,
            'seeds' => $seeds,
            'winner_exist' => $winner_exist,
            'referee_form_view' => $referee_form_view,
            'change_request_form_view' => $change_request_form_view,
            'referee_name' => $referee_name,
            'round_names' => json_encode($round_names),
        ));
    }


    public function getTest($draw_id = '')
    {
//        PointsCalculator::drawPointsAssign($draw_id);
        die('zavrsilo');
    }
}
