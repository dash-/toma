<?php

class SignupsController extends BaseController
{

    public function getIndex($tournament_id = '')
    {
        $regions_array = [];

        if (Auth::getUser()->hasRole('superadmin')) {

            $tournaments = Tournament::with('signups', 'draws', 'draws.category', 'draws.size',
                'draws.qualification', 'date', 'draws.signups', 'schedules')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->whereStatus(2)
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->select('*', 'tournaments.id as id');

            $regions_array = array(null => LangHelper::get('choose_region', 'Choose region'))
                + Region::orderBy('region_name')->get()->lists('region_name', 'id');

        } elseif (Auth::getUser()->hasRole('referee')) {

            $tournaments = Tournament::with('signups', 'draws', 'draws.category', 'draws.size', 'draws.qualification', 'date')
                ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id');

            if ($tournament_id != '')
                $tournaments = $tournaments->where('tournaments.id', $tournament_id);

            $tournaments->whereStatus(2)->where("referee_id", Auth::getUser()->id)
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->select('*', 'tournaments.id as id');

        } else {

            $tournaments = Tournament::join("clubs", function ($join) {
                $join->on("tournaments.club_id", "=", "clubs.id");
            })->join("regions", function ($join) {
                $join->on("regions.id", "=", "clubs.region_id");
            })->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                ->where("regions.id", Auth::getUser()->getUserRegion())
                ->whereStatus(2)
                ->orderBy('tournament_dates.main_draw_from', 'DESC')
                ->select("*", "tournaments.id as id");

        }

        if (Request::query('search')) {

            if (Request::query('month')) {
                // if year is submitted in query set that year if not set current year
                $year = (Request::query('year')) ? Request::query('year') : 'Y';

                $from_date = date($year . '-' . Request::query('month') . '-01');
                $to_date = date($year . '-' . Request::query('month') . '-t');

                $formatted_from = new DateTime($from_date);
                $formatted_to = new DateTime($to_date);

                // $tournaments = $tournaments->leftJoin('tournament_dates', 'tournament_dates.tournament_id', '=', 'tournaments.id')
                $tournaments = $tournaments->whereBetween('tournament_dates.main_draw_from', array($formatted_from->format('Y-m-d H:i:s'), $formatted_to->format('Y-m-d H:i:s')));

                if (Auth::user()->hasRole('superadmin'))
                    $tournaments = $tournaments->select("*", "tournaments.id as id");
            }
            if (Request::query('region')) {

                $tournaments = $tournaments->join("clubs", "tournaments.club_id", "=", "clubs.id")
                    ->join("regions", "regions.id", "=", "clubs.region_id")
                    ->where("regions.id", Request::query('region'));

            }

            if (Request::query('title'))
                $tournaments = $tournaments->where('tournaments.title', 'ILIKE', "%" . Request::query('title') . "%");
        }

        $draws_count = TournamentHelper::countTotalDraws(2);
        $draws_with_matches_list = TournamentDrawHelper::getDrawsWithMatches();

        $tournaments = $tournaments->paginate(30);

        $draw_history = DrawHistory::all();


        $this->layout->title = LangHelper::get('signups_and_draws', "Signups and draws");
        $this->layout->content = View::make('signups/index', array(
            'tournaments' => $tournaments,
            'regions_array' => $regions_array,
            'draws_count' => $draws_count,
            'draws_with_matches_list' => $draws_with_matches_list,
            'draw_history' => $draw_history,
        ));
    }

    public function getReferee($tournament_id)
    {
        $this->getIndex($tournament_id);
    }

    public function getList($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        $tournament = Tournament::find($draw->tournament_id);

        if (PermissionHelper::checkPagePermission($tournament))
            return PermissionHelper::noPermissionPage($this);

        $main_draw_matches_exists = TournamentHelper::checkIfMatchesExist($draw_id, 1);
        $qualifiyng_draw_matches_exists = TournamentHelper::checkIfMatchesExist($draw_id, 2);
        $qualification_finished = TournamentHelper::checkIfQualificationsAreFinished($draw_id);
        $types_array = [
            null => LangHelper::get('complete_list', 'Complete list'),
            1 => LangHelper::get('main_draw', 'Main draw'),
            2 => LangHelper::get('qualifying_draw', 'Qualifying draw'),
            3 => LangHelper::get('alternate_draw', 'Alternate draw'),
            4 => LangHelper::get('withdraw', 'Withdraw'),
        ];

        if (TournamentHelper::checkListState($draw->id))
            TournamentHelper::prepareSignupLists($draw);

        $players = TournamentSignup::with('teams')->whereDrawId($draw_id);

        $wild_cards_count = TournamentSignup::with('teams')
            ->whereDrawId($draw_id)
            ->where('list_type', 1)
            ->where('status', 1)
            ->count();

        if (Request::query('list_type'))
            $players = $players->where('list_type', '=', Request::query('list_type'));

        $players = $players
            ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
            ->get();
//        echo Debug::vars($players->toArray());die;

        $player_list = [];
        foreach ($players as $key => $player) {
            $player_list[$key]['id'] = $player->id;
            $player_list[$key]['wild_card_status'] = SignupHelper::wildCardStatus($player);
            $player_list[$key]['surname'] = TournamentHelper::getPlayerValue($player, 'surname');
            $player_list[$key]['name'] = TournamentHelper::getPlayerValue($player, 'name');
            $player_list[$key]['licence_number'] = TournamentHelper::getPlayerValue($player, 'licence_number');
            $player_list[$key]['date_of_birth'] = TournamentHelper::getPlayerValue($player, 'date_of_birth');
            $player_list[$key]['ranking_points'] = $player['ranking_points'];
            $player_list[$key]['status_name'] = $player->status();
            $player_list[$key]['status'] = $player->status;
            $player_list[$key]['list_type'] = $player->list_type;
            $player_list[$key]['list_type_name'] = $player->list_type();
            $player_list[$key]['list_color'] = $player->list_color();
        }
        $player_list = new Illuminate\Support\Collection($player_list);


        if (Request::query("sort") && Request::query("or"))
            $player_list = $player_list->sortBy(Request::query("sort"), null, Request::query("or") == "desc");

//d($player_list);die;
        $this->layout->title = LangHelper::get('list_of_signed_up_players', "List of signed up players");
        $this->layout->content = View::make('signups/list', array(
            'tournament' => $tournament,
            'draw' => $draw,
            'players' => $player_list,
            'types_array' => $types_array,
            'main_draw_matches_exists' => $main_draw_matches_exists,
            'qualifiyng_draw_matches_exists' => $qualifiyng_draw_matches_exists,
            "qualification_finished" => $qualification_finished,
            "wild_cards_count" => $wild_cards_count,
        ));
    }

    public function getRegister($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        $tournament = Tournament::find($draw->tournament_id);
        $signups = TournamentSignup::with('teams')->whereDrawId($draw_id)
            ->orderBy('id', 'desc')
            ->get();

        $signups_count = $signups->count();

        if (PermissionHelper::checkPagePermission($tournament))
            return PermissionHelper::noPermissionPage($this);

        $this->layout->title = LangHelper::get('signup_for_tournament', 'Signup for tournament') . " - " . $tournament->title;
        $this->layout->content = View::make('signups/register', array(
            'tournament' => $tournament,
            'draw' => $draw,
            'signups_count' => $signups_count,
            'signups' => $signups,
        ));
    }

    public function postRegister()
    {
        $draw = TournamentDraw::with('tournament')->find(Input::get('draw_id'));

        if (!Auth::getUser()->hasRole("superadmin") AND !Auth::user()->hasRole('referee')) {
            $tournament = $draw->tournament;
            if ($tournament->club->region_id != Auth::getUser()->getUserRegion())
                return PermissionHelper::noPermissionPage($this);
        }

        //part for automatic adding current_ranking to database
        $ranking_points = 0;
        $current_ranking = 0;
        $licence_number = Input::get('player.licence_number');
        $player = false; // double check
        $new_player = false;

        if ($licence_number) {
            $player = Player::whereLicenceNumber($licence_number)->first();
            if ($player) {
                $ranking_points = $player->ranking_points;
                $current_ranking = $player->ranking;
            } else {
                $contact = new Contact();
                $contact->name = Input::get("player.contact.name");
                $contact->surname = Input::get("player.contact.surname");
                $contact->date_of_birth = Input::get("player.contact.date_of_birth");
                $contact->email = Input::get("player.contact.email");
                $contact->phone = Input::get("player.contact.phone");
                $contact->save();

                $player = new Player();
                $player->contact_id = $contact->id;
                $player->status = 1;
                $player->created_by = Auth::id();
                $player->licence_number = $licence_number;
                $player->save();
                $new_player = true;

                Notification::TournamentNotValidatedSignupNotif($player->id, $draw->tournament->slug);

            }
        } else {
            $dob = DateTimeHelper::GetContactsDate(Input::get('player.contact.date_of_birth'));
            $player = Player::leftJoin('contacts', 'contacts.id', '=', 'players.contact_id')
                ->where('contacts.name', 'ILIKE', "%" . Input::get('player.contact.name') . "%")
                ->where('contacts.surname', 'ILIKE', "%" . Input::get('player.contact.surname') . "%")
                ->where('contacts.date_of_birth', '=', $dob)
                ->first();

            if (!is_null($player)) {
                $licence_number = $player->licence_number;
                $ranking_points = $player->ranking_points;
                $current_ranking = $player->ranking;
            }
        }

        // if tournament type is different from RFET check if player belongs to federation/province
        if (!is_null($player)) {
            $check_message = false;
            if ($draw->tournament->tournament_type > 1) // check tournament type if != RFET
                $check_message = Import::checkTournamentType($player, $draw->tournament); // return check message if there is no message return false

            if ($check_message) {
                return Response::json([
                    'success' => false,
                    'message' => $check_message,
                    'new_player' => $new_player
                ]);
            }
        }

        $current_ranking = ($current_ranking) ? $current_ranking : 0;
        $signup_data = [
            'submited_by' => Auth::getUser()->id,
            'tournament_id' => $draw->tournament_id,
            'draw_id' => $draw->id,
            'created_at' => DateTimeHelper::GetDateNow(),
            'ranking_points' => $ranking_points,
            'rank' => $current_ranking,
        ];

        $signup = TournamentSignup::create($signup_data);

        $team_data = [
            'licence_number' => $licence_number,
            'name' => Input::get('player.contact.name'),
            'surname' => Input::get('player.contact.surname'),
            'date_of_birth' => DateTimeHelper::GetPostgresDateTime(Input::get('player.contact.date_of_birth')),
            'email' => Input::get('player.contact.email'),
            'phone' => Input::get('player.contact.phone'),
            'current_ranking' => $current_ranking,
            'team_id' => $signup->id,
            'ranking_points' => $ranking_points,
        ];

        $team = TournamentTeam::create($team_data);

        $turnament_signup = TournamentSignup::whereDrawId($draw->id)->update([
            'list_type' => null,
        ]);

        TournamentHelper::prepareSignupLists($draw);

        // send notification to superadmin
        if (!$team->licence_number)
            Notification::TournamentSignupNotif($signup->id, $draw->tournament->title);

        return Response::json(['success' => true, 'redirect' => URL::to('signups/register/' . $draw->id)]);
    }

    public function postRegisterDoubles()
    {
        $draw = TournamentDraw::find(Input::get('draw_id'));

        if (!Auth::getUser()->hasRole("superadmin")) {
            $tournament = Tournament::find($draw->tournament_id);
            if ($tournament->club->region_id != Auth::getUser()->getUserRegion())
                return PermissionHelper::noPermissionPage($this);
        }
        $players = [];
        $players = array_merge($players, [Input::get('player'), Input::get('player2')]);
        $signup_data = [
            'submited_by' => Auth::getUser()->id,
            'tournament_id' => $draw->tournament_id,
            'draw_id' => $draw->id,
            'created_at' => DateTimeHelper::GetDateNow(),
            'rank' => 0,
        ];
        $signup = TournamentSignup::create($signup_data);

        $current_ranking = [0, 0];
        $ranking_points = [0, 0];
        foreach ($players as $key => $p) {
            $team_data = [];
            if (isset($p['licence_number'])) {

                $player = Player::whereLicenceNumber($p['licence_number'])->first();
                $current_ranking[$key] = ($player) ? $player->ranking : 0;
                $ranking_points[$key] = ($player) ? $player->ranking_points : 0;

            } else {

                $dob = (isset($p['contact']['date_of_birth'])) ? DateTimeHelper::GetContactsDate($p['contact']['date_of_birth']) : '';
                $contact_name = (isset($p['contact']['name'])) ? $p['contact']['name'] : '';
                $contact_surname = (isset($p['contact']['surname'])) ? $p['contact']['surname'] : '';

                $player = Player::leftJoin('contacts', 'contacts.id', '=', 'players.contact_id')
                    ->where('contacts.name', 'ILIKE', "%" . $contact_name . "%")
                    ->where('contacts.surname', 'ILIKE', "%" . $contact_surname . "%")
                    ->where('contacts.date_of_birth', '=', $dob)
                    ->first();


                if (!is_null($player)) {
                    $licence_number = $player->licence_number;
                    $current_ranking = $player->ranking;
                    $ranking_points = $player->ranking_points;
                }
            }

            $team_data = [
                'licence_number' => (isset($p['licence_number'])) ? $p['licence_number'] : 0,
                'name' => (isset($p['contact']['name'])) ? $p['contact']['name'] : '',
                'surname' => (isset($p['contact']['surname'])) ? $p['contact']['surname'] : '',
                'date_of_birth' => (isset($p['contact']['date_of_birth'])) ? DateTimeHelper::GetPostgresDateTime($p['contact']['date_of_birth']) : '',
                'email' => (isset($p['contact']['email'])) ? $p['contact']['email'] : '',
                'phone' => (isset($p['contact']['phone'])) ? $p['contact']['phone'] : '',
                'current_ranking' => $current_ranking[$key],
                'ranking_points' => $ranking_points[$key],
                'team_id' => $signup->id,
            ];
            $team = TournamentTeam::create($team_data);

            if (!$team->licence_number)
                Notification::TournamentSignupNotif($signup->id, $draw->tournament->title);
        }

        //update signup rank
        $signup->rank = (max($current_ranking)) ? max($current_ranking) : 0;
        $signup->ranking_points = (max($ranking_points)) ? max($ranking_points) : 0;
        $signup->save();

        return Response::json(['success' => true, 'redirect' => URL::to('signups/register/' . $draw->id)]);

    }

    public function postCheckSinguper()
    {
        $draw = TournamentDraw::find(Input::get('draw_id'));


        $category = DrawCategory::find($draw->draw_category_id);

        $signups = TournamentSignup::leftJoin('tournament_teams', function ($join) {
            $join->on('tournament_signups.id', '=', 'tournament_teams.team_id');
        })->whereDrawId(Input::get('draw_id'))
            ->where('name', 'ILIKE', "%" . Input::get('player.contact.name') . "%")
            ->where('surname', 'ILIKE', "%" . Input::get('player.contact.surname') . "%")
            ->where('date_of_birth', '=', DateTimeHelper::GetPostgresDateTime(Input::get('player.contact.date_of_birth')))
            ->first();

        $final_draw_date = $draw->tournament->date->main_draw_to;

        $years = DateTimeHelper::CompareDifferenceInTime(Input::get('player.contact.date_of_birth'), $final_draw_date);

        $years_check = $years >= $category->range_for_signup_from && $years <= $category->range_for_signup_to;

        if ($signups)
            return Response::json(['success' => false, 'message' => LangHelper::get('already_signed_up_for_draw', 'You have already signed up for this draw')]);
        elseif (!$years_check)
            return Response::json(['success' => false, 'message' => LangHelper::get('you_dont_belong_to_category', 'Sorry, You don\'t belong to this category')]);
        else
            return Response::json(['success' => true]);
    }

    public function postCheckDoubles()
    {
        $draw = TournamentDraw::find(Input::get('draw_id'));

        $category = DrawCategory::find($draw->draw_category_id);
        $players = [];
        $players = array_merge($players, [Input::get('player'), Input::get('player2')]);
        foreach ($players as $key => $p) {
            $contact_name = ($p['contact']['name']) ? $p['contact']['name'] : null;
            $contact_surname = ($p['contact']['surname']) ? $p['contact']['surname'] : null;
            $contact_dob = ($p['contact']['date_of_birth']) ? DateTimeHelper::GetPostgresDateTime($p['contact']['date_of_birth']) : null;

            $signup = TournamentSignup::leftJoin('tournament_teams', function ($join) {
                $join->on('tournament_signups.id', '=', 'tournament_teams.team_id');
            })->whereDrawId(Input::get('draw_id'))
                ->where('name', 'ILIKE', "%" . $contact_name . "%")
                ->where('surname', 'ILIKE', "%" . $contact_surname . "%")
                ->where('date_of_birth', '=', $contact_dob)
                ->first();

            $years = DateTimeHelper::GetDifferenceInTime($contact_dob);
            $years_check = $years >= $category->range_for_signup_from && $years <= $category->range_for_signup_to;
            if ($signup)
                return Response::json(['success' => false, 'message' => LangHelper::get('already_signed_up_for_draw', 'You have already signed up for this draw')]);
            elseif (!$years_check)
                return Response::json(['success' => false, 'message' => LangHelper::get('you_dont_belong_to_category', 'Sorry, You don\'t belong to this category')]);

        }

        return Response::json(['success' => true]);
    }

    public function getLicence()
    {
        $search_data = Request::query('licence_number');
        $category = DrawCategory::find(Request::query('category_id'));
        $draw_id = Request::query('draw_id');

        $player = Player::with('Contact')->whereLicenceNumber($search_data)->first();

        if ($player) {
            $draw = TournamentDraw::where("id", $draw_id)->first();
            $final_draw_date = $draw->tournament->date->main_draw_to;

            $years = DateTimeHelper::CompareDifferenceInTime($player->contact->date_of_birth, $final_draw_date);

            $signups = TournamentSignup::leftJoin('tournament_teams', function ($join) {
                $join->on('tournament_signups.id', '=', 'tournament_teams.team_id');
            })->whereDrawId($draw_id)
                ->whereLicenceNumber($player->licence_number)
                ->first();

            if ($signups)
                return Response::json(['success' => false, 'player' => 'no way', 'message' => LangHelper::get('already_signed_up_for_draw', 'You have already signed up for this draw')]);

            if ($years >= $category->range_for_signupfrom && $years <= $category->range_for_signup_to)
                return Response::json(['success' => true, 'player' => $player->getSignupInformation(), 'message' => 'ok']);

            return Response::json(['success' => false, 'player' => 'no way', 'message' => LangHelper::get('you_dont_belong_to_category_your_category_is', 'Sorry, You don\'t belong to this category, Your category is ') . '' . $player->getAgeCategory('name')]);

        }

        return Response::json(['success' => false, 'player' => $player, 'message' => LangHelper::get('no_information_found', 'Sorry, no information was found, populate information manually')]);
    }
    // morao sam odvojiti previše različitih stvari
    // da ne bi metoda bila 150 linija a ovako je i preglednije
    public function getDoublesLicence()
    {
        $search_data = array(Request::query('licence_number1'), Request::query('licence_number2'));

        $category = DrawCategory::find(Request::query('category_id'));
        // dd($search_data);
        $players = Player::with('Contact')->whereIn('licence_number', $search_data)->get();

        if (count($players)) {
            foreach ($players as $key => $player) {
                $years[] = DateTimeHelper::GetDifferenceInTime($player->contact->date_of_birth);

                $signups[] = TournamentSignup::leftJoin('tournament_teams', function ($join) {
                    $join->on('tournament_teams.team_id', '=', 'tournament_teams.team_id');
                })->whereDrawId(Request::query('draw_id'))
                    ->whereLicenceNumber($player->licence_number)
                    ->first();

                $gamers[] = (count($player)) ? $player->getSignupInformation() : null;
            }
            // dd($gamers);
            // $first_player = (isset($signups[0])) ? $signups[0]->getSignupInformation() : false;
            // $second_player = (isset($signups[1])) ? $signups[1]->getSignupInformation() : false;

            $first_player = (isset($gamers[0])) ? $gamers[0] : false;
            $second_player = (isset($gamers[1])) ? $gamers[1] : false;


            return Response::json(['success' => true, 'player' => $first_player, 'player2' => $second_player, 'message' => 'ok', 'doubles' => true]);

        }

        return Response::json(['success' => false, 'message' => LangHelper::get('no_information_found', 'Sorry, no information was found, populate information manually')]);
    }

    public function getPlayers()
    {
        $q = Request::query('query');
        $gender = strtoupper(Request::query('gender'));
        $cat = DrawCategory::find(Request::query('category_id'));

        $tournament = Tournament::find(Request::query('tournament_id'));

        $category_from = DateTimeHelper::GetYearsForDrawCategories($cat->range_for_signup_from, $tournament->date->main_draw_from, true);
        $category_to = DateTimeHelper::GetYearsForDrawCategories($cat->range_for_signup_to, $tournament->date->main_draw_from);

        if ($q[0] == "*") {
            $q = ltrim($q, "*");
            $q = "%" . $q;
        }

        $players = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id')
            ->where('contacts.sex', $gender)
            ->whereRaw("to_date(date_of_birth, 'MM:DD:YYYY') BETWEEN '" . $category_to . "' AND '" . $category_from . "'")
            ->where(function ($query) use ($q) {
                $query->whereRaw(" CAST(players.licence_number AS TEXT) LIKE '" . $q . "%' ")
                    ->orWhere('contacts.name', 'ILIKE', $q . "%")
                    ->orWhere('contacts.surname', 'ILIKE', $q . "%");
            });

        //check if tournament_type != RFET
        if (!is_null($tournament)) {
            if ($tournament->tournament_type == 2 AND $tournament->getRegionId())
                $players = $players->where('federation_club_id', $tournament->getRegionId());
            elseif ($tournament->tournament_type == 3 AND $tournament->getProvinceId())
                $players = $players->where('province_club_id', $tournament->getProvinceId());
        }

        $players = $players->orderBy('contacts.date_of_birth', 'desc')
            ->take(10)
            ->get();


        $players_array = [];
        foreach ($players as $player) {
            $item['licence_number'] = $player->licence_number;
            $item['full_name'] = $player->licence_number . ' - ' . $player->name . ' ' . $player->surname . ' - ' . DateTimeHelper::GetShortDateFullYear($player->date_of_birth);

            $players_array[] = $item;
        }

        return Response::json($players_array);
    }

    // automatic manipulation of data for testing purposes, remove on production
    public function getAutomaticSignup($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);

        if (!Auth::getUser()->hasRole("superadmin")) {
            $tournament = Tournament::find($draw->tournament_id);
            if ($tournament->club->region_id != Auth::getUser()->getUserRegion())
                return PermissionHelper::noPermissionPage($this);
        }

        $range = ($draw->category->range_from > 0) ? $draw->category->range_from : 10;
        $category_year = date("Y") - $range;
//        $category_year = 1987;
        $sum_of_players = ($draw->size->total * 2) + $draw->qualification->draw_size;
        $players = Player::leftJoin('contacts', 'players.contact_id', '=', 'contacts.id')
            ->where('date_of_birth', 'ILIKE', "%" . $category_year . "%")
            ->where('sex', 'ILIKE', "%" . $draw->draw_gender . "%")
            ->where('ranking_points', '>', 100)
            ->orderBy(DB::raw('RANDOM()'))
            ->take($sum_of_players)
            ->get();

        if (count($players) == 0)
            return Redirect::to(URL::to('signups?open=' . $draw->tournament_id));

        foreach ($players as $p) {
            $signup_data = [
                'submited_by' => Auth::getUser()->id,
                'tournament_id' => $draw->tournament_id,
                'draw_id' => $draw->id,
                'created_at' => DateTimeHelper::GetPostgresDate(date('Y-m-d', time())),
                'rank' => ($p->ranking) ? $p->ranking : 0,
                'ranking_points' => ($p->ranking_points) ? $p->ranking_points : 0,
            ];
            $signup = TournamentSignup::create($signup_data);
            $team_data = [];

            $team_data = [
                'licence_number' => $p->licence_number,
                'name' => $p->name,
                'surname' => $p->surname,
                'date_of_birth' => DateTimeHelper::GetPostgresDateTime($p->date_of_birth),
                'email' => $p->email,
                'phone' => $p->phone,
                'current_ranking' => ($p->ranking) ? $p->ranking : 0,
                'ranking_points' => ($p->ranking_points) ? $p->ranking_points : 0,
                'team_id' => $signup->id,
            ];
            $team = TournamentTeam::create($team_data);
        }

        return Redirect::to(URL::to('signups?open=' . $draw->tournament_id));
    }

    public function getAutomaticDoubles($draw_id)
    {
        $draw = TournamentDraw::find($draw_id);
        if (!Auth::getUser()->hasRole("superadmin")) {
            $tournament = Tournament::find($draw->tournament_id);
            if ($tournament->club->region_id != Auth::getUser()->getUserRegion())
                return PermissionHelper::noPermissionPage($this);
        }
        $range = ($draw->category->range_from > 0) ? $draw->category->range_from : 10;
        $category_year = date("Y") - $range;

        $sum_of_players = (($draw->size->total * 2) + $draw->qualification->draw_size) * 2;
        $players = Player::leftJoin('contacts', 'players.contact_id', '=', 'contacts.id')
            ->where('date_of_birth', 'ILIKE', "%" . $category_year . "%")
            ->where('sex', 'ILIKE', "%" . $draw->draw_gender . "%")
            ->orderBy(DB::raw('RANDOM()'))
            ->take($sum_of_players)
            ->get()
            ->toArray();

        if (count($players) == 0)
            return Redirect::to(URL::to('signups?open=' . $draw->tournament_id));

        for ($i = 0; $i < count($players); $i += 2) {
            $player1 = $players[$i];
            $player2 = $players[$i + 1];

            $pairs[] = array(
                'player1' => $player1,
                'player2' => $player2,
            );
        }


        foreach ($pairs as $key => $pair) {
            $rank = max($pair['player1']['ranking'], $pair['player2']['ranking']);
            $ranking_points = max($pair['player1']['ranking_points'], $pair['player2']['ranking_points']);
            $signup_data = [
                'submited_by' => Auth::getUser()->id,
                'tournament_id' => $draw->tournament_id,
                'draw_id' => $draw->id,
                'created_at' => DateTimeHelper::GetPostgresDate(date('Y-m-d', time())),
                'rank' => ($rank) ? $rank : 0,
                'ranking_points' => ($ranking_points) ? $ranking_points : 0,
            ];

            $signup = TournamentSignup::create($signup_data);
            foreach ($pair as $key => $p) {
                $team_data = [
                    'licence_number' => $p['licence_number'],
                    'name' => $p['name'],
                    'surname' => $p['surname'],
                    'date_of_birth' => DateTimeHelper::GetPostgresDateTime($p['date_of_birth']),
                    'email' => $p['email'],
                    'phone' => $p['phone'],
                    'current_ranking' => $p['ranking'],
                    'ranking_points' => $p['ranking_points'],
                    'team_id' => $signup->id,
                ];
                TournamentTeam::create($team_data);
            }

        }

        return Redirect::to(URL::to('signups?open=' . $draw->tournament_id));
    } // finished automatic populations

    public function getAddStatus($player_id)
    {
        $player = TournamentSignup::find($player_id);
        $status = Request::query('status');

        $player->update([
            'status' => $status,
        ]);

        $draw = TournamentDraw::with('qualification', 'size')->find($player->draw_id);
        TournamentHelper::prepareSignupLists($draw);

        return Redirect::to(URL::to('signups/list/' . $player->draw_id));

    }

    public function getRemoveStatus($player_id)
    {
        $player = TournamentSignup::find($player_id);
        $player->update([
            'status' => 0,
        ]);

        $draw = TournamentDraw::with('qualification', 'size')->find($player->draw_id);
        TournamentHelper::prepareSignupLists($draw);

        return Redirect::to(URL::to('signups/list/' . $player->draw_id));

    }

    public function getWithdraw($player_id)
    {
        $player = TournamentSignup::find($player_id);

        $player->update([
            'list_type' => 4,
            'withdrawal_date' => DateTimeHelper::GetDateNow(),
        ]);

        $draw = TournamentDraw::with('qualification', 'size')->find($player->draw_id);
        TournamentHelper::prepareSignupLists($draw);

        return Redirect::to(URL::previous());
    }

    public function getChangeType($player_id, $list_type)
    {

        $player = TournamentSignup::find($player_id);
        $draw = TournamentDraw::with('qualification', 'size')->find($player->draw_id);
        if ($draw->qualification->size) {
            $available_status = [
                1 => 5,
                2 => 6,
                3 => 7,
            ];

            $player->update([
                'list_type' => $list_type,
                'status' => $available_status[$list_type], // just so we know user was manually moved from one list to another
            ]);

            TournamentHelper::prepareSignupLists($draw);
        }
        return Redirect::to(URL::previous());
    }


    public function getFinalList($draw_id)
    {
        $draw = TournamentDraw::with('category', 'tournament')->find($draw_id);
        if(!$draw)
            return Redirect::to("/");

        $tournament = $draw->tournament;

        if (PermissionHelper::checkPagePermission($tournament))
            return PermissionHelper::noPermissionPage($this);

        $matches_count = DrawMatch::whereDrawId($draw_id)->count();
//
        if ($matches_count == 0)
            TournamentHelper::prepareSignupLists($draw);

        $check_if_matches_exists = [
            1 => TournamentHelper::checkIfMatchesExist($draw->id, 1),
            2 => TournamentHelper::checkIfMatchesExist($draw->id, 2),
            3 => TournamentHelper::checkIfMatchesExist($draw->id, 3),
            4 => TournamentHelper::checkIfMatchesExist($draw->id, 4),
        ];

        $players = TournamentSignup::with('teams')->whereDrawId($draw_id)
            ->orderBy(DB::raw('(ranking_points = 0), ranking_points'), 'desc')
            ->orderBy('move_from_qualifiers_type')
            ->get();

        $final = TournamentHelper::separateSignupsByCategories($players->toArray());
        $final = ArrayHelper::swapAssoc(1, 2, $final);

        TournamentHelper::calculateTournamentLevel(array_slice($players->toArray(), 0, 8), $draw);
        $reports = TournamentDrawHelper::getBracketStatusReport($draw_id);

        SignupHelper::checkPlayersWithoutListType($players);


        $this->layout->title = LangHelper::get('signups_final_list', "Signups final list");
        $this->layout->content = View::make('signups/final_list', array(
            'final' => $final,
            'draw' => $draw,
            'tournament' => $tournament,
            'reports' => $reports,
            'check_if_matches_exists' => $check_if_matches_exists,
        ));

    }

    public function getPlayerDetails($signup_id)
    {
        $player = TournamentSignup::with('teams')
            ->whereId($signup_id)
            ->first();

        // echo Debug::vars($player->toArray());die;

        $this->layout->title = LangHelper::get('signup_player_details', "Signup player details");
        $this->layout->content = View::make('signups/player_details', array(
            'player' => $player,
        ));

    }

    public function getBulkImport($draw_id)
    {
        $draw = TournamentDraw::with('tournament', 'signups', 'size')->find($draw_id);

        if (PermissionHelper::checkPagePermission($draw->tournament))
            return PermissionHelper::noPermissionPage($this);

        $this->layout->title = LangHelper::get('bulk_import', 'Bulk import') . " - " . $draw->tournament->title;
        $this->layout->content = View::make('signups/bulk_import', array(
            'draw' => $draw,
        ));
    }

    public function postBulkImport($draw_id)
    {
        $draw = TournamentDraw::with('category', 'tournament')->find($draw_id);

        // kad se radi upload fajla sa angularom Input::file() ne cita fajl jedino $_FILES radi 
        if (!$_FILES) {
            $importer = Import::parseInputAndSave(Input::get('bulk'), $draw, false);
        } else {
            $filename = Str::random(10) . '_' . str_replace(" ", "", basename($_FILES["file"]["name"]));
            $file_path = public_path() . '/csv/' . $filename;
            move_uploaded_file($_FILES["file"]["tmp_name"], $file_path);

            $importer = Import::parseInputAndSave($file_path, $draw, true);
        }

        if (count($importer)) {
            $turnament_signup = TournamentSignup::whereDrawId($draw_id)->update([
                'list_type' => null,
            ]);
            TournamentHelper::prepareSignupLists($draw);

            return Response::json([
                'success' => true,
                'licence_fails' => $importer['licence_fails'],
                'msg' => LangHelper::get('inserted', 'Inserted') . ' ' .
                    $importer['insert_count'] . ' of ' . $importer['bulk_count'] . ' ' . LangHelper::get('licence_numbers', 'licence numbers'),
            ]);
        } else {
            return Response::json([
                'success' => false,
            ]);
        }
    }

    public function getGenerateDoubles($draw_id)
    {
        $draw = TournamentDraw::with('tournament')
            ->find($draw_id);

        $this->layout->title = LangHelper::get('generate_doubles', "Generate doubles");
        $this->layout->content = View::make('signups/generate_doubles', array(
            'draw' => $draw,
        ));
    }

    public function postGenerateDoubles($draw_id)
    {
        $players = Input::get('player');

        foreach ($players as $player) {
            if ($player['team1_id'] != $player['team2_id']) {
                $team = TournamentTeam::where('team_id', $player['team2_id'])->first();

                $team->team_id = $player['team1_id'];
                $team->save();

                $team1 = TournamentSignup::find($player['team1_id']);

                $signup = TournamentSignup::find($player['team2_id']);

                $rank = ($team1->rank >= $signup->rank) ? $team1->rank : $signup->rank;
                $team1->rank = $rank;
                $team1->save();

                $signup->delete();
            }
        }

        return Response::json([
            'success' => true,
            'redirect_to' => URL::to('signups/list', $draw_id)
        ]);

    }

    public function getDoublesState($draw_id)
    {
        $signups = TournamentSignup::with('teams')
            ->whereDrawId($draw_id)
            ->get();

        $single_players = SignupHelper::separatePairs($signups, false);
        $single_players_count = count($single_players);
        $positions = range(1, round($single_players_count / 2));

        $pairs = SignupHelper::separatePairs($signups, true);

        return Response::json([
            'single_players' => $single_players,
            'pairs' => $pairs,
            'positions' => $positions,
        ]);
    }

    public function getFilePairs($draw_id)
    {
        $draw = TournamentDraw::with('tournament', 'signups', 'category', 'size')->find($draw_id);

        $this->layout->title = LangHelper::get('create_pairs_from_file', 'Create pairs from file');
        $this->layout->content = View::make('signups/create_pairs_from_file', [
            'draw' => $draw,
        ]);

    }

    public function postFilePairs($draw_id)
    {
        $draw = TournamentDraw::with('tournament')->find($draw_id);
        $list_type = Input::get('list_type');
        $draw->manual_draw = 1;
        $draw->save();
        $items = Import::parseStrAsCsv(Input::get('bulk'));

        $licence_numbers = array_filter(array_pluck($items, 0));

        Import::saveSignupsFromArray($licence_numbers, $draw, $list_type); // save signups to database

        $signup_ids = TournamentTeam::whereIn('licence_number', $licence_numbers)
            ->whereHas('signup', function ($query) use ($draw_id) {
                $query->where('draw_id', $draw_id);
            })
            ->selectRaw("DISTINCT ON (tournament_teams.team_id) tournament_teams.team_id, tournament_teams.licence_number")
            ->orderBy('tournament_teams.team_id', 'desc')
            ->lists('licence_number', 'team_id');

        $data = Import::generateRoundsFromFile($items, $draw_id, $signup_ids, $list_type);


        $rounds_preparation = Draw::prepareTournament($data, $draw_id);

        Import::generateMatches($rounds_preparation, $list_type, $draw_id);

        return Response::json([
            'success' => true,
            'redirect_to' => '/draws/db-bracket/' . $draw_id . '?list_type=' . $list_type,
        ]);
    }

    /**
     * method for development remove signups from tournament_signups
     * @param $signup_id
     */
    public function getDelete($signup_id)
    {
        $signups = TournamentSignup::with('teams')
            ->get();

        foreach ($signups as $signup) {
            foreach ($signup->teams as $team) {
                $team->delete();
            }
            $signup->delete();
        }

        return Redirect::to('signups');

    }

    public function getDeleteSignup($signup_id)
    {
        $signup = TournamentSignup::find($signup_id);
        if (!$signup)
            return Redirect::to('signups');
        $draw_id = $signup->draw_id;
        $signup->delete();

        return Redirect::to('signups/register/' . $draw_id);
    }

    public function getSuperValidate($draw_id)
    {
        if(!Auth::getUser()->hasRole('superadmin'))
            Redirect::to('/signups/final-list/'.$draw_id);
        $validate = TournamentDraw::find($draw_id);
        $validate->approval_status = 3;
        $validate->save();

        $approvals_id = DrawApproval::where('draw_id', $draw_id)->first();
        $approvals = DrawApproval::find($approvals_id->id);
        $approvals->approval_status = 3;
        $approvals->save();

        return Redirect::to('/signups/final-list/'.$draw_id);
    }

    public function getRegionalValidate($draw_id)
    {
        if(!Auth::getUser()->hasRole('regional_admin'))
            Redirect::to('/signups/final-list/'.$draw_id);
        $validate = TournamentDraw::find($draw_id);
        $validate->approval_status = 2;
        $validate->save();

        $approvals_id = DrawApproval::where('draw_id', $draw_id)->first();
        $approvals = DrawApproval::find($approvals_id->id);
        $approvals->approval_status = 2;
        $approvals->save();

        $superadmins = User::join('users_roles', 'users.id', '=', 'users_roles.user_id')->where('users_roles.role_id', 1)->get();

        if ($superadmins) {
            foreach ($superadmins as $sadmin) {
                $message = LangHelper::get('draw_submitted_for_approval', 'Draw submitted for approval from regional admin');
                Notification::TournamentNotif($sadmin->user_id, $validate->tournament_id, 2, $message);
            }
        }

        return Redirect::to('/signups/draw-approval');
    }

    public function getDrawApproval() 
    {

       $regional_draws = TournamentDraw::join("tournaments", "tournament_draws.tournament_id", "=", "tournaments.id")->join ("clubs","tournaments.club_id", "=", "clubs.id")->join("regions", "regions.id", "=", "clubs.region_id")
           ->select('*', 'tournament_draws.id as id')
           ->where("regions.id", Auth::getUser()->getUserRegion())
           ->orderBy('tournaments.title')
            ->get();

        $this->layout->title = LangHelper::get('draw_approval', 'Draw approval');
        $this->layout->content = View::make('/signups/draw_approval', array(
                'regional_draws' => $regional_draws,

            ));
    }


}
