<?php

class ScheduleController extends BaseController
{

    public function getIndex($tournament_id)
    {
        $tournament = Tournament::with("draws")->find($tournament_id);
        $dates_array = TournamentHelper::getDrawDates($tournament->date, 0);
        $current_date = (Request::query('date')) ?: array_keys($dates_array)[0];
        $configuration = ScheduleConfiguration::loadConfiguration($tournament, $current_date);
        $draws_ids = array_pluck($tournament->draws, "id");

        $time_types = [null => 'Choose type of time'] + [
                '1' => 'Starting at',
                '2' => 'Not before',
                '3' => 'Followed by',
            ];

        $all_schedules = MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $schedules = $all_schedules->filter(function ($schedule) use ($current_date) {
            return ($schedule->date_of_play == $current_date) ? true : false;
        });

        $scheduled_matches = array_pluck($all_schedules->toArray(), 'match_id');
        $draw_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category')
            ->whereIn("draw_id", $draws_ids)
        ->where("completed", false);

        $all_matches = $draw_matches->get();

        if ($scheduled_matches)
            $draw_matches = $draw_matches->whereNotIn('id', $scheduled_matches);

        $exlude_query =  clone($draw_matches);
        $byes = $exlude_query->where("round_number", 1)->where(function($query){
            $query->where("team1_id", 0)->orWhere("team2_id", 0);
        })->get();

        $exlude_byes = array_pluck($byes->toArray(), 'id');

        $draw_matches = $draw_matches
            ->whereNotIn("id", $exlude_byes)
            ->orderBy('id', 'asc')
            ->get();

        if ($configuration->waiting_list)
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'waiting_list_row')) : 3;
        else
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'row')) : 3;

        $courts_dropdown = range(1, 12);

        $view_arr = [
            'number_of_rows' => $number_of_rows,
            'configuration'  => $configuration,
            'time_types'     => $time_types,
            'all_matches'    => $all_matches,
            'current_date'   => $current_date,
            'schedules'      => $schedules,
        ];

        $date_key = Request::query('date');
        if(!$date_key && $dates_array)
            $date_key = current(array_keys($dates_array));

        $schedule_view = ($configuration->waiting_list)
            ? View::make('schedule/_waiting_list', $view_arr)
            : View::make('schedule/_standard_list', $view_arr);

        $this->layout->title = "Create schedule";
        $this->layout->content = View::make('schedule/index', array(
            'draw_matches'    => $draw_matches,
            'tournament'      => $tournament,
            'dates_array'     => $dates_array,
            'time_types'      => $time_types,
            'schedules'       => $schedules,
            'current_date'    => $current_date,
            'all_matches'     => $all_matches,
            'all_schedules'   => $all_schedules,
            'number_of_rows'  => $number_of_rows,
            'configuration'   => $configuration,
            'courts_dropdown' => $courts_dropdown,
            'schedule_view'   => $schedule_view,
            'date_key'        => $date_key
        ));
    }

    public function postScheduleCheck($edit = false)
    {
        $date = Input::get('date_selected');
        $match_id = Input::get('match_id');
        $court_number = Input::get('court_number');
        $row = Input::get('row');
        $time_of_play = Input::get('time_of_play');
        $type_of_time = Input::get('type_of_time');
        $tournament_id = Input::get('tournament_id');
        $draw_id = Input::get('draw_id');
        $list_type = Input::get('list_type');


        $schedules = MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament_id)
            ->where('date_of_play', $date)
            ->get();

        $conf = ScheduleConfiguration::where('tournament_id', $tournament_id)
            ->where('date', $date)
            ->first();

        $match = DrawMatch::with('team1_data', 'team2_data')->find($match_id);

        if (DrawMatchHelper::checkIfMatchCantBePlayed($schedules, $match, $time_of_play, $type_of_time, $row)) {
            return Response::json([
                'message' => LangHelper::get('this_match_cant_be_played_now', 'This match can\'t be played now'),
                'class'   => 'state-error',
                'success' => false,
            ]);
        }
        $message = "";
        $class = "state-accepted";
        if (DrawMatchHelper::checkIfPlayerHasMoreMatches($schedules, false, $match)) {
            $message = LangHelper::get('warning_player_has_more_than_one_match', 'Warning: player has more than one match');
            $class = 'state-warning';
        }

        $data_for_time = [
            'type_of_time' => $type_of_time,
            'time_of_play' => $time_of_play,
        ];

        if ($conf->waiting_list) {
            $sc = MatchSchedule::with('tournament')
                ->whereTournamentId($tournament_id)
                ->whereDateOfPlay($date)
                ->orderBy('row', 'desc')
                ->orderBy('court_number', 'desc')
                ->first();
        }

        if ($edit) {
            $schedule = MatchSchedule::whereMatchId($match_id)->first();

            if ($type_of_time OR $time_of_play)
                $schedule->time_of_play = DrawMatchHelper::getScheduleTime($data_for_time);
            if ($type_of_time) {
                $schedule->type_of_time = $type_of_time;
                if ($conf->waiting_list)
                    $schedule->waiting_list_type_of_time = $type_of_time;
            }

            if ($time_of_play)
                $schedule->schedule_time = $time_of_play;

            if ($conf->waiting_list)
                $schedule->waiting_list_time = $time_of_play;

            if ($court_number) {
                if ($conf->waiting_list) {
                    $schedule->court_number = ScheduleHelper::positionCalculator('court_number', $sc);
                    $schedule->waiting_list_court_number = $court_number;
                } else
                    $schedule->court_number = $court_number;
            }

            if ($row) {
                if ($conf->waiting_list) {
                    $schedule->row = ScheduleHelper::positionCalculator('row', $sc);
                    $schedule->waiting_list_row = $row;
                } else
                    $schedule->row = $row;
            }

            $schedule->save();
        } else {
            MatchSchedule::where("match_id", $match_id)->delete();
            $match_schedule = new MatchSchedule();
            $match_schedule->match_id = isset($match_id) ? $match_id : 0;
            $match_schedule->date_of_play = $date;

            if ($court_number) {
                if ($conf->waiting_list) {
                    $match_schedule->court_number = ScheduleHelper::positionCalculator('court_number', $sc);
                    $match_schedule->waiting_list_court_number = $court_number;
                } else
                    $match_schedule->court_number = $court_number;
            }

            if ($row) {
                if ($conf->waiting_list) {
                    $match_schedule->row = ScheduleHelper::positionCalculator('row', $sc);
                    $match_schedule->waiting_list_row = $row;
                } else
                    $match_schedule->row = $row;
            }

            $match_schedule->time_of_play = DrawMatchHelper::getScheduleTime($data_for_time);
            $match_schedule->type_of_time = $type_of_time;
            $match_schedule->schedule_time = $time_of_play;
            $match_schedule->draw_id = $draw_id;
            $match_schedule->list_type = $list_type;
            $match_schedule->tournament_id = $tournament_id;
            $match_schedule->save();
        }

        return Response::json(array("success" => true, "message" => $message, 'class' => $class));
    }

    public function postUpdateConfiguration()
    {
        $date = Input::get('date_selected');
        $tournament_id = Input::get('tournament_id');

        $conf = ScheduleConfiguration::with('tournament')->where("date", $date)
            ->whereTournamentId($tournament_id)
            ->first();

        $conf->waiting_list = $conf->tournament->number_of_courts > Input::get('number_of_courts') + 1 ? true : false;
        $conf->number_of_courts = Input::get('number_of_courts') + 1; // posto se koristi range za generisanje dropdowna key start je od 0 zbog toga dodajemo po 1
        $conf->save();

        if ($conf->tournament->number_of_courts > $conf->number_of_courts)
            ScheduleHelper::generateWaitingList($conf);

        return Response::json([
            'success'     => true,
            'redirect_to' => '/schedule/index/' . $tournament_id . '?date=' . $date,
        ]);
    }

    public function getConfiguration($tournament_id)
    {
        $tournament = Tournament::with("draws")->find($tournament_id);
        $dates_array = TournamentHelper::getDrawDates($tournament->date, 0);

        $configurations = ScheduleConfiguration::createInitialConfigurations($tournament, $dates_array);

        $this->layout->title = "Edit schedule configurations";
        $this->layout->content = View::make('schedule/configuration', array(
            'tournament'     => $tournament,
            'dates_array'    => $dates_array,
            'configurations' => $configurations,
            'number_range'   => array_combine(range(1, 24), range(1, 24)),
        ));
    }

    public function postConfiguration($tournament_id)
    {
        $inputs = Input::all();
//echo Debug::vars($inputs);die;
        foreach ($inputs as $input) {

            $conf = ScheduleConfiguration::with('tournament')->where("date", $input['date'])
                ->whereTournamentId($tournament_id)
                ->first();
            $conf->rest_time = ($input['rest_time']) ?: 120;
            $conf->number_of_courts = ($input['number_of_courts']) ?: 4;
            $conf->waiting_list = $conf->tournament->number_of_courts > Input::get('number_of_courts') ? true : false;
            $conf->save();
        }

        return Response::json([
            'success'     => true,
            'redirect_to' => '/schedule/index/' . $tournament_id,
            'message'     => LangHelper::get('configurations_saved', 'Configurations saved'),
        ]);

    }

    public function getWaitingList($tournament_id)
    {
        $conf = ScheduleConfiguration::whereTournamentId($tournament_id)
            ->where("date", Request::query('date'))
            ->first();

        $conf->waiting_list = ($conf->waiting_list) ? 'false' : true;
        $conf->save();

        if ($conf->waiting_list)
            ScheduleHelper::generateWaitingList($conf);

        return Redirect::to('/schedule/index/' . $tournament_id . '?date=' . Request::query('date'));
    }

    public function deleteSchedule($match_id)
    {
        MatchSchedule::where('match_id', $match_id)->delete();

        return Response::json([
            'success' => true,
        ]);
    }

    public function getMatchesData($tournament_id)
    {
        $tournament = Tournament::with("draws")->find($tournament_id);
        $draws_ids = array_pluck($tournament->draws, "id");

        $all_schedules = MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $scheduled_matches = array_pluck($all_schedules->toArray(), 'match_id');
        $draw_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category')
            ->whereIn("draw_id", $draws_ids);

        $all_matches = $draw_matches->get();

        if ($scheduled_matches)
            $draw_matches = $draw_matches->whereNotIn('id', $scheduled_matches);

        $draw_matches = $draw_matches
            ->orderBy('id', 'asc')
            ->get();

        $this->layout = null;

        return View::make('schedule/_match_list', [
            'draw_matches' => $draw_matches,
            'all_matches'  => $all_matches,
        ]);
    }

    public function getShow($tournament_id)
    {
        $tournament = Tournament::with("draws")->find($tournament_id);
        $dates_array = TournamentHelper::getDrawDates($tournament->date, 0);
        $current_date = (Request::query('date')) ?: array_keys($dates_array)[0];
        $configuration = ScheduleConfiguration::loadConfiguration($tournament, $current_date);

        $all_schedules = MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $schedules = $all_schedules->filter(function ($schedule) use ($current_date) {
            return ($schedule->date_of_play == $current_date) ? true : false;
        });

        $draws_ids = array_pluck($tournament->draws, "id");
        $all_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category')
            ->whereIn("draw_id", $draws_ids)
            ->get();

        if ($configuration->waiting_list)
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'waiting_list_row')) : 3;
        else
            $number_of_rows = ($schedules->toArray()) ? max(array_pluck($schedules->toArray(), 'row')) : 3;

        $results_form_view = View::make('draws/add_results');

        $this->layout->title = 'Schedule preview';
        $this->layout->content = View::make('schedule/show', [
            'tournament'     => $tournament,
            'dates_array'    => $dates_array,
            'schedules'      => $schedules,
            'current_date'   => $current_date,
            'all_schedules'  => $all_schedules,
            'configuration'  => $configuration,
            'number_of_rows' => $number_of_rows,
            'all_matches'    => $all_matches,
            'results_form_view' => $results_form_view
        ]);


    }

    public function getOrderPrint ($tournament_id)
     {
        
        $tournament = Tournament::with("draws")
            ->whereId($tournament_id)
            ->first();

        $configurations = ScheduleConfiguration::loadAllConfigurations($tournament);

        $schedules = MatchSchedule::with('match.team1_data', 'match.team2_data')
            ->where('tournament_id', $tournament->id)
            ->get();

        $matches = ScheduleHelper::sortByPlayer($schedules, $configurations);

        $this->layout->title = \LangHelper::get('view_per_player', 'View per player');
        $this->layout->content = View::make('schedule/player_list', [
            'tournament'   => $tournament,
            'matches'      => $matches,
        ]);

     }


}