<?php

class InvoicesController extends BaseController
{

    public function getIndex()
    {
        $invoices = Invoice::with('data')->getInvoicesByRole();

        $this->layout->title = LangHelper::get('invoices', 'Invoices');
        $this->layout->content = View::make('invoices/index', [
            'invoices' => $invoices,
        ]);
    }

    public function getUpdate($invoice_id)
    {
        $invoice = Invoice::with('data')->find($invoice_id);

        $this->layout->title = LangHelper::get('invoice_update', 'Invoice update');
        $this->layout->content = View::make('invoices/update', [
            'invoice' => $invoice,
        ]);
    }

    public function getData($invoice_id)
    {
        $invoice = Invoice::with('payments')->find($invoice_id);
        $data = $invoice->generateAngularData();

        return Response::json([
            'data' => $data
        ]);
    }

    public function putUpdate($invoice_id)
    {
        $rules = array(
            'invoice.paid' => array('required'),
        );
        $validator = Validator::make(array_dot(Input::all()), $rules);
        $data = Input::get('invoice');

        if ($validator->passes()) {
            $invoice = Invoice::with('payments')->find($invoice_id);
//            $paid = HtmlHelper::numberFormat($data['paid']);
            $paid = $data['paid'];
            $invoice->paid += $paid;

            if($invoice->paid > $invoice->total(false))
            {

                $data = Invoice::with('payments')->find($invoice_id)->generateAngularData();
                return Response::json([
                    'data'    => $data,
                    'show_message' => true,
                    'success' => FALSE,
                    'message' => LangHelper::get('total_payment_is_bigger_than_needed', 'Total payment is bigger than needed'),
                ]);
            }

            PaymentHistory::createNewPayment($invoice_id, $paid);
            $invoice->status = $invoice->calculateStatus();
            $invoice->save();

            $data = Invoice::with('payments')->find($invoice_id)->generateAngularData();

            return Response::json([
                'data'    => $data,
                'success' => true,
                'message' => LangHelper::get('form_is_successfully_updated', 'Form is successfully updated'),
            ]);
        }

        return $validator->messages()->toJson();
    }

    public function getDetails($invoice_id)
    {
        $invoice = Invoice::with('data', 'payments')->find($invoice_id);

        $this->layout->title = LangHelper::get('invoice_details', 'Invoice details');
        $this->layout->content = View::make('invoices/details', [
            'invoice' => $invoice,
        ]);
    }

    public function getPdf($invoice_id)
    {
        $invoice = Invoice::with('data')->find($invoice_id);
        $this->layout = false;
        $data = [
            'invoice' => $invoice,
        ];
        $pdf = PDF::loadView('invoices.pdf', $data);
        return $pdf->stream('invoice.pdf');

        $view = View::make('invoices/pdf', [
            'invoice' => $invoice,
        ]);

        return $view;
    }
} 