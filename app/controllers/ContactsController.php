<?php

class ContactsController extends BaseController
{

    public function getIndex()
    {
        $filter_array = [
            'this week' => LangHelper::get('this_week', 'This week'),
            'next week' => LangHelper::get('next_week', 'Next week'),
            '+1 week'   => LangHelper::get('next_2_weeks', 'Next 2 weeks'),
        ];

        $this->layout->title = LangHelper::get('contacts', "Contacts");
        $this->layout->content = View::make('contacts/index', array(
            'filter_array' => $filter_array,
        ));
    }

    public function getData()
    {
        $organizers = [];
        if (Auth::getUser()->hasRole('referee')) {
            $organizers = TournamentHelper::calculateByWeeks('', 'get', 'Organizer');
        } else {
            if (Request::query('search')) {
                if (TournamentHelper::calculateByWeeks(Request::query('select'), 'count', 'Organizer'))
                    $organizers = TournamentHelper::calculateByWeeks(Request::query('select'), 'get', 'Organizer');
            } else {
                if (TournamentHelper::calculateByWeeks('this week', 'count', 'Organizer'))
                    $organizers = TournamentHelper::calculateByWeeks('this week', 'get', 'Organizer');
            }
        }
        $organizers = (is_object($organizers)) ? $organizers->toArray() : $organizers;
        return Response::json(['data' => $organizers])->setCallback(Request::get('callback'));
    }


}
