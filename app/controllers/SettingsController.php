<?php
use Symfony\Component\Console\Output\StreamOutput;

class SettingsController extends BaseController
{

    public function getIndex()
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $this->layout->title = "Settings";
        $this->layout->content = View::make('settings/index', array());
    }

    public function getCategories()
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);


        $categories = DrawCategory::orderBy("id")->get();

        $this->layout->title = "Category Settings";
        $this->layout->content = View::make('settings/categories', array(
            'categories' => $categories
        ));

    }

    public function postCategories()
    {
        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $posts = Input::except('_token');
        foreach ($posts as $key => $value) {

            $update = DrawCategory::where("name", $key)->first();
            $update->range_from = $value['from'];
            $update->range_to = $value['to'];
            $update->range_for_points_from = $value['points_from'];
            $update->range_for_points_to = $value['points_to'];
            $update->range_for_signup_from = $value['signup_from'];
            $update->range_for_signup_to = $value['signup_to'];
            $update->save();
        }

        return Redirect::to("settings/categories");
    }

    public function getRecalculateRanking()
    {
        return PermissionHelper::noPermissionPage($this);

        if (!Auth::getUser()->hasRole("superadmin"))
            return PermissionHelper::noPermissionPage($this);

        $this->layout = false;
        DB::disableQueryLog();

        $email = '';
        $history = TournamentHistory::where('points_assigned', false)->count();
        if ($history == 0)
            $taking_long = false;
        else {
            $taking_long = true;
            $email = Auth::user()->email;
        }

        Artisan::call("recalculate:do", array('email' => $email));

        return Response::json(['success' => true, 'taking_long' => $taking_long]);

    }

    public function getCoefficient()
    {
        $coef = Config::get('coefficient.param') * 100;

        $this->layout->title = "Coefficient Settings";
        $this->layout->content = View::make('settings/coefficient', array(
            'coef' => $coef,
        ));
    }

    public function putCoefficient()
    {
        $rules = array(
            'coefficient' => array('required'),
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $coef = Input::get('coefficient') / 100;

            $items = "'param' => " . $coef;

            $file = app_path() . '/config/coefficient.php';
            file_put_contents($file, '<?php return array(' . $items . ');');

            return Response::json([
                'success' => true,
                'message' => LangHelper::get('coefficient_updated', 'Coefficient updated'),
                'redirect_to' => '/settings',
            ]);
        } else {
            return $validator->messages()->toJson();
        }
    }
}
