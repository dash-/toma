<?php

class ProfileController extends BaseController {

	public function getIndex()
	{
		$user = Auth::getUser();

		$this->layout->title = LangHelper::get('profile', 'Profile');
		$this->layout->content = View::make('profile/index', array(
			'user' => $user,
		));
	}

	public function getEdit()
	{
		$user = Auth::getUser();

		$langs_array = $user->profileLangDrodown();

		$this->layout->title = LangHelper::get('edit_profile', 'Edit profile');
		$this->layout->content = View::make('profile/edit', array(
			'user'        => $user,
			'langs_array' => $langs_array,
		));
	}

	public function putEdit()
	{
		$user = Auth::getUser();
		$contact = $user->contact->first();

		$rules = array(
			'contacts.name'    => array('required'),
			'contacts.surname' => array('required'),
		);

		$contact_data = Input::get('contacts');
		$user_data = Input::get('user');

		$validator = Validator::make(array_dot(Input::all()), $rules);
		
		if ($validator->passes())
		{
			if (!is_null($contact)) {
				$contact->update($contact_data);
			} else {
				$contact = Contact::create($contact_data);

				$admin_table = new Admin;
				$admin_table->contact_id = $contact->id;
				$admin_table->user_id = $user->id;
				$admin_table->save();
			}

			$user->language_id = isset($user_data['language_id']) ? $user_data['language_id'] : NULL;
			$user->save();

			// after successfull save add language to session
			Session::put('my.locale', $user->language->code);
			
			return Response::json([
				'success' => 'true',
            	'redirect_to' => URL::to('profile'),
			]);
		}
		else
		{
			return $validator->messages()->toJson();
		}
	}

	public function getChangePassword()
	{
		$user = Auth::getUser();

		
		$this->layout->title = LangHelper::get('change_password', 'Change password');
		$this->layout->content = View::make('profile/change_password', array(
			'user' => $user,
		));
	}

	public function postChangePassword()
	{
		$rules = array(
	        'old_password' => 'required',
	        'password' => 'required|confirmed'
	    );

	    $user = Auth::getUser();

	    $validator = Validator::make(Input::all(), $rules);

	    if ($validator->passes()) 
	    {
	        if (!Hash::check(Input::get('old_password'), $user->password)) 
	        {
	            return Response::json(array('not_match' => array('Your old password does not match')));
	        }
	        else
	        {
	            $user->password = Hash::make(Input::get('password'));
	            $user->save();
	            return Response::json([
	            	'success' => true,
	            ]);
	        }
	    } 
	    else 
	    {
	    	return $validator->messages()->toJson();
	    }
	}

	public function getUpload()
	{
		$user = Auth::getUser();

		$this->layout->title = LangHelper::get('upload_image', 'Upload image');
		$this->layout->content = View::make('profile/upload', array(
			'user' => $user,
		));
	}


	public function postUpload()
	{
		$user = Auth::getUser();

		$rules = array(
			'image' => 'required',
		);

		$image = Input::file('image');
		if ($image)
			$filename = Str::random().'_'.$image->getClientOriginalName();

		$save_path = public_path().'/uploads/images';

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->passes())
		{
			if ($image) {
				Image::make($image)->save($save_path.'/user/full/'.$filename);
				Image::make($image)->resize(300, 200)->save($save_path.'/user/thumbnails/'.$filename);
			}

			$user->image = $filename;
			$user->save();

			return Redirect::to('profile');
		}
		else
		{
			return $validator->messages()->toJson();
		}
	}

}
