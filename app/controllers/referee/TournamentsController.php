<?php

namespace Referee;

use BaseController, View, Input, Response, Auth, Request;


class TournamentsController extends BaseController
{

    public function getIndex()
    {

//        $year = (Request::query('year')) ? Request::query('year') : 'Y';
//        $month = (Request::query('month')) ? Request::query('month') : '';
//        $status = (Request::query('status')) ? Request::query('status') : '';

        $tournaments = \Tournament::with('draws', 'draws.referees', 'date')
            ->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
            ->where(function ($query) {
                $query->whereStatus(2)->orWhere('created_by', Auth::user()->id);
            })
            ->orderBy('tournament_dates.main_draw_from', 'DESC')
            ->select("*", "tournaments.id as id");

        $year = (Request::query('year')) ? Request::query('year') : 'Y';
        $month = (Request::query('month')) ? Request::query('month') : '';
        $status = (Request::query('status')) ? Request::query('status') : '';
        $title = (Request::query('title')) ? Request::query('title') : '';

        if (Request::query('search')) {
            if (Request::query('month')) {
                // if year is submitted in query set that year if not set current year

                $from_date = date($year . '-' . $month . '-01');
                $to_date = date($year . '-' . $month . '-t');

                $formatted_from = new DateTime($from_date);
                $formatted_to = new DateTime($to_date);

                // $tournaments = $tournaments->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
                $tournaments = $tournaments->whereBetween('tournament_dates.main_draw_from', array($formatted_from->format('Y-m-d H:i:s'), $formatted_to->format('Y-m-d H:i:s')));

                if (Auth::user()->hasRole('superadmin'))
                    $tournaments = $tournaments->select("*", "tournaments.id as id");

            }
            if ($status)
                $tournaments = $tournaments->where('tournaments.status', Request::query('status'));

            if ($title)
                $tournaments = $tournaments->where('tournaments.title', 'ILIKE', "%" . Request::query('title') . "%");
        }
        $tournaments = $tournaments->get();


        $this->layout->title = 'List of all tournaments';
        $this->layout->content = View::make('tournaments/index', array(
            'tournaments' => $tournaments,
            'year' => $year,
            'month' => $month,
            'status' => $status,
            'role' => 'referee'

        ));
    }


    public function getAssigned()
    {
        $year = (Request::query('year')) ? Request::query('year') : 'Y';
        $month = (Request::query('month')) ? Request::query('month') : '';
        $status = (Request::query('status')) ? Request::query('status') : '';

        $tournaments = \Tournament::with('draws', 'draws.referees')
            ->whereRefereeId(Auth::user()->id)
            ->whereStatus(2)
            ->get();

        $this->layout->title = 'List of assigned tournaments';
        $this->layout->content = View::make('tournaments/index', array(
            'head_title' => \LangHelper::get("assigned_tournaments", "Assigned tournaments"),
            'tournaments' => $tournaments,
            'year' => $year,
            'month' => $month,
            'status' => $status,
            'show_tournament_settings_button' => true,
            'role' => 'referee'
        ));
    }


    public function getDrawDetails($draw_id)
    {
        $draw = \TournamentDraw::with('tournament', 'category', 'size')
            ->find($draw_id);

        $this->layout->title = 'Draw details';
        $this->layout->content = View::make('referee/tournaments/draw_details', array(
            'draw' => $draw
        ));
    }

    public function postApply($draw_id)
    {
        $draw = \TournamentDraw::with('tournament')
            ->find($draw_id);


        $application = new \DrawReferee;
        $application->draw_id = $draw_id;
        $application->user_id = \Auth::user()->id;
        $application->status = 0;

        if ($application->save()) {

            \DrawRefereeHistory::saveHistory($application);

            $message = 'Refeere applied for draw on tournament ' . $draw->tournament->title;

            \Notification::RefereeNotif(Auth::user()->id, $message);

            return Response::json([
                'success' => true,
            ]);

        }

        return Response::json([
            'success' => false,
        ]);
    }

    public function postCancel($draw_id)
    {
        $draw = \TournamentDraw::with('tournament')
            ->find($draw_id);

        $application = \DrawReferee::whereDrawId($draw_id)
            ->whereUserId(Auth::user()->id)
            ->first();

        $application->status = 3;

        if ($application->save()) {

            \DrawRefereeHistory::saveHistory($application);

            $message = 'Refeere canceled application for draw on tournament ' . $draw->tournament->title;

            \Notification::RefereeNotif(Auth::user()->id, $message);

            return Response::json([
                'success' => true,
            ]);
        }

        return Response::json([
            'success' => false,
        ]);
    }

    public function getTournament($tournament_id)
    {
        $tournament = \Tournament::find($tournament_id);
        $draws_count = \TournamentDraw::whereTournamentId($tournament_id)->count();
        $signups_count = \TournamentSignup::whereTournamentId($tournament_id)->count();
        $live_score = \DrawMatchHelper::getLiveScores($tournament_id);
        $live_score_main = \DrawMatchHelper::getLiveScores($tournament_id, 1);
        $assigned_tournaments = \TournamentHelper::assignedTournaments();
        $single_draw_id = '';
        if ($draws_count == 1) {
            if ($tournament->draws())
                $single_draw_id = $tournament->draws()->first()->id;
        }

        $this->layout->title = $tournament->title;
        $this->layout->content = View::make('referee.tournaments.single_tournament', array(
            'tournament' => $tournament,
            'draws_count' => $draws_count,
            'signups_count' => $signups_count,
            'live_score' => $live_score,
            'live_score_main' => $live_score_main,
            'assigned_tournaments' => $assigned_tournaments,
            'single_draw_id' => $single_draw_id,
        ));
    }


    public function getListOfDraws($tournament_id, $next_action, $draw_type = 1)
    {
        $tournament = \Tournament::find($tournament_id);
        $draws = \TournamentDraw::with("category")->whereTournamentId($tournament_id)->get();
        $this->layout->title = \LangHelper::get('referees', "Referees");

        $link = '/';
        switch ($next_action) {
            case 'seeds':
                $link = '/draws/manual-bracket/{0}?list_type=' . $draw_type;
                break;
            case 'schedule':
                $link = '/schedule/show/' . $tournament_id;
                break;
            case 'show-schedule':
                $link = '/schedule/show/' . $tournament_id;
                break;
            case 'pay-pal':
                $link = '/signups/list/{0}';
                break;

        }

        $this->layout->content = View::make('referee.tournaments.list_of_draws', array(
            'tournament' => $tournament,
            'draws' => $draws,
            'link' => $link

        ));
    }

    public function getScores($tournament_id)
    {
        $in_progress = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereTournamentId($tournament_id)
            ->where('match_status', 3)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();


        $finished = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereTournamentId($tournament_id)
            ->where('match_status', 4)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();


        $not_started = \MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->whereTournamentId($tournament_id)
            ->where('match_status', 0)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->get();

        $this->layout->title = \LangHelper::get('scores', "Scores");
        $this->layout->content = View::make('/scores/index', array(
            'in_progress' => $in_progress,
            'finished' => $finished,
            'not_started' => $not_started,
        ));

    }
}

