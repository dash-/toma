<?php

class RegionsController extends BaseController {
	
	public function getIndex()
	{
		$regions = Region::all();

		$this->layout->title = LangHelper::get('regions', 'Regions');
		$this->layout->content = View::make('regions/index', array(
			'regions' => $regions,
		));
	}

	public function getEdit($id)
	{
		$region = Region::findOrFail($id);

		$this->layout->title = 'Edit region';
		$this->layout->content = View::make('regions/edit', array(
			'region' => $region,
		));
	}

	public function putEdit($id)
	{
		$region = Region::findOrFail($id);

		$rules = array(
            'regions.region_name' => array('required'),
        );

        $validator = Validator::make(array_dot(Input::all()), $rules);

		$region_data = Input::get('regions');

		if ($validator->passes())
        {
            $region = $region->update($region_data);

            return Response::json(array('success' => 'true'));
        }
        else
        {
            return $validator->messages()->toJson();
        }
	}

    public function getInfo($id)
    {
    	$regions = Region::findOrFail($id);
        $this->layout->title = LangHelper::get('regions', 'Regions');
        $this->layout->content = View::make('regions/info', array(
            'regions' => $regions,
        ));
    }


    public function getPosition($region_id)
    {
        $region = Region::find($region_id);
//        echo Debug::vars($region);die;
        $this->layout->title = LangHelper::get('regions', 'Regions');
        $this->layout->content = View::make('clubs/position', array(
            'club' => $region,
        ));
    }

    public function postPosition($region_id){

        $region = Region::find($region_id);
        $latitude = Input::get("latitude");
        $longitude = Input::get("longitude");

        if($latitude && $longitude)
        {
            $region->position_lat = $latitude;
            $region->position_long = $longitude;
            $region->save();
        }

        return Redirect::to('regions/position/'.$region->id);
    }
}