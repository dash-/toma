<?php
/**
 * Created by PhpStorm.
 * User: damirseremet
 * Date: 27/08/14
 * Time: 17:25
 */

class CreateTournamentPageController extends BaseController{
    public function getIndex()
    {
        $this->layout->title = 'Create Tournament';
        $this->layout->content = View::make('tournaments/createtournaments/index', array(
            //'tournaments' => $tournaments,
        ));
    }
} 