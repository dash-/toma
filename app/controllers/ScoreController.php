<?php

class ScoreController extends BaseController
{

    public function getIndex()
    {
        if (Auth::getUser()->hasRole("referee")) {
            list($in_progress, $finished, $not_started, $in_progress_matches, $finished_matches, $not_started_matches) = TournamentScoresHelper::getRefereeLiveScoreAssignedTournaments();
        } else {

            $only_in_progress = false;

            $in_draw_ids = [];
            $f_draw_ids = [];
            $not_draw_ids = [];

            $in_progress = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data',
                'match.scores', 'tournament', 'tournament.club.province')
                ->whereHas('match', function ($query) {
                    $query->whereNotNull('id');
                })
                ->take(10);

            if(!$only_in_progress) {
                $in_progress = $in_progress->where('match_status', 3)->get();
            }
            else {
                $in_progress = $in_progress->where('date_of_play','>', DateTimeHelper::GetDateNow())->get();
            }

            $in_progress = $in_progress->filter(function($item) {
                if ($item->match->team1_id >0 OR $item->match->team2_id >0 )
                    return true;
            });

            foreach ($in_progress as $in) {
                $in_draw_ids[] = $in->match->id;
            }

            $in_progress_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
            if (count($in_draw_ids))
                $in_progress_matches = $in_progress_matches->whereIn('draw_id', $in_draw_ids);

            $in_progress_matches = $in_progress_matches->get();

            $finished = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data',
                'match.scores', 'tournament', 'tournament.club.province')
                ->where('match_status', 4)
                ->whereHas('match', function ($query) {
                    $query->whereNotNull('id');
                })
                ->take(10)
                ->get();

            $finished = $finished->filter(function($item) {
                if ($item->match->team1_id >0 OR $item->match->team2_id >0 )
                    return true;
            });

            foreach ($finished as $f) {
                $f_draw_ids[] = $f->match->id;
            }

            $finished_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
            if (count($f_draw_ids))
                $finished_matches = $finished_matches->whereIn('draw_id', $f_draw_ids);

            $finished_matches = $finished_matches->get();


            $not_started = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data',
                'match.scores', 'tournament', 'tournament.club.province')
                ->where('match_status', 0)
                ->whereHas('match', function ($query) {
                    $query->whereNotNull('id');
                })
                ->orderBy('court_number')
                ->take(10)
                ->get();


            $not_started = $not_started->filter(function($item) {
               if ($item->match->team1_id >0 OR $item->match->team2_id >0 )
                   return true;
            });

            foreach ($not_started as $not) {
                $not_draw_ids[] = $not->match->id;
            }

            $not_started_matches = DrawMatch::with("team1_data", "team2_data", "draw", 'draw.category');
            if (count($not_draw_ids))
                $not_started_matches = $not_started_matches->whereIn('draw_id', $not_draw_ids);

            $not_started_matches = $not_started_matches->get();
        }

        $this->layout->title = LangHelper::get('scores', "Scores");
        $this->layout->content = View::make('/scores/index', array(
            'in_progress'         => $in_progress,
            'finished'            => $finished,
            'not_started'         => $not_started,
            'finished_matches'    => $finished_matches,
            'in_progress_matches' => $in_progress_matches,
            'not_started_matches' => $not_started_matches,
        ));

    }

    public function getRefereeHomeScores()
    {
        $in_progress = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->where('match_status', 3)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->take(10)
            ->get();

        $finished = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->where('match_status', 4)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->take(10)
            ->get();


        $not_started = MatchSchedule::with('match', 'match.team1_data', 'match.team2_data', 'match.scores', 'tournament', 'tournament.club.province')
            ->where('match_status', 0)
            ->whereHas('match', function ($query) {
                $query->whereNotNull('id');
            })
            ->take(10)
            ->get();

        $this->layout->title = LangHelper::get('scores', "Scores");
        $this->layout->content = View::make('/scores/index', array(
            'in_progress' => $in_progress,
            'finished'    => $finished,
            'not_started' => $not_started,
        ));
    }

    public function getPlayerScores($match_id)
    {
        $this->layout = null;
        $scores = MatchScore::where('match_id', $match_id)
            ->get();

        $result = '';

        foreach ($scores as $score) {
            $result .= $score->set_team1_score . DrawMatchHelper::matchTie($score->set_team1_tie) . '-' . $score->set_team2_score . DrawMatchHelper::matchTie($score->set_team2_tie) . '; ';
        }

        return $result;
    }

    public function getLiveScores()
    {
        $date = DateTimeHelper::GetPostgresDate();
        $tournaments = Tournament::with('schedules.match', 'schedules.match.team1', 'schedules.match.team2', 'schedules.match.team1_data', 'schedules.match.team2_data')
            ->whereHas('schedules', function ($query) use ($date) {
                $query->whereRaw("date_of_play::date = '" . $date . "'");
            })->get();


        $this->layout->title = LangHelper::get('live_scores', 'Live Scores');
        $this->layout->content = View::make('/scores/livescore', array(
            'tournaments' => $tournaments
        ));
    }

    public function getAdminView($tournament_id)
    {
        $tournament = Tournament::with('draws', 'draws.category', 'draws.matches', 'draws.matches.team1_data', 'draws.matches.team2_data', 'draws.matches.scores')
            ->find($tournament_id);
        $draws = $tournament->draws->filter(function ($draw) {
            return $draw->matches->count() ? true : false;
        });

        $this->layout->title = LangHelper::get('live_scores_administration', 'Live scores administration');
        $this->layout->content = View::make('/scores/admin_view', array(
            'tournament' => $tournament,
            'draws'      => $draws,
        ));
    }

}
