<?php

namespace Superadmin;

use Rfet\Storage\User\UserRepositoryInterface as User;
use BaseController, View, Input, Hash;


class UsersController extends BaseController
{

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getIndex()
    {
        if (!\Auth::getUser()->hasRole("superadmin"))
            return \PermissionHelper::noPermissionPage($this);

        $users = $this->user->allWithRoles();

        $this->layout->title = 'Users administration';
        $this->layout->content = View::make('superadmin/users/index', array(
            'users' => $users
        ));
    }

    public function getPlayers()
    {
        $items_per_page = array(
            10  => 10,
            20  => 20,
            30  => 30,
            50  => 50,
            100 => 100,
        );

        $this->layout->title = 'Players';
        $this->layout->content = View::make('superadmin/users/players', array(
            'items_per_page' => $items_per_page,
        ));
    }

    public function getPlayersData()
    {
        $items = \Request::query('items_counter');
        $order = \Request::query('order');
        $sort = \Request::query('sort');

        $query = \Request::query();

        if (\Request::query('search') == 1) {
            $posts = \Request::query();

            $query = \Player::leftJoin('contacts', function ($join) {
                $join->on('players.contact_id', '=', 'contacts.id');
            });
            foreach ($posts as $key => $post) {
                $key = str_replace("INT_", "", $key, $int);

                if ($post AND !in_array($key, array('items_counter', 'order', 'sort', 'search', 'callback', 'page'))) {
                    if ($int)
                        $query->where($key, '=', $post);
                    else
                        $query->where($key, 'ILIKE', "%" . $post . "%");
                }
            }

            $players = $query->orderBy($order, $sort)->paginate($items);

            return \Response::json($players->toArray())->setCallback(\Request::get('callback'));;
        }

        $players = \Player::leftJoin('contacts', function ($join) {
            $join->on('players.contact_id', '=', 'contacts.id');
        })->orderBy($order, $sort)->paginate($items);

        return \Response::json($players->toArray())->setCallback(\Request::get('callback'));
    }

    public function postSearch()
    {
        $posts = \Input::all();

        $query = \Player::leftJoin('contacts', function ($join) {
            $join->on('players.contact_id', '=', 'contacts.id');
        });
        foreach ($posts as $key => $post) {
            $key = str_replace("INT_", "", $key, $int);

            if ($post) {
                if ($int)
                    $query->where($key, '=', $post);
                else
                    $query->where($key, 'ILIKE', "%" . $post . "%");
            }
        }

        // $players = $query->paginate(10);
        return \Response::json($query->get()->toArray());
    }

    public function getInfo($id)
    {
        $user = $this->user->find($id);

        $this->layout->title = 'User informations ' . $user->usename;
        $this->layout->content = View::make('superadmin/users/info', array(
            'user' => $user,
        ));


    }

    public function getEdit($id)
    {
        if (!\Auth::getUser()->hasRole("superadmin"))
            return \PermissionHelper::noPermissionPage($this);

        $user = \User::with('roles', 'region')->whereId($id)->first();
        /*dd($user->region->firstOrFail()->region_name);*/
        $contact = $user->contact->first();
//        $roles = array(null => 'Choose role') + \Role::whereNotIn('id', [5, 6, 2, 7])->get()->lists('name', 'id');

        $roles = array($user->roles->first()->id => $user->roles->first()->name) + \Role::whereNotIn('id', [5, 6, 2, 7])->get()->lists('name', 'id');

        $user_region = $user->region->first();
        $primary = [];
        if ($user_region) {
            $primary = [
                $user_region->id => $user_region->region_name,
            ];
        } else {
            $primary = ['' => "Select user region"];
        }

        $regions = $primary + \Region::orderBy('region_name')->lists('region_name', 'id');


        $this->layout->title = 'Edit user ' . $user->usename;
        $this->layout->content = View::make('superadmin/users/edit', array(
            'user'    => $user,
            'roles'   => $roles,
            'contact' => $contact,
            'regions' => $regions,
        ));
    }

    public function putEdit($id)
    {
        $user = \User::with('roles', 'region')->whereId($id)->first();
        $contact = $user->contact->first();

        $rules = array(
            'contact.name'    => array('required'),
            'contact.surname' => array('required'),
            'users.username'  => array('required'),
            'users.email'     => array('required', 'email'),
            'roles.role'      => array('required'),
            'users.password'  => array('required'),
        );

        $validator = \Validator::make(array_dot(Input::all()), $rules);

        $user_data = Input::get('users');
        $contact_data = Input::get('contact');
        $roles_data = Input::get('roles');
        $region_data = Input::get('region');

        $UserRegion = \UserRegion::where('user_id', '=', $id)->first();

        $UserRegionId = ($UserRegion) ? $UserRegion->region_id : null;

        $UserRole = $user->roles->first();
        $UserRoleId = $UserRole->id;


        if ($validator->passes()) {
            $user->username = $user_data['username'];
            $user->email = $user_data['email'];
            $user->password = Hash::make($user_data['password']);

            $user->save();
            $user->addRole($roles_data['role']);

            // $UserRole = $UserRole->role_id = $roles_data['role_id'];
            // $UserRole->update();

            if ($roles_data['role'] == 3 AND count($region_data)) {
                $UserRegion = ($UserRegion) ? $UserRegion : new \UserRegion;
                $UserRegion->region_id = $region_data['region_id'];
                $UserRegion->user_id = $user->id;
                $UserRegion->save();
            }

            if ($UserRegion AND $roles_data['role'] != 3) {
                $UserRegion->delete();
            }

            if ($contact) {
                $contact->name = $contact_data['name'];
                $contact->surname = $contact_data['surname'];
                $contact->save();
            } else {
                $contact = new \Contact;
                $contact->name = $contact_data['name'];
                $contact->surname = $contact_data['surname'];
                $contact->save();

                $admin_table = new \Admin;

                $admin_table->contact_id = $contact->id;
                $admin_table->user_id = $user->id;
                $admin_table->save();
            }

            return \Response::json(array('success' => 'true', 'redirect_to' => route('users')));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function getCreate()
    {
        if (!\Auth::getUser()->hasRole("superadmin"))
            return \PermissionHelper::noPermissionPage($this);

        $roles = array(null => 'Choose role') + \Role::whereNotIn('id', [5, 6, 2, 7])->get()->lists('name', 'id');

        $regions = array(null => 'Choose region') + \Region::orderBy('region_name')->lists('region_name', 'id');

        $this->layout->title = 'Create new user';
        $this->layout->content = View::make('superadmin/users/create', array(
            'roles'   => $roles,
            'regions' => $regions,
        ));
    }

    public function postCreate()
    {
        $rules = array(
            'contact.name'    => array('required'),
            'contact.surname' => array('required'),
            'users.username'  => array('required', 'unique:users'),
            'users.email'     => array('required', 'email', 'unique:users'),
            'users.password'  => array('required'),
            'roles.role'      => array('required'),
        );


        $validator = \Validator::make(array_dot(Input::all()), $rules);

        $user_data = Input::get('users');
        $contact_data = Input::get('contact');
        $roles_data = Input::get('roles');
        $region_data = Input::get('region');


        if ($validator->passes()) {

            if ($roles_data['role'] == 3 AND !count($region_data))
                return \Response::json(array('errors' => 'Please choose region'));

            $user = new \User;
            $user->username = $user_data['username'];
            $user->email = $user_data['email'];
            $user->password = Hash::make($user_data['password']);

            $user->save();

            $userrole = new \UserRole;
            if ($roles_data['role'] == 'player')
                $userrole->role_id = 5;
            elseif ($roles_data['role'] == 'referee')
                $userrole->role_id = 6;
            else
                $userrole->role_id = $roles_data['role'];

            $userrole->user_id = $user->id;
            $userrole->save();

            if ($roles_data['role'] == 3 AND count($region_data)) {

                $userregion = new \UserRegion;
                $userregion->user_id = $user->id;
                $userregion->region_id = $region_data['region_id'];
                $userregion->save();
            }

            if ($roles_data['role'] == 'player') {
                $player = \Player::find(Input::get('player.id'));

                $admin_table = new \Admin;

                $admin_table->contact_id = $player->contact_id;
                $admin_table->user_id = $user->id;
                $admin_table->save();

                if (!$player->contact->email) {
                    $player->contact->email = $user_data['email'];
                    $player->contact->save();
                }

                $player->user_id = $user->id;
                $player->save();

                $redirect_to = '/players/info/'.$player->id;
            } elseif($roles_data['role'] == 'referee') {
                $referee = \Referee::find(Input::get('referee.id'));

                $admin_table = new \Admin;

                $admin_table->contact_id = $referee->contact_id;
                $admin_table->user_id = $user->id;
                $admin_table->save();

                if (!$referee->contact->email) {
                    $referee->contact->email = $user_data['email'];
                    $referee->contact->save();
                }

                $referee->user_id = $user->id;
                $referee->save();

                \DB::table("tournaments")->where("referee_id_real", $referee->id)->update(array("referee_id"=>$user->id));


                $redirect_to = '/referees/details/'.$referee->id;
            } else {
                $contact = new \Contact;

                $contact->name = $contact_data['name'];
                $contact->surname = $contact_data['surname'];
                $contact->save();

                $admin_table = new \Admin;

                $admin_table->contact_id = $contact->id;
                $admin_table->user_id = $user->id;
                $admin_table->save();

                $redirect_to = route('users');
            }

            return \Response::json(array('success' => 'true', 'redirect_to' => $redirect_to));
        } else {
            return $validator->messages()->toJson();
        }
    }

    public function deleteUser($id)
    {
        if (!\Auth::getUser()->hasRole("superadmin"))
            return \PermissionHelper::noPermissionPage($this);

        $user = $this->user->find($id);

        $user->roles()->detach();

        $user->delete();

        return \Response::json(array('success' => true));
    }


}

