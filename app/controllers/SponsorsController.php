<?php

class SponsorsController extends BaseController {

	
	public function getIndex()
	{
		$sponsors = Sponsor::orderBy('id', 'DESC')->get();

        // echo Debug::vars($sponsors->first()->image());die;

		$this->layout->title = 'Sponsors';
		$this->layout->content = View::make('sponsors/index', array(
			'sponsors' => $sponsors,
		));
	}

	public function getCreate()
	{
		$regions_array = [ 0 => LangHelper::get('all_regions', 'All regions') ] + Region::lists('region_name', 'id');

		$this->layout->title = LangHelper::get('add_sponsor', 'Add sponsor');
        $this->layout->content = View::make('sponsors/create', array(
			'regions_array' => $regions_array,
        ));
	}

	public function postCreate()
	{
		$image = Input::file('image');
        $filename = '';

        if ($image)
            $filename = Str::random().'_'.$image->getClientOriginalName();

        $rules = array(
        	'title'=>'required'
        );

        $save_path = public_path().'/uploads/images';

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes())
        {
            if ($image) {
                Image::make($image)->save($save_path.'/sponsors/full/'.$filename);
                Image::make($image)->resize(210, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(210, 280)->save($save_path.'/sponsors/thumbnails/'.$filename);
            }

            $sponsor = Sponsor::create(Input::all());

            // save image name to sponsors table
            $sponsor->image = $filename;
            $sponsor->save();

            return Redirect::to('sponsors');
        }
        else
        {
            return Redirect::to('sponsors/create');
        }
	}

	public function getEdit($sponsor_id)
	{
		$sponsor = Sponsor::with('region')
			->find($sponsor_id);


        $sponsor_region_array = (!$sponsor->region) ? array() : [$sponsor->region_id => $sponsor->region->region_name];
        // echo Debug::vars($sponsor_region_array);die;

		$regions_array = $sponsor_region_array + [0 => LangHelper::get('all_regions', 'All regions') ] + Region::lists('region_name', 'id');

		$this->layout->title = LangHelper::get('edit_sponsor', 'Edit sponsor');
        $this->layout->content = View::make('sponsors/edit', array(
			'regions_array' => $regions_array,
			'sponsor'       => $sponsor,
        ));
	}

	public function putEdit($sponsor_id)
	{
		$sponsor = Sponsor::find($sponsor_id);

		$image = Input::file('image');
        $filename = '';

        if ($image)
            $filename = Str::random().'_'.$image->getClientOriginalName();


        $rules = array(
        	'title'=>'required'
        );

        $validator = Validator::make(Input::all(), $rules);
        
        $input_array = Input::all();
        $save_path = public_path().'/uploads/images';

        if ($validator->passes())
        {
            if ($image) {
                Image::make($image)->save($save_path.'/sponsors/full/'.$filename);
                Image::make($image)->resize(210, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(210, 280)->save($save_path.'/sponsors/thumbnails/'.$filename);
                $sponsor->image = $filename;
                $sponsor->save();
            }

            unset($input_array['image']);

            $sponsor->update($input_array);


            return Redirect::to('sponsors');
        }
        else
        {
            return Redirect::to('sponsors/edit');
        }
	}


    public function getViewImage($sponsor_id)
    {
        $sponsor = Sponsor::find($sponsor_id);

        $this->layout = NULL;

        return Response::json(['image' => $sponsor->image()]);

    }
}
