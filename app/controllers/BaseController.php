<?php

class BaseController extends Controller
{

    public $layout = 'layouts.master';

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */


    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
//        echo Debug::vars(Auth::getUser()->hasRole("standard_user"));die;

            $layout = Request::ajax() ? 'layouts/ajax' : $this->layout;
            $this->layout = View::make($layout);

            $scripts = array();
            $tracking_scripts = array();
            if (Auth::user()) {
                if (Auth::user()->hasRole('referee')) {
                    $scripts = array(
                        'js/ang/referee/tournaments.js' => 'text/javascript',
                    );
                }
            }
            if (App::environment() == "production") {
                $tracking_scripts = array(
                    'js/google_analytics.js' => 'text/javascript',
                );
            };
            if (App::environment() == "dev") {
                $tracking_scripts = array(
                    'js/staging_google_analytics.js' => 'text/javascript',
                );
            };


            if (Config::get("app.offline_mode")) {
                $fonts_style = array(
                    'css/offline_css.css' => 'screen, projection',
                );
            } else {
                $fonts_style = array(
                    'http://fonts.googleapis.com/css?family=Roboto:400,100&subset=latin,latin-ext,vietnamese' => 'screen, projection',
                    'http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700,800,300&subset=latin,latin-ext' => 'screen, projection',
                    'css/external_fonts.css' => 'screen, projection',
                );
            }

            $this->layout->styles = $fonts_style + array(

                    'css/animate.css' => 'screen, projection',
                    'css/metro-bootstrap.css' => 'screen, projection',
                    'css/timepicki.css' => 'screen, projection',
                    'css/loading-bar.css' => 'screen, projection',
                    'min/iconFont.min.css' => 'screen, projection',
                    'css/dropzone.css' => 'screen, projection',
                    'css/bracket.css' => 'screen, projection',

                    'js/thirdparty/select2/select2.css' => 'screen, projection',
                    'js/thirdparty/angular-tags/ng-tags-input.css' => 'screen, projection',
                    'js/thirdparty/eventCalendar/fullcalendar.css' => 'screen, projection',

                    'css/main.css?v=1.1' => 'all',
                    'css/main-responsive.css?v=1.0' => 'all',
                    'css/helpers.css' => 'screen, projection',
                    'css/bracket-print.css' => 'print',

                );

            $this->layout->external_scripts = array();
            if (!Config::get("app.offline_mode")) {
                $this->layout->external_scripts = array(
                    'https://maps.googleapis.com/maps/api/js?key=AIzaSyCMHx_ag-fR-MlV6EPsiea8GywL3KuZqcY' => 'text/javascript',
                );
            }

            $this->layout->scripts = array(
                    'js/thirdparty/external/jquery.1-11.1.min.js' => 'text/javascript',
                    'js/thirdparty/external/angular.1.2.16.min.js' => 'text/javascript',
                    'js/thirdparty/external/angular-animate.1.2.16.js' => 'text/javascript',
                    'js/thirdparty/external/angular-resource.1.2.16.min.js' => 'text/javascript',
                    'js/thirdparty/external/angular-route.1.2.16.js' => 'text/javascript',
                    'js/thirdparty/external/angular-ui-utils.0.1.1.min.js' => 'text/javascript',
                    'js/thirdparty/dragDrop/jquery-ui.min.js' => 'text/javascript',
                    'js/thirdparty/highcharts.js' => 'text/javascript',
                    'js/bootstrap/js/bootstrap.min.js' => 'text/javascript',
                    'js/ang/bootstrap/ui-bootstrap-tpls-0.11.0.js' => 'text/javascript',
                    'js/bootstrap/js/timepicki.js' => 'text/javascript',
                    'js/thirdparty/select2/select2.js' => 'text/javascript',
                    'js/ang/angular-file-upload.js' => 'text/javascript', // add file upload handler
                    // 'js/thirdparty/history.js'                               => 'text/javascript',
                    'js/thirdparty/jquery.history.js' => 'text/javascript',
                    'js/thirdparty/jquery.fullscreen-0.4.1.js' => 'text/javascript',
                    'js/thirdparty/printThis.js' => 'text/javascript',
                    'js/thirdparty/dropzone.js' => 'text/javascript',
                    'js/thirdparty/metro-hint.js' => 'text/javascript',
                    'min/jquery-widget.min.js' => 'text/javascript',
                    'min/metro.min.js' => 'text/javascript',
                    'js/ang/loading-bar.js' => 'text/javascript',
                    'js/ang/genericForm.js' => 'text/javascript',
                    'js/ang/FormErrors.js' => 'text/javascript',
                    'js/ang/dataTables.js' => 'text/javascript',
                    'js/thirdparty/angular-tags/ng-tags-input.js' => 'text/javascript',
                    'js/ang/select2.js' => 'text/javascript',
                    'js/thirdparty/eventCalendar/moment.js' => 'text/javascript',
                    'js/thirdparty/eventCalendar/calendar.js' => 'text/javascript',
                    'js/thirdparty/eventCalendar/fullcalendar.min.js' => 'text/javascript',
                    'js/ang/calendarApp.js' => 'text/javascript',
                    'js/jquery.tinyscrollbar.js' => 'text/javascript',
                    'js/brain-socket.min.js' => 'text/javascript',
                    '/js/ang/sponsors.js' => 'text/javascript',
                    '/js/printThis.js' => 'text/javascript',
                    'js/html2canvas.js' => 'text/javascript',
                    // 'js/ang/app.js',
                    // 'js/ang/controllers.js',
                    // 'js/ang/filters.js',
                    // 'js/ang/directives.js',
                    // 'js/ang/services.js',
                    'js/main.js?v=1.4' => 'text/javascript',
                ) + $scripts + $tracking_scripts;
        }
        BackButtonHelper::new_step();
    }

}
