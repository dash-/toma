<?php

class ExternalTournamentscontroller extends BaseController
{

    public function getIndex()
    {
        if (!Auth::user()->hasRole('superadmin'))
            return Redirect::to('/');

        $data_session = Session::get('data');
        $data = '';
        if (isset($data_session)) {
            $data = "Total points assigned: " . $data_session['total_calculated_points'] . "<br/>";
            $data .= " for external tournament " . TournamentHelper::external_points_type_keys($data_session['tournament_type']) . " (" . $data_session['external_points_acquired'] . ") " . "<br/>";
            $data .= " to player " . $data_session['player_name'] . " with licence number " . $data_session['licence_number'];
        }
        $this->layout->title = LangHelper::get('external_tournament_points', 'External tournament points');
        $this->layout->content = View::make('external/index', array('data' => $data));
    }

    public function postIndex()
    {
        if (!Auth::user()->hasRole('superadmin'))
            return Redirect::to('/');

        $rules = array(
            'licence_number' => array('required', 'numeric'),
            'points_number' => array('required', 'numeric'),
            'points_type' => array('required', 'numeric'),
        );


        $validator = Validator::make(Input::all(), $rules);

        $single_error = '';
        if ($validator->passes()) {
            $player = Player::where("licence_number", Input::get('licence_number'))->first();
            if (!$player) {
                $single_error = "Player does not exists";
            } else {
                $total_points = Input::get('points_number') * TournamentHelper::external_points_type_keys(Input::get('points_type'));

                $external_points = new ExternalTournament();
                $external_points->player_id = $player->id;
                $external_points->licence_number = $player->licence_number;
                $external_points->tournament_type = Input::get('points_type');
                $external_points->external_points_acquired = Input::get('points_number');
                $external_points->total_calculated_points = $total_points;
                $external_points->user_edit_id = Auth::user()->id;
                $external_points->save();

                $data = array(
                    'total_calculated_points' => $external_points->total_calculated_points,
                    'tournament_type' => $external_points->tournament_type,
                    'external_points_acquired' => $external_points->external_points_acquired,
                    'licence_number' => $external_points->licence_number,
                    'player_name' => $player->contact->full_name()
                );
                return Redirect::to("/external/index")->with("data", $data);
            }
        }
        $errors = $validator->messages();

        $this->layout->title = LangHelper::get('external_tournament_points', 'External tournament points');
        $this->layout->content = View::make('external/index', array('single_error' => $single_error, 'errors' => $errors, 'input' => Input::all()));
    }

    public function getNotAssigned()
    {

        $this->layout->title = LangHelper::get('external_tournament_points', 'External tournament points - Not Assigned');
        $this->layout->content = View::make('external/list', array('assigned' => false, 'querter' => false));
    }

    public function getAssigned()
    {
        $this->layout->title = LangHelper::get('external_tournament_points', 'External tournament points - Assigned');
        $this->layout->content = View::make('external/list', array('assigned' => true, 'querter' => false));
    }

    public function getData($assigned = false)
    {

        $items = Request::query('items_counter');
        $order = Request::query('order') ? Request::query('order') : "players.id";
        $sort = Request::query('sort');


        $query = ExternalTournament::join('players', 'players.id', '=', 'external_tournaments.id')
            ->leftJoin('contacts', 'contacts.id', '=', 'players.contact_id')
            ->select("*", "players.id as id", "external_tournaments.licence_number as licence_number")
            ->where("points_used_for_ranking_calculations", $assigned);


        if (Request::query('search') == 1) {
            if (Request::query('name'))
                $query = $query->where('name', Request::query('name'));

            if (Request::query('surname'))
                $query = $query->where('surname', Request::query('surname'));

            if (Request::query('licence_number'))
                $query = $query->where('external_tournaments.licence_number', Request::query('licence_number'));

            if (Request::query('external_points_acquired'))
                $query = $query->where('external_points_acquired', Request::query('external_points_acquired'));

            if (Request::query('total_calculated_points'))
                $query = $query->where('total_calculated_points', Request::query('total_calculated_points'));

            $players = $query->paginate($items);

            return Response::json($players->toArray())->setCallback(Request::get('callback'));
        }

        $players = $query->orderBy($order, $sort)->paginate($items);
//        echo Debug::vars($players->toArray());die;
        return Response::json($players->toArray())->setCallback(Request::get('callback'));

    }

}

?>