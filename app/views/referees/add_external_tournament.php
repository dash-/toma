<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>


        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>
                    <?= LangHelper::get('add_external_tournament', 'Add external tournament') ?>
                    - <?= $referee->contact->full_name() ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>


        <div class="tournament-second gap-top20" ng-controller="ExternalTournamentController">
            <form name="externalForm" ng-submit="formSubmit($event, '/referees/add-external/<?= $referee->id ?>')"
                  novalidate>
                <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token() ?>'">
                <fieldset>
                    <div class="row">
                        <div class="form-control">
                            <label
                                for="tournament_title"><?= LangHelper::get('tournament_title', 'Tournament title') ?></label>

                            <div class="input-control text">
                                <input type="text" ng-model="formData.referee.tournament_title" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half gap-left0">
                            <div class="form-control">
                                <label
                                    for="refereed_date_from"><?= LangHelper::get('refereed_from', 'Refereed from') ?></label>

                                <div class="input-control text">
                                    <input type="text" datepicker-popup="dd.MM.yyyy"
                                           ng-model="formData.referee.refereed_date_from"
                                           is-open="opened['dateFrom']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'dateFrom')" formatdate required>
                                </div>
                            </div>
                        </div>

                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="refereed_date_from"><?= LangHelper::get('refereed_to', 'Refereed to') ?></label>

                                <div class="input-control text">
                                    <input type="text" datepicker-popup="dd.MM.yyyy"
                                           ng-model="formData.referee.refereed_date_to"
                                           is-open="opened['dateTo']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'dateTo')" formatdate required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half gap-left0">
                            <div class="form-control">
                                <label for="country"><?= LangHelper::get('country', 'Country') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.referee.country" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="city"><?= LangHelper::get('city', 'City') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.referee.city" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form-errors></form-errors>
                </fieldset>
                <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                        style="margin-top: 38px;"
                        ng-disabled="!externalForm.$valid"><?= LangHelper::get('save', 'Save') ?></button>
            </form>
        </div>


    </div>
</div>

<script src="/js/ang/referee.js"></script>

<script>
    $(function () {
        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['refereeApp']);
        });
    })
</script>
