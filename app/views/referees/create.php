<div class="container ang" ng-cloak>
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('add_referee', 'Add referee')?></h3>
                <span class="line"></span>
            </div>
        </div>

        
        <form name="createForm" ng-class="{ 'show-errors': showErrors }" ng-submit="formSubmit($event, 'POST','<?= LangHelper::get('added_new_referee', 'Added new referee')?>', '<?= Request::url()?>', true)"  ng-controller="CreateController" novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                  <div class="tournament-second">
                       <div class="row">
                           <div class="half">
                               <div class="form-control">
                                   <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                                   <div class="input-control text">
                                       <input type="text" name="name" ng-model="formData.contact.name" required>
                                   </div>
                               </div>
                           </div>
                           <div class="half">
                               <div class="form-control">
                                   <label for="surname"><?= LangHelper::get('surname', 'Surname')?></label>
                                   <div class="input-control text">
                                       <input type="text" name="surname" ng-model="formData.contact.surname" required>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="half">
                               <div class="form-control">
                                   <label for="address"><?= LangHelper::get('address', 'Address')?></label>
                                    <div class="input-control text">
                                        <input type="text" name="address" ng-model="formData.contact.address">
                                    </div>
                               </div>
                           </div>
                           <div class="half">
                               <div class="form-control">
                                   <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                                   <div class="input-control text">
                                       <input type="email" name="email" ng-model="formData.contact.email" required>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="half">
                               <div class="form-control">
                                   <label for="phone"><?= LangHelper::get('phone', 'Phone')?></label>
                                   <div class="input-control text">
                                       <input type="text" name="phone" ng-model="formData.contact.phone">
                                   </div>
                               </div>
                           </div>
                           <div class="half">
                               <div class="form-control">
                                   <label for="city"><?= LangHelper::get('city', 'City')?></label>
                                   <div class="input-control text">
                                       <input type="text" name="city" ng-model="formData.contact.city">
                                   </div>
                               </div>
                           </div>
                       </div>
                       
                       <div class="row">
                           
                       </div>
                       <div class="row">
                           <div class="half">
                               <div class="form-control">
                                   <label for="postal_code"><?= LangHelper::get('postal_code', 'Postal code')?></label>
                                   <div class="input-control text">
                                      <input type="text" name="postal_code" ng-model="formData.contact.postal_code">
                                   </div>
                               </div>
                           </div>
                           <div class="half">
                               <div class="form-control">
                                   <label for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth')?></label>
                                   <div class="input-control text">
                                       <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.contact.date_of_birth" is-open="opened['date_of_birth']" show-button-bar="false" required ng-click="openDatePicker($event, 'date_of_birth')" formatdate>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="half">
                               <div class="form-control" ng-class="{'has-error': createForm.$submitted && createForm.sex.$invalid, 'has-success': createForm.sex.$valid && createForm.sex.$dirty}">
                                   <label for="sex"><?= LangHelper::get('gender', 'Gender')?></label>
                                   <?= Form::select('sex', $genders, NULL, array('required','ng-model' => 'formData.contact.sex'));?>
                               </div>
                           </div> 
                          
                       </div>
                       <div class="row">
                           <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg" style="margin-top: 38px;"><?= LangHelper::get('save', 'Save')?></button>
                       </div>
                       <form-errors></form-errors>
                       </div>
                </fieldset>
       </form>
       
    </div>
</div>

<script src="/js/ang/referee.js"></script>
<script>
$(document).ready(function() {
        $("select").select2();
    });

angular.element(document).ready(function() {
    angular.bootstrap('.ang', ['refereeApp']);
});
</script>
