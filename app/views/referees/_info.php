<div class="row players-info" style="padding-top:50px">

    <div class="leftPartOne">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('name', 'Name') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('surname', 'Surname') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('phone', 'Phone') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('email', 'Email') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('city', 'City') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('address', 'Address') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('postal_code', 'Postal code') ?>:
                </td>
            </tr>
        </table>
    </div>

    <div class="leftPartTwo">
        <table>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $ref->contactInfo('name') ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('surname') ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('phone')?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('email')?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('date_of_birth') ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $ref->contactInfo('city')?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('address') ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('postal_code')?></td>
            </tr>
        </table>
    </div>

    <div class="rightPartOne">

        <table>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('licence_number', 'Licence number') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('sex', 'Sex') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('card_id', 'Card id') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('licence_category', 'Licence category') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('school_number', 'School number') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('level', 'Level') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('status', 'Status') ?>:
                </td>
            </tr>

        </table>
    </div>
    <div class="rightPartTwo">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px; height:20px;">
                    &nbsp;<?= $ref->licence_number ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('sex')?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->contactInfo('card_id')?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->licence_category ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->school_number ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->level?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $ref->active() ?>
                </td>
            </tr>
        </table>
    </div>
</div>