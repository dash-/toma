<div class="container ang">
    <div class="grid datatable" ng-controller="tableController" data-ng-init="init(1)"
         data-scUrl="/referees/data">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('referees', 'Referees')?></h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="row clearfix pad-top20">
            <form ng-submit="search($event, 1)" class="tabScrl" name="searchForm">
                <table class="table bg-dark striped table-scrollable-w768">
                    <thead>
                    <tr>
                        <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.surname"
                                       placeholder="<?= LangHelper::get("search_by_surname", "Search by surname") ?>">
                            </div>
                            <a ng-click="doOrder('contacts.surname')" href="#">
                                <?= LangHelper::get('surname', 'Surname') ?> <i
                                    ng-class="sortBy('contacts.surname')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.name"
                                       placeholder="<?= LangHelper::get("search_by_name", "Search by name") ?>">
                            </div>
                            <a ng-click="doOrder('contacts.name')" href="#">
                                <?= LangHelper::get('name', 'Name') ?> <i ng-class="sortBy('contacts.name')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.licence_number"
                                       placeholder="<?= LangHelper::get("search_by_licence_number", "Search by licence number") ?>">
                            </div>
                            <a ng-click="doOrder('licence_number')" href="#">
                                <?= LangHelper::get('licence_number', 'Licence number') ?> <i
                                    ng-class="sortBy('licence_number')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.city"
                                       placeholder="<?= LangHelper::get("search_by_city", "Search by city") ?>">
                            </div>
                            <a ng-click="doOrder('city')" href="#">
                                <?= LangHelper::get('city', 'City') ?> <i ng-class="sortBy('city')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.licence_category"
                                       placeholder="<?= LangHelper::get("by_licence_category", "Search by licence category") ?>">
                            </div>
                            <a ng-click="doOrder('licence_category')" href="#">
                                <?= LangHelper::get("licence_category", "Licence category") ?> <i ng-class="sortBy('licence_category')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.active"
                                       placeholder="<?= LangHelper::get("by_status", "Search by status") ?>">
                            </div>
                            <a ng-click="doOrder('active')" href="#">
                                <?= LangHelper::get("status", "Status") ?> <i ng-class="sortBy('active')"></i>
                            </a>
                        </th>
                        <th class="text-left last-col">
                            <div class="input-control text">
                                <button type="submit" ng-click="search($event, 1)"
                                        ng-class="scSearch == 1 ? 'split-button' : ''"
                                        class="primary dark full"><?= LangHelper::get('filter', 'Filter') ?></button>
                                <a ng-click="resetSearch()" ng-show="scSearch == 1"
                                   class="button primary split-button reset-button">
                                    <?= LangHelper::get('reset_search', 'Reset search') ?>
                                </a>
                            </div>
                            <a class="options"><?= LangHelper::get('options', 'Options') ?></a>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="referee in rows track by $index">
                        <td>{{ referee.surname }}</td>
                        <td>{{ referee.name }}</td>
                        <td>{{ referee.licence_number }}</td>
                        <td>{{ referee.city }}</td>
                        <td>{{ referee.licence_category}}</td>
                        <td>{{ referee.active | showStatus}}</td>
                        <td class="text-left input-control transparent">
                            <a class="button primary info inline-block" ng-href="/referees/details/{{referee.id}}">
                                <?= LangHelper::get('info', 'Info') ?>
                            </a>
                            <a class="call button small primary default" ng-href="/referees/edit/{{referee.id}}">
                                <?= LangHelper::get('edit', 'Edit') ?>
                            </a>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </form>

            <h3 class="fg-white text-center"
                ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data') ?></h3>

            <div class="input-control back-in-pagination" ng-show="rows.length > 0">
                <a class="button primary yellow back big call" href="/"><?= LangHelper::get('back', 'BACK') ?></a>
            </div>
            <div class="pagination" ng-show="rows.length > 0">
                <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage" max-size="maxSize"
                            boundary-links="true" rotate="false" items-per-page="counter"></pagination>
            </div>
            <div class="input-control next-button marginTop-50" ng-show="rows.length > 0">
                <a class="button primary big call next"
                   href="<?= URL::to('referees/create') ?>"><?= LangHelper::get('submit_new_referee', 'Submit new referee') ?></a>
            </div>
        </div>

    </div>

</div>

<script>
    //napomena : hack rjesenje funkcije showSearch u select.js, stoga ne korisititi select.min.js u produkciji
    $(document).ready(function () {
        $("select").select2({});
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>
