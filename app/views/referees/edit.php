<div class="container ang" ng-cloak>
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_referee', 'Edit referee') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <form name="editForm" ng-class="{ 'show-errors': showErrors }"
              ng-submit="formSubmit($event, 'PUT','<?= LangHelper::get('form_updated', 'Form updated') ?>', '<?= Request::url() ?>')"
              ng-controller="EditController" novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
                <div class="tournament-second">
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="name"
                                           ng-init="formData.contact.name='<?= HTML::entities($ref->contactInfo('name')) ?>'"
                                           ng-model="formData.contact.name" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="surname"
                                           ng-init="formData.contact.surname='<?= HTML::entities($ref->contactInfo('surname')) ?>'"
                                           ng-model="formData.contact.surname" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="address"><?= LangHelper::get('address', 'Address') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="address"
                                           ng-init="formData.contact.address='<?= HTML::entities($ref->contactInfo('address')) ?>'"
                                           ng-model="formData.contact.address">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="email"
                                           ng-init="formData.contact.email='<?= $ref->contactInfo('email') ?>'"
                                           ng-model="formData.contact.email">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="phone"
                                           ng-init="formData.contact.phone='<?= $ref->contactInfo('phone') ?>'"
                                           ng-model="formData.contact.phone">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="city"><?= LangHelper::get('city', 'City') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="city"
                                           ng-init='formData.contact.city="<?= HTML::entities($ref->contactInfo('city')) ?>"'
                                           ng-model="formData.contact.city">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="postal_code"><?= LangHelper::get('postal_code', 'Postal code') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="postal_code"
                                           ng-init="formData.contact.postal_code='<?= $ref->contactInfo('postal_code') ?>'"
                                           ng-model="formData.contact.postal_code">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth') ?></label>

                                <div class="input-control text">
                                    <input type="text" datepicker-popup="dd.MM.yyyy"
                                           ng-init="formData.contact.date_of_birth='<?= $ref->contactInfo('date_of_birth') ?>'"
                                           ng-model="formData.contact.date_of_birth" is-open="opened['date_of_birth']"
                                           show-button-bar="false" required
                                           ng-click="openDatePicker($event, 'date_of_birth')" formatdate>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{'has-error': editForm.$submitted && editForm.sex.$invalid, 'has-success': editForm.sex.$valid && editForm.sex.$dirty}">
                                <label for="sex"><?= LangHelper::get('gender', 'Gender') ?></label>
                                <?= Form::select('sex', $genders, null, array('required', 'ng-model' => 'formData.contact.sex',
                                      'ng-init' => "formData.contact.sex='" . $ref->contactInfo('sex') . "'")); ?>
                            </div>
                        </div>

                        <div class="half">
                            <div class="form-control">
                                <label for="postal_code"><?= LangHelper::get('school_number', 'School number') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="school_number"
                                           ng-init="formData.referee.school_number='<?= $ref->school_number?>'"
                                           ng-model="formData.referee.school_number">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="level"><?= LangHelper::get('level', 'Level') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-init="formData.referee.level='<?= $ref->level ?>'"
                                           ng-model="formData.referee.level">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="licence_category"><?= LangHelper::get('licence_category', 'Licence category') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-init="formData.referee.licence_category='<?= $ref->licence_category ?>'"
                                           ng-model="formData.referee.licence_category">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="active"><?= LangHelper::get('status', 'Status') ?></label>
                                <?= Form::select('active', $status_array, null, array('required', 'ng-model' => 'formData.referee.active',
                                      'ng-init' => "formData.referee.active='" . $ref->active . "'")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                                style="margin-top: 38px;"><?= LangHelper::get('save', 'Save') ?></button>
                    </div>
                    <form-errors></form-errors>
                </div>
            </fieldset>
        </form>

    </div>
</div>

<script src="/js/ang/referee.js"></script>
<script>
    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['refereeApp']);
    });
</script>
