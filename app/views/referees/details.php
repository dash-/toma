<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <?php if (!$ref->user_id AND Auth::user()->hasRole('superadmin')): ?>
            <a class="place-right rfet-yellow rfet-button gap-left20" href="/referees/create-account/<?= $ref->id ?>">
                <?= LangHelper::get('create_user_account', 'Create user account') ?>
            </a>
        <?php endif ?>

        <a class="place-right rfet-yellow rfet-button gap-left20" href="/referees/edit/<?= $ref->id ?>">
            <?= LangHelper::get('edit', 'Edit') ?>
        </a>

        <a class="place-right rfet-yellow rfet-button gap-left20" href="/referees/add-external/<?= $ref->id ?>">
            <?= LangHelper::get('add_external_tournament', 'Add external tournament') ?>
        </a>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('referee_info', 'Referee info') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <?= View::make('referees/_info', ['ref' => $ref]) ?>

        <?php if ($ref->user_id): ?>
            <div class="row">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('user_informations', 'User informations') ?></h3>
                    <span class="line"></span>
                </div>

                <div class="clearfix gap-top20 players-info">
                    <p>
                    <span class="color-rfet">
                        <?= LangHelper::get('username', 'Username') ?>:
                    </span>
                        <?= $ref->user->username ?>
                    </p>

                    <p>
                    <span class="color-rfet">
                        <?= LangHelper::get('user_email', 'User email') ?>:
                    </span>
                        <?= $ref->user->email ?>
                    </p>

                    <p>
                        <span class="color-rfet">
                            <?= LangHelper::get('role', 'Role') ?>:
                        </span>
                            <?= LangHelper::get('referee', 'Referee') ?>

                        <?php if (Auth::user()->hasRole('superadmin')):?>
                            <div class="input-control checkbox" id="ref-check-holder">
                                <label>
                                    <input type="checkbox" id="referee_check" data-id="<?= $ref->user_id ?>"
                                           name="super_referee"
                                        <?= ($ref->user->isRefereeAdmin()) ? 'checked' : '' ?>>
                                    <span class="check"></span>
                                    <?= LangHelper::get('referee_admin', 'Referee admin') ?>
                                </label>
                            </div>
                        <?php endif?>

                    </p>

                </div>
            </div>
        <?php endif ?>

        <?php if (count($tournaments)): ?>
            <div class="row">
                <div class="table-title tournament">
                    <h3>RFET <?= LangHelper::get('tournaments', 'Tournaments') ?></h3>
                    <span class="line"></span>
                </div>

                <div class="clearfix gap-top20">
                    <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                        <thead>
                        <tr>
                            <th class="text-left"><?= LangHelper::get('start_date', 'Start date') ?></th>
                            <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                            <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                <th class="text-left"><?= LangHelper::get('submited_by', 'Submited by') ?></th>
                            <?php endif ?>
                            <th class="text-left"><?= LangHelper::get('surface', 'Surface') ?></th>
                            <th class="text-left"><?= LangHelper::get('organizer', 'Organizer') ?></th>
                            <th class="text-left"><?= LangHelper::get('status', 'Status') ?></th>
                            <th class="text-right" width="10%">Options</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($tournaments as $tournament): ?>
                            <tr>
                                <td style="<?= ($tournament->status == 5) ? 'border-left: 3px solid #449d44' : null ?>">
                                    <?= DateTimeHelper::GetShortDateFullYear($tournament->date->main_draw_from) ?>
                                </td>
                                <td><?= $tournament->title ?></td>
                                <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                    <td>
                                        <?= ($tournament->creator())
                                            ? $tournament->creator()->info("name") . " "
                                            . $tournament->creator()->info("surname")
                                            : false ?>
                                    </td>
                                <?php endif ?>
                                <td><?= ($tournament->surface) ? $tournament->surface->name : null ?></td>
                                <td><?= $tournament->organizer->name ?></td>
                                <td><?= $tournament->status() ?></td>
                                <!-- options -->
                                <td class="transparent text-right">
                                    <a class="button yellow-col-bg d-small" data-title="Tournament details"
                                       href="/tournaments/details/<?= $tournament->id ?>">
                                        <?= LangHelper::get('details', 'Details') ?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </div>
            </div>
        <?php endif ?>

        <?php if (count($ref->external_tournaments)):?>
            <div class="row">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('external_tournaments', 'External tournaments') ?></h3>
                    <span class="line"></span>
                </div>

                <div class="clearfix gap-top20" ng-controller="RefereeDetailsController">
                    <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                        <thead>
                        <tr>
                            <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                            <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                <th class="text-left"><?= LangHelper::get('submited_by', 'Submited by') ?></th>
                            <?php endif ?>
                            <th class="text-left"><?= LangHelper::get('country', 'Country') ?></th>
                            <th class="text-left"><?= LangHelper::get('city', 'city') ?></th>
                            <th class="text-left"><?= LangHelper::get('refereed_from', 'Refereed from') ?></th>
                            <th class="text-left"><?= LangHelper::get('refereed_to', 'Refereed to') ?></th>
                            <th class="text-right" width="15%">Options</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($ref->external_tournaments as $external): ?>
                            <tr id="external_<?= $external->id?>">
                                <td><?= $external->tournament_title ?></td>
                                <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                    <td><?= $external->createdBy() ?></td>
                                <?php endif ?>
                                <td><?= $external->country?></td>
                                <td><?= $external->city?></td>
                                <td><?= DateTimeHelper::GetShortDateFullYear($external->refereed_date_from)?></td>
                                <td><?= DateTimeHelper::GetShortDateFullYear($external->refereed_date_to)?></td>
                                <!-- options -->
                                <td class="transparent text-right">
                                    <a class="button yellow-col-bg d-small"
                                       href="/referees/edit-external/<?= $external->id ?>">
                                        <?= LangHelper::get('edit', 'Edit') ?>
                                    </a>

                                    <a class="button yellow-col-bg d-small"
                                       href="/referees/external/<?= $external->id ?>"
                                        ng-click="removeExternal($event, <?= $external->id?>)">
                                        <?= LangHelper::get('delete', 'Delete') ?>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </div>
            </div>
        <?php endif?>
    </div>
</div>

<script src="/js/ang/referee.js"></script>

<script>
    $(function () {
        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['refereeApp']);
        });
    })
</script>


