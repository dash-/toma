<!DOCTYPE html>
<html lang="en-US">

<body>
<p>New error exception</p>

<p>
    <strong>Time:</strong> <?= DateTimeHelper::GetDateNow()?> <br/> <br/>
    <strong>URL:</strong> <?= $url?><br/><br/>
    <strong>IP address:</strong> <?= $ip?><br/><br/>
    <strong>File:</strong> <?= $exception->getFile()?> <br/><br/>
    <strong>Line:</strong> <?= $exception->getLine()?> <br/><br/>
    <strong>Code:</strong> <?= $exception->getCode()?> <br/><br/>
    <strong>Message:</strong> <?= $exception->getMessage()?> <br/><br/>
    <strong>Trace:</strong> <?= str_replace('#', "<br/>", $exception->getTraceAsString())?> <br/><br/>
    <strong>Environment:</strong> <?= $env?>
</p>

</body>
</html>

