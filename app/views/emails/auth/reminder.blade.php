<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>

		<div>
			To reset your password, follow the link and complete the form: {{ URL::to($url) }}.
		</div>
	</body>
</html>
