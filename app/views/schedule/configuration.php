<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament">
                <h3>
                    <?= LangHelper::get('edit_schedule_configuration', 'Edit schedule configuration') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?></p>
        </div>


        <div class="row pad-top20" ng-controller="ConfigurationController">

            <form name="editForm" ng-submit="confSubmit($event, <?= $tournament->id ?>)" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <?php foreach($configurations as $key => $conf):?>
                        <div class="row">
                            <h3><?= $conf->date()?></h3>
                            <input type="hidden" ng-model="formData.conf<?= $key?>.date" ng-init="formData.conf<?= $key?>.date='<?= $conf->date?>'">
                            <div class="half">
                                <div class="form-control">
                                    <label for="number_of_courts"><?= LangHelper::get('number_of_courts', 'Number of courts') ?> *</label>
                                    <div class="input-control select">
                                        <?= Form::select('number_of_courts[]', $number_range, $conf->number_of_courts, array('required',
                                            'ng-model' => 'formData.conf'.$key.'.number_of_courts',
                                            'ng-init'  => 'formData.conf'.$key.'.number_of_courts="' . $conf->number_of_courts . '"')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control">
                                    <label for="rest_time"><?= LangHelper::get('minimum_rest_time', 'Minimum rest time (in minutes)') ?> *</label>
                                    <div class="input-control text">
                                        <input type="text"  ng-model="formData.conf<?= $key?>.rest_time" ng-init="formData.conf<?= $key?>.rest_time=<?= $conf->rest_time?>" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="break-draw">
                    <?php endforeach?>
                    <button type="submit" class="rfet-button rfet-yellow big button"
                            style="margin-top:27px"><?= LangHelper::get('save', 'Save') ?></button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="/js/ang/scheduleForm2.js"></script>
<script type="text/javascript">
    $(function () {
        $("select").select2({width: '140px'});
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['scheduleApp']);
    });
</script>