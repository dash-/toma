<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="row">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('create_schedule', 'Create schedule') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <p class="yellow-color">
                <?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?>
            </p>
        </div>
        <a href="<?= URL::to("/schedule/order-print/". $tournament->id . "#print") ?>">
            <input type="button" value="Print order of play" class="btn btn-primary printOOP hideResponsive"/>
        </a>
        <a href="<?= URL::to("signups?open=" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('signups', 'Signups') ?>" class="btn btn-primary"/>
        </a>

        <a href="<?= URL::to("schedule/show/" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('preview', 'Preview') ?>" class="btn btn-primary"/>
        </a>

        <a href="<?= URL::to("/tournaments/draws/" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('draws_page', 'Draws page') ?>" class="btn btn-primary"/>
        </a>

        <div class="row clearfix">
            <h3 class="place-left">
                <?= LangHelper::get('order_of_play', 'Order of play') ?>
            </h3>

            <div class="place-right" style="margin-top: -30px; margin-bottom: 20px">
                <div class="courts_dropdown">
                    <label for="courts_number"><?= LangHelper::get('number_of_courts', 'Number of courts') ?></label>
                    <?= Form::select('courts_number', $courts_dropdown, $configuration->number_of_courts - 1, array('required', 'id' => 'courts_number')); ?>
                </div>
                <a class="rfet-button rfet-yellow pull-right gap-top30 gap-right10"
                   href="<?= URL::to('schedule/configuration', $tournament->id) ?>">
                    <?= LangHelper::get('schedule_configuration', 'Schedule configuration') ?>
                </a>
                <a class="rfet-button  <?= ($configuration->waiting_list) ? "rfet-danger" : "rfet-success" ?> pull-right gap-top30 gap-right10"
                   href="/schedule/waiting-list/<?= $tournament->id ?>?date=<?= $current_date ?>">
                    <?= ($configuration->waiting_list) ? LangHelper::get('turn_off', 'Turn off') : LangHelper::get('turn_on', 'Turn on') ?>
                    <?= LangHelper::get('waiting_list', 'Waiting list') ?>
                </a>
            </div>
        </div>
        <input type="hidden" id="tournament_id" value="<?= $tournament->id ?>"/>

        <div class="orderOfplay">
            <?php foreach ($dates_array as $key => $dates): ?>
                <div class="button date_select <?= ($key == $date_key) ? 'selected' : null ?>"
                     data-id="<?= $key ?>">
                    <a href="/schedule/index/<?= $tournament->id ?>?date=<?= $key ?>&his_ignore=true">
                        <div class="timeTextD"><?= date("j", strtotime($key)); ?></div>
                        <div class="timeTextM"><?= date("M", strtotime($key)); ?></div>
                        <div class="timeTextY"><?= date("Y", strtotime($key)); ?></div>
                    </a>
                </div>
            <?php endforeach ?>
        </div>

        <div class="list-of-teams">
            <div class="teams-by-draw">
                <?= View::make('schedule/_match_list', [
                    'all_matches'  => $all_matches,
                    'draw_matches' => $draw_matches,
                ])?>
            </div>
        </div>
        <div class="list-of-courts">
            <?= $schedule_view ?>
            <div class="add_new_row rfet-yellow rfet-button">Add new row</div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $(function () {
        $("select").select2({width: '140px'});

        $("#search-input").searchFilter({targetSelector: ".teams-by-draw .draggable.team", charCount: 2})
    });
</script>