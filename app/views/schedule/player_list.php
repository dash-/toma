<input type="hidden" id="previous_url" value="<?= URL::previous() ?>">
<div class="page-title">
    <h1 class="pull-left" style="color:black !important"><?= LangHelper::get('order_of_play', 'Order of play') ?>
</div>

<div class="clearfix gap-top20">

    <div class="list-of-courts">
        <?php if (count($matches)): ?>

            <?php foreach ($matches as $player_name => $match): ?>
                <div class="schedule-player-holder" style="color:black !important">
                    <h3 style="color:black !important; margin-top: 3px !important"><?= (string)$player_name ?></h3>
                    <table class="table table-striped" style="color:black !important; margin-bottom: 0px; margin-top: -25px">
                        <thead>
                        <tr>
                            <th class="text-left" width="40%">
                                <?= LangHelper::get('match', 'Match') ?>
                            </th>
                            <th class="text-left" width="20%">
                                <?= LangHelper::get('date', 'Date') ?>
                            </th>
                            <th class="text-left" width="20%">
                                <?= LangHelper::get('time', 'Time') ?>
                            </th>
                            <th class="text-left" width="10%">
                                <?= LangHelper::get('court_number', 'Court number') ?>

                            </th>
                            <th class="text-left" width="10%">
                                <?= LangHelper::get('order_number', 'Order number') ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($match as $schedule): ?>
                            <tr>
                                <td style="font-size:15px !important"><?= $schedule['teams'] ?></td>
                                <td style="font-size:15px !important"><?= $schedule['date'] ?></td>
                                <td style="font-size:15px !important"><?= $schedule['time'] ?></td>
                                <td style="font-size:15px !important"><?= $schedule['court_number'] ?></td>
                                <td style="font-size:15px !important"><?= $schedule['order_number'] ?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            <?php endforeach ?>

        <?php else: ?>
            <h3 class="pad-top20 gap-top20">
                <?= LangHelper::get('there_are_no_scheduled_matches', 'There are no scheduled matches') ?>
            </h3>
        <?php endif; ?>

    </div>


</div>

<script>
       $(function(){
        prev_url = $("#previous_url").val();
         if(location.hash)
            window.print();
            document.location.href = prev_url;
        });
</script>