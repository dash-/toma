<div class="yellow-color" style="text-align: left; font-size:12px"><?= LangHelper::get("example_search_round_2_qualifying_federer_male","Example search: 'round 2', 'qualifying', 'federer', 'male'") ?></div>
<input id="search-input" placeholder="<?= LangHelper::get('search', 'Search')?>">
<?php foreach ($draw_matches as $match): ?>
    <div class="draggable team" data-matchid="<?= $match->id ?>" data-drawid="<?= $match->draw_id ?>"
         data-listtype="<?= $match->list_type ?>">
        <div class="match_id"><?= $match->id ?></div>
                        <span class="hidden-span">
                            <?= $match->list_type == 1 ?  LangHelper::get('main','Main') : LangHelper::get('qualifying','Qualifying') ?>
                            </span>

                        <span class="hidden-span">
                            <?=  LangHelper::get('round','Round').' '.$match->round_number . ' ' ?>
                            </span>
                        <span class="hidden-span">
                            <?= $match->draw->typeName() . ' ' . $match->draw->genderName() . ' ' .
                            $match->draw->category->name

                            ?>
                        </span>
        <?= DrawMatchHelper::scheduleName($match, 'team1_data', $all_matches) ?>
        <span class="small">vs</span>
        <?= DrawMatchHelper::scheduleName($match, 'team2_data', $all_matches, true) ?>
    </div>
<?php endforeach ?>
