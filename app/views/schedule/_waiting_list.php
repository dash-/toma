<?php $timer = '09:00' ?>
<?php for ($j = 1; $j <= $number_of_rows + 1; $j++): ?>
    <div class="row schedule-row <?= $j == $number_of_rows + 1 ? "additional-row hidden-row" : "" ?>">
        <?php for ($i = 0; $i < $configuration->number_of_courts; $i++): ?>
            <?php $schedule = DrawMatchHelper::getScheduledMatch($schedules, $j, $i + 1, true) ?>
            <div
                class="clearfix court-column droppable court-drop
                            <?= ($schedule AND DrawMatchHelper::checkIfPlayerHasMoreMatches($schedules, $schedule)) ? 'state-warning' : null ?>"
                data-courtnumber="<?= $i + 1 ?>" data-row="<?= $j ?>">
                <p class="court-title"><?= LangHelper::get('court', 'Court') ?>: <?= $i + 1 ?></p>
                <?php $selected_time = ($schedule) ? $schedule->waiting_list_type_of_time : (($j == 1) ? 1 : 3) ?>
                <?= Form::select('type_of_time', $time_types, $selected_time, array('required', 'class' => 'type_of_time')); ?>
                <input type="text" name="timepicker"
                       class="time_element <?= ($selected_time == 3) ? 'hide-time' : false ?>"
                       value="<?= ($schedule) ? $schedule->getTime() : $timer ?>"/>
                <span class="text"></span>
                <?php if ($schedule): ?>
                    <div class="draggable team ui-draggable ui-draggable-handle" id="draggable_<?= $schedule->match_id?>"
                         data-matchid="<?= $schedule->match_id ?>"
                         data-drawid="<?= $schedule->draw_id ?>"
                         data-listtype="<?= $schedule->list_type ?>">

                        <a class="delete-schedule" data-id="<?= $schedule->match_id?>"
                           data-message="<?= LangHelper::get('remove_match', 'Remove match')?>?"
                           data-tournamentid="<?= $schedule->tournament_id?>"
                           href="/schedule/schedule/<?= $schedule->match_id?>">x</a>
                        <div class="match_id bottom"><?= $schedule->match_id ?></div>
                        <?= DrawMatchHelper::scheduleName($schedule->match, 'team1_data', $all_matches) ?>
                        <span class="small">vs</span>
                        <?= DrawMatchHelper::scheduleName($schedule->match, 'team2_data', $all_matches, true) ?>
                    </div>
                    <div style="margin:10% 0 0 30%"><?=$schedule->status() ?></div>
                <?php endif ?>
            </div>
        <?php endfor ?>
    </div>
    <?php $timer = ScheduleHelper::updateTimer($timer, $configuration->rest_time) ?>
<?php endfor ?>