<div class="container ang">
    <div class="grid" ng-controller="bracketController">

    <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="row">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('schedule_preview', 'Schedule preview') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <p class="yellow-color">
                <?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?>
            </p>
        </div>
        <a href="<?= URL::to("/schedule/order-print/" . $tournament->id . "#print") ?>">
            <input type="button" value="Print order of play" class="btn btn-primary printOOP hideResponsive"/>
        </a>
        <a href="<?= URL::to("signups?open=" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('signups', 'Signups') ?>" class="btn btn-primary"/>
        </a>

        <a href="<?= URL::to("schedule/index/" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('administration', 'Administration') ?>" class="btn btn-primary"/>
        </a>

        <a href="<?= URL::to("/tournaments/draws/" . $tournament->id) ?>">
            <input type="button" value="<?= LangHelper::get('draws_page', 'Draws page') ?>" class="btn btn-primary"/>
        </a>

        <div class="row clearfix">
            <h3 class="place-left">
                <?= LangHelper::get('order_of_play', 'Order of play') ?>
            </h3>
        </div>

        <div class="orderOfplay gap0">
            <?php $counter = 0;?>
            <?php foreach ($dates_array as $key => $dates): ?>
                <div class="button date_select
                    <?= ($key == Request::query('date') OR (!Request::query('date') AND $counter == 0)) ? 'selected' : '' ?>"
                     data-id="<?= $key ?>">
                    <a href="/schedule/show/<?= $tournament->id ?>?date=<?= $key ?>">
                        <div class="timeTextD"><?= date("j", strtotime($key)); ?></div>
                        <div class="timeTextM"><?= date("M", strtotime($key)); ?></div>
                        <div class="timeTextY"><?= date("Y", strtotime($key)); ?></div>
                    </a>
                </div>
                <?php $counter++?>
            <?php endforeach ?>
        </div>

        <div class="list-of-courts">
            <?php for ($j = 1; $j <= $number_of_rows; $j++): ?>
                <div class="row schedule-row">
                    <?php for ($i = 0; $i < $configuration->number_of_courts; $i++): ?>
                        <?php $schedule = DrawMatchHelper::getScheduledMatch($schedules, $j, $i + 1) ?>
                        <div class="clearfix court-column droppable court-drop preview-box box-size-main">
                            <?php if(isset($schedule->match->id)): ?>
                            <small class="enter_score_order_of_play yellow-color" ng-click="openModal('<?= $schedule->match->id ?>')" >Score</small>
                            <?php endif ?>
                            <p class="schedule-show-court-title"><?= LangHelper::get('court', 'Court') ?>: <?= $i + 1 ?></p>

                            <?php if ($schedule): ?>
                                <p class="schedule-showtime">
                                    <?= $schedule->type_of_time() ?>
                                    <?php if ($schedule->type_of_time != 3):?>
                                        :<?= $schedule->getTime()?>
                                    <?php endif?>
                                </p>
                                <div class="order-of-p team">
                                    <?= DrawMatchHelper::scheduleName($schedule->match, 'team1_data', $all_matches) ?>
                                    <?= TournamentScoresHelper::getOrderOfPlayScores($schedule->match_id)?>
                                    <?= DrawMatchHelper::scheduleName($schedule->match, 'team2_data', $all_matches, true) ?>
                                </div>


                                <div class="yellow-color" style="text-align:center; margin-top:8% "><?= $schedule->status() ?></div>
                            <?php endif ?>
                        </div>
                    <?php endfor ?>
                </div>
            <?php endfor ?>
        </div>
        <script type="text/ng-template" id="myModalContent.html">
            <?= $results_form_view ?>
        </script>

    </div>

</div>

<script src="/js/ang/bracket.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['bracket']);
    });
</script>