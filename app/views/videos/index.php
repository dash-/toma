<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">

                <h3>
                    <?= LangHelper::get('video_tutorials', 'Video tutorials') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <table class="video-tutorials-table">
                    <tr>
                        <td class="title">Title</td>
                        <td>Link</td>
                    </tr>
                    <tr>
                        <td>Add users regional admin</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=15XaBKjlV8I">https://www.youtube.com/watch?v=15XaBKjlV8I</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Create New Tournament</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=5yb4PWtbq1Y">https://www.youtube.com/watch?v=5yb4PWtbq1Y</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Approve Tournament</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=11Goj7X2k3c">https://www.youtube.com/watch?v=11Goj7X2k3c</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Create Tournament Schedules</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=viifbQWHJz0">https://www.youtube.com/watch?v=viifbQWHJz0</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Edit Tournament add Notes</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=Fdy0zu5PLEI">https://www.youtube.com/watch?v=Fdy0zu5PLEI</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Add Players to Draw</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=79x4v7klPVU">https://www.youtube.com/watch?v=79x4v7klPVU</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Enter Scores</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=s056CM4_Ct4">https://www.youtube.com/watch?v=s056CM4_Ct4</a>
                        </td>
                    </tr>
                    <tr>
                        <td>Create Fase Previa Draws</td>
                        <td>
                            <a target="_blank" href="https://www.youtube.com/watch?v=haCgsM2TnFA">https://www.youtube.com/watch?v=haCgsM2TnFA</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>