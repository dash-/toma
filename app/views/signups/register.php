<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('signup_for_tournament', 'Signup for tournament')?></h3>
                <span class="line"></span>
                <h3><?= $tournament->title?></h3>
                <h3>
                    <?= LangHelper::get('signup_count', 'Signup count')?>: <?= $signups_count?>
                </h3>
            </div>

            <div class="row tournament-first-links place-left registerResponsive">
                <a class="rfet-button rfet-yellow call" href="<?= URL::to('signups/bulk-import', $draw->id)?>">
                    <?= LangHelper::get('bulk_import', 'Bulk import')?>
                </a>
                <br/>
                <a class="rfet-button rfet-yellow call gap-top20 place-left" href="<?= URL::to('signups/file-pairs', $draw->id)?>">
                    <?= LangHelper::get('draw_import', 'Draw import')?>
                </a>
            </div>
        </div>

        <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                    <p>
                        <span class="color-rfet"><?= LangHelper::get('category', 'Category') ?>
                            :</span> <?= $draw->category->name.''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('type', 'Type') ?>:</span> <?= $draw->typeName() ?>

                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('gender', 'Gender') ?>
                            :</span> <?= $draw->genderName() ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('total_acceptance', 'Total acceptances') ?>
                            :</span> <?= $draw->size->total ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances') ?>
                            :</span> <?= $draw->size->direct_acceptances ?>
                    </p>
                </div>
                <div class="half">
                    <p>
                        <span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers') ?>
                            :</span> <?= $draw->size->accepted_qualifiers ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards') ?>
                            :</span> <?= $draw->size->wild_cards ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts') ?>
                            :</span> <?= $draw->size->onsite_direct ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('onsite_direct', 'Onsite direct') ?>
                            :</span> <?= $draw->size->onsite_direct ?>
                    </p>

                    <p>
                        <span class="color-rfet"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?>
                            :</span> <?= $draw->qualification->draw_size ?>
                    </p>
                </div>
            </div>
            <div class="row" ng-controller="checkController">
                <div class="signup-form-holder">
                    <?php if ($draw->draw_type == 'singles'): ?>
                        <?= View::make('signups/singles_form', array('draw' => $draw,)) ?>
                    <?php else: ?>
                        <?= View::make('signups/doubles_form', array('draw' => $draw,)) ?>
                    <?php endif ?>
                </div>
            </div>

            <div class="row gap-top20 pad-top20">
                <table class="table bg-dark tournament-table">
                    <thead>
                        <tr>
                            <th class="text-left"><?= LangHelper::get('surname', 'Surname')?></th>
                            <th class="text-left"><?= LangHelper::get('name', 'Name')?></th>
                            <th class="text-left"><?= LangHelper::get('licence_number', 'Licence number')?></th>
                            <th class="text-left"><?= LangHelper::get('options', 'Options')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($signups as $signup):?>
                            <tr>
                                <td><?= TournamentHelper::getPlayerValue($signup, 'surname') ?></td>
                                <td><?= TournamentHelper::getPlayerValue($signup, 'name') ?></td>
                                <td><?= TournamentHelper::getPlayerValue($signup, 'licence_number') ?></td>
                                <td class="transparent"><a class="button small rfet-yellow pad-top15 hideOptions"
                                       href="<?= URL::to('signups/delete-signup/' . $signup->id) ?>">
                                        <?= LangHelper::get('remove', 'Remove') ?>
                                    </a></td>
                            </tr>
                        <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>


<script src="/js/ang/signupForm.js"></script>
<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['signupForm']);
    });
</script>