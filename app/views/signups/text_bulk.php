<form name="formCheck" ng-submit="textBulk(<?= $draw->id?>)" novalidate>

  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <fieldset>
        <div class="form-control">
            <label for="">Enter licence numbers separated with ; OR separated with NEW LINE</label>
            <div class="input-control textarea">
                <textarea type="text" id="bulk" required ng-model="formData.bulk" rows="20"></textarea>
            </div>
        </div>
     <button type="submit" ng-disabled="!formCheck.$valid" class="primary submit-btn"><?= LangHelper::get('import', 'Import')?></button>
  </fieldset>
</form>

<div class="row">
    <p class="yellow-color">{{ msg }}</p>
    
    <div ng-show="fails.length > 0">
        <p class="red-color gap-bottom0"><?= LangHelper::get('list_of_fail_players', 'List of not inserted players')?></p>
        <div class="cleafix pad-top0" ng-repeat="player in fails track by $index">
            <a target="_blank" class="courts-view" href="/players/info/{{player.player_id}}">
                {{ player.full_name }} - {{ player.licence_number }} - {{player.reason}}
            </a>
        </div>
    </div>
</div>

<div class="row gap-top20">
    <div class="form-control gap-bottom20">
        <label style="min-height: 10px;">
            <?= LangHelper::get('choose_csv_file', 'Choose CSV file')?> - <?= LangHelper::get('auto_triggered_upload', 'Upload is auto-triggered once document is selected')?>
            <span class="form-error">{{ errors.text }}</span>
        </label>
        <div class="input-control">

            <input type="file" ng-model="formData.file" ng-file-select="onFileSelect($files, <?= $draw->id?>)">
        </div>
    </div>
</div>