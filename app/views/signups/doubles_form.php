<div class="row">
    <div class="half pad-right10">
        <form name="formCheck" ng-submit="checkLicence(<?= $draw->category->id?>, <?= $draw->id?>, true, 1)" novalidate action="">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
            <div class="form-control">
                <div class="yellow-color">Enter '*' in the beginning of the work to search from both sides</div>
                <label for="licence_number"><?= LangHelper::get('licence_number_for_first_player', 'Licence number for first player')?></label>
                <div class="input-control text">
                    <input type="text" ng-class="{}" ng-model="formData.player.licence_number" placeholder="enter licence number or name" typeahead="gamer.licence_number as gamer.full_name for gamer in getPlayers($viewValue, false, '<?= $draw->draw_gender?>', '<?= $draw->draw_category_id?>', <?= $draw->tournament_id?>) | filter:$viewValue" typeahead-on-select="checkLicence(<?= $draw->category->id?>, <?= $draw->id?>)" typeahead-min-length="3">
                </div>
            </div>
            </fieldset>
            <button type="submit" class="primary"><?= LangHelper::get('find', 'Find')?></button>
            <a ng-click="showSignupForm($event, 1)" href="#" class="primary" style="display: inherit;"><?= LangHelper::get('enter_information_manually', 'Enter information manually')?></a>
        </form>
    </div>
    <div class="half pad-left10">
        <form name="formCheck" ng-submit="checkLicence($event, <?= $draw->category->id?>, <?= $draw->id?>, true, 2)" novalidate action="">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
            <div class="form-control">
                <label for="licence_number"><?= LangHelper::get('licence_number_for_second_player', 'Licence number for second player')?></label>
                <div class="input-control text">
                    <input type="text" ng-class="{}" ng-model="formData.player2.licence_number" placeholder="enter licence number or name" typeahead="gamer2.licence_number as gamer2.full_name for gamer2 in getPlayers($viewValue, true, '<?= $draw->draw_gender?>', '<?= $draw->draw_category_id?>', <?= $draw->tournament_id?>) | filter:$viewValue" typeahead-on-select="checkLicence(<?= $draw->category->id?>, <?= $draw->id?>, true, 2)" typeahead-min-length="3">
                </div>
            </div>
            </fieldset>
            <button type="submit" class="primary"><?= LangHelper::get('find', 'Find')?></button>
            <a ng-click="showSignupForm($event, 2)" href="#" class="primary"><?= LangHelper::get('enter_information_manually', 'Enter information manually')?></a>
        </form>
        
    </div>
</div>
     
<div class="player-data" ng-show="showInfo">
    <form name="formCheck" ng-submit="signupForm($event, '<?= $draw->id?>', false, true)" novalidate action="">
    <div class="row">
        <div class="half">
            <h3><?= LangHelper::get('first_player_information', 'First player information')?></h3>
            <p class="fg-white">
                <?= LangHelper::get('name', 'Name')?>: {{ player.contact.name}} - <a ng-click="showInput($event, 'name')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.name">
                    <div class="input-control text">
                        <input type="text" ng-model="player.contact.name" required>
                    </div>
                    <a class="button default" ng-click="showInput($event, 'name')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('surname', 'Surname')?>: {{ player.contact.surname}} - <a ng-click="showInput($event, 'surname')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.surname">
                    <div class="input-control text">
                        <input type="text" ng-model="player.contact.surname" required>
                    </div>
                    <a class="button default" ng-click="showInput($event, 'surname')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('date_of_birth', 'Date of birth')?>: {{ player.contact.date_of_birth}} - <a ng-click="showInput($event, 'date_of_birth')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.date_of_birth">
                    <div class="input-control text">
                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="player.contact.date_of_birth" is-open="opened['date_of_birth']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'date_of_birth')" formatdate>

                    </div>
                    <a class="button default" ng-click="showInput($event, 'date_of_birth')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('email', 'Email')?>: {{ player.contact.email}} - <a ng-click="showInput($event, 'email')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.email">
                    <div class="input-control text">
                        <input type="text" ng-model="player.contact.email">
                    </div>
                    <a class="button default" ng-click="showInput($event, 'email')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('contact_phone_number', 'Contact phone number')?>: {{ player.contact.phone}} - <a ng-click="showInput($event, 'phone')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.phone">
                    <div class="input-control text">
                        <input type="text" ng-model="player.contact.phone">
                    </div>
                    <a class="button default" ng-click="showInput($event, 'phone')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
        </div>
        <div class="half">
            <h3><?= LangHelper::get('second_player_information', 'Second player information')?></h3>
            <p class="fg-white">
                <?= LangHelper::get('name', 'Name')?>: {{ player2.contact.name}} - <a ng-click="showInput($event, 'name2')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.name2">
                    <div class="input-control text">
                        <input type="text" ng-model="player2.contact.name" required>
                    </div>
                    <a class="button default" ng-click="showInput($event, 'name2')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('surname', 'Surname')?>: {{ player2.contact.surname}} - <a ng-click="showInput($event, 'surname2')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.surname2">
                    <div class="input-control text">
                        <input type="text" ng-model="player2.contact.surname" required>
                    </div>
                    <a class="button default" ng-click="showInput($event, 'surname2')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('date_of_birth', 'Date of birth')?>: {{ player2.contact.date_of_birth}} - <a ng-click="showInput($event, 'date_of_birth2')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.date_of_birth2">
                    <div class="input-control text">
                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="player2.contact.date_of_birth" is-open="opened['date_of_birth']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'date_of_birth')" formatdate>

                    </div>
                    <a class="button default" ng-click="showInput($event, 'date_of_birth2')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('email', 'Email')?>: {{ player2.contact.email}} - <a ng-click="showInput($event, 'email2')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.email2">
                    <div class="input-control text">
                        <input type="text" ng-model="player2.contact.email">
                    </div>
                    <a class="button default" ng-click="showInput($event, 'email2')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
            <p class="fg-white">
                <?= LangHelper::get('contact_phone_number', 'Contact phone number')?>: {{ player2.contact.phone}} - <a ng-click="showInput($event, 'phone2')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
                <div class="form-control" ng-show="input.phone2">
                    <div class="input-control text">
                        <input type="text" ng-model="player2.contact.phone">
                    </div>
                    <a class="button default" ng-click="showInput($event, 'phone2')" href="#"><?= LangHelper::get('close', 'Close')?></a>
                </div>
            </p>
        </div>
    
    </div>
    <button type="submit" class="primary"><?= LangHelper::get('signup', 'Signup')?></button>

    </form>
</div>
<div class="no-info">
    <div class="row">
        <form name="formCheck" ng-submit="signupForm($event, '<?= $draw->id?>', true, true)" novalidate action="">
            <div class="half pad-right10">
                <h3 ng-show="noInfo">{{message}}</h3>
                <div class="clearfix" ng-show="showForm">
                    <p class="fg-white"><?= LangHelper::get('first_player_information', 'First player information')?></p>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="form-control">
                            <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player.contact.name" name="name" required>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="surname"><?= LangHelper::get('surname', 'Surname')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player.contact.surname" name="surname" required>
                            </div>
                        </div>
                        
                        <div class="form-control">
                            <label for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth')?></label>
                            <div class="input-control text">
                                <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="player.contact.date_of_birth" is-open="opened['date_of_birth2']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'date_of_birth2')" formatdate>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player.contact.email" name="email">
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="phone"><?= LangHelper::get('contact_phone', 'Contact phone')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player.contact.phone" name="phone">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="half pad-left10">
                <h3 ng-show="noInfo">{{message2}}</h3>
                <div class="clearfix" ng-show="showForm">
                    <p class="fg-white"><?= LangHelper::get('second_player_information', 'Second player information')?></p>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="form-control">
                            <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player2.contact.name" name="name" required>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="surname"><?= LangHelper::get('surname', 'Surname')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player2.contact.surname" name="surname" required>
                            </div>
                        </div>
                        
                        <div class="form-control">
                            <label for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth')?></label>
                            <div class="input-control text">
                                <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="player2.contact.date_of_birth" is-open="opened['date_of_birth3']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'date_of_birth3')" formatdate>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player2.contact.email" name="email">
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="phone"><?= LangHelper::get('contact_phone', 'Contact phone')?></label>
                            <div class="input-control text">
                                <input type="text" ng-class="{}" ng-model="player2.contact.phone" name="phone">
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row" ng-show="showForm">
                <button type="submit" class="primary"><?= LangHelper::get('signup', 'Signup')?></button>
            </div>
        </form>
    </div>  
</div>


