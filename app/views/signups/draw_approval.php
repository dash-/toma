<table class="table bg-dark tournament-table tournamentIndexResponsiveScroll" style="margin-top: 70px">
    <thead>
    <tr>
        <th class="text-left"><?= LangHelper::get('tournament', 'Tournament')?></th>
        <th class="text-left"><?= LangHelper::get('category', 'Category')?></th>
        <th class="text-left"><?= LangHelper::get('type', 'Type')?></th>
        <th class="text-left"><?= LangHelper::get('gender', 'Gender')?></th>
        <th class="text-left"><?= LangHelper::get('approval_status', 'Approval status')?></th>
        <th class="text-right" width="45%"><?= LangHelper::get('options', 'Options') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($regional_draws as $draw):?>
        <?php $tournament = $draw->tournament;?>
        <tr>
            <td><?= $draw->title ?></td>
            <td><?= $draw->category->name.''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?></td>
            <td><?= $draw->typeName() ?></td>
            <td><?= $draw->genderName() ?></td>
            <td><?= $draw->approvalStatus()?></td>
            <td class="transparent text-right">
                <?php if(Auth::getUser()->hasRole('regional_admin') && $draw->approval_status == 1): ?>
                    <a class="call rfet-button rfet-yellow center-text"
                       href="/signups/regional-validate/<?= $draw->id ?>">
                        <?= LangHelper::get('validate_draw', 'Validate draw') ?>
                    </a>
                <?php endif ?>
                <a class="rfet-button rfet-yellow call"
                   href="/tournaments/draw-details/<?= $draw->id ?>"><?= LangHelper::get('details', 'Details') ?></a>
                <a class="rfet-button rfet-yellow call"
                   href="/tournaments/edit-draw/<?= $draw->id ?>"><?= LangHelper::get('edit', 'Edit') ?></a>
                <a class="call rfet-button rfet-yellow center-text" data-title="List of signed up players"
                   href="/signups/list/<?= $draw->id ?>">
                    <?= LangHelper::get('players_signed', 'Players') ?>
                </a>
                <a ng-show="draw.signups.length > 0" class="rfet-button rfet-yellow call"
                   data-title="Final listing"
                   href="/signups/final-list/<?= $draw->id ?>"><?= LangHelper::get('view_final_listing', 'View final listing') ?></a>
            </td>

        </tr>
    <?php endforeach ?>

    </tbody>