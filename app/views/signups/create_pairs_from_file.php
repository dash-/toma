<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('pairs_from_file', 'Pairs from file') ?></h3>
                <span class="line"></span>

                <h3><?= $draw->tournament->title ?></h3>

                <h3>
                    <?= LangHelper::get('signup_count', 'Signup count') ?>: <?= $draw->signups->count() ?>
                </h3>
            </div>
        </div>

        <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('category', 'Category') ?>
                            :</span> <?= $draw->category->name ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('type', 'Type') ?>:</span> <?= $draw->typeName() ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('gender', 'Gender') ?>
                            :</span> <?= $draw->genderName() ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('total_acceptance', 'Total acceptances') ?>
                            :</span> <?= $draw->size->total ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances') ?>
                            :</span> <?= $draw->size->direct_acceptances ?></p>
                </div>
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers') ?>
                            :</span> <?= $draw->size->accepted_qualifiers ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards') ?>
                            :</span> <?= $draw->size->wild_cards ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts') ?>
                            :</span> <?= $draw->size->onsite_direct ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('onsite_direct', 'Onsite direct') ?>
                            :</span> <?= $draw->size->onsite_direct ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?>
                            :</span> <?= $draw->qualification->draw_size ?></p>
                </div>
            </div>
            <div class="row" ng-controller="fileController">
                <div class="pad10">
                    <p class="danger-color">{{ errorList.list_type }}</p>
                    <form name="formCheck" ng-submit="pairsImport(<?= $draw->id ?>)" novalidate>

                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <div class="form-control">
                                <label for="">Paste created pairs from the excel sheet</label>

                                <div class="input-control textarea">
                                    <textarea type="text" id="bulk" required ng-model="formData.bulk"
                                              rows="30"></textarea>
                                </div>
                            </div>

                            <div class="clearfix">
                                <div class="form-control place-left gap-right20">
                                    <input id="main_draw" class="checkbox-ang" type="radio" ng-model="formData.list_type" value="1">
                                    <label class="place-left" for="main_draw"><?= LangHelper::get('main_draw', 'Main draw') ?></label>
                                </div>

                                <div class="form-control place-left">
                                    <input id="quali_draw" class="checkbox-ang" type="radio" ng-model="formData.list_type" value="2">
                                    <label class="place-left" for="quali_draw"><?= LangHelper::get('qualifications_draw', 'Qualifications draw') ?></label>
                                </div>
                            </div>
                            <button type="submit" ng-disabled="!formCheck.$valid"
                                    class="primary submit-btn"><?= LangHelper::get('import', 'Import') ?></button>
                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


<script src="/js/ang/signupForm.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['signupForm']);
    });
</script>