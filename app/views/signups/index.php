<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">

                <h3>
                    <?= LangHelper::get('signups_and_draws', 'Signups & draws') ?>
                    - <?= LangHelper::get('draws_count', 'Draws count') ?>: <?= $draws_count ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row filter-row">
            <div class="clearfix search-form-holder">
                <form action="<?= URL::current() ?>" method="GET">
                    <input type="hidden" name="search" value="1">

                    <div class="form-control place-left">
                        <div class="input-control text">
                            <input type="text" name="title" placeholder="Enter title"
                                   value="<?= Request::query('title') ?>">
                        </div>
                    </div>
                    <div class="form-control place-left">
                        <?= Form::select('year', DateTimeHelper::getYears(), Request::query('year'), array('class' => 'select2')); ?>
                    </div>

                    <div class="form-control place-left">
                        <?= Form::select('month', DateTimeHelper::getMonths(), Request::query('month'), array('class' => 'select2')); ?>
                    </div>

                    <?php if ($regions_array): ?>
                        <div class="form-control place-left">
                            <?= Form::select('region', $regions_array, Request::query('region'), array('class' => 'select2')); ?>
                        </div>
                    <?php endif ?>

                    <div class="form-control submit-type place-left tournamentButtonIndex">
                        <button type="submit" class="button clear primary large dark">
                            <?= LangHelper::get('search', 'Search') ?>
                        </button>
                    </div>
                    <div class="form-control submit-type place-right">
                        <a href="<?= URL::current() ?>" class="button clear primary large yellowBtnSignups">
                            <?= LangHelper::get('reset_search', 'Reset search') ?>
                        </a>
                    </div>
                </form>
            </div>

        </div>

        <div class="row pad-top20 signups-accordion" ng-controller="SignupsListController">
            <accordion>
                <?php foreach ($tournaments as $key => $tournament): ?>
                    <accordion-group
                        is-open="<?= (Request::query('open')) ? ($tournament->id == Request::query('open')) : false ?>">
                        <accordion-heading>

                            <div class="aHeading" ah-id=<?= "ah" . $key ?>>
                                <?= $tournament->title ?>
                                - <?= LangHelper::get('main_draw_start', 'Main draw start') ?>
                                : <?= $tournament->getDate('main_draw_from') ?>
                                - <?= LangHelper::get('currently_signed_up', 'Currenly signed up') ?>
                                : <?= $tournament->signups->count() ?>
                                <div class="toggleAccordion" id=<?= "ah" . $key ?>>+</div>
                                <i class="pull-right"></i>
                            </div>
                        </accordion-heading>
                        <p class="panel-content-title">
                            Draws
                            <?php if (TournamentHelper::checkIfTournamentHasMatches($tournament->id)):?>
                                <a style="padding: 10px 5px;height: auto; font-size:14px;" class="rfet-button rfet-yellow place-right gap-left10"
                                   href="/schedule/index/<?= $tournament->id ?>">
                                    <?= LangHelper::get('schedule_administration', 'Schedule administration') ?>
                                </a>
                            <?php endif?>

                            <?php if (DrawMatchHelper::scheduleExist($tournament->schedules, 2)): ?>
                                <a style="padding: 10px 5px;height: auto; font-size:14px;" class="rfet-button rfet-yellow place-right"
                                   href="/schedule/show/<?= $tournament->id ?>">
                                    <?= LangHelper::get('show_tournament_schedule', 'Show tournament schedule') ?>
                                </a>
                            <?php endif ?>

                        </p>

                        <table class="table bg-dark tournament-table">
                            <thead>
                            <tr>
                                <th class="text-left"><?= LangHelper::get('status', 'Status') ?></th>
                                <th class="text-left"><?= LangHelper::get('category', 'Category') ?></th>
                                <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                                <th class="text-left"><?= LangHelper::get('gender', 'Gender') ?></th>
                                <th class="text-left"><?= LangHelper::get('main_draw_size', 'Main draw size') ?></th>
                                <th class="text-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?></th>
                                <th class="text-left"><?= LangHelper::get('unique_draw_id', 'Unique Draw ID') ?></th>
                                <th class="text-right"
                                    width="11%"><?= LangHelper::get('players_signed', 'Players') ?></th>
                                <th class="text-right" width="36%"><?= LangHelper::get('options', 'Options') ?></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($tournament->draws as $draw): ?>
                                <tr>
                                    <td style="<?= (TournamentHelper::mainDrawFinished($draw->id, $draw_history)) ? 'border-left: 3px solid #449d44' : null ?>">
                                        <?= TournamentDrawHelper::drawStatus($draw, $draws_with_matches_list, $tournament->date->entry_deadline, $draw_history) ?>
                                    </td>
                                    <td>
                                        <?= $draw->category->name.''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?>
                                    </td>
                                    <td><?= $draw->typeName() ?></td>
                                    <td><?= $draw->genderName() ?></td>
                                    <td><?= $draw->size ? $draw->size->total : 0 ?></td>
                                    <td><?= $tournament->has_qualifying && $draw->qualification ? $draw->qualification->draw_size : 0 ?></td>
                                    <td><?= $draw->unique_draw_id ?></td>
                                    <!-- players -->
                                    <td class="transparent text-right">
                                        <a class="call rfet-button rfet-yellow center-text"
                                           data-title="List of signed up players" style="width: 100%"
                                           href="<?= URL::to('signups/list', $draw->id) ?>">
                                            <?= LangHelper::get('players_signed', 'Players') ?> <?= $draw->signups->count() ?>
                                        </a>
                                    </td>
                                    <!-- options -->
                                    <td class="transparent text-right">
                                        <?php if ($draw->signups->count() > 0): ?>
                                            <a class="rfet-button rfet-yellow" data-title="Final listing"
                                               href="<?= URL::to('signups/final-list', $draw->id) ?>"><?= LangHelper::get('create_edit_draw', 'Create/Edit draw') ?></a>
                                        <?php endif; ?>

                                        <?php if (TournamentHelper::drawHasMatches($draw->id, 2, $draws_with_matches_list)): ?>
                                            <a class="button small rfet-yellow pad-top0"
                                               href="/draws/db-bracket/<?= $draw->id ?>?list_type=2">
                                                <?= LangHelper::get('view_qualify_bracket', 'View qualify bracket') ?>
                                            </a>
                                        <?php endif ?>

                                        <?php if (TournamentHelper::drawHasMatches($draw->id, 1, $draws_with_matches_list)): ?>
                                            <a class="button small rfet-yellow pad-top0 es-width"
                                               href="/draws/db-bracket/<?= $draw->id ?>?list_type=1">
                                                <?= LangHelper::get('view_main_bracket', 'View main bracket') ?>
                                            </a>
                                        <?php endif ?>

                                        <a class="rfet-button rfet-yellow call" data-title="Signup for tournament"
                                           href="<?= URL::to('signups/register', $draw->id) ?>">
                                            <?= LangHelper::get('register', 'Register') ?>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ?>


                            </tbody>
                        </table>

                    </accordion-group>
                <?php endforeach ?>
            </accordion>

        </div>

        <div class="pagination text-center">
            <?= $tournaments->appends(Request::query())->links() ?>
        </div>
    </div>


</div>

<script src="/js/ang/signupsList.js"></script>
<script>
    $(document).ready(function () {
        $("select").select2({});
    });
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['signupsList']);
    });

</script>


