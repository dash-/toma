<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="clearfix row">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?></h3>
                <span class="line"></span>

                <h3><?= LangHelper::get('draw_category', 'Draw category') ?>
                    : <?= $draw->category->name . ', ' . $draw->typeName() .''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?></h3>

                <h3><?= LangHelper::get('number_of_signups', 'Number of signups') ?>: <?= count($players) ?></h3>

                <h3><?= LangHelper::get('main_draw_wild_cards', 'Main draw wild cards') ?>
                    : <?= $wild_cards_count ?></h3>

                <a href="#" class="rfet-button rfet-grey printSignupList visibleResponsive">
                    <?= LangHelper::get('print_signup_list', 'Print signup list') ?>
                </a>
            </div>

            <div class="clearfix place-right" style="width: 200px">
                <div class="form-control">
                    <?= Form::select('list_type', $types_array, Request::query('list_type'), array('class' => 'select2 select_list_type')); ?>
                </div>
            </div>

            <div class="clearfix place-right listResponsive">
                <?php if ($qualifiyng_draw_matches_exists): ?>
                    <a class="rfet-button rfet-yellow place-right gap-right10"
                       href="/draws/db-bracket/<?= $draw->id ?>?list_type=2">
                        <?= LangHelper::get('view_qualify_bracket', 'View qualify bracket') ?>
                        <img src="/css/icons/draws.png" alt="">
                    </a>
                <?php endif ?>
                <?php if ($main_draw_matches_exists): ?>
                    <a class="rfet-button rfet-yellow place-right gap-right10"
                       href="/draws/db-bracket/<?= $draw->id ?>?list_type=1">
                        <?= LangHelper::get('view_main_bracket', 'View main bracket') ?>
                        <img src="/css/icons/draws.png" alt="">
                    </a>
                <?php endif ?>

                <?php if (($qualification_finished OR TournamentHelper::checkIfThereIsntQualificationDraw($draw))): ?>
                    <?php if(!$main_draw_matches_exists): ?>
                    <a class="rfet-button rfet-danger place-right gap-right10"
                       href="/draws/manual-bracket/<?= $draw->id ?>?list_type=1">
                        <?= LangHelper::get('create_main_draw', 'Create main draw') ?>
                    </a>
                    <?php endif ?>
                <?php else: ?>
                    <?php if (count($players)): ?>

                        <a class="rfet-button rfet-danger place-right gap-right10"
                           href="/draws/manual-bracket/<?= $draw->id ?>?list_type=2">
                            <?= LangHelper::get('create_qualification_draw', 'Create qualification draw') ?>
                        </a>
                    <?php endif ?>
                <?php endif ?>

            </div>
            <?php if(Auth::getUser()->hasRole('superadmin') || $tournament->status != 1): ?>
                <?php if (!$qualifiyng_draw_matches_exists AND !$main_draw_matches_exists): ?>
                    <div class="clearfix place-right">
                        <a class="metro button bigger yellow-col-bg right call gap-right10"
                           href="<?= URL::to('signups/register', $draw->id) ?>">
                            <?= LangHelper::get("register_new_player", "Register New Player") ?>
                        </a>
                    </div>
                <?php endif ?>
            <?php endif ?>

            <?php if ($draw->draw_type == 'doubles'): ?>
                <a class="metro button bigger yellow-col-bg right call gap-right10"
                   href="<?= URL::to('signups/generate-doubles', $draw->id) ?>">
                    <?= LangHelper::get("generate_doubles_pairs", "Generate doubles pairs") ?>
                </a>
            <?php endif ?>
        </div>
        <div class="row pad-top20 tabScrl clearfix clear" ng-controller="SignupsListController">
            <div class="panel-body" id="signup-list-printable">
                <div class="hideForPrint">
                    <span><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?> <br></span>
                    <span><?= LangHelper::get('draw_category', 'Draw category') ?>: <?= $draw->category->name.''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?>
                        <br></span>
                    <span><?= LangHelper::get('number_of_signups', 'Number of signups') ?>: <?= count($players) ?> <br></span>
                </div>
                <div class="row pad-top20 tabScrl clearfix clear" ng-controller="SignupsListController">
                    <div id="signup-list-printable">
                        <div class="hideForPrint">
                            <span><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?>
                                <br></span>
                    <span><?= LangHelper::get('draw_category', 'Draw category') ?>: <?= $draw->category->name ?>
                        <br></span>
                            <span><?= LangHelper::get('number_of_signups', 'Number of signups') ?>
                                : <?= count($players) ?> <br></span>
                        </div>
                        <table class="table bg-dark tournament-table signupsList">
                            <thead>
                            <tr>
                                <th class="text-left fiveCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "surname") ?>">
                                        <?= LangHelper::get('surname', 'Surname') ?>
                                    </a>
                                </th>
                                <th class="text-left fiveCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "name") ?>">
                                        <?= LangHelper::get('name', 'Name') ?>
                                        <?= PageHelper::returnClassAndDirection("name") ?>
                                    </a>
                                </th>
                                <th class="text-left fiveCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "licence_number") ?>">
                                        <?= LangHelper::get('licence_number', 'Licence number') ?>
                                        <?= PageHelper::returnClassAndDirection("licence_number") ?>
                                    </a>
                                </th>
                                <th class="text-left fiveCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "date_of_birth") ?>">
                                        <?= LangHelper::get('date_of_birth', 'Date of birth') ?>
                                        <?= PageHelper::returnClassAndDirection("date_of_birth") ?>
                                    </a>
                                </th>
                                <th class="text-left fiveCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "ranking_points") ?>">
                                        <?= PageHelper::returnClassAndDirection("ranking_points") ?>

                                        <?= LangHelper::get('ranking_points_1', 'Ranking') ?><br/>
                                        <?= LangHelper::get('ranking_points_2', 'points') ?>
                                    </a>
                                </th>
                                <th class="text-left sixthCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "status") ?>">
                                        <?= LangHelper::get('status', 'Status') ?>
                                        <?= PageHelper::returnClassAndDirection("status") ?>

                                    </a>
                                </th>
                                <th class="text-left sixthCol no-color">
                                    <a class="call" href="<?= PageHelper::createSignupListLink($draw->id, "draw") ?>">
                                        <?= LangHelper::get('draw', 'Draw') ?>
                                        <?= PageHelper::returnClassAndDirection("draw") ?>

                                    </a>
                                </th>
                                <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                    <th width="47%"
                                        class="text-right hideOptions"><?= LangHelper::get('options', 'Options') ?></th>
                                <?php endif ?>
                            </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($players as $player): ?>
                                <tr>
                                    <td><?= $player['wild_card_status'] ?>
                                        <?= $player['surname'] ?></td>
                                    <td><?= $player['name'] ?></td>
                                    <td><?= $player['licence_number'] ?></td>
                                    <td><?= $player['date_of_birth'] ?></td>
                                    <td><?= $player['ranking_points'] ?></td>
                                    <td><?= $player['status_name'] ?></td>
                                    <td style="color:<?= $player['list_color'] ?>">
                                        <?php if ($qualifiyng_draw_matches_exists AND $player['list_type'] == 2): ?>
                                            <a class="link-underline"
                                               href="/draws/db-bracket/<?= $draw->id ?>?list_type=2"
                                               style="color:<?= $player['list_color'] ?>">
                                                <?= $player['list_type_name'] ?>
                                            </a>
                                        <?php elseif ($qualifiyng_draw_matches_exists AND $player['list_type'] == 1): ?>
                                            <a class="link-underline"
                                               href="/draws/db-bracket/<?= $draw->id ?>?list_type=1"
                                               style="color:<?= $player['list_color'] ?>">
                                                <?= $player['list_type_name'] ?>
                                            </a>
                                        <?php
                                        else: ?>
                                            <?= $player['list_type_name'] ?>
                                        <?php endif ?>
                                    </td>
                                    <!-- options -->
                                    <td class="transparent text-right">
                                        <?php //TODO: CHECK EXACT LOGIC FOR THIS ?>
                                        <?php if (!$player['status']): ?>
                                            <div class="button-dropdown text-left">
                                                <button
                                                    class="dropdown-toggle public-toggle rfet-yellow"><?= LangHelper::get('status', 'Status') ?></button>
                                                <ul class="dropdown-menu" data-role="dropdown">
                                                    <?php if ($player['list_type'] != 1): ?>
                                                        <li>
                                                            <a href="/signups/add-status/<?= $player['id'] ?>?status=1"><?= LangHelper::get('wild_card_main', 'Wild card main draw') ?></a>
                                                        </li>
                                                        <li>
                                                            <a href="/signups/add-status/<?= $player['id'] ?>?status=2"><?= LangHelper::get('wild_card_quali', 'Wild card qualification draw') ?></a>
                                                        </li>
                                                    <?php endif ?>
                                                    <li>
                                                        <a href="/signups/add-status/<?= $player['id'] ?>?status=3"><?= LangHelper::get('special_exempt', 'Special exempt') ?></a>
                                                    </li>
                                                    <li>
                                                        <a href="/signups/add-status/<?= $player['id'] ?>?status=4"><?= LangHelper::get('on_site_direct', 'On site direct') ?></a>
                                                    </li>
                                                </ul>
                                            </div>

                                        <?php elseif (in_array($player['status'], [1, 2])): ?>
                                            <a class="button small rfet-yellow place-right pad-top10"
                                               href="<?= URL::to('signups/remove-status', $player['id']) ?>"
                                               style="width: 60px"><?= LangHelper::get('remove_wild_card', 'Remove wild card') ?></a>
                                        <?php endif ?>
                                        <?php if (!in_array($player['status'], [1, 2])): ?>
                                            <div class="button-dropdown text-left">
                                                <button
                                                    class="dropdown-toggle public-toggle rfet-yellow"><?= LangHelper::get('move_to', 'Move to') ?>
                                                    ...
                                                </button>
                                                <ul class="dropdown-menu" data-role="dropdown">
                                                    <li>
                                                        <a href="/signups/change-type/<?= $player['id'] ?>/1"><?= LangHelper::get('move_to_main_draw', 'Move to main draw') ?></a>
                                                    </li>
                                                    <li>
                                                        <a href="/signups/change-type/<?= $player['id'] ?>/2"><?= LangHelper::get('move_to_qualifying_draw', 'Move to qualifying draw') ?></a>
                                                    </li>
                                                    <li>
                                                        <a href="/signups/change-type/<?= $player['id'] ?>/3"><?= LangHelper::get('move_to_alternate_draw', 'Move to alternate draw') ?></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        <?php endif ?>

                                        <!-- end of checker for match exist -->
                                        <?php if ($player['list_type'] != 4): ?>
                                            <a class="button small rfet-yellow pad-top10 hideOptions"
                                               href="<?= URL::to('signups/player-details', $player['id']) ?>">
                                                Player info
                                            </a>
                                        <?php endif ?>

                                        <?php if (!$qualifiyng_draw_matches_exists AND !$main_draw_matches_exists AND $player['list_type'] != 4): ?>
                                            <a class="rfet-button rfet-danger"
                                               href="<?= URL::to('signups/withdraw', $player['id']) ?>">
                                                <?= LangHelper::get('withdraw', 'Withdraw') ?>
                                            </a>
                                        <?php endif ?>
                                        <div class="pull-right gap-left3">
                                            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_top">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="P7S95L63EKFN4">
                                                <input type="submit" class="rfet-paypal rfet-button rfet-info"
                                                       border="0" name="submit"
                                                       alt="PayPal. La forma rápida y segura de pagar en Internet."
                                                       value="<?= LangHelper::get("pay_with_paypal", "Pay with PayPal") ?>">
                                                <img alt="" border="0"
                                                     src="https://www.sandbox.paypal.com/es_ES/i/scr/pixel.gif"
                                                     width="1" height="1">
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("select").select2({});
        $("[data-role=dropdown]").dropdown();

        $('.select_list_type').on('change', function () {
            var value = $(this).val();
            var docUrl = document.URL.split("?")[0];
            console.log(docUrl);
            if (value == "")
                window.location = docUrl;
            else {
                window.location = docUrl + '?list_type=' + value;
            }
        });

    });
    angular.element(document).ready(function () {
        // angular.bootstrap('.ang', ['signupsList']);
    });
</script>