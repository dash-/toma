<div class="container ang">
    <div class="grid" ng-controller="playerActionsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('signup_team_details', 'Signup team details')?></h3>
                <span class="line"></span>
            </div>
       </div>
    
        <div class="tournament-second pad-top40 clear">
            <?php foreach ($player->teams as $player): ?>
                <div class="row players-info gap-bottom20">
                    <div class="half">
                        <p><span class="color-rfet"><?= LangHelper::get('name', 'Name')?>:</span> <?= $player->name ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('surname', 'Surname')?>:</span> <?= $player->surname ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('date_of_birth', 'Date of birth')?>:</span> <?= DateTimeHelper::GetShortDateFullYear($player->date_of_birth) ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('email', 'Email')?>:</span> <?= $player->email ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('phone', 'Phone')?>: </span><?= $player->phone ?></p>
                        
                        <?php if ($player->licence_number): ?>
                            <p><span class="color-rfet"><?= LangHelper::get('licence_number', 'Licence number')?>: </span><?= $player->licence_number ?></p>
                        <?php endif ?>
                    </div>

                    <div class="half">
                        <?php if (!$player->licence_number): ?>
                            <a class="button default" href="/players/create?team=<?= $player->id?>">Create player from this data</a>
                        <?php endif ?>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
