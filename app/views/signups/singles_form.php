<form name="formCheck" ng-submit="checkLicence(<?= $draw->category->id?>, <?= $draw->id?>)" novalidate>

  <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
  <fieldset>
        <div class="form-control">
            <div class="yellow-color">Enter '*' in the beginning of the work to search from both sides</div>
            <div class="input-control text">
                <input type="text" ng-class="{}" ng-model="formData.player.licence_number" placeholder="enter licence number or name" typeahead="gamer.licence_number as gamer.full_name for gamer in getPlayers($viewValue, false, '<?= $draw->draw_gender?>', '<?= $draw->draw_category_id?>', <?= $draw->tournament_id?>) | filter:$viewValue" typeahead-on-select="checkLicence(<?= $draw->category->id?>, <?= $draw->id?>)" typeahead-min-length="3" required>
            </div>
        </div>
     <button type="submit" class="primary"><?= LangHelper::get('find', 'Find')?></button>
     <a ng-click="showSignupForm($event)" href="#" class="primary" style="display: inherit;"><?= LangHelper::get('enter_information_manually', 'Enter information manually')?></a>
  </fieldset>
</form>

<div class="player-data" ng-show="showInfo">
    <form name="formCheck" ng-submit="signupForm($event, '<?= $draw->id?>')" novalidate  action="">
    <h3><?= LangHelper::get('your_information', 'Your information')?></h3>
        <p class="fg-white">
            <?= LangHelper::get('name', 'Name')?>: {{ player.contact.name}} - <a ng-click="showInput($event, 'name')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
            <div class="form-control" ng-show="input.name">
                <div class="input-control text">
                    <input type="text" ng-model="player.contact.name" required>
                </div>
                <a class="button default" ng-click="showInput($event, 'name')" href="#"><?= LangHelper::get('close', 'Close')?></a>
            </div>
        </p>
        <p class="fg-white">
            <?= LangHelper::get('surname', 'Surname')?>: {{ player.contact.surname}} - <a ng-click="showInput($event, 'surname')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
            <div class="form-control" ng-show="input.surname">
                <div class="input-control text">
                    <input type="text" ng-model="player.contact.surname" required>
                </div>
                <a class="button default" ng-click="showInput($event, 'surname')" href="#"><?= LangHelper::get('close', 'Close')?></a>
            </div>
        </p>
        <p class="fg-white">
            <?= LangHelper::get('date_of_birth', 'Date of birth')?>: {{ player.contact.date_of_birth}} - <a ng-click="showInput($event, 'date_of_birth')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
            <div class="form-control" ng-show="input.date_of_birth">
                <div class="input-control text">
                    <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="player.contact.date_of_birth" is-open="opened['date_of_birth']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'date_of_birth')" formatdate>

                </div>
                <a class="button default" ng-click="showInput($event, 'date_of_birth')" href="#"><?= LangHelper::get('close', 'Close')?></a>
            </div>
        </p>
        <p class="fg-white">
            <?= LangHelper::get('email', 'Email')?>: {{ player.contact.email}} - <a ng-click="showInput($event, 'email')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
            <div class="form-control" ng-show="input.email">
                <div class="input-control text">
                    <input type="text" ng-model="player.contact.email">
                </div>
                <a class="button default" ng-click="showInput($event, 'email')" href="#"><?= LangHelper::get('close', 'Close')?></a>
            </div>
        </p>
        <p class="fg-white">
            <?= LangHelper::get('contact_phone_number', 'Contact phone number')?>: {{ player.contact.phone}} - <a ng-click="showInput($event, 'phone')" href="#"><?= LangHelper::get('edit_info', 'Edit info')?></a>
            <div class="form-control" ng-show="input.phone">
                <div class="input-control text">
                    <input type="text" ng-model="player.contact.phone">
                </div>
                <a class="button default" ng-click="showInput($event, 'phone')" href="#"><?= LangHelper::get('close', 'Close')?></a>
            </div>
        </p>

        <button type="submit" class="primary"><?= LangHelper::get('signup', 'Signup')?></button>

    </form>
</div>
<div class="no-info">
    <h3 ng-show="noInfo" class="danger-color">{{message}}</h3>

    <form name="formCheck" ng-show="showForm" ng-submit="signupForm($event, '<?= $draw->id?>', true)" novalidate action="">
        <p class="fg-white"><?= LangHelper::get('enter_information', 'Enter information')?></p>

        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <fieldset>
            <div class="row">
                <div class="half">
                    <div class="form-control">
                        <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                        <div class="input-control text">
                            <input type="text" ng-class="{}" ng-model="player.contact.name" name="name" required>
                        </div>
                    </div>
                </div>
                <div class="half">
                    <div class="form-control padd">
                        <label for="surname"><?= LangHelper::get('surname', 'Surname')?></label>
                        <div class="input-control text">
                            <input type="text" ng-class="{}" ng-model="player.contact.surname" name="surname" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="half">
                    <div class="form-control">
                        <label for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth')?></label>
                        <div class="input-control text">
                            <input type="text" datepicker-popup="dd/MM/yyyy"
                                   ng-model="player.contact.date_of_birth"
                                   is-open="opened['date_of_birth2']"
                                   show-button-bar="false"
                                   ng-required="true"
                                   ng-click="openDatePicker($event, 'date_of_birth2')" formatdate>
                        </div>
                    </div>
                </div>
                <div class="half">
                    <div class="form-control padd">
                        <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                        <div class="input-control text">
                            <input type="text" ng-class="{}" ng-model="player.contact.email" name="email">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="half">
                    <div class="form-control">
                        <label for="phone"><?= LangHelper::get('contact_phone', 'Contact phone')?></label>
                        <div class="input-control text">
                            <input type="text" ng-class="{}" ng-model="player.contact.phone" name="phone">
                        </div>
                    </div>
                </div>
                <div class="half">
                    <div class="form-control">
                        <label for="licence_number"><?= LangHelper::get('licence_number', 'Licence number')?></label>
                        <div class="input-control text">
                            <input type="text" ng-class="{}" ng-model="player.licence_number" name="licence_number" maxlength="9">
                        </div>
                    </div>
                </div>
            </div>
         <button type="submit" class="primary"><?= LangHelper::get('signup', 'Signup')?></button>
      </fieldset>
    </form>
</div>
