<div class="container ang">
    <div class="grid" ng-controller="TabsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('list_after_entry_deadline', 'List after entry deadline') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?></p>

            <p class="yellow-color"><?= LangHelper::get('draw_level', 'Draw level') ?>: <?= $draw->level ?>
                estrellas</p>

            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category') ?>
                : <?= $draw->category->name.''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?></p>
            <?= $reports ?>
        </div>
        <tabset class="pad-top40">
            <a class="rfet-button rfet-yellow place-right call"
               href="/signups/list/<?= $draw->id ?>"><?= LangHelper::get('players_signed', 'Players') ?></a>
            <a class="rfet-button rfet-yellow call place-right gap-right10" data-title="Signup for tournament"
               href="<?= URL::to('signups/register', $draw->id) ?>">
                <?= LangHelper::get('register', 'Register') ?>
            </a>
            <a class="rfet-button rfet-yellow call place-right gap-right10" data-title="Consolation draws"
               href="<?= URL::to('draws/consolation-draws', $draw->id) ?>">
                <?= LangHelper::get('consolation_draws', 'Consolation draws') ?>
            </a>
            <?php if (TournamentHelper::checkIfTournamentHasMatches($tournament->id)):?>
                <a class="rfet-button rfet-yellow place-right gap-right10"
                   href="/schedule/index/<?= $tournament->id ?>">
                    <?= LangHelper::get('schedule_administration', 'Schedule administration') ?>
                </a>
            <?php endif?>
            <?php foreach ($final as $key => $players): ?>
                <tab class="tab_draw_<?= $key ?>">
                    <tab-heading>
                        <?php if ($key == 1): ?>
                            <?= LangHelper::get('main_draw', 'Main draw') ?>
                        <?php elseif ($key == 2): ?>
                            <?= LangHelper::get('qualifying_draw', 'Qualifying draw') ?>
                        <?php elseif ($key == 3): ?>
                            <?= LangHelper::get('alternate_draw', 'Alternate draw') ?>
                        <?php else: ?>
                            <?= LangHelper::get('withdrawals', 'Withdrawals') ?>
                        <?php endif ?>
                        - <?= count($players) ?>
                    </tab-heading>
                    <div class="row pad-top20 asfaasfa">
                        <?php if ($key == 1 OR $key == 2): ?>
                            <!-- if draw has matches show order of play and view bracket buttons -->
                            <?php if ($check_if_matches_exists[$key]): ?>
                                <?php if ($key == 2): ?>
                                    <a class="rfet-button rfet-yellow place-right gap-right10"
                                       href="/draws/db-bracket/<?= $draw->id ?>?list_type=<?= $key ?>">
                                        <?= LangHelper::get('view_bracket', 'View bracket') ?>
                                        <img src="/css/icons/draws.png" alt="">
                                    </a>
                                <?php endif ?>

                                <?php if ($key == 1): ?>
                                    <a class="rfet-button rfet-yellow place-right gap-right10"
                                       href="/draws/db-bracket/<?= $draw->id ?>?list_type=<?= $key ?>">
                                        <?= LangHelper::get('view_bracket', 'View bracket') ?>
                                        <img src="/css/icons/draws.png" alt="">
                                    </a>
                                <?php endif ?>

                            <?php else: ?>
                                <!-- if draw is main draw and qualifications are finished show create button -->
                                <?php if (($key == 1 AND TournamentHelper::checkIfQualificationsAreFinished($draw->id)) ||
                                    ($key == 1 && TournamentHelper::checkIfThereIsntQualificationDraw($draw))): ?>

                                    <a class="rfet-button rfet-danger place-right"
                                       href="/draws/manual-bracket/<?= $draw->id ?>?list_type=<?= $key ?>">
                                        <?= LangHelper::get('create_main_draw', 'Create main draw') ?>
                                    </a>
                                <?php elseif ($key == 2): ?>
                                    <a class="rfet-button rfet-danger place-right"
                                       href="/draws/manual-bracket/<?= $draw->id ?>?list_type=<?= $key ?>">
                                        <?= LangHelper::get('create_qualification_draw', 'Create qualification draw') ?>
                                    </a>
                                <?php endif ?>
                            <?php endif ?>
                        <?php endif ?>
                        <div style="width:100% !important; overflow-x:scroll !important">
                            <table class="table bg-dark tournament-table table-scrollable-w800">
                                <thead>
                                <tr>
                                    <th width="15%" class="text-left"><?= LangHelper::get('surname', 'Surname') ?></th>
                                    <th width="15%" class="text-left"><?= LangHelper::get('name', 'Name') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('licence_number', 'Licence number') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('date_of_birth', 'Date of birth') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('ranking_points', 'Ranking points') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('submitted_on', 'Submitted on') ?></th>
                                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                        <th width="30%"
                                            class="text-right"><?= LangHelper::get('options', 'Options') ?></th>
                                    <?php endif ?>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach ($players as $player): ?>
                                    <tr>
                                        <td>
                                            <?= ($player['move_from_qualifiers_type'] == 2 && $key == 1) ? ' (Q)' : null; ?>
                                            <?= SignupHelper::wildCardStatus($player)?> <?= TournamentHelper::getPlayerValue($player, 'surname') ?>
                                        </td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'name') ?></td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'licence_number') ?></td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'date_of_birth') ?></td>
                                        <td><?= $player['ranking_points'] ?></td>
                                        <td><?= DateTimeHelper::GetFullDateTime($player['created_at']) ?></td>

                                        <td class="transparent text-right">

                                            <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                                <?php if (!$check_if_matches_exists[$key]): ?>
                                                    <?php if (!$player['status']): ?>
                                                        <div class="button-dropdown text-left">
                                                            <button
                                                                class="dropdown-toggle public-toggle rfet-yellow"><?= LangHelper::get('move_to', 'Move to') ?>
                                                                ...
                                                            </button>
                                                            <ul class="dropdown-menu" data-role="dropdown">
                                                                <li>
                                                                    <a href="/signups/change-type/<?= $player['id'] ?>/1?key=<?= $key ?>">
                                                                        <?= LangHelper::get('move_to_main_draw', 'Move to main draw') ?>
                                                                    </a>
                                                                </li>
                                                                <?php if(!$tournament->has_qualifying): ?>
                                                                <li>
                                                                    <a href="/signups/change-type/<?= $player['id'] ?>/2?key=<?= $key ?>">
                                                                        <?= LangHelper::get('move_to_qualifying_draw', 'Move to qualifying draw') ?>
                                                                    </a>
                                                                </li>
                                                                <?php endif ?>
                                                                <li>
                                                                    <a href="/signups/change-type/<?= $player['id'] ?>/3?key=<?= $key ?>">
                                                                        <?= LangHelper::get('move_to_alternate_draw', 'Move to alternate draw') ?>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    <?php endif ?>
                                                <?php endif ?>

                                                <?php if ($key != 4): ?>
                                                    <a class="rfet-button rfet-danger" href="<?= URL::to('signups/withdraw', $player['id']) ?>">
                                                        <?= LangHelper::get('withdraw', 'Withdraw') ?>
                                                    </a>
                                                <?php endif ?>
                                            <?php endif ?>

                                            <a class="button small rfet-yellow pad-top10" href="<?= URL::to('signups/player-details', $player['id']) ?>">
                                                Player info
                                            </a>
                                        </td>

                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </tab>

            <?php endforeach ?>
        </tabset>

    </div>


    <script src="/js/ang/tabsModule.js"></script>
    <script>

        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['tabsModule']);
            $("[data-role=dropdown]").dropdown();
        });

        $(function(){
            if(document.location.hash !== undefined)
            {
                var link = document.location.hash.replace("#", "");
                if(link=="main")
                    $(".tab_draw_1 a").click();
                else if(link=="qualifying")
                    $(".tab_draw_2 a").click();
                else if(link=="alternate")
                    $(".tab_draw_3 a").click();

            }
        })
    </script>