<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament">
                <h3>
                   <?= LangHelper::get('create_pairs', 'Create pairs')?>
                </h3>
                <span class="line"></span>
            </div>
        </div>


        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament')?>: <?= $draw->tournament->title?></p>
            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category')?>: <?= $draw->category->name.', '.$draw->typeName()?></p>
        </div>

        <div class="row" ng-controller="generatorController"
             data-ng-init="init()" data-scurl="<?= URL::to('signups/doubles-state', $draw->id)?>">
            <div class="half side-holder">
                <div class="team-holder" ng-repeat="player in list1 track by $index">
                    {{ player.teams[0].name }} {{ player.teams[0].surname }}

                    <select class="choose-position" ng-model="position"
                            ng-options="position for position in positions"
                            ng-change="onChanged($index, position)">
                        <option value="">Choose position</option>
                    </select>
                </div>
            </div>

            <div class="half side-holder place-right">
                <div class="team-holder" ng-repeat="team in list2 track by $index">
                    {{ team.teams[0].name }} {{ team.teams[0].surname }},
                    {{ team.teams[1].name }} {{ team.teams[1].surname }}
                </div>


                <a class="button inverse place-right submit-pairs-btn"
                   ng-click="manualSubmit($event, 'Pairs successfully created', '<?= Request::url()?>', true)">
                    <?= LangHelper::get('submit_pairs', 'Submit pairs')?>
                </a>
            </div>
        </div>

    </div>
</div>

<script src="/js/ang/manualDoubles.js"></script>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['manualDoubles']);
    });
</script>