<div class="container ang">
    <div class="grid" ng-controller="InvoiceController" ng-init="init(<?= $invoice->id ?>)">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('update_invoice', 'Update invoice') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="clearfix row pad-top40">
            <div class="invoice-info half">
                <p><span class="color-rfet"><?= LangHelper::get('title', 'Title') ?>
                        :</span> <?= $invoice->title ?></p>

                <p><span class="color-rfet"><?= LangHelper::get('order_number', 'Order number') ?>
                        :</span> <?= $invoice->order_number ?></p>

                <p><span class="color-rfet"><?= LangHelper::get('region', 'Region') ?>
                        :</span> <?= $invoice->regionName() ?></p>

                <p><span class="color-rfet"><?= LangHelper::get('created_by', 'Created by') ?>
                        :</span> <?= $invoice->creator() ?></p>

                <p><span class="color-rfet"><?= LangHelper::get('status', 'Status') ?>
                        :</span> {{invoice.status}}</p>
            </div>

            <div class="half">
                <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('description', 'Description') ?></th>
                        <th class="text-left"><?= LangHelper::get('amount', 'Amount') ?></th>
                        <th class="text-left"><?= LangHelper::get('price', 'Price') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($invoice->data as $data): ?>
                        <tr>
                            <td class="text-left"><?= $data->description ?></td>
                            <td class="text-left"><?= $data->amount ?></td>
                            <td class="text-left"><?= $data->price() ?></td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="clearfix row pad-top20 invoice-update-holder">
            <p class="big-\"><?= LangHelper::get('total', 'Total') ?>: {{invoice.total | to_trusted}}</p>

            <p class="big-total"><?= LangHelper::get('paid', 'Paid') ?>: {{ invoice.paid | to_trusted}}</p>

            <form name="editForm" class="half" ng-show="invoice.total_number > invoice.paid_number"
                  ng-submit="formSubmit($event, <?= $invoice->id ?>)" novalidate>
                <div class="form-control">
                    <label for="name"><?= LangHelper::get('paid', 'Paid') ?></label>

                    <div class="input-control text">
                        <input type="text" ng-model="formData.invoice.paid" required>
                    </div>
                </div>
                <button type="submit" class="rfet-button rfet-yellow">
                    <?= LangHelper::get('save', 'Save')?>
                </button>

            </form>
        </div>

        <div class="clearfix pad-top20 row">
            <h3><?= LangHelper::get('payment_history','Payment history')?></h3>

            <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('created_by', 'Created by') ?></th>
                    <th class="text-left"><?= LangHelper::get('created_at', 'Created at') ?></th>
                    <th class="text-left"><?= LangHelper::get('paid', 'Paid') ?></th>
                </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="payment in invoice.payments track by $index">
                        <td class="text-left">{{payment.created_by}}</td>
                        <td class="text-left">{{payment.created_at}}</td>
                        <td class="text-left">{{payment.paid}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>