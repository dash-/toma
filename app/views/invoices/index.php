<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('invoices', 'Invoices') ?></h3>
                <span class="line"></span>
            </div>
        </div>
<div class="input-control">

        <input class="yellow-color search-invoice" id="search-invoice" placeholder="<?= LangHelper::get('search_for_invoice', 'Search for invoice')?>">
</div>

        <div class="row tabScrl">
            <div class="table-scrollable tournamentIndexResponsive">

                <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('order_number', 'Order number') ?></th>
                        <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                        <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                            <th class="text-left"><?= LangHelper::get('created_by', 'Created by') ?></th>
                        <?php endif ?>
                        <th class="text-left"><?= LangHelper::get('paid', 'Paid') ?></th>
                        <th class="text-left"><?= LangHelper::get('total', 'Total') ?></th>
                        <th class="text-left"><?= LangHelper::get('status', 'Status') ?></th>
                        <th class="text-right" width="25%"><?= LangHelper::get('options', 'Options') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($invoices as $invoice): ?>
                        <tr class="invoice-search">
                            <td><?= $invoice->order_number ?></td>
                            <td><?= $invoice->title ?></td>
                            <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                <td><?= $invoice->creator() ?></td>
                            <?php endif ?>
                            <td><?= $invoice->paid() ?></td>
                            <td><?= $invoice->total() ?></td>
                            <td><?= $invoice->status() ?></td>
                            <!-- options -->
                            <td class="transparent text-right">
                                <?php if (Auth::user()->hasRole('superadmin')): ?>
                                    <a class="call rfet-button gap-left10 rfet-yellow"
                                       href="<?= URL::to('invoices/update', $invoice->id) ?>">
                                        <?= LangHelper::get('update', 'Update') ?>
                                    </a>
                                <?php endif ?>
                                <a class="call rfet-button rfet-yellow"
                                   href="<?= URL::to('invoices/details', $invoice->id) ?>">
                                    <?= LangHelper::get('details', 'Details') ?>
                                </a>
                                <a class="rfet-button rfet-yellow" target="_blank"
                                   href="<?= URL::to('invoices/pdf', $invoice->id)."?his_ignore=true" ?>">
                                    <?= LangHelper::get('pdf', 'PDF') ?>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function () {
        $("select").select2({});
        $("[data-role=dropdown]").dropdown();

        $(function () {
            $("select").select2({width: '140px'});
            $("#search-invoice").searchFilter({targetSelector: ".invoice-search", charCount: 2})
        });
    });
</script>
