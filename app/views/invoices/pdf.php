<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/invoice.css">
</head>

<body>
<div id="paper-size">
    <img class="logoBig" src="<?= URL::to('/css/images/LogoLogin.png') ?>" title="Tennis Organization & Management Application"
         alt="Tennis Organization & Management Application"/>

    <div style="margin-left:400px; padding-top:-100px;"> Passeig Olimpic, 17-19 (Estadi Olimpic)<br><span
            style="padding:57px"> 0 8 0 3 8  BARCELONA </span><br> <br>TELEFONOS 932 005 355 - 932 005 878 <br><span
            style="padding-left:95px;">932 091 090 - 932 091 377</span><br><span
            style="padding:58px">FAX 932 021 279</span><br>
            <span style="margin-left:150px">http://www.rfet.es</span>
            <span style="margin-left:150px">e-mail:rfet@rfet.es</span>
    </div>

    <div style="padding-top:30px">Date: <?= $invoice->created_at ?></div>

    <div style="padding-top:20px">Facture n. : <?= $invoice->order_number?></div>

    <div style="padding-top:20px">Code: <?= $invoice->code?></div>

    <div style="padding-top:20px">Vencimiento:</div>

    <table style="position:absolute; padding-top:100px; padding-left:50px; width:650px;">
        <tr>
            <th style="border-bottom:1px solid black"><?= LangHelper::get('amount', 'Amount') ?></th>
            <th style="border-bottom:1px solid black"><?= LangHelper::get('description', 'Description') ?></th>
            <th style="border-bottom:1px solid black"><?= LangHelper::get('price', 'Price') ?></th>
            <th style="border-bottom:1px solid black"><?= LangHelper::get('total', 'Total') ?></th>
        </tr>
        <?php foreach ($invoice->data as $data): ?>
            <tr>
                <td style="text-align:center !important;border-bottom:1px solid black"><?= $data->amount; ?>
                </td>
                <td style="text-align:center !important;border-bottom:1px solid black">
                    <?= $data->description ?>
                </td>
                <td style="text-align:center !important;border-bottom:1px solid black"><?= $data->price() ?></td>
                <td style="text-align:center !important;border-bottom:1px solid black"><?= $data->total() ?></td>

            </tr>
        <?php endforeach ?>

    </table>
    <div style="position:absolute; padding-top:650px; padding-left:530px">
        <span>Total --> </span>  <?= $invoice->total() ?>
    </div>
    <div style="position:absolute; padding-top:610px;padding-left:100px">
        Factura extenta de IVA segun el articulo 20.uno.13 de la ley de IVA 37/92 <br> <br>
        <span style="padding-left:150px">%I.V.A.</span>Importe
        I.V.A.
    </div>
</div>
</body>
</html>


