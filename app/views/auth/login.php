<div class="container" ng-app="login">
    <div class="grid login">
        <div class="login-middle">
            <div class="login-top">
                <div style="max-width:100%; height:auto;">
                    <img class="logoBig" src="css/images/TomaLogoLogin.png" title="Real Federación Española de Tenis"
                         alt="Real Federación Española de Tenis"/>

                </div>
            </div>

            <form name="loginForm" ng-submit="login()" ng-controller="loginController" novalidate ng-cloak>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                <fieldset>

                    <!--                        <label>email</label>-->

                    <div class="input-control text darker">
                        <input type="text" ng-model="user.email" name="email" placeholder="USERNAME" required>

                    </div>

                    <!--                        <label>password</label>-->

                    <div class="input-control password darker">
                        <input type="password" ng-model="user.password" name="password" placeholder="PASSWORD" required>
                        <button class="btn-reveal"></button>
                    </div>
                    <label>
                        <small>Hint: admin, admin</small>
                    </label>
                    <div class="input-control checkbox">
                        <label>
                            <input type="checkbox" ng-model="user.remember_me" name="remember_me">
                            <span class="check big dark"></span>
                            <?= LangHelper::get('remember_me', 'Remember me') ?>
                        </label>
                    </div>

                    <div class="login-button text-right">
                        <button type="submit" class="primary dark login-buton-main"
                                ng-disabled="!loginForm.$valid"><?= LangHelper::get('login', 'login') ?>
                    </div>
                    </button>
                    <a href="/password-reset">
                        <div class="passReset"
                             style="color:#F7A700"><?= LangHelper::get('forgot_your_password_?', 'Forgot your password?') ?></div>
                    </a>

                </fieldset>

                <div class="warning fg-red text-center" ng-show="showError" ng-label="errorName">{{ errorName }}
                </div>
            </form>

        </div>
    </div>
</div>
<script src="/js/ang/login.js"></script>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['login']);
    });
</script>