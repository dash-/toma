<div class="container" ng-app="resetPassApp">
    <div class="grid login">
            <div class="login-middle" ng-controller="formController">
                <div class="login-top">
                    <div style="max-width:100%; height:auto;">
                        <img class="logoBig" src="css/images/TomaLogoLogin.png" title="Tennis Organization & Management Application" alt="Tennis Organization & Management Application"/>

                        </div>
                </div>
                <form name="resetForm" ng-submit="formSubmit($event)" ng-show='!msg'>
                    <input type="hidden" action="<?= Request::url()?>" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="input-control text darker">
                            <input type="email" ng-model="user.email" name="email" placeholder="EMAIL" required autocomplete="off">
                        </div>

                        <div class="login-button text-left">
                            <button type="submit" class="primary dark" ng-disabled="!resetForm.$valid">reset</button>
                        </div>
                    </fieldset>
                    <div class="warning fg-red text-center" ng-show="showError" ng-label="errorName">{{ errorName }}
                    </div>
                </form>
                
                <p class="reset-success-msg" ng-show='msg'>{{ msg }}</p>
            </div>
    </div>
</div>

<script src="/js/ang/resetPass.js"></script>
