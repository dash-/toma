<div class="container" ng-app="genericFormApp">
    <div class="grid">
        <div class="row">
            <div class="clearfix">
                <h3 class="fg-white textPassChange"><?= LangHelper::get('change_password', 'Change password')?></h3>
            </div>
            
            <form name="editForm" class="passFormReset" ng-submit="formSubmit($event, 'POST', 'Password have been changed', '<?= URL::current()?>' ,true)" ng-controller="FormController" novalidate autocomplete="off">
				
				<div class="notice marker-on-bottom bg-white" ng-show="errors.length > 0">
	                <p class="fg-black" ng-repeat="problem in errors track by $index">{{problem}}</p>
	            </div>

                <input type="text" style="display:none" name="_token" ng-model="formData._token" ng-init="formData._token='<?php echo csrf_token(); ?>'">
                <input type="text" style="display:none" ng-model="formData.queryToken" ng-init="formData.queryToken='<?php echo Request::query('token'); ?>'">
                <fieldset>
                	<div class="form-control clearfix">
	                    <label for="name"><?= LangHelper::get('new_password', 'New password')?></label>
	                    <div class="input-control {{errorState}} text">
	                        <input type="password" required ng-model="formData.password" >
	                    </div>
	                </div>

	                <div class="form-control clearfix">
	                    <label for="name"><?= LangHelper::get('repeat_new_password', 'Repeat new password')?></label>
	                    <div class="input-control {{errorState}} text">
	                        <input type="password" required ng-model="formData.password_confirmation">
	                    </div>
	                </div>
                    <button type="submit" class="primary" ng-disabled="!editForm.$valid"><?= LangHelper::get('save', 'Save')?></button>
                    
                </fieldset>
            </form>

        </div>
    </div>
</div>