<div class="container" ng-app="signup">
    <div class="grid login">
        <div class="row">
            <div class="span4">
                <h3 class="fg-white">Signup for new account</h3>
                
                <form ng-controller='signupController' ng-submit="signup()">
                    <fieldset>
                        <label>Email</label>
                        <div class="input-control {{errorState}} text">
                            <input type="text" ng-model="user.email" name="email" placeholder="enter email">
                        </div>
                        <p class="text-alert">{{errorEmail}}</p>

                    
                        <label>Password</label>
                        <div class="input-control {{errorState}} password">
                            <input type="password" ng-model="user.password" name="password" placeholder="enter password">
                            <button class="btn-reveal"></button>
                        </div>
                        <p class="text-alert">{{errorPassword}}</p>


                        <button type="submit" class="primary">Login</button>
                    </fieldset>

                </form>
                

            </div>
        </div>
    </div>
</div>

<script>
    var signupModule = angular.module('signup', []);

    signupModule.controller('signupController', function($scope, $http, $window) {
        
        $scope.user = {};

        $scope.signup = function() {
            $http({
                method  : 'POST',
                url     : '/signup',
                data    : $.param($scope.user), 
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                })
                .success(function(data) {
                    if (!data.success) {
                        $scope.errorEmail = data.email ? data.email[0] : '';
                        $scope.errorPassword = data.password ? data.password[0] : '';
                        $scope.errorState = 'error-state';
                    } else {
                        $window.location.href = "/";
                    }
                });

        };

    });

</script>

