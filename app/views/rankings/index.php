<div class="container ang">
    <div class="grid datatable" ng-controller="tableController" data-ng-init="init(1)"
         data-tableSortcolumn="players.ranking" data-scUrl="/rankings/data">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="yellow-color last-recalculation text-right">
            <?php if ($last_weekly_recalculation): ?>
                <?= LangHelper::get('last_recalculation', 'Last player weekly ranking recalculation was on') . ' ' . DateTimeHelper::GetSpanishShortDateFullYear($last_weekly_recalculation->date) ?>
                <br/>
            <?php endif ?>

            <?php if ($last_quarter_recalculation): ?>
                <?= LangHelper::get('last_recalculation', 'Last player quarter ranking recalculation was on') . ' ' . DateTimeHelper::GetSpanishShortDateFullYear($last_quarter_recalculation->date) ?>
                <br/>
            <?php endif ?>
        </div>

        <div>
            <a id="exportRankings" class="rfet-button rfet-yellow place-right gap-left20"
               href="/rankings/download-export/3"> <?= LangHelper::get('download_male_rankings_as_pdf', 'Download male rankings as PDF') ?></a>
        </div>
        <div>
            <a id="exportRankings" class="rfet-button rfet-yellow place-right gap-left20"
               href="/rankings/download-export/4"> <?= LangHelper::get('download_female_rankings_as_pdf', 'Download female rankings as PDF') ?></a>
        </div>

        <div>
            <a id="exportRankings" class="rfet-button rfet-yellow place-right gap-left20"
               href="/rankings/download-export/1"> <?= LangHelper::get('download_full_rankings_as_excel', 'Download full rankings as Excel') ?></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('ranking', 'Ranking') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div>
            <h3 class="movers"><?= LangHelper::get("movers_of_the_week", "Movers of the week") ?></h3>

            <div class="yellow-color ranking_genderM">
                <?= LangHelper::get('male', 'Male') ?>
            </div>
            <table class="table bg-dark striped ranking-table"
                   style="width:40%; float:left; margin-left:15% !important">
                <tr>
                    <thead>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('points', 'Points') ?>
                    </th>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('ranking_move', 'Ranking move') ?>
                    </th>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('player', 'Player') ?>
                    </th>
                    </thead>
                </tr>

                <?php foreach ($movers_male as $mover): ?>
                    <tr>
                        <td><?= $mover->ranking ?></td>
                        <td>
                            <span class="rankingicon positive"></span>
                            <?= $mover->ranking_move ?>
                        </td>
                        <td>
                            <a class="call"
                               href="/players/info/<?= $mover->id ?>"><?= $mover->contact->surname . ' ' . $mover->contact->name ?></a>
                        </td>

                    </tr>

                <?php endforeach; ?>
            </table>
            <div class="yellow-color ranking_genderF">
                <?= LangHelper::get('female', 'Female') ?>

            </div>
            <table class="table bg-dark striped" style="width:40%; float:right;">
                <tr>
                    <thead>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('points', 'Points') ?>
                    </th>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('ranking_move', 'Ranking move') ?>
                    </th>
                    <th class="text-left yellow-color">
                        <?= LangHelper::get('player', 'Player') ?>
                    </th>
                    </thead>
                </tr>
                <?php foreach ($movers_female as $mover): ?>
                    <tr>

                        <td><?= $mover->ranking ?></td>
                        <td>
                            <span class="rankingicon positive"></span>
                            <?= $mover->ranking_move ?>
                        <td>
                            <a class="call"
                               href="/players/info/<?= $mover->id ?>"><?= $mover->contact->surname . ' ' . $mover->contact->name ?></a>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
            <span class="line yellow-color" style="padding-bottom:200px; width:100%; padding-top:200px"></span>

        </div>
        <div class="table-title players">
            <div class="input-control select">
                <?= Form::select('items', $items_per_page, null, array('ui-select2' => "select2Options", 'ng-change' => 'itemsPerPage()', 'ng-model' => 'counter', 'ng-init' => 'counter=10', 'ng-class' => 'no-search-box')); ?>
            </div>

            <div class="input-control select">
                <?= Form::select('sex', $types_array, "M", array('ui-select2' => "select2Options", 'ng-change' => 'sexChange()', 'ng-model' => 'sex', 'ng-init' => 'sex=M', 'ng-class' => 'no-search-box')); ?>
            </div>
            <div class="input-control select">
                <?= Form::select('regions', $regions, NULL,
                    array('ui-select2' => "select2Options", 'ng-change' => 'filterBy()',
                        'ng-model' => 'federation_club_id', 'ng-class' => 'no-search-box')); ?>
            </div>
        </div>
        <div class="player-align table-content players tabScrl rankingsPad">
            <form ng-submit="search($event, 1)" name="searchForm" class="tabScrl">
                <table class="table bg-dark striped rankings-table table-scrollable-w768">
                    <thead>
                    <tr>
                        <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking_points" placeholder="Search by ranking points">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('ranking_points')" href="#">
                                <?= LangHelper::get('points', 'Points') ?> <i
                                    ng-class="sortBy('ranking_points')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.surname"
                                       placeholder="<?= LangHelper::get('surname', 'Surname') ?>">
                            </div>
                            <a ng-click="doOrder('contacts.surname')" href="#">
                                <?= LangHelper::get('surname', 'Surname') ?> <i
                                    ng-class="sortBy('contacts.surname')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.name"
                                       placeholder="<?= LangHelper::get('name', 'Name') ?>">
                            </div>
                            <a ng-click="doOrder('contacts.name')" href="#">
                                <?= LangHelper::get('name', 'Name') ?> <i ng-class="sortBy('contacts.name')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.INT_licence_number"
                                       placeholder="<?= LangHelper::get('licence_number', 'Licence number') ?>">
                            </div>
                            <a ng-click="doOrder('licence_number')" href="#">
                                <?= LangHelper::get('licence_number', 'Licence number') ?> <i
                                    ng-class="sortBy('licence_number')"></i>

                            </a>
                        </th>
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking" placeholder="Search by ranking">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('ranking')" href="#">
                                <?= LangHelper::get('ranking', 'Ranking') ?> <i ng-class="sortBy('ranking')"></i>

                            </a>
                        </th>
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking" placeholder="Search by ranking">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('regional_ranking')" href="#">
                                <?= LangHelper::get('regional_ranking', 'Regional ranking') ?> <i
                                    ng-class="sortBy('regional_ranking')"></i>

                            </a>
                        </th>
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking" placeholder="Search by ranking">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('province_ranking')" href="#">
                                <?= LangHelper::get('province_ranking', 'Province ranking') ?> <i
                                    ng-class="sortBy('province_ranking')"></i>

                            </a>
                        </th>
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking" placeholder="Search by ranking">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('club_ranking')" href="#">
                                <?= LangHelper::get('club_ranking', 'Club ranking') ?> <i
                                    ng-class="sortBy('club_ranking')"></i>

                            </a>
                        </th>

                        <th class="text-left" style="width:10%">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.city" placeholder="Search by ranking move">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('ranking_move')" href="#">
                                <?= LangHelper::get('move_with', 'Move with') ?> <br/>
                                <?= LangHelper::get('last_match', 'last match') ?> <i
                                    ng-class="sortBy('ranking_move')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <!--                            <div class="input-control text">-->
                            <!--                                <input type="text" ng-model="searchText.ranking" placeholder="Search by ranking">-->
                            <!--                            </div>-->
                            <a ng-click="doOrder('quarter_ranking')" href="#">
                                <?= LangHelper::get('quarter_ranking', 'Quarter ranking') ?> <i
                                    ng-class="sortBy('quarter_ranking')"></i>

                            </a>
                        </th>
                        <th class="text-left last-col">
                            <div class="input-control text">
                                <button type="submit" ng-click="search($event, 1)"
                                        ng-class="scSearch == 1 ? 'split-button' : ''" class="primary dark full">Filter
                                </button>
                                <a ng-click="resetSearch()" ng-show="scSearch == 1"
                                   class="button primary split-button reset-button" href="">Reset search</a>
                            </div>
                            <a class="options"><?= LangHelper::get('options', 'Options') ?></a>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="player in rows track by $index">
                        <td>{{ player.ranking_points }}</td>
                        <td>{{ player.surname }}</td>
                        <td>{{ player.name }}</td>
                        <td>{{ player.licence_number }}</td>
                        <td>{{ player.ranking }}</td>
                        <td>{{ player.regional_ranking }}</td>
                        <td>{{ player.province_ranking }}</td>
                        <td>{{ player.club_ranking }}</td>
                        <td>{{ player.ranking_move | rankingMove }}</td>
                        <td>{{ player.quarter_ranking }}</td>
                        <td class="text-left input-control transparent">
                            <a class="button primary info inline-block"
                               ng-href="/players/info/{{player.id}}"><?= LangHelper::get('info', 'Info') ?></a>
                            <a class="call button small primary default"
                               ng-hide="(user_role == 3 || user_role == 6) && user_region != player.federation_club_id"
                               ng-href="/players/edit/{{player.id}}"><?= LangHelper::get('edit', 'Edit') ?></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>

            <h3 class="fg-white text-center"
                ng-show="players.length < 1"><?= LangHelper::get('no_available_data', 'No available data') ?></h3>

            <div class="input-control back-in-pagination">
                <a class="button primary yellow back big call" href="/"><?= LangHelper::get('back', 'BACK') ?></a>
            </div>
            <div class="pagination">
                <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage" max-size="maxSize"
                            boundary-links="true" rotate="false" items-per-page="counter"></pagination>
            </div>

        </div>
    </div>
</div>

<script>
    //TODO: napomena : hack rjesenje funkcije showSearch u select.js, stoga ne korisititi select.min.js u produkciji
    $(document).ready(function () {
        $("select").select2({});
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>