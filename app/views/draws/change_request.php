<div class="metro" style="width: auto">
<div class="modal-body">
    <h3>
        <?= LangHelper::get('request_for_result_change', 'Request for result change')?>
    </h3>
    
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('player', 'Player')?> 1</th>
                    <th class="text-left"><?= LangHelper::get('player', 'Player')?> 2</th>
                    <th class="text-right"><?= LangHelper::get('score', 'Score')?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-left">{{data.team1_name}}</td>
                    <td class="text-left">{{data.team2_name}}</td>
                    <td class="text-right">
                        {{data.match_score}}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <form name="requestForm" ng-submit="submitForm($event, '/draws/request-change')" novalidate>
        <fieldset>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="row">
                <div class="form-control">
                    <label for="text"><?= LangHelper::get('explanation', 'Explanation')?></label>
                    <div class="input-control textarea">
                        <textarea ng-model="formData.text" required></textarea>
                    </div>
                </div>
            </div>
            <p class="clear gap-top10 pad-left5 danger-color" ng-show="errors.msg.length > 0">{{errors.msg}}</p>
            <button type="submit" ng-disabled="!requestForm.$valid" class="button medium next-yellow-arrow right yellow-color dark-col-bg" style="margin-top: 38px;">
                <?= LangHelper::get('send', 'Send')?>
            </button>   
        </fieldset>
    </form>
        
</div>
</div>

