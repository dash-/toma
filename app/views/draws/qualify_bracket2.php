<div class="container ang">
    <div class="grid">
        <!-- <div class="row">
            <div class="clearfix">
                <h3 class="fg-white place-left">Qualify draw bracket</h3>
                <a class="button primary big call next" href="<?= URL::previous()?>">Back</a>
            </div>
        </div> -->
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3>Qualify draw bracket</h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="row">
            <p class="yellow-color">Tournament: <?= $draw->tournament->title?></p>
            <p class="yellow-color">Draw category: <?= $draw->category->name?></p>
        </div>

        <div class="row">

            <div id="bracket"></div>

        </div>
</div>

<link rel="stylesheet" href="/js/thirdparty/bracket2/jquery.bracket-world.css" />
<script src="/js/thirdparty/bracket2/jquery.bracket-world.js"></script>

<script>

$(document).ready(function() {
    $('#bracket').bracket({
        teams: 64
    });
    // var data = {
    //     teams : <?= $jsArray?>,
    //     results: <?= $results?>
    // }

    // $(function() {
    //     $('#bracket').bracket({
    //         init: data /* data to initialize the bracket with */
    //     });
    // });
});

</script>