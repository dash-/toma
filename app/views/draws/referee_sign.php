<div class="metro" style="width: auto">
<div class="modal-body">
    <h3>
        <?= LangHelper::get('sign_main_draw_submit', 'Sign main draw submit')?>
    </h3>
    <form name="createForm" id="signForm" ng-submit="signForm($event, '/draws/submit-main-draw/<?= $draw->id?>')" action="/draws/check-referee-sign" novalidate>
        <fieldset>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="row">
                <div class="half">
                    <div class="form-control">
                        <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                         <div class="input-control text">
                            <input type="text" ng-model="formData.name" name="name" required>
                        </div>
                    </div>
                </div>
                <div class="half">
                    <div class="form-control">
                        <label for="surname"><?= LangHelper::get('surname', 'Surname')?></label>
                        <div class="input-control text">
                            <input type="text" ng-model="formData.surname" name="surname" required>
                        </div>
                    </div>
                </div>
            </div>
            <p class="clear gap-top10 pad-left5 danger-color" ng-show="errors.msg.length > 0">{{errors.msg}}</p>
            <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg" style="margin-top: 38px;">
                <?= LangHelper::get('submit_main_draw', 'Submit main draw')?>
            </button>   
        </fieldset>
    </form>
<script>
$(document).ready(function () {
    
});
</script>
</div>
</div>

