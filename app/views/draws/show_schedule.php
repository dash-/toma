<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <a class="button view-schedule-full-screen">
                <?= LangHelper::get('show_schedule_content', 'Show full screen')?>
            </a>
            <a class="button" href="<?= URL::to("signups?open=". $tournament->id)?>">
                <?= LangHelper::get('signups', 'Signups')?>
            </a>
             <div class="table-title tournament">
                <h3>
                    <?php if($list_type == 1):?>
                        <?= LangHelper::get('main_draw', 'Main draw')?>
                    <?php elseif($list_type == 2):?>
                        <?= LangHelper::get('qualifying_draw', 'Qualifying draw')?>
                    <?php else: ?>
                        <?= LangHelper::get('matches', 'Matches')?>
                    <?php endif?>
                     - <?= LangHelper::get('schedule', 'schedule')?>

                </h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="row">
            <p class="yellow-color tour-nameOOP"><?= LangHelper::get('tournament', 'Tournament')?>: <?= $tournament->title?></p>
        </div>
        
        <div id="wrap-schedule">
            <?= View::make('draws/show_schedule_content', [
                'list_type'       => $list_type,
                'tournament'      => $tournament,
                'default_counter' => $default_counter,
                'dates'           => $dates,
            ])?>
        </div>
</div>

<script>
$(document).ready(function(){

    app.BrainSocket.Event.listen('generic.event',function(msg)
    {
        if (msg !== undefined) {
            console.log(msg.client.data[0]);
            var schedule_id = msg.client.data[0];
            if (schedule_id == 00) {
                $('#wrap-schedule').load('/schedule/show/<?= $tournament->id?>?list_type=<?= $list_type?>&ajax=true');
            } else {
                $('#wrap-schedule .match' + schedule_id).load('/draws/single-schedule/' + schedule_id);
            }

        }
    });
})
</script>