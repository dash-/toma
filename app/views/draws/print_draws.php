<input type="hidden" id="previous_url" value="<?= URL::previous() ?>">
<div id="print-bracket-canvas">

    <!-- <div class="bracketPadding"></div> -->

    <div style="top:10px; float:left">

        <?php for ($i = 1; $i <= $round_number - 1; $i++): ?>
            <span style="padding-right:30px; padding-left:20px;">Round <?= $i ?></span>
        <?php endfor; ?>
    </div>

    <div style="top:10px; float:right;">

        <?php for ($i = $round_number - 2; $i >= 1; $i--): ?>
            <span style="padding-left:25px; padding-right:30px;">Round <?= $i ?></span>
        <?php endfor; ?>
    </div>

    <div id="left-column">


        <?php foreach ($left_side1 as $key => $match): ?>

            <div class="print_round" print-round=<?= $key - 1 ?>>

                <?php foreach ($match as $key2 => $pair): ?>

                    <div class="match" print-match=<?= $key - 1 ?>>
                        <div id="c1s1" class="slot">
                            <span class="bracketNameLeftTop boldText"><?= substr($pair['team1_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team1_data']['surname']) ?></span>
                            <span
                                class="bracketScoreLeftTop boldText"><?= TournamentScoresHelper::getBracketScoresTeam1($pair['id']) ?></span>
                        </div>

                        <div id="c1s1" class="slot">
                            <span
                                class="bracketScoreLeftBot boldText"><?= TournamentScoresHelper::getBracketScoresTeam2($pair['id']) ?></span>

                            <span class="bracketNameLeftBot boldText"><?= substr($pair['team2_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team2_data']['surname']) ?></span>
                        </div>
                    </div>

                <?php endforeach ?>

            </div>

        <?php endforeach ?>
    </div>

    <div id="right-column">


        <?php foreach ($right_side1 as $key => $match): ?>

            <div class="print_round_right" print-round=<?= $key - 1 ?>>

                <?php foreach ($match as $pair): ?>

                    <div class="match" print-match=<?= $key - 1 ?>>
                        <div id="c1s1" class="slot">
                           <span class="bracketScoreLeftBot boldText"><?= substr($pair['team1_data']['name'], 0, 1) ?>
                               . <?= DrawMatchHelper::shorten($pair['team1_data']['surname']) ?></span>
                            <span
                                class="bracketNameLeftBot boldText"><?= TournamentScoresHelper::getBracketScoresTeam1($pair['id']) ?></span>
                        </div>

                        <div id="c1s1" class="slot">
                            <span
                                class="bracketNameLeftTop boldText"><?= TournamentScoresHelper::getBracketScoresTeam2($pair['id']) ?></span>

                            <span class="bracketScoreLeftTop boldText"><?= substr($pair['team2_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team2_data']['surname']) ?></span>
                        </div>
                    </div>

                <?php endforeach ?>

            </div>

        <?php endforeach ?>
    </div>
    <div id="left-column">

        <?php foreach ($left_side2 as $key => $match): ?>


            <div class="print_round" print-round=<?= $key - 1 ?>>

                <?php foreach ($match as $key2 => $pair): ?>

                    <div class="match" print-match=<?= $key - 1 ?>>
                        <div id="c1s1" class="slot">
                            <span class="bracketNameLeftTop boldText"><?= substr($pair['team1_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team1_data']['surname']) ?></span>
                            <span
                                class="bracketScoreLeftTop boldText"><?= TournamentScoresHelper::getBracketScoresTeam1($pair['id']) ?></span>
                        </div>

                        <div id="c1s1" class="slot">
                            <span
                                class="bracketScoreLeftBot boldText"><?= TournamentScoresHelper::getBracketScoresTeam2($pair['id']) ?></span>

                            <span class="bracketNameLeftBot boldText"><?= substr($pair['team2_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team2_data']['surname']) ?></span>
                        </div>
                    </div>

                <?php endforeach ?>

            </div>

        <?php endforeach ?>
    </div>

    <div id="right-column">

        <?php foreach ($right_side2 as $key => $match): ?>

            <div class="print_round_right" print-round=<?= $key - 1 ?>>

                <?php foreach ($match as $pair): ?>

                    <div class="match" print-match=<?= $key - 1 ?>>
                        <div id="c1s1" class="slot">
                            <span class="bracketScoreLeftBot boldText"><?= substr($pair['team1_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team1_data']['surname']) ?></span>
                            <span
                                class="bracketNameLeftBot boldText"><?= TournamentScoresHelper::getBracketScoresTeam1($pair['id']) ?></span>
                        </div>

                        <div id="c1s1" class="slot">
                            <span
                                class="bracketNameLeftTop boldText"><?= TournamentScoresHelper::getBracketScoresTeam2($pair['id']) ?></span>

                            <span class="bracketScoreLeftTop boldText"><?= substr($pair['team2_data']['name'], 0, 1) ?>
                                . <?= DrawMatchHelper::shorten($pair['team2_data']['surname']) ?></span>
                        </div>
                    </div>

                <?php endforeach ?>

            </div>

        <?php endforeach ?>
    </div>


    <div class="clearfix">
        <div class="empty-space"></div>
    </div>
  
    <h3 class="finalists">Finalists!</h3>

    <div class="print_final_round">

        <?php foreach ($finalists as $finalist): ?>

            <div class="match">
                <div id="c1s1" class="slot">
                   <span><?= substr($finalist['team1_data']['name'], 0, 1) ?>
                       . <?= DrawMatchHelper::shorten($finalist['team1_data']['surname']) ?></span>
                    <span
                        class="bracketScoreLeftTop boldText"><?= TournamentScoresHelper::getBracketScoresTeam1($pair['id']) ?></span>
                </div>

                <div id="c1s1" class="slot">
                    <span
                        class="bracketScoreLeftBot boldText"><?= TournamentScoresHelper::getBracketScoresTeam2($pair['id']) ?></span>
                <span><?= substr($finalist['team2_data']['name'], 0, 1) ?>
                    . <?= DrawMatchHelper::shorten($finalist['team2_data']['surname']) ?></span>
                </div>
            </div>

        <?php endforeach ?>

        <div class="clearfix">
            <div class="empty-space"></div>
        </div>

        <h3>WINNER!</h3>

        <p>

        <div id="c1s1" class="slot">
            <?= substr($winner['name'], 0, 1) ?>. <?= DrawMatchHelper::shorten($winner['surname']) ?></div>
        </p>

    </div>
</div>

<?php if(Agent::browser() != 'IE'): ?>
    <script>

        $(function () {
            if (location.hash)
                prev_url = $("#previous_url").val();
            html2canvas(document.getElementById('print-bracket-canvas'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL();
                    var img_element = "<img id='printImg' src='" + img + "' alt=''>";
                    //canvas.strokeStyle = '#111';
                    document.write(img_element);
                    window.print();
                    document.location.href = prev_url;
                }
            });
        });
    </script>
<?php else: ?>
    <script>
        $(function () {
            if (location.hash)
                window.print();
        });
    </script>
<?php endif; ?>
