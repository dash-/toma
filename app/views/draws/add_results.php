<div class="metro" style="width: auto">
    <div class="modal-body">
        <form name="createForm" id="resultsForm" ng-submit="formSubmit($event, 'POST')" action="/draws/add-results"
              novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset ng-disabled="{{winner_next_round}}">
                <input type="hidden" ng-model="formData.match_id" ng-init="formData.match_id=match_id">

                <div ng-repeat="d in data track by $index">

                    <table>
                        <thead>
                        <th class="text-left" width="40%"><?= LangHelper::get('name', 'Name') ?></th>
                        <th class="text-right">Set 1</th>
                        <th class="text-right">Set 2</th>
                        <th class="text-right">Set 3</th>
                        <th class="text-right">Set 4</th>
                        <th class="text-right">Set 5</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{d.team1_name}}</td>

                            <td class="text-right">
                                <input id="first_input" class="table-input" type="text" ng-model="formData.team1.score1"
                                       tabindex="1" ng-init="formData.team1.score1=d.team1.0" maxlength="1"
                                       autofocus="true">
                            </td>

                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team1.score2" tabindex="3"
                                                          ng-init="formData.team1.score2=d.team1.1" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team1.score3" tabindex="5"
                                                          ng-init="formData.team1.score3=d.team1.2" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team1.score4" tabindex="7"
                                                          ng-init="formData.team1.score4=d.team1.3" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team1.score5" tabindex="9"
                                                          ng-init="formData.team1.score5=d.team1.4" maxlength="2"></td>
                        </tr>
                        <tr>
                            <td>{{d.team2_name}}</td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team2.score1" tabindex="2"
                                                          ng-init="formData.team2.score1=d.team2.0" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team2.score2" tabindex="4"
                                                          ng-init="formData.team2.score2=d.team2.1" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team2.score3" tabindex="6"
                                                          ng-init="formData.team2.score3=d.team2.2" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team2.score4" tabindex="8"
                                                          ng-init="formData.team2.score4=d.team2.3" maxlength="2"></td>
                            <td class="text-right"><input class="table-input" type="text"
                                                          ng-model="formData.team2.score5" tabindex="10"
                                                          ng-init="formData.team2.score5=d.team2.4" maxlength="2"></td>
                        </tr>
                        </tbody>
                    </table>

                    <div class="row pad-top10 text-center" ng-repeat="error in errors track by $index">
                        <p class="fg-white">{{error}}</p>
                    </div>

                    <h3><?= LangHelper::get('tie_breaks', 'Tie breaks') ?></h3>
                    <table>
                        <thead>
                        <th class="text-left" width="40%"><?= LangHelper::get('name', 'Name') ?></th>
                        <th class="text-right">Set 1</th>
                        <th class="text-right">Set 2</th>
                        <th class="text-right">Set 3</th>
                        <th class="text-right">Set 4</th>
                        <th class="text-right">Set 5</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{d.team1_name}}</td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team1.tie1"
                                                          tabindex="11" ng-init="formData.team1.tie1=d.tie1.0"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team1.tie2"
                                                          tabindex="13" ng-init="formData.team1.tie2=d.tie1.1"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team1.tie3"
                                                          tabindex="15" ng-init="formData.team1.tie3=d.tie1.2"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team1.tie4"
                                                          tabindex="17" ng-init="formData.team1.tie4=d.tie1.3"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team1.tie5"
                                                          tabindex="19" ng-init="formData.team1.tie5=d.tie1.4"
                                                          maxlength="2"></td>
                        </tr>
                        <tr>
                            <td>{{d.team2_name}}</td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team2.tie1"
                                                          tabindex="12" ng-init="formData.team2.tie1=d.tie2.0"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team2.tie2"
                                                          tabindex="14" ng-init="formData.team2.tie2=d.tie2.1"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team2.tie3"
                                                          tabindex="16" ng-init="formData.team2.tie3=d.tie2.2"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team2.tie4"
                                                          tabindex="18" ng-init="formData.team2.tie4=d.tie2.3"
                                                          maxlength="2"></td>
                            <td class="text-right"><input class="tie-input" type="text" ng-model="formData.team2.tie5"
                                                          tabindex="20" ng-init="formData.team2.tie5=d.tie2.4"
                                                          maxlength="2"></td>
                        </tr>
                        </tbody>
                    </table>

                    <h3>Extra options</h3>
                    <table class="table">
                        <thead>
                        <th class="text-left" width="30%">Name</th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        <th class="text-right"></th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{d.team1_name}}</td>
                            <td rowspan="2">
                                <a class="match-status-button remove"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 0, 0)">
                                    <?= LangHelper::get('remove_status', 'Remove status') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 1, 1)">
                                    <?= LangHelper::get('w_o', 'W.O.') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 1, 2)">
                                    <?= LangHelper::get('w_o_justified', 'W.O. justified') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 2, 3)">
                                    <?= LangHelper::get('retired', 'Retired') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 2, 4)">
                                    <?= LangHelper::get('default', 'Default') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 2, 5)">
                                    <?= LangHelper::get('def_misconduct', 'Def. misconduct') ?>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>{{d.team2_name}}</td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 2, 1)">
                                    <?= LangHelper::get('w_o', 'W.O.') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 2, 2)">
                                    <?= LangHelper::get('w_o_justified', 'W.O. justified') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 1, 3)">
                                    <?= LangHelper::get('retired', 'Retired') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 1, 4)">
                                    <?= LangHelper::get('default', 'Default') ?>
                                </a>
                            </td>
                            <td>
                                <a class="match-status-button"
                                   ng-click="addMatchStatus($event, '/draws/add-match-status/' + match_id, 1, 5)">
                                    <?= LangHelper::get('def_misconduct', 'Def. misconduct') ?>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <div class="add-results-error-message">{{ error_message }}</div>
                <a class="place-left gap-right10 button big next-yellow-arrow yellow-color dark-col-bg gap-top30"
                   ng-click="partialMatch('/draws/add-results')">
                    <?= LangHelper::get('partial_save', 'Partial save') ?>
                </a>
                <button type="submit" class="place-left rfet-button rfet-yellow gap-top30">
                    <?= LangHelper::get('check_scores_and_complete_match', 'Check scores and complete the match') ?>
                </button>

                <a class="place-left clear rfet-button rfet-success gap-top10"
                   href="/match/match-live-mode/{{match_id}}">
                    <?= LangHelper::get('point_by_point_live_scoring', 'Point by Point live scoring') ?>
                </a>

                </a>
                <a class="button big yellow-color no-col-bg right text-right gap-top30" ng-click="cancel()">
                    <?= LangHelper::get('cancel', 'Cancel') ?>
                </a>
            </fieldset>
        </form>
        <script>
            $(document).ready(function () {
                setTimeout(function () {
                    $("[data-role=dropdown]").dropdown();

                    $('#first_input').focus();
                    $('#first_input').select();
                }, 100);
            });
        </script>
    </div>
</div>

