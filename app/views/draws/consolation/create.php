<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>
                    <?= LangHelper::get('create_consolation_draw', 'Create consolation draw') ?> -
                    <?= ($consolation_type == 1)
                        ? LangHelper::get('main_draw', 'Main draw')
                        : LangHelper::get('qualification_draw', 'Qualification draw');
                    ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row clearfix" class="pad-top20 gap-top20" ng-controller="ConsolationController"
             ng-init="getCheckedSignups(<?= $draw->id?>, <?= $consolation_type?>)" ng-cloak>

            <p class="courts-view">
                <?= LangHelper::get('choose_players_for_consolation_draw', 'Choose players for consolation draw')?>
            </p>

            <p class="danger-color">{{ errors.msg }}</p>

            <button ng-click="checkAll()" style="margin-right: 10px" ng-show="players.length > 0">
                <?= LangHelper::get('check_all', 'Check all')?>
            </button>

            <form name="createForm"
                  ng-submit="createConsolation($event, 'Draw successfully updated', '<?= Request::url() ?>', <?= $consolation_type?>)"
                  novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="row">
                        <div class="half" ng-repeat="player in players track by $index">
                            <label for="player{{player.id}}">
                                <input type="checkbox" id="player{{player.id}}" checklist-model="formData.signups" checklist-value="player.id"> {{player.name}}
                            </label>
                        </div>
                    </div>

                    <div class="row" ng-show="players.length > 0">
                        <div class="gap-top30">
                            <button type="submit" class="button big next-yellow-arrow yellow-color dark-col-bg">
                                <?= LangHelper::get('save', 'Save') ?>
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<script src="/js/ang/checkList.js"></script>
<script src="/js/ang/consolation.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['consolationApp']);
    });
</script>
