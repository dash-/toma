<div class="container ang">
    <div class="grid" ng-controller="TabsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('consolation_draws', 'Consolation draws') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row">
            <div class="place-left">
                <p class="yellow-color">
                    <?= LangHelper::get('tournament', 'Tournament') ?>: <?= $draw->tournament->title ?>
                </p>

                <p class="yellow-color">
                    <?= LangHelper::get('draw_level', 'Draw level') ?>: <?= $draw->level ?> estrellas
                </p>

                <p class="yellow-color">
                    <?= LangHelper::get('draw_category', 'Draw category') ?>: <?= $draw->category->name ?>
                </p>
            </div>

            <div class="place-right">
                <a class="rfet-button rfet-yellow" href="/draws/create-consolation/<?= $draw->id?>?consolation_type=1">
                    <?= LangHelper::get('add_edit_main_consolation_draw', 'Add/edit main consolation players')?>
                </a>
                <a class="rfet-button rfet-yellow" href="/draws/create-consolation/<?= $draw->id?>?consolation_type=2">
                    <?= LangHelper::get('add_edit_qualification_consolation_draw', 'Add/edit qualification consolation players')?>
                </a>
            </div>
        </div>

        <tabset class="pad-top40">
            <a class="rfet-button rfet-yellow place-right call" href="/signups/list/<?= $draw->id ?>">
                <?= LangHelper::get('players_signed', 'Players') ?>
            </a>
            <?php foreach ($players as $key => $teams): ?>
                <tab>
                    <tab-heading>
                        <?php if ($key == 1): ?>
                            <?= LangHelper::get('main_draw_consolation', 'Main draw consolation') ?>
                        <?php elseif ($key == 2): ?>
                            <?= LangHelper::get('qualifying_draw_consolation', 'Qualifying draw consolation') ?>
                        - <?= count($teams) ?>
                        <?php endif;?>
                    </tab-heading>
                    <div class="row pad-top20">
                        <?php if (!ConsolationHelper::checkIfDrawHasScores($draw->id, $key)):?>
                            <a class="rfet-button rfet-danger place-right"
                               href="/draws/consolation-manual-bracket/<?= $draw->id ?>?consolation_type=<?= $key ?>">
                                <?= ($key == 1)
                                    ? LangHelper::get('create_edit_main_consolation_draw', 'Create/edit main consolation draw')
                                    : LangHelper::get('create_edit_qualifying_consolation_draw', 'Create/edit qualifying consolation draw');
                                ?>
                            </a>
                        <?php else: ?>
                            <a class="rfet-button rfet-yellow place-right"
                               href="/draws/consolation-bracket/<?= $draw->id ?>?consolation_type=<?= $key ?>">
                                <?= ($key == 1)
                                    ? LangHelper::get('view_main_consolation_draw', 'View main consolation draw')
                                    : LangHelper::get('view_qualifying_consolation_draw', 'View qualifying consolation draw');
                                ?>
                                <img src="/css/icons/draws.png" alt="">
                            </a>
                        <?php endif?>

                        <div style="width:100% !important; overflow-x:scroll !important">
                            <table class="table bg-dark tournament-table table-scrollable-w800">
                                <thead>
                                <tr>
                                    <th width="15%" class="text-left"><?= LangHelper::get('surname', 'Surname') ?></th>
                                    <th width="15%" class="text-left"><?= LangHelper::get('name', 'Name') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('licence_number', 'Licence number') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('date_of_birth', 'Date of birth') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('ranking_points', 'Ranking points') ?></th>
                                    <th width="10%"
                                        class="text-left"><?= LangHelper::get('submitted_on', 'Submitted on') ?></th>
                                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                        <th width="30%"
                                            class="text-right"><?= LangHelper::get('options', 'Options') ?></th>
                                    <?php endif ?>
                                </tr>
                                </thead>

                                <tbody>
                                <?php foreach ($teams as $player): ?>
                                    <tr>
                                        <td>
                                            <?= TournamentHelper::getPlayerValue($player, 'surname') ?>
                                        </td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'name') ?></td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'licence_number') ?></td>
                                        <td><?= TournamentHelper::getPlayerValue($player, 'date_of_birth') ?></td>
                                        <td><?= $player['ranking_points'] ?></td>
                                        <td><?= DateTimeHelper::GetFullDateTime($player['created_at']) ?></td>

                                        <td class="transparent text-right">

                                        </td>

                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </tab>

            <?php endforeach ?>
        </tabset>

    </div>
</div>

<script src="/js/ang/tabsModule.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['tabsModule']);
        $("[data-role=dropdown]").dropdown();
    });
</script>