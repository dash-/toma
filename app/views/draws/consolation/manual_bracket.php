<script src="/js/thirdparty/dragDrop/angular-dragdrop.js"></script>

<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament">
                <h3>
                    <?php if ($consolation_type == 1): ?>
                        <?= LangHelper::get('main_consolation_draw', 'Main consolation draw') ?>
                    <?php elseif ($consolation_type == 2): ?>
                        <?= LangHelper::get('qualifying_consolation_draw', 'Qualifying consolation draw') ?>
                    <?php endif ?>
                    - <?= LangHelper::get('create_manual_bracket', 'Create manual bracket') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament') ?>
                : <?= $draw->tournament->title ?></p>

            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category') ?>
                : <?= $draw->category->name . ', ' . $draw->typeName() ?></p>
        </div>

        <div class="row clearfix ng-cloak" ng-controller="DragController" data-ng-init="init()"
             data-scUrl="/draws/consolation-manual-data/<?= $draw->id ?>?consolation_type=<?= $consolation_type?>">

            <div class="half side-holder">
                <div class="row">
                    <a class="gap-bottom20 rfet-button rfet-yellow" ng-show="list1.length > 1"
                       ng-click="shuffle(list1)">Randomly order players and move to other side</a>
                </div>
                <div id="scrollbar-tiny">
                    <div class="scrollbar">
                        <div class="track">
                            <div class="thumb">
                                <div class="end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="viewport">
                        <div class="overview">
                            <div ng-repeat="item in list1 track by $index" data-drop="true" ng-model='list1'
                                 jqyoui-droppable="{index: {{$index}}}">
                                <div class="team-holder" data-drag="{{item.drag}}"
                                     data-jqyoui-options="{revert: 'invalid', helper:'clone'}" ng-model="list1"
                                     jqyoui-draggable="{index: {{$index}},placeholder: item.hidden ? 'keep' : true,animate:true}"
                                     ng-hide="!item.name">
                                    <span class="order-number">{{ item.order_num }}</span>
                                    <span class="team-name">
                                        {{item.name}}
                                    </span>
                                    <select class="choose-position" ng-model="position"
                                            ng-options="position.vrijednost for position in positions"
                                            ng-change="onChanged($index, position)"
                                            ng-hide="item.hidden">
                                        <option value="">Choose position</option>
                                    </select>

                                    <input class="small-input" type="text" ng-model="list1[$index].seed_num"
                                           ng-hide='item.hidden' placeholder="SN">

                                </div>
                            </div>
                        </div>
                    </div>
                    <p></p>
                </div>
            </div>

            <div class="half">
                <div class="clearfix placing-holder" ng-repeat="item in list2 track by $index">
                    <div class="drop-holder" data-drop="true" ng-model='list2'
                         jqyoui-droppable="{index: {{$index}}, onDrop:'dropCallback($index, list2[$index])'}">
                        <div class="manual-draw-position-number">{{$index+1}}</div>

                        <div class="team-holder" ng-show="list2[$index].name"
                             data-drag="{{list2[$index].drag}}" data-jqyoui-options="{revert: 'invalid'}"
                             ng-model="list2" jqyoui-draggable="{index: {{$index}}, placeholder:true, animate:true}">
                            <span class="team-name">
                                {{list2[$index].name}}
                            </span>

                            <input class="small-input" ng-change="dropCallback($index, list2[$index])"
                                   type="text" ng-model="list2[$index].seed_num" placeholder="SN"
                                   ng-hide='list2[$index].hidden'>
                        </div>
                    </div>
                </div>

                <a class="button inverse place-right submit-pairs-btn"
                   ng-click="manualSubmit($event, 'POST', 'Pairs successfully created','<?= Request::url() ?>', true, <?= $consolation_type ?>)">
                    <?= LangHelper::get('submit_pairs', 'Submit pairs') ?>
                </a>
            </div>
        </div>
    </div>
</div>

<script src="/js/ang/manualBracket.js"></script>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['manualBracketApp']);
    });
</script>