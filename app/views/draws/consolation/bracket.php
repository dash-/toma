<style type="text/css" media="print">
    @page {
        size: 11in 8.5in;
    }

    .gracket-holder {
        display: block;
    }

</style>
<link rel="stylesheet" type="text/css" media="print" href="/css/bracket-print.css"/>
<div class="container ang">
    <div class="grid" ng-controller="bracketController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= $title ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="place-right sponsor-holder">
            <?php if ($tournament->getSponsor('id')): ?>
                <img class="basic rm-width-club-image" src="<?= $tournament->getSponsorImage() ?>">
                <p><?= $tournament->getSponsor('title') ?></p>
            <?php endif ?>
        </div>

        <div class="place-right sponsor-holder">
            <?php if ($tournament->getRegionImage()):?>
                <img class="basic rm-width-club-image" src="<?= $tournament->getRegionImage() ?>">
            <?php endif?>
        </div>

        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?></p>

            <p class="yellow-color"><?= LangHelper::get('draw_level', 'Draw level') ?>: <?= $draw->level ?>
                estrellas</p>

            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category') ?>
                : <?= $draw->category->name.', '.$draw->genderName().', '.$draw->typeName() ?></p>

            <p>
                <?php if (TournamentHelper::checkIfMainDrawIsFinished($draw->id) == false): ?>

                    <?php if (Request::query('list_type') != 2 && TournamentHelper::checkIfMainWinnerExist($draw->id) == true): ?>
                        <?php if (Auth::user()->hasRole('referee')): ?>
                            <a class="rfet-button rfet-success place-left" ng-click="openSignSheet()">
                                <?= LangHelper::get('submit_main_draw', 'Submit Main Draw') ?>
                            </a>
                        <?php else: ?>
                            <a class="rfet-button rfet-success place-left"
                               href="/draws/submit-main-draw/<?= $draw->id ?>">
                                <?= LangHelper::get('submit_main_draw', 'Submit Main Draw') ?>
                            </a>
                        <?php endif ?>
                    <?php endif ?>

                <?php endif ?>
            </p>
        </div>

        <div class="row gracket-holder">

            <p class="yellow-color bracketTourInfo"
               style="display: none;"><?= LangHelper::get('tournament', 'Tournament') ?>: <?= $tournament->title ?></p>

            <p class="yellow-color bracketTourInfo"
               style="display: none;"><?php if (Request::query('list_type') == 2): ?>
                    <?= 'Qualifying Draw' ?><?php endif ?></p>

            <p class="yellow-color bracketTourInfo"
               style="display: none;"><?php if (Request::query('list_type') != 2): ?>
                    <?= 'Main Draw' ?><?php endif ?></p>

            <p class="yellow-color bracketTourInfo"
               style="display: none;"><?= LangHelper::get('draw_level', 'Draw level') ?>: <?= $draw->level ?>
                estrellas</p>

            <p class="yellow-color bracketTourInfo"
               style="display: none;"><?= LangHelper::get('draw_category', 'Draw category') ?>
                : <?= $draw->category->name ?></p>

            <div id="gracket" data-gracket='<?= $jsArray ?>'></div>
            <!-- <img id="gracket-img"> -->
        </div>

        <div class="row">
            <!--            <a href="" id="tryCanvas">-->
            <!--                <input type="button" value="Test"/>-->
            <!--            </a>-->

            <input type="button" class="btn btn-primary showFullScreen hideResponsive" fullscreen-body=".gracket-holder"
                   value="<?= LangHelper::get('show_fullscreen', 'SHOW FULLSCREEN') ?>"/>
            <a href="<?= URL::to('/bracketprint/print/' . $draw->id . '?list_type=' . Request::query('list_type')). '#print' ?>">
                <input type="button" value="Print bracket" class="hideResponsive"/>
            </a>
            <?php if (Request::query('list_type') == 2 AND !TournamentHelper::checkIfQualificationsAreFinished($draw->id)): ?>
                <a href="<?= URL::to('/schedule/show/' . $tournament->id . '?list_type=' . Request::query('list_type')) ?>">
                    <input type="button" value="<?= LangHelper::get('order_of_play', 'Order of play') ?>">
                </a>
            <?php elseif (Request::query('list_type') == 1 AND !TournamentHelper::checkIfMainDrawIsFinished($draw->id)): ?>
                <a href="<?= URL::to('/schedule/show/' . $tournament->id . '?list_type=' . Request::query('list_type')) ?>">
                    <input type="button" value="<?= LangHelper::get('order_of_play', 'Order of play') ?>">
                </a>
            <?php endif ?>

            <a href="<?= URL::to('/signups/register/'.$draw->id)?>">
                <input type="button" value="<?= LangHelper::get('register', 'Register') ?>">
            </a>

            <!-- check if matches not started -->
            <?php if (!DrawMatchHelper::checkIfDrawHasScores($draw->id, Request::query('list_type'))): ?>
                <a class="place-right rfet-button rfet-danger"
                   href="<?= URL::to('/draws/undo-bracket/' . $draw->id . '?consolation_type=' . Request::query('consolation_type')) ?>">
                    <?= LangHelper::get('undo_this_bracket', 'Undo this bracket') ?>
                </a>
            <?php endif ?>
        </div>
        <div class="row">
            <div class="gracket-box" style="width: 20%">
                <h3><?= LangHelper::get('tournament_info', 'Tournament info') ?></h3>
                <?php if ($tournament->player_representative):?>
                    <p class="fg-white">
                        <?= LangHelper::get('player_representative', 'Player representative')?>:
                        <br>
                        <?= $tournament->player_representative?>
                    </p>
                <?php endif?>
                <?php if ($tournament->official_ball): ?>
                    <p class="fg-white">
                        <?= LangHelper::get('official_ball', 'Official ball')?>: <?= $tournament->official_ball?>
                    </p>
                <?php endif?>

                <p class="fg-white">
                    <?= LangHelper::get('referee', 'Referee')?>: <?= $referee_name?>
                </p>
            </div>
            <div class="gracket-box" style="width: 30%">
                <h3><?= LangHelper::get('seeded_players', 'Seeded players') ?></h3>
                <?php foreach ($seeds as $key => $seed): ?>
                    <p class="fg-white">
                        <?= $key + 1 ?>. <?= DrawMatchHelper::getDoublesPair($seed->teams) ?>
                        - <?= $seed->signup->ranking_points ?> (pts)
                    </p>
                <?php endforeach ?>
            </div>

            <div class="gracket-box place-right">
                <h3><?= $tournament->club->club_name ?></h3>
                <?php if ($tournament->club->image_link): ?>
                    <img class="club-image" src="<?= $tournament->club->image() ?>">
                <?php endif ?>
            </div>

        </div>

        <div class="row clear pad-bottom20">
            <!-- Modal template -->
            <script type="text/ng-template" id="myModalContent.html">
                <?= $results_form_view ?>
            </script>

            <?php if (Auth::user()->hasRole('referee')): ?>
                <script type="text/ng-template" id="refereeSubmit.html">
                    <?= $referee_form_view ?>
                </script>
            <?php endif ?>

            <script type="text/ng-template" id="changeRequest.html">
                <?= $change_request_form_view ?>
            </script>
        </div>
    </div>
</div>

<script src="/js/jquery.gracket.js"></script>
<script src="/js/ang/bracket.js"></script>

<script>
    $(document).ready(function () {
        var history_query = "<?= Request::query('history')?>";
        if (history_query) {
            $("#gracket").hide();
            location.reload();
        }

        $("#gracket").gracket({
            canvasLineWidth: 1,      // adjusts the thickness of a line
            cornerRadius: 0,         // adjusts edges of line
            canvasLineCap: "square",  // or "square"
//            isDoubles: <?//= $isDoubles ? "true" : "false" ?>//,
            roundLabels: <?= $round_names ?>
        });

        $(".g_gracket").width($(".g_winner").width() + $(".g_winner").position().left + 70);

        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['bracket']);
        });

        // quick fix must be redone
        <?php if($scroll_to = Request::query('scrollTo')):?>
        $("html, body").animate({scrollTop: $('#<?= $scroll_to?>').offset().top - 100}, 400);
        <?php endif?>
    });
</script>

