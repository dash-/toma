<div class="ang">
    <div ng-controller="ScheduleController">

        <input type="hidden" ng-model="formData.formData.date_of_play"
               ng-init="formData.date_of_play='<?= $date ?>'">

        <?php for ($i = 0; $i < $draw->tournament->number_of_courts; $i++): ?>

            <div class="clearfix court-column">
                <p class="fg-white bb">Court <?= $i + 1 ?></p>
                <?php for ($j = 0; $j < $default_counter; $j++): ?>

                    <input type="hidden" ng-model="formData.match<?= $i . '' . $j ?>.order"
                           ng-init="formData.match<?= $i . '' . $j ?>.order=<?= $j + 1 ?>">
                    <input type="hidden" ng-model="formData.match<?= $i . '' . $j ?>.court_num"
                           ng-init="formData.match<?= $i . '' . $j ?>.court_num=<?= $i + 1 ?>">
                    <div class="row court-match editable" id="match<?= $i . '' . $j ?>">
                        <?php if (DrawMatchHelper::getScheduleData($i, $j, $draw->tournament->id, $date, $list_type)): ?>

                            <?php
                            $match_data = DrawMatchHelper::getScheduleData($i, $j, $draw->tournament->id, $date, $list_type, false);
                            ?>

                            <p class="fg-white text-center" id="<?= $match_data->id ?>">
                                <?= $match_data->time_of_play ?>
                            </p>

                            <?php if ($match_data->match && !is_null($match_data->match->team1_data)): ?>
                                <p class="fg-white text-center">
                                    <?= $match_data->match->team1_data->full_name() ?>
                                    <?php if (isset($match_data->match->team1_data_array[1])): ?>
                                        <br/>
                                        <?= $match_data->match->team1_data_array[0]->surname . ' ' . $match_data->match->team1_data_array[0]->name ?>
                                    <?php endif; ?>
                                </p>

                                <div class="scoreholder rm-float center-score">
                                    <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score1'] ?>
                                </div>
                            <?php else: ?>
                                <p class="fg-white text-center">
                                    BYE
                                </p>
                            <?php endif ?>

                            <p class="fg-white text-center">
                                vs
                            </p>

                            <?php if ($match_data->match && !is_null($match_data->match->team2_data)): ?>
                                <div class="scoreholder rm-float center-score">
                                    <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score2'] ?>
                                </div>

                                <p class="fg-white text-center">
                                    <?= $match_data->match->team2_data->full_name() ?>
                                    <?php if (isset($match_data->match->team2_data_array[1])): ?>
                                        <br/>
                                        <?= $match_data->match->team2_data_array[0]->surname . ' ' . $match_data->match->team2_data_array[0]->name ?>
                                    <?php endif; ?>
                                </p>
                            <?php else: ?>
                                <p class="fg-white text-center">
                                    BYE
                                </p>
                            <?php endif ?>

                            <div>
                                <?php if (isset($match_data)): ?>
                                    <div class="text-center enter-match-score-schedule-holder"
                                         ng-click="openModal(<?= $match_data->match_id ?>, '<?= $date ?>')">
                                        <?= LangHelper::get('enter_match_score', 'Enter match score') ?>
                                    </div>
                                    <div class="text-center enter-match-score-schedule-holder">
                                        <a href="/match/match-live-mode/<?= $match_data->match_id ?>">
                                            <?= LangHelper::get('enter_livescore_mode', 'Enter live score mode') ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?= Form::select('match_status', $match_statuses, $match_data->match_status, array('class' => 'match_status', 'match_id' => $match_data->match_id, 'schedule_id' => $match_data->id)); ?>

                            </div>
                            <?php if ($match_data->match && !$match_data->match->team1_score AND !$match_data->match->team2_score): ?>
                                <div class="text-center" style="margin-top: 10px;">
                                    <a href="/draws/remove-scheduled-match/<?= $match_data->id . '/' . $draw_id_for_redirect . '/' . $list_type . '/' . $match_data->date_of_play ?>"
                                       class="btn button danger remove-schedule"
                                       schedule-id="<?= $match_data->id ?>"><?= LangHelper::get('remove', 'Remove') ?></a>
                                </div>
                            <?php endif ?>

                        <?php else: ?>

                            <div class="form-control">
                                <div class="input-control text">

                                    <?= Form::select('type_of_time', $time_types, null, array('required', 'ng-model' => 'formData.match' . $i . '' . $j . '.type_of_time', 'class' => 'type_of_time')); ?>

                                    <div class="clear timepicker-holder">
                                        <timepicker ng-change="changed()"
                                                    ng-model="formData.match<?= $i . '' . $j ?>.time_of_play"
                                                    hour-step="1" minute-step="5" show-meridian="false"
                                                    formattime></timepicker>
                                    </div>
                                </div>
                            </div>

                            <div class="form-control">
                                <div class="input-control text">
                                    <?= Form::select('match_id', $pairs, null, array('required', 'ng-model' => 'formData.match' . $i . '' . $j . '.match_id', 'class' => 'pairs_list')); ?>
                                </div>
                            </div>


                        <?php endif ?>
                    </div>

                <?php endfor; ?>

            </div>


        <?php endfor; ?>


        <div class="row clear">

            <a class="button info submit-schedule"
               ng-click="scheduleSubmit($event, 'POST', 'Schedule successfully created','/draws/schedule/<?= $draw->id ?>', true, <?= $list_type ?>)"><?= LangHelper::get('submit_schedule', 'Submit schedule') ?></a>

        </div>
        <script type="text/ng-template" id="myModalContent.html"><!-- Modal template -->
            <?= $results_form_view ?>
        </script>


    </div>

</div>

<script src="/js/ang/scheduleForm2.js"></script>
<script type="text/javascript">


    var pairs = <?= $json_pairs ?>;
    var selected_values = [];

    $(function () {

        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['scheduleApp']);
        });

        $('.type_of_time').on('change', function () {
            if ($(this).val() == 3)
                $(this).closest('.form-control').find('.timepicker-holder').css('visibility', 'hidden');
            else
                $(this).closest('.form-control').find('.timepicker-holder').css('visibility', 'visible');
        });
        $('.match_status').on('change', function () {
            if ($(this).val() != '') {
                var match_id = $(this).attr('match_id');
                var schedule_id = $(this).attr('schedule_id');
                $.ajax({
                    cache: false,
                    type: "POST",
                    dataType: "JSON",
                    url: '/draws/change-match-status',
                    data: {'status': $(this).val(), 'match_id': match_id},
                    success: function (response) {
                        evt = app.BrainSocket.message('generic.event', [schedule_id]);
                    }

                });
            }
        });

        $('.remove-schedule').on('click', function () {
            var schedule_id = $(this).attr('schedule-id');
            evt = app.BrainSocket.message('generic.event', [schedule_id]);
        });

        $(".pairs_list").change(function () {
            updateSelectBox();
        })
    });

    function updateSelectBox() {
        var selected_values = [];
        $.each($(".pairs_list"), function (key, elem) {
            var sl = $(elem).val();
            if (sl != '')
                selected_values.push(sl);
        });
        var selectBox = '<option selected="selected" value="">---</option>';
        $.each(pairs, function (key, value) {
            if ($.inArray(key.toString(), selected_values) < 0) {
                selectBox += '<option value=' + key + '>' + value + '</option>';
            }
        })
        $.each($(".pairs_list"), function (key, elem) {
            if ($(elem).val() == '')
                $(elem).html(selectBox);
        })
        $("select").select2({width: '200px'});
    }

</script>
