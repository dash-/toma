<script src="/js/thirdparty/dragDrop/angular-dragdrop.js"></script>


<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament">
                <h3>
                    <?php if ($list_type == 1): ?>
                        <?= LangHelper::get('main_draw', 'Main draw') ?>
                    <?php elseif ($list_type == 2): ?>
                        <?= LangHelper::get('qualifying_draw', 'Qualifying draw') ?>
                    <?php endif ?>
                    - <?= LangHelper::get('create_manual_bracket', 'Create manual bracket') ?>

                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament') ?>
                : <?= $draw->tournament->title ?></p>

            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category') ?>
                : <?= $draw->category->name . ', ' . $draw->typeName() .''.($draw->is_prequalifying ? ' ('.LangHelper::get('pq', 'PQ').')' : '') ?></p>
        </div>

        <div class="row clearfix ng-cloak" ng-controller="DragController" data-ng-init="init()"
             data-scUrl="/draws/signups-data/<?= $draw->id ?>?list_type=<?= $list_type ?>">
            <div class="row">
                <a class="place-right rfet-button rfet-yellow" ng-hide="automatic_draw_disabled"
                   ng-click="automaticDraw('<?= URL::to('draws/automatic-draw/' . $draw->id) . '?list_type=' . $list_type ?>')">
                    <?= LangHelper::get('automatic_draw', 'Automatic draw') ?>
                </a>
                <div class="rules-modal" data-toggle="modal" data-target="#rules-modal">
                <p class="manual-bracket-warning" ng-show="automatic_draw_disabled">
                    <?= LangHelper::get('automatic_draw_disabled', 'Automatic draw is disabled because rules in system do not apply for this type of draw! Click here for the list of rules.')?>
                </p>
                </div>
            </div>
            <div class="modal fade" id="rules-modal" tabindex="-1" role="dialog" aria-labelledby="rules-modal-label" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title yellow-color" id="myModalLabel"><?= LangHelper::get('seed_rules', 'Seed rules')?></h4>
                        </div>
                        <div class="modal-body">
                            <table class="table bg-dark tournament-table text-left">
                                <thead>
                                    <th class="yellow-color"><?= LangHelper::get('number_of_players', 'Number of players') ?></th>
                                    <th class="yellow-color"><?= LangHelper::get('number_of_seeds', 'Number of seeds') ?></th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="yellow-color">8 <?= LangHelper::get('players', 'Players')?> </td>
                                        <td class="yellow-color">4 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="yellow-color">9-16 <?= LangHelper::get('players', 'Players')?></td>
                                        <td class="yellow-color">4 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="yellow-color">17-23 <?= LangHelper::get('players', 'Players')?></td>
                                        <td class="yellow-color">4 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="yellow-color">24-32 <?= LangHelper::get('players', 'Players')?></td>
                                        <td class="yellow-color">8 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="yellow-color">33-47 <?= LangHelper::get('players', 'Players')?></td>
                                        <td class="yellow-color">8 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                    <tr>
                                        <td class="yellow-color">48-64 <?= LangHelper::get('players', 'Players')?></td>
                                        <td class="yellow-color">16 <?= LangHelper::get('seeds', 'Seeds') ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="rfet-button rfet-yellow" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="half side-holder">

                <div class="row">
<!--                    <a class="gap-bottom20 rfet-button rfet-yellow" ng-show="list1.length > 1"-->
<!--                       ng-click="shuffle(list1)">Randomly order players and move to other side</a>-->
                </div>
                <div id="scrollbar-tiny">
                    <div class="scrollbar">
                        <div class="track">
                            <div class="thumb">
                                <div class="end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="viewport">
                        <div class="overview">
                            <div ng-repeat="item in list1 track by $index" data-drop="true" ng-model='list1'
                                 jqyoui-droppable="{index: {{$index}}}">
                                <div class="team-holder {{item.same_points_class}}" data-drag="{{item.drag}}"
                                     data-jqyoui-options="{revert: 'invalid', helper:'clone'}" ng-model="list1"
                                     jqyoui-draggable="{index: {{$index}},placeholder: item.hidden ? 'keep' : true,animate:true}"
                                     ng-hide="!item.name">
                                    <span class="order-number">{{ item.order_num }}</span>
                                    <span class="team-name">
                                        {{item.name}}
                                    </span>

                                    <?= Form::select('player_type', $qualifier_types, null,
                                        array('ng-model' => 'list1[$index].move_from_qualifiers_type',
                                              'ng-hide'  => 'item.hidden || list_type == 2', 'class' => 'pos-right gap-right10')); ?>

                                    <select class="choose-position" ng-model="position"

                                            ng-options="position.vrijednost for position in positions"
                                            ng-change="onChanged($index, position)"
                                            ng-hide="item.hidden">
                                        <option value="">Choose position</option>
                                    </select>
                                    <input class="small-input" type="text" ng-model="list1[$index].seed_num"
                                           ng-hide='item.hidden || item.ranking_points == 0' placeholder="SN">

                                </div>
                            </div>
                        </div>
                    </div>
                    <p></p>
                </div>
            </div>

            <div class="half">
                <div class="clearfix placing-holder" ng-repeat="item in list2 track by $index">
                    <div class="drop-holder" data-drop="true" ng-model='list2'
                         jqyoui-droppable="{index: {{$index}}, onDrop:'dropCallback($index, list2[$index])'}">
                        <div class="manual-draw-position-number">{{$index+1}}</div>

                        <div class="team-holder" ng-show="list2[$index].name"
                             data-drag="{{list2[$index].drag}}" data-jqyoui-options="{revert: 'invalid'}"
                             ng-model="list2" jqyoui-draggable="{index: {{$index}}, placeholder:true, animate:true}">
                            <span class="team-name">
                                {{list2[$index].name}}
                            </span>

                            <input class="small-input" ng-change="dropCallback($index, list2[$index])"
                                   type="text" ng-model="list2[$index].seed_num" placeholder="SN"
                                   ng-hide='list2[$index].hidden  || list2[$index].ranking_points == 0'>

                            <?= Form::select('player_type', $qualifier_types, null, array(
                                'ng-model' => 'list2[$index].move_from_qualifiers_type',
                                'ng-hide'  => 'item.hidden || list_type == 2',
                                'class'    => 'pos-right gap-right10')); ?>
                        </div>
                    </div>
                </div>

                <a class="button inverse place-right submit-pairs-btn"
                   ng-click="manualSubmit($event, 'POST', 'Pairs successfully created','<?= Request::url() ?>', true, <?= $list_type ?>)">
                    <?= LangHelper::get('submit_pairs', 'Submit pairs') ?>
                </a>
            </div>
        </div>
    </div>
</div>



<script src="/js/ang/manualBracket.js"></script>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['manualBracketApp']);
    });

    $(document).ready(function(){
        setTimeout(function(){
            var $scrollbar = $("#scrollbar-tiny");
            $scrollbar.tinyscrollbar();
        }, 1000);

    });
</script>