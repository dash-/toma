<div class="row court-match <?= !$checker ? 'bg-black' : null ?>">

    <?php if ( $checker ): ?>
        <p class="courts-view text-center gap-bottom20 pad-bottom20">
            <?= $match_data->time_of_play?>
        </p>

        <?php if (!is_null($match_data->match->team1_data)): ?>



            <p class="courts-view text-center schedule-team-name">
                <?= $match_data->match->team1_data->name.' '.$match_data->match->team1_data->surname ?>
            </p>

            <div class="scoreholder rm-float center-score">
                <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score1']?>
            </div>
        <?php else: ?>
            <div class="scoreholder rm-float center-score"></div>
            <p class="fg-white text-center schedule-team-name">
                BYE
            </p>
        <?php endif ?>

        <p class="courts-view text-center">
            vs
        </p>

        <?php if (!is_null($match_data->match->team2_data)): ?>


            <div class="scoreholder rm-float center-score">
                <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score2']?>
            </div>

            <p class="courts-view text-center schedule-team-name">
                <?= $match_data->match->team2_data->name.' '.$match_data->match->team2_data->surname ?>
            </p>
        <?php else: ?>
            <div class="scoreholder rm-float center-score"></div>
            <p class="fg-white text-center schedule-team-name">
                BYE
            </p>
        <?php endif ?>



        <p class="<?= DrawMatchHelper::getNotStarted($match_data)?>" style="<?= DrawMatchHelper::getMatchStatus($match_data)?>; margin-bottom: 0px;">
            <?= $match_data->status()?>
        </p>

    <?php else: ?>
       
        <p></p>

    <?php endif ?>
</div>