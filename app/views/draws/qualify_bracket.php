<div class="container ang">
    <div class="grid">
        <!-- <div class="row">
            <div class="clearfix">
                <h3 class="fg-white place-left"><?= $title?></h3>
                <a class="button primary big call next" href="<?= URL::previous()?>">Back</a>
            </div>
        </div>

        <div class="row">
            <p class="fg-white">Tournament: <?= $draw->tournament->title?></p>
            <p class="fg-white">Draw category: <?= $draw->category->name?></p>
        </div> -->
        <div class="row">
             <a<a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= $title?></h3>
               <span class="line"></span>
             </div>
        </div>
        <div class="row">
            <p class="yellow-color">Tournament: <?= $draw->tournament->title?></p>
            <p class="yellow-color">Draw category: <?= $draw->category->name?></p>
        </div>
        <div class="row">
            
            <div id="bracket"></div>

        </div>
</div>


<link rel="stylesheet" href="/js/thirdparty/bracket/jquery.bracket.css">
<script src="/js/thirdparty/bracket/jquery.bracket.js"></script>

<script>
    
$(document).ready(function() {

    var res = <?= $results?>;
    var half = res.splice(0, Math.ceil(res.length / 2));    
    var half_of_half = half.splice(0, Math.ceil(half.length / 2));  


    var data = {
        teams : <?= $jsArray?>
        // results: [res, res, res]
    }
     
    $(function() {
        $('#bracket').bracket({
            init: data /* data to initialize the bracket with */ 
        });
    });
});

</script>