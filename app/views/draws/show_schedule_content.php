<!-- <input type="button" value="Print test" class="printBrackets"> -->
<?php foreach ($dates as $key => $date): ?>
<?php if (DrawMatchHelper::checkScheduleDataByDates($key, $tournament->id) > 0): ?>
    <h3 class="text-center dateOOP"><?= LangHelper::get('order_of_play', 'ORDER OF PLAY')?>: <?= $date?></h3>
    <div class="row clearfix">
        <?php for ($i=0; $i < $tournament->number_of_courts; $i++):?>
            <!-- <#?= $occupied_tiles = DrawMatchHelper::extraTiles($tournament->id, $date, $i+1) ?> -->
        <div class="clearfix court-column">
            <p class="courts-view courtnum">Court <?= $i+1?></p>
                <?php for ($j=0; $j <= $default_counter/2; $j++):?>
                    <?php
                        $match_data = DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type,  false);?>
                    <?php if ($j!=0 && $j%2==0): ?>
                        <!-- <div class="pageBreak">-</div><br> -->
                    <?php endif; ?>
                    <div class="match<?= ($match_data) ? $match_data->id : null?>" style="<?= ($match_data) ? 'visibility:visible;' : 'display:none;'?>">
                        <div class="row court-match <?= !DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type) ? 'hideTile bg-black' : null ?>">

                            <?php if ( DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type) ): ?>
                                <p class="courts-view text-center gap-bottom20 pad-bottom20 timeOfPLay">
                                    <?= $match_data->time_of_play?>
                                </p>

                                <?php if (!is_null($match_data->match->team1_data)): ?>

                                    <p class="courts-view text-center schedule-team-name">
                                        <?= $match_data->match->team1_data->full_name() ?>
                                    </p>

                                    <div class="scoreholder rm-float center-score scoreOOP">
                                        <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score1']?>
                                    </div>
                                <?php else: ?>
                                    <div class="scoreholder rm-float center-score"></div>
                                    <p class="courts-view text-center schedule-team-name">
                                        BYE
                                    </p>
                                <?php endif ?>

                                <p class="courts-view text-center vsOOP">
                                    vs
                                </p>

                                <?php if (!is_null($match_data->match->team2_data)): ?>


                                    <div class="scoreholder rm-float center-score">
                                        <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score2']?>
                                    </div>

                                    <p class="courts-view text-center schedule-team-name">
                                        <?= $match_data->match->team2_data->full_name() ?>
                                    </p>
                                <?php else: ?>
                                    <div class="scoreholder rm-float center-score"></div>
                                    <p class="courts-view text-center schedule-team-name">
                                        BYE
                                    </p>
                                <?php endif ?>



                                <p class="match-statuses <?= DrawMatchHelper::getNotStarted($match_data)?>" style="<?= DrawMatchHelper::getMatchStatus($match_data)?>; margin-bottom: 0px;">
                                    <?= $match_data->status()?>
                                </p>

                            <?php else: ?>
                               
                                <p></p>

                            <?php endif ?>
                        </div>
                    </div>

                <?php endfor;?>

            </div>

        <?php endfor;?>

    </div>

<?php endif ?>

<?php endforeach ?>

<script>

    $(function(){
        if(location.hash)
            window.print();
    });
</script>