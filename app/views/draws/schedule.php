<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
             <div class="table-title tournament">
                <h3>
                    <?php if($list_type == 1):?>
                        <?= LangHelper::get('main_draw', 'Main draw')?>
                    <?php elseif($list_type == 2):?>
                        <?= LangHelper::get('qualifying_draw', 'Qualifying draw')?>
                    <?php endif?>
                     - <?= LangHelper::get('create_schedule', 'Create schedule')?>

                </h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="row">
            <p class="yellow-color"><?= LangHelper::get('tournament', 'Tournament')?>: <?= $draw->tournament->title?></p>
            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category')?>: <?= $draw->category->name?></p>
        </div>
        <a href="<?= URL::to("/schedule/show/". $draw->tournament->id."?list_type=". $list_type."#print") ?>">
            <input type="button" value="Print order of play" class="btn btn-primary printOOP hideResponsive"/>
        </a>
        <a href="<?= URL::to("signups?open=". $draw->tournament->id)?>">
            <input type="button" value="<?= LangHelper::get('signups', 'Signups')?>" class="btn btn-primary"/>
        </a>

        
        <div class="row clearfix">
            <h3>
                <?= LangHelper::get('order_of_play', 'Order of play')?>
            </h3>
            <div class="row clearfix"></div>
     
            <!-- <div class="form-control"> -->
                <!-- <label><#?= LangHelper::get('date_of_play', 'Date of play') ?></label> -->
                <!-- <div class="input-control text"> -->
               <!--      <#?= Form::select('date_of_play', $dates_array, $date_of_play, array('required', 'ng-model' => 'formData.date_of_play', 'class' => 'place-left', 'id' => 'loadResults')); ?> -->
                <!-- </div> -->
            </div>
            <div class="orderOfplay">
                <?php foreach($dates_array as $key => $dates):  ?>
                    <div class="button date_select" data-id="<?= $key ?>">
                        <div class="timeTextD"><?= date("j", strtotime($key)); ?></div>
                        <div class="timeTextM"><?= date("M", strtotime($key)); ?></div>
                        <div class="timeTextY"><?= date("Y", strtotime($key)); ?></div>
                    </div>
                <?php endforeach ?>
                </div>
            <div id="content-loader">
                <?= $view_partial?>
            </div>
        </div>

</div>


<script>

    $(document).ready(function () {
        $("select").select2({ width: '200px' });
        $("body").on("click", ".date_select", function(e){
            var date_sel = this;
            $(".metro .button.date_select").css('background-color', '#009578');

            e.preventDefault();
            var data = $(this).data("id");

            if(data!=''){
                
                $('.metro').prepend('<div id="loading-bar-spinner"><div class="spinner-icon"></div></div>');
                $('body').prepend('<div class="overlay"></div>');

                $('#content-loader').load('/draws/schedule-data/<?= $draw->id?>?list_type=<?= $list_type?>&redirection_draw=<?= $draw_id_for_redirect ?>', data, function () {
                    $("select").select2({ width: '200px' });
                    

                    $('.metro #loading-bar-spinner').fadeOut(500);
                    $('.overlay').fadeOut(600);
                    $(date_sel).css('background-color', '#FFC627');

                });

            }

        });

        $('#loadResults').on('change', function() {
            var data = $(this).val();

            if(data!=''){
                
                $('.metro').prepend('<div id="loading-bar-spinner"><div class="spinner-icon"></div></div>');
                $('body').prepend('<div class="overlay"></div>');

                $('#content-loader').load('/draws/schedule-data/<?= $draw->id?>?list_type=<?= $list_type?>&redirection_draw=<?= $draw_id_for_redirect ?>', data, function () {
                    $("select").select2({ width: '200px' });
                    

                    $('.metro #loading-bar-spinner').fadeOut(500);
                    $('.overlay').fadeOut(600);
                });
            }

        });
        <?php if(Request::query("date_of_play")): ?>

            $('.metro').prepend('<div id="loading-bar-spinner"><div class="spinner-icon"></div></div>');
            $('body').prepend('<div class="overlay"></div>');

            $('#content-loader').load('/draws/schedule-data/<?= $draw->id ?>?list_type=<?= $list_type?>&redirection_draw=<?= $draw_id_for_redirect ?>', '<?= Request::query("date_of_play") ?>', function () {
                $("select").select2({ width: '200px' });

                $('.metro #loading-bar-spinner').fadeOut(500);
                $('.overlay').fadeOut(600);
            });
        <?php endif; ?>

    });
</script>