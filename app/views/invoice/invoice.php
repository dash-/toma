<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="/css/invoice.css">
    <script type="text/javascript" src="/js/thirdparty/external/jquery.1-11.1.min.js"></script>
</head>

<body>

<input type="button" value="Print invoice" class="btn btn-primary printInvoice"/>

<div id="paper-size">

    <img class="logoBig" src="<?= URL::to('/css/images/LogoLogin.png') ?>" title="Real Federación Española de Tenis"
         alt="Real Federación Española de Tenis"/>

    <div class="head-info"> Passeig Olimpic, 17-19 (Estadi Olimpic)<br><span
            style="padding:57px"> 0 8 0 3 8  BARCELONA </span><br> <br>TELEFONOS 932 005 355 - 932 005 878 <br><span
            style="padding-left:90px;">932 091 090 - 932 091 377</span><br><span
            style="padding:57px">FAX 932 021 279</span><br><span style="float:right">http://www.rfet.es <br>e-mail:rfet@rfet.es</span>
    </div>


    <div class="tournament-organizer"><?= $tournament->club->club_name ?> <br> <?= $tournament->club->club_city ?>
        <br> <?= $tournament->club->club_address ?> <br><?= $tournament->club->province->province_name ?>
        <br> <?= $tournament->club->nif ?>
    </div>

    <div class="facture-num">Date
        <div class="invoice-value">
            20/11/2014
        </div>
    </div>

    <div class="facture-num">Facture n.
        <div class="invoice-value">
            117045
        </div>
    </div>

    <div class="facture-num">Code
        <div class="invoice-value">
            1306004
        </div>
    </div>

    <div class="facture-num">Vencimiento
        <div class="invoice-value">
            21.11.2014
        </div>
    </div>

    <table class="table">
        <tr>
            <th><?= LangHelper::get('amount', 'Amount') ?></th>
            <th style="text-align:left !important"><?= LangHelper::get('draws', 'Draws') ?></th>
            <th><?= LangHelper::get('price', 'Price') ?></th>
            <th><?= LangHelper::get('total', 'Total') ?></th>
        </tr>
        <?php foreach ($number_of_draws as $numDraws): ?>
            <tr>
                <td><?= TournamentHelper::countDrawPlayers($numDraws->id); ?></td>
                <td style="text-align:left !important;">
                    <?= $numDraws->genderName() ?>,
                    <?= $numDraws->typeName() ?>,
                    <?= $numDraws->category->name ?>
                </td>
                <td><?= TournamentDrawHelper::TournamentDrawPrice($numDraws->draw_category_id, $numDraws->prize_pool, TournamentHelper::countDrawPlayers($numDraws->id))['coefficient']; ?></td>
                <td><?= TournamentDrawHelper::TournamentDrawPrice($numDraws->draw_category_id, $numDraws->prize_pool, TournamentHelper::countDrawPlayers($numDraws->id))['price']; ?></td>
            </tr>
        <?php endforeach ?>

    </table>
    <div class="total">
        <span>Total --> </span> <?= $sum; ?>
    </div>
    <div class="clause">
        Factura extenta de IVA segun el articulo 20.uno.13 de la ley de IVA 37/92 <br> <br><span class="padding-clause">%I.V.A.</span>Importe
        I.V.A.
    </div>
</div>
</body>
</html>

<script>
    $(".printInvoice").on("click", function () {
        window.print();
    });
</script>


