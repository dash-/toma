<div id="gallery-carousel" class="carousel slide" data-ride="carousel">
       
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php if (count($gals)): ?>
            <?php foreach ($gals as $key => $image): ?>
                <div class="item <?= ($key == 0) ? 'active' :  NULL?>">
                    <img src="<?= $image->image('thumbnails')?>" alt="">
                </div>    
            <?php endforeach ?>
        <?php else: ?>
            <div class="item active">
                    <img src="/uploads/images/gallery/default.jpg" alt="">
                </div> 
        <?php endif ?>

    </div>
    


    <!-- Controls -->
    <a class="left carousel-control" href="#gallery-carousel" role="button" data-slide="prev">
        <span class="i-arrow-left"></span>
    </a>
    <a class="right carousel-control" href="#gallery-carousel" role="button" data-slide="next">
        <span class="i-arrow-right-2"></span>
    </a>
</div>

