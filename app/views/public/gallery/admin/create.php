<div class="page-title">
	<h1>
		<?= LangHelper::get('gallery_administration', 'Gallery administration')?>
	</h1>

	<a class="pull-right btn-sm btn-default gap-top20 btn" href="<?= URL::to($tournament_slug.'/gallery-admin')?>">
		Back
	</a>
</div>

<div class="clear" ng-app="gallery" ng-controller='GalleryAdminController' ng-cloak>
	
	<div class="well well-small">
		
		<form name="uploadForm" id="redirectAfterUpload" method="post" class="dropzone" redirect="true"
                  action="<?= Request::url() ?>" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

			<div class="form-group" id="holder-image">
				<label for="image" class="control-label">
					<?= LangHelper::get('image', 'Image')?>
					<span class="form-error"></span>
				</label>
				<input type="file" class="file" name="image" multiple>
			</div>
			<button type="submit" class="clear btn btn-lg btn-default">
                <?= LangHelper::get('upload', 'Upload')?>
            </button>
        </form>

	</div>

</div>