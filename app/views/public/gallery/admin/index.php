<div class="page-title">
	<h1><?= LangHelper::get('gallery_administration', 'Gallery administration')?></h1>
</div>

<div ng-app="gallery" ng-controller="GalleryAdminController" ng-cloak>
	
	<div class="well well-small">
		
		<form name="uploadForm" id="redirectAfterUpload" method="post" class="dropzone" redirect="true"
                  action="<?= Request::url() ?>" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

			<div class="form-group" id="holder-image">
				<label for="image" class="control-label">
					<?= LangHelper::get('image', 'Image')?>
					<span class="form-error"></span>
				</label>
				<input type="file" class="file" name="image" multiple>
			</div>
			<button type="submit" class="clear btn btn-lg btn-default">
                <?= LangHelper::get('upload', 'Upload')?>
            </button>
        </form>

	</div>


	<?php foreach ($tournament->galleries->all() as $image): ?>
		
		<div class="col-md-4" id="asset_<?= $image->id?>">
			<div class="gallery-asset-holder clearfix">
				<a class="btn btn-danger btn-xs delete_asset" 
					ng-click="remove($event, '<?= URL::to($tournament_slug.'/gallery-admin/image', $image->id)?>')">
					Delete
				</a>
				<img class="img-thumbnail" src="<?= $image->image('thumbnails')?>" alt="">
			</div>
		</div>

	<?php endforeach ?>

</div>