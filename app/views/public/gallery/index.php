<div class="page-title">
	<h1><?= LangHelper::get('photos', 'Photos') ?></h1>
</div>


<div class="clearfix gap-top20">
	<?php foreach ($tournament->galleries as $image): ?>
		
		<div class="gal-img-holder">
			<a href="<?= $image->image('full')?>" class="fancybox" rel="group">
            	<img src="<?= $image->image('thumbnails')?>" alt="">
            </a>
		</div>

	<?php endforeach ?>
	

</div>