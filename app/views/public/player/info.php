<div class="page-title">
    <h1><?= LangHelper::get('player_profile', 'Player Profile')?></h1>
</div>

<div class="clearfix">

    <div>
        <div>

            <p><span><?= LangHelper::get('name', 'Name') ?>:</span> <?= $player->contact->name ?></p>

            <p><span class="color-rfet"><?= LangHelper::get('surname', 'Surname') ?>
                    :</span> <?= $player->contact->surname ?></p>

            <p><span class="color-rfet"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>
                    :</span> <?= $player->contact->date_of_birth ?></p>

            <p><span class="color-rfet"><?= LangHelper::get('city', 'City') ?>:</span> <?= $player->contact->city ?></p>

        </div>

        <div>

            <p><span class="color-rfet"><?= LangHelper::get('ranking', 'Ranking') ?>: </span><?= $player->ranking ?></p>

            <p><span class="color-rfet"><?= LangHelper::get('nationality', 'Nationality') ?>
                    :</span> <?= $player->nationality ?></p>


        </div>

        <div>

            <div class="page-title">
                <h1>Previous matches</h1>
            </div>

            <?php foreach ($results as $rt): ?>

                <?php foreach ($rt->scores as $sm): ?>

                    <?= $sm->set_team1_score . \DrawMatchHelper::matchTie($sm->set_team1_tie) . '-' . $sm->set_team2_score . \DrawMatchHelper::matchTie($sm->set_team2_tie) . '; ' ?>
                    <?php echo '' ?>

                <?php endforeach ?>

            <?php endforeach ?>


        </div>
    </div>

</div>

