<div class="page-title">
    <h1><?= LangHelper::get('search', 'Search')?></h1>
</div>

<div class="clearfix" ng-app="playerSearch" ng-controller="PlayerSearchController">

    <a class="btn btn-danger" ng-click="hideForm=false" ng-show="hideForm">
        <?= LangHelper::get('show_form', 'Show form') ?>
    </a>

    <form name="editForm" class="col-lg-7" novalidate ng-show="!hideForm">
        <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
        <div class="clearfix gap-bottom10">
            <span class="sex-error" ng-show="errors.sex"><?= LangHelper::get('please_choose_sex', 'Please choose sex')?></span>
            <div class="radio">
                <label>
                    <input type="radio" name="sex" ng-model="formData.sex" value="M" />
                    <span class="check"></span>
                    <?= LangHelper::get('male', 'Male')?>
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="sex" ng-model="formData.sex" value="F" />
                    <span class="check"></span>
                    <?= LangHelper::get('female', 'Female')?>
                </label>
            </div>
        </div>

        <div class="clearfix search-box"">
            <div class="form-group" id="holder-email">
                <label for="surname" class="control-label">
                    <?= LangHelper::get('surname', 'Surname')?>
                    <span class="form-error" ng-show="errors.surname">
                        <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                    </span>
                </label>
                <input type="text" class="form-control" ng-model="formData.surname">
            </div>
            <div class="submit-btn-inline">
                <a class="btn btn-default pull-right" ng-click="searchByName($event)"><?= LangHelper::get('search', 'Search')?></a>
            </div>
        </div>

        <hr class="form-separator">

        <div class="clearfix search-box"">
            <div class="form-group" id="holder-licence_number">
                <label for="licence_number" class="control-label">
                    <?= LangHelper::get('licence_number', 'Licence number')?>
                    <span class="form-error" ng-show="errors.licence_number">
                                <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                            </span>
                </label>
                <input type="text" class="form-control" ng-model="formData.licence_number">
            </div>
            <div class="submit-btn-inline">
                <a class="btn btn-default pull-right" ng-click="searchByLicence($event)"><?= LangHelper::get('search', 'Search')?></a>
            </div>
        </div>

        <hr class="form-separator">

        <div class="clearfix search-box">
            <div class="inline-inputs-holder">
                <div class="from-to-form gap-left0">
                    <div class="form-group" id="holder-ranking_from">
                        <label for="ranking_from" class="control-label">
                            <?= LangHelper::get('ranking_from', 'Ranking from')?>
                            <span class="form-error" ng-show="errors.ranking_from">
                                <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                            </span>
                        </label>
                        <input type="text" class="form-control" ng-model="formData.ranking_from">
                    </div>
                </div>

                <div class="from-to-form">
                    <div class="form-group second-inline-input" id="holder-ranking_to">
                        <label for="ranking_to" class="control-label">
                            <?= LangHelper::get('ranking_to', 'Ranking to')?>
                            <span class="form-error" ng-show="errors.ranking_to">
                                <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                            </span>
                        </label>
                        <input type="text" class="form-control" ng-model="formData.ranking_to">
                    </div>
                </div>
            </div>

            <div class="submit-btn-inline">
                <a class="btn btn-default pull-right" ng-click="searchByRanking($event)"><?= LangHelper::get('search', 'Search')?></a>
            </div>
        </div>

        <hr class="form-separator">

        <div class="clearfix search-box">
            <div class="inline-inputs-holder">
                <div class="from-to-form gap-left0">
                    <div class="form-group" id="holder-points_from">
                        <label for="points_from" class="control-label">
                            <?= LangHelper::get('points_from', 'Points from')?>
                            <span class="form-error" ng-show="errors.points_from">
                                    <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                </span>
                        </label>
                        <input type="text" class="form-control" ng-model="formData.points_from">
                    </div>
                </div>

                <div class="from-to-form">
                    <div class="form-group second-inline-input" id="holder-points_to">
                        <label for="points_to" class="control-label">
                            <?= LangHelper::get('points_to', 'Points to')?>
                            <span class="form-error" ng-show="errors.points_to">
                                    <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                </span>
                        </label>
                        <input type="text" class="form-control" ng-model="formData.points_to">
                    </div>
                </div>
            </div>

            <div class="submit-btn-inline">
                <a class="btn btn-default pull-right" ng-click="searchByPoints($event)"><?= LangHelper::get('search', 'Search')?></a>
            </div>
        </div>

        <hr class="form-separator">

        <div class="clearfix search-box">
            <div class="inline-inputs-holder">
                <div class="from-to-form gap-left0">
                    <div class="form-group" id="holder-birth_from">
                        <label for="birth_from" class="control-label">
                            <?= LangHelper::get('date_of_birth_from', 'Date of birth from')?>
                            <span class="form-error" ng-show="errors.birth_from">
                                <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                            </span>
                        </label>
                        <input type="text" name="birth_from" min-date="minDate"
                               datepicker-popup="dd.MM.yyyy" ng-required="true"
                               ng-model="formData.birth_from"
                               is-open="opened['birthFrom']" show-button-bar="false"
                               ng-click="openDatePicker($event, 'birthFrom')" formatdate
                               placeholder="dd.mm.yyyy"
                            class="form-control">
                    </div>
                </div>

                <div class="from-to-form">
                    <div class="form-group second-inline-input" id="holder-birth_to">
                        <label for="birth_to" class="control-label">
                            <?= LangHelper::get('date_of_birth_to', 'Date of birth to')?>
                            <span class="form-error" ng-show="errors.birth_to">
                                        <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                    </span>
                        </label>
                        <input type="text" name="birth_to" min-date="minDate"
                               datepicker-popup="dd.MM.yyyy" ng-required="true"
                               ng-model="formData.birth_to"
                               is-open="opened['birthTo']" show-button-bar="false"
                               ng-click="openDatePicker($event, 'birthTo')" formatdate
                               placeholder="dd.mm.yyyy"
                            class="form-control">
                    </div>
                </div>
            </div>

            <div class="submit-btn-inline">
                <a class="btn btn-default pull-right" ng-click="searchByDate($event)">
                    <?= LangHelper::get('search', 'Search')?>
                </a>
            </div>
        </div>

        <hr class="form-separator">

    </form>

    <div class="clearfix player-align table-content players pad-top40 gap-top30 col-lg-12">
        <table class="table bg-dark striped" ng-show="players.length > 0">
            <thead>
            <tr>
                <th class="text-left">
                    <?= LangHelper::get('name', 'Name') ?>
                </th>
                <th class="text-left">
                    <?= LangHelper::get('surname', 'Surname') ?>
                </th>
                <th class="text-left">
                    <?= LangHelper::get('licence_number', 'Licence number') ?>
                </th>
                <th class="text-left">
                    <?= LangHelper::get('city', 'City') ?>
                </th>
                <th class="text-left full">
                    DOB
                </th>
                <th class="text-left last-col">
                    <?= LangHelper::get('options', 'Options') ?>
                </th>

            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="player in players track by $index">
                <td>{{ player.name }}</td>
                <td>{{ player.surname }}</td>
                <td>{{ player.licence_number }}</td>
                <td>{{ player.city }}</td>
                <td>{{ player.date_of_birth | asDate | date:'dd.MM.yyyy' }}</td>
                <td class="text-left input-control transparent">
                    <a class="button primary info inline-block"
                       ng-href="<?= URL::to($tournament_slug . '/player/informations')?>/{{player.id}}">
                        <?= LangHelper::get('info', 'Info')?>
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
        <h3 class="fg-white text-center" ng-show="players.length < 1"><?= LangHelper::get('no_available_data', 'No available data')?></h3>
    </div>

</div>
