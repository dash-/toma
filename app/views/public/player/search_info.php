<div class="page-title">
    <h1><?= LangHelper::get('player_profile', 'Player Profile')?></h1>
</div>

<div class="clearfix">

    <div class="col-md-6">
        <p>
            <span><?= LangHelper::get('name', 'Name') ?>:</span>
            <?= $player->contact->name ?>
        </p>
        <p>
            <span class="color-rfet"><?= LangHelper::get('surname', 'Surname') ?>:</span>
            <?= $player->contact->surname ?>
        </p>
        <p>
            <span class="color-rfet"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>
                :</span> <?= $player->contact->date_of_birth ?>
        </p>
        <p>
            <span class="color-rfet"><?= LangHelper::get('city', 'City') ?>:</span>
            <?= $player->contact->city ?>
        </p>
        <p>
            <span class="color-rfet"><?= LangHelper::get('ranking', 'Ranking') ?>: </span>
            <?= $player->ranking ?>
        </p>
    </div>

</div>

