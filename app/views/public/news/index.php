<div class="page-title">
	<h1><?= LangHelper::get('news', 'News')?></h1>
</div>


<div ng-app="news" ng-controller='NewsController'>
	<?php foreach ($news as $article): ?>
		
		<div class="col-md-4">
			<div class="article-holder">
				<?php if ($article->images->count()): ?>
					<a href="<?= URL::to($tournament_slug.'/news/view', $article->id)?>">
						<img src="<?= $article->first_image('thumbnails')?>" alt="" class="pull-left">
					</a>
				<?php endif ?>
				
				<h3>
					<a href="<?= URL::to($tournament_slug.'/news/view', $article->id)?>">
						<?= $article->title?>
					</a>
				</h3>
				
				<p>
					<a href="<?= URL::to($tournament_slug.'/news/view', $article->id)?>">
					<?= Str::limit(HtmlHelper::strip($article->content), 350)?>
					</a>
				</p>
			</div>

		</div>

	<?php endforeach ?>
	

</div>