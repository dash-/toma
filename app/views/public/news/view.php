<div class="page-title">
	<h1><?= $article->title?></h1>
</div>


<div class="clearfix gap-top20">
	<?php if ($article->images->count()): ?>
		<div id="news-images-carousel" class="carousel slide col-md-5" data-ride="carousel">
		       
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">
		        <?php foreach ($article->images as $key => $image): ?>
		            <div class="item <?= ($key == 0) ? 'active' :  NULL?>">
		            	<a href="<?= $image->image('full')?>" class="fancybox" rel="group">
		                	<img src="<?= $image->image('full')?>" alt="">
		                </a>
		            </div>    
		        <?php endforeach ?>
		    </div>
		    


		    <!-- Controls -->
		    <a class="left carousel-control" href="#news-images-carousel" role="button" data-slide="prev">
		        <span class="i-arrow-left"></span>
		    </a>
		    <a class="right carousel-control" href="#news-images-carousel" role="button" data-slide="next">
		        <span class="i-arrow-right-2"></span>
		    </a>
		</div>
	<?php endif ?>
	
	<?= $article->content?>
	

	<div class="col-md-12" ng-app="news">
		
		<div class="comments-holder" ng-controller="CommentsController" ng-init="init('<?= URL::to($tournament_slug.'/news/comments/'.$article->id)?>')">
			<h3>
				Comments
			</h3>

            <?php if ($logged_user):?>
                <div class="comment-form-holder">
                    <form id="commentForm" class="clearfix" name="commentForm" ng-submit="commentSubmit('<?= $tournament_slug?>')" novalidate>
                        <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
                        <input type="hidden" ng-model="formData.table_id" ng-init="formData.table_id='<?= $article->id?>'">
                        <input type="hidden" ng-model="formData.table_name" ng-init="formData.table_name='<?= $article->getTable()?>'">
                        <div class="form-group" id="holder-text">
                            <textarea id="text" ng-model="formData.text" class="clearfix form-control" required rows="5"></textarea>
                        </div>
                        <button ng-disabled="!commentForm.$valid" class="btn btn-default pull-right" type="submit" id="send">
                            <?= LangHelper::get('comment', 'Comment')?>
                        </button>
                    </form>
                </div>
            <?php endif ?>

			<div id="all-comments">
				<?= View::make('public/news/_comment')?>
			</div>
		</div>

	</div>	

</div>