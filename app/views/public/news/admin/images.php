<div class="page-title">
	<h1>
		<?= LangHelper::get('news_administration', 'News administration')?> - <?= LangHelper::get('article_images', 'article images')?>
	</h1>

	<a class="pull-right btn-sm btn-default gap-top20 btn" href="<?= URL::to($tournament_slug.'/news-admin')?>">
		Back
	</a>
</div>

<div class="col-md-12 row gap-bottom20">
	<h3>
		<?= LangHelper::get('article', 'Article')?>: <?= $article->title?>
	</h3>
</div>

<div class="clear" ng-app="news" ng-controller='NewsAdminController' ng-cloak>
	
	<div class="well well-small">
		
		<form name="uploadForm" id="redirectAfterUpload" method="post" class="dropzone" redirect="true"
                  action="<?= Request::url() ?>" enctype="multipart/form-data" autocomplete="off">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

			<div class="form-group" id="holder-image">
				<label for="image" class="control-label">
					<?= LangHelper::get('image', 'Image')?>
					<span class="form-error"></span>
				</label>
				<input type="file" class="file" name="image" multiple>
			</div>
			<button type="submit" class="clear btn btn-lg btn-default">
                <?= LangHelper::get('upload', 'Upload')?>
            </button>
        </form>

	</div>


	<?php foreach ($article->images->all() as $image): ?>
		
		<div class="col-md-4" id="article_<?= $image->id?>">
			<div class="asset-holder clearfix">
				<a class="btn btn-danger btn-xs delete_asset" 
					ng-click="remove($event, '<?= URL::to($tournament_slug.'/news-admin/image', $image->id)?>')">
					Delete
				</a>
				<img class="img-thumbnail" src="<?= $image->image('thumbnails')?>" alt="">
			</div>
		</div>

	<?php endforeach ?>

</div>