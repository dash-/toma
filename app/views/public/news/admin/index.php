<div class="page-title">
    <h1><?= LangHelper::get('news_administration', 'News administration') ?></h1>

    <a class="pull-right btn-sm btn-default gap-top20 btn"
       href="<?= URL::to($tournament_slug . '/news-admin/create') ?>">
        Add new article
    </a>
</div>


<div ng-app="news" ng-controller='NewsAdminController' ng-cloak>

    <table class="table table-striped">
        <thead>
        <tr>
            <th class="text-left">Date</th>
            <th class="text-left">Title</th>
            <th class="text-left">Content</th>
            <th class="text-left">Created by</th>
            <th class="text-left">Status</th>
            <th class="text-right">Options</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tournament->articles as $article): ?>
            <tr id="article_<?= $article->id ?>">
                <td class="text-left">
                    <?= DateTimeHelper::GetFullDateTime($article->created_at) ?>
                </td>
                <td class="text-left"><?= $article->title ?></td>
                <td class="text-left"><?= Str::limit(HtmlHelper::strip($article->content), 60) ?></td>
                <td class="text-left"><?= $article->creator() ?></td>
                <td class="text-left">
						<span ng-bind="article.status[<?= $article->id ?>]"
                              ng-init="article.status[<?= $article->id ?>]='<?= $article->status() ?>'"></span>
                </td>
                <td class="text-right">

                    <a class="btn btn-xs btn-default"
                       href="<?= URL::to($tournament_slug . '/news-admin/edit', $article->id) ?>">Edit</a>

                    <a class="btn btn-xs btn-default"
                       ng-click='publishAction("<?= URL::to($tournament_slug . '/news-admin/publish', $article->id) ?>")'>
                        <span ng-bind="article.state[<?= $article->id ?>]"
                              ng-init="article.state[<?= $article->id ?>]='<?= ($article->active == 1) ? 'Unpublish' : 'Publish' ?>'"></span>
                    </a>

                    <a class="btn btn-xs btn-info"
                       href="<?= URL::to($tournament_slug . '/news-admin/images', $article->id) ?>">Images</a>

                    <a class="btn btn-xs btn-danger"
                       ng-click="remove($event, '<?= URL::to($tournament_slug . '/news-admin/article', $article->id) ?>')">Delete</a>

                </td>
            </tr>

        <?php endforeach ?>

        </tbody>
    </table>


</div>