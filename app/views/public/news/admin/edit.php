<div class="page-title">
	<h1>
        <?= LangHelper::get('news_administration', 'News administration')?> - <?= LangHelper::get('edit_article', 'edit article')?>
    </h1>

	<a class="pull-right btn-sm btn-default gap-top20 btn" href="<?= URL::to($tournament_slug.'/news-admin')?>">
		Back
	</a>
</div>


<div ng-app="news" ng-controller='NewsAdminController' ng-cloak>
	
	<form name="editForm" ng-submit="formSubmit($event, 'POST')" novalidate>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		
		<div class="form-group" id="holder-title">
			<label for="title" class="control-label">
				<?= LangHelper::get('title', 'Title')?>
				<span class="form-error"></span>
			</label>
			<input type="text" name="title" class="form-control" 
				ng-model="formData.article.title" 
				ng-init="formData.article.title='<?= $article->title?>'" required>
		</div>
		
		<div class="form-group" id="holder-content">
			<label for="content" class="control-label">
				<?= LangHelper::get('text', 'Text')?>
				<span class="form-error"></span>
			</label>
			<textarea name="content" 
				ng-model="formData.article.content" class="form-control tinymce" 
				ng-init='formData.article.content="<?= HTML::entities(HtmlHelper::strip($article->content)) ?>"'
				cols="30" rows="10" ui-tinymce="tinymceOptions" id="tinymce1" required></textarea>
		</div>

		<button type="submit" class="btn btn-success btn-lg clear"><?= LangHelper::get('save', 'Save')?></button>

   	</form>
               


</div>