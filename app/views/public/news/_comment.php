<div ng-repeat="comment in comments track by $index">
    <div class="comment" id="comment_{{comment.id}}">  <!-- Single comment start -->
        <div class="comment-head">
            
            <a class="commentator primary-link" href="/users/1">
                {{comment.username}}
                <span class="date">{{comment.created_at}}</span>
            </a>
            
            <a class="pull-right delete-action" ng-if="comment.remove_allow" 
                ng-click="remove($event, $index, '<?= LangHelper::get('delete_comment', 'Delete comment?')?>')" 
                href="/{{tournament_slug}}/news/comment/{{comment.id}}"><?= LangHelper::get('delete', 'Delete')?></a>
        </div>

        <div class="comment-body">
            <p>
                {{comment.text}}
            </p>
        </div>
    </div> <!-- comment end -->
</div>

