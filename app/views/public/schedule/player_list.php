<div class="page-title">
    <h1 class="pull-left"><?= LangHelper::get('order_of_play', 'Order of play') ?>
        - <?= LangHelper::get('view_per_player', 'View per player') ?></h1>
    <a class="btn btn-sm btn-default gap-top25 pull-right" href="<?= URL::to($tournament_slug.'/schedule')?>">
        <?= LangHelper::get('order_of_play', 'Order of play')?>
    </a>
       <a class="btn btn-sm btn-default gap-top25 pull-right printOrderPublic" style="margin-right:20px" href="<?= URL::to($tournament_slug.'/schedule')?>">
        <?= LangHelper::get('print_order_of_play', 'Print order of play')?>
    </a>
</div>
<input id="sear" placeholder="<?= LangHelper::get('search', 'Search') ?>">
<div class="clearfix gap-top20">

    <div class="list-of-courts">
        <?php if (count($matches)): ?>

            <?php foreach ($matches as $player_name => $match): ?>
                <div class="schedule-player-holder">
                    <h3><?= (string)$player_name ?></h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th class="text-left" width="40%">
                                <?= LangHelper::get('match', 'Match') ?>
                            </th>
                            <th class="text-left" width="20%">
                                <?= LangHelper::get('date', 'Date') ?>
                            </th>
                            <th class="text-left" width="20%">
                                <?= LangHelper::get('time', 'Time') ?>
                            </th>
                            <th class="text-left" width="10%">
                                <?= LangHelper::get('court_number', 'Court number') ?>

                            </th>
                            <th class="text-left" width="10%">
                                <?= LangHelper::get('order_number', 'Order number') ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($match as $schedule): ?>
                            <tr>
                                <td><?= $schedule['teams'] ?></td>
                                <td><?= $schedule['date'] ?></td>
                                <td><?= $schedule['time'] ?></td>
                                <td><?= $schedule['court_number'] ?></td>
                                <td><?= $schedule['order_number'] ?></td>
                            </tr>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            <?php endforeach ?>

        <?php else: ?>
            <h3 class="pad-top20 gap-top20">
                <?= LangHelper::get('there_are_no_scheduled_matches', 'There are no scheduled matches') ?>
            </h3>
        <?php endif; ?>

    </div>


</div>

<script>
      $(".printOrderPublic").on("click", function () {
         window.print();     
     });
</script>

<script type="text/javascript">
    $(function () {
        $("select").select2({width: '140px'});

        $("#sear").searchFilter({targetSelector: ".schedule-player-holder", charCount: 2})
    });
</script>