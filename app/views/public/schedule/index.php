<div class="page-title">
    <h1 class="pull-left"><?= LangHelper::get('order_of_play', 'Order of play') ?></h1>
    <a class="btn btn-sm btn-default gap-top25 pull-right" href="<?= URL::to($tournament_slug.'/schedule/print-list')?>">
        <?= LangHelper::get('list_per_player', 'List per player')?>
    </a>
</div>

<div class="clearfix gap-top20">

    <div class="orderOfplay gap0">
        <?php $counter = 0; ?>
        <?php foreach ($dates_array as $key => $dates): ?>
            <div class="button date_select
                    <?= ($key == Request::query('date') OR (!Request::query('date') AND $counter == 0)) ? 'selected' : '' ?>"
                 data-id="<?= $key ?>">
                <a href="<?= URL::to($tournament_slug . '/schedule?date=' . $key) ?>">
                    <div class="timeTextD"><?= date("j", strtotime($key)); ?></div>
                    <div class="timeTextM"><?= date("M", strtotime($key)); ?></div>
                    <div class="timeTextY"><?= date("Y", strtotime($key)); ?></div>
                </a>
            </div>
            <?php $counter++ ?>
        <?php endforeach ?>
    </div>

    <div class="list-of-courts">
        <?php if (count($schedules)): ?>
            <?php if ($configuration->waiting_list): ?>
                <?= View::make('public/schedule/waiting_list', [
                    'schedules'   => $schedules,
                    'all_matches' => $all_matches,
                ]) ?>
            <?php else: ?>

                <?php for ($j = 1; $j <= $number_of_rows; $j++): ?>
                    <div class="row schedule-row">
                        <?php for ($i = 0; $i < $configuration->number_of_courts; $i++): ?>
                            <?php $schedule = DrawMatchHelper::getScheduledMatch($schedules, $j, $i + 1) ?>
                            <div class="clearfix court-column droppable court-drop preview-box box-size-public">
                                <p class="schedule-show-court-title"><?= LangHelper::get('court', 'Court') ?>
                                    : <?= $i + 1 ?></p>
                                <?php if ($schedule): ?>
                                    <p class="schedule-showtime">
                                        <?= $schedule->type_of_time() ?>
                                        <?php if ($schedule->type_of_time != 3): ?>
                                            :<?= $schedule->getTime() ?>
                                        <?php endif ?>
                                    </p>
                                    <div class="draggable team">
                                        <?= DrawMatchHelper::scheduleName($schedule->match, 'team1_data', $all_matches) ?>
                                        <div class="public-order-of-play"><?= TournamentScoresHelper::getBracketScoresTeam1($schedule->match_id)?></div>
                                        <span class="small">vs</span>
                                        <div class="public-order-of-play"><?= TournamentScoresHelper::getBracketScoresTeam2($schedule->match_id) ?></div>
                                        <?= DrawMatchHelper::scheduleName($schedule->match, 'team2_data', $all_matches, true) ?>
                                    </div>
                                    <div class="match-status-public"><?= $schedule->status()?></div>
                                <?php endif ?>
                            </div>
                        <?php endfor ?>
                    </div>
                <?php endfor ?>

            <?php endif; ?>

        <?php else: ?>
            <h3 class="pad-top20 gap-top20">
                <?= LangHelper::get('there_are_no_scheduled_matches_for_this_day',
                    'There are no scheduled matches for this day') ?>
            </h3>
        <?php endif; ?>

    </div>


</div>