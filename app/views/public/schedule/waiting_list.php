<div class="row gap-top20">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>
                <?= LangHelper::get('match', 'Match') ?>
            </th>
            <th>
                <?= LangHelper::get('time', 'Time') ?>
            </th>
            <th>
                <?= LangHelper::get('court_number', 'Court number') ?>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($schedules as $schedule): ?>
            <tr>
                <td>
                    <?= DrawMatchHelper::scheduleName($schedule->match, 'team1_data', $all_matches) ?>
                    <span class="small">vs</span>
                    <?= DrawMatchHelper::scheduleName($schedule->match, 'team2_data', $all_matches, true) ?>
                </td>
                <td>
                    <?= $schedule->type_of_time(true) ?>
                    <?= ($schedule->waiting_list_type_of_time != 3) ? ": " . $schedule->getTime() : null ?>
                </td>
                <td>
                    <?= $schedule->waiting_list_court_number ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>