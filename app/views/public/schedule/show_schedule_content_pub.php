
<div class="orderOfplay">
        <?php foreach($dates as $key => $date):  ?>
              <a href=""><div class="button date_select" data-id="<?= $key ?>">
              <div class="timeTextD"><?= date("j", strtotime($key)); ?></div>
                <div class="timeTextM"><?= date("M", strtotime($key)); ?></div>
                <div class="timeTextY"><?= date("Y", strtotime($key)); ?></div>
            </div></a>
        <?php endforeach ?>
    </div>

<div id="content-loader" style="visibility:hidden">

<?php foreach ($dates as $key => $date): ?>

    <?php if (DrawMatchHelper::checkScheduleDataByDates($key, $tournament->id) > 0): ?>
               
        <h3 class="text-center"><?= LangHelper::get('order_of_play', 'ORDER OF PLAY')?>: <?= $date?></h3>
        <div class="row clearfix">

   <?php for ($i=0; $i < $tournament->number_of_courts; $i++):?>

        <div class="clearfix court-column">
            <p class="courts-view courtnum">Court <?= $i+1?></p>
                <?php for ($j=0; $j < $default_counter/2; $j++):?>
                    <?php
                        $match_data = DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type,  false);
                    ?>
                    <div class="match<?= ($match_data) ? $match_data->id : null?>">
                        <div class="row court-match <?= !DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type) ? 'hideTile bg-black' : null ?>">

                            <?php if ( DrawMatchHelper::getScheduleData($i, $j, $tournament->id, $key, $list_type) ): ?>
                                <p class="courts-view text-center gap-bottom20 pad-bottom20">
                                    <?= $match_data->time_of_play?>
                                </p>

                                <?php if (!is_null($match_data->match->team1_data)): ?>



                                    <p class="courts-view text-center schedule-team-name">
                                        <?= $match_data->match->team1_data->name.' '.$match_data->match->team1_data->surname ?>
                                    </p>

                                    <div class="scoreholder rm-float center-score">
                                        <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score1']?>
                                    </div>
                                <?php else: ?>
                                    <div class="scoreholder rm-float center-score"></div>
                                    <p class="courts-view text-center schedule-team-name">
                                        BYE
                                    </p>
                                <?php endif ?>

                                <p class="courts-view text-center">
                                    vs
                                </p>

                                <?php if (!is_null($match_data->match->team2_data)): ?>


                                    <div class="scoreholder rm-float center-score">
                                        <?= DrawMatchHelper::createScoreHtml($match_data->match->scores)['score2']?>
                                    </div>

                                    <p class="courts-view text-center schedule-team-name">
                                        <?= $match_data->match->team2_data->name.' '.$match_data->match->team2_data->surname ?>
                                    </p>
                                <?php else: ?>
                                    <div class="scoreholder rm-float center-score"></div>
                                    <p class="courts-view text-center schedule-team-name">
                                        BYE
                                    </p>
                                <?php endif ?>



                                <p class="<?= DrawMatchHelper::getNotStarted($match_data)?>" style="<?= DrawMatchHelper::getMatchStatus($match_data)?>; margin-bottom: 0px;">
                                    <?= $match_data->status()?>
                                </p>

                            <?php else: ?>
                               
                                <p></p>

                            <?php endif ?>
                        </div>
                    </div>

                <?php endfor;?>

            </div>

        <?php endfor;?>

                </div>
          
        <?php endif ?>

    <?php endforeach ?>

      </div>

<script>

$(document).ready( function () {

    $("body").on("click", ".date_select", function(e){
            var date_sel = this;
            $(".button.date_select").css('background-color', '#009578');
            $("#content-loader").css('visibility', 'visible');

            e.preventDefault();
            var data = $(this).data("id");
            if(data!=''){
                $.ajax({
                    url: '/p/schedule/show-schedule/<?= $tournament->id?>?list_type=<?= $list_type?>&ajax=true&date='+data,
                    dataType: "HTML",
                    success: function(data)
                    {
                        $('#content-loader').html(data);
                    }
                })
                $(date_sel).css('background-color', '#FFC627');
            }
    });
});

</script>

