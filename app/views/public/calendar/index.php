<div class="container ang">
    <div class="grid">
        <div class="row" ng-controller="CalendarController" data-ng-init="init()">
            <div class="row">
                <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
            </div>
            <div class="row">
                <div class="clearfix">
                    <div class="empty-space"></div>
                </div>
            </div>
            <div class="tournament-first" style="float:left; width: 20%">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('event_calendar', 'Event calendar') ?></h3>
                    <span class="line"></span>
                </div>

                <div class="clearfix filter-box place-left">
                    <form ng-submit="filter($event)" name="filterForm">
                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('club', $clubs_array, NULL,
                                array('ui-select2'=>"select2Options",
                                    'ng-model' => 'tournament.club_id', 'ng-class' => 'no-search-box'));?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('city', $cities_array, NULL,
                                array('ui-select2'=>"select2Options",
                                    'ng-model' => 'tournament.club_city', 'ng-class' => 'no-search-box'));?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('category', $categories, NULL,
                                array('ui-select2'=>"select2Options",
                                    'ng-model' => 'tournament.category', 'ng-class' => 'no-search-box'));?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <button type="submit" ng-click="search($event)"
                                    ng-class="scSearch == 1 ? 'half-button' : ''"
                                    class="primary full"><?= LangHelper::get('filter', 'Filter') ?></button>

                            <a ng-show="scSearch == 1"
                               class="button primary half-button reset-button"
                               href="<?= URL::current()?>"><?= LangHelper::get('reset_search', 'Reset search') ?></a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="clearfix gap-top20">
                <a class="btn btn-default" style="float:right" href="<?= URL::current().'?list_view=1'?>">
                    <?= LangHelper::get('show_list_view', 'Show list view')?>
                </a>
            </div>

            <div class="calendar-holder">

                <div ui-calendar="uiConfig.calendar" id="calendar" ng-model="eventSources"></div>

                <div style="margin-top: 50px">
                    <button ng-click="prev()" class="button place-left addLeftPad back-yellow">
                        <?= LangHelper::get('previous_month', 'Previous month') ?>
                    </button>
                    <button ng-click="next()" class="button place-right primary big dark">
                        <?= LangHelper::get('next_month', 'Next month') ?>
                    </button>
                </div>
            </div>

        </div>
    </div>

</div>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['calendarApp']);
    });
    $(function () {
        $("select").select2({});
        setTimeout(function () {
            $('.fc-button-today').trigger("click");
        }, 10);
    })
</script>