<div class="container ang">
    <div class="grid">
        <div class="row" ng-controller="ListController">
            <div class="row">
                <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
            </div>
            <div class="row">
                <div class="clearfix">
                    <div class="empty-space"></div>
                </div>
            </div>
            <div class="tournament-first" style="float:left; width: 20%">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('event_calendar', 'Event calendar') ?></h3>
                    <span class="line"></span>
                </div>

                <div class="clearfix">
                    <form ng-submit="filter($event)" name="filterForm">
                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('club', $clubs_array, NULL,
                                array('ui-select2' => "select2Options",
                                    'ng-model' => 'tournament.club_id', 'ng-class' => 'no-search-box')); ?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('city', $cities_array, NULL,
                                array('ui-select2' => "select2Options",
                                    'ng-model' => 'tournament.club_city', 'ng-class' => 'no-search-box')); ?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <?= Form::select('category', $categories, NULL,
                                array('ui-select2' => "select2Options",
                                    'ng-model' => 'tournament.category', 'ng-class' => 'no-search-box')); ?>
                        </div>

                        <div class="clearfix filter calendar-dropdown">
                            <button type="submit" ng-click="search($event)"
                                    ng-class="scSearch == 1 ? 'half-button' : ''"
                                    class="primary full"><?= LangHelper::get('filter', 'Filter') ?></button>

                            <a ng-show="scSearch == 1"
                               class="button primary half-button reset-button"
                               href="<?= URL::current() . '?list_view=1' ?>"><?= LangHelper::get('reset_search', 'Reset search') ?></a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="clearfix gap-top20 calendar-dropdown" style="float:right">
                <a class="btn btn-default" href="<?= URL::current() ?>">
                    <?= LangHelper::get('show_calendar_view', 'Show calendar view') ?>
                </a>
            </div>

            <div class="calendar-holder pad-top20" style="width:75%">
                <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll"
                       style="width:100% !important">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                        <th class="text-left"><?= LangHelper::get('start_date', 'Start date') ?></th>
                        <th class="text-left"><?= LangHelper::get('end_date', 'End date') ?></th>
                        <th class="text-right"><?= LangHelper::get('details', 'Details') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="tournament in tournaments track by $index">
                        <td>{{tournament.title}}</td>
                        <td>{{tournament.start}}</td>
                        <td>{{tournament.end}}</td>
                        <td class="transparent text-right">
                            <a class="rfet-button rfet-yellow" target="_blank" href="{{tournament.url_to}}">
                                <?= LangHelper::get('details', 'Details') ?>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['calendarApp']);
    });
    $(function () {
        $("select").select2({});
    })
</script>