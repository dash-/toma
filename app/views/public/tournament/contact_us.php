<div class="page-title">
    <h1><?= LangHelper::get('contact_us', 'Contact us')?></h1>
</div>


<div ng-app="tournament" ng-controller='ContactController' ng-cloak>
    <div class="col-md-7 pad-left0">
        <div class="alert alert-success alert-dismissable" ng-show="alertShow">
          <a class="close" href="<?= URL::to($tournament_slug.'/tournament')?>" data-dismiss="alert" aria-hidden="true">&times;</a>
          <strong>Success!</strong> You have successfully contacted us. We will get back to you shortly!
        </div>


        <form name="contactForm" ng-submit="formSubmit($event, 'POST', '<?= URL::current()?>')" novalidate ng-show="!alertShow">
            <fieldset ng-form>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                
                <div class="form-group" id="holder-name">
                    <label for="name" class="control-label">
                        <?= LangHelper::get('name', 'Name')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="text" name="name" class="form-control" ng-model="formData.name" required>
                </div>

                <div class="form-group" id="holder-email">
                    <label for="email" class="control-label">
                        <?= LangHelper::get('email', 'Email')?>
                        <span class="form-error" ng-show="contactForm.email.$error.email">
                            Not valid email!
                        </span>
                    </label>
                    <input type="email" name="email" class="form-control" ng-model="formData.email" required>
                </div>
                
                <div class="form-group" id="holder-subject">
                    <label for="subject" class="control-label">
                        <?= LangHelper::get('subject', 'Subject')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="text" name="subject" class="form-control" ng-model="formData.subject">
                </div>

                <div class="form-group" id="holder-msg">
                    <label for="msg" class="control-label">
                        <?= LangHelper::get('message', 'Message')?>
                        <span class="form-error"></span>
                    </label>
                    <textarea name="msg" ng-model="formData.msg" class="form-control" cols="30" rows="10" required></textarea>
                </div>

                <button type="submit" ng-disabled="contactForm.$invalid" class="btn btn-success btn-lg clear"><?= LangHelper::get('send', 'Send')?></button>
            </fieldset>

        </form>

    </div>
</div>
