<div class="page-title">
	<h1><?= LangHelper::get('tournament_details', 'Tournament details')?></h1>
</div>


<div class="row col-md-12">
	
    <div class="col-md-4 pad-left0">
        <h3 class="details-header"><?= LangHelper::get('organizer', 'ORGANIZER')?></h3>
		<div class="clear">
			<p class="detail-label"><?= $tournament->organizer->name?></p>
			<p class="detail-label"><?= LangHelper::get('email', 'Email')?>: <?= $tournament->organizer->email?></p>
			<p class="detail-label"><?= LangHelper::get('phone', 'Phone')?>: <?= $tournament->organizer->phone?></p>
			<p class="detail-label"><?= LangHelper::get('fax', 'Fax')?>: <?= $tournament->organizer->fax?></p>
			<p class="detail-label"><?= LangHelper::get('website', 'Website')?>: <?= $tournament->organizer->website?></p>
		</div>
    </div>

    <div class="col-md-8">
        <h3 class="details-header"><?= LangHelper::get('tournament_information', 'Tournament information')?></h3>
        <div class="row clear">
            <div class="col-md-6">
                <p class="detail-label">
                	<?= LangHelper::get('title', 'Title')?>: <?= $tournament->title?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('region', 'Region')?>: <?= $tournament->club->province->region->region_name?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('surface', 'Surface')?>: <?= ($tournament->surface) ? $tournament->surface->name : 'N/A'?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('club', 'Club')?>: <?= $tournament->club->club_name?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('main_draw_start', 'Main draw start')?>: <?= $tournament->date->getDate('main_draw_from')?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('main_draw_finish', 'Main draw finish')?>: <?= $tournament->date->getDate('main_draw_to')?>
                </p>
            </div>
               
            <div class="col-md-6">
                <p class="detail-label">
                	<?= LangHelper::get('qualifying_date_from', 'Qualifying date from')?>: <?= $tournament->date->getDate('qualifying_date_from')?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('qualifying_date_to', 'Qualifying date to')?>: <?= $tournament->date->getDate('qualifying_date_to')?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('entry_deadline', 'Entry deadline')?>: <?= $tournament->date->getDate('entry_deadline')?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('withdrawal_deadline', 'Withdrawal deadline')?>: <?= $tournament->date->getDate('withdrawal_deadline')?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('submitted_by', 'Submitted by')?>: <?= $tournament->creator()->username?>
                </p>
                <p class="detail-label">
                	<?= LangHelper::get('number_of_courts', 'Number of courts')?>: <?= $tournament->number_of_courts?>
                </p>		
            </div>   
        </div>      
    </div>

</div>

<div class="row col-md-12">
	
	<h3 class="details-header"><?= LangHelper::get('draws_information', 'Draws information')?></h3> 
	<div class="clear draws-info-holder">
		<?php foreach ($tournament->draws as $key => $draw): ?>
			<?php if ($key % 3 == 0): ?>
				<div class="col-md-3 pad-left0">
					<p><?= LangHelper::get('category', 'Category')?>:</p>
		            <p><?= LangHelper::get('type', 'Type')?>:</p>
		            <p><?= LangHelper::get('gender', 'Gender')?>:</p>
		            <p><?= LangHelper::get('total_acceptances', 'Total acceptances')?>:</p>
		            <p><?= LangHelper::get('total_number_of_seeds', 'Total number of seeds')?>:</p>
		            <p><?= LangHelper::get('direct_acceptances_from', 'Direct acceptances')?>:</p>
		            <p><?= LangHelper::get('accepted_qualifiers', 'Qualifiers')?>:</p>
		            <p><?= LangHelper::get('wild_cards', 'Wild cards')?>:</p>
		            <p><?= LangHelper::get('special_exempts', 'Special exempts')?>:</p>
		            <p><?= LangHelper::get('onsite_direct', 'Onsite direct')?>:</p>
		            <p><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size')?>:</p>
		            <p><?= LangHelper::get('prize_pool', 'Prize pool')?>:</p>
	           	</div>

	           	<div class="col-md-3">
	           		<p><?= $draw->category->name?></p>
                    <p><?= $draw->typeName()?></p>
                    <p><?= $draw->genderName()?></p>
                    <p><?= $draw->size->total?></p>
                    <p><?= $draw->number_of_seeds?></p>
                    <p><?= $draw->size->direct_acceptances?></p>
                    <p><?= $draw->size->accepted_qualifiers?></p>
                    <p><?= $draw->size->wild_cards?></p>
                    <p><?= $draw->size->special_exempts?></p>
                    <p><?= $draw->size->onsite_direct?></p>
                    <p><?= $draw->qualification->draw_size?></p>
                    <p> &euro;<?= $draw->prize_pool?></p>
	           	</div>
			<?php elseif ($key % 3 == 1): ?>
				<div class="col-md-3">
	           		<p><?= $draw->category->name?></p>
                    <p><?= $draw->typeName()?></p>
                    <p><?= $draw->genderName()?></p>
                    <p><?= $draw->size->total?></p>
                    <p><?= $draw->number_of_seeds?></p>
                    <p><?= $draw->size->direct_acceptances?></p>
                    <p><?= $draw->size->accepted_qualifiers?></p>
                    <p><?= $draw->size->wild_cards?></p>
                    <p><?= $draw->size->special_exempts?></p>
                    <p><?= $draw->size->onsite_direct?></p>
                    <p><?= $draw->qualification->draw_size?></p>
                    <p> &euro;<?= $draw->prize_pool?></p>
	           	</div>
           <?php elseif ($key % 3 == 2): ?>
				<div class="col-md-3">
	           		<p><?= $draw->category->name?></p>
                    <p><?= $draw->typeName()?></p>
                    <p><?= $draw->genderName()?></p>
                    <p><?= $draw->size->total?></p>
                    <p><?= $draw->number_of_seeds?></p>
                    <p><?= $draw->size->direct_acceptances?></p>
                    <p><?= $draw->size->accepted_qualifiers?></p>
                    <p><?= $draw->size->wild_cards?></p>
                    <p><?= $draw->size->special_exempts?></p>
                    <p><?= $draw->size->onsite_direct?></p>
                    <p><?= $draw->qualification->draw_size?></p>
                    <p> &euro;<?= $draw->prize_pool?></p>
	           	</div>
			<?php endif ?>

		<?php endforeach ?>
	</div>
</div>