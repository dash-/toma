<div class="page-title">
    <h1><?= $draw->category->name?>: <?= LangHelper::get('register', 'Register')?></h1>
</div>

<div class="clearfix">
    <div class="draw-holder col-md-6 pull-right">
        <div class="single-draw clearfix">
            <div class="col-md-6">
                    <p><span class="color-rfet"><?= LangHelper::get('category', 'Category')?>:</span> <?= $draw->category->name ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('type', 'Type')?>:</span> <?= $draw->typeName() ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('gender', 'Gender')?>:</span> <?= $draw->genderName() ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('total_acceptances', 'Total acceptances')?>:</span> <?= $draw->size->total ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances')?>:</span> <?= $draw->size->direct_acceptances ?></p>
                    <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                        <a class="btn btn-success pull-left" href="<?= URL::to($tournament_slug.'/draws/bracket/'.$draw->id.'/m')?>">View Main bracket</a>
                    <?php endif?>

            </div>
            <div class="col-md-6">
                <p><span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers')?>:</span> <?= $draw->size->accepted_qualifiers?></p>
                <p><span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards')?>: </span><?= $draw->size->wild_cards ?></p>
                <p><span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts')?>: </span><?= $draw->size->special_exempts ?></p>
                <p><span class="color-rfet"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances')?>:</span> <?= $draw->size->onsite_direct ?></p>
                <p><span class="color-rfet"><?= LangHelper::get('total_qualifiers', 'Total qualifiers')?>:</span> <?= $draw->size->onsite_direct ?></p>
                
                <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                    <a class="btn btn-info pull-left" href="<?= URL::to($tournament_slug.'/draws/bracket/'.$draw->id.'/q')?>">View Qualify bracket</a>
                <?php endif?>
            </div>
        </div>
    </div>
</div>

<div class="clearfix" ng-app="signups" ng-controller='SignupsController' 
    <?= ($logged_user && $logged_user->hasRole('player')) ? "ng-init=getPlayerData('".$tournament_slug."','". csrf_token() ."')" : null ?> 
    ng-cloak>
    <div class="row col-md-12" ng-if="message.length > 0">
        <p class="successful-msg">
            {{message}}
        </p>
    </div>

    <form ng-show="hideForm == false" name="registerForm" ng-submit="formSubmit($event)" 
        action="<?= URL::to($tournament_slug.'/signups/register/'.$draw->id)?>" novalidate>

        <fieldset class="col-sm-6">
            <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
            <div class="form-group" id="holder-name">
                <label for="name" class="control-label">
                    <?= LangHelper::get('name', 'Name')?> *
                    <span class="form-error"></span>
                </label>
                <input type="text" class="form-control" ng-model="formData.name" required>
            </div>

            <div class="form-group" id="holder-title">
                <label for="title" class="control-label">
                    <?= LangHelper::get('licence_number', 'Licence number')?>
                    <span class="form-error"></span>
                </label>
                <input type="text" class="form-control" ng-model="formData.licence_number">
            </div>

            <div class="form-group" id="holder-email">
                <label for="email" class="control-label">
                    <?= LangHelper::get('email', 'Email')?>
                    <span class="form-error"></span>
                </label>
                <input type="text" class="form-control" ng-model="formData.email">
            </div>
        </fieldset>

        <fieldset class="col-sm-6">
            <div class="form-group" id="holder-surname">
                <label for="surname" class="control-label">
                    <?= LangHelper::get('surname', 'Surname')?> *
                    <span class="form-error"></span>
                </label>
                <input type="text" class="form-control" ng-model="formData.surname" required>
            </div>

            <div class="form-group" id="holder-date_of_birth">
                <label for="date_of_birth" class="control-label">
                    <?= LangHelper::get('date_of_birth', 'Date of birth')?> *
                    <span class="form-error"></span>
                </label>
                <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date_of_birth" is-open="opened['date_of_birth']" show-button-bar="false" class="form-control" ng-click="openDatePicker($event, 'date_of_birth')" formatdate required>
            </div>

            <div class="form-group" id="holder-phone">
                <label for="phone" class="control-label">
                    <?= LangHelper::get('contact_phone_number', 'Contact phone number')?>
                    <span class="form-error"></span>
                </label>
                <input type="text" class="form-control" ng-model="formData.phone">
            </div>
        </fieldset>
        <div class="clear col-md-12">
            <button ng-disabled="!registerForm.$valid" type="submit" class="btn btn-success btn-lg clear">
                <?= LangHelper::get('save', 'Save')?>
            </button>
        </div>
        
    </form>

    <div class="row col-md-12" ng-if="msg.length > 0">
        <p class="signup-validation">{{msg}}</p>
    </div>

</div>