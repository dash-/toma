<div class="page-title">
	<h1><?= LangHelper::get('signups', 'Signups')?></h1>
</div>


<div class="clearfix" ng-app="signups" ng-controller='SignupsController' ng-cloak>
	
	<accordion>
		<?php foreach ($tournament->draws as $draw): ?>
			<accordion-group is-open="<?= $draw->id == $open_accordion ? "true" : "" ?>" class="<?= $draw->id ?>">
	            <accordion-heading>
	                <?= $draw->category->name.' - '.$draw->genderName().' - '.$draw->typeName()?> 
	                
	            </accordion-heading>
				
				<?php if ($logged_user): ?>
                    <?php if ($draw->status == 1): ?>
                        <?php if ($logged_user->isAdministrator()): ?>
                            <a class="pull-right btn-info btn-sm" href="<?= URL::to('/signups/register', $draw->id)?>">
                                Signup
                            </a>
                        <?php elseif ($logged_user->hasRole('player')): ?>
                            <!-- check if player already signed up -->
                            <?php if ($logged_user->checkIfPlayerAlreadySignedUp($draw->id)): ?>
                                <a class="pull-right btn-info btn-sm" href="javascript:return false">
                                    <?= LangHelper::get('already_signed_up_for_draw', 'You have already signed up for this draw') ?>
                                </a>
                            <?php endif ?>
    					<?php endif ?>
                    <?php endif ?>
				<?php endif ?>
				<div class="row pad-top20">
                    <?php if ($draw->status < 3): ?>
                        <?= View::make('public/signups/standard_list', [
                            'draw' => $draw,
                        ])?>

                   
                    <?php else: ?>
                        <?= View::make('public/signups/final_list', [
                            'draw' => $draw,
                        ])?>
                    <?php endif ?>
				</div>
            </accordion-group>
		<?php endforeach ?>
	<accordion>

</div>

<script>

    $(function(){
        if(document.location.hash !== undefined)
        {


        }
    })
</script>