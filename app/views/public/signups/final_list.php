<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-left"><a href="<?= \PageHelper::createSignupListLinkPublic("name", $tournament_slug, $draw->id) ?>">
                <?= LangHelper::get('name', 'Name')?></a>
        </th>
        <th class="text-left"><a href="<?= \PageHelper::createSignupListLinkPublic("surname", $tournament_slug, $draw->id) ?>">
                <?= LangHelper::get('surname', 'Surname') ?></a>
        </th>
        <th class="text-left"><a href="<?= \PageHelper::createSignupListLinkPublic("licence_number", $tournament_slug, $draw->id) ?>"><?= LangHelper::get('licence_number', 'Licence number')?></a></th>
        <th class="text-left"><a href="<?= \PageHelper::createSignupListLinkPublic("date_of_birth", $tournament_slug, $draw->id) ?>"><?= LangHelper::get('date_of_birth', 'Date of birth')?></a></th>
        <th class="text-left"><a href="<?= \PageHelper::createSignupListLinkPublic("ranking", $tournament_slug, $draw->id) ?>"><?= LangHelper::get('ranking', 'Ranking')?></a></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (TournamentDrawHelper::sortSignupsByTeams($draw->signups) as $player): ?>
        <tr>
            <td><?= TournamentHelper::getPlayerValue($player, 'name')?></td>
            <td><?= TournamentHelper::getPlayerValue($player, 'surname')?></td>
            <td><?= TournamentHelper::getPlayerValue($player, 'licence_number')?></td>
            <td><?= TournamentHelper::getPlayerValue($player, 'date_of_birth')?></td>
            <td><?= $player['rank']?></td>
            <td style="color:<?= $player->list_color()?>">
                <?= $player->list_type()?>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>