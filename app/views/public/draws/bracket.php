<div class="page-title">
	<h1><?= $title?></h1>
</div>


<div ng-app="draws" ng-controller='DrawsController'>
	
	<div class="row">
        <div class="pull-left">
            <p class="yellow-color"><?= LangHelper::get('draw_level', 'Draw level')?>: <?= $draw->level ?> estrellas</p>
            <p class="yellow-color"><?= LangHelper::get('draw_category', 'Draw category')?>: <?= $draw->category->name?></p>
        </div>

        <div class="pull-right sponsor-holder">
            <?php if ($draw->tournament->getSponsor('id')): ?>
                <img class="basic rm-width-club-image" src="<?= $draw->tournament->getSponsorImage() ?>">
                <p><?= $draw->tournament->getSponsor('title') ?></p>
            <?php endif ?>
        </div>
        <?php if ($draw->tournament->getRegionImage()):?>
            <div class="pull-right sponsor-holder">
                <img class="basic rm-width-club-image" src="<?= $draw->tournament->getRegionImage() ?>">
            </div>
        <?php endif?>
    </div>

    <div class="row gracket-holder">
        <div id="gracket" data-gracket='<?= $jsArray?>'></div>
    </div>

    <div class="row pad-bottom20">
        <div class="gracket-box" style="width: 20%">
            <h3><?= LangHelper::get('tournament_info', 'Tournament info') ?></h3>
            <?php if ($tournament->player_representative):?>
                <p class="fg-white">
                    <?= LangHelper::get('player_representative', 'Player representative')?>:
                    <br>
                    <?= $tournament->player_representative?>
                </p>
            <?php endif?>
            <?php if ($tournament->official_ball): ?>
                <p class="fg-white">
                    <?= LangHelper::get('official_ball', 'Official ball')?>: <?= $tournament->official_ball?>
                </p>
            <?php endif?>

            <p class="fg-white">
                <?= LangHelper::get('referee', 'Referee')?>: <?= $referee_name?>
            </p>
        </div>
        <div class="gracket-box pull-right">
            <h3><?= $tournament->club->club_name ?></h3>
            <?php if ($tournament->club->image_link): ?>
                <img class="club-image" src="<?= $tournament->club->image() ?>">
            <?php endif ?>
        </div>

    </div>
    
</div>

<script src="/js/jquery.gracket.js"></script>

<script>
    
$(document).ready(function() {
    $("#gracket").gracket({
        canvasLineWidth : 1,      
        cornerRadius : 0,         
        canvasLineCap : "square",  
        canvasLineColor : "#30373c", 
        isDoubles: <?= $isDoubles ? "true" : "false" ?>
    });
    
    $(".g_gracket").width($(".g_winner").width()+$(".g_winner").position().left+70);
    
});
</script>