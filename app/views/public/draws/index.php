<div class="page-title">
	<h1><?= LangHelper::get('draws', 'Draws')?></h1>
</div>


<div class="clearfix gap-top20" ng-app="draws" ng-controller='DrawsController'>
	<?php foreach ($tournament->draws as $draw): ?>
		
        <!-- reminder - remove -->
        <?php TournamentDrawHelper::drawStatus($draw, $draws_with_matches_list, $tournament->date->entry_deadline, $draw_history)?>
        
		<div class="draw-holder col-md-6">
			<div class="single-draw clearfix">
				<div class="col-md-6">
	                    <p><span class="color-rfet"><?= LangHelper::get('category', 'Category')?>:</span> <?= $draw->category->name ?></p>
	                    <p><span class="color-rfet"><?= LangHelper::get('type', 'Type')?>:</span> <?= $draw->typeName() ?></p>
	                    <p><span class="color-rfet"><?= LangHelper::get('gender', 'Gender')?>:</span> <?= $draw->genderName() ?></p>
	                    <p><span class="color-rfet"><?= LangHelper::get('total_acceptances', 'Total acceptances')?>:</span> <?= $draw->size->total ?></p>
	                    <p><span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances')?>:</span> <?= $draw->size->direct_acceptances ?></p>
	                    <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
	                    	<a class="btn btn-success pull-left" href="<?= URL::to($tournament_slug.'/draws/bracket/'.$draw->id.'/m')?>">View Main bracket</a>
	                    <?php endif?>

                </div>
                <div class="col-md-6">
                    <p><span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers')?>:</span> <?= $draw->size->accepted_qualifiers?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards')?>: </span><?= $draw->size->wild_cards ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts')?>: </span><?= $draw->size->special_exempts ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances')?>:</span> <?= $draw->size->onsite_direct ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('total_qualifiers', 'Total qualifiers')?>:</span> <?= $draw->size->onsite_direct ?></p>
					
					<?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                    	<a class="btn btn-info pull-left" href="<?= URL::to($tournament_slug.'/draws/bracket/'.$draw->id.'/q')?>">View Qualify bracket</a>
                    <?php endif?>
                </div>
            </div>
        </div>
	<?php endforeach ?>
</div>