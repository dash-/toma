<div class="page-title">
    <h1><?= LangHelper::get('message_box_administration', 'Message box administration')?></h1>

    <a class="pull-right btn-sm btn-default gap-top20 btn" href="<?= URL::to($tournament_slug.'/message-box/create')?>">
        <?= LangHelper::get('add_new_message', 'Add new message')?>
    </a>
</div>


<div ng-app="messageBox" ng-controller='MessageBoxController' ng-cloak>

    <table class="table table-striped">
        <thead>
        <tr>
            <th class="text-left">Date</th>
            <th class="text-left">Text</th>
            <th class="text-left">Created by</th>
            <th class="text-left">Status</th>
            <th class="text-right">Options</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($tournament->messages as $message):?>
            <tr id="message_<?= $message->id ?>">
                <td class="text-left">
                    <?= $message->created_at?>
                </td>
                <td class="text-left">
                    <?= $message->message?>
                </td>
                <td class="text-left">
                    <?= $message->created_by()?>
                </td>
                <td class="text-left">
                    <span ng-bind="message.status[<?= $message->id ?>]"
                          ng-init="message.status[<?= $message->id ?>]='<?= $message->status() ?>'"></span>
                </td>
                <td class="text-right">
                    <a class="btn btn-xs btn-default"
                       href="<?= URL::to($tournament_slug . '/message-box/edit', $message->id) ?>">
                        <?= LangHelper::get('edit', 'Edit')?>
                    </a>
                    <a class="btn btn-xs btn-default"
                       ng-click='publishAction("<?= URL::to($tournament_slug . '/message-box/publish', $message->id) ?>")'>
                        <span ng-bind="message.state[<?= $message->id ?>]"
                              ng-init="message.state[<?= $message->id ?>]='<?= ($message->active == 1)
                                  ? 'Unpublish' : 'Publish' ?>'"></span>
                    </a>
                    <a class="btn btn-xs btn-danger"
                       ng-click="remove($event, '<?= URL::to($tournament_slug . '/message-box/message', $message->id) ?>')">
                        Delete
                    </a>
                </td>
            </tr>
            <?php endforeach?>
        </tbody>
    </table>


</div>