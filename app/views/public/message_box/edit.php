<div class="page-title">
    <h1>
        <?= LangHelper::get('edit_message_box', 'Edit message box')?>
    </h1>

    <a class="pull-right btn-sm btn-default gap-top20 btn" href="<?= URL::to($tournament_slug.'/message-box')?>">
        <?= LangHelper::get('back', 'Back')?>
    </a>
</div>


<div ng-app="messageBox" ng-controller='MessageBoxController' ng-cloak>

    <form name="createForm" ng-submit="formSubmit($event, 'POST')" novalidate>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="form-group" id="holder-content">
            <label for="content" class="control-label">
                <?= LangHelper::get('text', 'Text')?>
                <span class="form-error"></span>
            </label>
            <textarea ng-model="formData.message.message" 
                  ng-init='formData.message.message="<?= $message->message?>"'
                  class="form-control" rows="6" required></textarea>
        </div>

        <button type="submit" class="btn btn-success btn-lg clear"><?= LangHelper::get('save', 'Save')?></button>

    </form>



</div>