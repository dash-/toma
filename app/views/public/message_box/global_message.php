<div id="message_box" style="display: <?= (!is_null($message)) ? 'block' : 'none'?>">
    <div class="container" class="critical-message">
        <?= (!is_null($message)) ? $message->message : null?>
    </div>
</div>
