<div ng-app="auth">

    <div class="col-md-5">

        <div class="page-title">
            <h1><?= LangHelper::get('login', 'Login')?></h1>
        </div>


        <div class="clearfix" ng-controller='LoginController' ng-cloak>
            <form name="loginForm" ng-submit="loginSubmit('<?= $tournament_slug?>')" novalidate>
                <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
                
                <div class="form-group" id="holder-email">
                    <label for="email" class="control-label">
                        <?= LangHelper::get('email', 'Email')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="text" name="email" class="form-control" ng-model="formData.email" required>
                </div>

                <div class="form-group" id="holder-password">
                    <label for="password" class="control-label">
                        <?= LangHelper::get('password', 'Password')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="password" name="password" class="form-control" ng-model="formData.password" required>
                </div>
            
                <button type="submit" ng-disabled="!loginForm.$valid" class="btn btn-success clear"><?= LangHelper::get('login', 'Login')?></button>
                
                <div class="clear row" ng-show="showError">
                    <p class="text-danger pad-left20 pad-top20">
                        {{ errorMsg }}
                    </p>
                </div>
            </form>
        </div>

    </div>

    <div class="col-md-5 col-md-offset-1">

        <div class="page-title">
            <h1><?= LangHelper::get('signup', 'Signup')?></h1>
        </div>

        <div class="clearfix" ng-controller='SignupController' ng-cloak>
            <form name="signupForm" ng-submit="signupSubmit('<?= $tournament_slug?>')" novalidate>
                <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
                
                <div class="form-group" id="holder-name">
                    <label for="name" class="control-label">
                        <?= LangHelper::get('name', 'Name')?>
                        <span class="form-error">{{errors.error_name}}</span>
                    </label>
                    <input type="text" name="name" class="form-control" ng-model="formData.name" required>
                </div>

                <div class="form-group" id="holder-surname">
                    <label for="surname" class="control-label">
                        <?= LangHelper::get('surname', 'Surname')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="text" name="surname" class="form-control" ng-model="formData.surname" required>
                </div>

                <div class="form-group" id="holder-email">
                    <label for="email" class="control-label">
                        <?= LangHelper::get('email', 'Email')?>
                        <span class="form-error">{{errors.error_email}}</span>
                    </label>
                    <input type="text" name="email" class="form-control" ng-model="formData.email" required>
                </div>

                <div class="form-group" id="holder-password">
                    <label for="password" class="control-label">
                        <?= LangHelper::get('password', 'Password')?>
                        <span class="form-error"></span>
                    </label>
                    <input type="password" name="password" class="form-control" ng-model="formData.password" required>
                </div>
            
                <button type="submit" ng-disabled="!signupForm.$valid" class="btn btn-success clear"><?= LangHelper::get('signup', 'Signup')?></button>
                
                <div class="clear row" ng-show="showError">
                    <p class="text-danger pad-left20 pad-top20">
                        {{ errorMsg }}
                    </p>
                </div>
            </form>
        </div>

    </div>
</div>