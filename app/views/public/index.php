<div class="page-title index-title">
    <h1><?= $tournament->title ?></h1>
</div>

<script type="text/javascript">
    var marker;
    var map;
    function initialize() {
        <?php if(!is_null($tournament->club->position_lat)): ?>
        showMap(<?= doubleval($tournament->club->position_lat) ?>, <?= doubleval($tournament->club->position_long) ?>, 17);
        <?php else: ?>

        new google.maps.Geocoder().geocode(
            {
                'address': "SPAIN"
            }, function (r, s) {
                if (s == google.maps.GeocoderStatus.OK) {
                    showMap(r[0].geometry.location.k, r[0].geometry.location.B, 7);
                }

            });
        <?php endif; ?>
    }

    function showMap(lat, lng, zoom) {
        if (lat != undefined && lng != undefined) {
            var mapOptions = {
                center: {lat: lat, lng: lng},
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map($('#map-canvas').get(0),
                mapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: {lat: lat, lng: lng},
                icon: "/css/icons/clubs.png",
                draggable: true,
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                $("#latitude").val(marker.getPosition().k);
                $("#longitude").val(marker.getPosition().B);
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<div class='map-tooltip'><?= addslashes($tournament->club->club_name) ?></br><?= addslashes($tournament->club->club_city) ?></br><?= addslashes($tournament->club->club_address) ?></br></div>"
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>
<div class="clearfix">

    <div class="container">

        <div class="row">
            <div class="col-md-2">

                <div class="thumbnail tile tile-small tiletopLeft1">

                    <p class="textDateD"><?= date("d"); ?></p>

                    <p class="textDateM"><?= date("M"); ?></p>

                </div>

                <div class="thumbnail tile tile-small tileLeft1">
                    <a href="<?= URL::to($tournament_slug . '/signups') ?>">
                        <?= LangHelper::get('signup', 'Signup')?>
                    </a>
                </div>
                <div class="thumbnail tile tile-small tileLeft1">
                    <a href="<?= URL::to($tournament_slug . '/tournament/details') ?>">
                        <?= LangHelper::get('details', 'Details')?>
                    </a>
                </div>
                <div class="thumbnail tile tile-small tileLeft1">
                    <a href="<?= URL::to($tournament_slug . '/tournament/contact-us') ?>">
                        <?= LangHelper::get('contact_us', 'Contact us')?>
                    </a>
                </div>
                <?php if ($tournament->getSponsor('id') && $tournament->getSponsorImage()): ?>
                    <div class="sponsor-public-index">
                        <img class="img-responsive" src="<?= $tournament->getSponsorImage() ?>">
                        <p class="text-center"><?= $tournament->getSponsor('title') ?></p>
                    </div>
                <?php endif ?>
            </div>


            <div class="col-md-6">
                <!-- gallery -->
                <div class="tileMiddle1 gallery-holder">
                    <?= View::make('public/index/gallery', [
                        'gals' => $tournament->galleries->all(),
                    ]) ?>
                </div>

                <!-- news -->
                <div class="news-holder clearfix">
                    <h3 class="title"><?= LangHelper::get('news', 'News') ?></h3>

                    <?php foreach ($news as $article): ?>
                        <div class="index-article-holder clearfix">
                            <?php if ($article->images->count()): ?>
                                <a href="<?= URL::to($tournament_slug . '/news/view', $article->id) ?>">
                                    <img src="<?= $article->first_image('small_thumbnails') ?>" alt=""
                                         class="pull-left">
                                </a>
                            <?php endif ?>
                            <h4>
                                <a href="<?= URL::to($tournament_slug . '/news/view', $article->id) ?>">
                                    <?= $article->title ?>
                                </a>
                            </h4>

                            <p>
                                <a href="<?= URL::to($tournament_slug . '/news/view', $article->id) ?>">
                                    <?= Str::limit(HtmlHelper::strip($article->content), 130) ?>
                                </a>
                            </p>
                        </div>
                    <?php endforeach ?>

                </div>

                <!-- live scores -->
                <div class="tileMiddle1 scores-holder upper-box">
                    <div class="scores-title">
                        <h3><?= LangHelper::get('live_scores', 'Live scores') ?></h3>
                    </div>
                    <div class="index-table-holder">
                        <?= View::make("/partials/public/index_page_score", array(
                            'scores' => $score,
                        )) ?>
                    </div>
                </div>

            </div>


            <div class="col-md-4">
                <div class="categories-holder clearfix">
                    <div class="countdown">
                        <input type="hidden" id="registration_date"
                               value="<?= DateTimeHelper::GetJSOnlyDate($tournament->date->entry_deadline) ?>"/>

                        <div class="deadline-title"><?= LangHelper::get("entry_deadline", "Entry deadline") ?></div>
                        <div>
                            <span id="c_days">0</span>
                            <small><?= LangHelper::get("days_short", "D") ?></small>
                        </div>
                        <div>
                            <span id="c_hours">0</span>
                            <small><?= LangHelper::get("hours_short", "H") ?></small>
                        </div>
                        <div>
                            <span id="c_minutes">0</span>
                            <small><?= LangHelper::get("minutes_short", "M") ?></small>
                        </div>
                        <div>
                            <span id="c_seconds">0</span>
                            <small><?= LangHelper::get("seconds_short", "S") ?></small>
                        </div>

                    </div>
                    <h3 class="title"><?= LangHelper::get('categories', 'Categories') ?></h3>
                    <a class="category-link" href="<?= URL::to($tournament_slug . '/scores') ?>">
                        <?= LangHelper::get('scores', 'Scores') ?>
                    </a>

                    <a class="category-link" href="<?= URL::to($tournament_slug . '/schedule') ?>">
                        <?= LangHelper::get('schedule', 'Schedule') ?>
                    </a>

                    <a class="category-link" href="<?= URL::to($tournament_slug . '/news') ?>">
                        <?= LangHelper::get('news', 'News') ?>
                    </a>

                    <a class="category-link" href="<?= URL::to($tournament_slug . '/draws') ?>">
                        <?= LangHelper::get('draws', 'Draws') ?>
                    </a>

                    <a class="category-link" href="<?= URL::to($tournament_slug . '/gallery') ?>">
                        <?= LangHelper::get('photos', 'Photos') ?>
                    </a>
                </div>


                <div id="map-canvas" class="public-club-map"></div>

            </div>
        </div>

    </div>

</div>

