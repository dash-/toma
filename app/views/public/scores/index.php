<div class="page-title">
	<h1><?= LangHelper::get('scores', 'Scores')?></h1>
</div>


<div class="clearfix" ng-app="scores" ng-controller='ScoresController'>
	<?php if ($scores->count()): ?>
		<?php foreach ($scores as $key => $score): ?>
			<h3><?= TournamentScoresHelper::getStatusByKey($key)?></h3>
			<?php if (!is_null($score)): ?>
				<?= View::make('public/scores/_scores_table', ['score' => $score, 'key' => $key])?>
			<?php endif ?>
		<?php endforeach ?>
	
	<?php else: ?>
		<p class="text-center"><?= LangHelper::get('no_available_data', 'No available data')?></p>
	<?php endif ?>

</div>