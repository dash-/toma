<table class="table">
    <thead>
        <tr>
            <th class="text-left"><?= LangHelper::get('court', 'Court')?></th>
            <?php if ($key == 0): ?>
                <th class="text-left"><?= LangHelper::get('info', 'Info')?></th>
            <?php endif ?>
            <th class="text-left"><?= LangHelper::get('player', 'Player')?> 1</th>
            <th class="text-left"><?= LangHelper::get('player', 'Player')?> 2</th>
            <th class="text-right"><?= LangHelper::get('result', 'Result')?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($score as $sc): ?>
            
            <tr>
                <td class="text-left">
                    <?= $sc['court_number'] ?>
                </td>
                <?php if ($key == 0): ?>
                    <td class="text-left">
                        <?= $sc['date_time'] ?>
                    </td>
                <?php endif ?>
                <td class="text-left contestants">
                    <?= $sc['team1_name'];?>
                </td>
                <td class="text-left contestants">
                    <?= $sc['team2_name'];?>
                </td>
                <td class="text-right scores" id="score_<?= $sc['match_id']?>">
                    <?= $sc['score']?>
                </td>
            </tr>

        <?php endforeach ?>
        
    </tbody>
</table>