<div class="grid">
<div class="main-page">
<div class="column3-layout">
            <span class="column-title cl-red">
                <?= LangHelper::get('assigned_tournaments', 'Assigned tournaments'); ?>
            </span>

    <div class="row">
        <div class="tiles tile-2 tile-red">

            <div class="tile-icon">
                <div class="tile-text">
                    <?php foreach ($assigned_tournaments->take(4) as $tournament): ?>
                        <p>
                            <a class="call" href="<?= URL::to('/referee/tournaments/tournament', $tournament->tournament_id) ?>">
                                <?= Str::limit($tournament->title . ', ' . $tournament->region_name, 46) ?>
                            </a>
                        </p>
                    <?php endforeach ?>
                </div>
            </div>
            <div class="tile-title">
                <a href="<?= URL::to('referee/tournaments/assigned') ?>" class="call" data-title="List of tournaments">
                    <?= LangHelper::get('assigned_tournaments', 'Assigned tournaments') ?>
                </a>
                <span class="tile-title-line"></span>

            </div>
            <div class="tile-number"><?= count($assigned_tournaments) ?></div>

        </div>


    </div>
    <div class="row">
        <div class="tiles tile-1 tile-green">
            <a href="<?= URL::to('notifications') ?>" class="call" data-title="Notifications">
                <div class="tile-icon">
                    <div class="alert-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('alerts', 'Alerts') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"><?= $alert_count ?></div>
            </a>
        </div>
        <div class="tiles tile-1 tile-blue">
            <a href="<?= URL::to('contacts') ?>" class="call" data-title="Contacts">
                <div class="tile-icon">
                    <div class="contact-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('contacts', 'Contacts') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"><?= $contacts_count ?></div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="tiles tile-2 tile-yellow">

            <div class="tile-icon">
                <div class="livescore-icon"></div>
                <div class="tile-text table-livescore">
                    <?= View::make("partials/live_score", array(
                        'live_score' => $live_score,
                    )) ?>
                </div>
            </div>

            <div class="tile-title">
                <a href="<?= URL::to('scores') ?>" class="call">
                    <?= LangHelper::get('live_scores', 'Live Scores') ?>
                    <span class="tile-title-line"></span>
            </div>
            <div class="tile-number"><?= count($live_score) ?></div>
            </a>
        </div>
    </div>
</div><!--
        --><div class="column3-layout">
                       <span class="column-title cl-red">
                <?= LangHelper::get('upcoming', 'Upcoming'); ?>
            </span>


    <div class="row">
        <div class="tiles tile-2 tile-yellow">
            <div class="tile-icon">
                <table class="tile-text upcoming-tournaments-table">
                    <tr>
                        <?php $tournaments_count = TournamentHelper::calculateByWeeks('next week', 'count') ?>
                        <td class="week">
                            <a href="<?= URL::to('tournaments/week', 2)?>" class="call">
                                <?= LangHelper::get('next_week', 'Next week')?>
                            </a>
                        </td>
                        <td class="noplayers"><?= $tournaments_count ?></td>
                        <td class="referees"><?= LangHelper::get('referees', 'Referees')?></td>
                        <td class="noreferees"><?= TournamentHelper::countRefereesByWeeks("next week") ?>/<?= $tournaments_count ?></td>
                        <td class="tourn-status"><span class="st-green"></span></td>
                    </tr>
                    <tr>
                        <?php $tournaments_count = TournamentHelper::calculateByWeeks('+1 week', 'count') ?>
                        <td>
                            <a href="<?= URL::to('tournaments/week', 3)?>" class="call">
                                <?= LangHelper::get('next_2_weeks', 'Next 2 Weeks')?>
                            </a>
                        </td>
                        <td><?= $tournaments_count ?></td>
                        <td><?= LangHelper::get('referees', 'Referees')?></td>
                        <td><?= TournamentHelper::countRefereesByWeeks("+1 week") ?>/<?= $tournaments_count ?></td>
                        <td><span class="st-green"></span></td>
                    </tr>
                    <tr>
                        <?php $tournaments_count = TournamentHelper::calculateByWeeks('+2 weeks', 'count') ?>
                        <td>
                            <a href="<?= URL::to('tournaments/week', 4)?>" class="call">
                                <?= LangHelper::get('next_3_weeks', 'Next 3 Weeks')?>
                            </a>
                        </td>
                        <td><?= $tournaments_count?></td>
                        <td><?= LangHelper::get('referees', 'Referees')?></td>
                        <td><?= TournamentHelper::countRefereesByWeeks("+2 weeks") ?>/<?= $tournaments_count ?></td>
                        <td><span class="st-red"></span></td>
                    </tr>
                </table>
            </div>
            <div class="tile-title">
                <a class="call" href="<?= URL::to('tournaments/upcoming')?>">
                    <?= LangHelper::get('upcoming_tournaments', 'Upcoming Tournaments')?>
                    <span class="tile-title-line"></span>
                </a>
            </div>
            <div class="tile-number"><?= TournamentHelper::sumCountByWeeks()?></div>
        </div>
    </div>
    <div class="row">
        <div class="tiles tile-2 tile-red">
            <a href="<?= URL::to('calendar') ?>" class="call">
                <div class="tile-icon">
                    <div class="calendar-icon"></div>

                </div>
                <div class="tile-title">
                    <?= LangHelper::get('calendar', 'Calendar') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"><?= $calendar_count ?></div>
            </a>
        </div>
    </div>
</div><!--
        --><div class="column3-layout">
                       <span class="column-title cl-red">
                <?= LangHelper::get('all_tournaments', 'All tournaments'); ?>
            </span>

    <div class="row">
        <a href="<?= URL::to('referee/tournaments') ?>" class="call tiles tile-2 tile-yellow"
           data-title="List of tournaments">
            <div class="tile-icon">
                <div class="tournament-icon referee"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('tournaments', 'Tournaments') ?>
                <span class="tile-title-line"></span>
            </div>
            <div class="tile-number"><? //= $alert_count?></div>
        </a>
    </div>
    <div class="row">

        <a href="<?= URL::to("rankings") ?>" class="call tiles tile-1 tile-yellow">
            <div class="tile-icon">
                <div class="ranking-icon"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('rankings', 'Rankings') ?>
                <span class="tile-title-line"></span>
            </div>
            <div class="tile-number" title="Ranking move"><?= $ranking_move_count ?></div>
        </a>
        <a href="<?= URL::to('clubs') ?>" data-title="Clubs"
           class="tiles tile-1 tile-yellow filled-gray1 call">
            <div class="tile-icon">
                <div class="club-icon"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('clubs', 'Clubs') ?>
                <span class="tile-title-line"></span>
            </div>
            <div class="tile-number"><?= $clubs_count ?></div>
        </a>
    </div>
    <div class="row">
        <a href="<?= route("players") ?>" data-title="Players"
           class="call tiles <?= Auth::user()->isRefereeAdmin() ? 'tile-1' : 'tile-2'?> tile-yellow ">
            <div class="tile-icon">
                <div class="player-icon"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('players', 'Players') ?>
                <span class="tile-title-line"></span>
            </div>
            <div class="tile-number"><?= $players_count ?></div>
        </a>
        <?php if (Auth::user()->isRefereeAdmin()):?>
            <a href="/referees" data-title="Referees" class="call tiles tile-1 tile-yellow ">
                <div class="tile-icon">
                    <div class="referee-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('referees', 'Referees') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"><?= $referees_count?></div>
            </a>
        <?php endif?>

    </div>

</div>
</div>

</div>

