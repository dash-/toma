<div class="container ang">
    <div class="grid" ng-controller="actionsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <h3 class="no-permission">
                <?= LangHelper::get("page_not_found", "Page not found") ?>
            </h3>
        </div>
    </div>
