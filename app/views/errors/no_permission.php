<div class="container ang">
    <div class="grid" ng-controller="actionsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <h3 class="no-permission">
                <?= LangHelper::get('no_page_permission', 'Sorry, but you do not have permission to enter this page.') ?>
            </h3>
        </div>
    </div>
</div>