  <div class="container">
    <div class="grid">
      <div class="row">
          <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
      </div>
      <div class="row">
        <div class="clearfix">
          <a class="place-right button gap-top10 gap-left10 call red-col-bg" href="<?= URL::to('profile/change-password')?>"><?= LangHelper::get('change_password', 'Change password')?></a>
          <a class="place-right button gap-top10 gap-left10 call" href="<?= URL::to('profile/edit')?>"><?= LangHelper::get('edit_profile', 'Edit profile')?></a>
          <a class="place-right button gap-top10 call green-col-bg" href="<?= URL::to('profile/upload')?>"><?= LangHelper::get('upload_image', 'Upload image')?></a>
        </div>
      </div>
      <div class="tournament-first">
        <div class="table-title tournament">
         <h3><?= LangHelper::get('profile', 'Profile')?></h3>
         <span class="line"></span>
       </div>
     </div>
     <div class="tournament-second pad-top40">

      <div class="row players-info">
        <div class="half">
          <p><span class="color-rfet"><?= LangHelper::get('name', 'Name')?>:</span> <?= $user->info('name') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('surname', 'Surname')?>:</span> <?= $user->info('surname') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('date_of_birth', 'Date of birth')?>:</span> <?= $user->info('date_of_birth')?></p>
          <p><span class="color-rfet"><?= LangHelper::get('phone', 'Phone')?>:</span> <?= $user->info('phone') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('address', 'Address')?>:</span> <?= $user->info('address') ?></p>                     
          <p><span class="color-rfet"><?= LangHelper::get('city', 'City')?>:</span> <?= $user->info('city') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('postal_code', 'Postal code')?>:</span> <?= $user->info('postal_code') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('sex', 'Sex')?>:</span> <?= $user->info('sex') ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('website', 'Website')?>:</span> <?=  $user->info('web') ?></p>
        </div>
        <div class="half">
          <p><span class="color-rfet"><?= LangHelper::get('username', 'Username')?>:</span> <?= $user->username ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('email', 'Email')?>:</span> <?= $user->email ?></p>
          <p><span class="color-rfet"><?= LangHelper::get('role', 'Role')?>:</span> <?= $user->getRole('name') ?></p>     
          <p><span class="color-rfet"><?= LangHelper::get('default_language', 'Default language')?>:</span> <?= $user->getLang() ?></p>     
        </div>
      </div>
    </div>     

  </div>
  </div>
  </div>