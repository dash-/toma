<div class="container ang">
	<div class="grid">
		<div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
		</div>
		<div class="row">
			<div class="clearfix">
				<div class="empty-space"></div>
			</div>
		</div>
		<div class="tournament-first">
			<div class="table-title tournament">
				<h3><?= LangHelper::get('change_password', 'Change password')?></h3>
				<span class="line"></span>
			</div>
		</div>

		<form name="editForm" ng-class="{ 'show-errors': showErrors }" ng-submit="formSubmit($event, 'POST', '<?= LangHelper::get('password_have_been_changed', 'Password have been changed')?>','<?= Request::url()?>')" ng-controller="FormController" novalidate autocomplete="off">
			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			<fieldset>
				<div class="tournament-second">
					<div class="row">
						<div class="half">
							<div class="form-control">
								<label for="name"><?= LangHelper::get('password', 'Password')?></label>
								<div class="input-control text">
									<input type="password" name="password" required ng-model="formData.old_password" >
								</div>
							</div>
						</div>
						<div class="half">		
							<div class="form-control">
								<label for="name"><?= LangHelper::get('new_password', 'New password')?></label>
								<div class="input-control text">
									<input type="password" name="new_password" required ng-model="formData.password" >
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="half">
							<div class="form-control">
								<label for="name"><?= LangHelper::get('repeat_new_password', 'Repeat new password')?></label>
								<div class="input-control {{errorState}} text">
									<input type="password" name="repeat_new_password" required ng-model="formData.password_confirmation">
								</div>
							</div>
						</div>
						<button type="submit" class="button clear place-right primary large dark" style="margin-top: 100px;"><?= LangHelper::get('save', 'Save')?></button>
						
					</div>
				</div>
				<form-errors></form-errors>	                    
			</fieldset>
		</form>
	</div>
</div>

<script>
angular.element(document).ready(function() {
	angular.bootstrap('.ang', ['genericFormApp']);
});
</script>