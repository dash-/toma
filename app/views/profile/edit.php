<div class="container ang">
<div class="grid">
	<div class="row">
        <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
	</div>
	<div class="row">
		<div class="clearfix">
			<div class="empty-space"></div>
		</div>
	</div>
	<div class="tournament-first">
		<div class="table-title tournament">
			<h3><?= LangHelper::get('edit_profile', 'Edit profile')?></h3>
			<span class="line"></span>
		</div>
	</div>
	<form name="editForm" ng-class="{ 'show-errors': showErrors }" enctype="multipart/form-data" ng-submit="formSubmit($event, 'PUT', '<?= LangHelper::get('profile_successfully_updated', 'Profile successfully updated')?>','<?= Request::url()?>', false, true)" ng-controller="FormController" novalidate>
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		<fieldset>
			<div class="tournament-second">	
				<div class="row">
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('name', 'Name')?></label>
							<div class="input-control text">
								<input type="text" name="name" ng-model="formData.contacts.name" required ng-init="formData.contacts.name='<?= $user->info('name')?>'">
							</div>
						</div>
					</div>
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('surname', 'Surname')?></label>
							<div class="input-control text">
								<input type="text" name="surname" ng-model="formData.contacts.surname" required ng-init="formData.contacts.surname='<?= $user->info('surname')?>'">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('date_of_birth', 'Date of birth')?></label>
							<div class="input-control text">
								<input type="text" name="date_of_birth" datepicker-popup="dd.MM.yyyy" is-open="opened['date_of_birth']" 
								show-button-bar="false" ng-click="openDatePicker($event, 'date_of_birth')" ng-model="formData.contacts.date_of_birth" 
								ng-init="formData.contacts.date_of_birth='<?= $user->info('date_of_birth')?>  '" formatdate>
							</div>                                                                                                                                                                                                                                 
						</div>
					</div>
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('phone', 'Phone')?></label>
							<div class="input-control text">
								<input type="text" name="phone" ng-model="formData.contacts.phone" ng-init="formData.contacts.phone='<?= $user->info('phone')?>'">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('address', 'Address')?></label>
							<div class="input-control text">
								<input type="text" name="address" ng-model="formData.contacts.address" ng-init="formData.contacts.address='<?= $user->info('address')?>'">
							</div>
						</div>
					</div>
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('city', 'City')?></label>
							<div class="input-control text">
								<input type="text" name="city" ng-model="formData.contacts.city" ng-init="formData.contacts.city='<?= $user->info('city')?>'">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('postal_code', 'Postal code')?></label>
							<div class="input-control text">
								<input type="text" name="postal_code" ng-model="formData.contacts.postal_code" ng-init="formData.contacts.postal_code='<?= $user->info('postal_code')?>'">
							</div>
						</div>
					</div>
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('website', 'Website')?></label>
							<div class="input-control text">
								<input type="text" name="website" ng-model="formData.contacts.web" ng-init="formData.contacts.web='<?= $user->info('web')?>'">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="half">
						<div class="form-control">
							<label for="name"><?= LangHelper::get('default_language', 'Default language')?></label>
								<?= Form::select('language_id', $langs_array, NULL, array('ng-model' => 'formData.user.language_id', 'ng-init' => "formData.user.language_id='".$user->language_id."'"));?>
						</div>
					</div>
				</div>

				<button type="submit" class="button clear place-right primary large dark" style="margin-top: 40px;"><?= LangHelper::get('save', 'Save')?></button>
				<form-errors></form-errors></div>

			</fieldset>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){

	$('select').select2();

});

angular.element(document).ready(function() {
	angular.bootstrap('.ang', ['genericFormApp']);
});
</script>