<div class="container ang">
    <div class="grid" ng-controller="tableController" data-ng-init="init(1)"  data-scUrl="/contacts/data">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
               <h3><?= LangHelper::get('contacts', 'Contacts')?></h3>
               <span class="line"></span>
            </div>
        </div>
    
        <div class="row filter-row">
            
            <div class="clearfix search-form-holder">
                <form action="<?= URL::current()?>" method="GET">
                    <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                    <div class="form-control place-left">
                        <?= Form::select('week', $filter_array, '{{ searchText.select }}' , array('class' => 'select2',
                            'ng-change' => 'contactsFilter(searchText.select)', 'ng-model' => 'searchText.select',
                            'ng-init' => 'searchText.select = "this week"', 'ng-class' => 'no-search-box'));?>
                    </div>
                </form>
            </div>

        </div>

        <div class="row">
            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('tournament', 'Tournament')?></th>
                    <th class="text-left"><?= LangHelper::get('name', 'Name')?></th>
                    <th class="text-left"><?= LangHelper::get('email', 'Email')?></th>
                    <th class="text-left"><?= LangHelper::get('phone', 'Phone')?></th>
                    <th class="text-left"><?= LangHelper::get('fax', 'Fax')?></th>
                    <th class="text-left"><?= LangHelper::get('website', 'Website')?></th>
                </tr>
                </thead>

                <tbody>
                    <tr ng-repeat="tournament in rows track by $index">
                        <td>{{ tournament.title }}</td>
                        <td>{{ tournament.name }}</td>
                        <td>
                            <a class="call yellow-color" href="mailto:{{tournament.email}}">
                                {{ tournament.email }}
                            </a>
                        </td>
                        <td>{{ tournament.phone }}</td>
                        <td>{{ tournament.fax }}</td>
                        <td>{{ tournament.website }}</td>
                    </tr>
                </tbody>
            </table>
            <h3 class="fg-white text-center" ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data')?></h3>

        </div>
    </div>

</div>

<script>
$(document).ready(function() {
    $("select").select2({});
});
angular.element(document).ready(function() {
    angular.bootstrap('.ang', ['datatablesApp']);
});
</script>