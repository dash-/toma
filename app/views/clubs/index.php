<div class="container ang">
    <div class="grid datatable" ng-controller="tableController" data-ng-init="init(1)" data-scUrl="/clubs/data" data-tablesortcolumn="region_name">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                 <h3><?= LangHelper::get('clubs', 'Clubs')?></h3>
                 <span class="line"></span>
            </div>
        </div> 

        <div class="table-title players">
            <div class="results-number input-control select">
                <?= Form::select('items', $items_per_page, NULL, array('ui-select2'=>"select2Options",'ng-change' => 'itemsPerPage()', 'ng-model' => 'counter', 'ng-init' => 'counter=10', 'ng-class' => 'no-search-box'));?>
                <p class="fg-yellow gap-top24"><?= LangHelper::get('total_results', 'Total results')?>: {{totalItems}}</p>
            </div>
        </div>

        <div class="player-align table-content players clubResponsive">
            <form ng-submit="search($event, 1)" name="searchForm" class="tabScrl">
                <table class="table bg-dark striped table-scrollable-w768">
                    <thead>
                    <tr>
                        <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.region_name" placeholder="Search by region">
                            </div>
                            <a ng-click="doOrder('region_name')" href="#">
                                <?= LangHelper::get('region', 'Region')?> <i ng-class="sortBy('region_name')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.club_name" placeholder="Search by name">
                            </div>
                            <a ng-click="doOrder('club_name')" href="#">
                                <?= LangHelper::get('name', 'Name')?> <i ng-class="sortBy('club_name')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.club_city" placeholder="Search by city">
                            </div>
                            <a ng-click="doOrder('club_city')" href="#">
                                <?= LangHelper::get('city', 'City')?> <i ng-class="sortBy('club_city')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.club_address" placeholder="Search by address">
                            </div>
                            <a ng-click="doOrder('club_address')" href="#">
                                <?= LangHelper::get('address', 'Address')?> <i ng-class="sortBy('club_address')"></i>
                            </a>
                        </th>
                        <th class="text-left full">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.nif" placeholder="Search by NIF">
                            </div>
                            <a ng-click="doOrder('nif')" href="#">
                                NIF <i ng-class="sortBy('nif')"></i>

                            </a>

                        </th>
                        <th class="text-left last-col">
                            <div class="input-control text">
                                <button type="submit" ng-click="search($event, 1)" ng-class="scSearch == 1 ? 'split-button' : ''" class="primary dark full">
                                    <?= LangHelper::get('filter', 'Filter')?>
                                </button>
                                <a ng-click="resetSearch()" ng-show="scSearch == 1"  class="button primary split-button reset-button call" href="#">
                                    <?= LangHelper::get('reset_search', 'Reset search')?>
                                </a>
                            </div>
                            <a class="options"><?= LangHelper::get('options', 'Options')?></a>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="club in rows track by $index">
                        <td>{{ club.region_name }}</td>
                        <td>{{ club.club_name }}</td>
                        <td>{{ club.club_city }}</td>
                        <td>{{ club.club_address }}</td>
                        <td class="map-missing-holder">
                            <span>{{ club.nif }}</span>
                            <span ng-class="club.position_lat == null ? 'map-missing' : ''"></span>
                        </td>
                        <td class="text-left input-control transparent">
                            <a class="button primary info inline-block" ng-href="/clubs/info/{{club.id}}"><?= LangHelper::get('info', 'Info')?></a>
                            <a class="call button small primary default" ng-href="/clubs/edit/{{club.id}}"><?= LangHelper::get('edit', 'Edit')?></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>

            <h3 class="fg-white text-center" ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data')?></h3>
            <div class="pagination" ng-show="rows.length > 1">
                <div class="input-control back-in-pagination">
                    <a class="button primary yellow back big call" href="/"><?= LangHelper::get('back', 'BACK')?></a>
                </div>
                <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage" max-size="maxSize" boundary-links="true" rotate="false" items-per-page="counter"></pagination>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $("select").select2();
    });

    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>
