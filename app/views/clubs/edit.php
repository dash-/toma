<div class="container ang">
    <div class="grid">
    	<div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>

        <div class="tournament-first">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('edit_club', 'Edit club')?></h3>
                    <span class="line"></span>
                </div>
        </div>
        <div class="row clubsEditResponsive">
            <a class="button green-col-bg" href="<?= URL::to('clubs/upload/'.$club->id)?>"><?= LangHelper::get('upload_image', 'Upload image')?></a>
        </div>
        <div class="row clubsEditResponsive">
            <?php if(is_null($club->position_lat)): ?>
            <a class="button red-col-bg" href="<?= URL::to('clubs/position/'.$club->id)?>"><?= LangHelper::get('position_not_set', 'POSITION NOT SET')?></a>
            <?php else: ?>
            <a class="button green-col-bg" href="<?= URL::to('clubs/position/'.$club->id)?>"><?= LangHelper::get('position', 'Position')?></a>
            <?php endif; ?>
        </div>
		
			<form name="editForm" ng-class="{ 'show-errors': showErrors }" ng-submit="formSubmit($event, 'PUT','Form updated','<?= Request::url()?>')" ng-controller="FormController" novalidate> 
				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
				<fieldset>
                    <div class="tournament-second">
					<div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name')?></label>
                                <div class="input-control text">
                                    <input type="text" name="club_name" ng-init="formData.club.club_name='<?= $club->club_name?>'" ng-model="formData.club.club_name" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('city', 'City')?></label>
                                <div class="input-control text">
                                    <input type="text" name="club_city" ng-init="formData.club.club_city='<?= $club->club_city?>'" ng-model="formData.club.club_city" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('province', 'Province')?></label>
                                 <?= Form::select('province_id', $province, NULL, array('required','ng-model' => 'formData.club.province_id', 'ng-init' => "formData.club.province_id='".$club->province_id."'"));?>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('region', 'Region')?></label>
                                <?= Form::select('region_id', $region, NULL, array('required','ng-model' => 'formData.club.region_id', 'ng-init' => "formData.club.region_id='".$club->region_id."'"));?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('address', 'Address')?></label>
                                <div class="input-control text">
                                    <input type="text" name="club_address" ng-init="formData.club.club_address='<?= $club->club_address?>'" ng-model="formData.club.club_address" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('short_name', 'Short name')?></label>
                                <div class="input-control text">
                                    <input type="text" name="short_name" ng-init="formData.club.short_name='<?= $club->short_name?>'" ng-model="formData.club.short_name">
                                </div>
                            </div>
                        </div>
                    </div>                  
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('founded', 'Founded')?></label>
                                <div class="input-control text">
                                    <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.club.founded_date" is-open="opened['founded_date']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'founded_date')" ng-init="formData.club.founded_date='<?= DateTimeHelper::GetShortDateFullYear($club->founded_date)?>'" formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('number_of_tracks', 'Number of tracks')?></label>
                                <div class="input-control text">
                                    <input type="text" name="number_of_tracks" ng-init="formData.club.number_of_tracks='<?= $club->number_of_tracks?>'" ng-model="formData.club.number_of_tracks" required>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('email', 'Email')?></label>
                                <div class="input-control text">
                                    <input type="email" name="club_email" ng-init="formData.club.club_email='<?= $club->club_email?>'" ng-model="formData.club.club_email">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('contact_#1', 'Contact #1')?></label>
                                <div class="input-control text">
                                    <input type="text" name="club_phone" ng-init="formData.club.club_phone='<?= $club->club_phone?>'" ng-model="formData.club.club_phone">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('contact_#2', 'Contact #2')?></label>
                                <div class="input-control text">
                                    <input type="text" name="phone2" ng-init="formData.club.phone2='<?= $club->phone2?>'" ng-model="formData.club.phone2">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('fax', 'Fax')?></label>
                                <div class="input-control text">
                                    <input type="text" name="fax" ng-init="formData.club.fax='<?= $club->fax?>'" ng-model="formData.club.fax">
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('website', 'Website')?></label>
                                <div class="input-control text">
                                    <input type="text" name="web" ng-init="formData.club.web='<?= $club->web?>'" ng-model="formData.club.web">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('nif', 'NIF')?></label>
                                <div class="input-control text">
                                    <input type="text" name="nif" ng-init="formData.club.nif='<?= $club->nif?>'" ng-model="formData.club.nif" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('president', 'President')?></label>
                                <div class="input-control text">
                                    <input type="text" name="president" ng-init="formData.club.president='<?= $club->president?>'" ng-model="formData.club.president">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg" style="margin-top: 38px;"><?= LangHelper::get('save', 'Save')?></button>
                    </div>

                    <form-errors></form-errors>
                </div>
				</fieldset>
			</form>
		
	</div>
</div>

<script>
$(document).ready(function() {
        $("select").select2();
    });

    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>