<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">

                <h3><?= LangHelper::get('edit_club_position', 'Edit club position') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">

            <?php if(is_null($club->position_lat)): ?>
            <h2 class="red-color"><?= LangHelper::get('set_club_position', 'Please set club position') ?></h2>
            <?php endif; ?>
            <?= Form::open() ?>
            <div class="input-control text">

            </div>
            <div class="half">
                <div class="form-control">
                    <label for="name"><?= LangHelper::get('latitude', 'Latitude')?></label>
                    <div class="input-control text">
                        <input class="form-control" type="text" name="latitude" id="latitude" value="<?= $club->position_lat ?>"
                               readonly/>
                    </div>
                </div>
            </div>
            <div class="half">
                <div class="form-control">
                    <label for="name"><?= LangHelper::get('longitude', 'Longitude')?></label>
                    <div class="input-control text">
                        <input class="form-control" type="text" name="longitude" id="longitude"
                               value="<?= $club->position_long ?>" readonly/>
                    </div>
                </div>
            </div>


            <div class="input-control gap-bottom20">
                <input type="submit" class="button medium green-col-bg pull-right" value="Save"/>
            </div>

            <?= Form::close() ?>
        </div>
        <div id="map-canvas" class="club-map"></div>
    </div>
</div>

<script type="text/javascript">
    var marker;
    var map;
    function initialize() {
        <?php if(!is_null($club->position_lat)): ?>
            showMap(<?= doubleval($club->position_lat) ?>, <?= doubleval($club->position_long) ?>, 17);
        <?php else: ?>
        new google.maps.Geocoder().geocode(
            {
                'address': "Barcelona"
            }, function (r, s) {
                if (s == google.maps.GeocoderStatus.OK) {
                    showMap(r[0].geometry.location.k, r[0].geometry.location.D, 7);
                }

            });
        <?php endif; ?>
    }

    function showMap(lat, lng, zoom) {
        if (lat != undefined && lng != undefined) {
            var mapOptions = {
                center: {lat: lat, lng: lng},
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map($('#map-canvas').get(0),
                mapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: {lat: lat, lng: lng},
                icon: "/css/icons/clubs.png",
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                $("#latitude").val(marker.getPosition().k);
                $("#longitude").val(marker.getPosition().D);
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<div class='map-tooltip'><?= addslashes($club->club_name) ?></br><?= addslashes($club->club_city) ?></br><?= addslashes($club->club_address) ?></br></div>"
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>