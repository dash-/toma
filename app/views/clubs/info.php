<div class="container" ng-app="datatablesApp">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('club_info', 'Club info') ?></h3>
                <span class="line"></span>
            </div>

            <?php if ($club->image_link): ?>
                <div class="club-image">
                    <img src="<?= $club->image('thumbnails') ?>" alt="">
                </div>
            <?php endif ?>
        </div>
        <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('name', 'Name') ?>:</span> <?= $club->club_name ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('city', 'City') ?>:</span> <?= $club->club_city ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('address', 'Address') ?>
                            :</span> <?= $club->club_address ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('short_name', 'Short Name') ?>
                            :</span> <?= $club->short_name ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('founded', 'Founded') ?>
                            :</span> <?= date("d.m.Y", strtotime($club->founded_date)) ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('number_of_tracks', 'Number of tracks') ?>
                            :</span> <?= $club->number_of_tracks ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('province', 'Province') ?>
                            :</span> <?= $club->province->province_name ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('region', 'Region') ?>
                            :</span> <?= $club->province->region->region_name ?></p>

                </div>
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('email', 'Email') ?>
                            :</span> <?= $club->club_email ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('contact_#1', 'Contact #1') ?>
                            :</span> <?= $club->club_phone ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('contact_#2', 'Contact #2') ?>
                            :</span> <?= $club->phone2 ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('fax', 'Fax') ?>:</span> <?= $club->fax ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('website', 'Website') ?>:</span> <?= $club->web ?>
                    </p>

                    <p><span class="color-rfet"><?= LangHelper::get('nif', 'NIF') ?>:</span> <?= $club->nif ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('president', 'President') ?>
                            :</span> <?= $club->president ?></p>

                </div>
            </div>
            <div class="clearfix gap-top20">
                <div id="map-canvas" class="club-map"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var marker;
    var map;
    function initialize() {
        <?php if(!is_null($club->position_lat)): ?>
        showMap(<?= doubleval($club->position_lat) ?>, <?= doubleval($club->position_long) ?>, 17);
        <?php else: ?>

        new google.maps.Geocoder().geocode(
            {
                'address': "SPAIN"
            }, function (r, s) {
                if (s == google.maps.GeocoderStatus.OK) {
                    showMap(r[0].geometry.location.k, r[0].geometry.location.B, 7);
                }

            });
        <?php endif; ?>
    }

    function showMap(lat, lng, zoom) {
        if (lat != undefined && lng != undefined) {
            var mapOptions = {
                center: {lat: lat, lng: lng},
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map($('#map-canvas').get(0),
                mapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: {lat: lat, lng: lng},
                icon: "/css/icons/clubs.png",
                draggable: false
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                $("#latitude").val(marker.getPosition().k);
                $("#longitude").val(marker.getPosition().B);
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<div class='map-tooltip'><?= addslashes($club->club_name) ?></br><?= addslashes($club->club_city) ?></br><?= addslashes($club->club_address) ?></br></div>"
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>