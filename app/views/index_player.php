<div class="grid">
    <div class="main-page">
        <div class="column3-layout">
            <span class="column-title cl-red">
                <?= LangHelper::get('today', 'Today'); ?>
            </span>

            <div class="row">
                <div class="tiles tile-2 tile-red">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="Today">-->
                            <?= LangHelper::get('this_week', 'This week'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>


            </div>
            <div class="row">
                <div class="tiles tile-1 tile-blue">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="My double partner">-->
                            <?= LangHelper::get('my_double_partner', 'My double partner'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
                <div class="tiles tile-1 tile-yellow">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="My double partner">-->
                            <?= LangHelper::get('my_practice_partner', 'My practice partner'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
            </div>

            <div class="row">
                <div class="tiles tile-2 tile-green">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="Today">-->
                            <?= LangHelper::get('coach_notes', 'Coach notes'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>


            </div>
        </div><!--
        --><div class="column3-layout">
            <span class="column-title cl-red">
                <?= LangHelper::get('upcoming', 'Upcoming') ?>
            </span>
            <div class="row">
                <div class="tiles tile-2 tile-green">
                    <div class="tile-icon">
                        <table class="tile-text upcoming-tournaments-table">
                            <tr>
                                <td class="week">
                                    <a href="<?= URL::to('tournaments/week', 2)?>" class="call">
                                        <?= LangHelper::get('this_week', 'This week')?>
                                    </a>
                                </td>
                                <td class="noplayers"><?= $this_week_tournaments ?></td>
                                <td class="tourn-status"><span class="st-green"></span></td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?= URL::to('tournaments/week', 3)?>" class="call">
                                        <?= LangHelper::get('next_2_weeks', 'Next 2 Weeks')?>
                                    </a>
                                </td>
                                <td><?= $next_week_tournaments ?></td>
                                <td><span class="st-green"></span></td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="<?= URL::to('tournaments/week', 4)?>" class="call">
                                        <?= LangHelper::get('next_3_weeks', 'Next 3 Weeks')?>
                                    </a>
                                </td>
                                <td><?= $next_2week_tournaments ?></td>
                                <td><span class="st-green"></span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('player/upcoming')?>" class="call"
                           data-title="Upcoming tournaments">
                            <?= LangHelper::get('upcoming_tournaments', 'Upcoming tournaments'); ?>
                        </a>
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?= $this_week_tournaments + $next_week_tournaments + $next_2week_tournaments ?></div>

                </div>


            </div>
            <div class="row">
                <div class="tiles tile-1 tile-blue">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="Signup">-->
                            <?= LangHelper::get('signup', 'Signup'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
                <div class="tiles tile-1 tile-red">
                    <a href="<?= URL::to('notifications') ?>" class="call"
                       data-title="<?= LangHelper::get('notifications', 'Notifications') ?>">
                        <div class="tile-icon">
                            <div class="alert-icon"></div>
                        </div>
                        <div class="tile-title">

                            <?= LangHelper::get('alerts', 'Alerts'); ?>

                            <span class="tile-title-line"></span>

                        </div>
                        <div class="tile-number"><?= $alert_count ?></div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="tiles tile-1 tile-green">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="RFET News">-->
                            <?= LangHelper::get('rfet_news', 'RFET News'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
                <div class="tiles tile-1 tile-red">
                    <div class="tile-icon">
                        <div class="player-icon"></div>
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('players') ?>" class="call"
                           data-title="Players">
                            <?= LangHelper::get('players', 'Players'); ?>
                        </a>
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?= $players_count ?></div>

                </div>


            </div>
        </div><!--
        --><div class="column3-layout">
            <span class="column-title cl-red">
                <?= LangHelper::get('my_tools', 'My tools') ?>
            </span>
            <div class="row">
                <div class="tiles tile-2 tile-red">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('player/my-ranking') ?>" class="call"
                           data-title="My ranking">
                            <?= LangHelper::get('my_ranking', 'My ranking'); ?>
                        </a>
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>


            </div>
            <div class="row">
                <div class="tiles tile-1 tile-blue">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('player/my-statistics') ?>" class="call"
                           data-title="My statistics">
                            <?= LangHelper::get('my_statistics', 'My statistics'); ?>
                        </a>
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
                <div class="tiles tile-1 tile-yellow">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="My annual plan">-->
                            <?= LangHelper::get('my_annual_plan', 'My annual plan'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
            </div>

            <div class="row">
                <div class="tiles tile-1 tile-blue">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="My favorites">-->
                            <?= LangHelper::get('my_favorites', 'My favorites'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?= $favorited_players ?></div>

                </div>
                <div class="tiles tile-1 tile-yellow">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
<!--                        <a href="<#?//= URL::to('referee/tournaments/assigned') ?>" class="call"-->
<!--                           data-title="Match analysis">-->
                            <?= LangHelper::get('match_analysis', 'Match analysis'); ?>
<!--                        </a>-->
                        <span class="tile-title-line"></span>

                    </div>
                    <div class="tile-number"><?//= count($assigned_tournaments) ?></div>

                </div>
            </div>
        </div>
    </div>
</div>
