<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">

                <a class="metro button bigger yellow-col-bg right call br" data-title="Sponsors"
                   href="/sponsors"><?= LangHelper::get('sponsors', 'Sponsors') ?></a>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= $tournament->title ?></h3>
                <span class="line"></span>
            </div>
        </div>
    </div>
    <div class="list_of_draws">
    <?php foreach ($draws as $draw): ?>

            <div class="tiles tile-2 tile-green">
                <a href="<?= URL::to(UrlHelper::getDrawsUrl($link, $draw->id)) ?>" class="call">
                    <div class="tile-icon">
                        <!--                    <div class="signup-icon"></div>-->
                        <!--                    <div class="draws-icon"></div>-->
                    </div>
                    <div class="tile-title">
                        <?= $draw->category->name ?> -
                        <?= $draw->typeName() . ' - ' . $draw->genderName() ?>
                        <span class="tile-title-line"></span>
                    </div>
                    <!--                <div class="tile-number">-->
                    <? //= $signups_count . ' / ' . $draws_count ?><!--</div>-->
                </a>
            </div>
    <?php endforeach; ?>
</div>
</div>