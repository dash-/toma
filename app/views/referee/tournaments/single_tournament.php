<div class="container">
<div class="grid">
    <div class="row">
        <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
    </div>
    <div class="row">
        <div class="clearfix">

            <a class="metro button bigger yellow-col-bg right call br" data-title="Sponsors"
               href="/sponsors"><?= LangHelper::get('sponsors', 'Sponsors') ?></a>
        </div>
    </div>
    <div class="tournament-first">
        <div class="table-title tournament">
            <h3><?= LangHelper::get('tournament', 'Tournament').' - '.$tournament->title ?></h3>
            <span class="line"></span>
        </div>
    </div>
</div>
<div class="grid">
<div class="main-page">
<div class="column3-layout">
     <span class="column-title cl-red">

     </span>


    <div class="row">
        <div class="tiles tile-2 tile-red">
            <a href="<?= URL::to('signups/referee', $tournament->id) ?>?open=<?= $tournament->id?>" class="call">
                <div class="tile-icon">
                    <div class="signup-icon"></div>
                    <div class="draws-icon"></div>
                </div>
                <div class="tile-number"><?= $signups_count . ' / ' . $draws_count ?></div>
                <div class="tile-title">

                    <?= LangHelper::get('signups_and_draws', 'Signups & Draws') ?>
                    <span class="tile-title-line"></span>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <a href="<?= URL::to("players/index/".$tournament->id) ?>" data-title="Players and ranking"
           class="call tiles tile-2 tile-red">
            <div class="tile-icon">
                <div class="player-icon"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('players', 'Players')?>
                <span class="tile-title-line"></span>
            </div>
            <div class="tile-number"><?= $signups_count ?></div>
        </a>
    </div>
    <div class="row">
        <a href="<?= URL::to("rankings") ?>" class="call tiles tile-2 tile-yellow">
            <div class="tile-icon">
                <div class="ranking-icon"></div>
            </div>
            <div class="tile-title">
                <?= LangHelper::get('rankings', 'Rankings') ?>
                <span class="tile-title-line"></span>
            </div>
        </a>
    </div>
</div><!--
         --><div class="column3-layout">
    <span class="column-title cl-red"></span>
    <div class="row">

        <div class="tiles tile-1 tile-red">
            <a href="<?= $single_draw_id ? Url::to("/signups/list", $single_draw_id) : URL::to('referee/tournaments/list-of-draws', array($tournament->id, 'pay-pal')) ?>"
               class="call">
                <div class="tile-icon">
                    <div class="paypal-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('pay_pal', 'PayPal') ?>
                    <span class="tile-title-line"></span>
                </div>
            </a>
        </div>

        <div class="tiles tile-1 tile-red">

            <a target="_blank" href="<?= URL::route('default_public', $tournament->slug)?>">
                <div class="tile-icon">
                    <div class="publicpage-icon text-icon">www</div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('public_site', 'Public site')?>
                    <span class="tile-title-line"></span>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="tiles tile-2 tile-yellow">
            <a href="<?= URL::to('referee/tournaments/list-of-draws', array($tournament->id, 'show-schedule')) ?>"
               class="call">
                <div class="tile-icon">
                    <div class="orderofplay-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('schedule_view', 'Schedule view') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"></div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="tiles tile-2 tile-green">
            <a href="<?= Url::to("/schedule/index/".$tournament->id)?>"
               class="call">
                <div class="tile-icon">
                    <div class="court-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('schedule_administration', 'Schedule administration') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"></div>
            </a>
        </div>
    </div>


</div><!--
         --><div class="column3-layout">
    <span class="column-title cl-red"></span>

    <div class="row">

        <div class="tiles tile-2 tile-green">
            <div class="tile-icon">
                <div class="livescore-icon"></div>
                <div class="tile-text table-livescore">
                    <?= View::make("partials/live_score", array(
                        'live_score' => $live_score_main,
                    )) ?>
                </div>
            </div>
            <div class="tile-title">
                <a href="<?= URL::to('referee/tournaments/scores', $tournament->id) ?>" class="call">
                    <?= LangHelper::get('live_scores', 'Live Scores') ?>
                    <span class="tile-title-line"></span>
                </a>
            </div>
            <div class="tile-number"><?= count($live_score) ?></div>
        </div>
    </div>

    <div class="row">
        <div class="tiles tile-2 tile-yellow">
            <a href="<?= Url::to('scores/admin-view', $tournament->id)?>"
               class="call">
                <div class="tile-icon">
                    <div class="court-icon"></div>
                </div>
                <div class="tile-title">
                    <?= LangHelper::get('live_scores_administration', 'Live Scores administration') ?>
                    <span class="tile-title-line"></span>
                </div>
                <div class="tile-number"><?= count($live_score) ?></div>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="transparent-tile">
            <a href="<?= Url::to('/')?>" class="call back-button">
                <?= LangHelper::get('back_to_all_tournaments', 'Back to all tournaments')?>
            </a>
        </div>
    </div>
</div>

</div>
</div>
</div>
