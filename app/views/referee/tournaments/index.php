<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
       
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('tournaments', 'Tournaments')?></h3>
               <span class="line"></span>
             </div>
        </div>
        

        <div class="row pad-top20" ng-controller="TournamentsController">
            <accordion>
                <?php foreach ($tournaments as $tournament): ?>
                    <accordion-group is-open="<?= $tournament->id == Request::query('open')?>">
                        <accordion-heading>
                            <?= $tournament->title?> - <?= LangHelper::get('main_draw_starts_on', 'Main draw starts on')?>: <?= $tournament->getDate('main_draw_from')?> - <?= LangHelper::get('currently_signed_up', 'Currenly signed up')?>: <?= $tournament->signups->count()?>
                        </accordion-heading>

                        <table class="table bg-dark tournament-table">
                            <thead>
                            <tr>
                                <th class="text-left"><?= LangHelper::get('category', 'Category')?></th>
                                <th class="text-left"><?= LangHelper::get('type', 'Type')?></th>
                                <th class="text-left"><?= LangHelper::get('gender', 'Gender')?></th>
                                <th class="text-left"><?= LangHelper::get('total_acceptances', 'Total acceptances')?></th>
                                <th class="text-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size')?></th>
                                <th class="text-left"><?= LangHelper::get('unique_draw_id', 'Unique Draw ID')?></th>
                                <th class="text-left"><?= LangHelper::get('options', 'Options')?></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($tournament->draws as $draw): ?>
                                    <tr>
                                        <td class="pad-right50"><?= $draw->category->name?></td>
                                        <td class="pad-right50"><?= $draw->typeName()?></td>
                                        <td class="pad-right50"><?= $draw->genderName()?></td>
                                        <td class="pad-right50"><?= $draw->size->total?></td>
                                        <td class="pad-right50"><?= $draw->qualification->draw_size?></td>
                                        <td><?= $draw->unique_draw_id?></td>
                                        <!-- options -->
                                        <td class="transparent text-left">
                                            <a class="rfet-button call rfet-grey gap-right10" data-title="Draw details" href="<?= URL::to('referee/tournaments/draw-details', $draw->id)?>">
                                                Details
                                            </a>
                                            <?php if ($draw->refereeAppliedStatus(Auth::user()->id) == -1): ?>
                                                <a class="rfet-button rfet-grey" ng-click="apply($event, 'POST', 'You have successfully applied for draw', '<?= URL::to('referee/tournaments/apply', $draw->id)?>', 'Apply for this draw?', <?= $tournament->id?>)">
                                                    Apply for draw
                                                </a>
                                            <?php elseif ($draw->refereeAppliedStatus(Auth::user()->id) == 0): ?>
                                                <a class="rfet-button rfet-danger" ng-click="apply($event, 'POST', 'You have successfully canceled your application', '<?= URL::to('referee/tournaments/cancel', $draw->id)?>', 'Cancel application for this draw?', <?= $tournament->id?>)">
                                                    Cancel application
                                                </a>
                                            <?php elseif ($draw->refereeAppliedStatus(Auth::user()->id) == 1): ?>
                                                <span class="rfet-button rfet-success">Approved</span>
                                            <?php elseif ($draw->refereeAppliedStatus(Auth::user()->id) == 2): ?>
                                                <span class="rfet-button rfet-danger">Rejected</span>
                                            <?php elseif ($draw->refereeAppliedStatus(Auth::user()->id) == 3): ?>
                                                <span class="rfet-button rfet-danger">Canceled</span>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    </accordion-group>
                <?php endforeach ?>
            </accordion>

        </div>


    </div>

</div>

<script>
    angular.element(document).ready(function() {
        $("[data-role=dropdown]").dropdown();
        angular.bootstrap('.ang', ['refereeTournaments']);
    });
</script>