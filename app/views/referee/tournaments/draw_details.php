<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= $draw->tournament->title?> - <?= LangHelper::get('draw_details', 'Draw details')?></h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="tournament-second pad-top40">
            <div class="row players-info">
                   <div class="half">
                        <p><span class="color-rfet"><?= LangHelper::get('category', 'Category')?>:</span> <?= $draw->category->name ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('type', 'Type')?>:</span> <?= $draw->typeName() ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('gender', 'Gender')?>:</span> <?= $draw->genderName() ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('total_acceptances', 'Total acceptances')?>:</span> <?= $draw->size->total ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances')?>:</span> <?= $draw->size->direct_acceptances ?></p>                         
                    </div>
                    <div class="half">
                        <p><span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers')?>:</span> <?= $draw->size->accepted_qualifiers?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards')?>: </span><?= $draw->size->wild_cards ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts')?>: </span><?= $draw->size->special_exempts ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances')?>:</span> <?= $draw->size->onsite_direct ?></p>
                        <p><span class="color-rfet"><?= LangHelper::get('total_qualifiers', 'Total qualifiers')?>:</span> <?= $draw->size->onsite_direct ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
