<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
       
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('my_tournaments', 'My Tournaments')?></h3>
               <span class="line"></span>
             </div>
        </div>
        
     <div class="row pad-top20" ng-controller="TournamentsController">
            <accordion>
                <?php foreach ($mytour as $tour): ?>
                    <accordion-group is-open="<?= $tour->id == Request::query('open')?>">
                        <accordion-heading>
                            <?= $tour->title?> - <?= LangHelper::get('main_draw_starts_on', 'Main draw starts on')?>: <?= $tour->getDate('main_draw_from')?> - <?= LangHelper::get('currently_signed_up', 'Currenly signed up')?>: <?= $tour->signups->count()?>
                        </accordion-heading>

                        <table class="table bg-dark tournament-table">
                            <thead>
                            <tr>
                                <th class="text-left"><?= LangHelper::get('category', 'Category')?></th>
                                <th class="text-left"><?= LangHelper::get('type', 'Type')?></th>
                                <th class="text-left"><?= LangHelper::get('gender', 'Gender')?></th>
                                <th class="text-left"><?= LangHelper::get('total_acceptances', 'Total acceptances')?></th>
                                <th class="text-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size')?></th>
                                <th class="text-left"><?= LangHelper::get('unique_draw_id', 'Unique Draw ID')?></th>
                                <th class="text-left"><?= LangHelper::get('options', 'Options')?></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php foreach ($tour->draws as $draw): ?>
                                    <tr>
                                        <td class="pad-right50"><?= $draw->category->name?></td>
                                        <td class="pad-right50"><?= $draw->typeName()?></td>
                                        <td class="pad-right50"><?= $draw->genderName()?></td>
                                        <td class="pad-right50"><?= $draw->size->total?></td>
                                        <td class="pad-right50"><?= $draw->qualification->draw_size?></td>
                                        <td><?= $draw->unique_draw_id?></td>
                                        <!-- options -->
                                        <td class="transparent text-left">
                                            <a class="rfet-button call rfet-grey gap-right10" data-title="Draw details" href="<?= URL::to('referee/tournaments/draw-details', $draw->id)?>">
                                                Details
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    </accordion-group>
                <?php endforeach ?>
            </accordion>

        </div>

    </div>

</div>

<script>
    angular.element(document).ready(function() {
        $("[data-role=dropdown]").dropdown();
        angular.bootstrap('.ang', ['refereeTournaments']);
    });
</script>