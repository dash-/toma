<div class="grid">
    <div class="main-page">
        <div class="column3-layout">
            <span class="column-title cl-gd">
                <?= LangHelper::get('this_week', 'This Week');?>
            </span>

            <div class="row">
                <div class="tiles tile-2 tile-red">
                    <div class="tile-icon">
                        <div class="tile-text">
                            <?php foreach ($this_week->take(4) as $tournament): ?>
                                <p>
                                    <a class="call" href="<?= URL::to('tournaments/details', $tournament->tournament_id)?>">
                                        <?= Str::limit($tournament->title.', '.$tournament->club->province->region->name, 46)?>
                                    </a>
                                </p>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('tournaments/week', 1)?>" class="call" data-title="<?= LangHelper::get('tournaments_in_this_week', 'Tournaments in this week') ?>">
                            <?= LangHelper::get('tournaments', 'Tournaments')?>
                        </a>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= count($this_week)?></div>
                </div>
            </div>
            <div class="row">
                <div class="tiles tile-1 tile-green">
                    <a href="<?= URL::to('notifications')?>" class="call" data-title="Notifications">
                    <div class="tile-icon">
                            <div class="alert-icon"></div>
                        </div>
                        <div class="tile-title">
                            <?= LangHelper::get('alerts', 'Alerts')?>
                            <span class="tile-title-line"></span>
                        </div>
                        <div class="tile-number"><?= $alert_count?></div>
                    </a>
                </div>
                <div class="tiles tile-1 tile-blue">
                    <a href="<?= URL::to('contacts')?>" class="call" data-title="<?= LangHelper::get('contacts', 'Contacts') ?>">
                        <div class="tile-icon">
                            <div class="contact-icon"></div>
                        </div>
                        <div class="tile-title">
                            <?= LangHelper::get('contacts', 'Contacts')?>
                            <span class="tile-title-line"></span>
                        </div>
                        <div class="tile-number"><?= $contacts_count?></div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="tiles tile-2 tile-yellow">

                    <div class="tile-icon">
                        <div class="livescore-icon"></div>
                        <div class="tile-text table-livescore">
                            <?= (!$live_score) ? NULL : View::make("partials/live_score", array(
                                'live_score' => $live_score,
                            ))?>
                       </div>
                    </div>

                    <div class="tile-title">
                        <a href="<?= URL::to('scores')?>" data-title="<?= LangHelper::get('live_scores', 'Live Scores') ?>" class="call">
                        <?= LangHelper::get('live_scores', 'Live Scores')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= count($live_score) ?></div>
                </a>
                </div>
            </div>
        </div><!--
        --><div class="column3-layout">
            <span class="column-title cl-red">
                <?= LangHelper::get('upcoming', 'Upcoming')?>
            </span>

            <div class="row">
                <div class="tiles tile-2 tile-yellow">
                    <div class="tile-icon">
                        <table class="tile-text upcoming-tournaments-table">
                            <tr>
                                <?php $tournaments_count = TournamentHelper::calculateByWeeks('next week', 'count') ?>
                                <?php $sum_tournaments_count = $tournaments_count ?>
                                <td class="week">
                                    <a href="<?= URL::to('tournaments/week', 2)?>" class="call">
                                        <?= LangHelper::get('next_week', 'Next week')?>
                                    </a>
                                </td>
                                <td class="noplayers"><?= $tournaments_count ?></td>
                                <td class="referees"><?= LangHelper::get('referees', 'Referees')?></td>
                                <td class="noreferees"><?= TournamentHelper::countRefereesByWeeks("next week") ?>/<?= $tournaments_count ?></td>
                                <td class="tourn-status"><span class="st-green"></span></td>
                            </tr>
                            <tr>
                                <?php $tournaments_count = TournamentHelper::calculateByWeeks('+1 week', 'count') ?>
                                <?php $sum_tournaments_count += $tournaments_count ?>

                                <td>
                                    <a href="<?= URL::to('tournaments/week', 3)?>" class="call">
                                        <?= LangHelper::get('next_2_weeks', 'Next 2 Weeks')?>
                                    </a>
                                </td>
                                <td><?= $tournaments_count ?></td>
                                <td><?= LangHelper::get('referees', 'Referees')?></td>
                                <td><?= TournamentHelper::countRefereesByWeeks("+1 week") ?>/<?= $tournaments_count ?></td>
                                <td><span class="st-green"></span></td>
                            </tr>
                            <tr>
                                <?php $tournaments_count = TournamentHelper::calculateByWeeks('+2 weeks', 'count') ?>
                                <?php $sum_tournaments_count += $tournaments_count ?>

                                <td>
                                    <a href="<?= URL::to('tournaments/week', 4)?>" class="call">
                                        <?= LangHelper::get('next_3_weeks', 'Next 3 Weeks')?>
                                    </a>
                                </td>
                                <td><?= $tournaments_count?></td>
                                <td><?= LangHelper::get('referees', 'Referees')?></td>
                                <td><?= TournamentHelper::countRefereesByWeeks("+2 weeks") ?>/<?= $tournaments_count ?></td>
                                <td><span class="st-red"></span></td>
                            </tr>
                        </table>
                    </div>
                    <div class="tile-title">
                        <a class="call" href="<?= URL::to('tournaments/upcoming')?>" data-title="<?= LangHelper::get('upcoming_tournaments', 'Upcoming Tournaments') ?>">
                            <?= LangHelper::get('upcoming_tournaments', 'Upcoming Tournaments')?>
                            <span class="tile-title-line"></span>
                        </a>
                    </div>
                    <div class="tile-number"><?= $sum_tournaments_count ?></div>
                </div>
            </div>

            <div class="row">
                <div class="tiles tile-1 tile-red">
                    <a href="<?= URL::to('calendar')?>" data-title="<?= LangHelper::get('calendar', 'Calendar') ?>" class="call">
                        <div class="tile-icon">
                        <div class="calendar-icon"></div>

                        </div>
                        <div class="tile-title">
                        <?= LangHelper::get('calendar', 'Calendar')?>
                            <span class="tile-title-line"></span>
                        </div>
                        <div class="tile-number"><?= $calendar_count?></div>
                    </a>
                </div>

                <div class="tiles tile-1 tile-yellow">
                    <a href="<?= URL::to('invoices') ?>" data-title="<?= LangHelper::get('invoices', 'Invoices') ?>"
                       class="call">
                        <div class="tile-icon">
                            <div class="calendar-icon"></div>
                        </div>
                        <div class="tile-title">
                            <?= LangHelper::get('invoices', 'Invoices') ?>
                            <span class="tile-title-line"></span>
                        </div>
                        <div class="tile-number"><?= $invoices_count ?></div>
                    </a>
                </div>
            </div>

            <div class="row">
                <div class="tiles tile-2 tile-green">
                    <a href="<?= URL::to('signups')?>" data-title="<?= LangHelper::get('signups_and_draws', 'Signups & Draws') ?>" class="call">
                        <div class="tile-icon">
                            <div class="signup-icon"></div>
                            <div class="draws-icon"></div>
                        </div>
                        <div class="tile-title">
                            <?= LangHelper::get('signups_and_draws', 'Signups & Draws')?>
                            <span class="tile-title-line"></span>
                        </div>
                        <div class="tile-number"><?= $signups_count .' / '.$tournament_draws_count ?></div>
                    </a>
                </div>
            </div>
        </div><!--
        --><div class="column3-layout">
             <span class="column-title cl-red">
                <?= LangHelper::get('administration', 'Administration')?>
            </span>
            <?php if($isSuperAdmin): ?>
            <div class="row">
                <a href="<?= URL::to('regions') ?>" data-title="<?= LangHelper::get('regions', 'Regions') ?>" class="tiles tile-1 tile-yellow filled filled-gray2 call">
                    <div class="tile-icon">
                        <div class="regions-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('regions', 'Regions')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= $regions_count?></div>
                </a>

                <a href="<?= URL::to('referees') ?>" data-title="<?= LangHelper::get('referees', 'Referees') ?>" class="tiles tile-1 tile-yellow filled filled-gray2 call">
                    <div class="tile-icon">
                        <div class="referee-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('referees', 'Referees')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= $referees_count?></div>
                </a>
            </div>
            <?php endif; ?>
            <div class="row">
                <a href="<?= URL::to('clubs') ?>" data-title="<?= LangHelper::get('clubs', 'Clubs') ?>"  class="tiles tile-1 tile-yellow filled filled-gray1 call">
                    <div class="tile-icon">
                        <div class="club-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('clubs', 'Clubs')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= $clubs_count ?></div>
                </a>
                <a href="<?= route("tournaments") ?>" data-title="<?= LangHelper::get('tournaments', 'Tournaments') ?>" class="call tiles tile-1 tile-yellow filled filled-gray4">
                    <div class="tile-icon">
                        <div class="tournament-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('tournaments', 'Tournaments')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= $tournament_count?></div>
                </a>
            </div>
            <div class="row">

                <a href="<?= route("players") ?>" data-title="<?= LangHelper::get('players', 'Players') ?>" class="call tiles tile-1 tile-yellow filled filled-gray4">
                    <div class="tile-icon">
                        <div class="player-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('players', 'Players')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= $players_count?></div>
                </a>

                <a href="<?= URL::to("rankings") ?>" data-title="<?= LangHelper::get('rankings', 'Rankings') ?>" class="call tiles tile-1 tile-yellow filled filled-gray3">
                    <div class="tile-icon">
                        <div class="ranking-icon"></div>
                    </div>
                    <div class="tile-title">
                        <?= LangHelper::get('rankings', 'Rankings')?>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number" title="Ranking move"><?= $ranking_move_count?></div>
                </a>
                <?php if (Auth::getUser()->hasRole('regional_admin')): ?>
                <div class="row">
                <div class="tiles tile-2 tile-red">
                    <div class="tile-icon">
                    </div>
                    <div class="tile-title">
                        <a href="<?= URL::to('signups/draw-approval') ?>" data-title="<?= LangHelper::get('draws_waiting_for_approval', 'Draws waiting for approval') ?>">
                            <?= LangHelper::get('draws_waiting_for_approval', 'Draws waiting for approval')?>
                        </a>
                        <span class="tile-title-line"></span>
                    </div>
                    <div class="tile-number"><?= count($regional_draws)?></div>
                </div>
                </div>
                <?php endif ?>

            </div>
        </div>
    </div>

</div>
<!--<a class="tile double bg-lightBlue live call" href="--><?//= URL::to('superadmin/users')?><!--">-->
<!--    <div class="tile-content text" >-->
<!---->
<!--        <div class="data">-->
<!--            <span class="title fg-white">Users</span>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="brand">-->
<!--        <div class="label"><h3 class="no-margin fg-white"><span class="icon-mail"></span></h3></div>-->
<!--        <div class="badge">3</div>-->
<!--    </div>-->
<!--</a>-->
