<nav class="navbar navbar-default navbar-public" role="navigation">
    <div class="container pad0">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if (Auth::check()): ?>
                <a class="navbar-brand"
                   href="<?= (in_array(Auth::user()->getRole(), ['superadmin', 'referee', 'regional_admin', 'national_admin'])) ? URL::to('/') : URL::route('default_public', $tournament_slug) ?>">
                    <img class="logoNormal" src="/css/images/LogoNormal.png" width="90px"
                         title="Real Federación Española de Tenis" alt="Real Federación Española de Tenis"/>
                </a>
            <?php else: ?>
                <a class="navbar-brand" href="<?= URL::route('default_public', $tournament_slug) ?>">
                    <img class="logoNormal" src="/css/images/LogoNormal.png" width="90px"
                         title="Real Federación Española de Tenis" alt="Real Federación Española de Tenis"/>
                </a>
            <?php endif ?>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a class="<?= ($controller == 'tournament') ? 'active' : null ?>"
                       href="<?= URL::route('default_public', $tournament_slug) ?>">
                        <?= LangHelper::get('home', 'Home') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'schedule') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/schedule') ?>">
                        <?= LangHelper::get('order_of_play', 'Order of play') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'draws') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/draws') ?>">
                        <?= LangHelper::get('draws', 'Draws') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'scores') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/scores') ?>">
                        <?= LangHelper::get('scores', 'Scores') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'signups') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/signups') ?>">
                        <?= LangHelper::get('signups', 'Signups') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'calendar') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/calendar') ?>">
                        <?= LangHelper::get('calendar', 'Calendar') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'news') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/news') ?>">
                        <?= LangHelper::get('news', 'News') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'player') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/player/search') ?>">
                        <?= LangHelper::get('search', 'Search') ?></a>
                </li>
                <li>
                    <a class="<?= ($controller == 'rankings') ? 'active' : null ?>"
                       href="<?= URL::to($tournament_slug . '/rankings') ?>">
                        <?= LangHelper::get('rankings', 'Rankings') ?>
                    </a>
                </li>
                <?php if (Auth::check() && Auth::user()->isAdministrator()): ?>
                    <li>
                        <a href="<?= URL::to($tournament_slug . '/message-box') ?>">
                            <?= LangHelper::get('message_box_admin', 'Message box admin') ?>
                        </a>
                    </li>
                <?php endif;?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if (Auth::check()): ?>
                    <li><a href="#"><?= $logged_user->username ?></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <?php if (Auth::user()->isAdministrator()): ?>
                                <li>
                                    <a href="<?= URL::to($tournament_slug . '/news-admin') ?>">
                                        <?= LangHelper::get('news_administration', 'News administration') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= URL::to($tournament_slug . '/gallery-admin') ?>">
                                        <?= LangHelper::get('gallery_administration', 'Gallery administration') ?>
                                    </a>
                                </li>
                            <?php endif ?>
                            <li>
                                <a href="<?= URL::to($tournament_slug . '/user/logout') ?>">
                                    <?= LangHelper::get('logout', 'Logout') ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= URL::to('/') ?>">
                                    <?= LangHelper::get('Main site', 'Main site') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="<?= URL::to($tournament_slug . '/user/login') ?>">
                            <?= LangHelper::get('login', 'Login') ?> / <?= LangHelper::get('signup', 'Signup') ?>
                        </a>
                    </li>

                <?php endif ?>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
