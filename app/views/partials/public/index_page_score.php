<table width="100%" class="table live-score-public">

    <thead>
    <tr>
        <th width="15%"><?= LangHelper::get('court', 'Court') ?></th>
        <th width="33%"><?= LangHelper::get('player', 'Player') ?> 1</th>
        <th width="33%"><?= LangHelper::get('player', 'Player') ?> 2</th>
        <th width="25%" class="text-right"><?= LangHelper::get('score', 'Score') ?></th>
    </tr>
    </thead>
    <tbody>


    <?php foreach ($scores as $score): ?>
        <tr>
            <td>
                <?= $score->court_number ?>
            </td>

            <td>
                <a href="<?= URL::to($tournament_slug . '/player/info/' . $score->match->team1_data->id) ?>">
                    <?= substr($score->match->team1_data->name, 0, 1) . '. ' . \DrawMatchHelper::shorten($score->match->team1_data->surname) ?>
                </a>
            </td>
				<td>
					<a href="<?= URL::to($tournament_slug.'/player/info/'.$score->match->team2_data->id)?>">
						<?= substr($score->match->team2_data->name,0,1).'. '.\DrawMatchHelper::shorten($score->match->team2_data->surname) ?>
					</a>
				</td>


            <td class="text-right" id="score_<?= $score->match_id ?>">
                <?php foreach ($score->match->scores as $result): ?>
                    <?= $result->set_team1_score . \DrawMatchHelper::matchTie($result->set_team1_tie)
                    . '-' . $result->set_team2_score . \DrawMatchHelper::matchTie($result->set_team2_tie) ?>
                <?php endforeach ?>
            </td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>

