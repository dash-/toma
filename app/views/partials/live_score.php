<div class="carousel live-score-carousel">
    <?php foreach ($live_score as $score): ?>
        <div class="slide">
            <div>
                <div class="tournament-name">
                    <?= Str::limit($score->tournament->title . ', ' . $score->tournament->club->province->province_name, 35); ?>
                </div>
            </div>
            <?php if (isset($score->match) && !is_null($score->match)): ?>
                <div>
                    <div class="player1">
                        <?= DrawMatchHelper::shortSinglePlayerName($score->match->team1_data)?>
                    </div>
                    <div class="vs">vs</div>
                    <div class="player2">
                        <?= DrawMatchHelper::shortSinglePlayerName($score->match->team2_data) ?>
                    </div>
                    </br>
                </div>
                <div>

                    <div class="result live_score_result" id="score_<?= $score->match_id ?>">
                        <?= TournamentScoresHelper::getLiveScoreStatus($score) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    <?php endforeach ?>
</div>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>