<style type="text/css" media="print">
@page
{
    size: 11in 8.5in;
}

.gracket-holder {
    display: block;
}

</style>

<link rel="stylesheet" type="text/css" media="print" href="/css/bracket-print.css" />
<div class="container ang">
    <div class="grid" ng-controller="bracketController">
        
        <div class="row gracket-holder">
            
            <div id="gracket" data-gracket='<?= $jsArray?>'></div>
            <!-- <img id="gracket-img"> -->
        </div>
        
        <div class="row">
            <input type="button" class="btn btn-primary showFullScreen" fullscreen-body=".gracket-holder" value="<?= LangHelper::get('show_fullscreen', 'SHOW FULLSCREEN')?>" />
            <input type="button" class="btn btn-primary printBrackets" value="<?= LangHelper::get('print_brackets', 'Print Brackets')?>" />
        </div>
        <div class="row">
            <div class="gracket-box">
                <h3><?= LangHelper::get('seeded_players', 'Seeded players')?></h3>
                <?php foreach ($seeds as $key => $seed): ?>
                    <p class="fg-white">
                        <?= $key+1?>. <?= DrawMatchHelper::getDoublesPair($seed->teams)?> - <?= LangHelper::get('rank', 'Rank')?>: <?= (!$draw->manual_draw) ? $seed->rank : $seed->signup->rank?>
                    </p>
                <?php endforeach ?>
            </div>
            <?php if (count($lucky_losers)): ?>
                <div class="gracket-box">
                    <h3><?= LangHelper::get('draw_info', 'Draw info')?></h3>
                    <?php foreach ($lucky_losers as $lucky): ?>
                        <?php if ($lucky->move_from_qualifiers_type == 3): ?>

                            <p class="fg-white">
                                <?= LangHelper::get('lucky_loser', 'Lucky loser')?>: <?= DrawMatchHelper::getDoublesPair($lucky->teams)?>
                            </p>
                        
                        <?php elseif ($lucky->move_from_qualifiers_type == 2): ?>
                        
                            <p class="fg-white">
                                <?= LangHelper::get('qualifiers', 'Qualifiers')?>: <?= DrawMatchHelper::getDoublesPair($lucky->teams)?>
                            </p>

                        <?php endif ?>
                    <?php endforeach ?>
                </div>
            <?php endif ?>
            <div class="gracket-box place-right">
                <h3><?= $tournament->club->club_name?></h3>
                <?php if ($tournament->club->image_path): ?>
                    <img class="club-image" src="<?= $tournament->club->image()?>">
                <?php endif ?>
            </div>

        </div>

        <div class="row clear pad-bottom20">
            <!-- Modal template -->
            <script type="text/ng-template" id="myModalContent.html">
                <?= $results_form_view?>
            </script>
            
        </div>
    </div>
</div>


<script src="/js/jquery.gracket.js"></script>
<script src="/js/ang/bracket.js"></script>
   
<script>
    
$(document).ready(function() {
    $("#gracket").gracket({
        canvasLineWidth : 1,      // adjusts the thickness of a line
//        canvasLineGap : 0,        // adjusts the gap between element and line
        cornerRadius : 0,         // adjusts edges of line
        canvasLineCap : "square",  // or "square"
        canvasLineColor : "#fff", // or #HEX
        isDoubles: <?= $isDoubles ? "true" : "false" ?>
    });
    $(".g_gracket").width($(".g_winner").width()+$(".g_winner").position().left+70);
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['bracket']);
    });

    // quick fix must be redone
    <?php if($scroll_to = Request::query('scrollTo')):?>
        $("html, body").animate({ scrollTop: $('#<?= $scroll_to?>').offset().top - 100 }, 400);
    <?php endif?>

});
</script>