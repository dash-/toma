<div class="carousel live-score-carousel">

    <?php foreach ($tournament->schedules as $schedule): ?>
    <div class="slide">
        <div>
            <div class="tournament-name livescore_box_text">
                <?= Str::limit($tournament->title . ', ' . $tournament->club->province->province_name, 35); ?>
            </div>
        </div>
            <?php if (isset($schedule->match) && !is_null($schedule->match)): ?>
                <div>
                    <div class="player1 livescore_box_text">
                        <?= $schedule->match && $schedule->match->team1_data ? substr($schedule->match->team1_data->name, 0, 1) . '. ' . DrawMatchHelper::shorten($schedule->match->team1_data->surname) : ''?>
                    </div>
                    <div class="vs">vs</div>
                    <div class="player2 livescore_box_text">
                        <?= $schedule->match && $schedule->match->team2_data ? substr($schedule->match->team2_data->name, 0, 1) .
                        '. ' . DrawMatchHelper::shorten($schedule->match->team2_data->surname) : '' ?>
                    </div>
                    </br>
                </div>
                <div>

                    <div class="result live_score_result livescore_box_text" id="score_<?= $schedule->match->id ?>">
                        <?= TournamentScoresHelper::getLiveScoreStatus($schedule) ?>
                    </div>
                </div>

            <?php endif ?>
</div>
        <?php endforeach ?>
</div>