<table class="table bg-dark tournament-table">
    <thead>
        <tr>
            <th width="40%" class="text-left">Tournament</th>
            <th class="text-left">Match contestants</th>
            <th class="text-right">Current result</th>
            <?php if(!Auth::user()->hasRole('player')): ?>
                <th class="text-right"><?= LangHelper::get('options', 'Options')?></th>
            <?php endif ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($in_progress as $score): ?>
            <tr>
                <td class="text-left"><?= $score->tournament->title.' - '.$score->match->draw->category->name ?></td>
                <td class="text-left contestants">
                    <?= $score->match && $score->match->team1_data ? substr($score->match->team1_data->name,0,1).'. '.DrawMatchHelper::shorten($score->match->team1_data->surname) : '' ?>
                <div class="vs live">vs</div>
                    <?= $score->match && $score->match->team2_data ? substr($score->match->team2_data->name,0,1).'. '.DrawMatchHelper::shorten($score->match->team2_data->surname) : '' ?>
            </td>
                <td class="text-right scores" id="score_<?= $score->match->id?>">
                <?php foreach($score->match->scores as $result): ?>

                <?= $result->set_team1_score.DrawMatchHelper::matchTie($result->set_team1_tie).'-'.$result->set_team2_score.DrawMatchHelper::matchTie($result->set_team2_tie).'; '?>
                <?php echo '' ?>

                <?php endforeach ?>
                    <?= DrawMatchHelper::ShowIfRetired($score->match) ?>

                </td>
                <?php if (Auth::user()->id==$score->tournament->referee_id || Auth::user()->hasRole("regional_admin") || Auth::user()->hasRole("superadmin")): ?>
                    <td class="rfet-yellow" style="width:6%"> <a href="/match/match-live-mode/<?= $score->match_id ?>"> Edit Score </a>
                </td>
                    
                <?php endif ?>
                
            </tr>

        <?php endforeach ?>  
    </tbody>
</table>