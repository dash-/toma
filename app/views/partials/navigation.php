<nav class="navigation-bar rfet">
    <nav class="navigation-bar-content">
        <h1 class="page-title">
            <a class="call" data-title="RFET" href="/">
                <img class="logoNormal" src="/css/images/TomaLogoNormal.png" title="Tennis Organization & Management Application" alt="Tennis Organization & Management Application" />
            </a>
        </h1>
		<div class="element user-logged place-right">
            <a class="dropdown-toggle" href="#">
                <?= Auth::user()->email?>
            </a>
            <ul class="dropdown-menu place-right" data-role="dropdown">
            <?php if (Auth::getUser()->getRole('name') == 'superadmin'): ?>
                <li><a class="call" data-title="Users" href="<?= URL::to('superadmin/users')?>"><?= LangHelper::get('users', 'Users')?></a></li>
                <li><a class="call" data-title="Users" href="<?= URL::to('external/index')?>"><?= LangHelper::get('external_tournaments', 'External Tournaments')?></a></li>
            <?php endif ?>
                <li><a class="call" data-title="Profile" href="<?= URL::to('profile')?>"><?= LangHelper::get('profile', 'Profile')?></a></li>
                <li><a class="call" data-title="Messages" href="<?= URL::to('messages')?>"><?= LangHelper::get('messages', 'Messages')?> (<?= Auth::getUser()->countMessages()?>)</a></li>
                <li><a class="call" data-title="Settings" href="<?= URL::to('settings')?>"><?= LangHelper::get('settings', 'Settings')?></a></li>
                <li><a href="<?= URL::route('logout'); ?>"><?= LangHelper::get('logout', 'Logout')?></a></li>
            </ul>
        </div>
        <a class="videos-link place-right call" href="/videos">?</a>
    </nav>
</nav>