<input type="hidden" id="is_mobile" value="<?= $is_mobile ?>"/>
<div class="container ang <?= $is_mobile ? "mobile_app" : "" ?>" ng-cloak>
    <div ng-controller="liveModeController">
        <div class="grid">
            <div class="row">
                <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
            </div>
        </div>

        <div class="row" ng-show="showWarningMsg">
            <p class="fg-white text-center">
                <?= LangHelper::get('live_mode_warning', 'There is no schedule appointed to this match please add match to schedule') ?>
            </p>
        </div>

        <div class="right-part live-mode-scoring" ng-init="init(<?= $match->id ?>)" ng-show="!showWarningMsg">
            <div class="buttons" ng-show="matchStatus == -1">
                Loading...
            </div>
            <div class="buttons live_mode_start_match" ng-show="matchStatus < 3 && matchStatus >= 0">

                <button ng-click="startTheMatch(<?= $match->id ?>)">
                    Not started. Start the match.
                </button>
            </div>
            <div class="scoring" ng-show="matchStatus == 3">
                <div class="time_of_playing" ng-if="match.match_started_at != null">
                    Started at <span>{{ match.match_started_at | asDateTime }}</span><br/>
                    Time playing: <span>{{ counter }}</span>
                </div>
                <div class="middle">
                    <h3>
                        <?= $tournament->title ?>
                    </h3>
                    <br/>

                    <div class="game-score">
                        <?= $match->team1_data ? $match->team1_data->name . ' ' . $match->team1_data->surname : '' ?>
                    </div>
                    <div class="game-score">
                        <?= $match->team2_data ? $match->team2_data->name . ' ' . $match->team2_data->surname : '' ?>
                    </div>
                </div>
                <div class="middle">
                    <div class="tie_break_enabled" ng-show="tieBreakEnabled">
                        Tie break playing in progress
                    </div>
                    <div class="game-score team1-game-score">
                        <div class="add_remove_score_holder">
                            <div class="add_score" ng-disabled="button_hold" ng-click="addScore(1)">+</div>
                        </div>

                        <div class="number_holder">{{ gamescore.currentScoreTeam11 }}</div>
                        <div class="number_holder">{{ gamescore.currentScoreTeam12 }}</div>
                        <div class="add_remove_score_holder">
                            <div class="remove_score" ng-disabled="button_hold" ng-show="removing == 1" ng-click="removeScore(1)">-</div>
                        </div>
                    </div>
                    <div class="separator">:</div>
                    <div class="game-score team2-game-score">
                        <div class="add_remove_score_holder">
                            <div class="add_score" ng-disabled="button_hold" ng-click="addScore(2)">+</div>
                        </div>
                        <div class="number_holder">{{ gamescore.currentScoreTeam21 }}</div>
                        <div class="number_holder">{{ gamescore.currentScoreTeam22 }}</div>
                        <div class="add_remove_score_holder">
                            <div class="remove_score" ng-disabled="button_hold" ng-show="removing == 2" ng-click="removeScore(2)">-</div>
                        </div>
                    </div>
                </div>
                <div class="middle">
                    <div class="game-score">
                        <div class="input-control radio default-style gap-right10">
                            <label>
                                <input type="radio" name="server" ng-model="serving" value="1"/>
                                <span class="check"></span>
                        <span class="radio-name">
                            <?= LangHelper::get('serve', 'Serve') ?>
                        </span>
                            </label>
                        </div>
                    </div>
                    <div class="game-score">
                        <div class="input-control radio default-style gap-left20">
                            <label>
                                <input type="radio" name="server" ng-model="serving" value="2"/>

                                <span class="check"></span>

                        <span class="radio-name">
                            <?= LangHelper::get('serve', 'Serve') ?>
                        </span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="middle">
                    <table class="set_points">
                        <tr>
                            <td class="player_name"></td>
                            <td ng-class="setInProgress == 1 ? 'active' : '' " class="set_point">Set 1</td>
                            <td ng-class="setInProgress == 2 ? 'active' : '' " class="set_point">Set 2</td>
                            <td ng-class="setInProgress == 3 ? 'active' : '' " class="set_point">Set 3</td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 4 ? 'active' : '' "
                                class="set_point">Set 4
                            </td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 5 ? 'active' : '' "
                                class="set_point">Set 5
                            </td>
                        </tr>
                        <tr>
                            <td><?= $match->team1_data ? $match->team1_data->surname : '' ?></td>
                            <td ng-class="setInProgress == 1 ? 'active' : '' ">{{ setPoints.team1[0] }} {{
                                tiePoints.team1[0] | tiebreakscore}}
                            </td>
                            <td ng-class="setInProgress == 2 ? 'active' : '' ">{{ setPoints.team1[1] }} {{
                                tiePoints.team1[1] | tiebreakscore }}
                            </td>
                            <td ng-class="setInProgress == 3 ? 'active' : '' ">{{ setPoints.team1[2] }} {{
                                tiePoints.team1[2] | tiebreakscore }}
                            </td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 4 ? 'active' : '' ">{{
                                setPoints.team1[3] }} {{ tiePoints.team1[3] | tiebreakscore }}
                            </td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 5 ? 'active' : '' ">{{
                                setPoints.team1[4] }} {{ tiePoints.team1[4] | tiebreakscore }}
                            </td>
                        </tr>
                        <tr>
                            <td><?= $match->team2_data ? $match->team2_data->surname : '' ?></td>
                            <td ng-class="setInProgress == 1 ? 'active' : '' ">{{ setPoints.team2[0] }} {{
                                tiePoints.team2[0] | tiebreakscore }}
                            </td>
                            <td ng-class="setInProgress == 2 ? 'active' : '' ">{{ setPoints.team2[1] }} {{
                                tiePoints.team2[1] | tiebreakscore }}
                            </td>
                            <td ng-class="setInProgress == 3 ? 'active' : '' ">{{ setPoints.team2[2] }} {{
                                tiePoints.team2[2] | tiebreakscore }}
                            </td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 4 ? 'active' : '' ">{{
                                setPoints.team2[3] }} {{ tiePoints.team2[3] | tiebreakscore }}
                            </td>
                            <td ng-show="draw_rules.match_rule == 1" ng-class="setInProgress == 5 ? 'active' : '' ">{{
                                setPoints.team2[4] }} {{ tiePoints.team2[4] | tiebreakscore }}
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="middle">
                    <br/>
                    <br/>
                    <br/>
                    <button class="btn button rfet-success" ng-click="finishSet()">
                        Finish current set
                    </button>
                    <br/>
                    <button class="btn button rfet-danger" ng-click="finishMatch()">
                        FINISH MATCH
                    </button>
                </div>
            </div>
            <div class="buttons time_of_playing" ng-show="matchStatus == 4">
                Finished at {{ finished_at | asDateTime }} <br/>
            </div>
        </div>

        <div class="modal_holder" ng-show="show_popup">
            <div class="modal_content">
                Match is finished.

                <br/>
                <br/>
                <br/>

                <button class="btn button rfet-danger" ng-click="finishMatch()">
                    FINISH THE MATCH AND STOP TIMING
                </button>
            </div>
        </div>
    </div>
</div>


<script src="/js/ang/liveModeApp.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['liveModeApp']);
    });

    $(function(){
        if($("#is_mobile").val()==true){
            $(".navigation-bar.rfet").slideUp();
        }
    })
</script>
