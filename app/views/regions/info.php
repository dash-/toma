<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('region_info', 'Region info') ?></h3>
                <span class="line"></span>
            </div>

            <div class="clearfix gap-top30">
                <a class="rfet-button rfet-yellow call" data-title="Edit region"
                   href="/regions/edit/<?= $regions->id ?>">
                    <?= LangHelper::get('edit', 'Edit') ?>
                </a>
                <a class="rfet-button rfet-yellow call" data-title="Edit region position"
                   href="/regions/position/<?= $regions->id ?>">
                    <?= LangHelper::get('position', 'Position') ?>
                </a>
            </div>
        </div>
        <div class="tournament-second pad-top40">
            <div class="row players-info region-info">
                <div class="half">
                    <table>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('name', 'Name') ?>:
                            </td>
                            <td> <?= $regions->region_name ?> </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('full_name', 'Full name') ?>:
                            </td>
                            <td> <?= $regions->full_name ?> </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('phone', 'Phone') ?>:
                            </td>
                            <td> <?= $regions->phone ?> </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('fax', 'Fax') ?>:
                            </td>
                            <td> <?= $regions->fax ?> </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('president', 'President') ?>:
                            </td>
                            <td> <?= $regions->president ?> </td>
                        </tr>
                    </table>

                </div>
                <div class="half">
                    <table>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('address', 'Address') ?>:
                            </td>
                            <td>
                                <?= $regions->address ?>
                            </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('email', 'Email') ?>:
                            </td>
                            <td>
                                <a href="mailto: <?= $regions->email ?>"><?= $regions->email ?></a>
                            </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('web', 'Web') ?>:
                            </td>
                            <td>
                                <a href="<?= $regions->web ?>"><?= $regions->web ?></a>
                            </td>
                        </tr>

                        <tr>
                            <td class="color-rfet">
                                <?= LangHelper::get('working_hours', 'Working hours') ?>:
                            </td>
                            <td>
                                <?= $regions->working_hours ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clearfix gap-top20">
                <div id="map-canvas" class="club-map"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var marker;
    var map;
    function initialize() {
        <?php if(!is_null($regions->position_lat)): ?>
        showMap(<?= doubleval($regions->position_lat) ?>, <?= doubleval($regions->position_long) ?>, 17);
        <?php else: ?>

        new google.maps.Geocoder().geocode(
            {
                'address': "SPAIN"
            }, function (r, s) {
                if (s == google.maps.GeocoderStatus.OK) {
                    showMap(r[0].geometry.location.k, r[0].geometry.location.B, 7);
                }

            });
        <?php endif; ?>
    }

    function showMap(lat, lng, zoom) {
        if (lat != undefined && lng != undefined) {
            var mapOptions = {
                center: {lat: lat, lng: lng},
                zoom: zoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map($('#map-canvas').get(0),
                mapOptions);

            marker = new google.maps.Marker({
                map: map,
                position: {lat: lat, lng: lng},
                icon: "/css/icons/regions.png",
                draggable: false
            });
            google.maps.event.addListener(marker, 'dragend', function () {
                $("#latitude").val(marker.getPosition().k);
                $("#longitude").val(marker.getPosition().B);
            });
            var infowindow = new google.maps.InfoWindow({
                content: "<div class='map-tooltip'><?= addslashes($regions->region_name) ?></br><?= addslashes($regions->full_city) ?></br><?= addslashes($regions->address) ?></br></div>"
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>