<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('update_region', 'Update region')?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="tournament-second">
            <form name="editForm" ng-class="{ 'show-errors': showErrors }"
            ng-submit="formSubmit($event, 'PUT', '<?= LangHelper::get('region_successfully_updated', 'Region successfully updated')?>','<?= Request::url() ?>')"
            ng-controller="FormController" novalidate>

<!--             <div class="notice marker-on-bottom bg-white" ng-show="errors.length > 0">
                <p class="fg-black" ng-repeat="problem in errors track by $index">{{problem}}</p>
            </div> -->

            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
                <div class="row">
                    <div class="half">
                        <label for="name"><?= LangHelper::get('name', 'Name')?></label>

                        <div class="input-control text">
                            <input type="text" name="name" ng-model="formData.regions.region_name"
                            ng-init="formData.regions.region_name='<?= $region->region_name ?>'" required>
                        </div>
                    </div>
                    <div class="half">
                        <label for="name"><?= LangHelper::get('full_name', 'Full name')?></label>

                        <div class="input-control text">
                            <input type="text" name="full_name" ng-model="formData.regions.full_name"
                            ng-init="formData.regions.full_name='<?= $region->full_name ?>'">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <label for="name"><?= LangHelper::get('president', 'President')?></label>

                        <div class="input-control text">
                            <input type="text" name="president" ng-model="formData.regions.president"
                            ng-init="formData.regions.president='<?= $region->president ?>'">
                        </div>
                    </div>
                    <div class="half">
                        <label for="name"><?= LangHelper::get('address', 'Address')?></label>

                        <div class="input-control text">
                            <input type="text" name="address" ng-model='formData.regions.address'
                            ng-init='formData.regions.address="<?= HTML::entities($region->address) ?>"' >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <label for="name"><?= LangHelper::get('phone', 'Phone')?></label>

                        <div class="input-control text">
                            <input type="text" name="phone" ng-model="formData.regions.phone"
                            ng-init="formData.regions.phone='<?= $region->phone ?>'" >
                        </div>
                    </div>
                    <div class="half">
                        <label for="name"><?= LangHelper::get('fax', 'Fax')?></label>

                        <div class="input-control text">
                            <input type="text" name="fax" ng-model="formData.regions.fax"
                            ng-init="formData.regions.fax='<?= $region->fax ?>'" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <label for="name"><?= LangHelper::get('email', 'Email')?></label>

                        <div class="input-control text">
                            <input type="text" name="email" ng-model="formData.regions.email"
                            ng-init="formData.regions.email='<?= $region->email ?>'" >
                        </div>
                    </div>
                    <div class="half">
                        <label for="name"><?= LangHelper::get('web', 'Web')?></label>

                        <div class="input-control text">
                            <input type="text" name="web" ng-model="formData.regions.web"
                            ng-init="formData.regions.web='<?= $region->web ?>'" >
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-left:1%; width:99%;">

                    <label for="name"><?= LangHelper::get('working_hours', 'Working hours')?></label>

                    <div class="input-control text">
                        <input type="text" name="working_hours" ng-model="formData.regions.working_hours"
                        ng-init="formData.regions.working_hours='<?= $region->working_hours ?>'" >
                    </div>

                </div>
                <button type="submit" 
                class="clear place-right primary large gap-top10"><?= LangHelper::get('save', 'Save')?>
            </button>
            <form-errors></form-errors>

        </fieldset>
    </form>
</div>
</div>
</div>

<script>


angular.element(document).ready(function() {
    angular.bootstrap('.ang', ['genericFormApp']);
});
</script>