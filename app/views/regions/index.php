<div class="container">
    <div class="grid">

        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('regions', 'Regions')?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="regions_holder">
            <?php foreach ($regions as $region): ?>
                <div class="region_holder <?= ($region->image_path == '') ? "image-not-set" : "" ?>">
                    <a class=" view_region " data-title="<?= $region->region_name?>" href="/regions/info/<?= $region->id ?>">
                        <img src="/uploads/regions/<?= $region->image_path ?>" alt="Image not set"/>
                        <div class="region_name"><?= $region->region_name ?></div>
                    </a>
                    <div class="region_edit_button">
                        <a class="edit-link call" data-title="Edit region" href="/regions/edit/<?= $region->id ?>">
                            <?= LangHelper::get('edit_region', 'Edit Region')?>
                        </a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>


</div>