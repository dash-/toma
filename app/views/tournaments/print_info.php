<div class="metro" style="width: auto" ng-cloak>
<div class="modal-body text-center">
   <h3>{{tournament.title}}</h3>

   <a class="clear fg-white" target="_blank" href="{{tournament.url}}">{{tournament.url}}</a>

    <div class="gap-top20">
        <img class="text-center" src="data:image/png;base64, {{tournament.image}} ">
    </div>

</div>
</div>

