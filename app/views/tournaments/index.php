<div class="container ang">
    <div class="grid" ng-controller="tModalController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <a class="metro button bigger yellow-col-bg right next-black-arrow call" data-title="Add tournament"
                   href="<?= route('tournaments/create') ?>">
                    <?= LangHelper::get('submit_new_tournament', 'Submit New Tournament') ?>
                </a>

                <a class="metro button bigger yellow-col-bg right call br" data-title="Sponsors"
                   href="/sponsors">
                    <?= LangHelper::get('sponsors', 'Sponsors') ?>
                </a>
                <?php if (Auth::user()->hasRole('superadmin')): ?>
                <a class="metro button bigger yellow-col-bg right call br" href="/tournaments/tournament-types">
                    <?= LangHelper::get('tournament_types', 'Tournament types') ?>
                </a>
                <?php endif ?>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= isset($head_title) ? $head_title : (LangHelper::get('tournaments', 'Tournaments')) ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row filter-row">

            <div class="clearfix search-form-holder">
                <form action="<?= URL::current() ?>" method="GET">
                    <input type="hidden" name="search" value="1">

                    <div class="form-control place-left">
                        <div class="input-control text">
                            <input type="text" name="title" placeholder="Enter title"
                                   value="<?= Request::query('title') ?>">
                        </div>
                    </div>

                    <div class="form-control place-left">
                        <?= Form::select('year', DateTimeHelper::getYears(), $year, array('class' => 'select2')); ?>
                    </div>

                    <div class="form-control place-left">
                        <?= Form::select('month', DateTimeHelper::getMonths(), $month, array('class' => 'select2')); ?>
                    </div>

                    <div class="form-control place-left" style="width: 200px">
                        <?= Form::select('status', TournamentHelper::getStatuses(), $status, array('class' => 'select2')); ?>
                    </div>

                    <div class="form-control submit-type place-left tournamentButtonIndex">
                        <button type="submit" class="button clear primary large dark">
                            <?= LangHelper::get('search', 'Search') ?>
                        </button>
                    </div>
                    <div class="form-control submit-type place-right">
                        <a href="<?= URL::current() ?>" class="button clear primary large">
                            <?= LangHelper::get('reset_search', 'Reset search') ?>
                        </a>
                    </div>
                </form>
            </div>

        </div>
        <div class="row tabScrl">
            <div class="table-scrollable tournamentIndexResponsive">

                <table class="table bg-dark tournament-table tournamentIndexResponsiveScroll">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('start_date', 'Start date') ?></th>
                        <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                        <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                            <th class="text-left"><?= LangHelper::get('submited_by', 'Submited by') ?></th>
                        <?php endif ?>
                        <th class="text-left"><?= LangHelper::get('surface', 'Surface') ?></th>
                        <th class="text-left"><?= LangHelper::get('organizer', 'Organizer') ?></th>
                        <th class="text-left"><?= LangHelper::get('status', 'Status') ?></th>
                        <th class="text-left"><?= LangHelper::get('courts', 'Courts') ?></th>
                        <th class="text-right" width="<?= Auth::user()->hasRole('referee') ? '45%' : '35%' ?>">Options
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($tournaments as $tournament): ?>
                        <tr>
                            <td style="<?= ($tournament->status == 5) ? 'border-left: 3px solid #449d44' : null ?>">
                                <?= DateTimeHelper::GetShortDateFullYear($tournament->date->main_draw_from) ?>
                            </td>
                            <td><?= $tournament->title ?></td>
                            <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                                <td>
                                    <?= ($tournament->creator())
                                        ? $tournament->creator()->info("name") . " " . $tournament->creator()->info("surname")
                                        : false ?>
                                </td>
                            <?php endif ?>
                            <td><?= ($tournament->surface) ? $tournament->surface->name : null ?></td>
                            <td><?= $tournament->organizer->name ?></td>
                            <td><?= $tournament->status() ?></td>
                            <td><?= $tournament->number_of_courts ?></td>
                            <!-- options -->
                            <td class="transparent text-right">
                                <?php if ($tournament->status == 5 && $tournament->validated): ?>
                                    <a class="rfet-button rfet-success gap-right5" target="_blank"
                                           href="/tournaments/tournament-invoice/<?= $tournament->id ?>?his_ignore=true"><?= LangHelper::get('invoices', 'Invoices') ?></a>

                                <?php endif ?>
                                <?php if (isset($show_tournament_settings_button) && $show_tournament_settings_button): ?>
                                    <a class="rfet-button rfet-success gap-right5 call"
                                       href="<?= URL::to('referee/tournaments/tournament', $tournament->id) ?>">
                                        <?= LangHelper::get("view_tournament_settings", "View tournament settings") ?>
                                    </a>
                                <?php endif; ?>

                                <a class="rfet-button rfet-yellow gap-right5"
                                   href="<?= URL::to('tournaments/draws', $tournament->id) ?>">
                                    <?= LangHelper::get('draws', 'Draws') ?> (<?= $tournament->draws()->count() ?>)
                                </a>

                                <?php if (($tournament->status == 1 AND Auth::user()->hasRole('superadmin'))): ?>
                                    <a class="rfet-button rfet-danger gap-right5 call" data-title="Tournament details"
                                       href="/tournaments/details/<?= $tournament->id ?>">
                                        <?= LangHelper::get('needs_action', 'Needs action') ?>
                                    </a>
                                <?php elseif (!$tournament->validated): ?>
                                    <a class="rfet-button rfet-danger gap-right5 call" data-title="Tournament details"
                                       href="/tournaments/details/<?= $tournament->id ?>">
                                        <?= LangHelper::get('not_valid', 'Not valid') ?>
                                    </a>
                                <?php endif ?>

                                <a class="button yellow-col-bg d-small" data-title="Tournament details"
                                   href="/tournaments/details/<?= $tournament->id ?>">
                                    <?= LangHelper::get('details', 'Details') ?>
                                    <?php if (Auth::user()->hasRole('superadmin') OR Auth::user()->hasRole('referee')): ?>
                                        <?php if ($tournament->status != 2 AND $tournament->status != 5): ?>
                                            <span class="not-approved-label">Not <br> approved</span>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </a>

                                <?php if (!PermissionHelper::checkPagePermission($tournament)): ?>
                                    <a class="button yellow-col-bg d-small" data-title="Edit tournament"
                                       href="/tournaments/edit/<?= $tournament->id ?>">
                                        <?= LangHelper::get('edit', 'Edit') ?>
                                    </a>
                                <?php endif ?>

                                <div class="button-dropdown text-left">
                                    <button
                                        class="dropdown-toggle public-toggle rfet-yellow"><?= LangHelper::get('public_site', 'Public site') ?></button>
                                    <ul class="dropdown-menu" data-role="dropdown">
                                        <li>
                                            <a target="_blank"
                                               href="<?= URL::route('default_public', $tournament->slug) ?>">
                                                <?= LangHelper::get('visit_site', 'Visit site') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a ng-click='openModal("/tournaments/print-info", "<?= $tournament->id ?>")'>
                                                <?= LangHelper::get('print_info', 'Print info') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>


        <div class="pagination text-center">
            <?php if (!isset($role) || ((isset($role) && $role != 'referee'))): ?>
                <?= (Request::query()) ? $tournaments->appends(Request::query())->links() : $tournaments->links() ?>
            <?php endif; ?>
        </div>

        <!-- Modal template -->
        <script type="text/ng-template" id="qrContent.html">
            <?= isset($print_info_view) ? $print_info_view : "" ?>
        </script>

    </div>

</div>

<script src="/js/ang/modalApp.js"></script>
<script>
    $(document).ready(function () {
        $("select").select2({});
        $("[data-role=dropdown]").dropdown();
    });
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['tModalApp']);
    });
</script>