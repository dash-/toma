<div class="container ang">
    <div class="grid">
       <form name="createForm" ng-submit="formSubmit($event, 'POST', 'Tournament successfully submitted')" action="<?= Request::url()?>" ng-controller="FormController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="tournament-first">
                        <div class="table-title tournament">
                            <h3>Create New Tournaments</h3>

                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="tournament-second">
                        <div class="form-control">
                            <label for="title">Title</label>
                            <div class="input-control text">
                                <input type="text" name="title" ng-model="formData.tournament.title" required>
                            </div>
                        </div>
                        <div class="form-control">
                            <label for="name">Surface</label>
                            <!--<select id="surface_id" ng-controller="TournamentsController" ng-required="true" ng-model="formData.tournament.surface_id" data-placeholder="Choose Surface">
                                <option value=""></option>
                                <option ng-class="ang-select" ng-repeat="surface in surfaces" ng-selected="surface.selected">{{surface.name}}</option>
                            </select>-->
                            <?= Form::select('surface_id', $surfaces_array, NULL, array('required','ng-model' => 'formData.tournament.surface_id'));?>
                        </div>

                        <div class="row">
                            <div class="half">
                                <div class="form-control">
                                    <label for="name">Main draw from</label>
                                    <div class="input-control text">
                                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.main_draw_from" is-open="opened['mainFrom']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'mainFrom')" formatdate>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control padd">
                                    <label for="name">Main draw to</label>
                                    <div class="input-control text">
                                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.main_draw_to" is-open="opened['mainTo']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'mainTo')" formatdate>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="half">
                                <div class="form-control">
                                    <label for="name">Qualifying date from</label>
                                    <div class="input-control text">
                                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.qualifying_date_from" is-open="opened['qualifyingFrom']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'qualifyingFrom')" formatdate>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control padd">
                                    <label for="name">Quafilifying date to</label>
                                    <div class="input-control text">
                                        <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.qualifying_date_to" is-open="opened['qualifyingTo']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'qualifyingTo')" formatdate>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="half">
                                    <div class="form-control">
                                        <label for="name">Entry deadline</label>
                                        <div class="input-control text">
                                            <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.entry_deadline" is-open="opened['entryDeadline']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'entryDeadline')" formatdate>
                                        </div>
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="form-control padd">
                                        <label for="name">Withdrawal deadline</label>
                                        <div class="input-control text">
                                            <input type="text" datepicker-popup="dd.MM.yyyy" ng-model="formData.date.withdrawal_deadline" is-open="opened['withdrawalDeadline']" show-button-bar="false" ng-required="true" ng-click="openDatePicker($event, 'withdrawalDeadline')" formatdate>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tournament-first">
                        <div class="table-title tournament">
                            <h3>Organizer information</h3>
                            <span class="line"></span>
                        </div>
                    </div>
                    <div class="tournament-second marginTop-10">
                        <div class="row">
                            <div class="half">
                                <div class="form-control">
                                    <label for="name">Name</label>
                                    <div class="input-control text">
                                        <input type="text" name="name" ng-model="formData.organizer.name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control padd">
                                    <label for="email">Email</label>
                                    <div class="input-control text">
                                        <input type="text" name="email" ng-model="formData.organizer.email" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <div class="form-control">
                                    <label for="phone">Phone</label>
                                    <div class="input-control text">
                                        <input type="text" name="phone" ng-model="formData.organizer.phone">
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control padd">
                                    <label for="fax">Fax</label>
                                    <div class="input-control text">
                                        <input type="text" name="fax" ng-model="formData.organizer.fax">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <div class="form-control">
                                    <label for="website">Website</label>
                                    <div class="input-control text">
                                        <input type="text" name="website" ng-model="formData.organizer.website">
                                    </div>
                                </div>
                            </div>

                            <button type="submit" ng-disabled="!createForm.$valid" class="save-button-margin-top clear place-right primary large dark gap-top50">Save</button>

                        </div>
                    </div>
                    <div class="row">
                        <div class="input-control back-in-pagination">

                            <a class="back-button-margin button primary yellow back big call gap-top50" data-title="Tournaments" href="/tournaments">BACK</a>


                        </div>
                    </div>
                </fieldset>
            </form>
    </div>
</div>

<script>

    $(document).ready(function() {
        $("select").select2({});
        $("datepicker-popup")
    });

    angular.element(document).ready(function() {
            angular.bootstrap('.ang', ['genericFormApp']);
    });

</script>

