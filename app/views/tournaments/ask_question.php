<div class="metro" style="width: auto">
<div class="modal-body">
    <form name="createForm" ng-submit="formSubmit('POST', 'Message successfully sent','<?= URL::to('messages/create')?>', $event)" novalidate>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <fieldset>
            <div class="form-control">
                <label class="modal-sendto-label">
                    <?= LangHelper::get('to', 'To')?>: 
                    <?php if ($tournament->creator()): ?>
                        <?php if ($tournament->creator()->username != Auth::user()->username): ?>
                            <?= $tournament->creator()->username ?>
                            <input type="hidden" ng-model="formData.messages.user_id" ng-init="formData.messages.user_id='<?= ($tournament->creator()) ? $tournament->creator()->id : false?>'">
                        <?php else: ?>
                            <?= $tournament->getSuperadmin()->username?>
                            <input type="hidden" ng-model="formData.messages.user_id" ng-init="formData.messages.user_id='<?= $tournament->getSuperadmin()->id?>'">
                        <?php endif ?>
                    <?php else: ?>
                        N/A
                    <?php endif ?>
                </label>
            </div>

            <div class="form-control">
                <!-- <label for="name">Subject</label> -->
                <div class="input-control text">
                    <input type="text" ng-model="formData.messages.subject" ng-init="formData.messages.subject='<?= $tournament->title?> question'" class="modal-subject">
                </div>
            </div>

            <div class="form-control">
                <!-- <label for="name">Message</label> -->
                <div class="input-control textarea">
                    <textarea type="text" required ng-model="formData.messages.text" placeholder="Message"></textarea>
                </div>
            </div>
            <button type="submit" class="button big next-yellow-arrow yellow-color dark-col-bg gap-top30"><?= LangHelper::get('send', 'Send')?></button>
            <a class="button big yellow-color no-col-bg right text-right gap-top30" ng-click="cancel()"><?= LangHelper::get('cancel', 'Cancel')?></a>
        </fieldset>
    </form>
</div>
</div>