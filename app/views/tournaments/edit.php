<div class="container ang">
    <div class="grid" ng-cloak>
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('tournament_information', 'Tournament Information') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <form name="editForm" ng-class="{ 'show-errors': showErrors }"
              ng-submit="formSubmit($event, 'PUT', '<?= LangHelper::get('tournament_successfully_updated', 'Tournament successfully updated') ?>' ,'<?= Request::url() ?>', true)"
              ng-controller="FormController" novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
                <div class="tournament-second">


                    <?php if (Auth::user()->hasRole('superadmin')): ?>
                        <div class="row">
                            <div style="margin-left: 1%; width: 99%;">
                                <div class="form-control">
                                    <label for="name"><?= LangHelper::get('status', 'status') ?> *</label>
                                    <?= Form::select('status', $status_array, $tournament->status, array('ng-model' => 'formData.tournament.status',
                                        'ng-init' => "formData.tournament.status='" . $tournament->status . "'")); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="title"><?= LangHelper::get('title', 'Title') ?> *</label>

                                <div class="input-control text" style="margin-left: 1%; width: 99%;">
                                    <input type="text" name="title" ng-model="formData.tournament.title"
                                           ng-init="formData.tournament.title='<?= $tournament->title ?>'" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for=""><?= LangHelper::get('tournament_type', 'Tournament type') ?> *</label>

                                <div class="input-control text">
                                    <?= Form::select('tournament_type', $tournament_types, NULL, array('ng-model' => 'formData.tournament.tournament_type')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('surface', 'Surface') ?></label>
                                <?= Form::select('surface_id', $surfaces_array, NULL, array('ng-model' => 'formData.tournament.surface_id',
                                    'ng-init' => "formData.tournament.surface_id='" . $tournament->surface_id . "'")); ?>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('club', 'Club') ?> *</label>
                                <?= Form::select('club_id', $clubs_array, $tournament->club_id, array('required', 'ng-model' => 'formData.tournament.club_id', 'ng-init' => "formData.tournament.club_id='" . $tournament->club_id . "'")); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="has_qualifying"><?= LangHelper::get('has_qualifying', 'Has qualifying?') ?></label>
                                <?= Form::select('has_qualifying', $qualiy_q, $tournament->has_qualifying, array('ng-model' => 'formData.tournament.has_qualifying', 'ng-init' => "formData.tournament.has_qualifying='" . $tournament->has_qualifying . "'")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-hide="formData.tournament.has_qualifying == 0">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('qualifying_date_from', 'Qualifying date from') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="qualifying_date_from"
                                           datepicker-popup="dd/MM/yyyy" ng-model="formData.date.qualifying_date_from"
                                           is-open="opened['qualifyingFrom']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'qualifyingFrom')"
                                           ng-init="formData.date.qualifying_date_from='<?= DateTimeHelper::GetSpanishShortDateFullYear($tournament->date->qualifying_date_from) ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('qualifying_date_to', 'Qualifying date to') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="qualifying_date_to"
                                           datepicker-popup="dd/MM/yyyy"
                                           ng-model="formData.date.qualifying_date_to"
                                           is-open="opened['qualifyingTo']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'qualifyingTo')"
                                           ng-init="formData.date.qualifying_date_to='<?= DateTimeHelper::GetSpanishShortDateFullYear($tournament->date->qualifying_date_to) ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('main_draw_start', 'Main draw start') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="main_draw_start"
                                           datepicker-popup="dd/MM/yyyy" ng-model="formData.date.main_draw_from"
                                           is-open="opened['mainFrom']"
                                           show-button-bar="false" ng-required="true"
                                           ng-click="openDatePicker($event, 'mainFrom')"
                                           ng-init="formData.date.main_draw_from='<?= DateTimeHelper::GetSpanishShortDateFullYear($tournament->date->main_draw_from) ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control padd">
                                <label for="name"><?= LangHelper::get('main_draw_finish', 'Main draw finish') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="main_draw_finish"
                                           datepicker-popup="dd/MM/yyyy"
                                           ng-model="formData.date.main_draw_to"
                                           is-open="opened['mainTo']" show-button-bar="false" ng-required="true"
                                           ng-click="openDatePicker($event, 'mainTo')"
                                           ng-init="formData.date.main_draw_to='<?= DateTimeHelper::GetSpanishShortDateFullYear($tournament->date->main_draw_to) ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('entry_deadline', 'Entry deadline') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="entry_deadline"
                                           datepicker-popup="dd/MM/yyyy"
                                           ng-model="formData.date.entry_deadline"
                                           is-open="opened['entryDeadline']" show-button-bar="false"
                                            ng-click="openDatePicker($event, 'entryDeadline')"
                                           ng-init="formData.date.entry_deadline='<?= ($tournament->date->entry_deadline) ? DateTimeHelper::GetShortDateFullYear($tournament->date->entry_deadline) : null ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="name"><?= LangHelper::get('withdrawal_deadline', 'Withdrawal deadline') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="withdrawal_deadline"
                                           datepicker-popup="dd/MM/yyyy"
                                           ng-model="formData.date.withdrawal_deadline"
                                           is-open="opened['withdrawalDeadline']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'withdrawalDeadline')"
                                           ng-init="formData.date.withdrawal_deadline='<?= ($tournament->date->withdrawal_deadline) ? DateTimeHelper::GetShortDateFullYear($tournament->date->withdrawal_deadline) : null ?>'"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="name"><?= LangHelper::get('number_of_courts', 'Number of courts') ?></label>

                                <div class="input-control text">
                                    <input type="text" required ng-model="formData.tournament.number_of_courts"
                                           ng-init="formData.tournament.number_of_courts='<?= $tournament->number_of_courts ?>'">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="sponsor_id"><?= LangHelper::get('general_sponsor', 'General sponsor') ?></label>
                                <?= Form::select('sponsor_id', $sponsors_array, NULL, array('ng-model' => 'formData.sponsor.sponsor_id', 'ng-init' => "formData.sponsor.sponsor_id='" . $tournament->getSponsor('id') . "'")); ?>

                            </div>
                        </div>
                    </div>

                    <div class="row gap-bottom20">
                        <div class="half">
                            <div class="form-control">
                                <label for="referee_id_real"><?= LangHelper::get('referee', 'Referee') ?> *</label>
                                <input type="hidden" name="referee_id_real" id="referee_id_real"
                                       value="<?= ($tournament->referee_id_real) ?: 0 ?>"
                                       ng-model="formData.tournament.referee_id_real"
                                       ng-init="formData.tournament.referee_id_real=<?= ($tournament->referee_id_real) ?: 0 ?>"
                                       data-text="<?= $referee_name ?>">
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for=""><?= LangHelper::get('player_representative', 'Player representative') ?></label>

                                <div class="input-control text">
                                    <input type="text"
                                           ng-init="formData.tournament.player_representative='<?= $tournament->player_representative ?>'"
                                           ng-model="formData.tournament.player_representative">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row gap-bottom20">
                        <div class="full">
                            <div class="form-control">
                                <label for=""><?= LangHelper::get('official_ball', 'Official ball') ?></label>

                                <div class="input-control text">
                                    <input type="text"
                                           ng-init="formData.tournament.official_ball='<?= $tournament->official_ball ?>'"
                                           ng-model="formData.tournament.official_ball">
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="tournament-first">
                    <div class="table-title tournament">
                        <h3><?= LangHelper::get('organizer_information', 'Organizer information') ?></h3>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="tournament-second marginTop-10">
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="name" ng-model="formData.organizer.name"
                                           ng-init="formData.organizer.name='<?= $tournament->organizer->name ?>'"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" name="email" ng-model="formData.organizer.email"
                                           ng-init="formData.organizer.email='<?= $tournament->organizer->email ?>'">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="phone" ng-model="formData.organizer.phone"
                                           ng-init="formData.organizer.phone='<?= $tournament->organizer->phone ?>'">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="fax"><?= LangHelper::get('fax', 'Fax') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="fax" ng-model="formData.organizer.fax"
                                           ng-init="formData.organizer.fax='<?= $tournament->organizer->fax ?>'">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="website"><?= LangHelper::get('website', 'Website') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="website" ng-model="formData.organizer.website"
                                           ng-init="formData.organizer.website='<?= $tournament->organizer->website ?>'">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="save-button-margin-top clear place-right primary large dark"
                                style="margin-top: 40px !important;"><?= LangHelper::get('save', 'Save') ?></button>


                    </div>
                </div>


                <form-errors></form-errors>
            </fieldset>
        </form>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('select').select2();
        $('#referee_id_real').select2({
            initSelection: function (element, callback) {
                var result = [];
                var id = $(element).val();
                var text = $(element).data('text');
                result.push({
                    id: id,
                    text: text
                });

                callback(result[0]);
            },
            ajax: {
                url: "/referees/search-referees",
                dataType: 'json',
                delay: 350,
                data: function (params) {
                    return {
                        q: params
                    };
                },
                results: function (data) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            minimumInputLength: 1
        });
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>