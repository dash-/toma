<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= (LangHelper::get('tournament_types', 'Tournament types')) ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <div class="table-scrollable ">
                <table class="table bg-dark tournament-table table-scrollable-w768">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                        <th class="text-left"><?= LangHelper::get('tournament_category', 'Tournament category') ?></th>
                        <th class="text-left"><?= LangHelper::get('coeficient', 'Coeficient') ?></th>
                        <th class="text-right" width="25%"><?= LangHelper::get('options', 'Options') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($types as $type): ?>
                        <tr>
                            <td><?= $type->type ?></td>
                            <td><?= $type->category_tournament ?></td>
                            <td><?= $type->coeficient ?></td>
                            <td class="transparent text-right">
                                <a class="rfet-button rfet-yellow call" href="/tournaments/edit-type/<?= $type->id ?>">
                                    <?= LangHelper::get('edit', 'Edit') ?>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>

        </div>
    </div>
</div>