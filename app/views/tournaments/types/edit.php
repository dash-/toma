<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= (LangHelper::get('tournament_types_edit', 'Tournament types - Edit')) ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row" ng-controller="TypeEditController" ng-cloak>

            <form name="editForm" novalidate ng-submit="typeSubmit($event, '<?= Request::url()?>')">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset class="half">
                    <div class="form-control">
                        <label><?= LangHelper::get('type', 'Type')?></label>
                        <div class="input-control text">
                            <input type="text" ng-model="formData.type.type"
                                   ng-init="formData.type.type='<?= $type->type ?>'" required>
                        </div>
                    </div>

                    <div class="form-control">
                        <label><?= LangHelper::get('tournament_category', 'Tournament category')?></label>
                        <div class="input-control text">
                            <input type="text" ng-model="formData.type.category_tournament"
                                   ng-init="formData.type.category_tournament='<?= $type->category_tournament?>'" required>
                        </div>
                    </div>

                    <div class="form-control">
                        <label><?= LangHelper::get('coeficient', 'Coeficient')?></label>
                        <div class="input-control text">
                            <input type="text" ng-model="formData.type.coeficient"
                                   ng-init="formData.type.coeficient='<?= $type->coeficient?>'" required>
                        </div>
                    </div>
                    <button type="submit" class="button primary big call next"><?= LangHelper::get('save', 'Save')?></button>
                </fieldset>
            </form>

        </div>
    </div>
</div>

<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>