<div class="row">

    <div class="table-title tournament" style="padding-left: 50%;margin-top: -55px;">
        <h3>{{formData.tournament.title}} - <?= LangHelper::get('draws', 'Draws') ?></h3>
        <span class="line"></span>
    </div>
    <div class="tabScrl">
        <table class="table bg-dark tournament-table gap-top20 table-scrollable-w900" ng-show="drawsData.length > 0">
            <thead>
            <tr>
                <th class="text-left"><?= LangHelper::get('category', 'Category') ?></th>
                <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                <th class="text-left"><?= LangHelper::get('gender', 'Gender') ?></th>
                <th class="text-left"><?= LangHelper::get('main_draw_size', 'Main draw size') ?></th>
                <th class="text-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?></th>
                <th class="text-right" width="20%"><?= LangHelper::get('options', 'Options') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="row in drawsData track by $index">
                <td class="pad-right100">{{ replaceText(row.draws.draw_category_id, drawCategories) }}</td>
                <td class="pad-right100">{{ replaceText(row.draws.draw_type, {singles:'Singles', 'doubles': 'Doubles'})
                    }}
                </td>
                <td class="pad-right100">{{ replaceText(row.draws.draw_gender, {m:'Male', 'f': 'Female'}) }}</td>
                <td class="pad-right100">{{ row.sizes.total }}</td>
                <td class="pad-right100">{{ row.qualifications.draw_size }}</td>
                <td class="transparent text-right">
                    <a ng-click="makeCopy(row, $index)"
                       class="rfet-button rfet-yellow"><?= LangHelper::get('make_copy', 'Make copy') ?></a>
                    <a ng-click="removeDraw(drawsData, $index)"
                       class="rfet-button rfet-danger"><?= LangHelper::get('remove', 'Remove') ?></a>
                </td>
            </tr>
        </table>
    </div>
    <!-- form submit start -->
    <div class="row">
        <div ng-show="hide_drawadd">
            <a class="button big rfet-yellow place-left" style="margin-top:27px" ng-click="showDrawAdd()">
                <?= LangHelper::get('add_draw', 'Add draw') ?>
            </a>
            <button type="submit" class="button big rfet-yellow place-right" style="margin-top: 26px"
                    ng-click="withoutDraw()">
                <?= LangHelper::get('finish_without_draw', 'Finish without draw') ?>
            </button>
        </div>
        <fieldset ng-hide="hide_drawadd">
            <div class="tournament-second paddingLeftStep2 respPadLeft">
                <div class="row">
                    <div class="half">
                        <div class="form-control"
                             ng-class="{ 'has-error' : drawclicked == true && createForm.draw_category_id.$invalid }">
                            <label for="title"><?= LangHelper::get('category', 'Category') ?> *</label>

                            <div class="input-control select">
                                <?= Form::select('draw_category_id', $categories, "{{formData.draws.draw_category_id}}", array('required', 'ng-model' => 'formData.draws.draw_category_id', 'id' => 'draw_category_id')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="half">
                        <div class="form-control"
                             ng-class="{ 'has-error' : drawclicked == true && createForm.draw_type.$invalid }">
                            <label for="title"><?= LangHelper::get('type', 'Type') ?> *</label>

                            <div class="input-control select">
                                <?= Form::select('draw_type', $types, NULL, array('required', 'ng-model' => 'formData.draws.draw_type', 'id' => 'draw_type')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <div class="form-control"
                             ng-class="{ 'has-error' : drawclicked == true && createForm.draw_gender.$invalid }">
                            <label for="title"><?= LangHelper::get('gender', 'Gender') ?> *</label>

                            <div class="input-control select">
                                <?= Form::select('draw_gender', $genders, NULL, array('required', 'ng-model' => 'formData.draws.draw_gender', 'id' => 'draw_gender')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="half">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.total.$invalid }">
                                <label><?= LangHelper::get('total_acceptance', 'Total acceptance') ?> *</label>

                                <div class="input-control select custom-width">
                                    <?= Form::select('total', $total_acceptance, NULL, array('required', 'ng-model' => 'formData.sizes.total', 'id' => 'total_acceptance')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.prize_pool.$invalid }">
                                <label><?= LangHelper::get('prize_pool', 'Prize pool') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="prize_pool" ng-model="formData.draws.prize_pool" required class="only-number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <div class="form-control"
                             ng-class="{ 'has-error' : drawclicked == true && createForm.draw_gender.$invalid }">
                            <label for="title"><?= LangHelper::get('is_this_pre_qualifying_draw', 'Is this pre qualifying draw?') ?> *</label>
                            <div class="input-control select">
                                <?= Form::select('is_pre_qualifying', $prequali_q, 0, array( 'ng-model' => 'formData.is_prequalifying')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="break-draw">
                <div class="row">
                    <div class="half">
                        <div class="half">
                            <div class="form-control">
                                <label for="draw_size"><?= LangHelper::get('number_of_seeds', 'Number of seeds') ?>
                                    *</label>

                                <div class="input-control text" style="width: 96%;">
                                    <input type="text" class="only-number" ng-model="formData.draws.number_of_seeds">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.direct_acceptances.$invalid }">
                                <label
                                    for="direct_acceptances"><?= LangHelper::get('direct_acceptances', 'Direct acceptances') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" class="only-number" name="direct_acceptances"
                                           ng-model="formData.sizes.direct_acceptances" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="half">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.accepted_qualifiers.$invalid }">
                                <label
                                    for="accepted_qualifiers"><?= LangHelper::get('accepted_qualifiers', 'Qualifiers') ?>
                                    *</label>

                                <div class="input-control text" style="width: 96%;">
                                    <input type="text" class="only-number" name="accepted_qualifiers"
                                           ng-disabled="formData.tournament.has_qualifying == 0 || formData.is_prequalifying == 1"
                                           ng-model="formData.sizes.accepted_qualifiers" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.draw_size.$invalid }">
                                <label
                                    for="draw_size"><?= LangHelper::get('size_of_qualifying_draw', 'Size of Qualifying Draw') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" class="only-number" name="draw_size" ng-model="formData.qualifications.draw_size"
                                           ng-disabled="formData.tournament.has_qualifying == 0 || formData.is_prequalifying == 1"
                                           required app-type="number">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <div class="half">
                            <div class="form-control">
                                <label for="wild_cards"><?= LangHelper::get('wild_cards', 'Wild cards') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <input type="text" class="only-number" name="wild_cards" id="wild_cards"
                                           ng-model="formData.sizes.wild_cards">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="special_exempts"><?= LangHelper::get('special_exempts', 'Special exempts') ?></label>

                                <div class="input-control text">
                                    <input type="text" class="only-number" name="special_exempts" ng-model="formData.sizes.special_exempts" disabled ng-disabled="true">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="half">
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="onsite_direct"><?= LangHelper::get('onsite_direct', 'Onsite direct') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <?= Form::select('onsite_direct', $onsite_direct, 0, array('required', 'ng-model' => 'formData.sizes.onsite_direct', 'id' => 'onsite_direct')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="has_final_round"><?= LangHelper::get('quali_to_finals', 'Qualifications has final round') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <?= Form::select('plays_to_finals', $quali_has_final_round, NULL, array('ng-model' => 'formData.qualifications.has_final_round')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half" style="width: 68%">
                        <div class="half">
                            <div class="form-control">
                                <label for="match_rules"><?= LangHelper::get('match_rules', 'Match rules') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <?= Form::select('match_rule', $match_rules, 0, array('required', 'ng-model' => 'formData.draws.match_rule', 'id' => 'match_rule')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="match_rules"><?= LangHelper::get('set_rules', 'Set rules') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <?= Form::select('set_rule', $set_rules, 0, array('required', 'ng-model' => 'formData.draws.set_rule', 'id' => 'set_rule')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="half pull-right" style="width: 30%">
                        <div class="form-control">
                            <label for="match_rules"><?= LangHelper::get('game_rules', 'Game rules') ?></label>

                            <div class="input-control text" style="width: 96%;">
                                <?= Form::select('game_rule', $game_rules, 0, array('required', 'ng-model' => 'formData.draws.game_rule', 'id' => 'game_rule')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="half">
                        <div class="form-control">
                            <a class="button clear place-left primary medium" ng-click="loadStep()"
                               style="margin-top:27px"><?= LangHelper::get('back', 'Back') ?></a>

                        </div>
                    </div>
                    <div class="half">
                        <div class="form-control">
                            <a class="button place-left primary medium" style="margin-top:27px" ng-click="addDraw()">
                                <?= LangHelper::get('add_draw', 'Add draw') ?>
                            </a>
                            <button type="submit" class="button big rfet-yellow place-right" style="margin-top: 26px">
                                <?= LangHelper::get('finish', 'Finish') ?>
                            </button>

                            <button type="submit" class="button big rfet-yellow place-right" style="margin-top: 26px" ng-hide="drawAdded"
                                    ng-click="withoutDraw()">
                                <?= LangHelper::get('finish_without_draw', 'Finish without draw') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="input-control back-in-pagination">
                    <a class="back-button-margin button primary yellow back big call" ng-click="loadStep()"
                       style="margin-top: -70px !important;"><?= LangHelper::get('tournaments', 'TOURNAMENTS') ?></a>
                </div>
            </div>


        </fieldset>
    </div>

    <div class="custom-errors">
        <a class="ns-close ng-cloak" id="closeError2" ng-show="drawclicked == true"></a>

        <p ng-if="drawclicked == true && createForm.draw_category_id.$error.required"><?= LangHelper::get('category_is_required', 'Category is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.draw_type.$error.required"><?= LangHelper::get('type_is_required', 'Type is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.draw_gender.$error.required"><?= LangHelper::get('gender_is_required', 'Gender is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.total.$error.required"><?= LangHelper::get('total_acceptence_is_required', 'Total acceptance is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.prize_pool.$error.required"><?= LangHelper::get('prize_pool_is_required', 'Prize Pool is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.direct_acceptances.$error.required"><?= LangHelper::get('direct_acceptances_is_required', 'Direct Acceptances') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.accepted_qualifiers.$error.required"><?= LangHelper::get('accepted_qualifiers_is_required', 'Qualifiers is required') ?>
            .</p>

        <p ng-if="drawclicked == true && createForm.draw_size.$error.required"><?= LangHelper::get('total_qualifiers_is_required', 'Total qualifiers is required') ?>
            .</p>
    </div>

    <div class="custom-warnings" ng-if="Sum1Error == true">
        <span class="yellow-color"><?= LangHelper::get('warning', 'Warning') ?>: </span>
    <span
        class="yellow-color"><?= LangHelper::get('total_sum_of_direct_and_qualify_and_wild_card', 'Sum of Direct acceptances, Qualifiers, Wild card and Special Exempts must be equal to Total Acceptances') ?>
        !</span>
    </div>
