<div class="container ang">
    <div class="grid" ng-controller="actionsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix button-bar">
                <?php if (Auth::user()->id != $tournament->created_by AND !Auth::user()->hasRole('superadmin')):?>
                    <a class="metro button bigger yellow-col-bg right" ng-click="open()">
                        <?= LangHelper::get('ask_a_question', 'Ask a question') ?>
                    </a>
                <?php endif?>


                <?php if (DrawMatchHelper::checkIfScheduleExist($tournament->id, 1)): ?>

                    <a class="metro button bigger yellow-col-bg right br pad-top10"
                       href="/schedule/show/<?= $tournament->id ?>?list_type=1">
                        <?= LangHelper::get('main_show_tournament_schedule', 'Main - Show tournament schedule') ?>
                    </a>

                <?php endif ?>

                <?php if (DrawMatchHelper::checkIfScheduleExist($tournament->id, 2)): ?>
                    <a class="metro button bigger yellow-col-bg right br pad-top10"
                       href="/schedule/show/<?= $tournament->id ?>?list_type=2">
                        <?= LangHelper::get('qualifications_show_tournament_schedule', 'Qualifications - Show tournament schedule') ?>
                    </a>

                <?php endif ?>

                <?php if (PermissionHelper::checkAdminPermission($tournament)): ?>
                    <a class="metro button bigger yellow-col-bg right br"
                       href="<?= Url::to('/tournaments/edit', $tournament->id) ?>">
                        <?= LangHelper::get('edit_tournament', 'Edit tournament') ?>
                    </a>
                <?php endif ?>

                <?php if ($tournament->status == 5): ?>
                    <div class="row gap-top15">
                        <a class="metro button big yellow-col-bg right br" style="margin-top: -15px;"
                           href="/tournaments/tournament-invoice/<?= $tournament->id ?>"><?= LangHelper::get('pay_tournament', 'Pay tournament') ?></a>
                    </div>
                <?php endif ?>

                 <?php if ($tournament->status == 5): ?>
                    <div class="row">
                        <a class="metro button bigger yellow-col-bg right br" style="margin-top: -20px;"
                           href="/tournaments/per-club-invoice/<?= $tournament->id ?>"><?= LangHelper::get('per_club_invoice', 'Per club invoice') ?></a>
                    </div>
                <?php endif ?>

            </div>
        </div>
        <div class="tournament-first">
            <div class="tournament">

                <h3><?= LangHelper::get('tournament_details', 'Tournament details') ?></h3>
                <span class="line"></span>
                <h3><a href="/tournaments/draws/<?= $tournament->id ?>" class="rfet-button rfet-yellow">   Open draws</a></h3>
            </div>

            <div class="tournament tour-info tournament_details_sponsor">
                <h3 class="extra-info pad-top70"><?= LangHelper::get('organizer', 'ORGANIZER') ?>:</h3>
                <span class="line"></span>

                <h3 class="extra-info pad-top20"><?= $tournament->organizer->name ?></h3>

                <h3 class="extra-info"><?= LangHelper::get('email', 'Email') ?>
                    : <?= $tournament->organizer->email ?></h3>

                <h3 class="extra-info"><?= LangHelper::get('phone', 'Phone') ?>
                    : <?= $tournament->organizer->phone ?></h3>

                <h3 class="extra-info"><?= LangHelper::get('fax', 'Fax') ?>: <?= $tournament->organizer->fax ?></h3>

                <h3 class="extra-info"><?= LangHelper::get('club_website', 'Club website') ?>
                    : <br/>
                    <a class="link" href="<?= $tournament->organizer->website ?>" title="<?= $tournament->organizer->website ?>">
                        <?= Str::limit($tournament->organizer->website, 40) ?>
                    </a>
                </h3>

                <h3 class="extra-info"><?= LangHelper::get('public_site', 'Tournament site') ?>:
                    <br/>
                    <a class="link" href="<?= URL::route('default_public', $tournament->slug)?>" >
                        <?= Str::limit( URL::route('default_public', $tournament->slug), 40) ?>
                    </a>
                </h3>

                <h3 class="extra-info"><?= LangHelper::get('referee', 'Referee') ?>
                    : <?= $tournament->refereeName() ?></h3>

                <br>

                <?php if ($tournament->date_of_approval): ?>
                    <h3 class="extra-info">
                        <?= LangHelper::get('tournament_approved_date', 'Tournament approved date') ?>:
                        <?= DateTimeHelper::GetShortDateFullYear($tournament->date_of_approval) ?>
                    </h3>
                <?php endif; ?>
                <?php if ($sponsor): ?>

                    <h3 class="extra-info pad-top70"><?= LangHelper::get('sponsor', 'Sponsor') ?>:</h3>
                    <span class="line"></span>

                    <h3 class="extra-info pad-top20"><?= $sponsor->title ?></h3>

                    <h3 class="extra-info"><?= LangHelper::get('city', 'City') ?>: <?= $sponsor->city ?></h3>

                    <h3 class="extra-info"><?= LangHelper::get('address', 'Address') ?>: <?= $sponsor->address ?></h3>

                    <h3 class="extra-info"><?= LangHelper::get('email', 'Email') ?>: <?= $sponsor->email ?></h3>

                    <h3 class="extra-info"><?= LangHelper::get('phone', 'Phone') ?>: <?= $sponsor->phone ?></h3>

                    <h3 class="extra-info"><?= LangHelper::get('website', 'Website') ?>: <?= $sponsor->website ?></h3>

                    <img src="<?= $sponsor->image('thumbnails') ?>" class="imgResp " alt=""/>

                <?php endif; ?>
            </div>
            <br/>

        </div>

        <div class="tournament-second pad-top5 tournament_details_content">
            <div class="details-header"><?= LangHelper::get('tournament_information', 'Tournament information') ?>
                <a class="call pull-right tournament-notes-link link-underline" href="<?= URL::to("/tournaments/notes/" . $tournament->id) ?>">
                    <?= LangHelper::get('tournament_notes', 'Tournament notes') ?> <?= $notes_count ? "(".$notes_count.")" : "" ?>
                </a>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('title', 'Title') ?>:</div>
                <div class="detail-label data"
                     title="<?= $tournament->title ?>"><?= Str::limit($tournament->title, 32) ?></div>
                <div class="detail-label"><?= LangHelper::get('region', 'Region') ?>:</div>
                <div class="detail-label data"><?= $tournament->club->province->region->region_name ?></div>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('surface', 'Surface') ?>:</div>
                <div class="detail-label data"><?= ($tournament->surface) ? $tournament->surface->name : 'N/A' ?></div>
                <div class="detail-label"><?= LangHelper::get('club', 'Club') ?>:</div>
                <div class="detail-label data"><?= Str::limit($tournament->club->club_name, 29) ?></div>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('main_draw_start', 'Main draw start') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('main_draw_from') ?></div>
                <div class="detail-label"><?= LangHelper::get('main_draw_finish', 'Main draw finish') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('main_draw_to') ?></div>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('qualifying_date_from', 'Qualifying date from') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('qualifying_date_from') ?></div>
                <div class="detail-label"><?= LangHelper::get('qualifying_date_to', 'Qualifying date to') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('qualifying_date_to') ?></div>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('entry_deadline', 'Entry deadline') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('entry_deadline') ?></div>
                <div class="detail-label"><?= LangHelper::get('withdrawal_deadline', 'Withdrawal deadline') ?>:</div>
                <div class="detail-label data"><?= $tournament->date->getDate('withdrawal_deadline') ?></div>
            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('submitted_by', 'Submitted by') ?>:</div>
                <div
                    class="detail-label data"><?= ($tournament->creator()) ? $tournament->creator()->info("name") . " " . $tournament->creator()->info("surname") : "N/A" ?></div>
                <div class="detail-label"><?= LangHelper::get('number_of_courts', 'Number of courts') ?>:</div>
                <div class="detail-label data"><?= $tournament->number_of_courts ?></div>

            </div>
            <div class="row">
                <div class="detail-label"><?= LangHelper::get('referee', 'Referee') ?>:</div>
                <div class="detail-label data">
                    <?= $tournament->refereeName() ?>
                </div>

                <div class="detail-label"><?= LangHelper::get('tournament_type', 'Tournament type') ?>:</div>
                <div class="detail-label data"> <?= LangHelper::get(strtolower($tournament->type->category_tournament), $tournament->type->category_tournament); ?> </div>
            </div>
        </div>
        <div class="tournament-second pad-top50">
            <div class="details-header"><?= LangHelper::get('draws_information', 'Draws information') ?></div>
            <div id="container4" class="pad-top20">
                <div id="container3">
                    <div id="container2">
                        <div id="container1">
                            <div id="col1">
                                <?php foreach ($draws as $key => $draw) {

                                    if ((($key % 3) == 0)) {
                                        ?>
                                        <!-- Column one start -->
                                        <p><?= LangHelper::get('options', 'Options') ?>:</p>
                                        <p><?= LangHelper::get('draw_id', 'Draw ID') ?>:</p>
                                        <p><?= LangHelper::get('category', 'Category') ?>:</p>
                                        <p><?= LangHelper::get('type', 'Type') ?>:</p>
                                        <p><?= LangHelper::get('gender', 'Gender') ?>:</p>
                                        <p><?= LangHelper::get('total_acceptances', 'Total acceptances') ?>:</p>
                                        <p><?= LangHelper::get('total_number_of_seeds', 'Total number of seeds') ?>:</p>
                                        <p><?= LangHelper::get('direct_acceptances_from', 'Direct acceptances') ?>:</p>
                                        <p><?= LangHelper::get('accepted_qualifiers', 'Qualifiers') ?>:</p>
                                        <p><?= LangHelper::get('wild_cards', 'Wild cards') ?>:</p>
                                        <p><?= LangHelper::get('special_exempts', 'Special exempts') ?>:</p>
                                        <p><?= LangHelper::get('onsite_direct', 'Onsite direct') ?>:</p>
                                        <p><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?>:</p>
                                        <p><?= LangHelper::get('prize_pool', 'Prize pool') ?>:</p>
                                        <p><?= LangHelper::get('match_rules', 'Match rules') ?>:</p>
                                        <p><?= LangHelper::get('set_rules', 'Set rules') ?>:</p>
                                        <p><?= LangHelper::get('game_rules', 'Game rules') ?>:</p>
                                        <div style="height: 40px;"></div>
                                        <!-- Column one end -->
                                    <?php }
                                } ?>
                            </div>
                            <div id="col2">
                                <!-- Column two start -->
                                <?php foreach ($draws as $key => $draw) {
                                    if ((($key % 3) == 0)) {
                                        ?>
                                        <p style="padding-top: 3px">
                                            <a href="/tournaments/edit-draw/<?= $draw->id ?>"
                                               style="color:#FFC627"><?= LangHelper::get('edit_draw', 'Edit draw') ?></a>
                                            <?php if (!TournamentHelper::checkIfMatchesExist($draw->id, 2) AND !TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                |
                                                <a style="color:#FFC627" data-title="Signup for tournament"
                                                   href="<?= URL::to('signups/register', $draw->id) ?>">
                                                    <?= LangHelper::get('signup', 'Signup') ?>
                                                </a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=2"><?= LangHelper::get('q_bracket', '(Q) bracket') ?></a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=1"><?= LangHelper::get('m_bracket', '(M) bracket') ?></a>
                                            <?php endif ?>
                                        </p>
                                        <p><?= $draw->unique_draw_id ?></p>
                                        <p><?= $draw->category->name ?></p>
                                        <p><?= $draw->typeName() ?></p>
                                        <p><?= $draw->genderName() ?></p>
                                        <p><?= $draw->size->total ?></p>
                                        <p><?= $draw->number_of_seeds ?></p>
                                        <p><?= $draw->size->direct_acceptances ?></p>
                                        <p><?= $draw->size->accepted_qualifiers ?></p>
                                        <p><?= $draw->size->wild_cards ?></p>
                                        <p><?= $draw->size->special_exempts ?></p>
                                        <p><?= $draw->size->onsite_direct ? LangHelper::get("yes", "Yes") : LangHelper::get("no", "No") ?></p>
                                        <p><?= $draw->qualification->draw_size ?></p>
                                        <p> &euro;<?= $draw->prize_pool ?></p>
                                        <p title="<?= $draw->match_rule() ?>"><?= Str::limit($draw->match_rule(), 32) ?></p>
                                        <p title="<?= $draw->set_rule() ?>"><?= Str::limit($draw->set_rule(), 32) ?></p>
                                        <p title="<?= $draw->game_rule() ?>"><?= Str::limit($draw->game_rule(), 32) ?></p>
                                        <div style="height: 40px;"></div>
                                        <!-- Column two end -->
                                    <?php }
                                } ?>
                            </div>
                            <div id="col3">
                                <!-- Column three start -->
                                <?php foreach ($draws as $key => $draw) {
                                    if ((($key % 3) == 1)) {
                                        ?>
                                        <p style="padding-top: 3px">
                                            <a href="/tournaments/edit-draw/<?= $draw->id ?>"
                                               style="color:#FFC627"><?= LangHelper::get('edit_draw', 'Edit draw') ?></a>
                                            <?php if (!TournamentHelper::checkIfMatchesExist($draw->id, 2) AND !TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                |
                                                <a style="color:#FFC627" data-title="Signup for tournament"
                                                   href="<?= URL::to('signups/register', $draw->id) ?>">
                                                    <?= LangHelper::get('signup', 'Signup') ?>
                                                </a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=2"><?= LangHelper::get('q_bracket', '(Q) bracket') ?></a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=1"><?= LangHelper::get('m_bracket', '(M) bracket') ?></a>
                                            <?php endif ?>
                                        </p>
                                        <p><?= $draw->unique_draw_id ?></p>
                                        <p><?= $draw->category->name ?></p>
                                        <p><?= $draw->typeName() ?></p>
                                        <p><?= $draw->genderName() ?></p>
                                        <p><?= $draw->size->total ?></p>
                                        <p><?= $draw->number_of_seeds ?></p>
                                        <p><?= $draw->size->direct_acceptances ?></p>
                                        <p><?= $draw->size->accepted_qualifiers ?></p>
                                        <p><?= $draw->size->wild_cards ?></p>
                                        <p><?= $draw->size->special_exempts ?></p>
                                        <p><?= $draw->size->onsite_direct ? LangHelper::get("yes", "Yes") : LangHelper::get("no", "No") ?></p>
                                        <p><?= $draw->qualification->draw_size ?></p>
                                        <p> &euro;<?= $draw->prize_pool ?></p>
                                        <p title="<?= $draw->match_rule() ?>"><?= Str::limit($draw->match_rule(), 32) ?></p>
                                        <p title="<?= $draw->set_rule() ?>"><?= Str::limit($draw->set_rule(), 32) ?></p>
                                        <p title="<?= $draw->game_rule() ?>"><?= Str::limit($draw->game_rule(), 32) ?></p>
                                        <div style="height: 40px;"></div>
                                    <?php }
                                } ?>
                                <!-- Column three end -->
                            </div>
                            <div id="col4">
                                <!-- Column four start -->
                                <?php foreach ($draws as $key => $draw) {
                                    if ((($key % 3) == 2)) {
                                        ?>
                                        <p style="padding-top: 3px">
                                            <a href="/tournaments/edit-draw/<?= $draw->id ?>"
                                               style="color:#FFC627"><?= LangHelper::get('edit_draw', 'Edit draw') ?></a>
                                            <?php if (!TournamentHelper::checkIfMatchesExist($draw->id, 2) AND !TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                |
                                                <a style="color:#FFC627" data-title="Signup for tournament"
                                                   href="<?= URL::to('signups/register', $draw->id) ?>">
                                                    <?= LangHelper::get('signup', 'Signup') ?>
                                                </a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=2"><?= LangHelper::get('q_bracket', '(Q) bracket') ?></a>
                                            <?php endif ?>
                                            <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                                                | <a style="color:#FFC627"
                                                     href="/draws/db-bracket/<?= $draw->id ?>?list_type=1"><?= LangHelper::get('m_bracket', '(M) bracket') ?></a>
                                            <?php endif ?>
                                        </p>
                                        <p><?= $draw->unique_draw_id ?></p>
                                        <p><?= $draw->category->name ?></p>
                                        <p><?= $draw->typeName() ?></p>
                                        <p><?= $draw->genderName() ?></p>
                                        <p><?= $draw->size->total ?></p>
                                        <p><?= $draw->number_of_seeds ?></p>
                                        <p><?= $draw->size->direct_acceptances ?></p>
                                        <p><?= $draw->size->accepted_qualifiers ?></p>
                                        <p><?= $draw->size->wild_cards ?></p>
                                        <p><?= $draw->size->special_exempts ?></p>
                                        <p><?= $draw->size->onsite_direct ? LangHelper::get("yes", "Yes") : LangHelper::get("no", "No") ?></p>
                                        <p><?= $draw->qualification->draw_size ?></p>
                                        <p> &euro;<?= $draw->prize_pool ?></p>
                                        <p title="<?= $draw->match_rule() ?>"><?= Str::limit($draw->match_rule(), 32) ?></p>
                                        <p title="<?= $draw->set_rule() ?>"><?= Str::limit($draw->set_rule(), 32) ?></p>
                                        <p title="<?= $draw->game_rule() ?>"><?= Str::limit($draw->game_rule(), 32) ?></p>
                                        <div style="height: 40px;"></div>
                                    <?php }
                                } ?>
                                <!-- Column four end -->
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (Auth::user()->hasRole('superadmin') AND !$tournament->validated AND $tournament->status != 1):?>
                    <div class="pad-top35" ng-hide="validated">
                        <a class="metro button bigger green-col-bg left"
                           ng-click="validateTournament($event, '<?= LangHelper::get('tournament_validated', 'Tournament validated') ?>')"
                           href="<?= URL::to('tournaments/validate', array($tournament->id)) ?>">
                            <?= LangHelper::get('validate', 'Validate') ?>
                        </a>
                    </div>
                <?php endif?>

                <?php if (Auth::user()->hasRole('regional_admin') AND !$tournament->validated AND $tournament->status == 1):?>
                    <div class="pad-top35">
                        <a class="metro button bigger green-col-bg left"
                           href="/tournaments/validate-and-approve/<?= $tournament->id ?>">
                            <?= LangHelper::get('validate_and_approve', 'Validate and approve') ?>
                        </a>
                    </div>
                <?php endif?>

                <div class="pad-top35" ng-hide="!actionsPermited">
                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                        <?php if (!in_array($tournament->status, [2, 3, 5])): ?>
                            <a class="metro button bigger green-col-bg left"
                               ng-click="postRequest($event, '<?= LangHelper::get('tournament_approved', 'Tournament approved') ?>', 2)"
                               href="<?= URL::to('tournaments/request-action', array($tournament->id)) ?>">
                                <?= LangHelper::get('approve', 'Approve') ?>
                            </a>

                            <a class="metro button bigger red-col-bg right"
                               ng-click="postRequest($event, '<?= LangHelper::get('tournament_reject', 'Tournament rejected') ?>', 3)"
                               href="<?= URL::to('tournaments/request-action', array($tournament->id)) ?>">
                                <?= LangHelper::get('reject', 'Reject') ?>
                            </a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>


            <div class="row clear pad-bottom20">
                <!-- Modal template -->
                <script type="text/ng-template" id="myModalContent.html">
                    <?= $ask_question_view ?>
                </script>

            </div>
        </div>

    </div>
</div>
<script src="/js/ang/approvalActions.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['approvalActions']);
    });
</script>