<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= ($param == 1) ? LangHelper::get('tournaments_in_this_week', 'Tournaments in this week') : LangHelper::get('upcoming_tournaments', 'Upcoming tournaments')?></h3>
               <span class="line"></span>
             </div>
        </div>
        <div class="row">
            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('title', 'Title')?></th>
                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                        <th class="text-left"><?= LangHelper::get('submited_by', 'Submited by')?></th>
                    <?php endif ?>
                    <th class="text-left"><?= LangHelper::get('surface', 'Surface')?></th>
                    <th class="text-left"><?= LangHelper::get('referee', 'Referee')?></th>
                    <th class="text-left"><?= LangHelper::get('organizer', 'Organizer')?></th>
                    <th class="text-left"><?= LangHelper::get('status', 'Status')?></th>
                    <th class="text-left"><?= LangHelper::get('region', 'Region')?></th>
                    <th class="text-left"><?= LangHelper::get('draws', 'Draws')?></th>
                    <th class="text-left"><?= LangHelper::get('options', 'Options')?></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($tournaments as $tournament): ?>
                    <tr>
                        <td class="pad-right100"><?= $tournament->title ?></td>
                        <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                            <td><?= ( $tournament->creator() ) ? $tournament->creator()->info("name")." ".$tournament->creator()->info("surname") : false?></td>
                        <?php endif ?>
                        <td class="pad-right100"><?= ($tournament->surface) ? $tournament->surface->name : NULL?></td>
                        <td class="pad-right100"><?= $tournament->refereeName()?></td>
                        <td class="pad-right100"><?= $tournament->organizer->name ?></td>
                        <td class="pad-right100"><?= $tournament->status() ?></td>
                        <td class="pad-right100"><?= $tournament->club->province->region->region_name ?></td>
                        <td class="pad-right100">
                            <?= TournamentHelper::countDraws($tournament->tournament_id); ?>
                        </td>
                        <td class="transparent text-right">
                            <a class="rfet-button rfet-yellow call" data-title="Tournament details" href="/tournaments/details/<?= $tournament->tournament_id?>"><?= LangHelper::get('details', 'Details')?></a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>
    </div>


</div>