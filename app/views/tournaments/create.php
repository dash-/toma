<div class="container ang" ng-cloak>
    <div class="grid" ng-controller="FormController" data-ng-init="init()">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('create_new_tournaments', 'Create New Tournaments') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <form name="createForm"
              ng-submit="formSubmit($event, 'POST', '<?= LangHelper::get('tournament_successfully_submitted', 'Tournament successfully submitted') ?>',false, true)"
              novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset ng-show="!showMe">

                <div class="tournament-second">
                    <div class="row">
                        <div class="half">
                            <div class="form-control" style="margin-left: 1%; width: 99%;"
                                 ng-class="{ 'has-error' : clicked == true && createForm.title.$invalid }">
                                <label><?= LangHelper::get('title', 'Title') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="title" ng-model="formData.tournament.title" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="" ><?= LangHelper::get('tournament_type', 'Tournament type') ?>*</label>

                                <div class="input-control text">
                                    <?= Form::select('tournament_type', $tournament_types, NULL, array('ng-model' => 'formData.tournament.tournament_type')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.surface.$invalid }">
                                <label for="name"><?= LangHelper::get('surface', 'Surface') ?></label>
                                <?= Form::select('surface', $surfaces_array, NULL, array('ng-model' => 'formData.tournament.surface_id')); ?>
                            </div>
                        </div>

                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.club.$invalid }">
                                <label for="name"><?= LangHelper::get('club', 'Club') ?> *</label>
                                <?= Form::select('club', $clubs_array, NULL, array('required', 'ng-model' => 'formData.tournament.club_id')); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="has_qualifying"><?= LangHelper::get('has_qualifying', 'Has qualifying?') ?></label>
                                <?= Form::select('has_qualifying', $qualiy_q, 1, array('ng-model' => 'formData.tournament.has_qualifying')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" ng-hide="formData.tournament.has_qualifying == 0">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.qualifying_date_from.$invalid }">
                                <label for="name"><?= LangHelper::get('qualifying_date_from', 'Qualifying date from') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="qualifying_date_from" min-date="minDate"
                                           datepicker-popup="dd/MM/yyyy" ng-required="true"
                                           ng-model="formData.date.qualifying_date_from"
                                           is-open="opened['qualifyingFrom']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'qualifyingFrom')" formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.qualifying_date_to.$invalid }">
                                <label for="name"><?= LangHelper::get('qualifying_date_to', 'Qualifying date to') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="qualifying_date_to"
                                           min-mode="formData.date.qualifying_date_from"
                                           min-date="formData.date.qualifying_date_from" datepicker-popup="dd/MM/yyyy"
                                           ng-required="true" ng-model="formData.date.qualifying_date_to"
                                           is-open="opened['qualifyingTo']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'qualifyingTo')" formatdate>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.main_draw_from.$invalid }">
                                <label for="name"><?= LangHelper::get('main_draw_start', 'Main draw start') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="main_draw_from" min-date="minDate2"
                                           min-mode="formData.date.qualifying_date_from"
                                           datepicker-popup="dd/MM/yyyy" ng-required="true"
                                           ng-model="formData.date.main_draw_from" is-open="opened['mainFrom']"
                                           show-button-bar="false" ng-click="openDatePicker($event, 'mainFrom')"
                                           formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.main_draw_to.$invalid }">
                                <label for="name"><?= LangHelper::get('main_draw_finish', 'Main draw finish') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="main_draw_to" min-date="formData.date.main_draw_from"
                                           min-mode="formData.date.qualifying_date_from"
                                           datepicker-popup="dd/MM/yyyy" ng-required="true"
                                           ng-model="formData.date.main_draw_to" is-open="opened['mainTo']"
                                           show-button-bar="false" ng-click="openDatePicker($event, 'mainTo')"
                                           formatdate ng-change="updateDate()">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.entry_deadline.$invalid }">
                                <label for="name"><?= LangHelper::get('entry_deadline', 'Entry deadline') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="entry_deadline" datepicker-popup="dd/MM/yyyy"
                                           min-mode="formData.date.main_draw_from"
                                           max-date="formData.date.qualifying_date_from"
                                           ng-model="formData.date.entry_deadline"
                                           is-open="opened['entryDeadline']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'entryDeadline')" formatdate>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="name"><?= LangHelper::get('withdrawal_deadline', 'Withdrawal deadline') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="withdrawal_deadline" datepicker-popup="dd/MM/yyyy"
                                           min-mode="formData.date.main_draw_from"
                                           max-date="formData.date.qualifying_date_from"
                                           ng-model="formData.date.withdrawal_deadline"
                                           is-open="opened['withdrawalDeadline']" show-button-bar="false"
                                           ng-click="openDatePicker($event, 'withdrawalDeadline')" formatdate>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('number_of_courts', 'Number of courts') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.tournament.number_of_courts">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="sponsor_id"><?= LangHelper::get('general_sponsor', 'General sponsor') ?></label>
                                <?= Form::select('sponsor_id', $sponsors_array, NULL, array('ng-model' => 'formData.sponsor.sponsor_id')); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="referee_id_real"><?= LangHelper::get('referee', 'Referee') ?> *</label>
                                <input type="hidden" name="referee_id_real" id="referee_id_real"
                                       ng-model="formData.tournament.referee_id_real">
                            </div>
                        </div>

                        <div class="half">
                            <div class="form-control">
                                <label
                                    for=""><?= LangHelper::get('player_representative', 'Player representative') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.tournament.player_representative">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="row gap-bottom20">
                            <div class="half">
                                <div class="form-control">
                                    <label for=""><?= LangHelper::get('official_ball', 'Official ball') ?></label>

                                    <div class="input-control text">
                                        <input type="text" ng-model="formData.tournament.official_ball">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tournament-first">
                    <div class="table-title tournament">
                        <h3><?= LangHelper::get('organizer_information', 'Organizer information') ?></h3>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="tournament-second marginTop-10">
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.name.$invalid }">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="name" ng-model="formData.organizer.name"
                                           typeahead="gamer as gamer.full_name for gamer in getOrganizers($viewValue) | filter:$viewValue"
                                           typeahead-on-select="populateInfo($item)" typeahead-min-length="3" required
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : clicked == true && createForm.email.$invalid }">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" name="email" ng-model="formData.organizer.email"
                                           autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="phone" ng-model="formData.organizer.phone">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="fax"><?= LangHelper::get('fax', 'Fax') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="fax" ng-model="formData.organizer.fax">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="website"><?= LangHelper::get('website', 'Website') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="website" ng-model="formData.organizer.website">
                                </div>
                            </div>
                        </div>

                        <a ng-click="loadStep()" class="button clear place-right primary large dark"
                           style="margin-top: 40px;"><?= LangHelper::get('next_step', 'Next step') ?></a>

                    </div>
                </div>

                <div ng-if="createForm.title.$invalid"></div>

                <div class="row">
                    <div class="input-control back-in-pagination">
                        <a class="back-button-margin button primary yellow back big call" data-title="Tournaments"
                           href="/tournaments"><?= LangHelper::get('back', 'BACK') ?></a>
                    </div>
                </div>

            </fieldset>

            <div ng-show="showMe == true">
                <?= $step2 ?>
            </div>

            <div class="custom-errors">
                <a class="ns-close ng-cloak" id="closeError" ng-show="clicked == true"></a>

                <p ng-if="clicked == true && createForm.title.$error.required"><?= LangHelper::get('title_is_required', 'Title is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.surface.$error.required"><?= LangHelper::get('surface_is_required', 'Surface is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.club.$error.required"><?= LangHelper::get('club_is_required', 'Club is required') ?>
                    .</p>



                <p ng-if="clicked == true && createForm.main_draw_from.$error.required"><?= LangHelper::get('main_draw_from_is_required', 'Main Draw From is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.main_draw_from.$error.date"><?= LangHelper::get('main_draw_from_is_invalid', 'Main Draw From is invalid') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.main_draw_to.$error.required"><?= LangHelper::get('main_draw_to_is_required', 'Main Draw To is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.main_draw_to.$error.date"><?= LangHelper::get('main_draw_to_is_invalid', 'Main Draw To is invalid') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.entry_deadline.$error.required"><?= LangHelper::get('entry_deadline_is_required', 'Entry Deadline is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.entry_deadline.$error.date"><?= LangHelper::get('entry_deadline_is_invalid', 'Entry Deadline is invalid') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.withdrawal_deadline.$error.required"><?= LangHelper::get('withdrawal_deadline_is_required', 'Withdrawal Deadline is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.withdrawal_deadline.$error.date"><?= LangHelper::get('withdrawal_deadline_is_invalid', 'Withdrawal Deadline is invalid') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.name.$error.required"><?= LangHelper::get('name_is_required', 'Name is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.email.$error.required"><?= LangHelper::get('email_is_required', 'Email is required') ?>
                    .</p>

                <p ng-if="clicked == true && createForm.email.$error.email"><?= LangHelper::get('email_is_invalid', 'Email is invalid') ?>
                    .</p>
            </div>

            <div class="custom-warnings" ng-if="DateQualifying == true">
                <span class="yellow-color">
                    <?= LangHelper::get('warning', 'Warning') ?>:
                </span>
        <span class="yellow-color">
            <?= LangHelper::get('qualifying_date_from_can_not_be_bigger_than_qualifying_date_to', 'Qualifying date from can not be bigger than Quafilifying date to') ?>
            !
        </span>
            </div>
            <div class="custom-warnings" ng-if="DateDraw == true">
                <span class="yellow-color">
                    <?= LangHelper::get('warning', 'Warning') ?>:
                </span>
        <span class="yellow-color">
            <?= LangHelper::get('main_draw_start_can_not_be_bigger_than_main_draw_finish', 'Main draw start can not be bigger than Main draw finish') ?>
            !
        </span>
            </div>
            <div class="custom-warnings" ng-if="DateEntry == true">
                <span class="yellow-color">
                    <?= LangHelper::get('warning', 'Warning') ?>:
                </span>
        <span class="yellow-color">
            <?= LangHelper::get('entry_deadline_can_not_be_bigger_than_withdrawal_deadline', 'Entry deadline can not be bigger than Withdrawal deadline') ?>
        </span>
            </div>
            <div class="custom-warnings" ng-if="DateFirstError == true">
                <span class="yellow-color">
                    <?= LangHelper::get('warning', 'Warning') ?>:
                </span>
                <span class="yellow-color">
                    <?= LangHelper::get('qualifying_date_to_can_not_be_bigger_than_main_draw_start', 'Qualifying Date To can not be bigger then Main Draw Start') ?>
                </span>
            </div>

        </form>
    </div>
</div>

<script src="/js/ang/multiStepForm.js"></script>
<script>

    $(document).ready(function () {
        $("select").select2();

        $('#referee_id_real').select2({
            ajax: {
                url: "/referees/search-referees",
                dataType: 'json',
                delay: 350,
                data: function (params) {
                    return {
                        q: params
                    };
                },
                results: function (data) {
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            minimumInputLength: 1
        });

        $('#closeError').click(function () {
            $(".custom-errors").fadeOut(500);
        });

        $('#closeError2').click(function () {
            $(".custom-errors").fadeOut(500);
        });

    });


    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['multiStepForm']);
    });

</script>
