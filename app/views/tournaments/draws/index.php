<div class="container ang" ng-cloak>
    <div class="grid" ng-controller="FormController" scurl="<?= URL::to('tournaments/draws-data', $tournament->id) ?>" data-hasqualifying="<?= $tournament->has_qualifying ? 1 : 0 ?>"
         data-ng-init="init()">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= $tournament->title ?> - <?= LangHelper::get('draws', 'draws') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <div class="table-scrollable ">
                <table class="table bg-dark tournament-table table-scrollable-w768">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('start_date', 'StartDate') ?></th>
                        <th class="text-left"><?= LangHelper::get('category', 'Category') ?></th>
                        <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                        <th class="text-left"><?= LangHelper::get('gender', 'Gender') ?></th>
                        <th class="text-left"><?= LangHelper::get('main_draw_size', 'Main draw size') ?></th>
                        <th class="text-left"><?= LangHelper::get('total_number_of_seeds', 'Total number of seeds') ?></th>
                        <th class="text-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size') ?></th>
                        <th class="text-left"><?= LangHelper::get('approval_status', 'Approval status') ?></th>
                        <th class="text-right" width="45%"><?= LangHelper::get('options', 'Options') ?></th>
                    </tr>
                    </thead>

                    <tbody>


                    <tr ng-repeat="draw in draws track by $index">
                        <td>{{ draw.tournament.date.main_draw_from | asDate | date:'dd/MM/yyyy' }}</td>
                        <td>{{ draw.category.name }} {{draw.is_prequalifying ? '<?= LangHelper::get('pq', 'PQ')?>' : '' }}</td>
                        <td>{{ replaceText(draw.draw_type, {singles:'Singles', 'doubles': 'Doubles'}) }}</td>
                        <td>{{ replaceText(draw.draw_gender, {m:'Male', 'f': 'Female'}) }}</td>
                        <td class="text-center"><a class="call" href="/signups/final-list/{{draw.id}}#main">{{ draw.size.total }}</a></td>
                        <td class="text-center">{{draw.number_of_seeds }}</td>
                        <td class="text-center"><a class="call" href="/signups/final-list/{{draw.id}}#qualifying">{{ draw.qualification.draw_size }}</a></td>
                        <td class="text-center">{{draw.approval_status | asApprovalStatus }}</td>
                        <td class="transparent text-right">
                            <?php if(Auth::getUser()->hasRole('superadmin')): ?>
                                <a class="rfet-button rfet-yellow" ng-show="draw.approval_status == 2"
                                   href="/signups/super-validate/{{draw.id}}">
                                    <?= LangHelper::get('validate_draw', 'Validate draw') ?>
                                </a>
                            <?php endif ?>
                            <a class="rfet-button rfet-yellow call"
                               href="/tournaments/draw-details/{{draw.id}}"><?= LangHelper::get('details', 'Details') ?></a>
                            <a class="rfet-button rfet-yellow call"
                               href="/tournaments/edit-draw/{{draw.id}}"><?= LangHelper::get('edit', 'Edit') ?></a>
                            <a class="call rfet-button rfet-yellow center-text" data-title="List of signed up players"
                               href="/signups/list/{{draw.id}}">
                                <?= LangHelper::get('players_signed', 'Players') ?>
                            </a>
                            <?php if(Auth::getUser()->hasRole('superadmin') || $tournament->status != 1): ?>
                            <a class="rfet-button rfet-yellow call"
                               href="/signups/register/{{draw.id}}"><?= LangHelper::get('signup', 'Signup') ?></a>
                            <?php endif ?>

                            <a ng-show="draw.signups.length > 0" class="rfet-button rfet-yellow call"
                               data-title="Final listing"
                               href="/signups/final-list/{{draw.id}}"><?= LangHelper::get('view_final_listing', 'View final listing') ?></a>
                        </td>
                        <td class="transparent">
                            <?php if($tournament->status!=5): ?>
                            <div class="rfet-button rfet-danger deleteDraw" draw-id="{{draw.id}}" tournament-id="<?= $tournament->id ?>" href="">Delete</div>
                            <?php endif ?>
                        </td>
                    </tr>

                </table>
            </div>

        </div>
        <?php if($tournament->status!=5): ?>

        <!-- form submit start -->
        <div class="row tabScrl">
            <div>
                <button class="button extra rfet-yellow next-black-arrow"
                        ng-click="isCollapsed = !isCollapsed"><?= LangHelper::get('add_new_draw', 'Add new draw') ?></button>
                <br>

                <div collapse="isCollapsed">
                    <form name="createForm" ng-class="{ 'show-errors': showErrors }"
                          ng-submit="formSubmit($event, 'POST', 'Draw successfully submitted', '<?= URL::to('tournaments/create-draw', $tournament->id) ?>', true)"
                          class="pad-top40" novalidate>
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <div class="pad-top50 tournament-second tourDrawIndRes" style="padding-left: 35%">
                                <div class="row">
                                    <div class="half">
                                        <div class="form-control">
                                            <label><?= LangHelper::get('category', 'Category') ?> *</label>
                                            <?= Form::select('draw_category', $categories, null, array('required', 'ng-model' => 'formData.draws.draw_category_id')); ?>
                                        </div>
                                    </div>
                                    <div class="half">
                                        <div class="form-control">
                                            <label><?= LangHelper::get('type', 'Type') ?> *</label>
                                            <?= Form::select('draw_type', $types, null, array('required', 'ng-model' => 'formData.draws.draw_type')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="half">
                                        <div class="form-control">
                                            <label for="title"><?= LangHelper::get('gender', 'Gender') ?> *</label>

                                            <div class="input-control select">
                                                <?= Form::select('draw_gender', $genders, null, array('required', 'ng-model' => 'formData.draws.draw_gender')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="half">
                                        <div class="half">
                                            <div class="form-control">
                                                <label for="title"><?= LangHelper::get('total_acceptance', 'Total acceptance') ?> *</label>
                                                <div class="input-control select" style="width: 96%;">
                                                    <?= Form::select('total_acceptance', $total_acceptance, null, array('required', 'ng-model' => 'formData.sizes.total')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="half">
                                            <div class="form-control">
                                                <label><?= LangHelper::get('prize_pool', 'Prize pool') ?> *</label>

                                                <div class="input-control text">
                                                    <input type="text" name="prize_pool" class="only-number"
                                                           ng-model="formData.draws.prize_pool" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="half">
                                        <div class="form-control"
                                             ng-class="{ 'has-error' : drawclicked == true && createForm.draw_gender.$invalid }">
                                            <label for="title"><?= LangHelper::get('is_this_pre_qualifying_draw', 'Is this pre qualifying draw?') ?> *</label>
                                            <div class="input-control select">

                                                <?= Form::select('is_prequalifying', $prequali_q, null, array('ng-model' => 'formData.is_prequalifying')); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="break-draw">
                                <div class="row">
                                    <div class="half">
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="draw_size"><?= LangHelper::get('number_of_seeds', 'Number of seeds') ?>
                                                    *</label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <input type="text" ng-model="formData.draws.number_of_seeds"  class="only-number">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="direct_acceptances"><?= LangHelper::get('direct_acceptances', 'Direct acceptances') ?>
                                                    *</label>

                                                <div class="input-control text">
                                                    <input type="text" name="direct_acceptances"  class="only-number"
                                                           ng-model="formData.sizes.direct_acceptances" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="half">
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="accepted_qualifiers"><?= LangHelper::get('accepted_qualifiers', 'Qualifiers') ?>
                                                    *</label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <input type="text" name="accepted_qualifiers"
                                                           ng-disabled="has_qualifying == 0 || formData.is_prequalifying == 1"  class="only-number"
                                                           ng-model="formData.sizes.accepted_qualifiers" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="draw_size"><?= LangHelper::get('size_of_qualifying_draw', 'Size of Qualifying Draw') ?>
                                                    *</label>

                                                <div class="input-control text">
                                                    <input type="text" name="total_qualifiers" class="only-number"
                                                           ng-disabled="has_qualifying == 0 || formData.is_prequalifying == 1"
                                                           ng-model="formData.qualifications.draw_size" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="half">
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="wild_cards"><?= LangHelper::get('wild_cards', 'Wild cards') ?></label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <input type="text" name="wild_cards"  class="only-number"
                                                           ng-model="formData.sizes.wild_cards">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="special_exempts"><?= LangHelper::get('special_exempts', 'Special exempts') ?></label>

                                                <div class="input-control text">
                                                    <input type="text" name="special_exempts"  class="only-number" disabled ng-disabled="true"
                                                           ng-model="formData.sizes.special_exempts">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="half">
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="onsite_direct"><?= LangHelper::get('onsite_direct', 'Onsite direct') ?></label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <?= Form::select('onsite_direct',
                                                        $onsite_direct, 0, array('ng-model' => 'formData.sizes.onsite_direct', 'id' => 'onsite_direct')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="has_final_round"><?= LangHelper::get('quali_to_finals', 'Qualifications has final round') ?></label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <?= Form::select('plays_to_finals', $quali_has_final_round, null, array('ng-model' => 'formData.qualifications.has_final_round')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="half">
                                        <div class="form-control">
                                            <label for="prequalifying_draw">
                                                <?= LangHelper::get('prequalifying_draw_size', 'Prequalifying draw size') ?>

                                            </label>
                                            <div class="input-control" style="width: 96%;">
                                                <?= Form::select('prequalifying_size', $total_acceptance_prequali, null, array(
                                                    'ng-model' => 'formData.sizes.prequalifying_size')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="break-draw">
                                <div class="row">
                                    <div class="half" style="width: 68%">
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="match_rules"><?= LangHelper::get('match_rules', 'Match rules') ?></label>

                                                <div class="input-control text" style="width: 96%;">

                                                    <?= Form::select('match_rule', $match_rules, 0, array('ng-model' => 'formData.draws.match_rule')); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="half">
                                            <div class="form-control">
                                                <label
                                                    for="match_rules"><?= LangHelper::get('set_rules', 'Set rules') ?></label>

                                                <div class="input-control text" style="width: 96%;">
                                                    <?= Form::select('set_rule', $set_rules, 0, array('ng-model' => 'formData.draws.set_rule')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="half pull-right" style="width: 30%">
                                        <div class="form-control">
                                            <label
                                                for="match_rules"><?= LangHelper::get('game_rules', 'Game rules') ?></label>

                                            <div class="input-control text" style="width: 96%;">
                                                <?= Form::select('game_rule', $game_rules, 0, array('ng-model' => 'formData.draws.game_rule')); ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit"
                                        class="button right big next-yellow-arrow yellow-color dark-col-bg"
                                        style="margin-top:27px"><?= LangHelper::get('save', 'Save') ?></button>
                                <form-errors></form-errors>
                                <div class="show-errors form-warnings" ng-if="Sum1Error == true">
                                    <span class="yellow-color"><?= LangHelper::get('warning', 'Warning') ?>: </span>
                                    <span class="black-color">
                                        Sum of Direct From and Qualifiers To and Wild Cards and Special Exempts must be equal to Total Acceptances!
                                    </span>
                                </div>
                                <div class="show-errors form-warnings" ng-if="Sum2Error == true">
                                    <span class="yellow-color"><?= LangHelper::get('warning', 'Warning') ?>: </span>
                                    <span class="black-color">Sum of Direct to and Qualifiers from and Wild Cards and Special Exempts must be equal to Total Acceptances!</span>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>

        <?php endif ?>
    </div>

</div>

<script src="/js/ang/drawsForm.js"></script>
<script>

    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['drawsFormApp']);
    });

</script>