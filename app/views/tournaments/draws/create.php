<div class="container ang">
    <div class="grid">
        <div class="row">

            <form name="createForm" ng-submit="formSubmit($event, 'POST', 'Draw successfully submitted')" action="<?= Request::url()?>" ng-controller="FormController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="row">

                        <div class="table-title players">
                            <div class="clearfix span4">
                                <h3><?= LangHelper::get('create_new_draw', 'Create New Draw')?></h3>
                                <span class="line"></span>
                            </div>
                        </div>
                        <div class="span8">
                            <div class="form-control">
                                <label for="title"><?= LangHelper::get('category', 'Category')?></label>
                                <div class="input-control select">
                                    <?= Form::select('draw_category_id', $categories, NULL, array('required', 'ng-model' => 'formData.draws.draw_category_id')); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="title"><?= LangHelper::get('type', 'Type')?></label>
                                        <div class="input-control select">
                                            <?= Form::select('draw_type', $types, NULL, array('required', 'ng-model' => 'formData.draws.draw_type')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="title"><?= LangHelper::get('gender', 'Gender')?></label>
                                        <div class="input-control select">
                                            <?= Form::select('draw_gender', $genders, NULL, array('required', 'ng-model' => 'formData.draws.draw_gender')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="clearfix span4">
                            <h3 class="place-left"><?= LangHelper::get('draw_size_information', 'Draw size information')?></h3>
                            <span class="line"></span>
                        </div>
                        <div class="span8">
                            <div class="form-control">
                                <label for="total"><?= LangHelper::get('total_acceptances', 'Total acceptances')?></label>
                                <div class="input-control text">
                                    <input type="text" name="dotal" ng-model="formData.sizes.total" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="direct_acceptances"><?= LangHelper::get('direct_acceptances', 'Direct acceptances')?></label>
                                        <div class="input-control text">
                                            <input type="text" name="direct_acceptances" ng-model="formData.sizes.direct_acceptances">
                                        </div>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="accepted_qualifiers"><?= LangHelper::get('accepted_qualifiers', 'Qualifiers')?></label>
                                        <div class="input-control text">
                                            <input type="text" name="accepted_qualifiers" ng-model="formData.sizes.accepted_qualifiers">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="wild_cards"><?= LangHelper::get('wild_cards', 'Wild cards')?></label>
                                        <div class="input-control text">
                                            <input type="text" name="wild_cards" ng-model="formData.sizes.wild_cards">
                                        </div>
                                    </div>
                                </div>
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="special_exempts"><?= LangHelper::get('special_exempts', 'Special exempts')?></label>
                                        <div class="input-control text">
                                            <input type="text" name="special_exempts" ng-model="formData.sizes.special_exempts" disabled ng-disabled="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="span4">
                                    <div class="form-control">
                                        <label for="onsite_direct"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances')?></label>
                                        <div class="input-control text">
                                            <input type="text" name="onsite_direct" ng-model="formData.sizes.onsite_direct">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="clearfix span4">
                            <h3 class="place-left"><?= LangHelper::get('qualifying_draw_size', 'Qualifying draw size')?></h3>
                            <span class="line"></span>
                        </div>
                        <div class="span8">
                            <div class="row">
                                    <div class="span4">
                                        <div class="form-control">
                                            <label for="draw_size"><?= LangHelper::get('total_qualifiers', 'Total qualifiers')?></label>
                                            <div class="input-control text">
                                                <input type="text" name="draw_size" ng-model="formData.qualifications.draw_size">
                                            </div>
                                        </div>
                                    </div>
                                <div class="offset4">
                                    <button type="submit" ng-disabled="!createForm.$valid" class="save-button-margin-top clear place-right primary large gap-top50"><?= LangHelper::get('save', 'Save')?></button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-control back-in-pagination">
                            <a class="back-button-margin button primary yellow back big call gap-top50" data-title="Tournaments" href="<?= URL::to('tournaments/draws', $tournament->id)?>"><?= LangHelper::get('back', 'BACK')?></a>
                        </div>

                    </div>
                    

                </fieldset>
            </form>
        </div>
    </div>
</div>

<script src="/js/ang/drawsForm.js"></script>
<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['drawsFormApp']);
    });
</script>
 
