<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_draw', 'Edit draw') ?></h3>
                <span class="line"></span>
            </div>

            <div class="row tournament-first-links">
                <p>Qualifications draw</p>

                <a class=""
                   href="/tournaments/draw-details/<?= $draw->id ?>"><?= LangHelper::get('details', 'Details') ?></a>
                <?php if(Auth::getUser()->hasRole('superadmin') || $tournament->status != 1): ?>
                    <?php if (!TournamentHelper::checkIfMatchesExist($draw->id, 2) AND !TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                        <a class="call" data-title="Signup for draw" href="<?= URL::to('signups/register', $draw->id) ?>">
                            <?= LangHelper::get('register', 'Register') ?>
                        </a>
                    <?php endif ?>
                <?php endif ?>

                <a class="call" data-title="List of signed up players" href="/signups/list/<?= $draw->id ?>">
                    <?= LangHelper::get('players', 'Players') ?>
                </a>

                <?php if ($draw->signups->count() > 0): ?>
                    <a class="call" data-title="Final list"
                       href="<?= URL::to('signups/final-list', $draw->id) ?>"><?= LangHelper::get('create_edit_draw', 'Create/Edit draw') ?></a>
                <?php endif ?>

                <!-- quali -->
                <p><?= LangHelper::get('qualifications_draw', 'Qualifications draw') ?></p>
                <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 2)): ?>
                    <a href="/draws/db-bracket/<?= $draw->id ?>?list_type=2">
                        <?= LangHelper::get('view_qualify_bracket', 'View qualify bracket') ?>
                    </a>
                    <?php if (!TournamentHelper::checkIfQualificationsAreFinished($draw->id)): ?>
                        <a href="/schedule/show/<?= $draw->tournament_id ?>?list_type=2">
                            <?= LangHelper::get('order_of_play', 'Order of play') ?>
                        </a>
                    <?php endif ?>
                <?php else: ?>
                    <a href="/draws/manual-bracket/<?= $draw->id ?>?list_type=2">
                        <?= LangHelper::get('create_qualification_draw', 'Create qualification draw') ?>
                    </a>
                <?php endif ?>

                <!-- main -->
                <p><?= LangHelper::get('main_draw', 'Main draw') ?></p>
                <?php if (TournamentHelper::checkIfMatchesExist($draw->id, 1)): ?>
                    <a href="/draws/db-bracket/<?= $draw->id ?>?list_type=1">
                        <?= LangHelper::get('view_main_bracket', 'View main bracket') ?>
                    </a>
                    <?php if (!TournamentHelper::checkIfMainDrawIsFinished($draw->id)): ?>
                        <a href="/schedule/show/<?= $draw->tournament_id ?>?list_type=2">
                            <?= LangHelper::get('order_of_play', 'Order of play') ?>
                        </a>
                    <?php endif ?>
                <?php elseif (TournamentHelper::checkIfQualificationsAreFinished($draw->id)): ?>
                    <a href="/draws/manual-bracket/<?= $draw->id ?>?list_type=1">
                        <?= LangHelper::get('create_main_draw', 'Create main draw') ?>
                    </a>
                <?php endif ?>
            </div>

        </div>

        <div class="tournament-second" style="padding-left: 35%">
            <form name="createForm" ng-class="{ 'show-errors': showErrors }"
                  ng-submit="formSubmit($event, 'PUT', 'Draw successfully updated', '<?= Request::url() ?>')"  data-hasqualifying="<?= $draw->tournament->has_qualifying ? 1 : 0 ?>"
                  ng-controller="EditFormController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="title"><?= LangHelper::get('category', 'Category') ?> *</label>

                                <div class="input-control select">
                                    <?= Form::select('category', $categories, null, array('required',
                                        'ng-model' => 'formData.draws.draw_category_id',
                                        'ng-init'  => 'formData.draws.draw_category_id="' . $draw->draw_category_id . '"')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="title"><?= LangHelper::get('type', 'Type') ?> *</label>

                                <div class="input-control select">
                                    <?= Form::select('type', $types, null, array('required',
                                        'ng-model' => 'formData.draws.draw_type',
                                        'ng-init'  => 'formData.draws.draw_type="' . $draw->draw_type . '"')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="title"><?= LangHelper::get('gender', 'Gender') ?> *</label>

                                <div class="input-control select">
                                    <?= Form::select('gender', $genders, $draw->draw_gender, array('required',
                                        'ng-model' => 'formData.draws.draw_gender',
                                        'ng-init'  => 'formData.draws.draw_gender="' . $draw->draw_gender . '"')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="half">
                                <div class="form-control">
                                    <label for="title"><?= LangHelper::get('total_acceptance', 'Total acceptance') ?> *</label>
                                    <div class="input-control select" style="width: 96%;">
                                        <?= Form::select('total_acceptance', $total_acceptance, ($draw->size) ? $draw->size->total: "", array('required',
                                            'ng-model' => 'formData.sizes.total',
                                            'ng-init'  => 'formData.sizes.total="' . (($draw->size) ? $draw->size->total : ""). '"')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control">
                                    <label><?= LangHelper::get('prize_pool', 'Prize pool') ?> *</label>

                                    <div class="input-control text">
                                        <input type="text" name="prize_pool" ng-model="formData.draws.prize_pool" class="only-number"
                                               ng-init="formData.draws.prize_pool='<?= $draw->prize_pool ?>'" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{ 'has-error' : drawclicked == true && createForm.draw_gender.$invalid }">
                                <label for="title"><?= LangHelper::get('is_this_pre_qualifying_draw', 'Is this pre qualifying draw?') ?> *</label>
                                <div class="input-control select">
                                    <?= Form::select('is_prequalifying', $prequali_q, $draw->is_prequalifying, array(
                                        'ng-model' => 'formData.is_prequalifying',
                                        'ng-init'  => 'formData.is_prequalifying="' . $draw->is_prequalifying. '"')); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="break-draw">
                    <div class="row">
                        <div class="half">
                            <div class="half">
                                <div class="form-control">
                                    <label for="draw_size"><?= LangHelper::get('number_of_seeds', 'Number of seeds') ?>
                                        *</label>

                                    <div class="input-control text">
                                        <input type="text" class="only-number" ng-model="formData.draws.number_of_seeds"
                                               ng-init="formData.draws.number_of_seeds='<?= $draw->number_of_seeds ?>'">
                                    </div>
                                </div>
                            </div>


                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="direct_acceptances"><?= LangHelper::get('direct_acceptances', 'Direct acceptances') ?>
                                        *</label>

                                    <div class="input-control text" style="width: 96%;">
                                        <input type="text" class="only-number" name="direct_acceptances_from"
                                               ng-model="formData.sizes.direct_acceptances"
                                               ng-init="formData.sizes.direct_acceptances='<?= ($draw->size) ? $draw->size->direct_acceptances : ""?>'"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="half">
                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="accepted_qualifiers"><?= LangHelper::get('accepted_qualifiers', 'Qualifiers') ?>
                                        *</label>

                                    <div class="input-control text" style="width: 96%;">
                                        <input type="text" class="only-number" name="accepted_qualifiers"
                                               ng-disabled="has_qualifying == 0 || formData.is_prequalifying == 1"
                                               ng-init="formData.sizes.accepted_qualifiers='<?= ($draw->size) ? $draw->size->accepted_qualifiers : ""?>'"

                                               ng-model="formData.sizes.accepted_qualifiers" required>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="draw_size"><?= LangHelper::get('size_of_qualifying_draw', 'Size of Qualifying Draw') ?>
                                        *</label>

                                    <div class="input-control text">
                                        <input type="text" class="only-number" name="total_qualifiers"
                                               ng-disabled="has_qualifying == 0 || formData.is_prequalifying == 1"
                                               ng-model="formData.qualifications.draw_size"
                                               ng-init="formData.qualifications.draw_size='<?= ($draw->qualification) ?$draw->qualification->draw_size :"" ?>'"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="onsite_direct"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances') ?></label>

                                    <div class="input-control text" style="width: 96%;">
                                        <?= Form::select('onsite_direct', $onsite_direct, 0, array('required',
                                            'ng-model' => 'formData.sizes.onsite_direct',
                                            'ng-init'  => 'formData.sizes.onsite_direct="' . (($draw->size) ? $draw->size->onsite_direct : ""). '"')); ?>

                                    </div>
                                </div>
                            </div>

                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="special_exempts"><?= LangHelper::get('special_exempts', 'Special exempts') ?></label>

                                    <div class="input-control text">
                                        <input type="text" class="only-number" name="special_exempts" disabled ng-disabled="true"
                                               ng-model="formData.sizes.special_exempts"
                                               ng-init="formData.sizes.special_exempts='<?= ($draw->size) ? $draw->size->special_exempts : ""?>'">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="half">
                                <div class="form-control">
                                    <label for="wild_cards"><?= LangHelper::get('wild_cards', 'Wild cards') ?></label>

                                    <div class="input-control text" style="width: 96%;">
                                        <input type="text" class="only-number" name="wild_cards" ng-model="formData.sizes.wild_cards"
                                               ng-init="formData.sizes.wild_cards='<?= ($draw->size) ? $draw->size->wild_cards : ""?>'">
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control">
                                    <label for="has_final_round"><?= LangHelper::get('quali_to_finals', 'Qualifications has final round')?></label>
                                    <div class="input-control text" style="width: 96%;">
                                        <?= Form::select('plays_to_finals', $quali_has_final_round, NULL, array('ng-model' => 'formData.qualifications.has_final_round')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="break-draw">
                    <div class="row">
                        <div class="half" style="width: 68%">
                            <div class="half">
                                <div class="form-control">
                                    <label
                                        for="match_rules"><?= LangHelper::get('match_rules', 'Match rules') ?></label>

                                    <div class="input-control text" style="width: 96%;">

                                        <?= Form::select('match_rule', $match_rules, 0, array('required',
                                            'ng-model' => 'formData.draws.match_rule',
                                            'ng-init'  => 'formData.draws.match_rule="' . $draw->match_rule . '"')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="half">
                                <div class="form-control">
                                    <label for="match_rules"><?= LangHelper::get('set_rules', 'Set rules') ?></label>

                                    <div class="input-control text" style="width: 96%;">
                                        <?= Form::select('set_rule', $set_rules, 0, array('required',
                                            'ng-model' => 'formData.draws.set_rule',
                                            'ng-init'  => 'formData.draws.set_rule="' . $draw->set_rule . '"')); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="half pull-right" style="width: 30%">
                            <div class="form-control">
                                <label for="match_rules"><?= LangHelper::get('game_rules', 'Game rules') ?></label>

                                <div class="input-control text" style="width: 96%;">
                                    <?= Form::select('game_rule', $game_rules, 0, array('required',
                                        'ng-model' => 'formData.draws.game_rule',
                                        'ng-init'  => 'formData.draws.game_rule="' . $draw->game_rule . '"')); ?>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="gap-top30">
                            <button type="submit" class="button right big next-yellow-arrow yellow-color dark-col-bg">
                                <?= LangHelper::get('save', 'Save') ?>
                            </button>
                        </div>
                    </div>
                    <form-errors></form-errors>
                    <div class="show-errors form-warnings" ng-if="Sum1Error == true">
                        <span class="yellow-color"><?= LangHelper::get('warning', 'Warning') ?>: </span>
                        <span class="black-color">Sum of Direct From and Qualifiers To and Wild Cards and Special Exempts must be equal to Total Acceptances!</span>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<script src="/js/ang/drawsForm.js"></script>
<script>
    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['drawsFormApp']);
    });
</script>
