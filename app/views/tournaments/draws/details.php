<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('draw_details', 'Draw details')?></h3>
               <span class="line"></span>
             </div>
        </div>

        <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('category', 'Category')?>:</span> <?= $draw->category->name ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('type', 'Type')?>:</span> <?= $draw->typeName() ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('gender', 'Gender')?>:</span> <?= $draw->genderName() ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('total_acceptances', 'Total acceptances')?>:</span> <?= ($draw->size) ? $draw->size->total : "" ?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('direct_acceptances', 'Direct acceptances')?>:</span> <?= ($draw->size) ? $draw->size->direct_acceptances : ""?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('draw_id', 'Draw ID')?>:</span> <?= $draw->unique_draw_id ?></p>
                </div>
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('qualifiers', 'Qualifiers')?>:</span> <?= ($draw->size) ? $draw->size->accepted_qualifiers : ""?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('wild_cards', 'Wild cards')?>: </span><?= ($draw->size) ? $draw->size->wild_cards : ""?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('special_exempts', 'Special exempts')?>: </span><?= ($draw->size) ? $draw->size->special_exempts : ""?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('on_site_direct_acceptances', 'On-site direct acceptances')?>:</span> <?= ($draw->size) ? $draw->size->onsite_direct : ""?></p>
                    <p><span class="color-rfet"><?= LangHelper::get('total_qualifiers', 'Total qualifiers')?>:</span> <?= ($draw->qualification) ? $draw->qualification->draw_size : ""?></p>
                </div>
            </div>

            <div class="row gap-top15">
                <a class="place-right rfet-button rfet-yellow call rfet-block" href="/tournaments/edit-draw/<?= $draw->id?>">Edit</a>
            </div>

        </div>
    </div>
</div>
 
