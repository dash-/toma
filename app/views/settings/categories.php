<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>


        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>
                    <?= LangHelper::get('settings', 'Settings') ?>
                    -
                    <?= LangHelper::get('categories', 'Categories') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>


        <div class="row">
            <?= Form::open() ?>
            <div class="row">

                <table class="table bg-dark tournament-table categories-table">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('category_name', 'Category name') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_from', 'Range from') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_to', 'Range to') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_for_points_from', 'Range for points from') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_for_points_to', 'Range for points to') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_for_signup_from', 'Range for signup from') ?></th>
                        <th class="text-left"><?= LangHelper::get('range_for_signup_to', 'Range for signup to') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($categories as $category): ?>

                        <tr>
                            <td><?= $category->name ?></td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[from]"
                                       value="<?= $category->range_from ?>"/>
                            </td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[to]"
                                       value="<?= $category->range_to ?>"/>
                            </td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[points_from]"
                                       value="<?= $category->range_for_points_from ?>"/>
                            </td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[points_to]"
                                       value="<?= $category->range_for_points_to ?>"/>
                            </td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[signup_from]"
                                       value="<?= $category->range_for_signup_from ?>"/>
                            </td>
                            <td>
                                <input type="text" class="small-input pad-left10" name="<?= $category->name ?>[signup_to]"
                                       value="<?= $category->range_for_signup_to ?>"/>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <input type="submit" class="button btn btn-primary info" value="Save"/>
                <?= Form::close() ?>
            </div>

        </div>
    </div>
</div>
