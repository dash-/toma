<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>
                    <?= LangHelper::get('settings', 'Settings') ?>
                    -
                    <?= LangHelper::get('coefficient', 'Coefficient') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <form ng-submit="formSubmit($event, '<?= Request::url() ?>')"
                  ng-controller="SettingsController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('coefficient', 'Coefficient') ?> *</label>
                                <div class="input-control text">
                                    <input type="text"
                                           ng-init="formData.coefficient='<?= $coef?>'"
                                           ng-model="formData.coefficient" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <button type="submit" class="button medium next-yellow-arrow yellow-color dark-col-bg gap-left10">
                    <?= LangHelper::get('save', 'Save') ?>
                </button>
            </form>
        </div>
    </div>
</div>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>