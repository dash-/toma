<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('settings', 'Settings') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row">
            <a href="/settings/categories" class="button btn-success info call">
                <?= LangHelper::get('category_settings', 'Category settings')?>
            </a>
            <a href="/settings/coefficient" class="button btn-success info call">
                <?= LangHelper::get('coefficient_settings', 'Coefficient settings')?>
            </a>
            <?php if (Auth::user()->hasRole('superadmin')): ?>
                <a class="button btn-success info call"href="/tournaments/tournament-types">
                    <?= LangHelper::get('tournament_types', 'Tournament types') ?>
                </a>
            <?php endif ?>
        </div>


        <div class="row">
            <div class="clearfix">
                <?php if (Auth::getUser()->hasRole("superadmin") && false): ?>
                <a class="button info right" id="recalculate" href="/settings/recalculate-ranking"  msg="<?= LangHelper::get('recalculate_rankings_for_players', 'Recalculate rankings for players') ?>">
                    <?= LangHelper::get('recalculate_rankings_for_players', 'Recalculate rankings for players') ?>
                </a>

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
