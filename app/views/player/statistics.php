<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('my_statistics', 'My statistics') ?></h3>
                <span class="line"></span>
                <a href="/player/edit-player-statistics" class="button rfet-button rfet-yellow edit-statistics"><?= LangHelper::get('edit_statistics', 'Edit statistics') ?></a>
            </div>
        </div>
        <div class="tournament-second pad-bottom20 gap-top20">
            <div id="player_height_statistics" class="line_chart"></div>

        </div>
    </div>

</div>

<script>

    $(function () {
        setTimeout(function () {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'player_height_statistics',
                    backgroundColor: null
                },
                title: "Players ranking points",
                series: [
                    {
                        data: [<?= implode(",", $height) ?>],
                        name: 'Height',
                        color: "#ffc627",
                        tooltip: {
                            valueSuffix: ' cm'
                        }

                    },
                    {
                        data: [<?= implode(",", $weight) ?>],
                        name: 'Weight',
                        color: "#c33a32",
                        tooltip: {
                            valueSuffix: ' kg'
                        }
                    }],
                xAxis: {
                    categories: ['<?= implode("', '", $date_range) ?>'],
                    labels: {enabled: true, y: 20, rotation: -45, align: 'right'}
                },
                yAxis: {
                    title: ""
                }

            });

            $("text:contains(Highcharts.com)").remove();

        }, 1)
    });
</script>