<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('my_ranking', 'My ranking') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <?= Form::open(); ?>

        <div class="row">
            <div class="third">
                <div class="form-control">
                    <label for="from_date">
                        <?= LangHelper::get('choose_date_range_from', 'Choose date range from:') ?>

                    </label>
                    <?= Form::select('from_date', array(null => "Show all") + $date_range_full, $date_from); ?>
                </div>

            </div>
            <div class="third">
                <div class="form-control">
                    <label for="to_date">
                        <?= LangHelper::get('choose_date_range_to', 'Choose date range to:') ?>

                    </label>
                    <?= Form::select('to_date', array(null => "Show all") + $date_range_full, $date_to); ?>
                </div>

            </div>
            <div class="third">
                <div class="form-control">
                    <?php if($show_reset): ?>
                    <a href="/player/my-statistics" class="button primary large dark"
                       style="margin-top: 30px;"><?= LangHelper::get('reset', 'Reset') ?></a>
                    <?php endif; ?>
                    <button type="submit" class="btn place-right primary large dark"
                            style="margin-top: 30px;"><?= LangHelper::get('filter', 'Filter') ?></button>


                </div>
            </div>
        </div>
        <?= Form::close(); ?>
        <div id="ranking" class="line_chart ranking-responsive"></div>
        <div id="ranking_points" class="line_chart ranking-responsive"></div>

    </div>
</div>


<script>

    $(function () {
        $('select').select2();
        setTimeout(function () {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'ranking',
                    backgroundColor: null
                },
                title: "Players ranking",
                series: [{
                    data: [<?= implode(",", $ranking) ?>],
                    name: 'Ranking',
                    color: "#c33a32",


                }],
                xAxis: {
                    categories: ['<?= implode("', '", $date_range) ?>'],
                    labels: {enabled: true, y: 20, rotation: -45, align: 'right'}
                },
                yAxis: {
                    min: 0,
                    title: ""
                },

            });
            new Highcharts.Chart({
                chart: {
                    renderTo: 'ranking_points',
                    backgroundColor: null
                },
                title: "Players ranking points",
                series: [{
                    data: [<?= implode(",", $ranking_points) ?>],
                    name: 'Ranking points',
                    color: "#ffc627",

                }],
                xAxis: {
                    categories: ['<?= implode("', '", $date_range) ?>'],
                    labels: {enabled: true, y: 20, rotation: -45, align: 'right'}
                },
                yAxis: {
                    title: ""
                }

            });

            $("text:contains(Highcharts.com)").remove();

        }, 1)
    });
</script>