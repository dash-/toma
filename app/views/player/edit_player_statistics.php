<div class="container ang">
    <div class="grid" ng-controller="IndexController" data-ng-init="init('/player/edit-player-statistics-data')">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_player_statistics', 'Edit player statistics') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="tournament-second pad-bottom20 gap-top20" ng-cloak>
            <form name="createForm" novalidate ng-submit="formSubmit($event, '<?= Request::url() ?>')">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="form-control move-bottom-30">
                        <span class="form-error">{{errors[0].key}}</span>

                        <div class="text input-control" style="width:60%">
                            <input type="text" required ng-model="formData.height" placeholder="Height" class="input-control text input cursor-text" style="width:30%;"/>
                            <input type="text" required ng-model="formData.weight" placeholder="Weight" class="input-control text input cursor-text" style="width:30%;" />

                            <input type="text" min-date="minDate"
                                   datepicker-popup="dd.MM.yyyy" ng-required="true"
                                   ng-model="formData.date"
                                   is-open="opened['date']" show-button-bar="false"
                                   ng-click="openDatePicker($event, 'date')" formatdate
                                   class="input-control text input cursor-text"
                                style="width:30%">
                        </div>
                    </div>
                    <button type="submit" class="button primary big call next button-yelow-pad" style="margin-bottom:100px">
                        <?= LangHelper::get('save', 'Save') ?>
                    </button>
                </fieldset>
            </form>

            <div class="row"> 
                <table class="table tournament-table" style="width:50%">
                <tr>
                <thead>
                    <th class="text-left"><?= LangHelper::get('height','Height') ?></th>
                    <th class="text-left"><?= LangHelper::get('weight','Weight') ?></th>
                   <!--  <th class="text-left"><#?= LangHelper::get('handed','Handed') ?></th> -->
                    <th class="text-left"><?= LangHelper::get('date','Date') ?></th>
                    <th class="text-right"><?= LangHelper::get('options','Options') ?></th>
                </thead>
                </tr>
                    <tr ng-repeat="player_statistic in player_statistics track by $index">
                        <td class="text-left">{{ player_statistic.height }} cm</td>
                        <td class="text-left">{{ player_statistic.weight }} kg</td>
                     <!--    <td class="text-left">{{ player_statistic.handed }}</td> -->
                        <td class="text-left">{{ player_statistic.date }}</td>
                        <td class="text-right">
                            <a class="danger-color" ng-click="delete($event, $index)" href="/player/player-stat/{{player_statistic.id}}">
                                <?= LangHelper::get('delete', 'Delete')?>
                            </a>
                        </td>
                    </tr>
                    <!--                    <div class="note-holder">-->
                    <!--                        <a class="delete_note" ng-show="{{note.note_user == note.current_user}}"-->
                    <!--                           ng-click="delete($event, $index)" href="/tournaments/note/{{note.id}}">-->
                    <!--                            <i class="icon-remove"></i>-->
                    <!--                        </a>-->
                    <!---->
                    <!--                        <p class="note-creator">-->
                    <!--                            {{note.user_name}} | {{note.created}}-->
                    <!--                        </p>-->
                    <!---->
                    <!--                        <p class="note-text">{{note.text}}</p>-->
                    <!--                    </div>-->
                </table>
            </div>
        </div>

    </div>
</div>


<script src="/js/ang/playerStatistics.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['playerStatisticsApp']);
    });
</script>