<?php
	$presenter = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

@if ($paginator->getLastPage() > 1)
<?php $previousPage = ($paginator->getCurrentPage() > 1) ? $paginator->getCurrentPage() - 1 : 1; ?>  
<ul class="pagination" >  
	<li>
  		<a ng-click="prevPage()">&lsaquo;</a>
	</li>
  @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
  <a href="{{ $paginator->getUrl($i) }}"
    class="item{{ ($paginator->getCurrentPage() == $i) ? ' active' : '' }}">
      {{ $i }}
  </a>
  @endfor
  <li>
  	<a ng-click="nextPage()">&rsaquo;</a>
  </li>
</ul>  
@endif