<div class="container ang">
    <div class="grid" ng-controller="tableController" data-ng-init="init(1)"  data-scUrl="/notifications/data">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament place-left">
               <h3><?= LangHelper::get('notifications', 'Notifications')?></h3>
               <span class="line"></span>
            </div>
            <a class="button danger place-right gap-left15" ng-click="removeAll($event)" ng-hide="rows.length < 1" href="/notifications/all"><?= LangHelper::get('delete_all_alerts', 'Delete all alerts')?></a>
            <a class="button success place-right" ng-click="showAll($event)" ng-hide="showDeleted" ><?= LangHelper::get('show_deleted_all_alerts', 'Show deleted alerts')?></a>
            <a class="button place-right" ng-click="resetSearch(1)" ng-show="showDeleted" ><?= LangHelper::get('show_all_alerts', 'Show alerts')?></a>

            
        </div>

        <div class="row">
            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('date', 'Date')?></th>
                    <th class="text-left"><?= LangHelper::get('type', 'Type')?></th>
                    <th class="text-left"><?= LangHelper::get('text', 'Text')?></th>
                    <th class="text-right"><?= LangHelper::get('options', 'Options')?></th>
                </tr>
                </thead>

                <tbody>
                    <tr ng-repeat="notif in rows track by $index">
                        <td class="pad-right100">{{ notif.created_at | asDate | date:'dd.MM.yyyy HH:mm' }}</td>
                        <td class="pad-right100">{{ notif.type.name }}</td>
                        <td class="pad-right100">
                            <a href="{{ notif.link }}" class="call yellow-color link-underline">
                                {{ notif.text }}
                            </a>
                        </td>
                        <td class="text-right transparent">
                            <a class="rfet-button rfet-danger" ng-click="remove($event, $index)" href="/notifications/notif/{{ notif.id }}"><?= LangHelper::get('delete', 'Delete')?></a>
                        </td>
                    </tr>
            </table>
            <h3 class="fg-white text-center" ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data')?></h3>

            <div class="pagination" ng-hide="rows.length < 1">
                <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage" max-size="maxSize" boundary-links="true" rotate="false" items-per-page="counter"></pagination>
            </div>
        </div>
    </div>

</div>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>