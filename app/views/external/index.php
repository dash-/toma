<div class="row">
    <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
</div>
<div class="row">
    <div class="clearfix">
        <div class="empty-space"></div>
    </div>
</div>
<div class="tournament-first">
    <div class="table-title tournament">
        <h3><?= LangHelper::get('external_points_assignment', 'External Points Assignment') ?></h3>
        <span class="line"></span>
    </div>
</div>

<div class="tournament-second">
    <div class="row">
        <a class="btn rfet-button rfet-yellow call" href="/external/not-assigned">Points not yet assigned</a>
        <a class="btn rfet-button rfet-yellow call" href="/external/assigned">Assigned external points</a>
    </div>
    <div>

        <h3>
            <?= isset($data) ? $data : "" ?>
            <?= isset($single_error) ? $single_error : "" ?>
        </h3>
    </div>
    <?= Form::open(array('method' => 'POST')) ?>
    <div class="row">
        <div class="third">
            <div class="form-control">
                <div class="input-control text">
                    <input type="text" name="licence_number"
                           value="<?= isset($input) ? $input['licence_number'] : "" ?>"
                           placeholder="<?= LangHelper::get("licence_number", "Licence Number") ?>" required>

                    <?= $errors->first('licence_number') ?>
                </div>
            </div>
        </div>
        <div class="third">
            <div class="form-control">
                <div class="input-control text">
                    <input type="text" name="points_number" value="<?= isset($input) ? $input['points_number'] : "" ?>"
                           placeholder="<?= LangHelper::get("points_number", "Points") ?>" required>
                    <?= $errors->first('points_number') ?>


                </div>
            </div>
        </div>
        <div class="third">
            <div class="form-control">
                <div class="input-control text">

                    <?= Form::select('points_type', TournamentHelper::external_points_type_names(), isset($input) ? $input['points_type'] : null); ?>
                    <?= $errors->first('points_type') ?>

                </div>
            </div>
        </div>
    </div>

    <div class="row pull-right gap-right10">
        <button type="submit" class="primary"><?= LangHelper::get('assign_points', 'Assign Points') ?></button>

    </div>

    <?= Form::close() ?>

</div>
<script>
    $(document).ready(function () {
        $("select").select2({});
    });
</script>