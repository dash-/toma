<div class="container ang">
    <div class="grid datatable" ng-controller="tableController" data-ng-init="init(1)"
         data-scUrl="/external/data/<?= $assigned ?>">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('not_assigned_external_points', 'Not assigned external points') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="table-title players">
            <div class="results-number input-control select">

                <p class="fg-yellow gap-top24"><?= LangHelper::get('total_results', 'Total results') ?>:
                    {{totalItems}}</p>
            </div>
        </div>
        <div class="tournament-second">
            <div class="row">
                <a class="btn rfet-button rfet-yellow call" href="/external/index">Assign points</a>
                <a class="btn rfet-button call <?= !$assigned ? "rfet-danger" : "rfet-yellow" ?>" href="/external/not-assigned">Points not yet assigned</a>
                <a class="btn rfet-button call <?= $assigned ? "rfet-danger" : "rfet-yellow" ?>" href="/external/assigned">Assigned external points</a>
            </div>
            <div class="players tabScrl">
                <form ng-submit="search($event, 1)" class="tabScrl" name="searchForm">
                    <table class="table bg-dark striped table-scrollable-w768">
                        <thead>
                        <tr>
                            <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                            <th class="text-left">
                                <div class="input-control text">
                                    <input type="text" ng-model="searchText.surname"
                                           placeholder="<?= LangHelper::get("search_by_surname", "Search by surname") ?>">
                                </div>
                                <a ng-click="doOrder('contacts.surname')" href="#">
                                    <?= LangHelper::get('surname', 'Surname') ?> <i
                                        ng-class="sortBy('contacts.surname')"></i>
                                </a>
                            </th>
                            <th class="text-left">
                                <div class="input-control text">
                                    <input type="text" ng-model="searchText.name"
                                           placeholder="<?= LangHelper::get("search_by_name", "Search by name") ?>">
                                </div>
                                <a ng-click="doOrder('contacts.name')" href="#">
                                    <?= LangHelper::get('name', 'Name') ?> <i ng-class="sortBy('contacts.name')"></i>
                                </a>
                            </th>
                            <?php if (!Auth::user()->hasRole('referee')): ?>
                                <th class="text-left">
                                    <div class="input-control text">
                                        <input type="text" ng-model="searchText.licence_number"
                                               placeholder="<?= LangHelper::get("search_by_licence_number", "Search by licence number") ?>">
                                    </div>
                                    <a ng-click="doOrder('licence_number')" href="#">
                                        <?= LangHelper::get('licence_number', 'Licence number') ?> <i
                                            ng-class="sortBy('licence_number')"></i>

                                    </a>
                                </th>
                            <?php endif ?>
                            <th class="text-left">

                                <a ng-click="doOrder('tournament_type')" href="#">
                                    <?= LangHelper::get("tournament_type", "Tournament type") ?> <i ng-class="sortBy('tournament_type')"></i>

                                </a>
                            </th>
                            <th class="text-left full">

                                <div class="input-control text">
                                    <input type="text" ng-model="searchText.external_points_acquired"
                                           placeholder="<?= LangHelper::get("search_by_points", "Search by points") ?>">
                                </div>
                                <a ng-click="doOrder('external_points_acquired')" href="#">
                                    <?= LangHelper::get('points_acquired', 'Points acquired') ?> <i ng-class="sortBy('external_points_acquired')"></i>

                                </a>
                            </th>
                            <th class="text-left full">
                                <div class="input-control text">
                                    <input type="text" ng-model="searchText.total_calculated_points"
                                           placeholder="<?= LangHelper::get("search_by_total_points", "Search by total points") ?>">
                                </div>
                                <a ng-click="doOrder('total_calculated_points')" href="#">
                                    <?= LangHelper::get('total_points_acquired', 'Total points') ?> <i ng-class="sortBy('total_calculated_points')"></i>

                                </a>
                            </th>
                            <th class="text-left last-col">
                                <div class="input-control text">
                                    <button type="submit" ng-click="search($event, 1)"
                                            ng-class="scSearch == 1 ? 'split-button' : ''"
                                            class="primary dark full"><?= LangHelper::get('filter', 'Filter') ?></button>
                                    <a ng-click="resetSearch()" ng-show="scSearch == 1"
                                       class="button primary split-button reset-button"
                                       href="#"><?= LangHelper::get('reset_search', 'Reset search') ?></a>
                                </div>
                                <a class="options"><?= LangHelper::get('options', 'Options') ?></a>
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="player in rows track by $index">
                            <td ng-class="{fined: player.penalized}">{{ player.surname }}</td>
                            <td>{{ player.name }}</td>

                            <?php if (!Auth::user()->hasRole('referee')): ?>
                                <td>{{ player.licence_number }}</td>
                            <?php endif ?>

                            <td>{{ player.tournament_type | externalTourType  }}</td>
                            <td>{{ player.external_points_acquired }}</td>
                            <td>{{ player.total_calculated_points }}</td>
                            <td class="text-left input-control transparent">
                                <a class="button primary info inline-block" ng-href="/players/info/{{player.id}}">
                                    <?= LangHelper::get('info', 'Info') ?>
                                </a>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </form>

                <h3 class="fg-white text-center"
                    ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data') ?></h3>

                <div class="input-control back-in-pagination" ng-show="rows.length > 0">
                    <a class="button primary yellow back big call" href="/"><?= LangHelper::get('back', 'BACK') ?></a>
                </div>
                <div class="pagination" ng-show="rows.length > 0">
                    <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage"
                                max-size="maxSize"
                                boundary-links="true" rotate="false" items-per-page="counter"></pagination>
                </div>
                <div class="input-control next-button marginTop-50" ng-show="rows.length > 0">
                    <a class="button primary big call next"
                       href="<?= URL::to('players/create') ?>"><?= LangHelper::get('submit_new_player', 'Submit new player') ?></a>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>