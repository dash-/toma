<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="UTF-8">
    <title><?= $title?></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100&subset=latin,latin-ext,vietnamese' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700,800,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <input type="hidden" id="port_id" value="<?= Config::get('app.port_id'); ?>" />

    <?php foreach ($styles as $file => $type) echo HTML::style( $file, array('media' => $type)), "\n" ?>
    <?php foreach ($external_scripts as $file) echo HTML::script($file), "\n" ?>
</head>
<body>
<?= PageHelper::CheckBrowser() ?>

<?= View::make('public/message_box/global_message', [
    'message' => $message,
])?>

<?= View::make('partials/public/_navigation', array(
	'tournament_slug' => $tournament_slug,
	'controller'      => $controller,
))?>

<div id="all-content" class="container">
    <?= $content?>
</div>

<?php foreach ($scripts as $file) echo HTML::script($file), "\n" ?>

</body>
</html>
