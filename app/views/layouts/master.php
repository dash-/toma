<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <?php foreach ($styles as $file => $type) echo HTML::style($file, array('media' => $type)), "\n" ?>
    <?php foreach ($external_scripts as $file_name => $type_name) echo HTML::script($file_name, ['type' => $type_name]), "\n" ?>
    <?php foreach ($scripts as $file => $type) echo HTML::script($file, ['type' => $type]), "\n" ?>
</head>
<body class="<?= !Auth::user() ? "loginpage" : "" ?>">
<?= PageHelper::CheckBrowser() ?>
<input type="hidden" id="port_id" value="<?= Config::get('app.port_id'); ?>"/>
<div class="metro <?= !Auth::user() ? "fullwidth" : "" ?>">
    <?php if (Auth::user()): ?>

        <?= View::make('partials.navigation') ?>
    <?php endif; ?>
    <div id="wrap-all">
        <div id="all-content">

            <?= $content ?>
        </div>
    </div>
</div>
</body>
</html>
