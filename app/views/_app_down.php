
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>RFET - Down</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?= HTML::style('css/offline_css.css', array('media' => 'screen, projection')) ?>
<?= HTML::style('css/metro-bootstrap.css', array('media' => 'screen, projection')) ?>
<?= HTML::style('css/main.css', array('media' => 'screen, projection')) ?>
<?= HTML::style('css/main-responsive.css', array('media' => 'screen, projection')) ?>

</head>
<body>
<div class="metro <?= !Auth::user() ? "fullwidth" : "" ?>">
    <div id="wrap-all">
        <div id="all-content">
<div class="container">
    <div class="grid login">
        <div class="login-middle">
            <div class="login-top">
                <div style="max-width:100%; height:auto;">
                    <img class="logoBig" src="css/images/TomaLogoLogin.png" title="Tennis Organization & Management Application" alt="Tennis Organization & Management Application"/>

                </div>
            </div>


        </div>
        <h3 style="text-align: center;">
            Application upgrade in progress...
        </h3>
    </div>
</div>

        </div>
    </div>
</div>
</body>
</html>
