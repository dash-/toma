<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('add_sponsor', 'Add sponsor')?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="tournament-second">
            <form name="createForm" action="<?= Request::url() ?>" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="row">
                            <div class="half">
                                <label for="title"><?= LangHelper::get('title', 'Title')?></label>
                                <div class="input-control text">
                                    <input type="text" name="title">
                                </div>
                            </div>
                            <div class="half">
                                <label for="region"><?= LangHelper::get('region', 'Region')?></label>
                                <?= Form::select('region_id', $regions_array, NULL, array()); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <label for="city"><?= LangHelper::get('city', 'City')?></label>
                                <div class="input-control text">
                                    <input type="text" name="city">
                                </div>
                            </div>
                            <div class="half">
                                <label for="address"><?= LangHelper::get('address', 'Address')?></label>
                                <div class="input-control text">
                                    <input type="text" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone')?></label>
                                <div class="input-control text">
                                    <input type="text" name="phone">
                                </div>
                            </div>
                            <div class="half">
                                <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                                <div class="input-control text">
                                    <input type="text" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="half">
                                <label for="website"><?= LangHelper::get('web', 'Web')?></label>
                                <div class="input-control text">
                                    <input type="text" name="website">
                                </div>
                            </div>

                            <div class="half">
                                <label for="image"><?= LangHelper::get('image', 'Image')?></label>
                                <div class="input-control file">
                                    <input type="file" name="image">
                                    <button class="btn-file"></button>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="clear place-right primary large gap-top10">
                            <?= LangHelper::get('save', 'Save')?>
                        </button>
                    <form-errors></form-errors>

                </fieldset>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $("select").select2({});
});

</script>