<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>

        <div class="row">
            <div class="clearfix">
                <a class="button info place-right"
                   href="/sponsors/create"><?= LangHelper::get('add_sponsor', 'Add sponsor') ?></a>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('sponsors', 'Sponsors') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="row tabScrl">
            <table class="table bg-dark tournament-table table-scrollable-w768" ng-controller="SponsorsController">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                    <th class="text-left"><?= LangHelper::get('region', 'Region') ?></th>
                    <th class="text-left"><?= LangHelper::get('city', 'City') ?></th>
                    <th class="text-left"><?= LangHelper::get('address', 'Address') ?></th>
                    <th class="text-left"><?= LangHelper::get('phone', 'Phone') ?></th>
                    <th class="text-left"><?= LangHelper::get('email', 'Email') ?></th>
                    <th class="text-left"><?= LangHelper::get('website', 'Website') ?></th>
                    <th class="text-right" width="20%"><?= LangHelper::get('options', 'Options') ?></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($sponsors as $sponsor): ?>
                    <tr>
                        <td><?= $sponsor->title ?></td>
                        <td><?= $sponsor->regionName() ?></td>
                        <td><?= $sponsor->city ?></td>
                        <td><?= $sponsor->address ?></td>
                        <td><?= $sponsor->phone ?></td>
                        <td><?= $sponsor->email ?></td>
                        <td><?= $sponsor->website ?></td>
                        <td class="text-right transparent">
                            <?php if ($sponsor->image): ?>
                                <a class="rfet-button rfet-yellow"
                                   ng-click='openModal("/sponsors/view-image", "<?= $sponsor->id ?>")'>
                                    <?= LangHelper::get('show_image', 'Show image') ?>
                                </a>
                            <?php endif ?>

                            <a class="rfet-button rfet-yellow" href="/sponsors/edit/<?= $sponsor->id ?>">
                                <?= LangHelper::get('edit', 'Edit') ?>
                            </a>

                        </td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['sponsorsApp']);
    });
</script>