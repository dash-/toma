<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_sponsor', 'Edit sponsor')?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="tournament-second">
            <form name="editForm" action="<?= Request::url()?>" method="POST" enctype="multipart/form-data">

                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <input type="hidden" name="_method" value="PUT">
                    <fieldset>
                        <div class="row">
                            <div class="half">
                                <label for="title"><?= LangHelper::get('title', 'Title')?></label>
                                <div class="input-control text">
                                    <input type="text" name="title" value="<?= $sponsor->title?>">
                                </div>
                            </div>
                            <div class="half">
                                <label for="region"><?= LangHelper::get('region', 'Region')?></label>
                                <?= Form::select('region_id', $regions_array, $sponsor->region_id, array()); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <label for="city"><?= LangHelper::get('city', 'City')?></label>
                                <div class="input-control text">
                                    <input type="text" value="<?= $sponsor->city?>" name="city">
                                </div>
                            </div>
                            <div class="half">
                                <label for="address"><?= LangHelper::get('address', 'Address')?></label>
                                <div class="input-control text">
                                    <input type="text" value="<?= $sponsor->address?>" name="address">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="half">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone')?></label>
                                <div class="input-control text">
                                    <input type="text" value="<?= $sponsor->phone?>" name="phone">
                                </div>
                            </div>
                            <div class="half">
                                <label for="email"><?= LangHelper::get('email', 'Email')?></label>
                                <div class="input-control text">
                                    <input type="text" value="<?= $sponsor->email?>" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="half">
                                <label for="website"><?= LangHelper::get('web', 'Web')?></label>
                                <div class="input-control text">
                                    <input type="text" name ="website" value="<?= $sponsor->website?>">
                                </div>
                            </div>
                            
                            <div class="half">
                                <label for="image"><?= LangHelper::get('image', 'Image')?></label>
                                <div class="input-control file">
                                    <input type="file" name="image">
                                    <button class="btn-file"></button>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="clear place-right primary large gap-top10">
                            <?= LangHelper::get('save', 'Save')?>
                        </button>
                    <form-errors></form-errors>

                </fieldset>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $("select").select2({});
});

</script>