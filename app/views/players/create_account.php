<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>


        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('create_player_account', 'Create player account') ?></h3>
                <span class="line"></span>
            </div>
        </div>


        <div class="tournament-second" ng-controller="createController">
            <form name="createForm" ng-class="{ 'show-errors': showErrors }" ng-submit="createUser()" novalidate>
                <input type="hidden" ng-model="formData._token" ng-init="formData._token='<?= csrf_token()?>'">
                <input type="hidden" ng-model="formData.player.id" ng-init="formData.player.id=<?= $player->id?>">
                <input type="hidden" ng-model="formData.player.contact_id" ng-init="formData.player.contact_id=<?= $player->contact_id?>">
                <fieldset>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?></label>

                                <div class="input-control text">
                                    <input type="text"
                                           ng-init="formData.contact.name='<?= $player->contact->name ?>'"
                                           ng-model="formData.contact.name" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="surname"
                                           ng-init="formData.contact.surname='<?= $player->contact->surname ?>'"
                                           ng-model="formData.contact.surname" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="username"><?= LangHelper::get('username', 'Username') ?></label>

                                <div class="input-control text">
                                    <input type="text"
                                           ng-model="formData.users.username" name="username" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" ng-model="formData.users.email"
                                           ng-init="formData.users.email='<?= $player->contact->email ?>'" name="email"
                                           required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="password"><?= LangHelper::get('password', 'Password') ?></label>

                                <div class="input-control text">
                                    <input type="password" ng-model="formData.users.password" name="password" required>
                                </div>
                            </div>
                        </div>

                        <div class="half">
                            <div class="form-control">
                                <label for="role"><?= LangHelper::get('role', 'Role') ?></label>

                                <div class="input-control text">

                                    <input type="text" name="role"
                                           ng-init="formData.roles.role='player'"
                                           ng-model="formData.roles.role" disabled>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form-errors></form-errors>
                </fieldset>
                <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                        style="margin-top: 38px;"><?= LangHelper::get('save', 'Save') ?></button>
            </form>
        </div>


    </div>
</div>


<script src="/js/ang/create.js"></script>

<script>
    $(function () {
        angular.element(document).ready(function () {
            angular.bootstrap('.ang', ['create']);
        });
    })
</script>
