<div class="row players-info" style="padding-top:50px">

    <div class="playerImage">
        <?php if ($player->contact->image_link): ?>
            <p><a style="display: inline;"
                  href="/uploads/images/user/full/<?= $player->contact->image_link ?>"><img
                        src="/uploads/images/user/small_thumbnails/<?= $player->contact->image_link ?>"/>
                </a>
            </p>
        <?php endif; ?>
    </div>
    <div class="leftPartOne">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('name', 'Name') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('surname', 'Surname') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('city', 'City') ?>
                    :
                </td>
            </tr>
        </table>
    </div>

    <div class="leftPartTwo">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->contact->name ?></td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->surname ?></td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->date_of_birth ?></td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->contact->city ?></td>
            </tr>
        </table>
    </div>

    <div class="rightPartOne">

        <table>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('licence_number', 'Licence number') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('sex', 'Sex') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('ranking', 'Ranking') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('ranking_points', 'Ranking points') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('quarter_ranking', 'Quarter ranking') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('quarter_ranking_move', 'Quarter ranking move') ?>:
                </td>
            </tr>
        </table>
    </div>
    <div class="rightPartTwo">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px; height:20px;">
                    &nbsp;<?= $player->licence_number ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->sex ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->ranking ?>
                    <span class="rankingsign <?= $sign[1] ?>">
                        <?= $player->ranking_move ?><span class="rankingicon <?= $sign[1] ?> "></span>
                    </span>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->ranking_points ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->quarter_ranking ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->quarter_ranking_move ?>
                </td>
            </tr>
        </table>
    </div>
</div>