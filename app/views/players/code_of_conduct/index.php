<div class="container ang">
    <div class="grid" ng-controller="IndexController"
         data-ng-init="init('/players/penalty-notes-data/<?= $player->id ?>')">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= (LangHelper::get('player_code_of_conduct', 'Player code of conduct')) ?>
                    - <?= $player->contact->full_name() ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row clearfix pad-top20" ng-cloak>
            <div class="half pad-bottom20 gap-top20">
                <form name="createForm" novalidate ng-submit="formSubmit($event, '<?= Request::url() ?>')">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="form-control move-bottom-30">
                            <?= Form::select('tournament_id', $tournaments, NULL, array('ng-model' => 'formData.tournament_id')); ?>
                        </div>
                        <div class="form-control move-bottom-30">
                            <span class="form-error">{{errors[0].key}}</span>
                            <div class="input-control text">
                            <input type="text" required ng-model="formData.subject" placeholder="Note subject">
                            </div>
                        </div>

                        <div class="form-control move-bottom-30">
                            <span class="form-error">{{errors[1].key}}</span>
                            <div class="input-control textarea">
                            <textarea type="text" required ng-model="formData.text"
                                      placeholder="Your note here..."></textarea>
                            </div>
                        </div>

                        <div class="form-control place-left">
                            <div class="input-control checkbox">
                                <label>
                                    <input type="checkbox" ng-model="formData.penalized">
                                    <span class="check big"></span>
                                    <?= LangHelper::get('penalize', 'Penalize')?>
                                </label>
                            </div>
                        </div>
                        <button type="submit"
                                class="rfet-button rfet-yellow place-right pad-left20 pad-right20"><?= LangHelper::get('save', 'Save') ?></button>
                    </fieldset>
                </form>
            </div>

            <div class="half pad-bottom20 gap-top20">
                <div ng-repeat="note in notes track by $index">
                    <div class="note-holder">
                        <a class="delete_note" ng-show="{{note.note_user == note.current_user}}"
                           ng-click="delete($event, $index)" href="/players/penalty-note/{{note.id}}">
                            <i class="icon-remove"></i>
                        </a>

                        <p class="note-creator">
                            {{note.user_name}} | {{note.created}}
                        </p>

                        <p class="note-subject">
                            {{note.subject}}
                            <span ng-show="note.tournament">| <?= LangHelper::get('tournament', 'Tournament')?>: {{note.tournament}}</span>
                        </p>
                        <p class="note-text">{{note.text}}</p>

                        <p class="note-penalized" ng-show="note.penalized"><?= LangHelper::get('penalized', 'Penalized')?></p>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>


<script src="/js/ang/notes.js"></script>
<script>
    $(document).ready(function(){
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['notesApp']);
    });
</script>