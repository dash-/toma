<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>


        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= $player->contact->full_name()?>: <?= LangHelper::get('match_scores', 'Match scores') ?></h3>
                <span class="line"></span>
            </div>
        </div>


        <div class="tournament-second gap-top20">
            <h3><?= LangHelper::get('match', 'Match')?></h3>


            <div id="game_scores" class="line_chart"></div>

        </div>

    </div>
</div>


<script>
    $(function () {
        setTimeout(function () {
            new Highcharts.Chart({
                chart: {
                    renderTo: 'game_scores',
                    backgroundColor: null
                },
                title: "Players ranking",
                series: <?= $chart_data?>,
//                xAxis: {
//                    categories: ['gem 1']
////                    labels: {enabled: true, y: 20, rotation: -45, align: 'right'}
//                },
                xAxis: {
                    labels:{enabled: false}
                },
                yAxis: {
                    labels:{enabled: false},
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    title: ""
                },

//                yAxis: {
//                    title: ""
//                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        }
//                        enableMouseTracking: false
                    }
                },
                tooltip: {
                    shared: false,
                    formatter: function() {
                        return this.series.options.tooltipText + '<br>' + // return stored text
                        'Value: ' + this.y;
                    }
                }

            });

            $("text:contains(Highcharts.com)").remove();

        }, 1)
    });
</script>