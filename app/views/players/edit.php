<div class="container ang" ng-cloak>
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_player', 'Edit player') ?></h3>
                <span class="line"></span>
            </div>

        </div>

        <div class="row">
            <a class="button green-col-bg"
               href="<?= URL::to('players/upload/' . $id) ?>"><?= LangHelper::get('upload_image', 'Upload image') ?></a>
            <?php if(Auth::getUser()->hasRole("superadmin") && (!in_array($player->status, [2, 3]))): ?>
                <a class="button danger"
                   href="<?= URL::to('players/delete/' . $id) ?>"><?= LangHelper::get('delete_player', 'Delete player') ?></a>
            <?php endif ?>
        </div>
        <form name="editForm" ng-class="{ 'show-errors': showErrors }"
              ng-submit="formSubmit($event, 'PUT','<?= LangHelper::get('form_updated', 'Form updated') ?>', '<?= Request::url() ?>', true)"
              ng-controller="FormController" novalidate>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
                <div class="tournament-second">
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="name"
                                           ng-init="formData.contact.name='<?= $player->contact->name ?>'"
                                           ng-model="formData.contact.name" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="surname"
                                           ng-init="formData.contact.surname='<?= $player->contact->surname ?>'"
                                           ng-model="formData.contact.surname" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="address"><?= LangHelper::get('address', 'Address') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="address"
                                           ng-init="formData.contact.address='<?= $player->contact->address ?>'"
                                           ng-model="formData.contact.address">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('club', 'Club') ?> *</label>
                                <?= Form::select('club_id', $clubs, $player->club_id, array('required', 'ng-model' => 'formData.data.club_id', 'ng-init' => "formData.data.club_id='" . $player->club_id . "'")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="phone"
                                           ng-init="formData.contact.phone='<?= $player->contact->phone ?>'"
                                           ng-model="formData.contact.phone">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="email"
                                           ng-init="formData.contact.email='<?= $player->contact->email ?>'"
                                           ng-model="formData.contact.email">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="web"><?= LangHelper::get('web', 'Web') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="web"
                                           ng-init="formData.contact.web='<?= $player->contact->web ?>'"
                                           ng-model="formData.contact.web">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="city"><?= LangHelper::get('city', 'City') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="city"
                                           ng-init="formData.contact.city='<?= $player->contact->city ?>'"
                                           ng-model="formData.contact.city">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="postal_code"><?= LangHelper::get('postal_code', 'Postal code') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="postal_code"
                                           ng-init="formData.contact.postal_code='<?= $player->contact->postal_code ?>'"
                                           ng-model="formData.contact.postal_code">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" datepicker-popup="dd.MM.yyyy"
                                           ng-init="formData.contact.date_of_birth='<?= DateTimeHelper::GetShortDateFullYear($player->contact->date_of_birth) ?>'"
                                           ng-model="formData.contact.date_of_birth" is-open="opened['date_of_birth']"
                                           show-button-bar="false" required
                                           ng-click="openDatePicker($event, 'date_of_birth')" formatdate>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control"
                                 ng-class="{'has-error': editForm.$submitted && editForm.sex.$invalid, 'has-success': editForm.sex.$valid && editForm.sex.$dirty}">
                                <label for="sex"><?= LangHelper::get('gender', 'Gender') ?> *</label>
                                <?= Form::select('sex', $genders, null, array('required', 'ng-model' => 'formData.contact.sex', 'ng-init' => "formData.contact.sex='" . $player->contact->sex . "'")); ?>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="card_id"><?= LangHelper::get('card_id', 'Card ID') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="card_id"
                                           ng-init="formData.contact.card_id='<?= $player->contact->card_id ?>'"
                                           ng-model="formData.contact.card_id">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label
                                    for="licence_number"><?= LangHelper::get('licence_number', 'Licence number') ?></label>

                                <div class="input-control text">
                                    <input disabled type="text" name="licence_number"
                                           ng-init="formData.data.licence_number='<?= $player->licence_number ?>'"
                                           ng-model="formData.data.licence_number">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="ranking"><?= LangHelper::get('ranking', 'Ranking') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="ranking"
                                           ng-init="formData.data.ranking='<?= $player->ranking ?>'"
                                           ng-model="formData.data.ranking">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="ape_id">APE ID</label>

                                <div class="input-control text">
                                    <input type="text" name="ape_id"
                                           ng-init="formData.data.ape_id='<?= $player->ape_id ?>'"
                                           ng-model="formData.data.ape_id">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                                style="margin-top: 38px;"><?= LangHelper::get('save', 'Save') ?></button>
                    </div>
                    <form-errors></form-errors>
                </div>
            </fieldset>
        </form>

    </div>
</div>

<script>
    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>
