<div class="metro" style="width: auto">
    <div class="modal-body">

        <p class="mutua-number">
            <?= LangHelper::get('new_mutua_number', 'New mutua number') ?>
            <br/>
            <?= $mutua_number ?>
        </p>

        <div class="clearfix row gap-top20">
            <a class="place-left rfet-button rfet-yellow" ng-click="approvePlayer(false, <?= $player->id?>)">
                <?= LangHelper::get('approve_player_without_invoice', 'Approve player without invoice') ?>
            </a>

            <a class="place-left rfet-button rfet-yellow gap-left20" ng-click="approvePlayer(true, <?= $player->id?>)">
                <?= LangHelper::get('approve_player_and_send_invoice', 'Approve player and send invoice') ?>
            </a>

            <a class="yellow-color place-right text-right gap-top30"
               ng-click="cancel()"><?= LangHelper::get('cancel', 'Cancel') ?></a>
        </div>
    </div>
</div>