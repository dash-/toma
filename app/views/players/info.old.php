<div class="container" ng-app="datatablesApp">
<div class="grid">
<div class="row">
    <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
</div>
<div class="row">
    <div class="clearfix">
        <div class="empty-space"></div>

    </div>
</div>

<?php if (!$player->user_id): ?>
    <a class="place-right rfet-yellow rfet-button" href="/players/create-account/<?= $player->id ?>">
        <?= LangHelper::get('create_user_account', 'Create user account') ?>
    </a>
<?php endif ?>


<div class="tournament-first">
    <div class="table-title tournament">
        <h3>Player info</h3>
        <span class="line"></span>
    </div>
</div>

<div class="row players-info">

    <div class="playerImage">
        <?php if ($player->contact->image_link): ?>
            <p><a style="display: inline;"
                  href="/uploads/images/user/full/<?= $player->contact->image_link ?>"><img
                        src="/uploads/images/user/small_thumbnails/<?= $player->contact->image_link ?>"/>
                </a>
            </p>
        <?php endif; ?>
    </div>
    <div class="leftPartOne">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('name', 'Name') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    <?= LangHelper::get('surname', 'Surname') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('phone', 'Phone') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('email', 'Email') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('city', 'City') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('address', 'Address') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('postal_code', 'Postal code') ?>:
                </td>
            </tr>
        </table>
    </div>

    <div class="leftPartTwo">
        <table>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->contact->name ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->surname ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->phone ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->email ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->date_of_birth ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->contact->city ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->address ?></td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->postal_code ?></td>
            </tr>
        </table>
    </div>

    <div class="rightPartOne">

        <table>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('licence_number', 'Licence number') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('sex', 'Sex') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('card_id', 'Card id') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('ranking', 'Ranking') ?>:
                </td>
            </tr>
            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('ranking_points', 'Ranking points') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('ape_id', 'Ape id') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('nationality', 'Nationality') ?>:
                </td>
            </tr>

            <tr>
                <td class="color-rfet"
                    style="padding-bottom:20px;"><?= LangHelper::get('current_situation', 'Current Situation') ?>
                    :
                </td>
            </tr>

            <tr>
                <td class="color-rfet" style="padding-bottom:20px;"><?= LangHelper::get('web', 'Web') ?>:
                </td>
            </tr>

        </table>
    </div>
    <div class="rightPartTwo">
        <table>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px; height:20px;">
                    &nbsp;<?= $player->licence_number ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->sex ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->card_id ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->ranking ?>
                    <span class="rankingsign <?= $sign[1] ?>">
                        <?= $player->ranking_move ?><span class="rankingicon <?= $sign[1] ?> "></span>
                    </span>
                </td>
            </tr><tr>
                <td class="color-rfet" style="padding-bottom:20px;">&nbsp;<?= $player->ranking_points ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->ape_id ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->nationality ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->current_situation ?>
                </td>
            </tr>
            <tr>
                <td class="color-rfet" style="padding-bottom:20px;">
                    &nbsp;<?= $player->contact->web ?>
                </td>
            </tr>
        </table>
    </div>
</div>

<?php if ($tournaments): ?>
    <div class="row players-info gap-top20">
        <h3><?= LangHelper::get('latest_tournaments', 'Latest tournaments') ?></h3>
        <?php foreach ($tournaments as $key => $tournament): ?>
            <p class="fg-white"><?= $tournament->title ?></p>

            <table class="table bg-dark striped">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                    <th class="text-left"><?= LangHelper::get('round', 'Round') ?></th>
                    <th class="text-left"><?= LangHelper::get('tournament_surface', 'Tournament surface') ?></th>
                    <th class="text-left">W/L</th>
                    <th class="text-left"><?= LangHelper::get('versus', 'Versus') ?></th>
                    <th class="text-left"><?= LangHelper::get('result', 'Result') ?></th>
                    <th class="text-right"><?= LangHelper::get('options', 'Options') ?></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($matches as $match): ?>
                    <?php if (in_array($match->draw_id, $tournament->draws->lists('id', 'id'))): ?>
                        <?php $team_id = DrawMatchHelper::retrieveTeamId($match->draw_id, $player->id); ?>
                        <tr>
                            <td>
                                <?= ucfirst($match->draw->draw_type) ?>
                                <?php if ($match->draw->draw_type == 'doubles'): ?>
                                    <br>
                                    (Partner) <?= DrawMatchHelper::listDoublesCoplayer($team_id, $player->licence_number) ?>
                                <?php endif ?>
                            </td>
                            <td><?= $match->round_number ?></td>
                            <td><?= ($tournament->surface) ? $tournament->surface->name : null ?></td>
                            <td>
                                <?= DrawMatchHelper::matchStatusByTeamId($match, $team_id) ?>
                            </td>
                            <td>
                                <?= DrawMatchHelper::getVersusName($match, $team_id) ?>
                            </td>
                            <td>
                                <?= DrawMatchHelper::getScoresInString($match->scores) ?>
                            </td>
                            <td class="text-right">
                                <?php if (count($match->game_scores)): ?>
                                    <a class="call yellow-color"
                                       href="/players/match-score/<?= $player->id . '/' . $match->id ?>">
                                        <?= LangHelper::get('head2head', 'Head 2 Head scores') ?>
                                    </a>
                                <?php endif ?>
                            </td>
                        </tr>

                    <?php endif ?>

                <?php endforeach ?>
                </tbody>
            </table>
        <?php endforeach ?>
    </div>
<?php endif ?>

</div>

</div>
</div>

