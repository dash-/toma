<div class="container" ng-app="datatablesApp">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>
                    <?= $player->contact->full_name() . ' - ' . LangHelper::get('tournament_info', 'Tournament info') ?>
                </h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="tournament-second gap-top30 pad-top10">
            <p class="fg-white"><?= $tournament->title ?></p>
            <table class="table bg-dark striped">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('type', 'Type') ?></th>
                    <th class="text-left"><?= LangHelper::get('round', 'Round') ?></th>
                    <th class="text-left"><?= LangHelper::get('tournament_surface', 'Tournament surface') ?></th>
                    <th class="text-left">W/L</th>
                    <th class="text-left"><?= LangHelper::get('versus', 'Versus') ?></th>
                    <th class="text-left"><?= LangHelper::get('round_points', 'Round points') ?></th>
                    <th class="text-left"><?= LangHelper::get('po', 'P.O') ?></th>
                    <th class="text-left"><?= LangHelper::get('result', 'Result') ?></th>
                    <th class="text-right"><?= LangHelper::get('options', 'Options') ?></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($matches as $match): ?>
                    <?php if (in_array($match->draw_id, $tournament->draws->lists('id', 'id'))): ?>
                        <?php $team_id = $match->matchpoints->signup_id; ?>
                        <tr>
                            <td>
                                <?= ucfirst($match->draw->draw_type) ?>
                                <?php if ($match->draw->draw_type == 'doubles'): ?>
                                    <br>
                                    (Partner) <?= DrawMatchHelper::listDoublesCoplayer($team_id, $player->licence_number) ?>
                                <?php endif ?>
                            </td>
                            <td><?= $match->round_number ?></td>
                            <td><?= ($tournament->surface) ? $tournament->surface->name : null ?></td>
                            <td>
                                <?= DrawMatchHelper::matchStatusByTeamId($match, $team_id) ?>
                            </td>
                            <td>
                                <?= DrawMatchHelper::getVersusName($match, $team_id) ?>
                            </td>
                            <td>
                                <?= $match->matchpoints->round_points ?>
                            </td>
                            <td>
                                <?= $match->matchpoints->otroga_points ?>
                            </td>
                            <td>
                                <?= DrawMatchHelper::getScoresInString($match->scores) ?>
                            </td>
                            <td class="text-right">
                                <?php if (count($match->game_scores)): ?>
                                    <a class="call yellow-color"
                                       href="/players/match-score/<?= $player->id . '/' . $match->id ?>">
                                        <?= LangHelper::get('match_analysis', 'Match analysis') ?>
                                    </a>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endif ?>
                <?php endforeach ?>
                </tbody>
            </table>

            <div class="clearfix gap-top10 pad-top20">
                <p class="player-total-points">
                    <?= LangHelper::get('round_points', 'Round points') ?>
                    : <?= PlayerHelper::maxRoundPoints($matches) ?>
                </p>
                <p class="player-total-points">
                    <?= LangHelper::get('points_otorga', 'Points otorga') ?>
                    : <?= PlayerHelper::otorgaPointsTotal($matches) ?>
                </p>
                <p class="player-total-points">
                    <?= LangHelper::get('total_points', 'Total points') ?>
                    : <?= PlayerHelper::tournamentTotalPoints($matches) ?>
                </p>
            </div>
        </div>
    </div>
</div>
