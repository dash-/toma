<div class="container ang" ng-cloak>
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('player_search', 'Player search') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="tournament-second pad-top40" ng-controller="PlayerSearchController">
            <a class="rfet-button rfet-yellow place-right" ng-click="hideForm=false" ng-show="hideForm">
                <?= LangHelper::get('show_form', 'Show form') ?>
            </a>


            <form name="editForm" novalidate ng-show="!hideForm">

                <div class="clearfix gap-bottom10">
                    <p class="form-error" ng-show="errors.sex"><?= LangHelper::get('please_choose_sex', 'Please choose sex')?></p>
                    <div class="input-control radio default-style gap-right20">
                        <label>
                            <input type="radio" name="sex" ng-model="formData.sex" value="M" />
                            <span class="check"></span>
                            <?= LangHelper::get('male', 'Male')?>
                        </label>
                    </div>
                    <div class="input-control radio default-style">
                        <label>
                            <input type="radio" name="sex" ng-model="formData.sex" value="F" />
                            <span class="check"></span>
                            <?= LangHelper::get('female', 'Female')?>
                        </label>
                    </div>
                </div>

                <div class="clearfix search-box">
                    <div class="input-control text inline-input">
                        <div style="height: 25px">
                            <label for="">
                                <?= LangHelper::get('surname', 'Surname')?>
                                <span class="form-error" ng-show="errors.surname">
                                    <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                </span>
                            </label>
                        </div>
                        <input type="text" ng-model="formData.surname">
                    </div>

                    <div class="submit-btn-inline">
                        <a class="rfet-button rfet-yellow" ng-click="searchByName($event)"><?= LangHelper::get('search', 'Search')?></a>
                    </div>
                </div>

                <hr class="form-separator">

                <div class="clearfix search-box">
                    <div class="input-control text inline-input">
                        <div style="height: 25px">
                            <label for="">
                                <?= LangHelper::get('licence_number', 'Licence number')?>
                                <span class="form-error" ng-show="errors.licence_number">
                                    <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                </span>
                            </label>
                        </div>
                        <input type="text" ng-model="formData.licence_number">
                    </div>

                    <div class="submit-btn-inline">
                        <a class="rfet-button rfet-yellow" ng-click="searchByLicence($event)"><?= LangHelper::get('search', 'Search')?></a>
                    </div>
                </div>

                <hr class="form-separator">

                <div class="clearfix search-box">
                    <div class="inline-inputs-holder">
                        <div class="half gap-left0">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('ranking_from', 'Ranking from')?>
                                        <span class="form-error" ng-show="errors.ranking_from">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required')?>
                                        </span>
                                    </label>
                                </div>
                                <input type="text" ng-model="formData.ranking_from">
                            </div>
                        </div>

                        <div class="half">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('ranking_to', 'Ranking to') ?>
                                        <span class="form-error" ng-show="errors.ranking_to">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required') ?>
                                        </span>
                                    </label>
                                </div>
                                <input type="text" ng-model="formData.ranking_to">
                            </div>
                        </div>
                    </div>

                    <div class="submit-btn-inline">
                        <a class="rfet-button rfet-yellow" ng-click="searchByRanking($event)"><?= LangHelper::get('search', 'Search')?></a>
                    </div>
                </div>

                <hr class="form-separator">

                <div class="clearfix search-box">
                    <div class="inline-inputs-holder">
                        <div class="half gap-left0">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('points_from', 'Points from') ?>
                                        <span class="form-error" ng-show="errors.points_from">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required') ?>
                                        </span>
                                    </label>
                                </div>
                                <input type="text" ng-model="formData.points_from">
                            </div>
                        </div>

                        <div class="half">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('points_to', 'Points to') ?>
                                        <span class="form-error" ng-show="errors.points_to">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required') ?>
                                        </span>
                                    </label>
                                </div>
                                <input type="text" ng-model="formData.points_to">
                            </div>
                        </div>
                    </div>

                    <div class="submit-btn-inline">
                        <a class="rfet-button rfet-yellow" ng-click="searchByPoints($event)"><?= LangHelper::get('search', 'Search')?></a>
                    </div>
                </div>

                <hr class="form-separator">

                <div class="clearfix search-box">
                    <div class="inline-inputs-holder">
                        <div class="half gap-left0">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('date_of_birth_from', 'Date of birth from') ?>
                                        <span class="form-error" ng-show="errors.birth_from">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required') ?>
                                        </span>
                                    </label>
                                </div>

                                <input type="text" name="birth_from" min-date="minDate"
                                       datepicker-popup="dd.MM.yyyy" ng-required="true"
                                       ng-model="formData.birth_from"
                                       is-open="opened['birthFrom']" show-button-bar="false"
                                       ng-click="openDatePicker($event, 'birthFrom')" formatdate
                                       placeholder="dd.mm.yyyy">
                            </div>
                        </div>

                        <div class="half">
                            <div class="input-control text">
                                <div style="height: 25px">
                                    <label for="">
                                        <?= LangHelper::get('date_of_birth_to', 'Date of birth to') ?>
                                        <span class="form-error" ng-show="errors.birth_to">
                                            <?= LangHelper::get('this_field_is_required', 'This field is required') ?>
                                        </span>
                                    </label>
                                </div>
<!--                                <input type="text" ng-model="formData.birth_to" placeholder="--><?//#= LangHelper::get('enter_year', 'Enter year')?><!--">-->
                                <input type="text" name="birth_to" min-date="minDate"
                                       datepicker-popup="dd.MM.yyyy" ng-required="true"
                                       ng-model="formData.birth_to"
                                       is-open="opened['birthTo']" show-button-bar="false"
                                       ng-click="openDatePicker($event, 'birthTo')" formatdate
                                       placeholder="dd.mm.yyyy">
                            </div>
                        </div>
                    </div>

                    <div class="submit-btn-inline">
                        <a class="rfet-button rfet-yellow" ng-click="searchByDate($event)"><?= LangHelper::get('search', 'Search')?></a>
                    </div>
                </div>

                <hr class="form-separator">

            </form>

            <div class="clearfix player-align table-content players pad-top40 gap-top30">
                <table class="table bg-dark striped"  ng-show="players.length > 0">
                    <thead>
                    <tr>
                        <th class="text-left">
                            <?= LangHelper::get('name', 'Name') ?>
                        </th>
                        <th class="text-left">
                            <?= LangHelper::get('surname', 'Surname') ?>
                        </th>
                        <th class="text-left">
                            <?= LangHelper::get('licence_number', 'Licence number') ?>
                        </th>
                        <th class="text-left">
                            <?= LangHelper::get('city', 'City') ?>
                        </th>
                        <th class="text-left full">
                            DOB
                        </th>
                        <th class="text-left last-col">
                            <?= LangHelper::get('options', 'Options') ?>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="player in players track by $index">
                        <td>{{ player.name }}</td>
                        <td>{{ player.surname }}</td>
                        <td>{{ player.licence_number }}</td>
                        <td>{{ player.city }}</td>
                        <td>{{ player.date_of_birth | asDate | date:'dd.MM.yyyy' }}</td>
                        <td class="text-left input-control transparent">
                            <a class="button primary info inline-block" ng-href="/players/info/{{player.id}}">
                                <?= LangHelper::get('info', 'Info')?>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <h3 class="fg-white text-center" ng-show="players.length < 1"><?= LangHelper::get('no_available_data', 'No available data')?></h3>
            </div>
        </div>
    </div>
</div>

<script src="/js/ang/search.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['playerSearch']);
    });
</script>
