<div class="container ang">
    <div class="grid" ng-controller="playerActionsController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <a class="metro button bigger yellow-col-bg right" ng-click="open()"
                   href=""><?= LangHelper::get('ask_a_question', 'Ask a question') ?></a>
            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('player_details', 'Player details') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('name', 'Name') ?>
                            :</span> <?= $player->contact->name ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('surname', 'Surname') ?>
                            :</span> <?= $player->contact->surname ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('phone', 'Phone') ?>
                            :</span> <?= $player->contact->phone ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('email', 'Email') ?>
                            :</span> <?= $player->contact->email ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>
                            :</span> <?= DateTimeHelper::GetShortDateFullYear($player->contact->date_of_birth) ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('city', 'City') ?>
                            :</span> <?= $player->contact->city ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('address', 'Address') ?>
                            :</span> <?= $player->contact->address ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('postal_code', 'Postal code') ?>
                            : </span><?= $player->contact->postal_code ?></p>
                </div>
                <div class="half">
                    <p><span class="color-rfet"><?= LangHelper::get('licence_number', 'Licence number') ?>
                            : </span><?= $player->licence_number ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('sex', 'Sex') ?>
                            : </span><?= $player->contact->sex ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('card_id', 'Card id') ?>
                            :</span> <?= $player->contact->card_id ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('ape_id', 'Ape id') ?>
                            : </span><?= $player->ape_id ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('nationality', 'Nationality') ?>
                            :</span> <?= $player->nationality ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('current_situation', 'Current Situation') ?>
                            : </span><?= $player->current_situation ?></p>

                    <p><span class="color-rfet"><?= LangHelper::get('web', 'Web') ?>
                            :</span> <?= $player->contact->web ?></p>
                </div>
            </div>

            <div class="row">
                <div class="pad-top15">
                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                        <?php if (!in_array($player->status, [2, 3])): ?>
                            <a class="metro button bigger green-col-bg left" ng-click="openLicNum()" href="">
                                <?= LangHelper::get('approve', 'Approve') ?>
                            </a>

                            <a class="metro button bigger red-col-bg right"
                               ng-click="postRequest($event, 'Player rejected', 3)"
                               href="<?= URL::to('players/request-action', array($player->id)) ?>">
                                <?= LangHelper::get('reject', 'Reject') ?>
                            </a>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>
        </div>

        <div class="row clear pad-bottom20">
            <!-- Modal template -->
            <script type="text/ng-template" id="myModalContent.html">
                <?= $ask_question_view ?>
            </script>

            <script type="text/ng-template" id="licenceNumberContent.html">
                <?= $licence_number_view ?>
            </script>
        </div>

    </div>
</div>


<script src="/js/ang/approvalActions.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['approvalActions']);
    });
</script>
