<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('players', 'Players') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="row">
            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th class="text-left"><?= LangHelper::get('name', 'Name') ?></th>
                    <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                        <th class="text-left"><?= LangHelper::get('submitted_by', 'Submited by') ?></th>
                    <?php endif ?>
                    <th class="text-left"><?= LangHelper::get('club', 'Club') ?></th>
                    <th class="text-left"><?= LangHelper::get('licence_number', 'Licence number') ?></th>
                    <th class="text-left"><?= LangHelper::get('date_of_birth', 'Date of birth') ?></th>
                    <th class="text-left"><?= LangHelper::get('city', 'City') ?></th>
                    <th class="text-left"><?= LangHelper::get('gender', 'Gender') ?></th>
                    <th class="text-left"><?= LangHelper::get('status', 'Status') ?></th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($players as $player): ?>
                    <tr>
                        <td class="pad-right100"><?= $player->contact->full_name() ?></td>
                        <?php if (Auth::getUser()->hasRole('superadmin')): ?>
                            <td class="pad-right100"><?= ($player->creator()) ? $player->creator()->username : false ?></td>
                        <?php endif ?>
                        <td class="pad-right20"><?= ($player->club) ? $player->club->club_name : false ?></td>
                        <td class="pad-right20"><?= $player->licence_number ?></td>
                        <td class="pad-right20"><?= ($player->contact->date_of_birth) ? DateTimeHelper::GetShortDateFullYear($player->contact->date_of_birth) : false ?></td>
                        <td class="pad-right20"><?= $player->contact->city ?></td>
                        <td class="pad-right100"><?= $player->contact->gender() ?></td>
                        <td class="pad-right100"><?= $player->status() ?></td>

                        <td class="transparent text-right">
                            <?php if (!($player->club)): ?>

                                <a class="button yellow-col-bg d-small" data-title="Player edit"
                                   href="/players/edit/<?= $player->id ?>"><?= LangHelper::get('edit', 'Edit') ?></a>
                                <?php else: ?>
                                <a class="button yellow-col-bg d-small" data-title="Player details"
                                   href="/players/details/<?= $player->id ?>"><?= LangHelper::get('details', 'Details') ?></a>
                            <?php endif ?>
                        </td>
                    </tr>
                <?php endforeach ?>
            </table>
        </div>
    </div>


</div>