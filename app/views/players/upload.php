<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('upload_image', 'Upload image')?></h3>
                <span class="line"></span>
            </div>
        </div>
        
        <div class="tournament-second">

            <form name="uploadForm" id="redirectAfterUpload" method="post" class="dropzone" redirect="true"
                  action="<?= Request::url() ?>" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <?php if( $player->contact->image_link ): ?>
                        <div class="image">
                            <a href="/uploads/images/user/full/<?= $player->contact->image_link ?>"><img
                                    src="/uploads/images/user/thumbnails/<?= $player->contact->image_link ?>" style="width: 250px"/>
                            </a>
                        </div>
                    <?php endif; ?>
                        <div class="form-control clearfix">
                            <label for="image"><?= LangHelper::get('image', 'Image')?></label>

                            <div class="input-control file">
                                <input type="file" name="image">
                                <button class="btn-file"></button>
                            </div>
                        </div>

                        <button type="submit" class="button clear place-right primary large dark" style="margin-top: 40px;">
                            <?= LangHelper::get('upload', 'Upload')?>
                        </button>

                </fieldset>
            </form>

        </div>

    </div>
</div>

