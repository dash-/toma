<div class="container" ng-app="datatablesApp">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>

        <?php if (!$player->user_id AND Auth::user()->hasRole('superadmin')): ?>
            <a class="place-right rfet-yellow rfet-button gap-left20" href="/players/create-account/<?= $player->id ?>">
                <?= LangHelper::get('create_user_account', 'Create user account') ?>
            </a>
        <?php endif ?>

        <a class="place-right rfet-yellow rfet-button gap-left15" href="/players/notes/<?= $player->id ?>">
            <?= LangHelper::get('notes', 'Notes') ?> (<?= $player->notes->count() ?>)
        </a>

        <a class="place-right rfet-danger rfet-button gap-left15" href="/players/penalty-notes/<?= $player->id ?>">
            <?= LangHelper::get('code_of_conduct', 'Code of conduct') ?> (<?= $player->penalty_notes()->count() ?>)
        </a>

        <?php if ($player->status != 2):?>
            <a class="place-right rfet-danger rfet-button" href="/players/details/<?= $player->id ?>">
                <?= LangHelper::get('need_action', 'Need action') ?>
            </a>
        <?php endif?>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3>Player info</h3>
                <span class="line"></span>
            </div>
        </div>
        <?= (Auth::user()->hasRole('referee'))
            ? View::make('players/_referee_info', ['player' => $player, 'sign' => $sign])
            : View::make('players/_admin_info', ['player' => $player, 'sign' => $sign]) ?>
        <?php if ($tournaments): ?>
            <div class="row players-info gap-top20">
                <h3><?= LangHelper::get('latest_tournaments', 'Latest tournaments') ?>
                <?php if($old_tournaments): ?>
                     - <?= LangHelper::get("older_unlisted_tournaments", "Older, unlisted tournaments") ?>: <?= $old_tournaments ?>
                    <?php endif ?>
                </h3>

                <table class="table bg-dark striped">
                    <thead>
                    <tr>
                        <th class="text-left"><?= LangHelper::get('title', 'Title') ?></th>
                        <th class="text-left"><?= LangHelper::get('club', 'Club') ?></th>
                        <th class="text-left"><?= LangHelper::get('surface', 'Surface') ?></th>
                        <th class="text-left"><?= LangHelper::get('total_points', 'Total points') ?></th>
                        <th class="text-right"><?= LangHelper::get('options', 'Options') ?></th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($tournaments as $key => $tournament): ?>
                        <tr>
                            <td><?= $tournament->title ?></td>
                            <td><?= $tournament->club->club_name ?></td>
                            <td><?= ($tournament->surface) ? $tournament->surface->name : 'N/A' ?></td>
                            <td>
                                <?= PlayerHelper::perTournamentTotal($matchpoints, $tournament->id) ?> -
                                <?= PlayerHelper::checkIfAssignedPoints($matchpoints, $tournament->id)?>
                            </td>
                            <td class="text-right">

                                <a class="call yellow-color"
                                   href="/players/tournament-info/<?= $player->id . '/' . $tournament->id ?>">
                                    <?= LangHelper::get('tournament_info', 'Tournament info') ?>
                                </a>

                            </td>
                        </tr>

                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        <?php endif ?>

    </div>

</div>

