<div class="container ang">
    <div class="grid datatable" ng-controller="tableController" data-ng-init="init(1)"
         data-scUrl="/players/data<?= $tournament_id ? "/" . $tournament_id : "" ?>">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <a class="rfet-button rfet-yellow place-right gap-left20 call"
                   href="/players/search"><?= LangHelper::get('search', 'Search') ?></a>

                <?php if (Auth::getUser()->players()->where('status', '!=', 2)->count() OR Auth::getUser()->hasRole('superadmin')): ?>
                    <a class="rfet-button rfet-yellow place-right"
                       href="/players/not-approved"><?= LangHelper::get('view_not_approved_players', 'View not approved players') ?></a>
                <?php else: ?>
                    <div class="empty-space"></div>
                <?php endif ?>
            </div>
        </div>
        <div class="row">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('players', 'Players') ?></h3>
                <span class="line"></span>
            </div>
        </div>
        <div class="table-title players">
            <div class="results-number input-control select">
                <?= Form::select('items', $items_per_page, null,
                    array('ui-select2' => "select2Options", 'ng-change' => 'itemsPerPage()',
                          'ng-model'   => 'counter', 'ng-init' => 'counter=10', 'ng-class' => 'no-search-box')); ?>

                <?= Form::select('regions', $regions, null,
                    array('ui-select2' => "select2Options", 'ng-change' => 'filterBy()',
                          'ng-model'   => 'federation_club_id', 'ng-class' => 'no-search-box', 'class' => 'gap-top20')); ?>

                <?= Form::select('fined', $fined_array, null,
                    array('ui-select2' => "select2Options", 'ng-change' => 'filterBy()',
                          'ng-model'   => 'fined', 'ng-class' => 'no-search-box', 'class' => 'gap-top20')); ?>

                <p class="fg-yellow gap-top24"><?= LangHelper::get('total_results', 'Total results') ?>:
                    {{totalItems}}</p>
            </div>
        </div>
        <div class="player-align table-content players tabScrl">
            <form ng-submit="search($event, 1)" class="tabScrl" name="searchForm">
                <table class="table bg-dark striped table-scrollable-w768">
                    <thead>
                    <tr>
                        <input type="hidden" ng-model="searchText.search" ng-init="searchText.search='1'">
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.surname"
                                       placeholder="<?= LangHelper::get("search_by_surname", "Search by surname") ?>">
                            </div>
                            <a ng-click="doOrder('contacts.surname')" href="#">
                                <?= LangHelper::get('surname', 'Surname') ?> <i
                                    ng-class="sortBy('contacts.surname')"></i>
                            </a>
                        </th>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.name"
                                       placeholder="<?= LangHelper::get("search_by_name", "Search by name") ?>">
                            </div>
                            <a ng-click="doOrder('contacts.name')" href="#">
                                <?= LangHelper::get('name', 'Name') ?> <i ng-class="sortBy('contacts.name')"></i>
                            </a>
                        </th>
                        <?php if (!Auth::user()->hasRole('referee')): ?>
                            <th class="text-left">
                                <div class="input-control text">
                                    <input type="text" ng-model="searchText.licence_number"
                                           placeholder="<?= LangHelper::get("search_by_licence_number", "Search by licence number") ?>">
                                </div>
                                <a ng-click="doOrder('licence_number')" href="#">
                                    <?= LangHelper::get('licence_number', 'Licence number') ?> <i
                                        ng-class="sortBy('licence_number')"></i>

                                </a>
                            </th>
                        <?php endif ?>
                        <th class="text-left">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.city"
                                       placeholder="<?= LangHelper::get("search_by_city", "Search by city") ?>">
                            </div>
                            <a ng-click="doOrder('city')" href="#">
                                <?= LangHelper::get('city', 'City') ?> <i ng-class="sortBy('city')"></i>

                            </a>
                        </th>
                        <th class="text-left full">
                            <div class="input-control text">
                                <input type="text" ng-model="searchText.date_of_birth"
                                       placeholder="<?= LangHelper::get("search_by_year", "Search by year") ?>">
                            </div>
                            <a ng-click="doOrder('date_of_birth')" href="#">
                                <?= LangHelper::get("dob", "DOB") ?> <i ng-class="sortBy('date_of_birth')"></i>

                            </a>
                        </th>
                        <th class="text-left last-col">
                            <div class="input-control text">
                                <button type="submit" ng-click="search($event, 1)"
                                        ng-class="scSearch == 1 ? 'split-button' : ''"
                                        class="primary dark full"><?= LangHelper::get('filter', 'Filter') ?></button>
                                <a ng-click="resetSearch()" ng-show="scSearch == 1"
                                   class="button primary split-button reset-button"
                                   href="#"><?= LangHelper::get('reset_search', 'Reset search') ?></a>
                            </div>
                            <a class="options"><?= LangHelper::get('options', 'Options') ?></a>
                        </th>

                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="player in rows track by $index">
                        <td ng-class="{fined: player.penalized}">{{ player.surname }}</td>
                        <td>{{ player.name }}</td>

                        <?php if (!Auth::user()->hasRole('referee')): ?>
                            <td>{{ player.licence_number }}</td>
                        <?php endif ?>

                        <td>{{ player.city }}</td>
                        <td>{{ player.date_of_birth | asDate | date:'dd.MM.yyyy' }}</td>
                        <td class="text-left input-control transparent">
                            <a class="button primary info inline-block" ng-href="/players/info/{{player.id}}">
                                <?= LangHelper::get('info', 'Info') ?>
                            </a>

                            <?php if (!Auth::user()->hasRole('referee')): ?>
                                <a class="call button small primary default"
                                   ng-hide="(user_role == 3 && user_region != player.federation_club_id) || (user_role == 6)"
                                   ng-href="/players/edit/{{player.id}}">
                                    <?= LangHelper::get('edit', 'Edit') ?>
                                </a>
                            <?php endif ?>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </form>

            <h3 class="fg-white text-center"
                ng-show="rows.length < 1"><?= LangHelper::get('no_available_data', 'No available data') ?></h3>

            <div class="input-control back-in-pagination" ng-show="rows.length > 0">
                <a class="button primary yellow back big call" href="/"><?= LangHelper::get('back', 'BACK') ?></a>
            </div>
            <div class="pagination" ng-show="rows.length > 0">
                <pagination total-items="totalItems" num-pages="noOfPages" ng-model="bigCurrentPage" max-size="maxSize"
                            boundary-links="true" rotate="false" items-per-page="counter"></pagination>
            </div>
            <div class="input-control next-button marginTop-50" ng-show="rows.length > 0">
                <a class="button primary big call next"
                   href="<?= URL::to('players/create') ?>"><?= LangHelper::get('submit_new_player', 'Submit new player') ?></a>
            </div>
        </div>
    </div>
</div>

<script>
    //napomena : hack rjesenje funkcije showSearch u select.js, stoga ne korisititi select.min.js u produkciji
    $(document).ready(function () {
        $("select").select2({});
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>