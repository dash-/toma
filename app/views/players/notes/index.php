<div class="container ang">
    <div class="grid" ng-controller="IndexController" data-ng-init="init('/players/notes-data/<?= $player->id ?>')">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= (LangHelper::get('player_notes', 'Player notes')) ?>
                    - <?= $player->contact->full_name() ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="tournament-second pad-bottom20 gap-top20" ng-cloak>
            <form name="createForm" novalidate ng-submit="formSubmit($event, '<?= Request::url() ?>')">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="form-control move-bottom-30">
                        <span class="form-error">{{errors[0].key}}</span>

                        <div class="input-control textarea">
                            <textarea type="text" required ng-model="formData.text"
                                      placeholder="Your note here..."></textarea>
                        </div>
                    </div>
                    <button type="submit"
                            class="button primary big call next"><?= LangHelper::get('save', 'Save') ?></button>
                </fieldset>
            </form>

            <div class="row">
                <div ng-repeat="note in notes track by $index">
                    <div class="note-holder">
                        <a class="delete_note" ng-show="{{note.note_user == note.current_user}}"
                           ng-click="delete($event, $index)" href="/players/note/{{note.id}}">
                            <i class="icon-remove"></i>
                        </a>

                        <p class="note-creator">
                            {{note.user_name}} | {{note.created}}
                        </p>

                        <p class="note-text">{{note.text}}</p>
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>


<script src="/js/ang/notes.js"></script>
<script>
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['notesApp']);
    });
</script>