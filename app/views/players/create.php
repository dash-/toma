<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('submit_new_player', 'Submit new player') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <form name="editForm" ng-class="{ 'show-errors': showErrors }"
              ng-submit="formSubmit($event, 'POST',
              '<?= LangHelper::get('player_successfully_created', 'Player successfully created') ?>,
              <br> request for player approval sent!', false, false, true)"
              ng-controller="FormController" novalidate ng-init="loadData(<?= $team_id ?>)" ng-cloak>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <fieldset>
                <div class="tournament-second">
                    <div class="row">
                        <div class="half">
                            <div class="form-control"> <!-- show-errors -->
                                <label for="name"><?= LangHelper::get('name', 'Name') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="name" ng-model="formData.contact.name" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?> *</label>

                                <div class="input-control text">
                                    <input type="text" name="surname" ng-model="formData.contact.surname" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="address"><?= LangHelper::get('address', 'Address') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="address" ng-model="formData.contact.address">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('club', 'Club') ?> *</label>
                                <?= Form::select('Club', $clubs, null, array('required', 'ng-model' => 'formData.data.club_id')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="phone"><?= LangHelper::get('phone', 'Phone') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="phone" ng-model="formData.contact.phone">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" name="email" ng-model="formData.contact.email">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="web"><?= LangHelper::get('web', 'Web') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="web" ng-model="formData.contact.web">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="city"><?= LangHelper::get('city', 'City') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="city" ng-model="formData.contact.city">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="postal_code"><?= LangHelper::get('postal_code', 'Postal code') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="postal_code" ng-model="formData.contact.postal_code">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control"> <!-- show-errors -->
                                <label for="date_of_birth"><?= LangHelper::get('date_of_birth', 'Date of birth') ?>
                                    *</label>

                                <div class="input-control text">
                                    <input type="text" name="date_of_birth" datepicker-popup="dd.MM.yyyy"
                                           ng-model="formData.contact.date_of_birth" is-open="opened['date_of_birth']"
                                           show-button-bar="false" ng-click="openDatePicker($event, 'date_of_birth')"
                                           formatdate required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="sex"><?= LangHelper::get('gender', 'Gender') ?> *</label>
                                <?= Form::select('gender', $genders, null, array('required', 'ng-model' => 'formData.contact.sex')); ?>
                                <!-- <div class="input-control text">
                                    <input type="text" name="sex" ng-model="formData.contact.sex">
                                </div> -->
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="card_id"><?= LangHelper::get('card_id', 'Card ID') ?></label>

                                <div class="input-control text">
                                    <input type="text" name="card_id" ng-model="formData.contact.card_id">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="ape_id">APE ID</label>

                                <div class="input-control text">
                                    <input type="text" name="ape_id" ng-model="formData.data.ape_id">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control" ng-class="{'has-error': editForm.$submitted && editForm.licence_number.$invalid, 'has-success': editForm.licence_number.$valid}">
                                <label for="licence_number">
                                    <?= LangHelper::get('licence_number', 'Licence number') ?> *</label>
                                <div class="input-control text">
                                    <input type="text" name="licence_number" ng-model="formData.data.licence_number" maxlength="9" required>
                                </div>
                                <small class="yellow-color">

                                {{ licence_range }}
                                </small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="submit" class="button clear place-right primary large dark"
                                style="margin-top: 40px;">Save
                        </button>
                    </div>

                    <form-errors></form-errors>
                </div>
            </fieldset>
        </form>


    </div>
</div>

<script>

    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>
