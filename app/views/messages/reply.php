<div class="container ang">
    <div class="grid">
        <div class="row">
            <div class="clearfix">
                <h3 class="fg-white place-left"><?= LangHelper::get('message_reply', 'Message reply')?></h3>
                <a class="place-right button gap-top10 call"  href="<?= URL::to('messages')?>"><?= LangHelper::get('back', 'Back')?></a>
            </div>
            <form name="createForm" ng-submit="formSubmit($event, 'POST', '<?= LangHelper::get('message_successfully_sent', 'Message successfully sent')?>', '<?= Request::url()?>')" ng-controller="FormController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="form-control">
                        <h4 class="fg-white">To: <?= $message->getUser('sender_id')->username;?></h4>
                        <input type="hidden" ng-model="formData.messages.receiver_id" ng-init="formData.messages.receiver_id='<?= $message->getUser('sender_id')->id;?>'">
                    </div>

                    <div class="form-control">
                        <label for="name"><?= LangHelper::get('subject', 'Subject')?></label>
                        <div class="input-control text">
                            <input type="text" ng-model="formData.messages.subject">
                        </div>
                    </div>

                    <div class="form-control">
                        <label for="name"><?= LangHelper::get('message', 'Message')?></label>
                        <div class="input-control textarea">
                            <textarea type="text" required ng-model="formData.messages.text"></textarea>
                        </div>
                    </div>
                    <button type="submit" ng-disabled="!createForm.$valid" class="clear place-left primary large gap-top10"><?= LangHelper::get('save', 'Save')?></button>

                </fieldset>
            </form>
        </div>
    </div>
</div>
 
<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>