<?= ($match->draw->draw_type == 'singles') ? $match->team1_data->full_name() : DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $match->team1_data->team_id)->get());?> VS <?= ($match->draw->draw_type == 'singles') ? $match->team2_data->full_name() : DrawMatchHelper::getDoublesPair(TournamentTeam::where('team_id', $match->team2_data->team_id)->get());?>
<br>
<?= LangHelper::get('result', 'Result')?>: <?= DrawMatchHelper::scoreAsString($match);?>
<br>
<?= LangHelper::get('tournament', 'Tournament')?>: <?= $match->draw->tournament->title?>
<br>
<?= LangHelper::get('draw_category', 'Draw category')?>: <?= $match->draw->category->name?>
<br>
<?= nl2br($msg_text)?>
<br>
<br>
<?= LangHelper::get('url', 'Url') ?>: <a href="<?= URL::to('draws/db-bracket/'.$match->draw_id.'?list_type='.$list_type)?>"><?= URL::to('draws/db-bracket/'.$match->draw_id.'?list_type='.$list_type)?></a> 