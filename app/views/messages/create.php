<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('compose', 'Compose')?></h3>
               <span class="line"></span>
             </div>
        </div>
            <div class="tournament-second move-down-30">
                <form name="createForm" ng-submit="formSubmit($event, 'POST', '<?= LangHelper::get('message_successfully_sent', 'Message successfully sent')?>','<?= Request::url()?>', true)" ng-controller="FormController" novalidate>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="form-control gap-bottom20">
                            <label style="min-height: 10px;">
                                <span class="form-error">{{ errors.user_id }}</span>
                            </label>
                            <div class="input-control">
                                <tags-input ng-model="formData.messages.user_id" 
                                    placeholder="Add user" 
                                    replace-spaces-with-dashes="false"
                                    add-from-autocomplete-only="true">
                                    <auto-complete source="loadUsers($query)" 
                                        min-length="3" 
                                        max-results-to-show="10"></auto-complete>
                                </tags-input> 
                            </div>
                        </div>

                        <div class="form-control gap-bottom20">
                            <label style="min-height: 10px;">
                                <span class="form-error">{{ errors.subject }}</span>
                            </label>
                            <div class="input-control text">
                                <input type="text" ng-model="formData.messages.subject" placeholder="Question">
                            </div>
                        </div>

                        <div class="form-control gap-bottom20">
                            <label style="min-height: 10px;">
                                <span class="form-error">{{ errors.text }}</span>
                            </label>
                            <div class="input-control textarea">
                                <textarea type="text" required ng-model="formData.messages.text" placeholder="Your text here..."></textarea>
                            </div>
                        </div>
                        <button type="submit" class="button primary big call next"><?= LangHelper::get('send', 'Send')?></button>
                    </fieldset>
                </form>
            </div>
        
    </div>
</div>


<link rel="stylesheet" href="/js/thirdparty/angular-tags/ng-tags-input.css"/>
<script src="/js/thirdparty/angular-tags/ng-tags-input.js"></script>
<script src="/js/ang/messagesForm.js"></script>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['messagesFormApp']);
    });
</script>