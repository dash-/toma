<div class="container ang">
    <div class="grid" ng-controller="FormController">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>  
            <div class="tournament-first">
                <div class="table-title tournament messageShowPadding">
                     <h3><?= LangHelper::get('conversation', 'Conversation')?>:</h3>
                     <span class="line"></span>
                     <h3><?= $first_message->subject?></h3>
                </div>
             </div>
             <div class="tournament-second gap-top10">
                <?php foreach ($messages as $message): ?>
                    <div class="message-show gap-bottom20">
                        <p><?= LangHelper::get('from', 'From')?>: <?= $message->getFrom()->username?></p>
                        <p><?= $message->text?></p>
                    </div>
                <?php endforeach;?> 
            </div>
            <div class="tournament-first">
                <div class="table-title tournament">
                     <h3><?= LangHelper::get('reply', 'Reply')?></h3>
                     <span class="line"></span>
                </div>
            </div>
            <div class="tournament-second">
                <form name="createForm" ng-submit="formSubmit($event, 'POST', '<?= LangHelper::get('message_successfully_sent', 'Message successfully sent')?>', '/messages/reply/<?= $first_message->conversation_id?>', true)" ng-controller="FormController" novalidate>
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <fieldset>
                        <div class="form-control pad-top20">
                            <h4 class="message-to "><?= LangHelper::get('to', 'To')?>: <?= $first_message->getTo();?></h4>
                            <input type="hidden" ng-model='formData.messages.user_id' ng-init='formData.messages.user_id=<?= $first_message->getTo("array")?>'>
                            <input type="hidden" ng-model='formData.messages.subject' ng-init='formData.messages.subject="<?= $first_message->subject?>"'>
                        </div>

                        <div class="form-control">
                            <!-- <label for="name">Text</label> -->
                            <div class="input-control textarea">
                                <textarea type="text" required ng-model="formData.messages.text" placeholder="Text"></textarea>
                            </div>
                        </div>
                        <button type="submit" ng-disabled="!createForm.$valid" class="button clear place-left primary large dark gap-top10"><?= LangHelper::get('send', 'Send')?></button>

                    </fieldset>
                </form>
            </div>            
            
        </div>
    </div>
</div>


<script src="/js/ang/replyForm.js"></script>
<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['replyFormApp']);
    });
</script>