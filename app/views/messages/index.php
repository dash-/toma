<div class="container">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <a class="button primary bigger call next" href="<?= URL::to('messages/create')?>"><?= LangHelper::get('compose', 'Compose')?></a>
            </div>
        </div>
        <div class="tournament-first">
           <div class="table-title tournament">
               <h3><?= LangHelper::get('messages', 'Messages')?></h3>
               <span class="line"></span>
           </div>
           <div class="table-title tournament gap-top5">
               <ul class="message-actions">
                  <li>
                      <a class="<?= (!Request::query('type') ? 'active' : NULL)?> call" href="<?= Request::url()?>"><?= LangHelper::get('inbox', 'Inbox')?></a> 
                  </li>
                  <li>
                      <a class="<?= (Request::query('type') == 'outbox') ? 'active' : NULL?> call" href="/messages?type=outbox"><?= LangHelper::get('outbox', 'Outbox')?></a> 
                  </li>
                  <li>
                      <a class="<?= (Request::query('type') == 'requests') ? 'active' : NULL?> call" href="/messages?type=requests"><?= LangHelper::get('requests', 'Requests')?></a> 
                  </li>
               </ul>
             </div>
           </div>
             <div class="tournament-second">
                <?= $view_file?>
             </div>
        </div>
    </div>


</div>