<div class="ang" ng-controller="tableController">
    <table class="table bg-dark message-table">
        <tbody>
        <?php foreach ($messages as $message): ?>
                  <tr class="<?= $message->pivot->seen ? 'seen' : '' ?>">
                    <td class="envelope-width"><a href="/messages/show/<?= $message->conversation_id?>"><div class="envelope"></div></td>
                    <td><a href="/messages/show/<?= $message->conversation_id?>"><?= $message->getFrom()->info("name")." ".$message->getFrom()->info("surname") ?></td>
                    <td class="bold"><a href="/messages/show/<?= $message->conversation_id?>"><?= "> ".$message->subject ?></td>
                    <td class="text-right pad-right15"><a href="/messages/show/<?= $message->conversation_id?>"><?= DateTimeHelper::GetFullDateTime($message->time_sent) ?></td>
                </tr>
        <?php endforeach ?>
    </table>
</div>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['datatablesApp']);
    });
</script>