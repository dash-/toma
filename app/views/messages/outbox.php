<div>
    <table class="table bg-dark message-table">
        <tbody>
        <?php foreach ($messages as $message): ?>
            <tr>
                <td><a href="/messages/show/<?= $message->conversation_id?>"><?= $message->getTo() ?></td>
                <td><a href="/messages/show/<?= $message->conversation_id?>"><?= "> ".$message->subject ?></td>
                <td class="text-right pad-right10"><a href="/messages/show/<?= $message->conversation_id?>"><?= DateTimeHelper::GetFullDateTime($message->time_sent) ?></td>
            </tr>
        <?php endforeach ?>
    </table>
</div>