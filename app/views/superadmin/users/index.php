<div class="container ang">
    <div class="grid">
    	<div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <a class="metro button bigger yellow-col-bg right next-black-arrow call" href="<?= route('users/create')?>"><?= LangHelper::get('add_user', 'Add user')?></a>
            </div>
        </div>
        <div class="tournament-first">
             <div class="table-title tournament">
               <h3><?= LangHelper::get('users_administration', 'Users administration')?></h3>
               <span class="line"></span>
             </div>
        </div>


		<table class="table bg-dark tournament-table" ng-controller="userController">
				<thead>
					<tr>
						<th class="text-left"><?= LangHelper::get('username', 'Username')?></th>
						<th class="text-left"><?= LangHelper::get('email', 'Email')?></th>
						<th class="text-left"><?= LangHelper::get('role', 'Role')?></th>
						<th class="text-left"><?= LangHelper::get('last_login', 'Last login')?></th>
						<th class="text-left"><?= LangHelper::get('created_at', 'Created at')?></th>
						<th class="text-right"><?= LangHelper::get('options', 'Options')?></th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($users as $user): ?>
						<tr>
							<td class="pad-right100"><?= $user->username?></td>
							<td class="pad-right100"><?= $user->email?></td>
							<td class="pad-right100"><?= $user->getRole('name')?> <?= $user->hasRole('regional_admin') ? '- '. ($user->region->first() != null ? $user->region->first()->region_name : "" ): NULL; ?></td>
							<td class="pad-right100"><?= $user->last_login?></td>
							<td class="pad-right100"><?= $user->created_at?></td>
							<td class="transparent text-right">
								<a class="button small call info" href="<?= route('users/info', $user->id)?>"><?= LangHelper::get('info', 'Info')?></a>
							</td>	
							<td class="transparent text-right">	
								<a class="button small call" href="<?= route('users/edit', $user->id)?>"><?= LangHelper::get('edit', 'Edit')?></a>
							</td>	
							<td class="transparent text-right">	
								<a class="button danger small" ng-click="removeUser($event)" href="<?= route('users/delete', $user->id)?>"><?= LangHelper::get('delete', 'Delete')?></a>
							</td>
						</tr>
					<?php endforeach ?>

				</tbody>
		</table>


    </div>
</div>

<script src="/js/ang/userApp.js"></script>

<script>
    angular.element(document).ready(function() {
        angular.bootstrap('.ang', ['usersApp']);
    });
</script>