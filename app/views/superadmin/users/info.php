<div class="container">
    <div class="grid">
    	<div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>

       <div class="tournament-first">
           <div class="table-title tournament">
              <h3><?= LangHelper::get('user_info', 'User info')?> - <?= $user->username?></h3>
              <span class="line"></span>
          </div>
       </div>

       <div class="tournament-second pad-top40">
            <div class="row players-info">
                <div class="half">
                      <p><span class="color-rfet"><?= LangHelper::get('username', 'Username')?>:</span> <?= $user->username?></p>
                      <p><span class="color-rfet"><?= LangHelper::get('email', 'Email')?>:</span> <?= $user->email?></p>
                      <p><span class="color-rfet"><?= LangHelper::get('last_login', 'Last login')?>:</span> <?= $user->last_login?></p>
                      <p><span class="color-rfet"><?= LangHelper::get('created_at', 'Created at')?>:</span> <?= $user->created_at?></p>
                </div>
            </div>
	    </div>
    </div>
</div>
