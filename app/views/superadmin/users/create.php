<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>
            </div>
        </div>
        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('add_new_user', 'Add new user') ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="tournament-second">
            <form name="createForm" ng-class="{ 'show-errors': showErrors }" ng-submit="createUser()"
                  ng-controller="createController" novalidate>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <fieldset>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.contact.name" name="name" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.contact.surname" name="surname" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="username"><?= LangHelper::get('username', 'Username') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.users.username" name="username" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="password"><?= LangHelper::get('password', 'Password') ?></label>

                                <div class="input-control text">
                                    <input type="password" ng-model="formData.users.password" name="password" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="roles"><?= LangHelper::get('roles', 'Roles') ?></label>

                                <div class="input-control select">
                                    <?= Form::select('role', $roles, null, array('required', 'ng-model' => 'formData.roles.role')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="email"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" ng-model="formData.users.email" name="email" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half" ng-show="formData.roles.role==3">
                            <div class="form-control">
                                <label for="region"><?= LangHelper::get('regions', 'Regions') ?></label>

                                <div class="input-control select">
                                    <?= Form::select('region', $regions, null, array('ng-model' => 'formData.region.region_id')); ?>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                                style="margin-top: 38px;"><?= LangHelper::get('save', 'Save') ?></button>
                    </div>

                    <form-errors></form-errors>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</div>

<script src="/js/ang/create.js"></script>

<script>
    $(document).ready(function () {
        $("select").select2();
    });

    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['create']);
    });
</script>