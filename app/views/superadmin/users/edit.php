<div class="container ang">
    <div class="grid">
        <div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
        </div>
        <div class="row">
            <div class="clearfix">
                <div class="empty-space"></div>

            </div>
        </div>

        <div class="tournament-first">
            <div class="table-title tournament">
                <h3><?= LangHelper::get('edit_user', 'Edit user') ?> - <?= $user->username ?></h3>
                <span class="line"></span>
            </div>
        </div>

        <div class="tournament-second">
            <form name="editForm" ng-class="{ 'show-errors': showErrors }"
                  ng-submit="formSubmit($event, 'PUT', 'User successfully updated','<?= route('users/edit', $user->id) ?>')"
                  ng-controller="FormController" novalidate autocomplete="off">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="_method" value="PUT"/>
                <fieldset>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('name', 'Name') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.contact.name" name="name" required
                                           ng-init="formData.contact.name='<?= $user->info('name') ?>'">
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('surname', 'Surname') ?></label>

                                <div class="input-control text">
                                    <input type="text" ng-model="formData.contact.surname" name="surname" required
                                           ng-init="formData.contact.surname='<?= $user->info('surname') ?>'">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('username', 'Username') ?></label>

                                <div class="input-control text">
                                    <input type="username" ng-init="formData.users.username='<?= $user->username ?>'"
                                           ng-model="formData.users.username" name="username" required>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('email', 'Email') ?></label>

                                <div class="input-control text">
                                    <input type="email" ng-model="formData.users.email"
                                           ng-init="formData.users.email='<?= $user->email ?>'" name="email" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="half">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('role', 'Role') ?></label>

                                <div class="input-control select">
                                    <?= Form::select('role', $roles, null, array('required',
                                        'ng-model' => 'formData.roles.role',
                                        'ng-init' => 'formData.roles.role="' . $user->getRole('id') . '"')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="half">
                            <div class="form-control">
                                <label for="surname"><?= LangHelper::get('change_user_password', 'Change user password') ?></label>
                                <div class="input-control text">
                                    <input type="password"
                                           ng-model="formData.users.password" name="password" required>
                                </div>
                            </div>
                        </div>
                        <div class="half" ng-show="formData.roles.role==3">
                            <div class="form-control">
                                <label for="name"><?= LangHelper::get('region', 'Region') ?></label>

                                <div class="input-control select">
                                    <?= Form::select('region_id', $regions, null, array('ng-model' => 'formData.region.region_id')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="button medium next-yellow-arrow right yellow-color dark-col-bg"
                            style="margin-top: 38px;"><?= LangHelper::get('save', 'Save') ?></button>
                    <form-errors></form-errors>
                </fieldset>
            </form>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("select").select2();
    });
    angular.element(document).ready(function () {
        angular.bootstrap('.ang', ['genericFormApp']);
    });
</script>