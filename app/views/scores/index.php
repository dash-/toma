<div class="container ang">
    <div class="grid">
        <div class="row">
            <div class="row">
                <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
            </div>
            <div class="row">
                <div class="clearfix">
                    <div class="empty-space"></div>
                </div>
            </div>

            <a href="<?= URL::to('/scores/live-scores') ?>">
                <div class="rfet-button rfet-yellow" style="float:right; color:black !important">Live Scores</div>
            </a>

            <div class="tournament-first">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('live_scores', 'Live Scores') ?></h3>
                    <span class="line"></span>
                </div>
            </div>

        </div>
        <div class="row">
            <?= View::make("partials/scores_live", array(
                'in_progress' => $in_progress,
            )) ?>

            <h3><?= LangHelper::get('Completed', 'Completed') ?></h3>
            <span class="line"></span>

            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th width="40%" class="text-left">Tournament</th>
                    <th class="text-left">Match contestants</th>
                    <th class="text-right">Current result</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($finished as $finish): ?>
                    <tr class="text-left">
                        <td class="text-left"><?= $finish->tournament->title ?></td>
                        <td class="text-left contestants">
                            <?= DrawMatchHelper::scheduleName($finish->match, 'team1_data', $finished_matches) ?>

                            <div class="vs live">vs</div>

                            <?= DrawMatchHelper::scheduleName($finish->match, 'team2_data', $finished_matches, true) ?>

                        </td>
                        <td class="text-right scores live-rfet-score">
                            <?php foreach ($finish->match->scores as $fresult): ?>

                                <?= $fresult->set_team1_score . DrawMatchHelper::matchTie($fresult->set_team1_tie) . '-' . $fresult->set_team2_score . DrawMatchHelper::matchTie($fresult->set_team2_tie) . '; ' ?>
                            <?php endforeach ?>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>
            </table>

            <h3><?= LangHelper::get('Not Started', 'Not Started') ?></h3>
            <span class="line"></span>
            <table class="table bg-dark tournament-table">
                <thead>
                <tr>
                    <th width="40%" class="text-left">Tournament</th>
                    <th class="text-left">Match contestants</th>
                    <th class="text-right"><?= LangHelper::get('info','Info') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($not_started as $nstart): ?>
                    <tr>
                        <td class="text-left"><?= $nstart->tournament->title ?></td>
                        <td class="text-left contestants">
                            <?php if ($nstart->match): ?>
                                <?= DrawMatchHelper::scheduleName($nstart->match, 'team1_data', $not_started_matches) ?>
                                <div class="vs live">vs</div>
                                <?= DrawMatchHelper::scheduleName($nstart->match, 'team2_data', $not_started_matches, true) ?>
                            <?php endif ?>
                        </td>
                        <td class="text-right scores">
                            <?= DrawMatchHelper::getStartingDateTime($nstart).' - Court '. $nstart->court_number ?>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>
            </table>

        </div>
    </div>

</div>
