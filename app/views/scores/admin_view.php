<div class="container ang">
    <div class="grid">
        <div class="row">
            <div class="row">
                <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
            </div>
            <div class="row">
                <div class="clearfix">
                    <div class="empty-space"></div>
                </div>
            </div>
            <div class="tournament-first">
                <div class="table-title tournament">
                    <h3><?= LangHelper::get('live_scores', 'Live Scores')?></h3>
                    <span class="line"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <?php foreach($draws as $draw):?>
            <h3><?= $draw->category->name.', '.$draw->typeName().', '.$draw->genderName() ?></h3>
            <?php if ($draw->matches):?>
                <table class="table bg-dark tournament-table">
                    <thead>
                    <tr>
                        <th class="text-left" width="10%"><?= LangHelper::get('round_number', 'Round number')?></th>
                        <th class="text-left"><?= LangHelper::get('match_contestants', 'Match contestants')?></th>
                        <th class="text-left"><?= LangHelper::get('current_result', 'Current result')?></th>
                        <th class="text-left"></th>
                    </tr>
                    </thead>
                <?php foreach($draw->matches as $match):?>
                    <?php if ($match->team1_data OR $match->team2_data):?>
                    <tr>
                        <td><?= $match->round_number?></td>
                        <td><?= DrawMatchHelper::getContestants($match)?></td>
                        <td><?= DrawMatchHelper::scoreAsString($match)?></td>
                        <td class="text-right">
                            <a class="yellow-color" href="/match/match-live-mode/<?= $match->id?>">
                                <?= LangHelper::get('enter_live_match_mode','Enter live match mode')?>
                            </a>
                        </td>
                    </tr>
                    <?php endif?>

                <?php endforeach?>
                </table>
            <?php endif?>


        <?php endforeach?>
    </div>

</div>
