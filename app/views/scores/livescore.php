<div class="row">
            <a class="metro button master-back call" href="<?= BackButtonHelper::go_back() ?>"></a>
</div>
<?php foreach ($tournaments as $tournament): ?>

    <div class="row live_score_box">
        <div class="tiles tile-2">

            <div class="tile-icon">
                <div class="livescore-icon">
                </div>
                <div class="tile-text table-livescore">

                    <?= View::make('partials/livescorepage', ['tournament' => $tournament]); ?>
                </div>
            </div>
        </div>
    </div>


<?php endforeach ?>
