<?php 


class TestJob {

    public function fire($job, $data)
    {
    	DB::disableQueryLog();
        Player::chunk(2000, function($players)
        {
            foreach ($players as $player) 
            {
                TempRanking::create([
                    'player_id'      => $player->id,
                    'ranking'        => $player->ranking,
                    'ranking_points' => $player->ranking_points,
                ]);
            }
        });

        Log::info('This is was written via the QueueDemo class at '.time().'.');
    }

}

