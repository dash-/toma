<?php

class CategoryRankingConfigurationSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


		$csvFile = public_path().'/csv/category_by_ranking.csv';
		$this->read_csv($csvFile);

	}

	private function read_csv($filename='', $delimiter=';')
	{
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$data = array();
		$header = FALSE;

		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

				if(!$header) {
					$header = array_flip($row);
				}
				else
				{
					$cat = new CategoryRankingConfiguration();
					$cat->ranking_from = $row[$header['ranking_from']] ? $row[$header['ranking_from']] : null;
					$cat->ranking_to = $row[$header['ranking_to']] ? $row[$header['ranking_to']] : null;
					$cat->points = $row[$header['points']] ? $row[$header['points']] : null;
					$cat->category = $row[$header['category']];
					$cat->save();
				}
			}
			fclose($handle);
		}
		return $data;
	}

}
