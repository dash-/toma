<?php

class TempTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
		function csv_to_array($filename='', $delimiter=',')
		{
		    if(!file_exists($filename) || !is_readable($filename))
		        return FALSE;
		 
		    $data = array();
		    $header = FALSE;

		    if (($handle = fopen($filename, 'r')) !== FALSE)
		    {
		    	while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[1]) {

			        if(!$header)
			        	$header = array_flip($row);
			        else
			        {
				        $item = array();
						$item['name']          = $row[$header['name']];
						$item['surname']       = $row[$header['surname']];
						$item['phone']         = $row[$header['phone']];
						$item['email']         = $row[$header['email']];
						$item['web']           = "";
						$item['date_of_birth'] = $row[$header['date_of_birth']];
						$item['address']       = $row[$header['address']];
						$item['city']          = $row[$header['city']];
						$item['postal_code']   = $row[$header['postal_code']];
						$item['sex']           = $row[$header['sex']];
						$item['card_id']       = $row[$header['card_id']];

			         	$contact = DB::table('contacts')->insertGetId($item);
			         	
			         	$player = array();

						$player['contact_id']         = $contact;
						$player['licence_number']     = $row[$header['licence_number']];
						$player['ranking']            = $row[$header['ranking']];
						$player['ape_id']             = $row[$header['ape_id']];
						$player['nationality']        = $row[$header['nationality']];
						$player['current_situation']  = $row[$header['current_situation']];
						$player['federation_club_id'] = $row[$header['federation_club_id']];
						$player['province_club_id']   = $row[$header['province_club_id']];
						$player['club_id']            = $row[$header['club_id']];
						$player['mutual']             = $row[$header['mutual']];

				        DB::table('players')->insert($player);
			        }
			    }
		        fclose($handle);
		    }
		    return $data;
		}

		$csvFile = public_path().'/csv/sampleExportTest.csv';
		csv_to_array($csvFile);

	}

}
