<?php

class NotificationTypesTableSeederPlayer extends Seeder {

	public function run()
	{
		$data = [
			[
				'id' => 5,
				'name' => 'Request for player approval',
			],
			[
				'id' => 6,
				'name' => 'Player request approved',
			],
			[
				'id' => 7,
				'name' => 'Player request rejected',
			],
		];

		DB::table('notification_types')->insert($data);
	}

}