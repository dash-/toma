<?php

class AddRolesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			'user_id' => 1,
			'role_id' => 1,
		);

		DB::table('users_roles')->insert($data);
	}

}
