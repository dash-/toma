<?php

class AddPlayerRoleSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			0 => array(
				'name' => 'player',
				'description' => 'Standard player role',
			)
		);

		DB::table('roles')->insert($data);
	}

}
