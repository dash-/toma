<?php

class DummyAddClubId extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        for($i=1;$i<Club::count();$i++) {
            $club_id = rand(1, 51);
            DB::table('tournaments')->where("id",$i)->update(array('club_id'=>$club_id));
        }
	}

}
