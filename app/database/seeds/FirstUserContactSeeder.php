<?php

class FirstUserContactSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$user = User::find(1);
		$contact = new Contact;

		$contact->name = 'Super';
		$contact->surname = 'User';
		$contact->save();

		$admin = new Admin;
		$admin->contact_id = $contact->id;
		$admin->user_id = $user->id;

		$admin->save();

	}

}
