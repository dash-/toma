<?php

class DummyRankingPointsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);

        $counter = range(1, Player::count());
		for ($i=1; $i <= count($counter); $i++) {
			DB::table('players')->where('id',$i)->update(array('ranking_points'=>rand(1, 3000)));
		}


		// DB::table('players')->insert($player);
	}

}
