<?php

class TournamentTypeSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


		$csvFile = public_path().'/csv/tournament_category.csv';
		$this->read_csv($csvFile);

	}

	private function read_csv($filename='', $delimiter=';')
	{
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$data = array();
		$header = FALSE;

		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

				if(!$header) {
					$header = array_flip($row);
				}
				else
				{
					$nat = new TournamentType();
					$nat->type = $row[$header['type']];
					$nat->category_tournament = $row[$header['category_tournament']];
					$nat->coeficient = str_replace(",", ".", $row[$header['coeficient']]);
					$nat->save();
				}
			}
			fclose($handle);
		}
		return $data;
	}

}
