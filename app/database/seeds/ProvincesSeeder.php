<?php

class ProvincesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			array('name'=> 'alava','region_id'=>'11','id'=>'1'),
			array('name'=> 'albacete','region_id'=>'16','id'=>'2'),
			array('name'=> 'alicante','region_id'=>'10','id'=>'3'),
			array('name'=> 'almeria','region_id'=>'1','id'=>'4'),
			array('name'=> 'avila','region_id'=>'15','id'=>'5'),
			array('name'=> 'badajoz','region_id'=>'13','id'=>'6'),
			array('name'=> 'illes balears','region_id'=>'4','id'=>'7'),
			array('name'=> 'barcelona','region_id'=>'7','id'=>'8'),
			array('name'=> 'burgos','region_id'=>'15','id'=>'9'),
			array('name'=> 'caceres','region_id'=>'13','id'=>'10'),
			array('name'=> 'cadiz','region_id'=>'1','id'=>'11'),
			array('name'=> 'castellon','region_id'=>'10','id'=>'12'),
			array('name'=> 'ciudad real','region_id'=>'16','id'=>'13'),
			array('name'=> 'cordoba','region_id'=>'11','id'=>'14'),
			array('name'=> 'la coruña','region_id'=>'9','id'=>'15'),
			array('name'=> 'cuenca','region_id'=>'16','id'=>'16'),
			array('name'=> 'girona','region_id'=>'7','id'=>'17'),
			array('name'=> 'granada','region_id'=>'1','id'=>'18'),
			array('name'=> 'guadalajara','region_id'=>'16','id'=>'19'),
			array('name'=> 'guipuzcoa','region_id'=>'11','id'=>'20'),
			array('name'=> 'huelva','region_id'=>'1','id'=>'21'),
			array('name'=> 'huesca','region_id'=>'2','id'=>'22'),
			array('name'=> 'jaen','region_id'=>'1','id'=>'23'),
			array('name'=> 'leon','region_id'=>'15','id'=>'24'),
			array('name'=> 'lleida','region_id'=>'7','id'=>'25'),
			array('name'=> 'la rioja','region_id'=>'17','id'=>'26'),
			array('name'=> 'lugo','region_id'=>'9','id'=>'27'),
			array('name'=> 'madrid','region_id'=>'8','id'=>'28'),
			array('name'=> 'malaga','region_id'=>'1','id'=>'29'),
			array('name'=> 'murcia','region_id'=>'12','id'=>'30'),
			array('name'=> 'navarra','region_id'=>'5','id'=>'31'),
			array('name'=> 'orense','region_id'=>'9','id'=>'32'),
			array('name'=> 'asturias','region_id'=>'3','id'=>'33'),
			array('name'=> 'palencia','region_id'=>'15','id'=>'34'),
			array('name'=> 'las palmas gran canaria','region_id'=>'14','id'=>'35'),
			array('name'=> 'pontevedra','region_id'=>'9','id'=>'36'),
			array('name'=> 'salamanca','region_id'=>'15','id'=>'37'),
			array('name'=> 'santa cruz tenerife','region_id'=>'14','id'=>'38'),
			array('name'=> 'cantabria','region_id'=>'6','id'=>'39'),
			array('name'=> 'segovia','region_id'=>'15','id'=>'40'),
			array('name'=> 'sevilla','region_id'=>'1','id'=>'41'),
			array('name'=> 'soria','region_id'=>'15','id'=>'42'),
			array('name'=> 'tarragona','region_id'=>'7','id'=>'43'),
			array('name'=> 'teruel','region_id'=>'2','id'=>'44'),
			array('name'=> 'toledo','region_id'=>'16','id'=>'45'),
			array('name'=> 'valencia','region_id'=>'10','id'=>'46'),
			array('name'=> 'valladolid','region_id'=>'15','id'=>'47'),
			array('name'=> 'vizcaya','region_id'=>'11','id'=>'48'),
			array('name'=> 'zamora','region_id'=>'15','id'=>'49'),
			array('name'=> 'zaragoza','region_id'=>'2','id'=>'50'),
			array('name'=> 'ceuta','region_id'=>'18','id'=>'51'),
			array('name'=> 'melilla','region_id'=>'19','id'=>'52'),
         // array('name'=>   'lesproves','region_id'=>'99','id'=>'99'),
        );

        foreach ($data as $d) {
            $item = array('province_name' => ucfirst($d['name']), 'id' => $d['id'],'region_id'=>$d['region_id']);
            DB::table('provinces')->insert($item);
        }
	}

}
