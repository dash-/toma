<?php

class AdditionalPointsConfigurationSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


		$csvFile = public_path().'/csv/additional_points.csv';
		$this->read_csv($csvFile);

	}

	private function read_csv($filename='', $delimiter=';')
	{
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$data = array();
		$header = FALSE;

		DB::table("additional_points_configuration")->truncate();

		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

				if(!$header) {
					$header = array_flip($row);
				}
				else
				{
					$conf = new AdditionalPointsConfiguration();
					$conf->gender = $row[$header['gender']];
					$conf->ranking_from = $row[$header['from_ranking']] ? $row[$header['from_ranking']] : null;
					$conf->ranking_to = $row[$header['to_ranking']] ? $row[$header['to_ranking']] : null;
					$conf->additional_points = $row[$header['additional_points']];
					$conf->save();
				}
			}
			fclose($handle);
		}
		return $data;
	}

}
