<?php


class RfetPreviousPointsTableSeeder extends Seeder
{

    public function run()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


        $csvFile = public_path() . '/csv/complet18LastMonths.txt';

        $this->read_csv($csvFile);

    }

    private function read_csv($filename = '', $delimiter = '|')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        $header = FALSE;
        echo "This seed will take a while...";
        DB::table("rfet_previous_points")->truncate();
        $i=0;
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

                if (!$header) {
                    $header = array_flip($row);
                } else {
                    echo $i.' ';
                    $i++;
                    $period[0] = substr($row[$header['period']], 0, 4);
                    $period[1] = substr($row[$header['period']], 4, 2);
                    $points = array(
                        'gender' => $row[$header['gender']] ? $row[$header['gender']] : null,
                        'period' => $period[0] . '-' . $period[1] . '-01',
                        'tournament_id' => $row[$header['tournament_id']] ? $row[$header['tournament_id']] : null,
                        'licence_id' => $row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'status_round' => $row[$header['status_round']] ? $row[$header['status_round']] : null,
                        'opponent_licence_number' => $row[$header['opponent_licence_number']] ? $row[$header['opponent_licence_number']] : null,
                        'round' => $row[$header['round']] ? $row[$header['round']] : null,
                        'opponent_ottorga_points' => $row[$header['opponent_ottorga_points']] ? $row[$header['opponent_ottorga_points']] : null,
                        'points_from_round' => $row[$header['points_from_round']] ? $row[$header['points_from_round']] : null,
                    );
                    DB::table("rfet_previous_points")->insert($points);

                }
            }
            fclose($handle);
        }
        return $data;
    }


}