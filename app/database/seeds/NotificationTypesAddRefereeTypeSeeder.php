<?php

class NotificationTypesAddRefereeTypeSeeder extends Seeder {

	public function run()
	{
		$data = [
			[
				'id'   => 9,
				'name' => 'Draw referee notification',
			]
		];

		DB::table('notification_types')->insert($data);
	}

}