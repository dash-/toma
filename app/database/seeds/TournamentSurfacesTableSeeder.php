<?php

class TournamentSurfacesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			0 => array(
				'name' => 'Acrylic',
			),
			1 => array(
				'name' => 'Artificial clay',
			),
			2 => array(
				'name' => 'Artificial grass',
			),
			3 => array(
				'name' => 'Asphalt',
			),
			4 => array(
				'name' => 'Carpet',
			),
			5 => array(
				'name' => 'Clay',
			),
			6 => array(
				'name' => 'Concrete',
			),
			7 => array(
				'name' => 'Grass',
			),
			8 => array(
				'name' => 'Other',
			),
		);

		DB::table('tournament_surfaces')->insert($data);
	}

}
