<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class RegionsTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			'Andaluza',
			'Aragonesa',
			'Principado de Asturias',
			'Illes Balears',
			'Navarra',
			'Cantabra',
			'Catalana',
			'Madrid',
			'Gallega',
			'Comunidad Valenciana',
			'Vasca',
			'Region de Murcia',
			'Extremeña',
			'Canaria',
			'Castilla-Leon',
			'Castilla la Mancha',
			'Riojana',
			'Ceuta',
			'Melillense',
		);

		for($i = 0; $i < count($data); $i++) {
            $item = array('region_name' => $data[$i]);
            DB::table('regions')->where("id", $i+1)->update($item);
        }
		
		// foreach ($data as $d) 
		// {
		// 	$item = array('name' => $d);
		// 	DB::table('regions')->update($item);
		// }
	}



}