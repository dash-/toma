<?php

class CreatePricePerPlayerSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


		$csvFile = public_path().'/csv/prices_per_player.csv';
		$this->read_csv($csvFile);

	}

	private function read_csv($filename='', $delimiter=';')
	{
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$data = array();
		$header = FALSE;

		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			$draw_category_id = 1;
			while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

				if(!$header) {
					$header = array_flip($row);
				}
				else
				{
					$tour = new TournamentPerPlayerPrice();
					$tour->draw_category_id = $draw_category_id;
					$tour->premia =  str_replace(",", ".", $row[$header['premia']]);
					$tour->premia_0_3000 =  str_replace(",", ".", $row[$header['premia_0_3000']]);
					$tour->premia_3000_6000 =  str_replace(",", ".", $row[$header['premia_3000_6000']]);
					$tour->premia_6001 =  str_replace(",", ".", $row[$header['premia_6001']]);
					$tour->save();
					$draw_category_id++;
				}
			}
			fclose($handle);
		}
		return $data;
	}

}
