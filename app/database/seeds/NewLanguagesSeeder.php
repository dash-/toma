<?php

class NewLanguagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data =[
            0 => [
                'name'     => 'Basque',
                'code'     => 'basq',
                'encoding' => 'UTF-8',
            ],
            1 => [
                'name'     => 'Galician',
                'code'     => 'gali',
                'encoding' => 'UTF-8',
            ],
            2 => [
                'name'     => 'Chinese',
                'code'     => 'chi',
                'encoding' => 'UTF-8',
            ],
        ];

        DB::table('languages')->insert($data);
    }

}
