<?php

class RankingCalculationPrizesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = [
        	array('prize_from' => 0, 'prize_to' => 300, 'stars' => 1),
        	array('prize_from' => 300, 'prize_to' => 600, 'stars' => 2),
        	array('prize_from' => 600, 'prize_to' => 1500, 'stars' => 3),
        	array('prize_from' => 1500, 'prize_to' => 3000, 'stars' => 4),
        	array('prize_from' => 3000, 'prize_to' => 4500, 'stars' => 5),
        	array('prize_from' => 4500, 'prize_to' => 6000, 'stars' => 6),
        	array('prize_from' => 6000, 'prize_to' => 7500, 'stars' => 7),
        	array('prize_from' => 7500, 'prize_to' => 9000, 'stars' => 8),
        	array('prize_from' => 9000, 'prize_to' => 12000, 'stars' => 9),
        	array('prize_from' => 12000, 'prize_to' => 15000, 'stars' => 10),
        	array('prize_from' => 15000, 'prize_to' => 1000000, 'stars' => 11),
        ];

        foreach ($data as $d) {
            DB::table('ranking_calculation_prizes')->insert($d);
        }
	}

}
