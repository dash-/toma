<?php

class ClubsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

//
//    public function __construct()
//    {
//        $this->table = 'clubs';
//        $this->filename = public_path().'/csv/clubs.csv';
//    }

    public function run()
    {
        ini_set("memory_limit", -1);
        function csv_to_array($filename = '', $delimiter = ';')
        {
            if (!file_exists($filename) || !is_readable($filename))
                return false;

            $data = array();
            $header = false;
            if (($handle = fopen($filename, 'r')) !== false) {
                while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[1]) {

                    if (!$header)
                        $header = array_flip($row);
                    else {
                        $string = $row[$header['city']];
                        $exploded = explode(" ", $string);
                        $postal_code = $exploded[0];
                        unset($exploded[0]);
                        $city = implode(" ", $exploded);

                        $item = array();
                        $item['region_id'] = $row[$header['region_id']];
                        $item['province_id'] = $row[$header['province_id']];
                        $item['province_club_id'] = $row[$header['province_club_id']];
                        $item['club_name'] = $row[$header['name']];
                        $item['club_city'] = $city;
                        $item['club_address'] = $row[$header['address']];
                        $item['club_postal_code'] = $postal_code;
                        $item['short_name'] = $row[$header['short_name']];
                        $item['founded_date'] = DateTimeHelper::GetPostgresDateTime($row[$header['founded_date']]);
                        $item['number_of_tracks'] = $row[$header['number_of_tracks']] ? $row[$header['number_of_tracks']] : 0;
                        $item['club_phone'] = $row[$header['phone']];
                        $item['fax'] = $row[$header['fax']];
                        $item['phone2'] = $row[$header['phone2']];
                        $item['president'] = $row[$header['president']];
                        $item['nif'] = $row[$header['nif']];
                        $item['club_email'] = $row[$header['email']];
                        $item['web'] = $row[$header['web']];
                        $item['created_at'] = DateTimeHelper::GetDateNow();
                        $item['updated_at'] = DateTimeHelper::GetDateNow();

                        DB::table('clubs')->insert($item);

                    }
                }
                fclose($handle);
            }

            return $data;
        }

        $csvFile = public_path() . '/csv/clubs_17122014.csv';
        csv_to_array($csvFile);

    }

}
