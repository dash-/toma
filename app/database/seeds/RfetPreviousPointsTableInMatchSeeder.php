<?php


class RfetPreviousPointsTableInMatchSeeder extends Seeder
{

    public function run()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


        $csvFile = public_path() . '/csv/complet18LastMonths.txt';

        $this->read_csv($csvFile);

    }

    private function read_csv($filename = '', $delimiter = '|')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        $header = FALSE;
        echo "This seed will take a while...";
        DB::table("ranking_matchpoints")->truncate();
        $i=0;
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

                if (!$header) {
                    $header = array_flip($row);
                } else {
                    echo $i.' ';
                    $i++;
                    $period[0] = substr($row[$header['period']], 0, 4);
                    $period[1] = substr($row[$header['period']], 4, 2);
                    $points = array(
                        'player_id' => 0,
                        'licence_number' => $row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'signup_id' => 0,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'match_id' => 0,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'draw_id' => 0,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'list_type' => 0,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'old_score' => true,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'match_status' => $row[$header['status_round']] && $row[$header['status_round']]  == 'G' ? 1 : 2,// : null,
//                        'round_number' => 0,//$row[$header['licence_number']] ? $row[$header['licence_number']] : null,
                        'tournament_period' => $period[0] . '-' . $period[1] . '-01',
                        'tournament_id' => $row[$header['tournament_id']] ? $row[$header['tournament_id']] : null,
                        'assigned' => false,
//                        'opponent_licence_number' => $row[$header['opponent_licence_number']] ? $row[$header['opponent_licence_number']] : null,
                        'round_number' => $row[$header['round']] ? $row[$header['round']] : null,
                        'otroga_points' => $row[$header['opponent_ottorga_points']] ? $row[$header['opponent_ottorga_points']] : null,
                        'round_points' => $row[$header['points_from_round']] ? $row[$header['points_from_round']] : null,
                        'created_at' => DateTimeHelper::GetPostgresDateTime(),
                        'updated_at' => DateTimeHelper::GetPostgresDateTime()
                    );
//                    echo Debug::vars($points);die;
                    DB::table("ranking_matchpoints")->insert($points);

                }
            }
            fclose($handle);
        }
        return $data;
    }


}