<?php

class ClubRankingSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);

        Player::orderBy('id')->chunk(1000, function($players)
		{
			$insert_arr = [];
		    foreach ($players as $player)
		    {
		    	$ranking = ($player->ranking) ? $player->ranking : 0;
		    	$player_id = $player->id;
		    	
				$regional_ranking = $ranking;
				$province_ranking = $ranking;
				$club_ranking     = $ranking;

        		$insert_arr[] = '(' .$player_id. ', ' . $regional_ranking. ', ' .$province_ranking.', '.$club_ranking.')';
		    }

		    $sql = "update players as player
				set regional_ranking = s.regional_ranking, province_ranking = s.province_ranking, club_ranking = s.club_ranking
				from 
				(
				  values". implode(',', $insert_arr)."
				) as s(id, regional_ranking, province_ranking, club_ranking)
				where player.id = s.id";

		    DB::statement($sql);
		});


		// DB::table('players')->insert($player);
	}

}
