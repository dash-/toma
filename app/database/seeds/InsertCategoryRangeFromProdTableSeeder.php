<?php


class InsertCategoryRangeFromProdTableSeeder extends Seeder {

	public function run()
	{
        $categories = DrawCategory::get();
        foreach($categories as $category){
            switch($category->name){
                case "Sub-10":
                    $category->range_from = 0;
                    $category->range_to = 9;
                    break;
                case "Alevin":
                    $category->range_from = 10;
                    $category->range_to = 11;
                    break;
                case "Infantil":
                    $category->range_from = 12;
                    $category->range_to = 13;
                    break;
                case "Cadete":
                    $category->range_from = 14;
                    $category->range_to = 15;
                    break;
                case "Junior":
                    $category->range_from = 16;
                    $category->range_to = 17;
                    break;
                case "Absoluto":
                    $category->range_from = 0;
                    $category->range_to = 170;
                    break;
                case "+35":
                    $category->range_from = 35;
                    $category->range_to = 39;
                    break;
                case "+40":
                    $category->range_from = 40;
                    $category->range_to = 44;
                    break;
                case "+45":
                    $category->range_from = 45;
                    $category->range_to = 49;
                    break;
                case "+50":
                    $category->range_from = 50;
                    $category->range_to = 54;
                    break;
                case "+55":
                    $category->range_from = 55;
                    $category->range_to = 59;
                    break;
                case "+60":
                    $category->range_from = 60;
                    $category->range_to = 64;
                    break;
                case "+65":
                    $category->range_from = 65;
                    $category->range_to = 69;
                    break;
                case "+70":
                    $category->range_from = 70;
                    $category->range_to = 74;
                    break;
                case "+75":
                    $category->range_from = 75;
                    $category->range_to = 79;
                    break;
                case "+80":
                    $category->range_from = 80;
                    $category->range_to = 84;
                    break;
                case "+85":
                    $category->range_from = 85;
                    $category->range_to = 100;
                    break;

            }
            $category->save();
        }
	}

}