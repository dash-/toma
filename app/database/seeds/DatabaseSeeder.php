<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->doSeed('RolesTableSeeder');
		$this->doSeed('AdminUserSeeder');
//		$this->doSeed('FirstUserContactSeeder');
		$this->doSeed('TournamentSurfacesTableSeeder');
		$this->doSeed('NotificationTypesTableSeeder');
		$this->doSeed('RegionsDataSeeder');
		$this->doSeed('ClubsSeeder');

		$this->doSeed('LanguagesSeeder');
		$this->doSeed('ProvincesSeeder');
		$this->doSeed('RankingCalculationPointsSeeder');
		$this->doSeed('RankingCalculationPrizesSeeder');
		$this->doSeed('RankingPointsSeeder');
		$this->doSeed('DrawCategoriesSeeder');

		$this->doSeed('NewPlayersSeeder');
		$this->doSeed('RefereesSeeder');
		$this->doSeed('PlayersRankingsSeeder');
//		$this->doSeed('MalePlayersRankingsSeeder');
        $this->doSeed('RegionsTableSeeder');
		$this->doSeed('NewNotificationTypeSeeder');
		$this->doSeed('CatLanguageSeeder');
		$this->doSeed('NewLanguagesSeeder');
		$this->doSeed('AdditionalPointsConfigurationSeeder');
		$this->doSeed('NationalitySeeder');
		$this->doSeed('CategoryRankingConfigurationSeeder');
		$this->doSeed('TournamentTypeSeeder');
		$this->doSeed('CreatePricePerPlayerSeeder');
		$this->doSeed('CategoryRankingConfigurationUpdateSeeder');
//		$this->doSeed('RfetPreviousPointsTableSeeder');
		$this->doSeed('RfetPreviousPointsTableInMatchSeeder');

//ADD LATER
		$this->doSeed('ClubsLocationSeeder');
		$this->doSeed('RegionsLocationSeeder');
		$this->doSeed('LicenceRangesSeeder');
		$this->doSeed('NotificationAddTournamentNotesSeeder');
		$this->doSeed('AddSuperRefereeRoleSeeder');

        //uncomment if database is fresh
		$this->doSeed('InsertCategoryRangeFromProdTableSeeder');

        $this->doSeed('InsertCategoryRangeSignupTableSeeder');
        $this->doSeed('AddReculcaltionNotificationTypeTableSeeder');



		// $this->doSeed('RegionsTableSeeder');
		// $this->doSeed('DummyRankingsSeeder');
		// $this->doSeed('DummyAddClubId');
		// $this->doSeed('DummyRankingPointsSeeder');
		// $this->doSeed('NewRankingsSeeder');
		// $this->doSeed('ClubRankingSeeder');


	}

    public function doSeed($seed_name)
    {
        $exists = DB::table("table_seeds")->where("seed_name", $seed_name)->count();
        if(!$exists)
        {
            $this->call($seed_name);
            DB::table("table_seeds")->insert(array("seed_name"=>$seed_name));
        }
    }


}
