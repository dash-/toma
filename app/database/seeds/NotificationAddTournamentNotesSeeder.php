<?php

class NotificationAddTournamentNotesSeeder extends Seeder {

	public function run()
	{
		$data = [
			[
				'id'   => 11,
				'name' => 'Tournament note notification',
			]
		];

		DB::table('notification_types')->insert($data);
	}

}