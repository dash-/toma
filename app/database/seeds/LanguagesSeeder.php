<?php

class LanguagesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data =[ 
			0 => [
				'name'     => 'English',
				'code'     => 'en',
				'encoding' => 'UTF-8',
			],
			1 => [
				'name'     => 'Español',
				'code'     => 'es',
				'encoding' => 'UTF-8',
			],
		];

		DB::table('languages')->insert($data);
	}

}
