<?php

class PlayersRankingsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


        $csvFile = public_path() . '/csv/ranking_17122014.csv';
        $this->rankings_to_array($csvFile);

    }

    private function rankings_to_array($filename = '', $delimiter = ';')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        $header = FALSE;

//        DB::table("players")->update(array(
//            "ranking_points" => null,
//            "ranking" => null,
//            "regional_ranking" => null,
//            "province_ranking" => null,
//            "club_ranking" => null
//        ));
//
        $i=0;
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

                if (!$header) {
                    $header = array_flip($row);
                } else {
//			        	$player = Player::whereLicenceNumber($row[$header['licence_number']])->update([
//							'ranking_points'   => $row[$header['ranking_points']],
//							'ranking'          => $row[$header['ranking']],
//							'regional_ranking' => $row[$header['regional_ranking']],
//							'province_ranking' => $row[$header['province_ranking']],
//							'club_ranking'     => $row[$header['club_ranking']],
// 			        	]);
                    $i++;
                    var_dump($i);
                    $player = Player::whereLicenceNumber($row[$header['licence_number']])->first();
                    if ($player) {
                        $player->ranking_points = $row[$header['ranking_points']] ? $row[$header['ranking_points']] : null;
                        $player->ranking = $row[$header['ranking']] ? $row[$header['ranking']] : null;
                        $player->quarter_ranking = $row[$header['ranking']] ? $row[$header['ranking']] : null;
                        $player->teoretical_ranking = $row[$header['teoretical_ranking']] ? $row[$header['teoretical_ranking']] : null;
                        $player->regional_ranking = $row[$header['regional_ranking']] ? $row[$header['regional_ranking']] : null;
                        $player->province_ranking = $row[$header['province_ranking']] ? $row[$header['province_ranking']] : null;
                        $player->club_ranking = $row[$header['club_ranking']] ? $row[$header['club_ranking']] : null;
                        $player->additional_points = $row[$header['additional_points']] ? $row[$header['additional_points']] : null;
                        $player->category = $row[$header['category']] ? $row[$header['category']] : null;

                        $player->save();
                    }

                }
            }
            fclose($handle);
        }
        return $data;
    }

}
