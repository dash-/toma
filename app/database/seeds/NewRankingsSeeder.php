<?php

class NewRankingsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);

        $players = Player::orderBy('ranking_points', 'desc')
			->get();

		foreach ($players as $key => $player) 
		{
			$player->ranking = $key + 1;
			$player->save();
		}
	}

}
