<?php

class NewNotificationTypeSeeder extends Seeder {

	public function run()
	{
		$data = array(
			0 => array(
				'id' => 10,
				'name' => 'Request for result change',
			),
		);

		DB::table('notification_types')->insert($data);
	}

}