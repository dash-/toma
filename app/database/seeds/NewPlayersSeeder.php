<?php

class NewPlayersSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


        $csvFile = public_path() . '/csv/licences_17122014.csv';
        $this->players_csv_to_array($csvFile);

    }
    private function players_csv_to_array($filename = '', $delimiter = ';')
    {
        DB::table('temporary_rankings')->truncate();
        DB::table('draw_matches')->truncate();
        DB::table('draw_seeds')->truncate();
        DB::table('match_schedules')->truncate();
        DB::table('match_scores')->truncate();
        DB::table('tournament_signups')->truncate();
        DB::table('tournament_teams')->truncate();
        DB::table('players')->truncate();

        //TODO: maybe delete
        DB::table('contacts')->truncate();
        DB::table('history_licences')->truncate();

        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        $header = FALSE;

$i = 0;
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 2024, $delimiter)) && $row[0]) {

                if (!$header) {
                    $header = array_flip($row);
                } else {
$i++;
                    $ape_id = isset($row[$header['ape_id']]) && $row[$header['ape_id']] != " " && $row[$header['ape_id']] != "" ? $row[$header['ape_id']] : '';

                    $clean_date = DateTimeHelper::cleanDate($row[$header['date_of_birth']]);
                    $dob = DateTimeHelper::GetPostgresDateTime($clean_date);

                    $contact = DB::table('contacts')->insertGetId([
                        'name' => $row[$header['name']],
                        'surname' => $row[$header['surname']],
                        'phone' => $row[$header['phone']],
                        'email' => $row[$header['email']],
                        'web' => "",
                        'date_of_birth' => $clean_date,
                        'dob' => $dob,
                        'address' => $row[$header['address']],
                        'city' => $row[$header['city']],
                        'postal_code' => $row[$header['postal_code']],
                        'sex' => $row[$header['sex']],
                        'card_id' => $row[$header['dni']],
                        'mail_subscription' => ($row[$header['mailing']] == 'N' || $row[$header['mailing']] == '') ? FALSE : TRUE,
                    ]);

                    $is_referee = false;
                    $is_coach = false;

                    if ($ape_id != '') {
                        //  AR(*) = Referee & Coach (Level *)
                        //  AP* = Coach
                        //  ARB = Only Referee

                        $referee_and_couch = str_contains($ape_id, "AR");
                        $only_couch = str_contains($ape_id, "AP");
                        $coach_level = null;
                        //ONLY REFEREE
                        if ($ape_id == "ARB")
                            $is_referee = true;
                        elseif ($referee_and_couch) {
                            $is_referee = true;
                            $is_coach = true;
                            $coach_level = substr($ape_id, 2, 1);
                        } elseif ($only_couch) {
                            $is_coach = true;
                            $coach_level = substr($ape_id, 2, 1);
                        }
                    }
var_dump($i);var_dump($row[$header['licence_number']]);

                    DB::table('players')->insert([
                        'contact_id' => $contact,
                        'licence_number' => $row[$header['licence_number']],
                        // 'ranking']        => $row[$header['ranking']],
                        'ape_id' => $row[$header['ape_id']],
                        'nationality' => $row[$header['nationality']] && $row[$header['nationality']]!= " " ? $row[$header['nationality']] : null,
                        'current_situation' => $row[$header['current_situation']],
                        'federation_club_id' => $row[$header['federation_club_id']],
                        'province_club_id' => $row[$header['province_club_id']],
                        'club_id' => $row[$header['club_id']],
                        'mutua' => $row[$header['mutua']],
                        'status' => 2,
                        'is_referee' => $is_referee,
                        'is_coach' => $is_coach,
                        'coach_level' => $coach_level ? $coach_level : null,
                    ]);


                    $licence_history = array(array(
                        'contact_id' => $contact,
                        'year' => 2012,
                        'federation_id' => $row[$header['2012_federation_club_id']] && $row[$header['2012_federation_club_id']] != " " ? $row[$header['2012_federation_club_id']] : 0,
                        'province_id' => $row[$header['2012_province_club_id']] && $row[$header['2012_province_club_id']] != " " ? $row[$header['2012_province_club_id']] : 0,
                        'club_id' => $row[$header['2012_club_id']] && $row[$header['2012_club_id']] != " " ? $row[$header['2012_club_id']] : 0,
                        'mutua' => $row[$header['2012_mutua']] ? $row[$header['2012_mutua']] : "",
                    ), array(
                        'contact_id' => $contact,
                        'year' => 2013,
                        'federation_id' => $row[$header['2013_federation_club_id']] && $row[$header['2013_federation_club_id']] != " " ? $row[$header['2013_federation_club_id']] : 0,
                        'province_id' => $row[$header['2013_province_club_id']] && $row[$header['2013_province_club_id']] != " " ? $row[$header['2013_province_club_id']] : 0,
                        'club_id' => $row[$header['2013_club_id']] && $row[$header['2013_club_id']] != " " ? $row[$header['2013_club_id']] : 0,
                        'mutua' => $row[$header['2013_mutua']] ? $row[$header['2013_mutua']] : "",
                    ));
                    DB::table('history_licences')->insert($licence_history);


                }
            }
            fclose($handle);
        }
        return $data;
    }
}
