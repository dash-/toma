<?php

class NotificationTypesTableSeederExtend2 extends Seeder {

	public function run()
	{
		$data = [
			[
				'id' => 3,
				'name' => 'Tournament request approved',
			],
			[
				'id' => 4,
				'name' => 'Tournament request rejected',
			],
		];

		DB::table('notification_types')->insert($data);
	}

}