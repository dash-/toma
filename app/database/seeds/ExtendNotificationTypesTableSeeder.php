<?php

class ExtendNotificationTypesTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			'id' => 8,
			'name' => 'Player signed up without licence',
		);

		DB::table('notification_types')->insert($data);
	}

}