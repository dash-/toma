<?php

class DrawCategoriesSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			array(
				'id' => 1,
				'name' => 'Sub-10',
				'range_from' => 0,
				'range_to' => 10,
			),
			array(
				'id' => 2,
				'name' => 'Alevin',
				'range_from' => 10,
				'range_to' => 12,
			),
			array(
				'id' => 3,
				'name' => 'Infantil',
				'range_from' => 12,
				'range_to' => 14,
			),
			array(
				'id' => 4,
				'name' => 'Cadete',
				'range_from' => 15,
				'range_to' => 16,
			),
			array(
				'id' => 5,
				'name' => 'Junior',
				'range_from' => 16,
				'range_to' => 18,
			),
			array(
				'id' => 6,
				'name' => 'Absoluto',
				'range_from' => 18,
				'range_to' => 35,
			),
			array(
				'id' => 7,
				'name' => '+35',
				'range_from' => 35,
				'range_to' => 40,
			),
			array(
				'id' => 8,
				'name' => '+40',
				'range_from' => 40,
				'range_to' => 45,
			),
			array(
				'id' => 9,
				'name' => '+45',
				'range_from' => 45,
				'range_to' => 50,
			),
			array(
				'id' => 10,
				'name' => '+50',
				'range_from' => 50,
				'range_to' => 55,
			),
			array(
				'id' => 11,
				'name' => '+55',
				'range_from' => 55,
				'range_to' => 60,
			),
			array(
				'id' => 12,
				'name' => '+60',
				'range_from' => 60,
				'range_to' => 65,
			),
			array(
				'id' => 13,
				'name' => '+65',
				'range_from' => 65,
				'range_to' => 70,
			),
			array(
				'id' => 14,
				'name' => '+70',
				'range_from' => 70,
				'range_to' => 75,
			),
			array(
				'id' => 15,
				'name' => '+75',
				'range_from' => 75,
				'range_to' => 80,
			),
			array(
				'id' => 16,
				'name' => '+80',
				'range_from' => 80,
				'range_to' => 85,
			),
			array(
				'id' => 17,
				'name' => '+85',
				'range_from' => 85,
				'range_to' => 100,
			),
		);

		
		
		foreach ($data as $d) 
		{
			DB::table('draw_categories')->insert($d);
		}
	}

}
