<?php

// Composer: "fzaninotto/faker": "v1.3.0"

class TempRankingsSeeder extends Seeder
{

    public function run()
    {
        DB::disableQueryLog();
        Player::chunk(2000, function($players)
        {
            foreach ($players as $player) 
            {
                TempRanking::create([
                    'player_id'      => $player->id,
                    'ranking'        => $player->ranking,
                    'ranking_points' => $player->ranking_points,
                ]);
            }
        });
    }


}