<?php

class NotificationTypesTableSeederExtend extends Seeder {

	public function run()
	{
		$data = array(
			'id' => 2,
			'name' => 'Request for tournament approval',
		);

		DB::table('notification_types')->insert($data);
	}

}