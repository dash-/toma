<?php

class DummyRankingsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);

        $numbers = range(1, Player::count());
		shuffle($numbers);

		foreach ($numbers as $key => $number) {
			DB::table('players')->where('id',$key)->update(array('ranking'=>$number));
		}


		// DB::table('players')->insert($player);
	}

}
