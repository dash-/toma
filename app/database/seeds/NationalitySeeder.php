<?php

class NationalitySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);


		$csvFile = public_path().'/csv/nationalities.csv';
		$this->read_csv($csvFile);

	}

	private function read_csv($filename='', $delimiter=';')
	{
		if(!file_exists($filename) || !is_readable($filename))
			return FALSE;

		$data = array();
		$header = FALSE;

		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
			while (($row = fgetcsv($handle, 1024, $delimiter)) && $row[0]) {

				if(!$header) {
					$header = array_flip($row);
				}
				else
				{
					$nat = new Nationality();
					$nat->country_id = $row[$header['id']];
					$nat->country = $row[$header['country']];
					$nat->save();
				}
			}
			fclose($handle);
		}
		return $data;
	}

}
