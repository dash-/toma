<?php

class RefereesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);

        $csvFile = public_path() . '/csv/referees_17122014.csv';
        $this->players_csv_to_array($csvFile);

    }

    function players_csv_to_array($filename = '', $delimiter = ';')
    {
//        DB::table('temporary_rankings')->truncate();
//        DB::table('draw_matches')->truncate();
//        DB::table('draw_seeds')->truncate();
//        DB::table('match_schedules')->truncate();
//        DB::table('match_scores')->truncate();
//        DB::table('tournament_signups')->truncate();
//        DB::table('tournament_teams')->truncate();
        DB::table('referees')->truncate();
        DB::table('referee_registrations')->truncate();
//
//        //TODO: maybe delete
//        DB::table('contacts')->truncate();
//        DB::table('history_licences')->truncate();

        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $data = array();
        $header = FALSE;

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 2024, $delimiter)) && $row[0]) {

                if (!$header) {
                    $header = array_flip($row);
                } else {
                    $licence_full = explode("-", $row[$header['licence']]);
                    $licence_number_str = $licence_full[0];
                    $level = $licence_full[1];

                    $licence_number = intval($licence_number_str);
                    $player_id = null;
                    var_dump($licence_number);
                    $player = '';
                    $contact_id = '';
                    if ($licence_number != '')
                        $player = Player::where("licence_number", $licence_number)->first();

                    if ($player) {
                        $player_id = $player->id;
                        $contact_id = $player->contact_id;;

                    }

                    $active = 0;
                    if (strtolower($row[$header['active']]) == "s" && strtolower($row[$header['active']]) == 'a')
                        $active = 1;
                    elseif (strtolower($row[$header['active']]) == "d")
                        $active = -1;
                    elseif (strtolower($row[$header['active']]) == "out")
                        $active = -2;
                    elseif (strtolower($row[$header['active']]) == "rip")
                        $active = -3;


                    if ($contact_id == '') {

                        $clean_date = DateTimeHelper::cleanDate($row[$header['date_of_birth']]);
                        $dob = DateTimeHelper::GetPostgresDateTime($clean_date);
                        $contact_id = DB::table('contacts')->insertGetId([
                            'name' => $row[$header['name']],
                            'surname' => $row[$header['surname']],
                            'phone' => $row[$header['phone']],
                            'email' => $row[$header['email']],
                            'web' => "",
                            'date_of_birth' => $clean_date,
                            'dob' => $dob,
                            'address' => $row[$header['address']],
                            'city' => $row[$header['city']],
                            'postal_code' => $row[$header['postal_code']],
                            //'sex' => $row[$header['sex']],
                            'card_id' => $row[$header['DNI']],
                            'mail_subscription' => TRUE,//($row[$header['mailing']] == 'N' || $row[$header['mailing']] == '') ? FALSE : TRUE,
                        ]);
                    }


                    $referee = new Referee();
                    $referee->player_id = $player_id;
                    $referee->contact_id = $contact_id;
                    $referee->level = $level;
                    $referee->school_number = $row[$header['school_number']];
                    $referee->licence_number = $licence_number_str;
                    $referee->licence_category = $row[$header['licence_category']];
                    $referee->active = $active;
                    $referee->save();


                    $date_time = DateTimeHelper::GetPostgresDateTime();
                    $referee_registration = array();
                    if ($row[$header['HP3']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2003,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP4']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2004,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP5']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2005,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP6']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2006,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP7']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2007,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP8']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2008,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP9']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2009,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP10']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2010,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP11']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2011,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP12']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2012,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }
                    if ($row[$header['HP13']] != "") {
                        $referee_registration[] = array(
                            'referee_id' => $referee->id,
                            'year' => 2013,
                            'status' => 1,
                            'created_at' => $date_time,
                            'updated_at' => $date_time
                        );
                    }

                    if ($referee_registration)
                        DB::table("referee_registrations")->insert($referee_registration);
                }


            }

            fclose($handle);

        }
        return $data;
    }
}
