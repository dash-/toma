<?php

class NotificationTypesTableSeeder extends Seeder {

	public function run()
	{
		$data = array(
			0 => array(
				'id' => 1,
				'name' => 'Message notification',
			),
			1 => array(
				'id' => 2,
				'name' => 'Request for tournament approval',
			),
			2 => array(
				'id' => 3,
				'name' => 'Tournament request approved',
			),
			3 => array(
				'id' => 4,
				'name' => 'Tournament request rejected',
			),
			4 => array(
				'id' => 5,
				'name' => 'Request for player approval',
			),
			5 => array(
				'id' => 6,
				'name' => 'Player request approved',
			),
			6 => array(
				'id' => 7,
				'name' => 'Player request rejected',
			),
			7 => array(
				'id' => 8,
				'name' => 'Player signed up without licence',
			),
			8 => array(
				'id'   => 9,
				'name' => 'Draw referee notification',
			),
		);

		DB::table('notification_types')->insert($data);
	}

}