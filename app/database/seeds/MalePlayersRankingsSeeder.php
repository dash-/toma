<?php

class MalePlayersRankingsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);

		function male_rankings_to_array($filename='', $delimiter=';')
		{
			if(!file_exists($filename) || !is_readable($filename)){
				echo 'palo';
		        return FALSE;
		    }
		 
		    $data = array();
		    $header = FALSE;

		    if (($handle = fopen($filename, 'r')) !== FALSE)
		    {
		    	// $counter = 0;
		    	while (($row = fgetcsv($handle, 1024, $delimiter))) {

			        if(!$header) {
			        	$header = array_flip($row);
			        }
			        else
			        {
			    //     	$player = Player::whereLicenceNumber($row[$header['licence_number']])->update([
							// 'ranking_points'   => $row[$header['ranking_points']],
							// 'ranking'          => $row[$header['ranking']],
							// 'regional_ranking' => $row[$header['regional_ranking']],
							// 'province_ranking' => $row[$header['province_ranking']],
							// 'club_ranking'     => $row[$header['club_ranking']],
 			   //      	]);
				        $player = Player::whereLicenceNumber($row[$header['licence_number']])->first();
				        if ($player) {
				        	$player->update([
				        		'ranking_points'   => $row[$header['ranking_points']],
								'ranking'          => $row[$header['ranking']],
								'regional_ranking' => $row[$header['regional_ranking']],
								'province_ranking' => $row[$header['province_ranking']],
								'club_ranking'     => $row[$header['club_ranking']],
				        	]);
							// $player->ranking_points   = $row[$header['ranking_points']];
							// $player->ranking          = $row[$header['ranking']];
							// $player->regional_ranking = $row[$header['regional_ranking']];
							// $player->province_ranking = $row[$header['province_ranking']];
							// $player->club_ranking     = $row[$header['club_ranking']];

							// $player->save();
				        }

			        }

			        // if ($counter % 1000 == 0)
			        // 	echo $counter."\n";

			        // $counter++;
			    }
		        fclose($handle);
		    }
		    return $data;
		}

		$csvFile = public_path().'/csv/male_rankings.csv';
		male_rankings_to_array($csvFile);

	}

}
