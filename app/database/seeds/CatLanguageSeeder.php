<?php

class CatLanguageSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data =[ 
			0 => [
				'name'     => 'Catalan',
				'code'     => 'cat',
				'encoding' => 'UTF-8',
			],
		];

		DB::table('languages')->insert($data);
	}

}
