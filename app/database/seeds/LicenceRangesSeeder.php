<?php

class LicenceRangesSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = array(
            0  => array(
                'id'         => 1,
                'region_id'  => 1,
                'range_from' => 9250000,
                'range_to'   => 9749999,
            ),
            1  => array(
                'id'         => 2,
                'region_id'  => 2,
                'range_from' => 16000000,
                'range_to'   => 16199999,
            ),
            2  => array(
                'id'         => 3,
                'region_id'  => 3,
                'range_from' => 8750000,
                'range_to'   => 8999999,
            ),
            3  => array(
                'id'         => 4,
                'region_id'  => 4,
                'range_from' => 5750000,
                'range_to'   => 5999999,
            ),
            4  => array(
                'id'         => 5,
                'region_id'  => 5,
                'range_from' => 14500000,
                'range_to'   => 14999999,
            ),
            5  => array(
                'id'         => 6,
                'region_id'  => 6,
                'range_from' => 9000000,
                'range_to'   => 9249999,
            ),
            6  => array(
                'id'         => 7,
                'region_id'  => 7,
                'range_from' => 11000000,
                'range_to'   => 11999999,
            ),
            7  => array(
                'id'         => 8,
                'region_id'  => 8,
                'range_from' => 10000000,
                'range_to'   => 10999999,
            ),
            8  => array(
                'id'         => 9,
                'region_id'  => 9,
                'range_from' => 7500000,
                'range_to'   => 7749999,
            ),
            9  => array(
                'id'         => 10,
                'region_id'  => 10,
                'range_from' => 13000000,
                'range_to'   => 13999999,
            ),
            10  => array(
                'id'         => 11,
                'region_id'  => 11,
                'range_from' => 12500000,
                'range_to'   => 12999999,
            ),
            11  => array(
                'id'         => 12,
                'region_id'  => 12,
                'range_from' => 15500000,
                'range_to'   => 15999999,
            ),
            12  => array(
                'id'         => 13,
                'region_id'  => 15,
                'range_from' => 12000000,
                'range_to'   => 12499999,
            ),
            13  => array(
                'id'         => 14,
                'region_id'  => 16,
                'range_from' => 14000000,
                'range_to'   => 14499999,
            ),
            14  => array(
                'id'         => 15,
                'region_id'  => 17,
                'range_from' => 15000000,
                'range_to'   => 15499999,
            ),
            15  => array(
                'id'         => 16,
                'region_id'  => 19,
                'range_from' => 5500000,
                'range_to'   => 5749999,
            ),
        );

        foreach ($data as $key => $d) {
            DB::table('licence_ranges')
                ->insert($d);
        }
    }

}
