<?php

class RolesTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			0 => array(
				'id' => 1,
				'name' => 'superadmin',
				'description' => 'Can do anything',
			),
			1 => array(
				'id' => 2,
				'name' => 'national_admin',
				'description' => 'National administrator',
			),
			2 => array(
				'id' => 3,
				'name' => 'regional_admin',
				'description' => 'Regional administrator',
			),
			3 => array(
				'id' => 4,
				'name' => 'standard_user',
				'description' => 'Standard user',
			),
			4 => array(
				'id' => 5,
				'name' => 'player',
				'description' => 'Standard player role',
			),
			5 => array(
				'id' => 6,
				'name' => 'referee',
				'description' => 'Standard referee role',
			),
		);

		DB::table('roles')->insert($data);
	}

}
