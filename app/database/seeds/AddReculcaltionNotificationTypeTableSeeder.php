<?php


class AddReculcaltionNotificationTypeTableSeeder extends Seeder {

	public function run()
	{
        $notification_type = new NotificationType();
        $notification_type->id = 12;
        $notification_type->name = "Weekly recalculation is completed";
        $notification_type->save();

        $notification_type2 = new NotificationType();
        $notification_type2->id = 13;
        $notification_type2->name = "Quarter recalculation is completed";
        $notification_type2->save();
	}

}