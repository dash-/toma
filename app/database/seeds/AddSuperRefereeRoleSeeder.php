<?php

class AddSuperRefereeRoleSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = array(
            0 => array(
                'id'          => 7,
                'name'        => 'super_referee',
                'description' => 'Admin referee role, this is same as standard referee but can see referees tab and administer them',
            )
        );

        DB::table('roles')->insert($data);
    }

}
