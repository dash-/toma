<?php


class InsertCategoryRangeSignupTableSeeder extends Seeder {

	public function run()
	{
        $categories = DrawCategory::get();
        foreach($categories as $category){
            if($category->range_for_signup_from == 0 && $category->range_for_signup_to == 100)
            {
                $category->range_for_signup_from = $category->range_from;
                $category->range_for_signup_to = $category->range_to;
                $category->save();
            }
        }
	}

}