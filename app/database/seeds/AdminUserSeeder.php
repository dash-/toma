<?php

class AdminUserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run()
	{
		$data = array(
			'username'   => 'admin',
			'email'      => 'info@dash.ba',
			'password'   => Hash::make('admin'),
			'secret'     => Str::random(16),
			'active'     => 1,
			'verified'   => 1,
			'created_at' => new DateTime,
		);

		$role_data = array(
			'user_id' => 1,
			'role_id' => 1,
		);

		DB::table('users')->insert($data);
		DB::table('users_roles')->insert($role_data);

        $contact = new Contact;
        $contact->name = 'Super';
        $contact->surname = 'User';
        $contact->save();

        $admin = new Admin;
        $admin->contact_id = $contact->id;
        $admin->user_id = 1;
        $admin->save();
	}

}
