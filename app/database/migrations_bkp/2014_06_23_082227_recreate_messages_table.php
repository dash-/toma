<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RecreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('messages');

		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conversation_id');
			$table->integer('sender_id');
			$table->integer('type_id')->nullable();
			$table->string('subject')->nullable();
			$table->text('text')->nullable();
			$table->dateTime('time_sent');
		});

		Schema::create('messages_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('message_id');
			$table->boolean('seen')->default(false);
			$table->dateTime('time_seen');
			$table->dateTime('deleted_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
		Schema::drop('messages_users');
	}

}
