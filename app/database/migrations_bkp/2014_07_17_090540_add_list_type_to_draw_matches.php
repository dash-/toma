<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddListTypeToDrawMatches extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->integer('list_type');
			$table->renameColumn('score_id', 'round_position');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->dropColumn('list_type');
			$table->renameColumn('position', 'score_id');
		});
	}

}
