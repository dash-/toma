<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRangeToDrawCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_categories', function(Blueprint $table)
		{
			$table->integer('range_from')->nullable();
			$table->integer('range_to')->nullable();
		});

		$data = array(
			array(
				'id' => 1,
				'range_from' => 0,
				'range_to' => 10,
			),
			array(
				'id' => 2,
				'range_from' => 10,
				'range_to' => 12,
			),
			array(
				'id' => 3,
				'range_from' => 12,
				'range_to' => 14,
			),
			array(
				'id' => 4,
				'range_from' => 15,
				'range_to' => 16,
			),
			array(
				'id' => 5,
				'range_from' => 16,
				'range_to' => 18,
			),
			array(
				'id' => 6,
				'range_from' => 18,
				'range_to' => 35,
			),
			array(
				'id' => 7,
				'range_from' => 35,
				'range_to' => 40,
			),
			array(
				'id' => 8,
				'range_from' => 40,
				'range_to' => 45,
			),
			array(
				'id' => 9,
				'range_from' => 45,
				'range_to' => 50,
			),
			array(
				'id' => 10,
				'range_from' => 50,
				'range_to' => 55,
			),
			array(
				'id' => 11,
				'range_from' => 55,
				'range_to' => 60,
			),
			array(
				'id' => 12,
				'range_from' => 60,
				'range_to' => 65,
			),
			array(
				'id' => 13,
				'range_from' => 65,
				'range_to' => 70,
			),
			array(
				'id' => 14,
				'range_from' => 70,
				'range_to' => 75,
			),
			array(
				'id' => 15,
				'range_from' => 75,
				'range_to' => 80,
			),
			array(
				'id' => 16,
				'range_from' => 80,
				'range_to' => 85,
			),
			array(
				'id' => 17,
				'range_from' => 85,
				'range_to' => 100,
			),
		);
		
		foreach ($data as $d) {
			DB::table('draw_categories')->where('id', $d['id'])->update($d);
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_categories', function(Blueprint $table)
		{
			$table->dropColumn('range_from');
			$table->dropColumn('range_to');
		});
	}

}
