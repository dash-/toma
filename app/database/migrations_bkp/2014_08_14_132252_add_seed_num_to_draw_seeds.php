<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSeedNumToDrawSeeds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_seeds', function(Blueprint $table)
		{
			$table->integer('seed_num')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_seeds', function(Blueprint $table)
		{
			$table->dropColumn('seed_num');
		});
	}

}
