<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddApprovedToPlayers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('players', function(Blueprint $table)
		{
			$table->integer('status')->nullable();
			$table->integer('approved_by')->nullable();
			$table->dateTime('date_of_approval')->nullable();
		});

		DB::table('players')->update(['status' => 2]);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('players', function(Blueprint $table)
		{
			$table->dropColumn('status');
			$table->dropColumn('approved_by');
			$table->dropColumn('date_of_approval');
		});
	}

}
