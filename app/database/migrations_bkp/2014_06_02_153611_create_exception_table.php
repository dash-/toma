<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExceptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('exceptions', function($table)
        {
            $table->increments('id');
            $table->string("statusCode", 10)->nullable();
            $table->text("message")->nullable();
            $table->string("url", 100)->nullable();
            $table->string("browser", 100)->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('exceptions');
	}

}
