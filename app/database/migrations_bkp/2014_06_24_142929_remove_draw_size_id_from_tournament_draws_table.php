<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveDrawSizeIdFromTournamentDrawsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->dropColumn('draw_size_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->integer('draw_size_id')->nullable();
		});
	}

}
