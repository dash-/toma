<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_players',function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('player_id');
			$table->dateTime('date_added');
			$table->integer('added_by');			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tournament_players");
	}

}
