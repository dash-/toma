<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddStatusToTournamentSignups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->integer('status')->nullable();
			$table->integer('list_type')->nullable();
			$table->dateTime('withdrawal_date')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->dropColumn('status');
			$table->dropColumn('list_type');
			$table->dropColumn('withdrawal_date');
		});
	}

}
