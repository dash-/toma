<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrawSizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_sizes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('total')->nullable();
			$table->integer('direct_from')->nullable();
			$table->integer('direct_to')->nullable();
			$table->integer('qualifyers_from')->nullable();
			$table->integer('qualifyers_to')->nullable();
			$table->integer('wild_cards')->nullable();
			$table->integer('special_exempts')->default(0);
			$table->integer('onsite_direct')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draw_sizes');
	}

}
