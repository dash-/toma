<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table("regions", function($table)
        {
            $table->string("full_name")->nullable();
            $table->string("president")->nullable();
            $table->string("address")->nullable();
            $table->string("phone")->nullable();
            $table->string("fax")->nullable();
            $table->string("email")->nullable();
            $table->string("web")->nullable();
            $table->string("working_hours")->nullable();
            $table->string("image_path")->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table("regions", function($table)
        {
            $table->dropColumn("full_name")->nullable();
            $table->dropColumn("president")->nullable();
            $table->dropColumn("address")->nullable();
            $table->dropColumn("phone")->nullable();
            $table->dropColumn("fax")->nullable();
            $table->dropColumn("email")->nullable();
            $table->dropColumn("web")->nullable();
            $table->dropColumn("working_hours")->nullable();
            $table->dropColumn("image_path")->nullable();
        });
	}

}
