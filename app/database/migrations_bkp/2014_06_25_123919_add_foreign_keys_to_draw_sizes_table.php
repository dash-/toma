<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDrawSizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->foreign('draw_id')
		      ->references('id')->on('tournament_draws');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->dropForeign('draw_sizes_draw_id_foreign');
		});
	}

}
