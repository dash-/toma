<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrawMatchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_matches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team1_id')->nullable();
			$table->integer('team2_id')->nullable();
			$table->integer('draw_id');
			$table->integer('score_id')->nullable();
			$table->integer('round_number');
			$table->integer('team1_score')->nullable();
			$table->integer('team2_score')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draw_matches');
	}

}
