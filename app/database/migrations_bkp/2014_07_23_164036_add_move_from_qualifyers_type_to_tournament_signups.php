<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMoveFromQualifyersTypeToTournamentSignups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->integer('move_from_qualifiers_type')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->dropColumn('move_from_qualifiers_type');
		});
	}

}
