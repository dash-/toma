<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSignupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('signup_seeds');
		Schema::drop('tournament_signups');

		Schema::create('tournament_signups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('tournament_id');
			$table->integer('team_id')->nullable();
			$table->integer('submited_by');
			$table->integer('rank');
			$table->integer('status')->nullable();
			$table->integer('list_type')->nullable();
			$table->dateTime('withdrawal_date')->nullable();
			$table->dateTime('created_at');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_signups');
	}

}
