<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->string('title')->nullable();
			$table->text('content')->nullable();
			$table->boolean('active')->default(false);
			$table->integer('created_by');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_articles');
	}

}
