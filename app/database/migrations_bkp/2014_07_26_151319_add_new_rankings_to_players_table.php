<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddNewRankingsToPlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('players', function(Blueprint $table)
		{
			$table->integer('regional_ranking')->nullable();
			$table->integer('province_ranking')->nullable();
			$table->integer('club_ranking')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('players', function(Blueprint $table)
		{
			$table->dropColumn('regional_ranking');
			$table->dropColumn('province_ranking');
			$table->dropColumn('club_ranking');
		});
	}

}
