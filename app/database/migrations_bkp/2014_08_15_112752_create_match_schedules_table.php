<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_schedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id');
			$table->integer('draw_id');
			$table->integer('court_number');
			$table->string('time_of_play');
			$table->dateTime('date_of_play');
			$table->boolean('exact_time')->default(false);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_schedules');
	}

}
