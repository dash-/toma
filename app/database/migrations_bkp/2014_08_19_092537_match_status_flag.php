<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MatchStatusFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('match_schedules', function(Blueprint $table)
        {
            $table->integer('match_status')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('match_schedules', function(Blueprint $table)
        {
            $table->dropColumn('match_status');
        });
	}

}
