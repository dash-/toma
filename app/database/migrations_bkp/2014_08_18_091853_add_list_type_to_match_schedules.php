<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddListTypeToMatchSchedules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->integer('list_type')->default(2);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->dropColumn('list_type');
		});
	}

}
