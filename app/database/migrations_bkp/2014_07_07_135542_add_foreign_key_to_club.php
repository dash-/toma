<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToClub extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('clubs', function(Blueprint $table)
        {
            $table->foreign('province_id')
                ->references('id')->on('provinces');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('clubs', function(Blueprint $table) {
            $table->dropForeign('clubs_province_id_foreign');
        });
	}

}
