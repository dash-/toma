<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddDrawsRefereesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draws_referees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('user_id');
			$table->boolean('main_referee')->default(false);
			$table->integer('status')->default(0);
			$table->dateTime('date_of_application');
			$table->dateTime('date_of_status_change');
			$table->integer('approved_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draws_referees');
	}

}
