<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conversation_id')->nullable();
			$table->integer('sender_id');
			$table->integer('receiver_id');
			$table->integer('type_id')->nullable();
			$table->string('subject')->nullable();
			$table->text('text')->nullable();
			$table->dateTime('time_sent')->nullable();
			$table->dateTime('time_seen')->nullable();
			$table->boolean('seen');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
