<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToMessagesSeen extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('messages', function(Blueprint $table)
		{
			$table->dropColumn('seen');
		});
		Schema::table('messages', function(Blueprint $table)
		{
			$table->boolean('seen')->default(FALSE);
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('messages', function(Blueprint $table)
		{
			$table->dropColumn('seen');
		});
		Schema::table('messages', function(Blueprint $table)
		{
			$table->boolean('seen');		
		});
	}

}
