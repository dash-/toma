<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DeleteTournamentsRefereesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::drop('tournaments_referees');
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('tournaments_referees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');		
			$table->integer('referee_id');
		});
	}

}
