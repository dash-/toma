<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('web')->nullable();
			$table->string('date_of_birth')->nullable();
			$table->string('address')->nullable();
			$table->string('city')->nullable();
			$table->string('postal_code', 32)->nullable();
			$table->string('sex', 6)->nullable();
			$table->string('card_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
