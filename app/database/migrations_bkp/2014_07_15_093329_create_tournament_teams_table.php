<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_teams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team_id');
			$table->string('licence_number')->nullable();
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->dateTime('date_of_birth')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->integer('current_ranking')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_teams');
	}

}
