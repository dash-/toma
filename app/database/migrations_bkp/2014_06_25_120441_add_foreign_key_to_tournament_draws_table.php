<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeyToTournamentDrawsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->foreign('tournament_id')
		      ->references('id')->on('tournaments');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->dropForeign('tournament_draws_tournament_id_foreign');
		});
	}

}
