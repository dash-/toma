<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveAllForeignKeys2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clubs', function(Blueprint $table) {
            $table->dropForeign('clubs_province_id_foreign');
        });

		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->dropForeign('tournaments_club_id_foreign');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
