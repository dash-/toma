<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrimaryKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_roles', function($table) {
		    $table->increments('id');
		});

		Schema::table('users_federations', function($table) {
		    $table->increments('id');
		});

		Schema::table('users_regions', function($table) {
		    $table->increments('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_roles', function($table) {
			$table->dropColumn('id');
		});

		Schema::table('users_federations', function($table) {
			$table->dropColumn('id');
		});
		
		Schema::table('users_regions', function($table) {
			$table->dropColumn('id');
		});
	}

}
