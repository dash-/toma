<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTournamentIdToTournamentDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_dates', function($table) {
		    $table->integer('tournament_id');
		});

		Schema::table('tournament_referees', function($table) {
		    $table->integer('tournament_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_dates', function($table) {
			$table->dropColumn('tournament_id');
		});

		Schema::table('tournament_referees', function($table) {
			$table->dropColumn('tournament_id');
		});
	}

}
