<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToProvinces extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('provinces', function(Blueprint $table)
        {
            $table->foreign('region_id')
                ->references('id')->on('regions');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('provinces', function(Blueprint $table) {
            $table->dropForeign('provinces_region_id_foreign');
        });
	}

}
