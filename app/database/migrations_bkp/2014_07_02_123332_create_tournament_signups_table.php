<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentSignupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_signups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_id');
			$table->integer('submited_by');
			$table->string('licence_number')->nullable();
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->dateTime('date_of_birth')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->dateTime('created_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_signups');
	}

}
