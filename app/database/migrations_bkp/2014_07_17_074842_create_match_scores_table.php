<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchScoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_scores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->nullable();
			$table->integer('set_number')->nullable();
			$table->integer('set_team1_score')->nullable();
			$table->integer('set_team2_score')->nullable();
			$table->integer('set_difference')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_scores');
	}

}
