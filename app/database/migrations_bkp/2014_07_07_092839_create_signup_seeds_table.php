<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSignupSeedsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('signup_seeds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('signup_id');
			$table->integer('seed_number');
		});

		Schema::table('signup_seeds', function(Blueprint $table)
		{
			$table->foreign('signup_id')
		      ->references('id')->on('tournament_signups');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('signup_seeds');
	}

}
