<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_notifications', function(Blueprint $table)
		{
			$table->foreign('user_id')
		      ->references('id')->on('users');

		    $table->foreign('notification_id')
		      ->references('id')->on('notifications');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_notifications', function(Blueprint $table)
		{
			$table->dropForeign('users_notifications_user_id_foreign');
			$table->dropForeign('users_notifications_notification_id_foreign');
		});
	}

}
