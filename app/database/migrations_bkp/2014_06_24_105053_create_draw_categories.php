<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrawCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
		});

		$data = array(
			'Sub-10',
			'Alevin',
			'Infantil',
			'Cadete',
			'Junior',
			'Absoluto',
			'+35',
			'+40',
			'+45',
			'+50',
			'+55',
			'+60',
			'+65',
			'+70',
			'+75',
			'+80',
			'+85',
		);
		
		foreach ($data as $d) 
		{
			$item = array('name' => $d);
			DB::table('draw_categories')->insert($item);
		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draw_categories');
	}

}
