<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMatchStatusToDrawMatches extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->integer('match_status')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->dropColumn('match_status');
		});
	}

}
