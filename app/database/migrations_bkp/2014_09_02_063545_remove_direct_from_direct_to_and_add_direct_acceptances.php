<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveDirectFromDirectToAndAddDirectAcceptances extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->renameColumn('direct_from', 'direct_acceptances');
			$table->dropColumn('direct_to');

			$table->renameColumn('qualifiers_to', 'accepted_qualifiers');
			$table->dropColumn('qualifiers_from');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->renameColumn('direct_acceptances', 'direct_from');
			$table->integer('direct_to')->nullable();

			$table->renameColumn('accepted_qualifiers', 'qualifiers_to');
			$table->integer('qualifiers_from')->nullable();
		});
	}

}
