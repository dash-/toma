<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultCourts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->dropColumn('number_of_courts');
        });
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->integer('number_of_courts')->default(4);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
