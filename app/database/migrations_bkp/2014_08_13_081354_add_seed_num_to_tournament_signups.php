<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSeedNumToTournamentSignups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
//		Schema::table('tournament_teams', function(Blueprint $table)
//		{
//			$table->dropColumn('seed_num');
//		});

		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->integer('seed_num')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->dropColumn('seed_num');
		});
	}

}
