<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentDrawsTableNew extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_draws', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_category_id');
			$table->string('draw_type', 16);
			$table->string('draw_gender', 12);
			$table->integer('draw_size_id')->nullable();
			$table->string('draw_strength', 32)->nullable();
			$table->integer('number_of_seeds')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_draws');
	}

}
