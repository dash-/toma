<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentDatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_dates',function (Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('main_draw_from');
			$table->dateTime('main_draw_to');
			$table->dateTime('qualifing_date_from');
			$table->dateTime('qualifing_date_to');
			$table->dateTime('entry_deadline');
			$table->dateTime('withdrawal_deadline');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tournament_dates");
	}

}
