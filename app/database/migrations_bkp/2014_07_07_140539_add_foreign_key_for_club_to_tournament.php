<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyForClubToTournament extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('tournaments', function(Blueprint $table)
        {

            // $table->dropForeign('tournaments_region_id_foreign');
            // $table->dropColumn('region_id');

            $table->foreign('club_id')
                ->references('id')->on('clubs');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->integer('region_id')->nullable();

            $table->foreign('region_id')
                ->references('id')->on('regions');
        });
	}

}
