<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSlugToTournaments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});

		foreach (Tournament::all() as $tournament) {
			
			$tournament->slug = Str::slug($tournament->title).'-'.$tournament->id;
			$tournament->save();

		}
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});
	}

}
