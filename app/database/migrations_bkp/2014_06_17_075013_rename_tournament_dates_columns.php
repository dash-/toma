<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTournamentDatesColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_dates', function(Blueprint $table)
		{
			$table->renameColumn('qualifing_date_from', 'qualifying_date_from');
			$table->renameColumn('qualifing_date_to', 'qualifying_date_to');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_dates', function(Blueprint $table)
		{
			$table->renameColumn('qualifying_date_from', 'qualifing_date_from');
			$table->renameColumn('qualifying_date_to', 'qualifing_date_to');
		});
	}

}
