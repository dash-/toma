<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMessagesUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('messages_users', function(Blueprint $table)
		{
			$table->foreign('user_id')
		      ->references('id')->on('users');

		    $table->foreign('message_id')
		      ->references('id')->on('messages');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('messages_users', function(Blueprint $table)
		{
			$table->dropForeign('messages_users_user_id_foreign');
			$table->dropForeign('messages_users_message_id_foreign');
		});
	}

}
