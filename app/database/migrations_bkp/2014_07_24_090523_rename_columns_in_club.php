<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsInClub extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
			$table->renameColumn('club_address', 'club_address_temp');
			$table->renameColumn('club_city', 'club_address');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table->renameColumn('club_address', 'club_city');
		$table->renameColumn('club_address_temp', 'club_address');
	}

}
