<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddManualDrawFlagToTournamentDraws extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->integer('manual_draw')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->dropColumn('manual_draw');
		});
	}

}
