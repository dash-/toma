<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RemoveAllForeignKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->dropForeign('tournament_draws_tournament_id_foreign');
		});

		Schema::table('draw_qualifications', function(Blueprint $table)
		{
			$table->dropForeign('draw_qualifications_draw_id_foreign');
		});

		Schema::table('admins', function(Blueprint $table)
		{
			$table->dropForeign('admins_user_id_foreign');
			$table->dropForeign('admins_contact_id_foreign');
			
		});

		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->dropForeign('draw_sizes_draw_id_foreign');
		});

		Schema::table('login_history', function(Blueprint $table)
		{
			$table->dropForeign('login_history_user_id_foreign');
		});

		Schema::table('messages_users', function(Blueprint $table)
		{
			$table->dropForeign('messages_users_user_id_foreign');
			$table->dropForeign('messages_users_message_id_foreign');
		});

		Schema::table('notifications', function(Blueprint $table)
		{
			$table->dropForeign('notifications_type_id_foreign');
		});

		Schema::table('players', function(Blueprint $table)
		{
			$table->dropForeign('players_user_id_foreign');
			$table->dropForeign('players_contact_id_foreign');
		});

		Schema::table('tournament_dates', function(Blueprint $table)
		{
			$table->dropForeign('tournament_dates_tournament_id_foreign');
		});

		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->dropForeign('tournaments_surface_id_foreign');
			$table->dropForeign('tournaments_organizer_id_foreign');
		});

		Schema::table('users_notifications', function(Blueprint $table)
		{
			$table->dropForeign('users_notifications_user_id_foreign');
			$table->dropForeign('users_notifications_notification_id_foreign');
		});

		Schema::table('users_regions', function(Blueprint $table)
		{
			$table->dropForeign('users_regions_user_id_foreign');
			$table->dropForeign('users_regions_region_id_foreign');
		});

		Schema::table('users_roles', function(Blueprint $table)
		{
			$table->dropForeign('users_roles_user_id_foreign');
			$table->dropForeign('users_roles_role_id_foreign');
		});

		// Schema::table('tournaments', function(Blueprint $table)
		// {
		// 	$table->dropForeign('tournaments_region_id_foreign');
		// });

		Schema::table('signup_seeds', function(Blueprint $table)
		{
			$table->dropForeign('signups_seeds_signup_id_foreign');
		});

		// Schema::table('clubs', function(Blueprint $table) {
  //           $table->dropForeign('clubs_province_id_foreign');
  //       });

        Schema::table('provinces', function(Blueprint $table) {
            $table->dropForeign('provinces_region_id_foreign');
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		
	}

}
