<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPrizepoolAndTournamentLevel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->dropColumn("prize_pool");
            $table->dropColumn("level");
        });
        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->integer('prize_pool')->nullable();
            $table->integer('level')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->dropColumn("prize_pool");
            $table->dropColumn("level");
        });
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->integer('prize_pool')->nullable();
            $table->integer('level')->nullable();
        });
	}

}
