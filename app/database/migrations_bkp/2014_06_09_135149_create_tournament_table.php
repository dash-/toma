<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments',function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('level_id');
			$table->integer('nation_id');
			$table->integer('surface_id');
			$table->integer('event_organizer_id');
			$table->integer('dates')->nullable();
			$table->integer('referees')->nullable();
			$table->string('main_draw_size')->nullable();
			$table->string('doubles_draw_size')->nullable();
			$table->string('qualifing_draw_size')->nullable();
			$table->integer('approved')->nullable();
			$table->integer('approved_by')->nullable();
			$table->dateTime('date_of_approvel')->nullable();
			$table->integer('added_to_calendar')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tournaments");
	}

}
