<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClubsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clubs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("province_id");
            $table->integer("region_id");
			$table->string('name');
			$table->string('city');
			$table->string('address');
			$table->string('short_name');
			$table->dateTime('founded_date')->nullable();
			$table->integer('number_of_tracks')->nullable();
			$table->string('phone')->nullable();
			$table->string('phone2')->nullable();
			$table->string('fax')->nullable();
			$table->string('email')->nullable();
			$table->string('web')->nullable();
			$table->string('president')->nullable();
			$table->string('nif');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clubs');
	}

}
