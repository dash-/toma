<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsInClubRegionProvince extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
			$table->renameColumn('name', 'club_name');
			$table->renameColumn('city', 'club_city');
			$table->renameColumn('address', 'club_address');
			$table->renameColumn('phone', 'club_phone');
			$table->renameColumn('email', 'club_email');
		});

		Schema::table('regions', function(Blueprint $table)
		{
			$table->renameColumn('name', 'region_name');
		});

		Schema::table('provinces', function(Blueprint $table)
		{
			$table->renameColumn('name', 'province_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
			$table->renameColumn('club_name', 'name');
			$table->renameColumn('club_city', 'city');
			$table->renameColumn('club_address', 'address');
			$table->renameColumn('club_phone', 'phone');
			$table->renameColumn('club_email', 'email');
		});

		Schema::table('regions', function(Blueprint $table)
		{
			$table->renameColumn('region_name', 'name');
		});

		Schema::table('provinces', function(Blueprint $table)
		{
			$table->renameColumn('province_name', 'name');
		});
	}

}
