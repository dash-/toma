<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentRefereesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_referees',function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('referee1');
			$table->integer('referee2')->nullable();
			$table->integer('alternative_referee')->nullable();			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists("tournament_referees");
	}

}
