<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTournamentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->foreign('surface_id')
		      ->references('id')->on('tournament_surfaces');

		    $table->foreign('organizer_id')
		      ->references('id')->on('tournament_organizers');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->dropForeign('tournaments_surface_id_foreign');
			$table->dropForeign('tournaments_organizer_id_foreign');
		});
	}

}
