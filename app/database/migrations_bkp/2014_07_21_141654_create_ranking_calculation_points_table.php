<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRankingCalculationPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ranking_calculation_points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('points_from');
			$table->integer('points_to');
			$table->integer('stars');
		});

		$data = [
        	array('points_from' => 0, 'points_to' => 50, 'stars' => 1),
        	array('points_from' => 50, 'points_to' => 100, 'stars' => 2),
        	array('points_from' => 100, 'points_to' => 150, 'stars' => 3),
        	array('points_from' => 150, 'points_to' => 200, 'stars' => 4),
        	array('points_from' => 200, 'points_to' => 250, 'stars' => 5),
        	array('points_from' => 250, 'points_to' => 300, 'stars' => 6),
        	array('points_from' => 300, 'points_to' => 350, 'stars' => 7),
        	array('points_from' => 350, 'points_to' => 400, 'stars' => 8),
        	array('points_from' => 400, 'points_to' => 450, 'stars' => 9),
        	array('points_from' => 450, 'points_to' => 500, 'stars' => 10),
        	array('points_from' => 500, 'points_to' => 600, 'stars' => 11),
        	array('points_from' => 600, 'points_to' => 700, 'stars' => 12),
        	array('points_from' => 700, 'points_to' => 800, 'stars' => 13),
        	array('points_from' => 800, 'points_to' => 1000, 'stars' => 14),
        	array('points_from' => 1000, 'points_to' => 1500, 'stars' => 15),
        	array('points_from' => 1500, 'points_to' => 2000, 'stars' => 16),
        	array('points_from' => 2000, 'points_to' => 2500, 'stars' => 17),
        	array('points_from' => 2500, 'points_to' => 3000, 'stars' => 18),
        	array('points_from' => 3000, 'points_to' => 1000000, 'stars' => 19),
        ];

        foreach ($data as $d) {
            DB::table('ranking_calculation_points')->insert($d);
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ranking_calculation_points');
	}

}
