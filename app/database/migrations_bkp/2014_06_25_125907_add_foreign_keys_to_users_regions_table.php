<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_regions', function(Blueprint $table)
		{
			$table->foreign('user_id')
		      ->references('id')->on('users');

		    $table->foreign('region_id')
		      ->references('id')->on('regions');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_regions', function(Blueprint $table)
		{
			$table->dropForeign('users_regions_user_id_foreign');
			$table->dropForeign('users_regions_region_id_foreign');
		});
	}

}
