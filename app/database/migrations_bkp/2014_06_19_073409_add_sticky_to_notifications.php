<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStickyToNotifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_notifications', function(Blueprint $table)
		{
			$table->boolean('sticky')->default(TRUE);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_notifications', function(Blueprint $table)
		{
			$table->dropColumn('sticky');
		});
	}

}
