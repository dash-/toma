<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrawsReferessHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draws_referees_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_referee_id');
			$table->integer('draw_id');
			$table->integer('user_id');
			$table->integer('status')->default(0);
			$table->dateTime('date');
			$table->integer('changed_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draws_referees_history');
	}

}
