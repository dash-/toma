<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentDrawsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_draws', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('level_id');
			$table->integer('player_id');
			$table->integer('match_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_draws');
	}

}
