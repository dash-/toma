<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialUsersMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
		    $table->increments('id');
            $table->string('username', 64);
            $table->string('email', 64);
            $table->string('password', 255);
            $table->string('secret', 16);

            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('verified')->default(0);

            $table->dateTime('last_login')->nullable();
            $table->dateTime('created_at');
            $table->string('remember_token', 100)->nullable();
            
		});

		Schema::create('roles', function($table)
		{
		    $table->increments('id');
            $table->string('name', 32);
            $table->string('description', 255);
		});

		Schema::create('users_roles', function($table)
		{
            $table->integer('user_id');
            $table->integer('role_id');
		});

		Schema::create('users_regions', function($table)
		{
            $table->integer('user_id');
            $table->integer('region_id');
		});

		Schema::create('users_federations', function($table)
		{
            $table->integer('user_id');
            $table->integer('federation_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('roles');
		Schema::drop('users_roles');
		Schema::drop('users_regions');
		Schema::drop('users_federations');
	}

}
