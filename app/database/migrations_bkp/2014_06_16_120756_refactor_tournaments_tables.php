<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorTournamentsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//tournaments table
		Schema::dropIfExists('tournaments');
		Schema::create('tournaments',function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('surface_id');
			$table->integer('organizer_id');
			$table->string('title');
			$table->integer('status')->nullable();
			$table->integer('approved_by')->nullable();
			$table->dateTime('date_of_approval')->nullable();
		});

		Schema::dropIfExists('tournament_event_organizers');
		Schema::create('tournament_organizers',function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->string('fax')->nullable();
			$table->string("website", 100)->nullable();			
		});

		Schema::dropIfExists('tournament_level');
		Schema::create('tournaments_levels',function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');		
			$table->integer('level_id');		
		});


		Schema::dropIfExists('tournament_players');
		Schema::create('tournaments_players',function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');		
			$table->integer('player_id');		
		});


		Schema::dropIfExists('tournament_referees');
		Schema::create('tournaments_referees',function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');		
			$table->integer('referee_id');		
		});

		Schema::dropIfExists('tournament_dates');
		Schema::create('tournament_dates',function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->dateTime('main_draw_from')->nullable();
			$table->dateTime('main_draw_to')->nullable();
			$table->dateTime('qualifing_date_from')->nullable();
			$table->dateTime('qualifing_date_to')->nullable();
			$table->dateTime('entry_deadline')->nullable();
			$table->dateTime('withdrawal_deadline')->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tournaments');
		Schema::dropIfExists('tournament_organizers');
		Schema::dropIfExists('tournaments_levels');
		Schema::dropIfExists('tournaments_players');
		Schema::dropIfExists('tournaments_referees');
	}

}
