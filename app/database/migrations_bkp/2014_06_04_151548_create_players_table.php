<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->integer('contact_id')->nullable();
			$table->string('licence_number')->nullable();
			$table->string('ranking')->nullable();
			$table->string('ape_id')->nullable();
			$table->string('nationality')->nullable();
			$table->string('current_situation')->nullable();
			$table->string('federation_club_id')->nullable();
			$table->string('province_club_id')->nullable();
			$table->string('club_id')->nullable();
			$table->string('mutua')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
	}

}
