<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTournamentIdToMatchSchedules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->integer('tournament_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->dropColumn('tournament_id');
		});
	}

}
