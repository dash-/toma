<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddSetTeam1TieToMatchScores extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_scores', function(Blueprint $table)
		{
			$table->dropColumn('set_difference');
		});
		Schema::table('match_scores', function(Blueprint $table)
		{
			$table->integer('set_team1_tie')->nullable();
			$table->integer('set_team2_tie')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_scores', function(Blueprint $table)
		{
			$table->dropColumn('set_team1_tie');
			$table->dropColumn('set_team2_tie');
		});
	}

}
