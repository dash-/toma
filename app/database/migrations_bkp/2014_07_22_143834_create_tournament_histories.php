<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentHistories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_id');
			$table->integer('player_id')->nullable();
			$table->integer('team_id');
			$table->integer('points_earned')->nullable();
			$table->string('round_achieved', 12);
			$table->boolean('points_assigned')->default(FALSE);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_histories');
	}

}
