<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateQualifyersFromFromDrawSizesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->renameColumn('qualifyers_from', 'qualifiers_from');
			$table->renameColumn('qualifyers_to', 'qualifiers_to');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			
		});
	}

}
