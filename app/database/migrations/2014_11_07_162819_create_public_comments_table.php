<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePublicCommentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_comments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('table_name');
            $table->integer('table_id');
            $table->integer('user_id');
            $table->integer('parent_id')->nullable();
            $table->text('text');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('public_comments');
    }

}
