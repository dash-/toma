<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraceLogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('trace_logs', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("user_id")->nullable();
            $table->string("ip", 25)->nullable();
            $table->string("url", 255)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('trace_logs');
	}

}
