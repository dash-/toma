<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTeamIdColumnsFromGameScore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection("scoring_connection")->table('game_score', function(Blueprint $table)
		{
			$table->dropColumn("team1_id");
			$table->dropColumn("team2_id");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::connection("scoring_connection")->table('game_score', function(Blueprint $table)
		{
            $table->integer("team1_id");
            $table->integer("team2_id");
		});
	}

}
