<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerRegionalRankingHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::connection('pgsql2')->create('player_regional_ranking_history', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('regional_ranking')->nullable();
            $table->integer('regional_ranking_move')->nullable();
            $table->integer('ranking_points')->nullable();
            $table->timestamps();
        });
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('pgsql2')->drop('player_regional_ranking_history');
	}

}
