<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCronJobFinishedTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cron_job_finished', function(Blueprint $table)
		{
			$table->dropColumn('test');
			$table->string('action_done');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cron_job_finished', function(Blueprint $table)
		{
			//
		});
	}

}
