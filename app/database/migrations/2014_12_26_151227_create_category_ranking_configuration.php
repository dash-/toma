<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryRankingConfiguration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_ranking_configuration', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('gender', 1)->nullable();
			$table->integer('ranking_from')->nullable();
			$table->integer('ranking_to')->nullable();
			$table->integer('points')->nullable();
			$table->string('category')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_ranking_configuration');
	}

}
