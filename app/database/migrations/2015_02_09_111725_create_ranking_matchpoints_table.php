<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRankingMatchpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ranking_matchpoints', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('player_id');
			$table->integer('licence_number');
			$table->integer('signup_id');
			$table->integer('match_id');
			$table->integer('tournament_id');
			$table->integer('draw_id');
			$table->tinyInteger('list_type');
			$table->tinyInteger('round_number');
			$table->float('round_points')->default(0);
			$table->float('otroga_points')->default(0);
			$table->tinyInteger('match_status');
			$table->boolean('assigned')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ranking_matchpoints');
	}

}
