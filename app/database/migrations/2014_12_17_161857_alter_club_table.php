<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClubTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
            $table->string("club_postal_code", 10)->nullable();
            $table->integer("province_club_id")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function(Blueprint $table)
		{

            $table->dropColumn("province_club_id");
            $table->dropColumn("club_postal_code", 10);
		});
	}

}
