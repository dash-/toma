<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerClubRankingHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('pgsql2')->create('player_club_ranking_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('player_id');
            $table->integer('club_ranking')->nullable();
            $table->integer('club_ranking_move')->nullable();
            $table->integer('ranking_points')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('pgsql2')->drop('player_club_ranking_history');
	}

}
