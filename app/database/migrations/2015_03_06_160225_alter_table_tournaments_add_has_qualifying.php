<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTournamentsAddHasQualifying extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
            $table->boolean('has_qualifying')->default(true);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournaments', function(Blueprint $table)
		{
			$table->dropColumn("has_qualifying");
		});
	}

}
