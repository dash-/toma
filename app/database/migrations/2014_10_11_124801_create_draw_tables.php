<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDrawTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('range_from');
			$table->integer('range_to');
		});

		Schema::create('draw_matches', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team1_id')->nullable();
			$table->integer('team2_id')->nullable();
			$table->integer('draw_id');
			$table->integer('round_position')->nullable();
			$table->integer('round_number');
			$table->integer('team1_score')->nullable();
			$table->integer('team2_score')->nullable();
			$table->integer('list_type');
			$table->integer('match_status')->default(0);
		});

		Schema::create('draw_qualifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('draw_size')->nullable();
		});

		Schema::create('draw_seeds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('signup_id');
			$table->integer('list_type');
			$table->integer('seed_num')->default(0);
		});

		Schema::create('draw_sizes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('total')->nullable();
			$table->integer('direct_acceptances')->nullable();
			$table->integer('accepted_qualifiers')->nullable();
			$table->integer('wild_cards')->nullable();
			$table->integer('special_exempts')->default(0);
			$table->integer('onsite_direct')->default(0);
		});

		Schema::create('draws_referees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_id');
			$table->integer('user_id');
			$table->boolean('main_referee')->default(FALSE);
			$table->integer('status')->default(0);
			$table->dateTime('date_of_application');
			$table->dateTime('date_of_status_change');
			$table->integer('approved_by')->nullable();
		});

		Schema::create('draws_referees_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_referee_id');
			$table->integer('draw_id');
			$table->integer('user_id');
			$table->integer('status')->default(0);
			$table->dateTime('date');
			$table->integer('changed_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draw_categories');
		Schema::drop('draw_matches');
		Schema::drop('draw_qualifications');
		Schema::drop('draw_seeds');
		Schema::drop('draw_sizes');
		Schema::drop('draws_referees');
		Schema::drop('draws_referees_history');
	}

}
