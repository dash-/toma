<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableForRfetPreviousPoints extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfet_previous_points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('gender', 1);
			$table->date('period');
			$table->string('tournament_id');
			$table->string('licence_id');
			$table->string('status_round', 1);
			$table->string('opponent_licence_number');
			$table->integer('round');
			$table->float('opponent_ottorga_points');
			$table->float('points_from_round');
			$table->boolean('user_for_ranking_calculation')->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfet_previous_points');
	}

}
