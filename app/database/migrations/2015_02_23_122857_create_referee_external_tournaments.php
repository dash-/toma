<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefereeExternalTournaments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referee_external_tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('referee_id');
            $table->integer('created_by');
            $table->string('tournament_title');
			$table->datetime('refereed_date_from')->nullable();
			$table->datetime('refereed_date_to')->nullable();
			$table->string('city')->nullable();
			$table->string('country')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referee_external_tournaments');
	}

}
