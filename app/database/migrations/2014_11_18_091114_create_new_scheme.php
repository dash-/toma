<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewScheme extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $schemaName = 'ranking_history';
        DB::connection('pgsql')->statement('CREATE SCHEMA '.$schemaName);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        $schemaName = 'ranking_history';
        DB::connection('pgsql')->statement('DROP SCHEMA '.$schemaName);
	}

}
