<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSurfaceIdInTournaments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->dropColumn('surface_id');
        });

        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->integer("surface_id")->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->dropColumn('surface_id');
        });

        Schema::table('tournaments', function(Blueprint $table)
        {
            $table->integer("surface_id")->nullable();
        });
	}

}
