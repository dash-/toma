<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefereeRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referee_registrations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("referee_id");
			$table->integer("year");
			$table->tinyInteger("status")->default(1);
			$table->integer("created_by")->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referee_registrations');
	}

}
