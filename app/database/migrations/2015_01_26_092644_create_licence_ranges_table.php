<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLicenceRangesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licence_ranges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('region_id');
			$table->integer('range_from');
			$table->integer('range_to');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licence_ranges');
	}

}
