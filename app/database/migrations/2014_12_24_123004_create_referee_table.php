<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefereeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('referees', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("player_id")->nullable();
			$table->integer("contact_id")->nullable();
			$table->string("licence_number", 20)->nullable();
			$table->string("school_number", 10)->nullable();
			$table->string("licence_category", 2)->nullable();
			$table->integer("level")->nullable();
			$table->integer("active")->default(1);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('referees');
	}

}
