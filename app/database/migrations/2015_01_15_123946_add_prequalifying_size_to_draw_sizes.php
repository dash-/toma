<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddPrequalifyingSizeToDrawSizes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->integer('prequalifying_size')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_sizes', function(Blueprint $table)
		{
			$table->dropColumn('prequalifying_size');
		});
	}

}
