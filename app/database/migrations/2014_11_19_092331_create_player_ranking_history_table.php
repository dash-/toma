<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerRankingHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('pgsql2')->create('player_ranking_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('player_id');
			$table->integer('ranking')->nullable();
			$table->integer('ranking_move')->nullable();
			$table->integer('ranking_points')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection('pgsql2')->drop('player_ranking_history');
	}

}
