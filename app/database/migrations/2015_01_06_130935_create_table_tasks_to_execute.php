<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasksToExecute extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_to_execute', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime("time_of_schedule")->nullable();
			$table->dateTime("time_to_execute")->nullable();
			$table->boolean("executed")->default(0);
			$table->string("model")->nullable();
			$table->string("action")->nullable();
			$table->string("serialized_parameters")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_to_execute');
	}

}
