<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricePerPlayerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_per_player_prices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('draw_category_id');
			$table->float('premia');
			$table->float('premia_0_3000');
			$table->float('premia_3000_6000');
			$table->float('premia_6001');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournament_per_player_prices');
	}

}
