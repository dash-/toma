<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlayerQuarterRankingHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::connection('pgsql2')->create('player_quarter_ranking_history', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('player_id');
            $table->integer('ranking')->nullable();
            $table->integer('ranking_move')->nullable();
            $table->integer('ranking_points')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::connection('pgsql2')->drop('player_quarter_ranking_history');
	}

}
