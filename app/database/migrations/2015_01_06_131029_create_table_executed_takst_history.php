	<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExecutedTakstHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('executed_tasks_history', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime("executed_time")->nullable();
			$table->string("model")->nullable();
			$table->string("action")->nullable();
			$table->string("serialized_parameters")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('executed_tasks_history');
	}

}
