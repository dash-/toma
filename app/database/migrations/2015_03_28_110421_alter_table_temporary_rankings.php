<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTemporaryRankings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('temporary_rankings', function(Blueprint $table)
		{
            $table->integer("quarter_ranking")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('temporary_rankings', function(Blueprint $table)
		{
			//
            $table->dropColumn("quarter_ranking");
		});
	}

}
