<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDrawCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('draw_categories', function(Blueprint $table)
        {
            $table->integer('range_for_signup_from')->default(0);
            $table->integer('range_for_signup_to')->default(100);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('draw_categories', function(Blueprint $table)
        {
            $table->dropColumn('range_for_signup_from');
            $table->dropColumn('range_for_signup_to');
        });
	}

}
