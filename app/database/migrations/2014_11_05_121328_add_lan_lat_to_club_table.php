<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanLatToClubTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
			$table->string("position_lat", 20)->nullable();
			$table->string("position_long", 20)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clubs', function(Blueprint $table)
		{
            $table->dropColumn("position_lat");
            $table->dropColumn("position_long");
		});
	}

}
