<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDrawSetupOnDrawTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{
			$table->tinyInteger("match_rule")->default(0);
			$table->tinyInteger("set_rule")->default(0);
			$table->tinyInteger("game_rule")->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_draws', function(Blueprint $table)
		{

            $table->dropColumn("match_rule");
            $table->dropColumn("set_rule");
            $table->dropColumn("game_rule");
		});
	}

}
