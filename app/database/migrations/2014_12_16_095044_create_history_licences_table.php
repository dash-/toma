<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryLicencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('history_licences', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("contact_id");
            $table->integer("year");
            $table->integer("federation_id")->nullable();
            $table->integer("province_id")->nullable();
            $table->integer("club_id")->nullable();
            $table->string("mutua", 50)->nullable();
//			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('history_licences');
	}

}
