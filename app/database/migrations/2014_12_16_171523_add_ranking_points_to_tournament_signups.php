<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRankingPointsToTournamentSignups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->integer('ranking_points')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_signups', function(Blueprint $table)
		{
			$table->dropColumn('ranking_points');
		});
	}

}
