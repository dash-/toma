<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameScoreTiebreaks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection("scoring_connection")->create('game_score_tiebreaks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("match_id");
			$table->integer("game_score_id");
			$table->integer("game_number_id");
			$table->integer("team1_tiescore")->default(0);
			$table->integer("team2_tiescore")->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::connection("scoring_connection")->drop('game_score_tiebreaks');
	}

}
