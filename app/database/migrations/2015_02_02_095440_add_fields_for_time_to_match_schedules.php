<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsForTimeToMatchSchedules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->time('schedule_time')->nullable();
			$table->tinyInteger('type_of_time')->defaut(0)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->dropColumn('schedule_time');
			$table->dropColumn('type_of_time');
		});
	}

}
