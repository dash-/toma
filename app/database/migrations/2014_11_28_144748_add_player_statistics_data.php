<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPlayerStatisticsData extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('player_history_statistics', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("player_id");
            $table->float("height")->nullabl();
            $table->float("weight")->nullable();
            $table->integer("handed")->nullable();
            $table->datetime("date")->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('player_history_statistics');
	}

}
