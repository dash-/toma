<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestOfTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sponsors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('region_id')->default(0);
			$table->string('city')->nullable();
			$table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('website')->nullable();
			$table->string('image')->nullable();
		});

		Schema::create('article_images', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('article_id');
			$table->string('image');
		});

		Schema::create('nation',function (Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
					
		});

		Schema::create('temporary_rankings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('player_id');
			$table->integer('ranking')->nullable();
			$table->integer('ranking_points')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sponsors');
		Schema::drop('article_images');
		Schema::drop('nation');
		Schema::drop('temporary_rankings');
	}

}
