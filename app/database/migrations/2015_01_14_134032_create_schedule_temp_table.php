<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTempTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temp_match_schedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->nullable();
			$table->integer('tournament_id')->nullable();
			$table->integer('draw_id')->nullable();
			$table->integer('list_type')->nullable();
			$table->integer('order_of_play')->nullable();
			$table->integer('court_number')->nullable();
			$table->string('time_of_play')->nullable();
			$table->dateTime('date_of_play')->nullable();
			$table->boolean('exact_time')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temp_match_schedules');
	}

}
