<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTournamentDraws extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->integer('approval_status')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->dropColumn('approval_status');
        });
	}

}
