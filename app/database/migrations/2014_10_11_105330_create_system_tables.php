<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exceptions', function($table)
        {
            $table->increments('id');
            $table->string("statusCode", 10)->nullable();
            $table->text("message")->nullable();
            $table->string("url", 100)->nullable();
            $table->string("browser", 100)->nullable();
            $table->timestamps();
        });

        Schema::create('login_history', function($table) 
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('ip', 20);
			$table->string('browser', 255);
			$table->dateTime('created_at');
		});

		Schema::create('cache', function($table)
        {
            $table->increments('id');
            $table->string('key')->unique();
            $table->text('value');
            $table->integer('expiration');
        });

        Schema::create('cron_job_finished', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('ran')->nullable();
            $table->string('test')->nullable();
		});

		Schema::create('table_seeds', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string("seed_name");
		});

		Schema::create('email_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email_from');
			$table->string('email_to');
			$table->text('full_body');
			$table->dateTime('created_at');
		});

		Schema::create('languages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 6);
			$table->string('name', 12);
			$table->string('encoding', 16);
			$table->boolean('enabled')->default(true);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exceptions');
		Schema::drop('login_history');
		Schema::drop('cache');
		Schema::drop('cron_job_finished');
		Schema::drop('table_seeds');
		Schema::drop('email_logs');
		Schema::drop('languages');
	}

}
