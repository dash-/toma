<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFixScoreSchemeProblem extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::connection("pgsql2")->drop('game_score');
        Schema::connection("pgsql2")->drop('game_score_details');

        Schema::connection("scoring_connection")->create('game_score_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("game_score_id");
            $table->integer("team1_score")->default(0);
            $table->integer("team2_score")->default(0);
            $table->timestamps();
        });

        Schema::connection("scoring_connection")->create('game_score', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("match_id");
            $table->integer("team1_id");
            $table->integer("team2_id");
            $table->integer("game_number");
            $table->integer("set_number");
            $table->tinyInteger("served_by");
            $table->tinyInteger("game_winner");
            $table->tinyInteger("break_point");
            $table->timestamps();
        });


    }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::connection("scoring_connection")->drop('game_score');
        Schema::connection("scoring_connection")->drop('game_score_details');

        Schema::connection("pgsql2")->create('game_score', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("match_id");
            $table->integer("team1_id");
            $table->integer("team2_id");
            $table->integer("game_number");
            $table->integer("set_number");
            $table->tinyInteger("served_by");
            $table->tinyInteger("game_winner");
            $table->tinyInteger("break_point");
            $table->timestamps();
        });

        Schema::connection("pgsql2")->create('game_score_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer("game_score_id");
            $table->integer("team1_score")->default(0);
            $table->integer("team2_score")->default(0);
            $table->timestamps();
        });
	}

}
