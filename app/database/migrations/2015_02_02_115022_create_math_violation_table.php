<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMathViolationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_violations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("team_id");
			$table->integer("player_id")->nullable();
			$table->string("licence_number")->nullable();
			$table->integer("tournament_id");
			$table->integer("draw_id");
			$table->integer("round_number")->nullable();
			$table->integer("match_id")->nullable();
			$table->integer("type_of_violation")->default(0);
			$table->boolean("used_for_ranking")->default(FALSE);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_violations');
	}

}
