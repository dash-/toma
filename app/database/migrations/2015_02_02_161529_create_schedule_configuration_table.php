<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScheduleConfigurationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedule_configuration', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->datetime('date');
			$table->integer('number_of_courts');
			$table->boolean('waiting_list')->default(false);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedule_configuration');
	}

}
