<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddConsolationTypeToDrawMatches extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->tinyInteger('consolation_type')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->dropColumn('consolation_type');
		});
	}

}
