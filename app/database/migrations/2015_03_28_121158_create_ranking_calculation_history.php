<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankingCalculationHistory extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ranking_calculation_history', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("ranking_type");
            $table->integer("number_recalculated")->nullable();
            $table->boolean("manual")->default(false);
			$table->dateTime("date");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ranking_calculation_history');
	}

}
