<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrawApprovalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_approvals', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('draw_id');
            $table->integer('approval_status');
            $table->string('approved_by');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('draw_approvals', function(Blueprint $table)
		{
			//
		});
	}

}
