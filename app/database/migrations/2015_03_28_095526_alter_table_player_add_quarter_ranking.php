<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlayerAddQuarterRanking extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('players', function(Blueprint $table)
		{
			$table->integer("quarter_ranking")->nullable();
			$table->integer("quarter_ranking_move")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('players', function(Blueprint $table)
		{
			//
			$table->dropColumn("quarter_ranking");
			$table->dropColumn("quarter_ranking_move");
		});
	}

}
