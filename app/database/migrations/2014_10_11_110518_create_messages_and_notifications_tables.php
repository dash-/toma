<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesAndNotificationsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('conversation_id');
			$table->integer('sender_id');
			$table->integer('type_id')->nullable();
			$table->string('subject')->nullable();
			$table->text('text')->nullable();
			$table->dateTime('time_sent')->nullable();
		});

		Schema::create('messages_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('message_id');
			$table->boolean('seen')->default(FALSE);
			$table->dateTime('time_seen')->nullable();
			$table->dateTime('deleted_at')->nullable();
		});

		Schema::create('notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type_id');
			$table->string('link');
			$table->string('text');
			$table->dateTime('created_at')->nullable();
		});

		Schema::create('notification_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
		Schema::drop('messages_users');
		Schema::drop('notifications');
		Schema::drop('notification_types');
	}

}
