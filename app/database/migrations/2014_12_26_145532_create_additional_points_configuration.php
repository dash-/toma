<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalPointsConfiguration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('additional_points_configuration', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string("gender", 1)->nullable();
			$table->integer("ranking_from")->nullable();
			$table->integer("ranking_to")->nullable();
			$table->integer("additional_points")->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('additional_points_configuration');
	}

}
