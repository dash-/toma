<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddWaitingListSettingsToMatchSchedules extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->integer('waiting_list_row')->nullable();
			$table->integer('waiting_list_court_number')->nullable();
			$table->integer('waiting_list_type_of_time')->nullable();
			$table->time('waiting_list_time')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('match_schedules', function(Blueprint $table)
		{
			$table->dropColumn('waiting_list_row');
			$table->dropColumn('waiting_list_court_number');
			$table->dropColumn('waiting_list_type_of_time');
			$table->dropColumn('waiting_list_time');
		});
	}

}
