<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLicenceNumberColumnTypeInTournamentTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
//		DB::statement('ALTER TABLE tournament_teams MODIFY COLUMN licence_number integer');
		DB::statement('ALTER TABLE tournament_teams ALTER COLUMN licence_number TYPE integer USING (licence_number::integer)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
//		DB::statement('ALTER TABLE tournament_teams MODIFY COLUMN licence_number varchar');
		DB::statement('ALTER TABLE tournament_teams ALTER COLUMN licence_number TYPE VARCHAR(50)');

	}

}
