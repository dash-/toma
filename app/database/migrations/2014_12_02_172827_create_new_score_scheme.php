<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewScoreScheme extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $schemaName = 'scoring';
        DB::connection('pgsql')->statement('CREATE SCHEMA '.$schemaName);

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $schemaName = 'scoring';
        DB::connection('pgsql')->statement('DROP SCHEMA '.$schemaName);
	}

}
