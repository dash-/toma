<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersMigrations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
		    $table->increments('id');
            $table->string('username', 64);
            $table->string('email', 64);
            $table->string('password', 255);
            $table->string('secret', 16);
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('verified')->default(0);
            $table->dateTime('last_login')->nullable();
            $table->dateTime('created_at');
            $table->string('remember_token', 100)->nullable();
            $table->string('image')->nullable();
            $table->integer('language_id')->nullable();
		});

		Schema::create('roles', function($table)
		{
		    $table->increments('id');
            $table->string('name', 32);
            $table->string('description', 255);
		});

		Schema::create('users_roles', function($table)
		{
		    $table->increments('id');
            $table->integer('user_id');
            $table->integer('role_id');
		});

		Schema::create('users_regions', function($table)
		{
		    $table->increments('id');
            $table->integer('user_id');
            $table->integer('region_id');
		});

		Schema::create('users_federations', function($table)
		{
		    $table->increments('id');
            $table->integer('user_id');
            $table->integer('federation_id');
		});

		Schema::create('users_notifications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('notification_id');
			$table->boolean('seen')->default(FALSE);
			$table->boolean('deleted')->default(FALSE);
			$table->boolean('sticky')->default(TRUE);
		});


		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('contact_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
		Schema::drop('users_notifications');
		Schema::drop('users_federations');
		Schema::drop('users_regions');
		Schema::drop('users_roles');
		Schema::drop('roles');
		Schema::drop('users');
	}

}
