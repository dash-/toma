<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalTournamentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('external_tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('player_id');
			$table->integer('licence_number');
			$table->string('tournament_type');
			$table->integer('external_points_acquired');
			$table->integer('total_calculated_points');
			$table->boolean('points_used_for_ranking_calculations')->default(0);
			$table->integer('user_edit_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
			Schema::drop('external_tournaments');
	}

}
