<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExternalTournaments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('external_tournaments', function(Blueprint $table)
		{
			$table->boolean("points_used_for_quarter_ranking_calculations")->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('external_tournaments', function(Blueprint $table)
		{
			$table->dropColumn("points_used_for_quarter_ranking_calculations");
		});
	}

}
