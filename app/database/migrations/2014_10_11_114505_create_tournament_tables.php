<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTournamentTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('organizer_id');
			$table->integer('surface_id');
			$table->integer('club_id')->nullable();
			$table->integer('referee_id')->nullable();
			$table->integer('number_of_courts')->default(4);
			$table->integer('approved_by')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('status')->nullable();
			$table->dateTime('date_of_approval')->nullable();
			$table->string('slug')->nullable();
		});

		Schema::create('tournament_articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->string('title')->nullable();
			$table->text('content')->nullable();
			$table->boolean('active')->default(false);
			$table->integer('created_by');
			$table->timestamps();
		});

		Schema::create('tournament_dates',function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->dateTime('main_draw_from')->nullable();
			$table->dateTime('main_draw_to')->nullable();
			$table->dateTime('qualifying_date_from')->nullable();
			$table->dateTime('qualifying_date_to')->nullable();
			$table->dateTime('entry_deadline')->nullable();
			$table->dateTime('withdrawal_deadline')->nullable();
		});

		Schema::create('tournament_draws', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_category_id');
			$table->string('draw_type', 16);
			$table->string('draw_gender', 12);
			$table->string('draw_strength', 32)->nullable();
			$table->integer('number_of_seeds')->nullable();
			$table->integer('level')->nullable();
			$table->integer('prize_pool')->nullable();
			$table->integer('manual_draw')->default(0);
			$table->string('unique_draw_id')->nullable();
		});

		Schema::create('tournament_histories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_id');
			$table->integer('player_id')->nullable();
			$table->integer('team_id');
			$table->integer('points_earned')->nullable();
			$table->string('round_achieved', 12);
			$table->boolean('points_assigned')->default(FALSE);
			$table->timestamps();
		});

		Schema::create('tournament_organizers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('email')->nullable();
			$table->string('phone', 64)->nullable();
			$table->string('fax', 64)->nullable();;
			$table->string('website', 100)->nullable();
		});

		Schema::create('tournament_signups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('draw_id');
			$table->integer('team_id')->nullable();
			$table->integer('list_type')->nullable();
			$table->integer('move_from_qualifiers_type')->default(0);
			$table->integer('rank');
			$table->integer('status')->nullable();
			$table->integer('submited_by');
			$table->dateTime('withdrawal_date')->nullable();
			$table->dateTime('created_at');
		});

		Schema::create('tournament_surfaces',function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');			
		});

		Schema::create('tournament_teams', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('team_id');
			$table->string('licence_number')->nullable();
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->dateTime('date_of_birth')->nullable();
			$table->string('email', 128)->nullable();
			$table->string('phone', 64)->nullable();
			$table->integer('current_ranking')->nullable();
			$table->integer('ranking_points')->nullable();
			$table->integer('seed_num')->default(0);
		});

		Schema::create('tournaments_sponsors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('tournament_id');
			$table->integer('sponsor_id');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tournaments');
		Schema::drop('tournament_articles');
		Schema::drop('tournament_dates');
		Schema::drop('tournament_draws');
		Schema::drop('tournament_histories');
		Schema::drop('tournament_organizers');
		Schema::drop('tournament_signups');
		Schema::drop('tournament_surfaces');
		Schema::drop('tournament_teams');
		Schema::drop('tournaments_sponsors');
	}

}
