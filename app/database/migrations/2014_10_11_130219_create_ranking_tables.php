<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRankingTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ranking_calculation_points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('points_from');
			$table->integer('points_to');
			$table->integer('stars');
		});

		Schema::create('ranking_calculation_prizes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('prize_from');
			$table->integer('prize_to');
			$table->integer('stars');
		});

		Schema::create('ranking_points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('w');
			$table->integer('f');
			$table->integer('sf');
			$table->integer('qf');
			$table->integer('r16');
			$table->integer('r32');
			$table->integer('r64');
			$table->integer('r128');
			$table->integer('stars');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ranking_calculation_points');
		Schema::drop('ranking_calculation_prizes');
		Schema::drop('ranking_points');
	}

}
