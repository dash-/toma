<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddHasFinalRoundToDrawQualification extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_qualifications', function(Blueprint $table)
		{
			$table->boolean('has_final_round')->default(false);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_qualifications', function(Blueprint $table)
		{
			$table->dropColumn('has_final_round');
		});
	}

}
