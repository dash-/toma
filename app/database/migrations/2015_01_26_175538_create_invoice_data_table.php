<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoiceDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoice_data', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('invoice_id');
			$table->integer('amount')->default(0);
			$table->text('description')->nullable();
			$table->float('price')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoice_data');
	}

}
