<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRestTimeToScheduleConfiguration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('schedule_configuration', function(Blueprint $table)
		{
			$table->integer('rest_time')->default(120);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('schedule_configuration', function(Blueprint $table)
		{
			$table->dropColumn('rest_time');
		});
	}

}
