<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGameScoreDetails extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection("pgsql2")->create('game_score_details', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("game_score_id");
            $table->integer("team1_score")->default(0);
            $table->integer("team2_score")->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::connection("pgsql2")->drop('game_score_details');
	}

}
