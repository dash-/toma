<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('match_schedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id');
			$table->integer('draw_id');
			$table->integer('tournament_id');
			$table->integer('court_number');
			$table->string('time_of_play');
			$table->dateTime('date_of_play');
			$table->boolean('exact_time')->default(FALSE);
			$table->integer('list_type')->default(2);
			$table->integer('order_of_play')->default(0);
			$table->integer('match_status')->default(0);
		});

		Schema::create('match_scores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('match_id')->nullable();
			$table->integer('set_number')->nullable();
			$table->integer('set_team1_score')->nullable();
			$table->integer('set_team2_score')->nullable();
			$table->integer('set_team1_tie')->nullable();
			$table->integer('set_team2_tie')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('match_schedules');
		Schema::drop('match_scores');
	}

}
