<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayerTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('players', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->integer('contact_id')->nullable();
			$table->integer('licence_number')->nullable();
			$table->string('club_id', 12)->nullable();
			$table->string('federation_club_id', 12)->nullable();
			$table->string('province_club_id', 64)->nullable();
			$table->integer('ranking')->nullable();
			$table->integer('ranking_points')->nullable();
			$table->integer('ranking_move')->nullable();
			$table->integer('province_ranking')->nullable();
			$table->integer('club_ranking')->nullable();
			$table->integer('regional_ranking')->nullable();
			$table->string('ape_id', 128)->nullable();
			$table->string('mutua', 64)->nullable();
			$table->integer('nationality')->nullable();
			$table->string('current_situation', 128)->nullable();
			$table->integer('approved_by')->nullable();
			$table->dateTime('date_of_approval')->nullable();
			$table->integer('additional_points')->nullable();
			$table->string('category', 10)->nullable();
			$table->boolean('is_referee')->default(false);
			$table->boolean('is_coach')->default(false);
			$table->integer('coach_level')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('status')->nullable();
		});

		Schema::create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('surname')->nullable();
			$table->string('date_of_birth', 64)->nullable();
			$table->string('sex', 6)->nullable();
			$table->string('email', 128)->nullable();
			$table->string('phone', 128)->nullable();
			$table->string('address')->nullable();
			$table->string('postal_code', 32)->nullable();
			$table->string('city')->nullable();
			$table->string('card_id', 128)->nullable();
			$table->string('image_link')->nullable();
			$table->boolean('mail_subscription')->default(FALSE);
			$table->string('web', 128)->nullable();
		});

		Schema::create('clubs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('region_id');
			$table->integer('province_id');
			$table->string('club_name');
			$table->string('short_name');
			$table->string('club_address')->nullable();
			$table->string('club_city')->nullable();
			$table->string('club_email')->nullable();
			$table->string('club_phone', 64)->nullable();
			$table->string('phone2', 64)->nullable();
			$table->string('fax', 64)->nullable();
			$table->string('president')->nullable();
			$table->string('image_link')->nullable();
			$table->string('nif')->nullable();
			$table->integer('number_of_tracks')->nullable();
			$table->dateTime('founded_date')->nullable();
			$table->string('web')->nullable();
			$table->timestamps();
		});

		Schema::create('provinces', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('region_id');
			$table->string('province_name');
		});

		Schema::create("regions", function($table)
        {
			$table->increments('id');
            $table->string("region_name")->nullable();
            $table->string("president")->nullable();
            $table->string("full_name")->nullable();
            $table->string("email")->nullable();
            $table->string("address")->nullable();
            $table->string("phone")->nullable();
            $table->string("fax")->nullable();
            $table->string("working_hours")->nullable();
            $table->string("web")->nullable();
            $table->string("image_path")->nullable();
        });
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('players');
		Schema::drop('contacts');
		Schema::drop('clubs');
		Schema::drop('provinces');
		Schema::drop('regions');
	}

}
