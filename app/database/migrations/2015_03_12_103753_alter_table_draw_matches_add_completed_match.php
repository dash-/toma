<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDrawMatchesAddCompletedMatch extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
            $table->boolean("completed")->default(FALSE);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('draw_matches', function(Blueprint $table)
		{
			$table->dropColumn("completed");
		});
	}

}
