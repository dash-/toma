<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRankingFieldsToTemporaryRankings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('temporary_rankings', function(Blueprint $table)
		{
			$table->integer('club_ranking')->nullable();
			$table->integer('province_ranking')->nullable();
			$table->integer('regional_ranking')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('temporary_rankings', function(Blueprint $table)
		{
			$table->dropColumn('club_ranking');
			$table->dropColumn('province_ranking');
			$table->dropColumn('regional_ranking');
		});
	}

}
