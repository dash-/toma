<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGameScore extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::connection("pgsql2")->create('game_score', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer("match_id");
            $table->integer("team1_id");
            $table->integer("team2_id");
            $table->integer("game_number");
            $table->integer("set_number");
            $table->tinyInteger("served_by");
            $table->tinyInteger("game_winner");
            $table->tinyInteger("break_point");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::connection("pgsql2")->drop('game_score');
	}

}
