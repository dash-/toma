<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRankingMatchpointsTableAddCustomdate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ranking_matchpoints', function(Blueprint $table)
		{
            $table->boolean("old_score")->default(false);
            $table->date("tournament_period")->nullable();
            $table->boolean("assigned_for_quarter_ranking")->default(false);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ranking_matchpoints', function(Blueprint $table)
		{
            $table->dropColumn("old_score");
            $table->dropColumn("tournament_period");
            $table->dropColumn("assigned_for_quarter_ranking");
		});
	}

}
