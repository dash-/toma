<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsPreQualiToTournamentDraws extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->boolean('is_prequalifying')->default(false);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('tournament_draws', function(Blueprint $table)
        {
            $table->dropColumn('is_prequalifying');
        });
	}

}
