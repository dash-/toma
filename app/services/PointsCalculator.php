<?php namespace App\Services;

class PointsCalculator
{

    private $round_keys = [
        1 => 'w',
        2 => 'f',
        4 => 'sf',
        8 => 'qf',
        16 => 'r16',
        32 => 'r32',
        64 => 'r64',
        128 => 'r128',
    ];

    /**
     * calculate all points in draw no m
     * @param $draw_id integer
     */
    public function drawPointsAssign($draw_id)
    {
        // first we need to clear all matches already with this draw id to the ranking_matchpoints table
        // because we are doing complete recalculation of all points
        \RankingMatchpoint::where('draw_id', $draw_id)->delete();

        $matches = \DrawMatch::with('team1_data.player', 'team2_data.player', 'draw')
            ->whereDrawId($draw_id)
            ->orderBy('id')
            ->get();

        $draw = \TournamentDraw::with('tournament', 'qualification')->find($draw_id);
        $quali_coeficient = \TournamentType::find(9)->coeficient;
        $main_coeficient = \TournamentType::find($draw->tournament->tournament_type)->coeficient;
        $consolation_coeficient = \TournamentType::find(8)->coeficient;
        $negative_coeficient = \Config::get('coefficient.param');
        $tournament_period = \DateTimeHelper::GetTournamentPeriodDate($draw->tournament->date->main_draw_to);

        // first calculate winner points separate for each draw
        if ($draw->qualification->has_final_round)
            self::calculateAndSaveQualiWinner($matches, $quali_coeficient);

        $main_points = self::calculateAndSaveMainWinner($matches, $main_coeficient);
        self::calculateAndSaveConsolationWinner($matches, $consolation_coeficient);

        foreach ($matches as $match) {
            for ($i = 1; $i <= 2; $i++) {
                if ($match->team1_data OR $match->team2_data) {
                    $matchpoint = new \RankingMatchpoint();

                    $matchpoint->tournament_id = $match->draw->tournament_id;
                    $matchpoint->draw_id = $match->draw_id;
                    $matchpoint->list_type = $match->list_type;
                    $matchpoint->round_number = $match->round_number;
                    $matchpoint->match_id = $match->id;
                    $matchpoint->tournament_period = $tournament_period;

                    if ($i == 1) {
                        $matchpoint->player_id = ($match->team1_data) ? $match->team1_data->player->id : 0;
                        $matchpoint->licence_number = ($match->team1_data) ? $match->team1_data->licence_number : 0;
                        $matchpoint->signup_id = $match->team1_id;

                        if ($match->team1_id == 0 OR $match->team2_id == 0 OR $match->match_status > 0)
                            $matchpoint->otroga_points = 0;
                        elseif ($match->list_type == 2)
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team1_data') * $quali_coeficient;
                        elseif ($match->list_type == 0 AND $match->consolation_type > 0)
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team1_data') * $consolation_coeficient;
                        else
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team1_data') * $main_coeficient;

                    } else {
                        $matchpoint->player_id = ($match->team2_data) ? $match->team2_data->player->id : 0;
                        $matchpoint->licence_number = ($match->team2_data) ? $match->team2_data->licence_number : 0;
                        $matchpoint->signup_id = $match->team2_id;

                        if ($match->team1_id == 0 OR $match->team2_id == 0 OR $match->match_status > 0)
                            $matchpoint->otroga_points = 0;
                        elseif ($match->list_type == 2)
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team2_data') * $quali_coeficient;
                        elseif ($match->list_type == 0 AND $match->consolation_type > 0)
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team2_data') * $consolation_coeficient;
                        else
                            $matchpoint->otroga_points = self::calculateOtorgaPoints($match, 'team2_data') * $main_coeficient;
                    }

                    $matchpoint->match_status = ($match->team1_score > $match->team2_score AND $i == 1) ? 1 : 2;

                    if (($match->team1_id == 0 OR $match->team2_id == 0) OR ($match->match_status > 0 AND $match->round_number == 1))
                        $matchpoint->round_points = 0;
                    elseif ($match->list_type == 2)
                        $matchpoint->round_points = self::calculateRoundPoints($match, $matches) * $quali_coeficient;
                    elseif ($match->list_type == 0 AND $match->consolation_type > 0)
                        $matchpoint->round_points = self::calculateRoundPoints($match, $matches) * $consolation_coeficient;
                    else
                        $matchpoint->round_points = self::calculateRoundPoints($match, $matches) * $main_coeficient;

                    if (in_array($match->match_status, [1, 5]) AND $match->list_type == 1) {
                        if ($i == 1 AND $match->team1_score < $match->team2_score)
                            $matchpoint->round_points = ($main_points * $negative_coeficient) * -1;
                        elseif ($i == 2 AND $match->team1_score > $match->team2_score)
                            $matchpoint->round_points = ($main_points * $negative_coeficient) * -1;
                    }

                    $matchpoint->save();
                }
            }
        }
    }


    private function calculateNegativePoints($match, $main_points, $which_player)
    {


    }

    /**
     * method to calculate winner points for consolation rounds
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @param $consolation_coeficient float
     * @return mixed void|boolean
     */
    private function calculateAndSaveConsolationWinner($matches, $consolation_coeficient)
    {
        $finals = $matches->filter(function ($item) {
            return $item->list_type == 0 AND $item->consolation_type > 0;
        })->last();

        if (!count($finals))
            return false;

        $matchpoint = new \RankingMatchpoint();

        $matchpoint->tournament_period = \DateTimeHelper::GetTournamentPeriodDate($finals->draw->tournament->date->main_draw_to);
        $matchpoint->tournament_id = $finals->draw->tournament_id;
        $matchpoint->draw_id = $finals->draw_id;
        $matchpoint->list_type = 0;
        $matchpoint->round_number = $finals->round_number + 1;
        $matchpoint->match_id = 0; // 0 defines winner because we don't have them in matches database


        $matchpoint->player_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->player->id
            : $finals->team2_data->player->id;

        $matchpoint->licence_number = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->licence_number
            : $finals->team2_data->licence_number;

        $matchpoint->signup_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_id
            : $finals->team2_id;

        $matchpoint->match_status = 3;
        $matchpoint->otroga_points = 0;

        $matchpoint->round_points = self::calculateWinnerPoints($finals) * $consolation_coeficient;

        $matchpoint->save();
    }

    /**
     * method to calculate winner points for quali rounds
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @param $quali_coeficient float
     * @return mixed void|boolean
     */
    private function calculateAndSaveQualiWinner($matches, $quali_coeficient)
    {
        $finals = $matches->filter(function ($item) {
            return $item->list_type == 2;
        })->last();

        if (!count($finals))
            return false;

        $matchpoint = new \RankingMatchpoint();

        $matchpoint->tournament_period = \DateTimeHelper::GetTournamentPeriodDate($finals->draw->tournament->date->main_draw_to);
        $matchpoint->tournament_id = $finals->draw->tournament_id;
        $matchpoint->draw_id = $finals->draw_id;
        $matchpoint->list_type = $finals->list_type;
        $matchpoint->round_number = $finals->round_number + 1;
        $matchpoint->match_id = 0; // 0 defines winner because we don't have them in matches database


        $matchpoint->player_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->player->id
            : $finals->team2_data->player->id;

        $matchpoint->licence_number = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->licence_number
            : $finals->team2_data->licence_number;

        $matchpoint->signup_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_id
            : $finals->team2_id;

        $matchpoint->match_status = 3;
        $matchpoint->otroga_points = 0;

        $matchpoint->round_points = self::calculateWinnerPoints($finals) * $quali_coeficient;

        $matchpoint->save();
    }

    /**
     * method to calculate winner points for main rounds
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @param $main_coeficient float
     * @return integer rount_points
     */
    private function calculateAndSaveMainWinner($matches, $main_coeficient)
    {
        $finals = $matches->filter(function ($item) {
            return $item->list_type == 1;
        })->last();

        $matchpoint = new \RankingMatchpoint();

        $matchpoint->tournament_period = \DateTimeHelper::GetTournamentPeriodDate($finals->draw->tournament->date->main_draw_to);
        $matchpoint->tournament_id = $finals->draw->tournament_id;
        $matchpoint->draw_id = $finals->draw_id;
        $matchpoint->list_type = $finals->list_type;
        $matchpoint->round_number = $finals->round_number + 1;
        $matchpoint->match_id = 0; // 0 defines winner because we don't have them in matches database


        $matchpoint->player_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->player->id
            : $finals->team2_data->player->id;

        $matchpoint->licence_number = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_data->licence_number
            : $finals->team2_data->licence_number;

        $matchpoint->signup_id = ($finals->team1_score > $finals->team2_score)
            ? $finals->team1_id
            : $finals->team2_id;

        $matchpoint->match_status = 3;
        $matchpoint->otroga_points = 0;

        $matchpoint->round_points = self::calculateWinnerPoints($finals) * $main_coeficient;

        $matchpoint->save();

        return $matchpoint->round_points;
    }

    /**
     * @param $match \Illuminate\Database\Eloquent\Collection
     * @param $which_player string
     * @return int
     */
    private function calculateOtorgaPoints($match, $which_player)
    {
        if ($match->team1_id == 0 OR $match->team2_id == 0)
            return 0;

        if ($match->team1_score > $match->team2_score AND $which_player == 'team1_data')
            return ($match->team2_data->player->ranking)
                ? self::getPlayerPoints($match->team2_data->player->ranking, $match->draw->draw_gender) : 0;
        elseif ($match->team1_score < $match->team2_score AND $which_player == 'team2_data')
            return ($match->team1_data->player->ranking)
                ? self::getPlayerPoints($match->team1_data->player->ranking, $match->draw->draw_gender) : 0;

        return 0;

    }

    /**
     * return how much player is worth in match
     * @param $ranking int
     * @param $gender string
     * @return int
     */
    private function getPlayerPoints($ranking, $gender)
    {
        $additional_points = \AdditionalPointsConfiguration::where('ranking_from', '<', $ranking)
            ->where('ranking_to', '>', $ranking)
            ->whereGender(strtoupper($gender))
            ->first();


        return ($additional_points) ? $additional_points->additional_points : 0;
    }

    /**
     * calculate how many points round is worth
     * @param $match \Illuminate\Database\Eloquent\Collection
     * @param $matches \Illuminate\Database\Eloquent\Collection
     * @return int
     */
    private function calculateRoundPoints($match, $matches)
    {
        if ($match->team1_id == 0 OR $match->team2_id == 0)
            return 0;

        if ($match->round_number == 1)
            return 1;

        $matches = $matches->filter(function ($item) use ($match) {
            if ($item->list_type == $match->list_type)
                return true;
            elseif ($item->list_type == 0 AND $match->consolation_type == $item->consolation_type)
                return true;
        });

        $players_per_round = array_count_values($matches->lists('round_number'));

        $players_per_round = array_map(function ($item) {
            return $item * 2;
        }, $players_per_round);

        $key = $this->round_keys[$players_per_round[$match->round_number]];
        $points_table = \RankingPoint::whereStars($match->draw->level)->first()->toArray();

        return $points_table[$key];
    }

    /**
     * return winner points based on draw level
     * @param $match \Illuminate\Database\Eloquent\Collection
     * @return int
     */
    private function calculateWinnerPoints($match)
    {
        $points_table = \RankingPoint::whereStars($match->draw->level)->first()->toArray();

        return $points_table["w"];
    }


    public function getPlayersRoundStatus($players, $stars)
    {
        $points_table = \RankingPoint::whereStars($stars)->first()->toArray();
        $players = array_reverse($players, true);
        $keys = ['w', 'f', 'sf', 'qf', 'r16', 'r32', 'r64', 'r128'];
        $keys_to_take = array_slice($keys, 0, count($players));
        $reversed_players = array_combine($keys_to_take, $players);


        foreach ($reversed_players as $key => $match) {
            $item = [];
            foreach ($match as $competitor) {
                if ($key == 'w') {
                    $item[] = [
                        'team_id' => $competitor[0]['id'],
                        'earned_points' => $points_table[$key],
                    ];
                } else {
                    $score1 = (isset($competitor[0]['score'])) ? $competitor[0]['score'] : 0;
                    $score2 = (isset($competitor[1]['score'])) ? $competitor[1]['score'] : 0;

                    if ($score1 == $score2)
                        $score1 = 3;

                    if (self::parseFinalWinner($competitor[0]['matchid'])) {
                        if (is_numeric($competitor[1]['id'])) {
                            $item[] = [
                                'team_id' => $competitor[1]['id'],
                                'earned_points' => $points_table[$key],
                            ];

                        }
                    } else {
                        if (is_numeric($competitor[0]['id'])) {
                            $item[] = [
                                'team_id' => $competitor[0]['id'],
                                'earned_points' => $points_table[$key],
                            ];
                        }
                    }
                }
            }
            $result[$key] = $item;
        }

        return $result;
    }

    public function parseFinalWinner($match_id)
    {
        $scores = \MatchScore::whereMatchId($match_id)->get();
        $final1_score = 0;
        $final2_score = 0;

        foreach ($scores as $score) {
            $final1_score += ($score->set_team1_score > $score->set_team2_score) ? 1 : 0;
            $final2_score += ($score->set_team1_score < $score->set_team2_score) ? 1 : 0;
        }

        return ($final1_score > $final2_score) ? true : false;
    }

    public function saveTournamentHistory($players, $stars, $tournament_id, $draw_id)
    {
        $histories = \TournamentHistory::whereTournamentId($tournament_id)->where("draw_id", $draw_id)->get();

        if (count($histories)) {
            foreach ($histories as $hist) {
                $hist->delete();
            }
        }

        $players_round_status = self::getPlayersRoundStatus($players, $stars);

        foreach ($players_round_status as $key => $teams) {
            foreach ($teams as $team) {
                $players = self::getPlayerId($team['team_id']);
                if ($players) {
                    foreach ($players as $player) {

                        \TournamentHistory::create([
                            'tournament_id' => $tournament_id,
                            'draw_id' => $draw_id,
                            'player_id' => $player,
                            'team_id' => $team['team_id'],
                            'points_earned' => $team['earned_points'],
                            'round_achieved' => $key,
                        ]);
                    }
                }
            }
        }
    }

    public function getPlayerId($team_id)
    {
        $licence_numbers = \TournamentTeam::leftJoin('players', 'players.licence_number', '=', 'tournament_teams.licence_number')
            ->where('tournament_teams.team_id', '=', $team_id)
            ->get()
            ->toArray();

        return (count($licence_numbers)) ? array_pluck($licence_numbers, 'id') : false;

    }

    /**
     * generate new ranking list
     * @param $history_collection collection
     * @param $gender string
     */
    public function newRankList($history_collection, $gender)
    {
        if (count($history_collection)) {
            // save new points from tournament_history table
            // and update history table so we know when points are assigned
            self::saveNewPoints($history_collection);

            // calculate new ranking and ranking move for global ranking
            self::calculateGlobalRankingMove($gender);

            // calculate new ranking and ranking move for clubs ranking
//            self::calculateClubRankingMove($gender);

            // calculate new ranking and ranking move for regional ranking
//            self::calculateRegionalRankingMove($gender);
        }


    }

    /**
     * save from tournament history table new points for players
     * and update tournament_history table
     * @param $histories
     * @return void
     */
    private function saveNewPoints($histories)
    {
        foreach ($histories as $history) {
            $player = \Player::find($history->player_id);

            $player->ranking_points = ($player->ranking_points)
                ? $player->ranking_points + $history->points_earned
                : 0 + $history->points_earned;

            $player->save();

            $history->points_assigned = true;
            $history->save();
        }
    }

    /**
     * calculate new ranking and ranking move for all players that have ranking points
     * @param $gender
     * @return void
     */
    public function calculateGlobalRankingMove($gender, $weekly_recalculation = true)
    {
        // prepare temp variables for processing
        $rank = 1;
        $temp_points = 0;
        $rank_count = 1;

        $players = \Player::with('old_ranking')
            ->whereHas('contact', function ($query) use ($gender) {
                $query->where('sex', $gender);
            })
            ->whereRaw('ranking_points NOTNULL')
            ->orderBy('ranking_points', 'desc')
            ->get();

        $update_arr = [];
        $insert_arr = [];
        foreach ($players as $player) {
            if (!self::checkPlayerForRankingMove($player))
                continue;

            // check if previous player points are same as current player points
            // if they are keep same rank
            // else set new rank and update rank counter. ex: 1,1,1,4,4,6,7,8...
            if ($temp_points == $player->ranking_points) {
                $ranking = $rank;
            } else {
                $ranking = $rank_count;
                $rank = $rank_count;
            }

            // store ranking points for check
            $temp_points = $player->ranking_points;

            //calculate ranking move
            if ($player->old_ranking->ranking == 0 OR is_null($player->old_ranking->ranking_points)) {
                $ranking_move = 0;
            } else {
                $ranking_move = $player->old_ranking->ranking - $ranking; // 10 - 20 = -10
            }

            //create update array for updating players table
            // and create insert_arr for inserting data to player_ranking_history
            $update_arr[] = '(' . $player->id . ', ' . $ranking . ', ' . $ranking_move . ')';
            $time = \DateTimeHelper::GetPostgresDateTime();
            $insert_arr[] = "(" . $player->id . ", " . $ranking . ", " . $ranking_move . ", " . $player->ranking_points . ", '" . $time . "', '" . $time . "')";

            //update internal counter
            $rank_count++;
        }

        $imploded = implode(',', $update_arr);

        if ($weekly_recalculation) {
            // Update players table with new data
            $update_sql = "update players as player
					set ranking = s.ranking, ranking_move = s.ranking_move
					from
					(
					  values" . $imploded . "
					) as s(id, ranking, ranking_move)
					where player.id = s.id";
        } else {
            $update_sql = "update players as player
					set quarter_ranking = s.ranking, quarter_ranking_move = s.ranking_move
					from
					(
					  values" . $imploded . "
					) as s(id, ranking, ranking_move)
					where player.id = s.id";
        }
//echo \Debug::vars($update_sql);die;
        \DB::statement($update_sql);

        if ($weekly_recalculation) {// Store new data to player_ranking_history table
            \DB::connection('pgsql2')
                ->statement('insert into player_ranking_history (player_id, ranking, ranking_move, ranking_points, created_at, updated_at) values ' . implode(',', $insert_arr));
        } else {
            \DB::connection('pgsql2')
                ->statement('insert into player_quarter_ranking_history (player_id, ranking, ranking_move, ranking_points, created_at, updated_at) values ' . implode(',', $insert_arr));
        }

    }

    /**
     * calculate new ranking and ranking move for clubs ranking
     * @param $gender
     * @return void
     */
    public function calculateClubRankingMove($gender)
    {
        // prepare temp variables for processing
        $rank = 1;
        $temp_points = 0;
        $rank_count = 1;
        $club_id = 1;

        $players = \Player::with('old_ranking')
            ->whereHas('contact', function ($query) use ($gender) {
                $query->where('sex', $gender);
            })
            ->whereRaw('ranking_points NOTNULL')
            ->where('club_id', '>', 0)
            ->orderBy('club_id')
            ->orderBy('ranking_points', 'desc')
            ->get();

        $update_arr = [];
        $insert_arr = [];

        foreach ($players as $player) {
            if (!self::checkPlayerForRankingMove($player))
                continue;

            // check if club_id is different from player club_id
            // if it is reset rank and rank_count variables
            // alse set new club_id to be same as player club_id
            if ($club_id != $player->club_id) {
                $club_id = $player->club_id;
                $rank = 1;
                $rank_count = 1;
            }

            // check if previous player points are same as current player points
            // if they are keep same rank
            // else set new rank and update rank counter. ex: 1,1,1,4,4,6,7,8...
            if ($temp_points == $player->ranking_points) {
                $club_ranking = $rank;
            } else {
                $club_ranking = $rank_count;
                $rank = $rank_count;
            }

            // store ranking points for check
            $temp_points = $player->ranking_points;

            //calculate ranking move
            if ($player->old_ranking->club_ranking == 0 OR $player->old_ranking->ranking_points == null) {
                $ranking_move = 0;
            } else {
                $ranking_move = $player->old_ranking->club_ranking - $club_ranking;
            }
            //create update array for updating players table
            // and create insert_arr for inserting data to player_ranking_history
            $update_arr[] = '(' . $player->id . ', ' . $club_ranking . ')';
            $time = \DateTimeHelper::GetPostgresDateTime();
            $insert_arr[] = "(" . $player->id . ", " . $club_ranking . ", " . $ranking_move . ", " . $player->ranking_points . ", '" . $time . "', '" . $time . "')";

            //update internal counter
            $rank_count++;
        }

        $imploded = implode(',', $update_arr);
        // Update players table with new data
        $update_sql = "update players as player
            set club_ranking = s.club_ranking
            from
            (
              values" . $imploded . "
            ) as s(id, club_ranking)
            where player.id = s.id";

        \DB::statement($update_sql);

        // Store new data to player_ranking_history table
        \DB::connection('pgsql2')->statement('insert into player_club_ranking_history (player_id, club_ranking, club_ranking_move, ranking_points, created_at, updated_at) values ' . implode(',', $insert_arr));

    }

    /**
     * calculate new ranking and ranking move for regional ranking
     * @param $gender
     * @return void
     */
    public function calculateRegionalRankingMove($gender)
    {
        // prepare temp variables for processing
        $rank = 1;
        $temp_points = 0;
        $rank_count = 1;
        $region_id = 1;

        $players = \Player::with('old_ranking')
            ->whereHas('contact', function ($query) use ($gender) {
                $query->where('sex', $gender);
            })
            ->whereRaw('ranking_points NOTNULL')
            ->where('federation_club_id', '>', 0)
            ->orderBy('federation_club_id')
            ->orderBy('ranking_points', 'desc')
            ->get();

        $update_arr = [];
        $insert_arr = [];

        foreach ($players as $player) {
            if (!self::checkPlayerForRankingMove($player))
                continue;

            // check if club_id is different from player club_id
            // if it is reset rank and rank_count variables
            // also set new club_id to be same as player club_id
            if ($region_id != $player->federation_club_id) {
                $region_id = $player->federation_club_id;
                $rank = 1;
                $rank_count = 1;
            }

            // check if previous player points are same as current player points
            // if they are keep same rank
            // else set new rank and update rank counter. ex: 1,1,1,4,4,6,7,8...
            if ($temp_points == $player->ranking_points) {
                $region_ranking = $rank;
            } else {
                $region_ranking = $rank_count;
                $rank = $rank_count;
            }

            // store ranking points for check
            $temp_points = $player->ranking_points;

            //calculate ranking move
            if ($player->old_ranking->regional_ranking == 0 OR $player->old_ranking->ranking_points == null) {
                $ranking_move = 0;
            } else {
                $ranking_move = $player->old_ranking->regional_ranking - $region_ranking;
            }
            //create update array for updating players table
            // and create insert_arr for inserting data to player_ranking_history
            $update_arr[] = '(' . $player->id . ', ' . $region_ranking . ')';
            $time = \DateTimeHelper::GetPostgresDateTime();
            $insert_arr[] = "(" . $player->id . ", " . $region_ranking . ", " . $ranking_move . ", " . $player->ranking_points . ", '" . $time . "', '" . $time . "')";

            //update internal counter
            $rank_count++;
        }

        $imploded = implode(',', $update_arr);

        // Update players table with new data
        $update_sql = "update players as player
                set regional_ranking = s.regional_ranking
                from
                (
                  values" . $imploded . "
                ) as s(id, regional_ranking)
                where player.id = s.id";

        \DB::statement($update_sql);

        // Store new data to player_ranking_history table
        \DB::connection('pgsql2')
            ->statement('insert into player_regional_ranking_history (player_id, regional_ranking, regional_ranking_move, ranking_points, created_at, updated_at) values ' . implode(',', $insert_arr));
    }

    private function checkPlayerForRankingMove($player)
    {
        if ($player->nationality > 0)
            return false;

        return true;
    }
}