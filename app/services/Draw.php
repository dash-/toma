<?php namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Col;
use Debug;

class Draw
{

    /**
     * @param $rank int rank of the player, best player == 1, second best player == 2
     * @param $partSize int number of total free positions. Has to be a power of 2 (e.g. 2,4,8,16,32, 64, 128)
     * @return returns the start position of the player, zero based
     */
    private function seedPlayer($rank, $partSize)
    {
        // base case, if rank == 1, return position 0
        if ($rank <= 1)
            return 0;

        // if our $rank is even we need to put the player into the right part
        // so we add half the part size to his position
        // and make a recursive call with half the $rank and half the part size
        if ($rank % 2 == 0)
            return $partSize / 2 + self::seedPlayer($rank / 2, $partSize / 2);

        // if the $rank is uneven, we put the player in the left part
        // since $rank is uneven we need to add + 1 so that it stays uneven
        return self::seedPlayer($rank / 2 + 1, $partSize / 2);
    }

    /**
     * @param $player_array array players in draw from database
     * @param $num_of_seeds int number of seeded players
     * @return returns shuffled players except seeds
     */
    public function shufflePlayers($player_array, $num_of_seeds)
    {
        // split players to seeds and standard mortals
        $final = array(
            array_splice($player_array, 0, $num_of_seeds),
            array_splice($player_array, 0, count($player_array)),
        );

        $seeds = $final[0];
        $rest = $final[1];
        shuffle($rest);

        return array_merge($seeds, $rest);
    }

    /**
     * @param $players array players in draw from database
     * @param $missing_players_num int number of players that is missing for "power of 2" calculation
     * @return returns array $players with added "bye" elements for direct passes of seeds
     */
    private function addBye($players, $missing_players_num)
    {
        for ($i = 1; $i <= $missing_players_num; $i++) {
            $players[] = array('id' => 'bye' . $i, 'rank' => '-' . $i);
        }
        return $players;
    }

    /**
     * @param $players array players in draw from database
     * @param $bye_count int number of players that is missing for "power of 2" calculation
     * @return returns array $players with positions in draw - only seeded players have fixed positions numbers
     */
    private function orderPlayers($players, $bye_count)
    {
        // this 2 lines just reposition array keys to start from 1 instead from 0
        array_unshift($players, "bye");
        unset($players[0]);

        // check if we need to add direct passes("bye") to players array
        if ($bye_count)
            $players = self::addBye($players, $bye_count);

        for ($i = 1; $i <= count($players); $i++) {
            // add positions to players - (store that position - mainly for debugging)
            $position = (self::seedPlayer($i, count($players)) + 1);
            $players[$i]['position'] = $position;
        }

        // order players in array by position so we can get pos1 vs pos2 etc
        $players = array_values(array_sort($players, function ($value) {
            return $value['position'];
        }));

        return $players;
    }

    /**
     * @param $players array array of initial signups from database
     * @param $num_seeds mixed int|boolean number of seeded players
     * @return returns $pairs array Finished tournament draw tree - multidimensional array of pairs in tournament
     */
    public function makePairs($players, $num_seeds = false)
    {
        //calculate number of seeds
        if (!$num_seeds)
            $num_seeds = self::roundUpToNextPowerOfTwo(count($players)) / 4;

        // if we receive 26 players round it up to 32
        $bye_count = self::roundUpToNextPowerOfTwo(count($players)) - count($players);

        // shuffle non seeded players
        $shuffled_players = self::shufflePlayers($players, $num_seeds);

        // take players and add them positions, ex: position1vsposition2, position3vsposition4 ...
        $ordered_players = self::orderPlayers($shuffled_players, $bye_count);
        $seed_positions = (self::isAutomaticDrawDisabled($num_seeds, count($players)))
            ? self::qualiSeedPositions(self::roundUpToNextPowerOfTwo(count($players)), $num_seeds)
            : self::seedPositions(count($players));

//        echo Debug::vars(count($ordered_players));die;
//echo Debug::vars($ordered_players);die;
        if (count($ordered_players) == 16) {

            //take last keys and shuffle
            $sub = array_splice($seed_positions, 2, 2);
            shuffle($sub);
            array_splice($seed_positions, 2, 0, $sub);
            //increate all keys by 1
            array_unshift($seed_positions, null);
            unset($seed_positions[0]);
        }
        $ordered_players = self::swapSeeds($ordered_players, count($players), [], $seed_positions);

        if ($bye_count)
            return self::swapByes($ordered_players, $players, $num_seeds, $bye_count);

        return $ordered_players;
    }

    /**
     * @param $players array array of initial signups from database
     * @param $num_seeds mixed int|boolean number of seeded players
     * @return returns $pairs array Finished tournament draw tree - multidimensional array of pairs in tournament
     */
    public function makeQualiPairs($players, $num_seeds = false)
    {
        //calculate number of seeds
        if (!$num_seeds)
            $num_seeds = self::roundUpToNextPowerOfTwo(count($players)) / 4;

        // if we receive 26 players round it up to 32
        $bye_count = self::roundUpToNextPowerOfTwo(count($players)) - count($players);

        // shuffle non seeded players
        $shuffled_players = self::shufflePlayers($players, $num_seeds);

        // take players and add them positions, ex: position1vsposition2, position3vsposition4 ...
        $ordered_players = self::orderPlayers($shuffled_players, $bye_count);
        $seed_positions = self::qualiSeedPositions(self::roundUpToNextPowerOfTwo(count($players)), $num_seeds);
//        echo Debug::vars(count($ordered_players));die;
//        if (count($ordered_players) == 16) {
//            //take last keys and shuffle
//            $sub = array_splice($seed_positions, 2, 2);
//            shuffle($sub);
//            array_splice($seed_positions, 2, 0, $sub);
//            //increase all keys by 1
//            array_unshift($seed_positions, null);
//            unset($seed_positions[0]);
//        }
//        echo Debug::vars($seed_positions);
//        if(count($seed_positions)==8 && count($ordered_players) == 64){

            $count_positions = count($seed_positions);
            $sub = array_splice($seed_positions, $count_positions / 2, $count_positions / 2);
            shuffle($sub);
            array_splice($seed_positions, $count_positions / 2, 0, $sub);
            //increase all keys by 1
            array_unshift($seed_positions, null);
            unset($seed_positions[0]);
//        }
        $ordered_players = self::swapSeeds($ordered_players, count($players), [], $seed_positions);
//        echo Debug::vars($ordered_players);die;
//        echo Debug::vars($ordered_players);
//        echo Debug::vars($seed_positions);
        if ($bye_count)
            return self::swapByes($ordered_players, $players, $num_seeds, $bye_count);
//echo Debug::vars('aaaa');die;
        return $ordered_players;
    }

    /**
     * @param $ordered_players array main array of players used for swapping
     * @param $first_players array get first players list ordered by ranking points,
     *        used to get seed_ids for swapping positions
     * @param $num_seeds integer
     * @param $bye_count integer
     * @return array $ordered_players
     */
    private function swapByes($ordered_players, $first_players, $num_seeds, $bye_count)
    {
        // store seeded players ids for swapping of "bye"s for seeds
        $temp_ids = [];
        // take only ids of seeded players
        for ($i = 0; $i < $num_seeds; $i++) {
            // very important pass PLAYERS NOT ORDERED_PLAYERS don't be stupid
            $temp_ids[] = $first_players[$i]['id'];
        }

        for ($i = 0; $i < $bye_count; $i++) {
            $bye = 'bye' . ($i + 1);

            if (isset($temp_ids[$i])) {
                //get "bye" id
                $pass = \ArrayHelper::getKeyByValue($ordered_players, $bye);

                // take seeded player id
                $seed_id = (\ArrayHelper::getKeyByValue($ordered_players, $temp_ids[$i]));
                $player_to_swap = ($seed_id % 2 == 0) ? $seed_id + 1 : $seed_id - 1;

                // swap positions of seeded enemy and pass
                \ArrayHelper::swapArrayElements($ordered_players, $pass, $player_to_swap);
            }
        }

        return $ordered_players;
    }

    private function swapSeeds($ordered_players, $players_number, $used_keys = [], $seed_positions)
    {

        foreach ($ordered_players as $key => $player) {
            if (isset($player['seeds'][0]) && (isset($player['seeds'][0]['seed_num']) && !in_array($player['seeds'][0]['seed_num'], $used_keys))) {

//                $seed_num = $seed_positions[$player['seeds'][0]['seed_num']] - 1;
                $seed_num = $seed_positions[$player['seeds'][0]['seed_num']] - 1;
                \ArrayHelper::swap($ordered_players, $key, $seed_num);
                $used_keys[] = $player['seeds'][0]['seed_num'];
                return self::swapSeeds($ordered_players, $players_number, $used_keys, $seed_positions);
            }
        }
        return $ordered_players;
    }

    /**
     * @param $num int number of currently available players
     * @return returns $num int number with next "power of two" if received 26 it will round it up to 32
     */
    public function roundUpToNextPowerOfTwo($num)
    {
        $num--;
        $num |= $num >> 1;  // handle  2 bit numbers
        $num |= $num >> 2;  // handle  4 bit numbers
        $num |= $num >> 4;  // handle  8 bit numbers
        $num |= $num >> 8;  // handle 16 bit numbers
        $num |= $num >> 16; // handle 32 bit numbers
        $num++;

        return $num;
    }

    /**
     * @param $num_of_teams int number of teams in draw
     * @return returns int number of rounds in draw
     */
    private function countNumOfRounds($num_of_teams)
    {
        return log($num_of_teams) / log(2);
    }

    /**
     * @param $pairs array pairs in draw
     * @param $draw_id int id of draw
     * @return array array of pairs by round
     */
    public function prepareTournament($pairs, $draw_id)
    {
        $num_of_pairs = count($pairs);
        $num_of_rounds = self::countNumOfRounds($num_of_pairs * 2);

        for ($i = 1; $i <= $num_of_rounds; $i++) {
            if ($i == 1)
                $rounds[$i] = $pairs;
            else {
                $num_of_pairs = $num_of_pairs / 2;
                $empty_pairs = ['player1' => $draw_id, 'player2' => $draw_id];
                $rounds[$i] = array_fill(0, $num_of_pairs, $empty_pairs);
            }
        }

        return $rounds;
    }

    /**
     * return seed positions for all players based on number of players
     * @param $players_number integer
     * @return array
     */
    private function seedPositions($players_number)
    {
        $seed_positions = \Config::get('seedPositions');

        foreach ($seed_positions as $key => $position) {
            $keys = explode('_', $key);
            if (($keys[0] <= $players_number) && ($players_number <= $keys[1]))
                return $seed_positions[$key];
        }
    }


    /**
     * return seed positions for all players based on number of players
     * @param $players_number integer
     * @param $number_of_seeds integer
     * @return array
     */
    private function qualiSeedPositions($players_number, $num_of_seeds)
    {
        
        $sections = $players_number / $num_of_seeds;
        
        $seed_positions = [];
        $seed_numbers = range(1, $num_of_seeds);

        $half_section = $sections / 2;

        // split players to seeds and standard mortals
        $separate_seed_numbers = array(
            array_splice($seed_numbers, 0, $num_of_seeds / 2),
            array_splice($seed_numbers, 0, $num_of_seeds / 2),
        );
        
        $multiplier = 2;
        foreach ($separate_seed_numbers[0] as $number) {
            $multiplier = ($number <= 2) ? $multiplier : $multiplier + 2;

            $position = ($number == 1) ? $number : ($multiplier * $sections) + 1;
            $seed_positions[$number] = $position;
        }

        shuffle($separate_seed_numbers[1]);

        $second_multiplier = 0;
        
        foreach ($separate_seed_numbers[1] as $num) {
            $second_multiplier += 2;
            $position  = $second_multiplier * $sections;
            $seed_positions[$num] = $position;
        }
//        u slucaju da bude trebalo po mailu ovo ostaviti a sve ostalo comment
//        $sections = $players_number / $num_of_seeds;
//        $seed_positions = [];
//        for($i=1; $i <= $num_of_seeds; $i++)
//        {
//            $position = $i == 1 ? $i : (($i-1) * $sections) + 1;
//            $seed_positions[$i] = $position;
//        }
        return $seed_positions;
    }

    /**
     * check if automatic draw is applicable based on rules
     * @param $num_of_seeds integer
     * @param $num_of_players integer
     * @return bool
     */
    public function isAutomaticDrawDisabled($num_of_seeds, $num_of_players)
    {
        $expected_num_of_seeds = count(self::seedPositions($num_of_players));
        return ($num_of_seeds == $expected_num_of_seeds) ? false : true;
    }
}