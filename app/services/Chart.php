<?php namespace App\Services;

class Chart {

    /**
     * method to parse scores per gem in set used in line chart on last-match-scores page
     * @param $last_match
     * @param $set_number integer
     * @param $team_id integer used for getting player in match - does he belongs to team1 or team2
     * @return array
     */
    public function scoresLine($last_match, $set_number, $team_id)
    {
        if (is_null($last_match))
            return [];

        $scores = $last_match->game_scores->filter(function ($item) use ($set_number) {
           return $item->set_number = $set_number;
        });

        $game_score = new \GameScoreDetail();

        $team1_id = $last_match->team1_id;

        $team_score = $team_id == $team1_id ? 'team1_score' : 'team2_score';
        $opposite_score = $team_score == 'team1_score' ? 'team2_score' : 'team1_score';

        $result = [];
        foreach ($scores as $score) {
            $item['name'] = 'Gem ' . $score->game_number;
//            $tooltip = '';
            $score_line = []; // reset score line between gems
            $line_counter = 0;
            $last_point = 0; // temp for calculation of chart line
            foreach ($score->details as $point) {
//                $tooltip = $game_score->displayScore($point->$team_score).':'.$game_score->displayScore($point->$opposite_score);
                if ($point->$team_score == 0 AND $point->$opposite_score == 0)
                    $score_line[] = 0;
                else {
                    if ($point->$team_score > $point->$opposite_score) {
                        if ($point->$team_score > $last_point)
                            $line_counter += 1;
                        else
                            $line_counter += -1;
                    } else {
                        if ($point->$team_score > $last_point)
                            $line_counter += 1;
                        else
                            $line_counter += -1;
                    }
                    $score_line[] = $line_counter;
                    $last_point = $point->$team_score;
                }
//                $item['tooltipText'] = $tooltip;

            }

            if ($score->game_number != 1)
                $item['visible'] = false;
            $item['data'] = $score_line;

            $result[] = $item;
        }
        return $result;
    }

}