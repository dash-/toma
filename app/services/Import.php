<?php namespace App\Services;
 use Debug;
class Import {

    /**
     * Parse input string and save signup to database
     *
     * @param  mixed $input
     * @param  collection $draw
     * @return array counters for response msg
     */
    public function parseInputAndSave($input, $draw, $isFile = FALSE)
    {
        $bulk = [];

        if (!$isFile) { 
            $bulk = self::stringToArray($input);
            
        } else {
            $bulk = self::parseCsv($input);
        }
        
        $insert_count = 0;
        $import_list = $bulk = array_filter($bulk);

        $bulk_count = count($bulk);
        
        if ($bulk_count) {
            $range_from = $draw->category->range_from;
            $range_to = $draw->category->range_to;
            $draw_gender = $draw->draw_gender;


            $players = \Player::with('contact')->whereIn('licence_number', $bulk)
                ->get();
            // prepare arrays to populate data    
            $licence_fails = [];
            foreach ($players as $player) {
                if(($key = array_search($player->licence_number, $import_list)) !== false) {
                    unset($import_list[$key]);
                }
                $years = \DateTimeHelper::GetDifferenceInTime($player->contact->date_of_birth);
                $years_check = $years >= $range_from && $years <= $range_to;
                $tournament_type_check = true;

                if ($draw->tournament->tournament_type == 2) {
                    if ($player->federation_club_id != $draw->tournament->getRegionId())
                        $tournament_type_check = false;
                } else if ($draw->tournament->tournament_type == 3) {
                    if ($player->province_club_id != $draw->tournament->getProvinceId()) {
                        $tournament_type_check = false;
                    }
                }

                // check if player already signed up for this draw
                $signup_check = \TournamentSignup::leftJoin('tournament_teams', 'tournament_signups.id', '=', 'tournament_teams.team_id')
                    ->whereDrawId($draw->id)
                    ->whereLicenceNumber($player->licence_number)
                    ->first();

                $sex_check = strtoupper($player->contact->sex) == strtoupper($draw_gender);

                // Validation
                // First check if player gender is equal to draw gender
                // Second check if player years are in draw age category
                // Third check if player already signed up for this draw
                // Fourth check if player belongs to tournament type, only if tournament type different from RFET (1)
                if ($sex_check AND $years_check AND !$signup_check AND $tournament_type_check) {
                    self::saveData($draw, $player);
                    // update insert counter
                    $insert_count++;
                } else {
                    // save not inserted licence numbers to array
                    $item['player_id'] = $player->id;
                    $item['licence_number'] = $player->licence_number;
                    $item['full_name'] = $player->contact->full_name();

                    if (!$sex_check)
                        $item['reason'] = \LangHelper::get('player_is_not_belonging_to_sex_category', 'Player is not belonging to sex category');
                    elseif (!$years_check)
                        $item['reason'] = \LangHelper::get('player_is_not_belonging_to_draw_category', 'Player is not belonging to draw category');
                    elseif ($signup_check)
                        $item['reason'] = \LangHelper::get('player_has_already_signed_up', 'Player has already signed up');
                    elseif (!$tournament_type_check)
                        $item['reason'] = \LangHelper::get('player_does_not_belong_to_region_federation_of_tournament', 'Player does not belong to region/federation of tournament');

//                    echo Debug::vars($item);die;
                    $licence_fails[] = $item;
                }
            }

            //show licence numbers that doesn't exist
            if($import_list){
                $not_valid_licence_fails = array();
                foreach($import_list as $lic)
                {
                    $item2['licence_number'] = $item2['player_id'] = '';
                    $item2['full_name'] = $lic;
                    $item2['reason'] = \LangHelper::get('player_with_licence_does_not_exist', 'Player with this licence number does not exists');
                    $not_valid_licence_fails[] = $item2;
                }
                $licence_fails = array_merge($licence_fails, $not_valid_licence_fails);
            }
        }

        return [
            'insert_count'  => $insert_count,
            'bulk_count'    => $bulk_count,
            'licence_fails' => $licence_fails,
        ];
    }

    /**
    *
    * @param string $input
    * @return array
    **/
    private function stringToArray($input)
    {
        if(strstr($input, PHP_EOL)) {
            return array_map("trim", explode("\n", $input));
        } else {
            return array_map("trim", explode(";", $input));
        }
    }

    /**
    *
    * @param string $input - file path to csv
    * @return array
    **/
    private function parseCsv($filepath)
    {
        if(!file_exists($filepath) || !is_readable($filepath))
                return [];

        $input = file_get_contents($filepath);
        
        $array = self::stringToArray($input);

        // delete file from storage
        unlink($filepath);
        
        return $array;
    }

    /**
    *
    * @param collection $draw
    * @param collection $player
    * @return void
    **/
    private function saveData($draw, $player, $list_type = null)
    {
        // Insert into signups table player basic info with draw details
        $signup = \TournamentSignup::create([
            'submited_by'    => \Auth::getUser()->id,
            'tournament_id'  => $draw->tournament_id,
            'draw_id'        => $draw->id,
            'created_at'     => \DateTimeHelper::GetDateNow(),
            'rank'           => ($player->ranking) ? $player->ranking : 0,
            'ranking_points' => ($player->ranking_points) ? $player->ranking_points : 0,
            'list_type'      => $list_type,
        ]);

        // // Insert into tournament_teams table player info
        \TournamentTeam::create([
            'licence_number'  => $player->licence_number,
            'name'            => $player->contact->name,
            'surname'         => $player->contact->surname,
            'date_of_birth'   => \DateTimeHelper::GetPostgresDateTime($player->contact->date_of_birth),
            'email'           => $player->contact->email,
            'phone'           => $player->contact->phone,
            'current_ranking' => ($player->ranking) ? $player->ranking : 0,
            'ranking_points'  => ($player->ranking_points) ? $player->ranking_points : 0,
            'team_id'         => $signup->id,
        ]);
    }


    /**
     * save signups from licence_numbers array
     * @param $licence_numbers array
     * @param $draw collection
     * @param $list_type mixed
     * return void
     */
    public function saveSignupsFromArray($licence_numbers, $draw, $list_type = null)
    {
        $players = \Player::with('contact')->whereIn('licence_number', $licence_numbers)
            ->get();

        foreach ($players as $player) {
            self::saveData($draw, $player, $list_type);
        }
    }

    /**
     * method for generating array for separating signups data for pairs
     * @param $items array
     * @param $draw_id integer
     * @param $signups_ids array - signups_ids for getting right seed number
     * @param $list_type integer
     * @return $data array
     */
    public function generateRoundsFromFile($items, $draw_id, $signup_ids, $list_type)
    {
        $counter = 1;

        foreach ($items as $key => $input) {
            if ($key % 2 != 0) {
                $round_num = $counter++;
            } else {
                $round_num = $counter;
            }

            $item['id'] = array_search($input[0], $signup_ids);
            $item['round_num'] = $round_num;

            $seed_num = (isset($input[1]) && !empty($input[1])) ? $input[1] : 0;

            if ($seed_num > 0) {
                \DrawSeed::create([
                    'draw_id'   => $draw_id,
                    'signup_id' => $item['id'],
                    'list_type' => $list_type,
                    'seed_num'  => $seed_num,
                ]);
            }

            $data[$item['round_num']][] = $item;
        }

        return $data;
    }

    /**
     * generate matches for draw based on $rounds_preparation data
     * @param $rounds_preparation array
     * @param $list_type integer
     * @param $draw_id integer
     * @return void
     */
    public function generateMatches($rounds_preparation, $list_type, $draw_id)
    {
        foreach ($rounds_preparation as $key => $rounds)
        {
            $counter = 1;
            foreach ($rounds as $round)
            {
                \DrawMatch::create([
                    'team1_id'       => (!isset($round[0]['id'])) ? 0 : $round[0]['id'],
                    'team2_id'       => (!isset($round[1]['id'])) ? 0 : $round[1]['id'],
                    'draw_id'        => $draw_id,
                    'list_type'      => $list_type,
                    'round_position' => $counter++,
                    'round_number'   => $key,
                ]);
            }
        }

        $matches = \DrawMatch::where(function ($query) {
            $query->where('team1_id',  0)
                ->orWhere('team2_id', 0);
        })
            ->where('draw_id', $draw_id)
            ->whereRoundNumber(1)
            ->whereListType($list_type)
            ->orderBy('id')
            ->get();


        if (count($matches))
            \DrawMatchHelper::setUpInitialScores($matches, $draw_id, $list_type);
    }

    /**
     * @param $input array
     * @return array $items
     */
    public function parseStrAsCsv($input)
    {
        $imported_string = str_getcsv($input, "\n"); //parse the rows
        foreach($imported_string as $d) {
            $items[] = preg_split('/\s+/', $d);
        }

        return $items;
    }

    /**
     * method to check if player belongs to tournament type
     * if tournament type is different from 1 (RFET) check if player belongs to region/province
     * @param $player Collection
     * @param $tournament Collection
     * @return mixed string | boolean
     */
    public function checkTournamentType($player, $tournament)
    {
        if ($tournament->tournament_type == 2) {
            if ($player->federation_club_id != $tournament->getRegionId())
                return \LangHelper::get('player_does_not_belong_to_federation', 'Player does not belong to federation');
        } else if ($tournament->tournament_type == 3) {
            if ($player->province_club_id != $tournament->getProvinceId()) {
                return \LangHelper::get('player_does_not_belong_to_province', 'Player does not belong to province');
            }
        }

        return false;

    }
}