<?php namespace Rfet;

use Illuminate\Support\ServiceProvider;

/**
 * Class RfetServiceProvider
 * @package Rfet
 */
class RfetServiceProvider extends ServiceProvider {

    /**
     *  Let's do some bindings here
     */
    public function register()
    {
        $this->app->bind( 'Rfet\\Storage\\User\\UserRepositoryInterface', 'Rfet\\Storage\\User\\EloquentUserRepository' );
    }

}