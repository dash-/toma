<?php namespace Rfet\Storage\User;

use Rfet\Exceptions\NotFoundException;
use User, Auth;
 
class EloquentUserRepository implements UserRepositoryInterface {
 
	public function all()
	{
		return User::all();
	}

	public function find($id)
	{
		$data = User::find($id);

		if (!$data)
            throw new \Exception("User not found");

        return $data;
	}

	public function create($input)
	{
		return User::create($input);
	}

	public function allWithRoles()
	{
		$data = \User::with('roles','region')->where('id', '<>', Auth::user()->id)->get();

		if (!$data)
            throw new NotFoundException();

        return $data;

	}
 
}