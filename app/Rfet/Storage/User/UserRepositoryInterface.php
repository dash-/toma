<?php namespace Rfet\Storage\User;
 
interface UserRepositoryInterface {
   
  public function all();
 
  public function find($id);
 
  public function create($input);
  
  public function allWithRoles();
 
}