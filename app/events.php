<?php 

Event::listen('auth.login', function($user) {
    $user->last_login = new DateTime;
    $user->save();

    $history = new LoginHistory;
    $history->save(array('user_id' => $user->id));
});

Event::listen('generic.event',function($client_data){
    return BrainSocket::message('generic.event',array('message'=>'A message from a generic event fired in Laravel!', 'data' => $client_data));
});

Event::listen('notification.event',function($client_data){
    return BrainSocket::message('notification.event',array('message'=>'Notification event fired', 'data' => $client_data));
});

Event::listen('messagebox.event',function($client_data){
    return BrainSocket::message('messagebox.event',array('message'=>'Message box event fired', 'data' => $client_data));
});

Event::listen('app.success',function($client_data){
    return BrainSocket::success(array('There was a Laravel App Success Event!'));
});

Event::listen('app.error',function($client_data){
    return BrainSocket::error(array('There was a Laravel App Error!'));
});

Event::listen('live_score.event',function($client_data){
    return BrainSocket::message('live_score.event',array('message'=>'A message from a generic event fired in Laravel!', 'data' => $client_data));
});