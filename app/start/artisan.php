<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/

Artisan::add(new CronJob);
Artisan::add(new RecalculatePoints);
Artisan::add(new FakerAdd);
Artisan::add(new FakerRemove);
Artisan::add(new AddClubsLocation);
Artisan::add(new ExportRankings);
Artisan::add(new ScheduledTasks);
Artisan::add(new AssignPoints);
Artisan::add(new AddRegionsLocation);
Artisan::add(new CalculateMove);
Artisan::add(new BackupDatabase);
Artisan::add(new CheckStatus);

