<?php

$zone = 'EU';
$values = array();

switch ($zone) {
    case "US":
        $values = array(
            'shortDate'         => 'm/d/y',
            'shortDateFullYear' => 'm/d/Y',
            'fullDateTime'      => 'd M Y h:ia',
            'fullShortDateTime' => 'm/d/Y h:ia',
            'onlyTime'          => 'h:ia',
            'onlyDate'          => 'd.m.Y',
            'postgresDateTime'  => 'Y-m-d H:i:s',
        );
        break;

    case "EU":
        $values = array(
            'shortDate'         => 'd/m/y',
            'shortDateFullYear' => 'd/m/Y',
            'fullDateTime'      => 'd M Y H:i',
            'onlyTime'          => "H:i",
            'onlyDate'          => 'd.m.Y',
            'postgresDateTime'  => 'Y-m-d H:i:s',
        );
        break;
}

return $values;