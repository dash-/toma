<?php

return array(
    'default' => 'pgsql',
    'connections' => array(

        'pgsql' => array(
            'driver' => 'pgsql',
            'host' => '127.0.0.1',
            'database' => 'rfet_demo_17122014',
            'username' => 'postgres',
            'password' => 'sa',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
        ),

        'pgsql2' => array(
            'driver' => 'pgsql',
            'host' => '127.0.0.1',
            'database' => 'rfet_demo_17122014',
            'username' => 'postgres',
            'password' => 'sa',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'ranking_history',
        ),

        'scoring_connection' => array(
            'driver' => 'pgsql',
            'host' => '127.0.0.1',
            'database' => 'rfet_demo_17122014',
            'username' => 'postgres',
            'password' => 'sa',
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'scoring',
        ),

    ),



//    'elastic' => [
//        'hosts' => [ "127.0.0.1:9200" ],
//        'log_file' => "logs/elasticsearch.log"
//    ]
);