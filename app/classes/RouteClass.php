<?php

class RouteClass {

	public static function getParams($type = 'controller')
	{
		$routeArray = Str::parseCallback(Route::currentRouteAction(), null);
	 
		if (last($routeArray) != null) {
			// Remove 'controller' from the controller name.
			$controller = str_replace('Controller', '', class_basename(head($routeArray)));
	 
			// Take out the method from the action.
			$action = str_replace(['get', 'post', 'patch', 'put', 'delete'], '', last($routeArray));
	 	
			return ($type == 'controller') ? Str::slug($controller) : Str::slug($action); 
		}
	 
		return 'closure';
	}

}