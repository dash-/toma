<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', array('before' => 'auth',  'uses' => 'IndexController@getIndex'));

Route::get('login', array('before' => 'guest', 'uses' => 'AuthController@getLogin'));
Route::post('login', array('uses' => 'AuthController@postLogin'));

Route::get('password-reset', array('before' => 'guest', 'uses' => 'AuthController@getPasswordReset'));
Route::post('password-reset', array('uses' => 'AuthController@postPasswordReset'));

Route::get('auth/change-password', array('before' => 'guest', 'uses' => 'AuthController@getChangePassword'));
Route::post('auth/change-password', array('uses' => 'AuthController@postChangePassword'));

Route::get('signup', array('before' => 'guest', 'uses' => 'AuthController@getSignup'));
Route::post('signup', array('uses' => 'AuthController@postSignup'));

Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));


Route::group(array('prefix' => 'superadmin', 'before' => 'auth'), function() {
	Route::controller('users', 'Superadmin\UsersController', array(
		'getIndex'   => 'users',
		'getInfo'    => 'users/info',
		'getCreate'  => 'users/create',
		'postCreate' => 'users/create',
		'getEdit'    => 'users/edit',
		'putEdit'    => 'users/edit',
		'deleteUser' => 'users/delete',
		'getPlayers' => 'users/players',
		'postSearch' => 'users/search',
	));

});


Route::group(array('before' => 'auth'), function ()
{
    Route::controller('index', 'IndexController');
    Route::controller('tournaments', 'TournamentsController', array(
        'getIndex'   => 'tournaments',
        'getInfo'    => 'tournaments/info',
        'getCreate'  => 'tournaments/create',
        'postCreate' => 'tournaments/create',
        'getEdit'    => 'tournaments/edit',
        'putEdit'    => 'tournaments/edit',
        'deleteTournament' => 'tournaments/delete',
        'getCreateEventOrganizer'  => 'tournaments/createEventOrganizer',
    ));

    Route::controller('players', 'PlayersController', array(
		'getIndex' => 'players',
		'Data'     => 'players/data',
    ));

    Route::controller('profile', 'ProfileController');
    Route::controller('messages', 'MessagesController');
    Route::controller('notifications', 'NotificationsController');
    Route::controller('regions', 'RegionsController');
    Route::controller('calendar', 'CalendarController');
    Route::controller('contacts', 'ContactsController');
    Route::controller('signups', 'SignupsController');
    Route::controller('clubs', 'ClubsController');
    Route::controller('draws', 'DrawsController');
    Route::controller('settings', 'SettingsController');
    Route::controller('rankings', 'RankingsController');
    Route::controller('scores', 'ScoreController');
    Route::controller('sponsors', 'SponsorsController');
    Route::controller('bracketprint', 'BracketPrintController');
    Route::controller('referees', 'RefereesController');
    Route::controller('match', 'MatchController');
    Route::controller('invoices', 'InvoicesController');
    Route::controller('schedule', 'ScheduleController');
    Route::controller('external', 'ExternalTournamentsController');
    Route::controller('videos', 'VideosController');

});

Route::controller('tournament', 'P\TournamentController');

//show schedule partial on public site
Route::get("/p/schedule/show-schedule/{tournament_id}", array('uses'=>'P\ScheduleController@getShowSchedule'));

Route::group(array('prefix' => '{tournament_id}'), function() {
    Route::controller('tournament', 'P\TournamentController', [
        'getIndex' => 'default_public',
    ]);
    Route::controller('schedule', 'P\ScheduleController');
    Route::controller('draws', 'P\DrawsController');
    Route::controller('player', 'P\PlayerController');
    Route::controller('signups', 'P\SignupsController');
    Route::controller('scores', 'P\ScoresController');
    Route::controller('news-admin', 'P\NewsAdminController');
    Route::controller('message-box', 'P\MessageBoxController');
    Route::controller('news', 'P\NewsController');
    Route::controller('gallery-admin', 'P\GalleryAdminController');
    Route::controller('gallery', 'P\GalleryController');
    Route::controller('user', 'P\UserController');
    Route::controller('rankings', 'P\RankingsController');
    Route::controller('calendar', 'P\CalendarController');

});


Route::group(array('prefix' => 'referee', 'before' => 'auth'), function() {
    Route::controller('tournaments', 'Referee\TournamentsController');
});

Route::group(array('prefix' => 'player', 'before' => 'auth'), function() {
    Route::controller('', 'Player\IndexController');
});


