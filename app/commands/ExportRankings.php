<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExportRankings extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'export:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export rankings to Excel or PDF.';


    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        DB::connection()->disableQueryLog();

        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 1000000);


        $export_name = 'Exported Rankings ' . date('Y-m-d');
        $genders = [ 1 => 'M', 2 => 'F'];

            $sheet = new \ExportRanking;
            $sheet->document_name = $export_name;
            $sheet->doc_type = 1;
            $sheet->save();

            \Excel::create($export_name, function ($excel) {

                $excel->sheet('Ranking', function ($sheet) {

                    $players = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id')
                        //TODO: provjeriti logiku za export rankinga(ispisivati i negativni ranking?)
                        ->where('ranking_points', '>', 0)
                        ->whereNotNull("ranking_points")
                        ->orderBy('ranking_points', 'DESC')
                        ->get(array('surname AS Surname', 'name AS Name', 'sex AS Sex', 'date_of_birth AS DOB',
                        'licence_number AS Licence', 'province_club_id AS ProvinceID', 'federation_club_id AS FederationID',
                        'club_id AS ClubID', 'ranking_points AS Points', 'ranking As Ranking', 'regional_ranking AS Regional',
                        'province_ranking AS Province', 'club_ranking AS Club', 'ranking_move AS Move'));

                    $sheet->cells('A1:O1', function ($cells) {

                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                    });

                    $sheet->cells('A1:' . 'O' . (count($players) + 1), function ($cells) {

                        $cells->setAlignment('left');
                        $cells->setValignment('middle');
                    });


                    $sheet->fromArray($players);

                });

            })->store('xlsx');

            foreach($genders as $key => $gender) {
                $sheet = new \ExportRanking;
                $sheet->document_name = $export_name.' - '.$gender;
                $sheet->doc_type = $gender == 'M' ? 3 : 4;
                $sheet->save();

                \Excel::create($export_name.' - '.$gender, function ($excel) use ($gender) {

                    $excel->sheet('Ranking', function ($sheet) use ($gender) {

                        $players = Contact::leftJoin('players', 'players.contact_id', '=', 'contacts.id')
                            //TODO: provjeriti logiku za export rankinga(ispisivati i negativni ranking?)
                            ->where('ranking_points', '>', 0)
                            ->where('sex', $gender)
                            ->whereNotNull("ranking_points")
                            ->orderBy('ranking_points', 'DESC')
                            ->limit(10000)
                            ->get(array('surname AS Surname', 'name AS Name', 'sex AS Sex', 'date_of_birth AS DOB',
                                'licence_number AS Licence', 'province_club_id AS ProvinceID', 'federation_club_id AS FederationID',
                                'club_id AS ClubID', 'ranking_points AS Points', 'ranking As Ranking', 'regional_ranking AS Regional',
                                'province_ranking AS Province', 'club_ranking AS Club', 'ranking_move AS Move'));

                        $sheet->cells('A1:O1', function ($cells) {

                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        });

                        $sheet->cells('A1:' . 'O' . (count($players) + 1), function ($cells) {

                            $cells->setAlignment('left');
                            $cells->setValignment('middle');
                        });


                        $sheet->fromArray($players);

                    });

                })->store('pdf');
            }

        DB::connection()->enableQueryLog();
        echo "PDF/Excel exported\n";
    }


    protected function getArguments()
    {
        return array(
            array('doc_type', InputArgument::OPTIONAL, 'An doc type argument (xlsx or pdf).')
        );
    }
}
