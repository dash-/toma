<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CalculateMove extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'calculatemove:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for calculating move of the players.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 25009200);

        $args = $this->argument();
        // type 0 - weekly, 1 - quarter
        $weekly_recalculation = !(isset($args['type']) && $args['type'] == 1);
        // calculate ranking moves for MALE players
        // calculate ranking moves for FEMALE players

        \PointsCalculator::calculateGlobalRankingMove('M', $weekly_recalculation);
        \PointsCalculator::calculateGlobalRankingMove('F', $weekly_recalculation);

        if (!$weekly_recalculation) {
            \PointsCalculator::calculateClubRankingMove('M');
            \PointsCalculator::calculateClubRankingMove('F');

            \PointsCalculator::calculateRegionalRankingMove('M');
            \PointsCalculator::calculateRegionalRankingMove('F');
        }
        echo "Calculation is done\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('type', InputArgument::OPTIONAL, '1 - quarter recalculation'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
//	protected function getOptions()
//	{
//		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
//		);
//	}

}
