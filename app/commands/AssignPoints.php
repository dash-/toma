<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AssignPoints extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'assignpoints:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for generating points table from completed tournaments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 259200);

        $args = $this->argument();
        $email = new EmailClass();
        if (isset($args['email']) && $args['email'] != '')
            $email->to = $args['email'];
        else
            $email->to = Config::get("app.admin_email");

        $email->subject = "Points assign";

        $tournaments = \Tournament::with(['draws' => function ($query) {
            $query->where('status', 5);
        }])->where('status', 5)
            ->where('points_assigned', false)
            ->where('validated', true)
            ->get();

        foreach ($tournaments as $tournament) {
            foreach ($tournament->draws as $draw) {
                if (!$draw->is_prequalifying)
                    \PointsCalculator::drawPointsAssign($draw->id);
            }
            $tournament->points_assigned = true;
            $tournament->save();
        }

        DB::Table("cron_job_finished")->insert(array("ran" => 1, "action_done" => "assign_points", 'created_at' => DateTimeHelper::GetDateNow(), 'updated_at' => DateTimeHelper::GetDateNow()));

        echo "Assign points - Done\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('email', InputArgument::OPTIONAL, 'An email argument.')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
//	protected function getOptions()
//	{
//		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
//		);
//	}

}
