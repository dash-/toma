<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RecalculatePoints extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'recalculate:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for recalculation of points.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        echo Debug::vars("temporary disabled");die;
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 25009200);

        $args = $this->argument();
        // type 0 - weekly, 1 - quarter
        $weekly_recalculation = isset($args['type']) && $args['type'] == 0;


        $email = new EmailClass();
        if (isset($args['email']) && $args['email'] != '')
            $email->to = $args['email'];
        else
            $email->to = Config::get("app.admin_email");

        $email->subject = ($weekly_recalculation ? "Weekly" : "Quarter") . " recalculation";
        Artisan::call("assignpoints:do");

        // clear temporary_rankings table and populate it again so we can check ranking move for every player
        DB::table('temporary_rankings')->truncate();
        DB::statement('INSERT INTO temporary_rankings (player_id, ranking, quarter_ranking, ranking_points) SELECT id, ranking, quarter_ranking, ranking_points FROM players');

        // load matchPoints grouped per player where not assigned
        $ranking_matchpoints_query = \RankingMatchpoint::selectRaw("DISTINCT ON (ranking_matchpoints.licence_number) *")
            ->orderBy('licence_number');

        $ranking_matchpoints_query = $weekly_recalculation ? $ranking_matchpoints_query->where('assigned', false) : $ranking_matchpoints_query->where('assigned_for_quarter_ranking', false);

        $ranking_matchpoints = $ranking_matchpoints_query->get();


        $column_to_check = $weekly_recalculation ? "points_used_for_ranking_calculations" : "points_used_for_quarter_ranking_calculations";

        $players_external_points = ExternalTournament::where($column_to_check, false)->get();
        foreach($players_external_points as $pla){
            $player_from_db = Player::find($pla->player_id);
            $ranking_points = $player_from_db->ranking_points;
            $new_ranking_points = $ranking_points + $pla->total_calculated_points;
            $player_from_db->ranking_points = $new_ranking_points;
            $player_from_db->save();
            $pla->$column_to_check = true;
            $pla->save();
        }

        // ovo treba unaprijediti + prebaciti u points calculator
        foreach ($ranking_matchpoints as $key => $ranking_matchpoint) {
            $player = \Player::with('contact')->where("licence_number", $ranking_matchpoint->licence_number)->first();
            if (!$player)
                continue;

            echo $key.' of '.(count($ranking_matchpoints))."\n";

            if ($ranking_matchpoint->old_score) {
                $years_check = true;
            } else {
                $draw = \TournamentDraw::find($ranking_matchpoint->draw_id);
                $category = \DrawCategory::find($draw->draw_category_id);
                $final_draw_date = $draw->tournament->date->main_draw_to;
                $years = DateTimeHelper::CompareDifferenceInTime($player->contact->date_of_birth, $final_draw_date);

                $years_check = $years >= $category->range_for_points_from && $years <= $category->range_for_points_to;
            }

            //TODO: Provjeriti mutua samo ako je iza 31.marta
            // check if player has range for points and has valid mutua number
            if ($years_check AND $player->mutua > 0) {
                $player_matchpoints_query = \RankingMatchpoint::where('licence_number', $ranking_matchpoint->licence_number)
                    ->where('tournament_period', '>=', DateTimeHelper::GetTournamentPeriodMinusYear());
                $player_matchpoints_query = $weekly_recalculation ? $player_matchpoints_query->where('assigned', false) : $player_matchpoints_query->where('assigned_for_quarter_ranking', false);
                $player_matchpoints = $player_matchpoints_query->get();

                // get best 14 tournaments in last year and sum their total points
                $best_tournament_matchpoints = \PlayerHelper::calculateBestTournamentMatchPoints($player_matchpoints, $player->licence_number, $weekly_recalculation);

                // add points to player
                $player->ranking_points = ($player->ranking_points)
                    ? $player->ranking_points + $best_tournament_matchpoints
                    : 0 + $best_tournament_matchpoints;

                $player->save();
            }
        }


        // calculate ranking moves for MALE players
        // calculate ranking moves for FEMALE players
        echo "Moving calculator started\n";
        Artisan::call("calculatemove:do");

        EmailHelper::sendEmail("emails/recalculation", $email,
            array("content" => "Recalculation is done. ", "players" => count($ranking_matchpoints)));


        $rankingcalculationhistory = new RankingCalculationHistory();
        $rankingcalculationhistory->ranking_type = $weekly_recalculation ? 0 : 1;
        $rankingcalculationhistory->number_recalculated = count($ranking_matchpoints);
        $rankingcalculationhistory->date = DateTimeHelper::GetPostgresDateTime();
        $rankingcalculationhistory->save();

        Notification::recalculationNotification($weekly_recalculation);

        DB::Table("cron_job_finished")->insert(array("ran" => 1, "action_done" => "weekly_recalculation", 'created_at' => DateTimeHelper::GetDateNow(), 'updated_at' => DateTimeHelper::GetDateNow()));
        Log::info(($weekly_recalculation ? "Weekly" : "Quarter")." recalculation finished");

        echo ($weekly_recalculation ? "Weekly" : "Quarter")." recalculation finished\n";
        Artisan::call("export:do");

    }

    protected function getArguments()
    {
        return array(
            array('type', InputArgument::REQUIRED, '0 - weekly recalculation, 1 - quarter recalculation'),
            array('email', InputArgument::OPTIONAL, 'An email argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    // protected function getOptions()
    // {
    // 	return array(
    // 		array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
    // 	);
    // }

}
