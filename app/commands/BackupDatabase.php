<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BackupDatabase extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'backupdatabase:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Database backup. Occuring before new recalculaction.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        try {
//        echo Debug::vars(App::environment());die;
            Artisan::call("down");
//        try {
//            if (App::environment() == 'production')
//            elseif (App::environment() == 'staging')
//                Artisan::call("php /var/www/rfet.dash.ba/rfet_dev/artisan down");
//            else
//                Artisan::call("php /Users/damirseremet/Documents/WebProjects/rfet/artisan down");
//        } catch (Exception $ex) {
//
//        }

            $date = date("Y-m-d", time());
            if (App::environment() == 'production')
                exec("/usr/pgsql-9.4/bin/pg_dump -Fc -h localhost -U postgres rfet_dev > /var/www/rfet.dash.ba/db_backups/rfet_dev-" . $date . ".backup");
            elseif (App::environment() == 'staging')
                exec("/usr/pgsql-9.4/bin/pg_dump -Fc -h localhost -U postgres rfet_demo > /var/www/rfet.dash.ba/db_backups/rfet_demo-" . $date . ".backup");


            Artisan::call("up");
            DB::Table("cron_job_finished")->insert(array("ran" => 1, "action_done" => "db_backups", 'created_at' => DateTimeHelper::GetDateNow(), 'updated_at' => DateTimeHelper::GetDateNow()));

            //Backup logic
//        echo Debug::vars('aaa');die;

//        try {
//            if (App::environment() == 'production')
//                Artisan::call("php /var/www/rfet.dash.ba/rfet/artisan up");
//            elseif (App::environment() == 'staging')
//                Artisan::call("php /var/www/rfet.dash.ba/rfet/artisan up");
//            else
//                Artisan::call("php /Users/damirseremet/Documents/WebProjects/rfet/artisan up");
        } catch (Exception $ex) {
            Artisan::call("up");

        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
//	protected function getArguments()
//	{
//		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
//		);
//	}
//
//	/**
//	 * Get the console command options.
//	 *
//	 * @return array
//	 */
//	protected function getOptions()
//	{
//		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
//		);
//	}

}
