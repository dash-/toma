<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FakerAdd extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'faker:do';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for inserting fake data for tournaments';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		ini_set("memory_limit", -1);
		ini_set("max_execution_time", 3000);

		$faker = Faker\Factory::create();

		for ($i=0; $i < 20; $i++) { 
			
			$organizer = new TournamentOrganizer;
			$organizer->email = $faker->safeEmail;
			$organizer->phone = $faker->phoneNumber;
			$organizer->name = $faker->colorName;
			$organizer->save();

			FakerHelper::insert('TournamentOrganizer', $organizer->id);

			$tournament = new Tournament;
			$tournament->title = $faker->sentence($nbWords = 4);
			$tournament->approved_by = 1;
			$tournament->created_by = 1;
			$tournament->club_id = $faker->numberBetween(1, 1302);
			$tournament->date_of_approval = DateTimeHelper::GetDateNow();
			$tournament->number_of_courts = $faker->numberBetween(1, 4);
			$tournament->status = 2;
			$tournament->surface_id = $faker->numberBetween(1, 9);
			$tournament->organizer_id = $organizer->id;
			$tournament->save();
			$tournament->slug = Str::slug($tournament->title).'-'.$tournament->id;
			$tournament->save();

			FakerHelper::insert('Tournament', $tournament->id);


			$dates = new TournamentDate;
			$dates->tournament_id = $tournament->id;
			$dates->main_draw_from = $faker->dateTimeBetween('+1 days', '+4 weeks')->format('Y-m-d H:i:s');
			$dates->main_draw_to = date("Y-m-d H:i:s", strtotime('+1 week', strtotime($dates->main_draw_from)));
			$dates->qualifying_date_from = date("Y-m-d H:i:s", strtotime('-4 days', strtotime($dates->main_draw_from)));
			$dates->qualifying_date_to = date("Y-m-d H:i:s", strtotime('-1 days', strtotime($dates->main_draw_from)));
			$dates->withdrawal_deadline = date("Y-m-d H:i:s", strtotime('-1 days', strtotime($dates->qualifying_date_from)));
			$dates->entry_deadline = date("Y-m-d H:i:s", strtotime('-1 days', strtotime($dates->qualifying_date_from)));
			$dates->save();

			FakerHelper::insert('TournamentDate', $dates->id);


			for ($j=0; $j < 4; $j++) { 
				$draw = new TournamentDraw;
				$draw->tournament_id = $tournament->id;
				$draw->draw_category_id = $faker->numberBetween(1, 6);
				$draw->draw_gender = ($j%2==0) ? 'M' : 'F';
				$draw->draw_type = ($j%2==0) ? 'singles' : 'doubles';
				$draw->level = '20';
				$draw->number_of_seeds = 4;
				$draw->prize_pool = 300;
				$draw->unique_draw_id = date("Y.m.d") . "." . str_pad($tournament->club->province->region->id, 6, '0', STR_PAD_LEFT) . "." . $tournament->club->id . "." . str_pad($tournament->id, 4, '0', STR_PAD_LEFT) . "." . str_pad($j + 1, 2, '0', STR_PAD_LEFT);;
				$draw->save();

				FakerHelper::insert('TournamentDraw', $draw->id);

				$draw_size = new DrawSize;
				$draw_size->draw_id = $draw->id;
				// $draw_size->total = $faker->randomElement($array = array (8, 16, 32, 64, 128));
				$draw_size->total = $faker->randomElement($array = array (8, 16));
				$draw_size->direct_acceptances = $draw_size->total - 4;
				$draw_size->accepted_qualifiers = $draw_size->total - $draw_size->direct_acceptances;
				$draw_size->onsite_direct = 0;
				$draw_size->special_exempts = 0;
				$draw_size->wild_cards = 0;
				$draw_size->save();

				FakerHelper::insert('DrawSize', $draw_size->id);


				$draw_qualification = new DrawQualification;
				$draw_qualification->draw_id = $draw->id;
				// $draw_qualification->draw_size = $faker->randomElement($array = array (8, 16, 32, 64, 128));
				$draw_qualification->draw_size = $faker->randomElement($array = array (8, 16));
				$draw_qualification->save();
				
				FakerHelper::insert('DrawQualification', $draw_qualification->id);

			}


		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	// protected function getArguments()
	// {
	// 	return array(
	// 		array('example', InputArgument::REQUIRED, 'An example argument.'),
	// 	);
	// }

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	// protected function getOptions()
	// {
	// 	return array(
	// 		array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
	// 	);
	// }

}
