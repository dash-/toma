<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AddRegionsLocation extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'regionlocation:do';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for getting region location where not exists.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 3000);

        $regions = Region::whereNull("position_lat")->get();

        foreach($regions as $key => $region){

            $result = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=" . urlencode($region->address);


            $resp_json = $this->_curl_file_get_contents($result);

            $resp = json_decode($resp_json, true);

            if ($resp['status'] == 'OK') {
                $lat = $resp['results'][0]['geometry']['location']['lat'];
                $lng = $resp['results'][0]['geometry']['location']['lng'];

                $region->position_lat = $lat;
                $region->position_long = $lng;
                $region->save();
            }
            sleep(2.5);
            echo $key+1 ." of ".  count($regions)."\n";
        };
        echo 'Done!';
	}

    private function _curl_file_get_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents)
            return $contents;
        else
            return FALSE;
    }
	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
//	protected function getArguments()
//	{
//		return array(
//			array('example', InputArgument::REQUIRED, 'An example argument.'),
//		);
//	}
//
//	/**
//	 * Get the console command options.
//	 *
//	 * @return array
//	 */
//	protected function getOptions()
//	{
//		return array(
//			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
//		);
//	}

}
