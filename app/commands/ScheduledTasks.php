<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScheduledTasks extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'scheduledtask:execute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check DB for scheduled task and execute it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $tasks_to_execute = TasksToExecute::where("time_to_execute", "<", DateTimeHelper::GetDateNow())->get();
        foreach ($tasks_to_execute as $task) {
            $model = new $task->model;
            if ($model) {
                $action = $task->action;
                if (in_array($action, get_class_methods($task->model))) {
                    $parameters = '';
                    if($task->serialized_parameters)
                        $parameters = unserialize($task->serialized_parameters);
                    call_user_func(array($model, $action), $parameters);
                    echo "Executed.";

                    $executed = new ExecutedTasksHistory();
                    $executed->model = $task->model;
                    $executed->action = $task->action;
                    $executed->serialized_parameters = $task->serialized_parameters;
                    $executed->executed_time = DateTimeHelper::GetDateNow();
                    $executed->save();
                    $task->delete();

                }

            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('parameters', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
