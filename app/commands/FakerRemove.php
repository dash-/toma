<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FakerRemove extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'faker:remove';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command for removing fake data for tournaments';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		ini_set("memory_limit", -1);
		ini_set("max_execution_time", 3000);

		$faker_table = Faker::all();

		foreach ($faker_table as $faker) {
			FakerHelper::remove($faker->model_name, $faker->table_id);
			$faker->delete();
		}

		// Tournament::where('id', '>', 20)->delete();
		// TournamentOrganizer::where('id', '>', 20)->delete();
		// TournamentDate::where('tournament_id', '>', 20)->delete();
		// TournamentDraw::where('tournament_id', '>', 20)->delete();
		// DrawSize::where('id', '>', 42)->delete();
		// DrawQualification::where('id', '>', 42)->delete();

		// Tournament::truncate();
		// TournamentOrganizer::truncate();
		// TournamentDate::truncate();
		// TournamentDraw::truncate();
		// DrawSize::truncate();
		// DrawQualification::truncate();
		// DrawMatch::truncate();
		// MatchScore::truncate();
		// MatchSchedule::truncate();
		// DrawHistory::truncate();
		// TournamentSignup::truncate();
		// TournamentTeam::truncate();


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	// protected function getArguments()
	// {
	// 	return array(
	// 		array('example', InputArgument::REQUIRED, 'An example argument.'),
	// 	);
	// }

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	// protected function getOptions()
	// {
	// 	return array(
	// 		array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
	// 	);
	// }

}
