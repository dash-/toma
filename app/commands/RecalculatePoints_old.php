<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RecalculatePoints_old extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'recalculate:off';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for recalculation of points.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set("memory_limit", -1);
        ini_set("max_execution_time", 259200);

        $args = $this->argument();
        $email = new EmailClass();
        if (isset($args['email']) && $args['email'] != '')
            $email->to = $args['email'];
        else
            $email->to = Config::get("app.admin_email");

        $email->subject = "Weekly recalculation";

        // get 2 different collections on for MALE draws and other for FEMALE draws
        $male_history = TournamentHistory::whereHas('draw', function($query) {
                $query->where('draw_gender', 'M');
            })
            ->where('points_assigned', false)
            ->get();

        $female_history = TournamentHistory::whereHas('draw', function($query) {
            $query->where('draw_gender', 'F');
        })
            ->where('points_assigned', false)
            ->get();

        // history check counter don't do nothing if counter = 0
        $history_counter = count($male_history) + count($female_history);

        if ($history_counter) {

            // clear temporary_rankings table and populate it again so we can check ranking move for every player
            DB::table('temporary_rankings')->truncate();
            DB::statement('INSERT INTO temporary_rankings (player_id, ranking, ranking_points) SELECT id, ranking, ranking_points FROM players');

            // recalculate points for MALE draws
            \PointsCalculator::newRankList($male_history, 'M');
            // recalculate points for FEMALE draws
            \PointsCalculator::newRankList($female_history, 'F');

            EmailHelper::sendEmail("emails/recalculation", $email,
                array("content" => "Recalculation is done. ", "players" => $history_counter));
        }
        else{
            EmailHelper::sendEmail("emails/recalculation", $email,
                array("content" => "There wasn't any player for points recalculation.", "players" => $history_counter));
        }

        DB::Table("cron_job_finished")->insert(array("ran" => 1, "action_done" => "weekly_recalculation", 'created_at' => DateTimeHelper::GetDateNow(), 'updated_at' => DateTimeHelper::GetDateNow()));
        Log::info("Recalculation finished");
    }

    protected function getArguments()
    {
        return array(
            array('email', InputArgument::OPTIONAL, 'An email argument.')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    // protected function getOptions()
    // {
    // 	return array(
    // 		array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
    // 	);
    // }

}
