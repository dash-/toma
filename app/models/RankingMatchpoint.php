<?php

class RankingMatchpoint extends Eloquent
{
	protected $table   = 'ranking_matchpoints';
	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

    public function signup()
    {
        return $this->belongsTo('TournamentSignup', 'signup_id');
    }

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }

    public function player()
    {
        return $this->belongsTo('Player', 'player_id');
    }

    public function match()
    {
        return $this->belongsTo('DrawMatch', 'match_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    //================================ Custom Functions ===============================================
    public function status()
    {
        $statuses = [
            1 => LangHelper::get('win', 'Win'),
            2 => LangHelper::get('lose', 'Lose'),
            3 => LangHelper::get('winner', 'Winner'),
        ];

        return isset($statuses[$this->match_status]) ? $statuses[$this->match_status] : null;
    }


}