<?php

class MatchSchedule extends Eloquent
{
    public $timestamps = false;
    protected $table = 'match_schedules';

    protected $guarded = ['id'];

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

    public function match()
    {
        return $this->belongsTo('DrawMatch', 'match_id');
    }

    //================================ Custom Functions ===============================================

    public function status()
    {
        $statuses = [
            0 => LangHelper::get('not_started', 'Not started'),
            1 => LangHelper::get('to_be_played', 'To be played'),
            2 => LangHelper::get('delayed', 'Delayed'),
            3 => LangHelper::get('in_progress', 'In progress'),
            4 => LangHelper::get('completed', 'Completed'),
        ];

        return $statuses[$this->match_status];
    }

    public function color()
    {
        $colors = [
            0 => '#222',
            1 => '#525252',
            2 => '#500000',
            3 => '#FFC627',
            4 => '#009578',
        ];

        return $colors[$this->match_status];
    }

    public function type_of_time($waiting_list = false)
    {
        $types = [
            1 => LangHelper::get('starting_at', 'Starting at'),
            2 => LangHelper::get('not_before', 'Not before'),
            3 => LangHelper::get('followed_by', 'Followed by'),
        ];

        $type = (!$waiting_list) ? $this->type_of_time : $this->waiting_list_type_of_time;

        return isset($types[$type]) ? $types[$type] : null;
    }

    public function getTime($waiting_list = false)
    {
        $time = (!$waiting_list) ? $this->schedule_time : $this->waiting_list_time;
        return date('H:i', strtotime($time));
    }
}