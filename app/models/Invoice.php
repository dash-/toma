<?php

class Invoice extends Eloquent
{

    protected $guarded = ['id'];


    public function region()
    {
        return $this->belongsto('Region', 'region_id');
    }

    public function clubs()
    {
        return $this->belongsTo('Club', 'club_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function data()
    {
        return $this->hasMany('InvoiceData', 'invoice_id');
    }

    public function payments()
    {
        return $this->hasMany('PaymentHistory', 'invoice_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    //================================ Custom Functions ===============================================
    public function status()
    {
        $statuses = [
            1 => LangHelper::get('unpaid', 'Unpaid'),
            2 => LangHelper::get('partially_paid', 'Partially paid'),
            3 => LangHelper::get('paid', 'Paid'),
        ];

        if (isset($statuses[$this->status])) {
            if ($this->status > 1) {
                if ($this->total() >= $this->paid()) {
                    $this->status = 3;
                    $this->save();
                }
            }

            return $statuses[$this->status];
        }

        return null;
    }

    public function creator()
    {
        $creator = User::find($this->created_by);

        return (is_null($creator)) ? false : $creator->username;
    }

    public function getNewOrderNumber()
    {
        return $this->max('order_number') + 1;
    }

    public function getNewControlCode()
    {
        return mt_rand();
    }

    /**
     * @param $query
     * @param $player Illuminate\Database\Eloquent\Collection
     */
    public function scopeCreateMutuaInvoice($query, $player)
    {
        $creator = User::with('roles')->find($player->created_by);
        $this->title = 'Mutua invoice for - ' . $player->contact->full_name();
        $this->created_by = Auth::user()->id;
        $this->role_id = $creator->roles[0]->id; // for which user role is invoice
        $this->region_id = $creator->getUserRegion();
        $this->order_number = $this->getNewOrderNumber();
        $this->code = $this->getNewControlCode();
        $this->save();

        $invoice_data = new InvoiceData();
        $invoice_data->invoice_id = $this->id;
        $invoice_data->amount = 1;
        $invoice_data->description = 'One player licence approved';
        $invoice_data->price = 13.85;
        $invoice_data->save();
    }

    /**
     * @param $query
     * @param $tournament Illuminate\Database\Eloquent\Collection
     * @return integer $id integer
     */
    public function scopeCreateTournamentInvoice($query, $tournament)
    {
        $this->title = 'Invoice for - tournament ' . $tournament->title;
        $this->created_by = Auth::user()->id;
        $this->role_id = 3; // for which user role is invoice this invoice is for regional admin
        $this->region_id = $tournament->getRegionId();
        $this->tournament_id = $tournament->id;
        $this->order_number = $this->getNewOrderNumber();
        $this->code = $this->getNewControlCode();
        $this->save();

        foreach ($tournament->draws as $draw) {
            $invoiceData = new InvoiceData();
            $invoiceData->invoice_id = $this->id;
            $invoiceData->amount = TournamentHelper::countDrawPlayers($draw->signups);
            $invoiceData->description = $draw->genderName() . ', ' . $draw->typeName() . ', ' . $draw->category->name;
            $invoiceData->price = TournamentDrawHelper::tournamentDrawPrice($draw->draw_category_id, $draw->prize_pool,
                TournamentHelper::countDrawPlayers($draw->signups))['coefficient'];
            $invoiceData->save();
        }

        return $this->id;
    }

    public function scopeCreatePerClubInvoice($query, $tournament)
    {

        $number_of_draws = TournamentDraw::with('signups', 'signups.teams', 'signups.teams.player')->whereTournamentId($tournament->id)->get();

        $clubs = [];
        $data = [];

        foreach ($number_of_draws as $draw) {
            foreach ($draw->signups as $signup) {
                foreach ($signup->teams as $teams) {

                    $clubs_ids = $teams->player->club_id;
                    $players = $teams->player->licence_number;
                    $draw_id = $draw->id;
                    $clubs[$clubs_ids][] = $players;

                    $data[$clubs_ids][$draw_id][] = $players;
                }
            }
        }

        foreach ($data as $club_id => $d) {

            if (!$club_id) {
                continue;
            }

            $club_name = Club::select('club_name')->find($club_id)->club_name;

            $invoice = new Invoice;
            $invoice->title = 'Club invoice for - ' . $club_name;
            $invoice->created_by = Auth::user()->id;
            $invoice->role_id = 3; // for which user role is invoice invoice invoice is for regional admin
            $invoice->region_id = $tournament->getRegionId();
            $invoice->tournament_id = $tournament->id;
            $invoice->order_number = $invoice->getNewOrderNumber();
            $invoice->code = $invoice->getNewControlCode();
            $invoice->save();

            foreach ($d as $draw_id => $player) {

                $draw_category_id = TournamentDraw::select('draw_category_id')->find($draw_id)->draw_category_id;
                $prize_pool = TournamentDraw::select('prize_pool')->find($draw_id)->prize_pool;
                $category_name = DrawCategory::select('name')->find($draw_category_id)->name;

                $invoice_data = new InvoiceData;
                $invoice_data->invoice_id = $invoice->id;
                $invoice_data->amount = count($player);
                $invoice_data->description = 'Players invoice for club ' . $club_name . ' (' . $category_name . ')';
                $invoice_data->price = TournamentDrawHelper::tournamentDrawPrice($draw_category_id, $prize_pool, count($player))['coefficient'];
                $invoice_data->save();

            }
        }
    }

    /**
     * Get invoice data based on user role
     * @param $query
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function scopeGetInvoicesByRole($query)
    {
        if (Auth::user()->hasRole('superadmin')) {
            return $query->get();
        } elseif (Auth::user()->hasRole('regional_admin')) {
            return $query->where('role_id', 3)
                ->where('region_id', Auth::user()->getUserRegion())
                ->get();
        }
    }
    /**
     * Get count of invoices data based on user role
     * @param $query
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function scopeGetCountUnpaidInvoicesByRole($query)
    {
        $count = 0;
        if (Auth::user()->hasRole('superadmin')) {
            $count =  $query->where('status', '!=', 2)->count();
        } elseif (Auth::user()->hasRole('regional_admin')) {
            $count = $query->where('role_id', 3)
                ->where('status', '!=', 2)
                ->where('region_id', Auth::user()->getUserRegion())
                ->count();
        }

        return ($count) ? $count : -1;
    }

    /**
     * Calculate total amount to paid of invoice
     * @return int
     */
    public function total($add_sign = true)
    {
        $data = $this->data;
        $total = 0;
        foreach ($data as $d) {
            $total += $d->price * $d->amount;
        }

        return $add_sign ? HtmlHelper::numberFormat($total) . ' €' : $total;
    }

    public function paid($add_sign = true)
    {
        return $add_sign ? HtmlHelper::numberFormat($this->paid) . ' €' : $this->paid;
    }

    public function regionName()
    {
        return ($this->region_id)
            ? Region::find($this->region_id)->region_name
            : null;
    }

    public function generateAngularData()
    {
        return [
            'total' => $this->total(),
            'total_number' => $this->total(false),
            'status' => $this->status(),
            'paid' => $this->paid(),
            'paid_number' => $this->paid(false),
            'payments' => $this->getPaymentsData(),
        ];
    }

    /**
     * generate array for angular parsing of payment history for invoice
     * @return array
     */
    private function getPaymentsData()
    {
        $data = [];
        foreach ($this->payments as $payment) {
            $item['created_at'] = $payment->created_at;
            $item['created_by'] = $payment->creator();
            $item['paid'] = $payment->paid();

            $data[] = $item;
        }

        return array_reverse($data);
    }

    public function calculateStatus()
    {
        if ($this->total(false) - $this->paid(false) == 0)
            return 3;
        else if ((int)$this->paid == 0)
            return 1;
        else
            return 2;
    }
}