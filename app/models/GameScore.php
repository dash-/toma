<?php

class GameScore extends \Eloquent {
    protected $table   = 'game_score';
    protected $guarded = ['id'];
    protected $connection = 'scoring_connection';

    public function match()
    {
        return $this->belongsTo('DrawMatch', 'match_id');
    }

    public function details()
    {
        return $this->hasMany('GameScoreDetail', 'game_score_id')->orderBy('id');
    }
}