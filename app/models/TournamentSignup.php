<?php

class TournamentSignup extends Eloquent
{
    // List type explanation:
    //     1 => 'Main draw',
    //     2 => 'Qualifying draw',
    //     3 => 'Alternate draw',
    //     4 => 'Withdraw',
    //     5 => 'PreQualifying draw',

    // Status explanation:
    //     1 => 'Wild card main draw',
    //     2 => 'Wild card main draw',
    //     3 => 'Special exempt',
    //     4 => 'On site direct',
    //     5 => 'Manual move to main draw',
    //     6 => 'Manual move to qualifying draw',
    //     6 => 'Manual move to alternate draw',

    // move_from_qualifiers_type explanation:
    //      2 => '(Q)',
    //      3 => '(LL)',


	public $timestamps = false;
	protected $table   = 'tournament_signups';

	protected $guarded = array('id');

    public function tournament()
    {
        return $this->belongsTo('Tournament');
    }

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

    public function teams()
    {
        return $this->hasMany('TournamentTeam', 'team_id');
    }

    public function match_team1()
    {
        return $this->hasMany('DrawMatch', 'team1_id');
    }

    public function match_team2()
    {
        return $this->hasMany('DrawMatch', 'team2_id');
    }

    public function seeds()
    {
        return $this->hasMany('DrawSeed', 'signup_id');//->whereListType($this->list_type);
    }
    

    // public function team()
    // {
    //     return $this->hasOne('TournamentTeam', 'team_id');
    // }
	//================================ Custom Functions ===============================================
    public function getCreator()
    {
        return User::whereId($this->submited_by)->first()->username;
    }

    public function list_type()
    {
        $types = [
            1 => LangHelper::get('main', 'Main'),
            2 => LangHelper::get('qualifying', 'Qualifying'),
            3 => LangHelper::get('alternate', 'Alternate'),
            4 => LangHelper::get('withdraw', 'Withdraw'),
            5 => LangHelper::get('prequalifying', 'Prequalifying'),
        ];

        return ($this->list_type) ? $types[$this->list_type] : FALSE;
    }

    public function status()
    {
        $statuses = [
            1 => LangHelper::get('wild_card_main', 'Wild card main draw'),
            2 => LangHelper::get('wild_card_quali', 'Wild card qualification draw'),
            3 => LangHelper::get('special_exempt', 'Special exempt'),
            4 => LangHelper::get('on_site_direct', 'On site direct'),
            5 => LangHelper::get('manual_move_to_main', 'Manual move to main draw'),
            6 => LangHelper::get('manual_move_to_quali', 'Manual move to qualification draw'),
            7 => LangHelper::get('manual_move_to_alternate', 'Manual move to alternate draw'),
        ];

        return ($this->status) ? $statuses[$this->status] : FALSE;
    }

    public function list_color()
    {
        $colors = [
            1 => '#6FD6FF',
            2 => '#3EBB73',
            3 => '#bababa',
            4 => '#B75E5E',
        ];

        return ($this->list_type) ? $colors[$this->list_type] : FALSE;
    }

    /**
     * @param $query
     * @param $list_type integer
     * @param $draw_id integer
     * @return mixed
     */
    public function scopeManualDrawData($query, $list_type, $draw_id)
    {
        return $query->with(array('seeds'=>function($q) use ($list_type){
            $q->whereListType($list_type);
        }))
            ->with('teams')
            ->where(function ($q) use ($list_type) {
                $q->where('move_from_qualifiers_type', '>', 0)
                    ->orWhere('list_type', '=', $list_type);
            })
            ->where('draw_id', $draw_id)
            ->select(DB::raw("COALESCE(ranking_points, 0) as ranking_points_null"), "*")
            ->orderBy('ranking_points_null', 'desc')
//            ->orderBy('ranking_points', 'desc')
            ->orderBy('move_from_qualifiers_type')
            ->get();
    }

    /**
     * @param $query
     * @param $list_type integer
     * @param $draw_id integer
     * @param $seed_numbers array of seed numbers where key is id of seeded player
     * @param $number_of_seeds integer total number of seeds
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function scopeManualDrawDataWithDefinedSeeds($query, $list_type, $draw_id, $seed_numbers, $number_of_seeds)
    {
        $signups = $this->manualDrawData($list_type, $draw_id);
        $count_passed_seed_numbers = count($seed_numbers);
        foreach ($seed_numbers as $id => $seed_number) {
            $signup = $signups->filter(function($item) use ($id) {
                return $item->id == $id;
            })->first();
            $this->addSeedsDataToCollection($signup, $draw_id, $seed_number, $list_type);
        }

        if ($count_passed_seed_numbers < $number_of_seeds) {
            $how_many = $number_of_seeds - $count_passed_seed_numbers;
            $added_seed_numbers = array_values($seed_numbers);
            $available_seed_range = range(1, $number_of_seeds);

            $available_seed_range = array_filter($available_seed_range, function($item) use ($added_seed_numbers) {
                return !in_array($item, $added_seed_numbers);
            });

            $available_seed_range = array_values($available_seed_range);

            for ($i = 0; $i < $how_many; $i++) {
                if (!$signups[$i]->seeds->count()) {
                    $this->addSeedsDataToCollection($signups[$i], $draw_id, $available_seed_range[$i], $list_type);
                }
            }
        }

        return $signups;
    }

    /**
     * @param $signup Illuminate\Database\Eloquent\Collection
     * @param $draw_id integer
     * @param $seed_number integer
     * @param $list_type integer
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function addSeedsDataToCollection($signup, $draw_id, $seed_number, $list_type)
    {
        $signup['seeds'][] = [
            'draw_id'   => $draw_id,
            'signup_id' => $signup['id'],
            'list_type' => $list_type,
            'seed_num'  => $seed_number,
        ];
        DrawSeed::createSingleSeed($draw_id, $signup['id'], $list_type, $seed_number);

        return $signup;
    }
}