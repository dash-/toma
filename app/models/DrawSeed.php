<?php

class DrawSeed extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'draw_seeds';

	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw');
    }

    public function signup()
    {
        return $this->belongsTo('TournamentSignup');
    }

    public function teams()
    {
        return $this->hasMany('TournamentTeam', 'team_id', 'signup_id');
    }

    //================================ Custom Functions ===============================================

    public function scopeGetSeeds($query, $draw_id, $list_type, $consolation_draw = false)
    {
        $seeds = $this->where('draw_id', $draw_id);

        $seeds = ($consolation_draw)
            ? $seeds->where('consolation_type', $list_type)
            : $seeds->where('list_type', $list_type);

        return $seeds->lists('seed_num', 'signup_id');

    }

    public function scopeCreateSeeds($query, $signups, $draw, $list_type)
    {
        $seed_num = 1;
        foreach ($signups->take($draw->number_of_seeds) as $signup) {
            $seed_number = $seed_num++;
            $signup->addSeedsDataToCollection($signup, $draw->id, $seed_number, $list_type);

            usleep(100000);
        }
    }

    public function scopeCreateSingleSeed($query, $draw_id, $signup_id, $list_type, $seed_num)
    {
        if ($seed_num > 0) {
            $this->create([
                'draw_id'   => $draw_id,
                'signup_id' => $signup_id,
                'list_type' => $list_type,
                'seed_num'  => $seed_num,
            ]);
        }

    }

}