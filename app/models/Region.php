<?php

class Region extends Eloquent {

	protected $guarded = ['id'];
	public $timestamps = false;

	
	public function provinces()
	{
		return $this->hasMany('Province');
	}

    public function clubs()
    {
        return $this->hasMany('Club');
    }

    public function users()
    {
        return $this->belongsToMany('User', 'users_regions');
    }

	public function licence_range()
	{
		return $this->hasOne('LicenceRange', 'region_id');
	}

	public function sponsors()
	{
		return $this->hasMany('Sponsor', 'region_id');
	}

    public function regionImage()
    {
        return ($this->image_path) ? URL::to('uploads/regions', $this->image_path) : null;
    }
}