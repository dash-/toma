<?php

class TournamentArticle extends Eloquent
{
    protected $table   = 'tournament_articles';
    protected $guarded = array('id');

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function images()
    {
    	return $this->hasMany('ArticleImage', 'article_id');
    }

    public function comments()
    {
        return $this->hasMany('PublicComments', 'table_id')->where('table_name', 'tournament_articles');
    }

    //================================ Custom Functions ===============================================

    public function creator()
    {
        return $this->user->username;
    }

    public function status()
    {
        $statuses = [
            0 => 'Not published',
            1 => 'Published',
        ];

        return $statuses[$this->active];
    }

    public function first_image($size = 'full')
    {
        // dd($this->images->first());
        return (!is_null($this->images->first())) 
            ? URL::to('/').'/uploads/images/articles/'.$size.'/'.$this->images->first()->image
            : false;
        // return URL::to('/').'/uploads/images/articles/'.$size.'/'.$this->image;
    }
}