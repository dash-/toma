<?php

class Admin extends Eloquent
{
    public $timestamps = false;

    protected $guarded = array('id');

    public function user()
    {
        return $this->hasOne('User');
    }

    public function contact()
    {
        return $this->hasOne('Contact');
    }

}