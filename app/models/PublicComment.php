<?php

class PublicComment extends Eloquent
{
    protected $guarded = array('id');
    protected $table = 'public_comments';

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function article()
    {
        return $this->belongsTo('TournamentArticle', 'table_id');
    }

    

}