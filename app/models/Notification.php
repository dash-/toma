<?php

class Notification extends Eloquent
{
    public $timestamps = false;

    protected $guarded = array('id');

    public function users()
    {
        return $this->belongsToMany('User', 'users_notifications')->withPivot(['sticky', 'seen']);
    }

    public function type()
    {
        return $this->belongsTo('NotificationType');
    }

    public function addUsers($user_id)
    {
        $this->users()->attach($user_id);
    }

    /*
    *  string $text
    *  integer $type_id
    *  mixed(string|array) $to
    *  integer $conversation_id
    */
    public function scopeMessageNotif($query, $to, $conversation_id)
    {
        $notif = $this->create(array(
            'type_id' => 1,
            'text' => LangHelper::get('new_message_received', 'New message received'),
            'link' => URL::to('messages/show/' . $conversation_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));


        // check if user_id is string (if string, its a single user else its an array where there are multiple users for a single message)
        if (is_array($to)) {
            foreach ($to as $receiver) {
                $user = User::find($receiver);
                $conversation = Message::whereConversationId($conversation_id);
                $email = new EmailClass();
                $email->to = $user->email;
                $email->name = $user->name;
                $email->subject = $conversation->subject;

                $data = ['name' => $user->username, 'link' => '/messages/show/' . $conversation_id, 'timestamp' => $conversation->time_sent];

                EmailHelper::sendEmail('emails.message_mail', $email, $data);
            }

        } else {
            $user = User::find($to);
            $conversation = Message::whereConversationId($conversation_id)->first();

            $email = new EmailClass();
            $email->to = $user->email;
            $email->name = $user->name;
            $email->subject = $conversation->subject;
            $data = ['name' => $user->username, 'link' => '/messages/show/' . $conversation_id, 'timestamp' => $conversation->time_sent];

            EmailHelper::sendEmail('emails.message_mail', $email, $data);
        }

        $notif->addUsers($to);

    }

    public function scopeChangeRequestNotif($query, $to, $conversation_id)
    {
        $notif = $this->create(array(
            'type_id' => 10,
            'text' => 'User ' . Auth::user()->username . ' sent request for result change',
            'link' => URL::to('messages/show/' . $conversation_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));


        $notif->addUsers($to);

    }

    public function scopeTournamentNotif($query, $to, $tournament_id, $notif_type, $message)
    {
        $notif = $this->create(array(
            'type_id' => $notif_type,
            'text' => $message,
            'link' => URL::to('tournaments/details/' . $tournament_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));
        $notif->addUsers($to);
    }

    public function scopePlayerNotif($query, $to, $player_id, $notif_type, $message)
    {
        $notif = $this->create(array(
            'type_id' => $notif_type,
            'text' => $message,
            'link' => URL::to('players/details/' . $player_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));
        $notif->addUsers($to);
    }

    public function scopeTournamentSignupNotif($query, $signup_id, $tournament_title)
    {
        $notif = $this->create(array(
            'type_id' => 8,
            'text' => 'Player without licence signed up for tournament ' . $tournament_title,
            'link' => URL::to('signups/player-details/' . $signup_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));
        $notif->addUsers(Role::find(1)->users()->get()->lists('id', 'id'));
    }
    public function scopeTournamentNotValidatedSignupNotif($query, $player_id, $tournament_title)
    {
        $notif = $this->create(array(
            'type_id' => 8,
            'text' => 'Player without validated licence signed up for tournament ' . $tournament_title,
            'link' => URL::to('players/details/' . $player_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));
        $notif->addUsers(Role::find(1)->users()->get()->lists('id', 'id'));
    }

    public function scopeRefereeNotif($query, $referee_id, $message)
    {
        $notif = $this->create(array(
            'type_id' => 9,
            'text' => $message,
            'link' => URL::to('superadmin/users/info/' . $referee_id),
            'created_at' => date('Y-m-d H:i:s', time()),
        ));
        $notif->addUsers(Role::find(1)->users()->get()->lists('id', 'id'));
    }

    public function scopeSendNoteNotification($query, $tournament_id)
    {
        $tournament = Tournament::with('club')->find($tournament_id);

        $regional_admins = Region::find($tournament->getRegionId())->users()->get();
        $super_admins = Role::find(1)->users()->get();
        $users = [];

        foreach ($regional_admins as $regional) {
            $users[$regional->id] = $regional->id;
        }

        foreach ($super_admins as $super) {
            $users[$super->id] = $super->id;
        }

        unset($users[Auth::user()->id]);
        if ($users) {
            $notif = $this->create(array(
                'type_id' => 11,
                'text' => 'New note added - ' . $tournament->title,
                'link' => URL::to('tournaments/details/' . $tournament->id),
                'created_at' => date('Y-m-d H:i:s', time()),
            ));
            $notif->addUsers($users);
        }
    }
    public function scopeRecalculationNotification($query, $weekly_recalculation)
    {
        $referees = Role::find(6)->users()->get();
        $super_referees = Role::find(7)->users()->get();
        $regional_admins = Role::find(3)->users()->get();
        $super_admins = Role::find(1)->users()->get();
        $users = [];

        foreach ($referees as $referee) {
            $users[$referee->id] = $referee->id;
        }
        foreach ($super_referees as $super_referee) {
            $users[$super_referee->id] = $super_referee->id;
        }
        foreach ($regional_admins as $regional) {
            $users[$regional->id] = $regional->id;
        }

        foreach ($super_admins as $super) {
            $users[$super->id] = $super->id;
        }

//        unset($users[Auth::user()->id]);
        $message = $weekly_recalculation ? LangHelper::get('weekly_recalculation_is_completed', 'Weekly recalculation is completed') : LangHelper::get('quarter_recalculation_is_completed', 'Quarter recalculation is completed');
        if ($users) {
            $notif = $this->create(array(
                'type_id' => $weekly_recalculation ? 12 : 13,
                'text' =>  $message, //'New note added - ' . $tournament->title,
                'link' => URL::to('/rankings'),
                'created_at' => date('Y-m-d H:i:s', time()),
            ));
            $notif->addUsers($users);
        }
    }

}