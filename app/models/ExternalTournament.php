<?php

class ExternalTournament extends Eloquent {

	protected $table = 'external_tournaments';
	protected $guarded = ['id'];


	public function player ()
	{
		return $this->belongsTo('Player', 'player_id');
	}

	public function licence ()
	{
		return $this->belongsTo('Player', 'licence_number');
	}

	public function user_edit () 
	{
		return $this->belongsTo('User', 'user_edit_id');
	}

// ============================ Custom functions ======================================== 


	public function tournament_type($key)
	{
		$types = TournamentHelper::external_points_type_names();

		return $types[$key];
	}

	public function external_points_acquired ()
	{
		return $this;
	}

}