<?php

class DrawApproval extends Eloquent {
    protected $guarded = ['id'];

    public function draw() {
        return $this->hasOne('TournamentDraw', 'draw_id');
    }
}