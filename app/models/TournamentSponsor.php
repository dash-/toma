<?php

class TournamentSponsor extends Eloquent {

	protected $table = 'tournaments_sponsors';

	public $timestamps = false;

	public function tournaments()
	{
		return $this->hasMany('Tournament');
	}

	public function sponsors()
	{
		return $this->hasMany('Sponsor');
	}
}