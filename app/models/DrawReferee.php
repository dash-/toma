<?php

class DrawReferee extends Eloquent
{

    protected $guarded = ['id'];
    public $timestamps = false;
    protected $table   = 'draws_referees';



    public function draw()
    {
        return $this->belongsTo('TournamentDraw');
    }

    public function referee()
    {
        return $this->belongsTo('User');
    }

    public function history()
    {
        return $this->hasMany('DrawRefereeHistory', 'draw_referee_id');
    }

    //================================ Custom Functions ===============================================

    // public function status()
    // {
    //     $statuses = [
    //         0 => LangHelper::get('applied', 'Applied'),
    //         1 => LangHelper::get('approved', 'Approved'),
    //         2 => LangHelper::get('rejected', 'Rejected'),
    //         3 => LangHelper::get('canceled', 'Canceled'),
    //     ];
        
    //     return $statuses[$this->status];
    // }

    public function save(array $options = array())
    {
        if (!$this->exists)
        {
            $this->date_of_application = new DateTime;
            $this->date_of_status_change = new DateTime;
        }
        return  parent::save();
    }
    
}