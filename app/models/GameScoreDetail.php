<?php

class GameScoreDetail extends \Eloquent {
    protected $guarded = ['id'];
    protected $connection = 'scoring_connection';

    /*
     * Numbers 0 - 3 are used for game scoring
     * If player has won the gem without deuce , than the final score for the winner is 4
     * If player has won the gem with deuce, than the final score fir the winner is 6
     *
     */
    public function displayScore($key)
    {
        $score = array(
            0 => 0,
            1 => 15,
            2 => 30,
            3 => 40,
            4 => '',    //won without deuce
            5 => 'A',
            6 => ''     //won with deuce
        );

        return $score[$key];
    }

    public function score()
    {
        return $this->belongsTo('GameScore', 'game_score_id');
    }

}