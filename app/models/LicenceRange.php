<?php

class LicenceRange extends Eloquent {

	protected $guarded = ['id'];
	public $timestamps = false;

	
	public function region()
	{
		return $this->belongsTo('Region', 'region_id');
	}

	public function scopeGetLicenceRanges($query, $region_id)
	{
		return $query->whereRegionId($region_id)
			->first();
	}

	public function rangeToString()
	{
		return LangHelper::get('enter_number_between', 'Enter number between').' '.$this->range_from.' - '.$this->range_to;
	}
}