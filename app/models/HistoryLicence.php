<?php

class HistoryLicence extends \Eloquent {

    protected $guarded = ['id'];
    public $timestamps = false;
}