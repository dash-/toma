<?php

class PenaltyNote extends Eloquent
{

	protected $table = 'penalty_notes';

	protected $guarded = array('id');

    public function player()
    {
        return $this->belongsTo('Player');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function tournament()
    {
        return $this->belongsTo('Tournament');
    }

	//================================ Custom Functions ===============================================

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    public function scopeGetNotes($query, $player_id)
    {
        return $query->with('user', 'tournament')
            ->wherePlayerId($player_id)
            ->orderBy('created_at', 'desc')
            ->get();
    }


    
}