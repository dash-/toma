<?php

class DrawMatch extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'draw_matches';

	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

    public function scores()
    {
    	return $this->hasMany('MatchScore', 'match_id')->orderBy('id');
    }

    // this was fuckup one match can have only one schedule but we didn't know that before
    // so right now its pretty hard to track where this relationship was used
    public function schedules()
    {
        return $this->hasMany('MatchSchedule', 'match_id');
    }

    public function schedule()
    {
        return $this->hasOne('MatchSchedule', 'match_id');
    }

    public function team1()
    {
        return $this->belongsTo('TournamentSignup', 'team1_id');
    }

    public function team2()
    {
        return $this->belongsTo('TournamentSignup', 'team2_id');
    }

    public function team1_data()
    {
        return $this->belongsTo('TournamentTeam', 'team1_id', 'team_id');
    }
    public function team1_data_array()
    {
        return $this->hasMany('TournamentTeam', 'team_id', 'team1_id');
    }

    public function team2_data()
    {
        return $this->belongsTo('TournamentTeam', 'team2_id', 'team_id');
    }

    public function team2_data_array()
    {
        return $this->hasMany('TournamentTeam', 'team_id', 'team2_id');
    }

    public function game_scores()
    {
        return $this->hasMany('GameScore', 'match_id');
    }

    public function matchpoints()
    {
        return $this->hasOne('RankingMatchpoint', 'match_id');
    }

    //================================ Custom Functions ===============================================

    public function sendTeamToNextRound($team1_score, $team2_score)
    {

        // calculate next round position for player
        $next_round_pos = ($this->round_position % 2 != 0) ? ($this->round_position + 1) / 2 : $this->round_position / 2;

        $obj = DrawMatch::where('round_position', $next_round_pos)
            ->where('round_number', $this->round_number + 1)
            ->where('list_type', $this->list_type)
            ->where('draw_id', $this->draw_id)
            ->first();

        $passing_team = ($team1_score >= $team2_score) ? $this->team1_id : $this->team2_id;
        
        if ($obj != NULL)
        {
            // position in this calcuation
            if ($this->round_position % 2 == 1) {
                $obj->team1_id = $passing_team;
                // $data[] = $passing_team;
            } else {
                $obj->team2_id = $passing_team;
                // $data[] = $passing_team;
            }

            $obj->save();
        }

        return $this->round_number;
    }

    
    public function match_status()
    {
        $statuses = [
            '0' => '',
            '1' => LangHelper::get('w_o', 'W.O.'), // pobjeda, protivnik dobio kaznu
            '2' => LangHelper::get('w_o_justified', 'W.O. justified'), // pobjeda
            '3' => LangHelper::get('retired', 'Retired'), // izgubio
            '4' => LangHelper::get('default', 'Default'), // izgubio
            '5' => LangHelper::get('def_misconduct', 'Def. misconduct'), // izgubio sa kaznom
        ];

        return (isset($statuses[$this->match_status])) ? $statuses[$this->match_status] : null;
    }

    public function scopeCreateMatchesFromManualDraw($query, $rounds_preparation, $draw_id, $list_type)
    {
        foreach ($rounds_preparation as $key => $rounds) {
            $counter = 1;
            foreach ($rounds as $round) {
                DrawMatch::create([
                    'team1_id'       => (!isset($round[0]['id'])) ? 0 : $round[0]['id'],
                    'team2_id'       => (!isset($round[1]['id'])) ? 0 : $round[1]['id'],
                    'draw_id'        => $draw_id,
                    'list_type'      => $list_type,
                    'round_position' => $counter++,
                    'round_number'   => $key,
                ]);
            }
        }
    }

    public function scopeFirstRoundMatches($query, $draw_id, $list_type)
    {
        return $query->where(function ($q) {
            $q->where('team1_id', 0)
                ->orWhere('team2_id', 0);
        })
            ->where('draw_id', $draw_id)
            ->whereRoundNumber(1)
            ->whereListType($list_type)
            ->orderBy('id')
            ->get();
    }

}