<?php

class Province extends Eloquent {

	protected $guarded = ['id'];
	public $timestamps = false;

	
	public function region()
	{
		return $this->belongsTo('Region');
	}
}