<?php

class TournamentDraw extends Eloquent
{
    public $timestamps = false;
    protected $table = 'tournament_draws';

    protected $guarded = ['id'];

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }

    public function signups()
    {
        return $this->hasMany('TournamentSignup', 'draw_id');
    }

    public function schedules()
    {
        return $this->hasMany('MatchSchedule', 'draw_id');
    }

    public function category()
    {
        return $this->belongsTo('DrawCategory', 'draw_category_id');
    }

    public function qualification()
    {
        return $this->hasOne('DrawQualification', 'draw_id');
    }

    public function size()
    {
        return $this->hasOne('DrawSize', 'draw_id');
    }

    public function matches()
    {
        return $this->hasMany('DrawMatch', 'draw_id');
    }

    public function seeds()
    {
        return $this->hasMany('DrawSeed', 'draw_id');
    }

    public function referees()
    {
        return $this->hasMany('DrawReferee', 'draw_id');
    }

    public function history()
    {
        return $this->hasMany('DrawHistory', 'draw_id');
    }

    //================================ Custom Functions ===============================================

    public function typeName()
    {
        $types = [
            'singles' => LangHelper::get('singles', 'Singles'),
            'doubles' => LangHelper::get('doubles', 'Doubles'),
        ];

        return $types[$this->draw_type];
    }

    public function genderName()
    {
        $genders = [
            'm' => LangHelper::get('male', 'Male'),
            'f' => LangHelper::get('female', 'Female')
        ];

        return $genders[strtolower($this->draw_gender)];
    }

    public function refereeAppliedStatus($user_id)
    {
        $referee = $this->referees()
            ->where('user_id', '=', $user_id)
            ->first();

        // if referre does not exist return -1 
        if (is_null($referee))
            return -1;
        else
            return $referee->status;

    }

    public function status()
    {
        // Open Registration, Registration Closed, Qualification Rounds, Main Rounds, Completed
        $statuses = [
            '0' => '',
            '1' => LangHelper::get('open_registration', 'Open registration'),
            '2' => LangHelper::get('registration_closed', 'Registration closed'),
            '3' => LangHelper::get('qualification_rounds', 'Qualification rounds'),
            '4' => LangHelper::get('main_rounds', 'Main rounds'),
            '5' => LangHelper::get('completed', 'Completed'),
        ];

        return $this->status ? $statuses[$this->status] : FALSE;
    }

    /*
     * 0 - Standard (3 set)
     * 1 - Grand Slam (5 set)
     */
    public function match_rule()
    {
        $setup = TournamentDrawHelper::getMatchRule();
        return $setup[$this->match_rule];
    }

    /*
     * 0 - Standard (6 games, tie-break at 6:6)
     * 1 - Short Set (4 games, tie-break at 4:4)"
     */
    public function set_rule()
    {
        $setup = TournamentDrawHelper::getSetRule();
        return $setup[$this->set_rule];
    }

    /*
     * 0 - Standard
     * 1 - no_Ad
     */
    public function game_rule()
    {
        $setup = TournamentDrawHelper::setGameRule();
        return $setup[$this->game_rule];
    }

    public function approvalStatus() {

        $status = [
            0 => LangHelper::get('draw_not_completed','Draw not completed'),
            1 => LangHelper::get('waiting_for_validation','Waiting for validation'),
            2 => LangHelper::get('validated_by_regional_admin','Validated by regional admin'),
            3 => LangHelper::get('validated_by_superadmin','Validated by superadmin')
        ];
        return isset($status[$this->approval_status]) ? $status[$this->approval_status] : '';
    }

}