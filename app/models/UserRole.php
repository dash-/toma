<?php

class UserRole extends Eloquent {

	protected $table = 'users_roles';

	public $timestamps = false;

	public function user()
	{
		return $this->hasMany('User');
	}

	public function role()
	{
		return $this->hasMany('Role');
	}
}