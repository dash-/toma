<?php

class LoginHistory extends Eloquent
{

    public $timestamps = false;
    protected $table = 'login_history';
    // Relations
    public function user()
    {
        return $this->belongsTo('User');
    }

   public function save(array $options = array())
	{
	    $this->created_at = new DateTime;
	    $this->ip = Request::getClientIp();
		$this->browser = Agent::platform() ." - " .Agent::browser()." : ".Agent::version(Agent::browser());
		$this->user_id = $options['user_id'];
    	return  parent::save();
	}
}