<?php

class UserRegion extends Eloquent {

	protected $table = 'users_regions';

	public $timestamps = false;

	public function user()
	{
		return $this->hasOne('User');
	}

	public function region()
	{
		return $this->hasOne('Region');
	}

}