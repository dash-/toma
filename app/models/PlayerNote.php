<?php

class PlayerNote extends Eloquent
{

	protected $table   = 'player_notes';

	protected $guarded = array('id');

    public function player()
    {
        return $this->belongsTo('Player');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

	//================================ Custom Functions ===============================================

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    
}