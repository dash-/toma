<?php

class Referee extends \Eloquent
{
    protected $guarded = ['id'];

    public function player()
    {
        return $this->belongsTo('Player', 'player_id');
    }

    public function contact()
    {
        return $this->belongsTo('Contact', 'contact_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function external_tournaments()
    {
        return $this->hasMany('RefereeExternalTournament', 'referee_id');
    }

    /*
     * ACTIVE
     *  1 - yes
     *  0 - no
     * -1 - Only applied
     * -2 - Out
     * -3 - RIP
     */
    public function active()
    {
        //TODO: Translate
        return $this->activeArray()[$this->active];
    }

    public function activeArray()
    {
        return array(
            -3 => LangHelper::get('rip', 'RIP'),
            -2 => LangHelper::get('out', 'out'),
            -1 => LangHelper::get('only_applied', 'Only applied'),
            0  => LangHelper::get('no', 'No'),
            1  => LangHelper::get('yes', 'Yes'),
        );
    }

    /**
     * Check search string and map it ti $status_map
     *
     * @param $string
     * @return int|null mixed
     */
    public function getActiveFromString($string)
    {
        $status_map = array(
            'rip'          => -3,
            'out'          => -2,
            'only_applied' => -1,
            'no'           => 0,
            'yes'          => 1,
        );

        $string = strtolower(str_replace(' ', '_', $string));

        return (isset($status_map[$string])) ? $status_map[$string] : null;
    }

    public function scopeFilteredData($query, $filters, $pages = 20)
    {
        $referees = Referee::leftJoin('contacts', 'referees.contact_id', '=', 'contacts.id')
            ->select("*", "referees.id as id");

        foreach ($filters as $key => $post) {
            $key = str_replace("INT_", "", $key, $int);

            if ($post AND !in_array($key, array('items_counter', 'order', 'sort', 'search', 'callback', 'page'))) {
                if ($key == 'active' AND $post = $this->getActiveFromString($post))
                    $referees->where('active', $post);
                elseif ($int)
                    $referees->where($key, '=', $post);
                else
                    $referees->where($key, 'ILIKE', $post . "%");
            }
        }

        return $referees->paginate($pages);
    }

    public function scopeGetData($query, $order, $sort)
    {
        return $this->leftJoin('contacts', 'referees.contact_id', '=', 'contacts.id')
            ->select("*", "referees.id as id")
            ->orderBy($order, $sort)
            ->paginate(20);
    }

    public function contactInfo($what)
    {
        if (is_null($this->contact))
            return null;

        return ($what == 'postal_code')
            ? sprintf('%05d', $this->contact->postal_code)
            : $this->contact->$what;
    }

    public function statusDropdown($default_value = null)
    {
        return (!is_null($default_value))
            ? [$default_value => $this->active()] + $this->activeArray()
            : $this->activeArray();
    }

    private function select2Dropdown($referees)
    {
        $items = [];
        $results = [];

        foreach ($referees as $referee) {
            $items['id'] = $referee->id;
            $items['text'] = $this->dropdownName($referee);

            $results[] = $items;
        }

        return $results;
    }

    private function dropdownName($referee)
    {
        return ($referee)
            ? $referee->contactInfo('name') . ' ' . $referee->contactInfo('surname') . ' - ' . $referee->licence_number
            : null;
    }

    public function scopeRefereeName($query, $ref_id)
    {
        $referee = $query->find($ref_id);

        if (!$referee)
            return 'N/A';

        return $this->dropdownName($referee);
    }

    public function scopeDataForDropdown($query, $search_term)
    {
        $referees = $query->leftJoin('contacts', 'referees.contact_id', '=', 'contacts.id')
            ->where('contacts.name', 'ILIKE', $search_term . "%")
            ->orWhere('contacts.surname', 'ILIKE', $search_term . "%")
            ->orWhere('licence_number', 'ILIKE', $search_term . "%")
            ->take(10)
            ->select('*', 'referees.id as id')
            ->get();

        return count($referees) ? $this->select2Dropdown($referees) : [];
    }
}