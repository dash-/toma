<?php


class TournamentSurface extends Eloquent
{

    protected $table = 'tournament_surfaces';

    public function tournament()
    {
        return $this->hasOne('Tournament');
    }


    public function scopeDropdown($query)
    {
        $surfaces = $this->all();

        $item = [];
        foreach ($surfaces as $surface) {
            $key_name = strtolower(str_replace(' ', '_', $surface->name));
            $item[$surface->id] = LangHelper::get($key_name, $surface->name);
        }

        return $item;
    }

    public function name()
    {
        $key_name = strtolower(str_replace(' ', '_', $this->name));
        return LangHelper::get($key_name, $this->name);
    }
}