<?php

class Player extends Eloquent
{
    public $timestamps = false;
    protected $guarded = array('id');

    //     1 => 'Pending',
    //     2 => 'Accepted',
    //     3 => 'Rejected',
    //     4 => 'In progress',
    
    public function contact()
    {
        return $this->belongsTo('Contact');
    }

   	public function tournaments()
	{
		return $this->belongsToMany('Tournament', 'tournaments_players');
	}

    public function draws()
    {
        return $this->hasMany('TournamentDraw');
    }

    public function club()
    {
        return $this->belongsTo('Club');
    }

    public function old_ranking()
    {
        return $this->hasOne('TempRanking', 'player_id');
    }

    public function ranking_histories()
    {
        return $this->hasMany('PlayerRankingHistory', 'player_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function notes()
    {
        return $this->hasMany('PlayerNote', 'player_id');
    }

    public function penalty_notes()
    {
        return $this->hasMany('PenaltyNote', 'player_id');
    }

    public function team()
    {
        return $this->hasOne('TournamentTeam', 'licence_number', 'licence_number');
    }

    public function history()
    {
        return $this->hasMany('RankingMatchpoint', 'player_id');
    }

    public function nation()
    {
        return $this->hasOne('Nationality', 'country_id', 'nationality');
    }

    //================================ Custom Functions ===============================================
    /**
     * save new player status after generating new mutua number
     * @return bool
     */
    public function updateStatus()
    {
        $this->mutua = self::newMutuaNumber();
        $this->approved_by = Auth::user()->id;
        $this->status = 2;
        $this->date_of_approval = DateTimeHelper::GetDateNow();
        if ($this->save())
            return true;

        return false;
    }

    public function showNationality()
    {
        return ($this->nation) ? $this->nation->country : Nationality::find(1)->country;
    }


    public function creator()
    {   
        return User::whereId($this->created_by)->first();
    }
    
    public function getSignupInformation()
    {
        return $player = [
            'licence_number' => $this->licence_number,
            'contact' => [
                'name' => $this->contact->name,
                'surname' => $this->contact->surname,
                'email' => $this->contact->email,
                'phone' => $this->contact->phone,
                'date_of_birth' => DateTimeHelper::GetShortDateFullYear($this->contact->date_of_birth),
            ]
        ];
    }

    public function getAgeCategory($type = 'name')
    {
        $years = DateTimeHelper::GetDifferenceInTime($this->contact->date_of_birth);

        $draw_categories = DrawCategory::all();

        foreach ($draw_categories as $category) 
        {
            if($years >= $category->range_from && $years <= $category->range_to)
                return $category->$type;
        }

        return false;


    }

    public function status()
    {
        $statuses = [
            1 => LangHelper::get('pending', 'Pending'),
            2 => LangHelper::get('approved', 'Approved'),
            3 => LangHelper::get('rejected', 'Rejected'),
            4 => LangHelper::get('in_progress', 'In progress'),
        ];

        return $statuses[$this->status];
    }

    public function scopeMaxMutuaNumber($query)
    {
        return DB::select(DB::raw("SELECT MAX((0 || mutua)::INTEGER) FROM players"))[0]->max;
    }

    public function scopeNewMutuaNumber($query)
    {
        return $this->maxMutuaNumber() + 1;
    }
}