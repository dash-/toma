<?php

class TournamentOrganizer extends Eloquent
{
	public $timestamps = false;
	protected $table = 'tournament_organizers';
   	
	protected $guarded = array('id');

    public function tournament()
    {
        return $this->hasMany('Tournament');
    }

}