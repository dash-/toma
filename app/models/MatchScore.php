<?php

class MatchScore extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'match_scores';

	protected $guarded = ['id'];

    public function match()
    {
        return $this->belongsTo('DrawMatch', 'match_id');
    }

}