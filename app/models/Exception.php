<?php

class ExceptionModel extends Eloquent
{
    protected $table = 'exceptions';
    public $timestamps = true;

    public function __construct()
    {
        Eloquent::unguard();
    }

    public function createException($exception)
    {
//        ExceptionModel::create(array('Url'=>'test'));
        $this->statusCode = $exception->getStatusCode();
        $this->message = $exception->getMessage();
        $this->url = Request::url();
        $this->browser = Agent::platform() ." - " .Agent::browser()." : ".Agent::version(Agent::browser());
        $this->save();
    }

} 