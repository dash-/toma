<?php

class Tournament extends Eloquent
{
    public $timestamps = false;
    protected $guarded = array('id');

//    public $tournament_types = [
//        1 => 'RFET',
//        2 => 'Regional',
//        3 => 'Province',
//    ];

    public function organizer()
    {
        return $this->belongsTo('TournamentOrganizer');
    }

    public function surface()
    {
        return $this->belongsTo('TournamentSurface');
    }

    public function club()
    {
        return $this->belongsTo('Club');
    }

    public function date()
    {
        return $this->hasOne('TournamentDate')->orderBy('main_draw_from', 'DESC');
    }

    public function draws()
    {
        return $this->hasMany('TournamentDraw', 'tournament_id');
    }

    public function players()
    {
        return $this->belongsToMany('Player', 'tournaments_players');
    }

    public function signups()
    {
        return $this->hasMany('TournamentSignup');
    }

    public function schedules()
    {
        return $this->hasMany('MatchSchedule', 'tournament_id');
    }

    public function sponsors()
    {
        return $this->belongsToMany('Sponsor', 'tournaments_sponsors');
    }

    public function articles()
    {
        return $this->hasMany('TournamentArticle', 'tournament_id');
    }

    public function galleries()
    {
        return $this->hasMany('TournamentGallery', 'tournament_id');
    }

    public function notes()
    {
        return $this->hasMany('TournamentNote', 'tournament_id');
    }

    public function messages()
    {
        return $this->hasMany('TournamentMessage', 'tournament_id')->orderBy('id', 'desc');
    }

    public function type()
    {
        return $this->belongsTo('TournamentType', 'tournament_type');
    }

    //================================ Custom Functions ===============================================

//    public function scopeTournamentTypesDropdown($query, $chosen_type = false)
//    {
//        return ($chosen_type)
//            ? [$chosen_type => $this->getTournamentType($chosen_type)] + $this->tournament_types
//            : $this->tournament_types;
//    }

    public function scopeTournamentListDropdown($query)
    {
        return [NULL => LangHelper::get('choose_tournament', 'Choose tournament')] + $this->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
            ->orderBy('tournament_dates.main_draw_from', 'DESC')
            ->select("*", "tournaments.id as id")
            ->lists('title', 'id');
    }
    public function scopeTournamentListDropdownFilter($query, $tournament_ids)
    {
        return [NULL => LangHelper::get('choose_tournament', 'Choose tournament')] + $this->join('tournament_dates', 'tournaments.id', '=', 'tournament_dates.tournament_id')
            ->orderBy('tournament_dates.main_draw_from', 'DESC')
            ->whereIn("tournaments.id", $tournament_ids)
            ->select("*", "tournaments.id as id")
            ->lists('title', 'id');
    }

    public function scopeGetCalendarData($query)
    {
        if (Auth::getUser()->hasRole('superadmin') || Auth::getUser()->hasRole('referee'))
            $tournaments = $query->whereStatus(2)->get();
        elseif (Auth::getUser()->hasRole("regional_admin")) {
            $tournaments = $query->join("clubs", "tournaments.club_id", "=", "clubs.id")
                ->join("regions", "regions.id", "=", "clubs.region_id")
                ->where("regions.id", Auth::getUser()->getUserRegion())->where('tournaments.status','!=', 3)->select("*", "tournaments.id as id")
                ->get();
        }

        return $tournaments;
    }

//    public function getTournamentType($chosen_type)
//    {
//        return (isset($this->tournament_types[$chosen_type])) ? $this->tournament_types[$chosen_type] : null;
//    }


    public function creator()
    {
        return User::whereId($this->created_by)->first();
    }

    public function referee()
    {
        return User::with("contact")->whereId($this->referee_id)->first();
    }

    public function status($only_status = false)
    {
        $statuses = [
            1 => LangHelper::get('pending_approval', 'Pending approval'),
            2 => LangHelper::get('approved', 'Approved'),
            3 => LangHelper::get('rejected', 'Rejected'),
             4 => LangHelper::get('submitted_for_approval', 'Submited for approval'), //
            5 => LangHelper::get('finished', 'Finished'),
        ];
        $is_valid_msg = '';
        if ($this->status > 1)
            $is_valid_msg = ($this->validated)
                ? ' - '.LangHelper::get('validated', 'Validated')
                : ' - '.LangHelper::get('not_valiid', 'Not valid');

        if ($only_status)
            return (isset($statuses[$this->status])) ? $statuses[$this->status] : '';

        return isset($statuses[$this->status]) ? ($statuses[$this->status].''.$is_valid_msg) : "";
    }

    public function statusDropdown()
    {
        $statuses = [
            1 => LangHelper::get('pending_approval', 'Pending approval'),
            2 => LangHelper::get('approved', 'Approved'),
            3 => LangHelper::get('rejected', 'Rejected'),
            5 => LangHelper::get('finished', 'Finished'),
        ];

        return [$this->status => $this->status(true)] + $statuses;
    }

    public function getDate($type)
    {
        return (is_null($this->date)) ?: DateTimeHelper::GetShortDateFullYear($this->date->$type);
    }

    public function getSponsor($type)
    {
        $sponsor = $this->sponsors()->first();
        return (is_null($sponsor)) ? NULL : $sponsor->$type;
    }

    public function getSponsorImage($size = 'full')
    {
        $sponsor = $this->sponsors()->first();
        return (is_null($sponsor)) || !$sponsor->image ? NULL : $sponsor->image($size);
    }

    public function getRegionImage()
    {
        return ($this->club AND $this->club->region) ? $this->club->region->regionImage() : null;
    }

    public function getRegionId()
    {
        return ($this->club) ? $this->club->region_id : null;
    }

    public function getProvinceId()
    {
        return ($this->club) ? $this->club->province_id : null;
    }

    public function addSponsor($sponsor_id)
    {
        if ($sponsor_id) {
            $this->sponsors()->detach();
            $this->sponsors()->attach($sponsor_id);
        }
    }

    public function generateQr()
    {
        return base64_encode(QrCode::format('png')->size(100)->generate(URL::route('default_public', $this->slug)));
    }

    public function getSuperadmin()
    {
        $superadmin = User::whereHas('roles', function ($query) {
                  $query->where('role_id', '=', 1);
            })
            ->first();

        return $superadmin;
    }

    public function refereeName()
    {
        $referee = Referee::with("contact")->whereId($this->referee_id_real)->first();

//        echo Debug::vars($referee['contact']->full_name());die;
        return $referee ? $referee['contact']->full_name() : "N/A";

//        return ($this->referee() && $this->referee()->contact[0])
//            ? $this->referee()->contact[0]->full_name()
//            : "N/A";
    }
}

