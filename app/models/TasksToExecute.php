<?php

class TasksToExecute extends \Eloquent {
	protected $table = "tasks_to_execute";
	public $timestamps = false;
	protected $guarded = array('id');
}