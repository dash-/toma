<?php

class DrawHistory extends Eloquent
{
	protected $table = 'draw_history';

	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'submitted_by');
    }

    //================================ Custom Functions ===============================================

   
}