<?php

class ScheduleConfiguration extends Eloquent
{
    public $timestamps = false;
    protected $table = 'schedule_configuration';

    protected $guarded = ['id'];

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }

    //================================ Custom Functions ===============================================

    public function scopeLoadConfiguration($query, $tournament, $date)
    {
        $conf = $query->whereTournamentId($tournament->id)
            ->where('date', $date)
            ->first();

        if (is_null($conf)) {
            $conf = ScheduleConfiguration::create([
                'tournament_id'    => $tournament->id,
                'date'             => $date,
                'number_of_courts' => $tournament->number_of_courts,
                'waiting_list'     => false,
            ]);
        }

        return $conf;
    }

    public function scopeLoadAllConfigurations($query, $tournament)
    {
        return $query->whereTournamentId($tournament->id)
            ->get();
    }

    public function scopeCreateInitialConfigurations($query, $tournament, $dates_array)
    {
        $schedules = $this->where('tournament_id', $tournament->id)
            ->whereIn('date', array_keys($dates_array))
            ->get(['date'])->toArray();
        $schedules = array_flatten($schedules);


        $dates_array = array_filter(array_flip($dates_array), function ($date) use ($schedules) {
            return (in_array($date, $schedules)) ? false : true;
        });

        if (count($dates_array)) {
            echo Debug::vars('a');
            foreach ($dates_array as $date) {
                ScheduleConfiguration::create([
                    'tournament_id'    => $tournament->id,
                    'date'             => $date,
                    'number_of_courts' => $tournament->number_of_courts,
                    'waiting_list'     => false,
                    'rest_time'        => 120,
                ]);
            }
        }
        return $this->where('tournament_id', $tournament->id)->orderBy('id', 'asc')->get();

    }

    public function date()
    {
        return DateTimeHelper::GetShortDateFullYear($this->date);
    }
}