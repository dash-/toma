<?php

class TournamentDate extends Eloquent
{

	public $timestamps = false;
	protected $table   = 'tournament_dates';

	protected $guarded = array('id');

    public function tournament()
    {
        return $this->belongsTo('Tournament');
    }

	//================================ Custom Functions ===============================================

    public function getDate($field)
    {
        return ($this->$field)
            ? DateTimeHelper::GetShortDate($this->$field)
            : 'N/A';
    }
}