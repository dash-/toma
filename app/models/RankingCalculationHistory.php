<?php

class RankingCalculationHistory extends \Eloquent {
    // 0 - weekly
    // 1 - quarter
    protected $table   = 'ranking_calculation_history';
	protected $guarded = ['id'];
    public $timestamps = false;

    public function type()
    {
        $type = [
            0 => 'Weekly',
            1 => 'Quarterly'
        ];

        return $type[$this->ranking];
    }


}