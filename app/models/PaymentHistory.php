<?php

class PaymentHistory extends Eloquent
{

    protected $guarded = ['id'];
    protected $table = 'payment_history';

    public function invoice()
    {
        return $this->belongsTo('Invoice', 'invoice_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    public function scopeCreateNewPayment($query, $invoice_id, $paid)
    {
        $this->created_by = Auth::user()->id;
        $this->invoice_id = $invoice_id;
        $this->paid= $paid;
        $this->save();
    }

    public function creator()
    {
        $creator = User::find($this->created_by);

        return (is_null($creator)) ? false : $creator->username;
    }

    public function paid($add_sign = true)
    {
        return $add_sign ? HtmlHelper::numberFormat($this->paid) . ' €' : $this->paid;
    }
}