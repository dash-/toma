<?php

class TournamentTeam extends Eloquent
{

	public $timestamps = false;
	protected $table   = 'tournament_teams';

	protected $guarded = array('id');

    public function signup()
    {
        return $this->belongsTo('TournamentSignup', 'team_id');
    }

    public function player()
    {
        return $this->belongsTo('Player', 'licence_number', 'licence_number');
    }


	//================================ Custom Functions ===============================================
	public function full_name()
	{
		return $this->surname.' '.$this->name;
	}

    public function short_name()
    {
        return $this->surname.'. '.$this->name[0];
    } 
}