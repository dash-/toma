<?php

class Message extends Eloquent {

    public $timestamps = false;
    protected $guarded = array('id');

    public function users()
    {
        return $this->belongsToMany('User', 'messages_users')->withPivot(['time_seen', 'deleted_at', 'seen']);
    }

    public function getFrom()
    {
        return User::where('id', '=', $this->sender_id)->first();
    }
    

    public function getTo($type = 'string')
    {
        $users = $this->users()->get();

        // dd($users->toArray());

        $users_array = [];

        // if (Auth::getUser()->id != $this->sender_id)
        // {
            $users_array = array(
                array(
                    'id' => $this->sender_id,
                    'username' => User::whereId($this->sender_id)->first()->username,
                ));
        // }

        foreach ($users as $user) {
            if (Auth::getUser()->id != $user->id)
            {
                $item['id'] = $user->id;
                $item['username'] = $user->contact->first()->full_name();

                $users_array[] = $item;
            }
        }

        if ($type == 'string' && count($users_array) > 0)
        {
            return implode(', ',array_column($users_array, 'username'));
        }

        return json_encode($users_array);

    }

}