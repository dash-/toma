<?php

class Sponsor extends Eloquent
{
    public $timestamps = false;

    protected $guarded = array('id');

	
    public function tournaments()
    {
        return $this->belongsToMany('Tournament', 'tournaments_sponsors');
    }

    public function region()
    {
    	return $this->belongsTo('Region', 'region_id');
    }

    //================================ Custom Functions ===============================================


    public function regionName()
    {
    	$regions_array = [ 0 => LangHelper::get('all_regions', 'All regions') ] + Region::lists('region_name', 'id');

    	return $regions_array[$this->region_id];
    }

    public function image($size = 'full')
    {
        
        return URL::to('/').'/uploads/images/sponsors/'.$size.'/'.$this->image;
    }
}