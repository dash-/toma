<?php

class CategoryRankingConfiguration extends \Eloquent {
	protected $guarded = array('id');
	protected $table = 'category_ranking_configuration';
}