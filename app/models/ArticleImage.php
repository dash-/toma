<?php

class ArticleImage extends Eloquent
{
    protected $table   = 'article_images';
    public $timestamps = false;
    protected $guarded = array('id');

    public function article()
    {
        return $this->belongsTo('TournamentArticle', 'article_id');
    }
    

    //================================ Custom Functions ===============================================

    public function image($size = 'full')
    {
        return URL::to('/').'/uploads/images/articles/'.$size.'/'.$this->image;
    }
}