<?php

class DrawRefereeHistory extends Eloquent
{

    protected $guarded = ['id'];
    public $timestamps = false;
    protected $table   = 'draws_referees_history';



    public function draw()
    {
        return $this->belongsTo('Draw');
    }

    public function referee()
    {
        return $this->belongsTo('User');
    }

    public function draw_referee()
    {
        return $this->belongsTo('DrawReferee', 'draw_referee_id');
    }

    //================================ Custom Functions ===============================================

    public function scopeSaveHistory($query, DrawReferee $referee)
    {
        $this->draw_referee_id = $referee->id;
        $this->draw_id = $referee->draw_id;
        $this->user_id = $referee->user_id;
        $this->status = $referee->status;
        $this->date = new DateTime;
        $this->changed_by = Auth::user()->id;

        $this->save();

    }

}