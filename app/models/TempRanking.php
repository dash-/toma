<?php

class TempRanking extends Eloquent
{

    protected $table = 'temporary_rankings';

    protected $guarded = array('id');

    public function player()
    {
    	return $this->belongsTo('Player', 'player_id');
    }
    
}