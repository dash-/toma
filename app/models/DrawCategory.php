<?php

class DrawCategory extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'draw_categories';

	protected $guarded = ['id'];

    public function draws()
    {
        return $this->hasMany('TournamentDraw');
    }

}