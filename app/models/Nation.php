<?php

class Nation extends Eloquent
{

    protected $table = 'nation';

    public function tournament()
    {
        return $this->hasOne('Tournament');
    }

}