<?php

class PlayerRankingHistory extends Eloquent
{

    protected $table   = 'player_ranking_history';
    protected $connection = 'pgsql2';

    protected $guarded = array('id');

    public function player()
    {
        return $this->belongsTo('Player');
    }


    //================================ Custom Functions ===============================================

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
    }


}