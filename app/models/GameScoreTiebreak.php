<?php

class GameScoreTiebreak extends \Eloquent {
	protected $guarded = ['id'];
	protected $connection = 'scoring_connection';

	public function match()
	{
		return $this->belongsTo('DrawMatch', 'match_id');
	}

	public function game_score(){
		return $this->belongsTo('GameScore');
	}
}