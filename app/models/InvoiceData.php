<?php

class InvoiceData extends Eloquent
{

    protected $guarded = ['id'];
    protected $table = 'invoice_data';
    public $timestamps = false;

    public function invoice()
    {
        return $this->belongsTo('invoice', 'invoice_id');
    }

    public function price()
    {
        return HtmlHelper::numberFormat($this->price) . ' €';
    }

    public function total($add_sign = true)
    {
        $total = 0;
        $total += $this->price * $this->amount;

        return $add_sign ? HtmlHelper::numberFormat($total) . ' €' : $total;
    }
}