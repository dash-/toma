<?php

class FavoritesPlayer extends Eloquent{

    protected $guarded = ['id'];

    public function favorited_players()
    {
        return $this->hasMany('Player', 'favorited_player');
    }
} 