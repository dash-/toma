<?php

class RefereeExternalTournament extends \Eloquent {
	protected $guarded = ['id'];

    public function referee() {
        return $this->belongsTo('Referee', 'referee_id');
    }

    public function user() {
        return $this->belongsTo('User', 'created_by');
    }

    public function createdBy()
    {
        return $this->user->info('name').' '.$this->user->info('surname');
    }

    public function date($what)
    {
        return DateTimeHelper::GetPostgresDate($this->$what);
    }
}