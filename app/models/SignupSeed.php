<?php

class SignupSeed extends Eloquent
{

	public $timestamps = false;
	protected $table   = 'signup_seeds';

	protected $guarded = array('id');

    public function signup()
    {
        return $this->belongsTo('TournamentSignup', 'signup_id');
    }


	//================================ Custom Functions ===============================================
    
}