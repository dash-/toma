<?php

class TournamentGallery extends Eloquent
{
    protected $table   = 'tournament_galleries';
    public $timestamps = false;
    protected $guarded = array('id');

    public function tournament()
    {
        return $this->belongsTo('Tournament', 'tournament_id');
    }
    

    //================================ Custom Functions ===============================================

    public function image($size = 'full')
    {
        return URL::to('/').'/uploads/images/gallery/'.$size.'/'.$this->image_path;
    }
}