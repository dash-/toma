<?php

class NotificationType extends Eloquent {
	protected $guarded = ['id'];

	public $timestamps = false;

	protected $table = 'notification_types';

	public function notifications()
    {
        return $this->hasMany('Notification');
    }
}