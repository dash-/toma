<?php

class TournamentType extends \Eloquent
{
    protected $guarded = array('id');

    public function scopeGetDropdown($query, $chosen = false, $skip_faseprevia = false)
    {
        if ($skip_faseprevia)
            $types = $query->orderBy('id')->where("type", "!=", "CO")->where("type", "!=", "PR")->get();
        else
            $types = $query->orderBy('id')->get();

        $item = [];
        foreach ($types as $type) {
            $item[$type->id] = $this->typeFullName($type);
        }

        if ($chosen) {
            $chosen_type = $this->find($chosen);
            $item = [$chosen_type->id => $this->typeFullName($chosen_type)] + $item;
        } else {
            $item = [null => LangHelper::get('choose_tournament_type', 'Choose tournament type')] + $item;
        }

        return $item;
    }

    private function typeFullName($type)
    {
        return $type->type . ' - ' . $type->category_tournament;
    }
}