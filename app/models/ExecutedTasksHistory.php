<?php

class ExecutedTasksHistory extends \Eloquent {
	protected $table = "executed_tasks_history";
	public $timestamps = false;
	protected $guarded = array('id');
}