<?php

class DrawSize extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'draw_sizes';

	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

}