<?php

class TournamentMessage extends Eloquent
{

    protected $table   = 'tournament_messages';

    protected $guarded = array('id');

    public function tournament()
    {
        return $this->belongsTo('Tournament');
    }

    public function user()
    {
        return $this->belongsTo('User', 'created_by');
    }

    //================================ Custom Functions ===============================================

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y h:ia');
    }

    public function created_by()
    {
        return (!is_null($this->user))
            ? $this->user->email
            : '';
    }

    public function status()
    {
        $statuses = [
            0 => 'Inactive',
            1 => 'Active',
        ];

        return $statuses[$this->active];
    }

    public function scopeGetStates($query, $tournament_id)
    {
        $msgs = $this->whereTournamentId($tournament_id)->get();

        $result = [];
        foreach ($msgs as $msg) {
            $item['message_id'] = $msg->id;
            $item['state'] = ($msg->active == 1) ? 'Unpublish' : 'Publish';
            $item['status'] = $msg->status();

            $result[] = $item;
        }

        return $result;
    }

    public function scopeUpdateStatus($query, $tournament_id, $message_id)
    {
        $query->where('tournament_id', $tournament_id)
            ->where('id', '!=', $message_id)
            ->update([
                'active' => false
            ]);
    }
}