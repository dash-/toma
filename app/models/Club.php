<?php

class Club extends Eloquent
{

    protected $guarded = ['id'];
    public $timestamps = false;


    public function province()
    {
        return $this->belongsTo('Province');
    }

    public function region()
    {
        return $this->belongsTo('Region');
    }

    public function tournaments()
    {
        return $this->hasMany("Tournament", "club_id");
   }

    //================================ Custom Functions ===============================================


    public function image($size = 'full')
    {
        
        return URL::to('/').'/uploads/images/clubs/'.$size.'/'.$this->image_link;
    }
}