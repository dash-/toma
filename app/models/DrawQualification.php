<?php

class DrawQualification extends Eloquent
{
	public $timestamps = false;
	protected $table   = 'draw_qualifications';

	protected $guarded = ['id'];

    public function draw()
    {
        return $this->belongsTo('TournamentDraw', 'draw_id');
    }

}