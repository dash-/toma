<?php

class TournamentHistory extends Eloquent
{

    protected $table = 'tournament_histories';

    protected $guarded = array('id');

	public function tournament()
	{
		return $this->belongsTo('Tournament', 'tournament_id');
	}    

	public function draw()
	{
		return $this->belongsTo('TournamentDraw', 'draw_id');
	}

	public function matches()
	{
		return $this->hasMany('DrawMatch', 'team1_id', 'team_id');
	}

	public function matches2()
	{
		return $this->hasMany('DrawMatch', 'team2_id', 'team_id');
	}

	
}