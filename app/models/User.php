<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	public $timestamps = false;

	protected $fillable = array('username', 'email');

	public function contact()
    {
        return $this->belongsToMany('Contact', 'admins');
    }

	public function roles()
    {
        return $this->belongsToMany('Role', 'users_roles')->orderBy('id', 'asc');
    }

    public function notifications()
    {
        return $this->belongsToMany('Notification', 'users_notifications')->withPivot(['sticky','seen']);
    }
   
    public function logins()
    {
    	return $this->hasMany('LoginHistory');
    }

    public function messages()
    {
    	return $this->belongsToMany('Message', 'messages_users')->withPivot(['time_seen', 'deleted_at', 'seen']);
    }

    public function region()
    {
    	return $this->belongsToMany('Region', 'users_regions');
    }

    public function players()
    {
    	return $this->hasMany('Player', 'created_by');
    }

    public function language()
    {
    	return $this->belongsTo('Language', 'language_id');
    }

    public function articles()
    {
        return $this->HasMany('TournamentArticle', 'created_by');
    }
	
	public function draws()
    {
        return $this->hasMany('DrawReferee', 'user_id');
    }

    public function draws_histories()
    {
        return $this->hasMany('DrawHistory', 'submitted_by');
    }

    public function tournament_notes()
    {
        return $this->hasMany('TournamentNote', 'user_id');
    }

    public function player_notes()
    {
        return $this->hasMany('PlayerNote', 'user_id');
    }

    public function penalty_notes()
    {
        return $this->hasMany('PenaltyNote', 'user_id');
    }

    public function tournament_messages()
    {
        return $this->hasMany('TournamentMessage', 'created_by');
    }

    public function player()
    {
        if($this->hasRole("player"))
            return $this->hasOne("Player", "user_id");

    }

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *o
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	
	//================================ Custom Functions ===============================================

	/**
	* Find out if user has a specific role
	* $return boolean
	*/
    public function hasRole($check)
    {
        return in_array($check, array_fetch($this->roles->toArray(), 'name'));
    }

    public function getRole($param = 'name')
    {
    	return $this->roles->first()->$param;
    }

    public function addRole($role_id) 
    {
		$this->roles()->detach();
	    $this->roles()->attach($role_id);
    }

    public function addRefereeAdminRole()
    {
        $this->roles()->detach();

        $this->roles()->attach(6);
        $this->roles()->attach(7);
    }

	public function save(array $options = array())
	{
		if (!$this->exists)
		{
		    $this->created_at = new DateTime;
		    $this->secret = Str::random($length = 16);
		}
    	return  parent::save();
	}
	
	public function info($param = 'name')
	{
		if ($param == 'date_of_birth')
			return ($this->contact->first()) ? DateTimeHelper::GetShortDateFullYear($this->contact->first()->$param) : false;

		return ($this->contact->first()) ? $this->contact->first()->$param : false;
	}

	public function countMessages()
	{
		return Auth::getUser()->messages()->wherePivot('seen', '=', FALSE)->count();
	}

    public function getUserRegion()
    {
        return $this->region->first() ? $this->region->first()['id'] : 0;
    }

    public function profileLangDrodown($dropdown = TRUE)
    {
    	return ($this->language_id)
			? [$this->language_id => $this->language->name] + Language::lists('name', 'id')
			: [NULL => LangHelper::get('choose_language', 'Choose language')] + Language::lists('name', 'id');
    }

    public function getLang() {
    	return (is_null($this->language)) ? FALSE : $this->language->name;
    }

    public function getPlayerAge()
    {
        if($this->hasRole("player") AND $this->player)
        {
            return $this->player->contact->date_of_birth;
        }
        return 0;
    }

    public function isAdministrator()
    {
        return in_array($this->getRole(), ['superadmin', 'referee', 'regional_admin', 'national_admin']);
    }

    public function isRefereeAdmin()
    {
        return in_array(7, $this->roles()->get()->lists('id'));
    }

    public function checkPlayerAgeCategory($draw)
    {

        if ($this->hasRole("player")) {
            $years = DateTimeHelper::GetDifferenceInTime($this->player->contact->date_of_birth);
            $years_check = $years >= $draw->category->range_from && $years <= $draw->category->range_to;
            
            $gender_check = $draw->draw_gender == $this->player->contact->sex;
            
            return ($years_check && $gender_check) ? true : false;
        }
    }

    public function checkIfPlayerAlreadySignedUp($draw_id)
    {
        $contact_info = $this->player->contact;
        $signups = TournamentSignup::leftJoin('tournament_teams', 'tournament_signups.id', '=', 'tournament_teams.team_id')
            ->whereDrawId($draw_id)
            ->where('name', 'ILIKE', "%" . $contact_info->name . "%")
            ->where('surname', 'ILIKE', "%" . $contact_info->surname . "%")
            ->where('date_of_birth', '=', DateTimeHelper::GetPostgresDateTime($contact_info->date_of_birth))
            ->first();

        return ($signups) ? true : false;
    }

    public function playerInfo($param = 'name')
    {
        if ($param == 'date_of_birth')
            return ($this->player->contact) ? DateTimeHelper::GetShortDateFullYear($this->player->contact->$param) : false;

        return ($this->player->contact) ? $this->player->contact->$param : false;
    }
}
