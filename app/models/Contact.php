<?php

class Contact extends Eloquent
{
    public $timestamps = false;

    protected $guarded = array('id');

    public function player()
    {
        return $this->hasOne('Player');
    }

    public function user()
    {
        return $this->belongsToMany('User', 'admins');
    }

    public function full_name()
    {
        return $this->surname . ' ' . $this->name;
    }


    //================================ Custom Functions ===============================================


    public function gender()
    {
        $types = ['M' => LangHelper::get('male', 'Male'), 'F' => LangHelper::get('female', 'Female'),];

        return ($this->sex) ? $types[strtoupper($this->sex)] : false;
    }
}