<?php

class Role extends Eloquent
{

    public $timestamps = false;

    // Relations
    public function users()
    {
        return $this->belongsToMany('User', 'users_roles');
    }
}