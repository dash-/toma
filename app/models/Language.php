<?php

class Language extends Eloquent
{
    public $timestamps = false;

    protected $guarded = array('id');

    public function users()
    {
        return $this->hasMany('User', 'language_id');
    }

    //================================ Custom Functions ===============================================

}